﻿namespace Cimp.Messages.MessageParts.Fhl
{
    public class ContactDetail
    {
        public ContactDetail()
        {
        }

        public ContactDetail(string identifier, string number)
        {
            Identifier = identifier;
            Number = number;
        }

        public string Identifier { get; set; }
        public string Number { get; set; }
    }
}