﻿namespace Cimp.Messages.MessageParts.Fhl
{
    public class TarifInformation
    {
        public TarifInformation()
        {
        }

        public TarifInformation(string line)
        {
            Information = line.Substring(4);
        }

        public string Information { get; set; }
    }
}