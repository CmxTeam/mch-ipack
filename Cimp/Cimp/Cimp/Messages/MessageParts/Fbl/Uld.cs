﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimp.Messages.MessageParts.Fbl
{
    public class Uld
    {
        public string Type { get; set; }
        public string SerialNumber { get; set; }
        public string OwnerCode { get; set; }
        public string LoadingIndicator { get; set; }
        public string WeightUOM { get; set; }
        public decimal Weight { get; set; }
        public Uld()
        {

        }
        public static List<Uld> GetUldDescriptionFbls(string line, out int totalCount)
        {
            var retVal = new List<Uld>();

            var parts = line.SplitLines(new[] { @"/" });
            int count;
            int.TryParse(parts.GetValueOfIndex(1), out count);
            totalCount = count;

            var tmpUld = new Uld
                         {
                             Type = parts.GetValueOfIndex(2).Substring<string>(0, 3),
                             SerialNumber = parts.GetValueOfIndex(2).Substring<string>(3, 5),
                             OwnerCode = parts.GetValueOfIndex(2).Substring<string>(8, 2),
                             LoadingIndicator = parts.GetValueOfIndex(2).Substring<string>(11, 2),
                             WeightUOM = parts.GetValueOfIndex(3).Substring<string>(0, 1),
                             Weight = parts.GetValueOfIndex(3).Substring<decimal>(1)
                         };

            retVal.Add(tmpUld);

            if (parts.GetValueOfIndex(4) == null) return retVal;

            tmpUld = new Uld
            {
                Type = parts.GetValueOfIndex(4).Substring<string>(0, 3),
                SerialNumber = parts.GetValueOfIndex(4).Substring<string>(3, 5),
                OwnerCode = parts.GetValueOfIndex(4).Substring<string>(8, 2),
                LoadingIndicator = parts.GetValueOfIndex(4).Substring<string>(11, 2),
                WeightUOM = parts.GetValueOfIndex(5).Substring<string>(0, 1),
                Weight = parts.GetValueOfIndex(5).Substring<decimal>(1)
            };

            retVal.Add(tmpUld);

            if (parts.GetValueOfIndex(6) == null) return retVal;

            tmpUld = new Uld
            {
                Type = parts.GetValueOfIndex(6).Substring<string>(0, 3),
                SerialNumber = parts.GetValueOfIndex(6).Substring<string>(3, 5),
                OwnerCode = parts.GetValueOfIndex(6).Substring<string>(8, 2),
                LoadingIndicator = parts.GetValueOfIndex(6).Substring<string>(11, 2),
                WeightUOM = parts.GetValueOfIndex(7).Substring<string>(0, 1),
                Weight = parts.GetValueOfIndex(7).Substring<decimal>(1)
            };

            retVal.Add(tmpUld);
            return retVal;
        }
    }
}
