﻿using System.Collections.Generic;
using System.Linq;

namespace Cimp.Messages.MessageParts.Fbl
{
    public class Awb
    {
        public Awb()
        {
            Ulds = new List<Uld>();
            Dimensions = new List<DimensionsInformation>();
        }
        public Awb(string line)
        {
            var shiftResult = 0;
            var parts = line.SplitLines(new[] { @"/" });
            AirlinePrefix = parts.GetValueOfIndex(0).Substring<string>(0, 3);
            AwbSerialNumber = parts.GetValueOfIndex(0).Substring<string>(4, 8);
            AirportOfOrigin = parts.GetValueOfIndex(0).Substring<string>(12, 3);
            AirportOfDestination = parts.GetValueOfIndex(0).Substring<string>(15, 3);
            QuantityShipmentDescriptionCode = parts.GetValueOfIndex(1).Substring<string>(0, 1);
            QuantityNummberOfPieces = parts.GetValueOfIndex(1).Substring<int>(1, 4, ref shiftResult);
            WeightCode = parts.GetValueOfIndex(1).Substring<string>(5 - shiftResult, 1);
            Weight = parts.GetValueOfIndex(1).Substring<decimal>(6 - shiftResult, 9, ref shiftResult);

            var volumeDencityIndicator = parts.GetValueOfIndex(1).Substring<string>(15 - shiftResult, 2);
            if (volumeDencityIndicator == "DG")
            {
                DencityIndicator = 1;
                DencityGroup = parts.GetValueOfIndex(1).Substring<int>(17 - shiftResult, 2, ref shiftResult);
                shiftResult += 10;
            }
            else
            {
                DencityIndicator = 0;
                VolumeCode = volumeDencityIndicator;
                VolumeAmount = parts.GetValueOfIndex(1).Substring<decimal>(17 - shiftResult, 12, ref shiftResult);
            }

            if (QuantityShipmentDescriptionCode != "T")
            {
                TotalShipmentDescriptionCode = parts.GetValueOfIndex(1).Substring<string>(29 - shiftResult, 1);
                TotalNumberOfPieces = parts.GetValueOfIndex(1).Substring<int>(30 - shiftResult, 4, ref shiftResult);
            }
            else
            {
                TotalShipmentDescriptionCode = QuantityShipmentDescriptionCode;
                TotalNumberOfPieces = QuantityNummberOfPieces;
            }

            ManifestDescriptionOfGoods = parts.GetValueOfIndex(2).Substring<string>(0);
            if (parts.Count > 3)
            {
                SpecialHandlingCode1 = parts.GetValueOfIndex(3).Substring<string>(0, 3);
                SpecialHandlingCode2 = parts.GetValueOfIndex(3).Substring<string>(3);
            }


            Ulds = new List<Uld>();
            Dimensions = new List<DimensionsInformation>();

        }
        private string _otherServiceInfo1;
        private string _otherServiceInfo2;
        private string _specialServiceRequest1;
        private string _specialServiceRequest2;
        private string _nilCargoCode;


        public string AirlinePrefix { get; set; }
        public string AwbSerialNumber { get; set; }
        public string AirportOfOrigin { get; set; }
        public string AirportOfDestination { get; set; }
        public string QuantityShipmentDescriptionCode { get; set; }
        public int QuantityNummberOfPieces { get; set; }
        public string WeightCode { get; set; }
        public decimal Weight { get; set; }
        public string VolumeCode { get; set; }
        public decimal VolumeAmount { get; set; }
        public int DencityIndicator { get; set; }
        public int DencityGroup { get; set; }
        public string TotalShipmentDescriptionCode { get; set; }
        public int TotalNumberOfPieces { get; set; }
        public string ManifestDescriptionOfGoods { get; set; }
        public List<Uld> Ulds { get; set; }
        public List<DimensionsInformation> Dimensions { get; set; }
        public int TotalUlds { get; set; }
        public string OtherServiceInfo1
        {
            get { return _otherServiceInfo1; }
            set
            {
                if (value == null) return;
                _otherServiceInfo1 = value.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
        }
        public string OtherServiceInfo2
        {
            get { return _otherServiceInfo2; }
            set
            {
                if (value == null) return;
                _otherServiceInfo2 = value.TrimStart('/');
            }
        }
        public string SpecialServiceRequest1
        {
            get { return _specialServiceRequest1; }
            set
            {
                if (value == null) return;
                _specialServiceRequest1 = value.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            }
        }
        public string SpecialServiceRequest2
        {
            get { return _specialServiceRequest2; }
            set
            {
                if (value == null) return;
                _specialServiceRequest2 = value.TrimStart('/');
            }
        }
        public string SpecialHandlingCode1 { get; set; }
        public string SpecialHandlingCode2 { get; set; }
        public string AgentName { get; set; }
        public string InwardFlight { get; set; }
        public string MovementPriorityCode { get; set; }
      
        public void AddDimOrOrirginInfo(string line)
        {
            var parts = line.SplitLines(Constants.Hyphen);

            if (parts.Count == 3)
            {
                //Dimension
                Dimensions.Add(new DimensionsInformation(AirlinePrefix, AwbSerialNumber, line));
            }
            else
            {
                //Awb origin info line
                var originParts = line.SplitLines(Constants.SlantSeparator);
                AgentName = originParts.GetValueOfIndex(1);
                MovementPriorityCode = originParts.GetValueOfIndex(3);

                int shift = 0;
                var flight = originParts.GetValueOfIndex(2);
                if (!string.IsNullOrEmpty(flight))
                {
                    InwardFlight = originParts.GetValueOfIndex(2).Substring(0, 2) + Constants.Hyphen[0]
                                   + originParts.GetValueOfIndex(2).Substring(2) + Constants.Hyphen[0]
                                   + originParts.GetValueOfIndex(3) + Constants.Hyphen[0]
                                   + originParts.GetValueOfIndex(4);
                    shift = 2;
                }
                MovementPriorityCode = originParts.GetValueOfIndex(3 + shift);

            }

        }
    }
}
