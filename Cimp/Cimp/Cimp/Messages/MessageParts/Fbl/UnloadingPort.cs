﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimp.Messages.MessageParts.Fbl
{
    public class UnloadingPort
    {
        public UnloadingPort()
        {
            Awbs = new List<Awb>();
        }

        public UnloadingPort(string line)
        {
            var parts = line.SplitLines(new[] { @"/" });
            UnloadingAirportCode = parts.GetValueOfIndex(0);
            _nilCargoCode = parts.GetValueOfIndex(1);
            Awbs = new List<Awb>();
        }
        public string UnloadingAirportCode { get; set; }
        public int IsNilCargo
        {
            get
            {
                return _nilCargoCode == "NIL" ? 1 : 0;
            }
            set { /* requires for serialization */}
        }
        public List<Awb> Awbs { get; set; }
        private string _nilCargoCode;
    }
}
