﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimp.Messages.MessageParts.Fwb
{
    public class LegSegment
    {
        public string CarrierCode { get; set; }
        public string FlightNumber { get; set; }
        public string Day { get; set; }
        public string AirportCode { get; set; }

        public static List<LegSegment> CerateLegSegments(string line)
        {
            var parts = line.SplitLines(new[] { @"/" });
            parts.RemoveAt(0);

            var retList = new List<LegSegment>();
            for (int i = 0; i < parts.Count; i += 2)
            {
                retList.Add(new LegSegment
                {
                    CarrierCode = parts.GetValueOfIndex(i).Substring<string>(0,2),
                    FlightNumber = parts.GetValueOfIndex(i).Substring<string>(2),
                    Day = parts.GetValueOfIndex(i + 1)
                });
            }
            return retList;
        }

        public static void ProcessLegSegments(List<LegSegment> segments, string line)
        {
            var parts = line.SplitLines(new[] { @"/" });
            parts.RemoveAt(0);
            int currentSegmentIndex = 0;
            for (int i = 0; i < parts.Count; i++)
            {
                var tmpAirportCode = parts.GetValueOfIndex(i).Substring<string>(0, 3);
                var tmpCarrierCode = parts.GetValueOfIndex(i).Substring<string>(3);

                if (segments.Count > currentSegmentIndex)
                {
                    segments[currentSegmentIndex].AirportCode = tmpAirportCode;
                    segments[currentSegmentIndex].CarrierCode = tmpCarrierCode;
                }
                else
                {
                    var tmpSegment = new LegSegment() { AirportCode = tmpAirportCode, CarrierCode = tmpCarrierCode };
                    segments.Add(tmpSegment);
                }
                currentSegmentIndex++;
            }
        }
    }
}
