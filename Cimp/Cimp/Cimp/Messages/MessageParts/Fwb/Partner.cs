﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimp.Messages.MessageParts.Fwb
{
    public class Partner
    {
        public Partner() { ContactDetails = new List<ContactDetail>(); }

        public Partner(List<string> lines)
        {
            var tmpLine = lines[0];
            AccountNumber = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);

            tmpLine = lines[1];
            Name = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);

            tmpLine = lines[2];
            StreetAddress = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);

            tmpLine = lines[3];
            LocationPlace = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            LocationState = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(2);

            tmpLine = lines[4];
            CountryCode = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            PostalCode = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(2);

            ContactDetails = new List<ContactDetail>();
            if (lines.Count > 5)
            {
                for (int i = 5; i < lines.Count; i++)
                {
                    tmpLine = lines[i];
                    var cont = new ContactDetail
                        {
                            Identifier = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1),
                            Number = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(2)
                        };
                    ContactDetails.Add(cont);
                }
            }
        }


        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public string StreetAddress { get; set; }
        public string LocationPlace { get; set; }
        public string LocationState { get; set; }
        public string CountryCode { get; set; }
        public string PostalCode { get; set; }

        public List<ContactDetail> ContactDetails { get; set; }


        public static Partner GetAlsoNotifyPartner(List<string> lines)
        {
            var tmpPartner = new Partner();

            var tmpLine = lines[0];
            tmpPartner.Name = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);

            tmpLine = lines[1];
            tmpPartner.StreetAddress = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);

            tmpLine = lines[2];
            tmpPartner.LocationPlace = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            tmpPartner.LocationState = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(2);

            tmpLine = lines[3];
            tmpPartner.CountryCode = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            tmpPartner.PostalCode = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(2);

            tmpPartner.ContactDetails = new List<ContactDetail>();
            if (lines.Count > 4)
            {
                for (int i = 4; i < lines.Count; i++)
                {
                    tmpLine = lines[i];
                    var cont = new ContactDetail
                    {
                        Identifier = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1),
                        Number = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(2)
                    };
                    tmpPartner.ContactDetails.Add(cont);
                }
            }

            return tmpPartner;
        }
    }

    public class ContactDetail
    {
        public string Identifier { get; set; }
        public string Number { get; set; }
    }
}
