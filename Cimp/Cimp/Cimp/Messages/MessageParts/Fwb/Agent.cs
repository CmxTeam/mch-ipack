﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cimp.Messages.MessageParts.Fwb
{
    public class Agent
    {
        public Agent(){}

        public Agent(List<string> lines)
        {
            var tmpLine = lines[0];
            AccountNumber = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
            IATACargoAgentCode = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(2);
            IATACargoCassAddress = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(3);
            ParticipantIdentifier = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(4);

            tmpLine = lines[1];
            Name = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);

            tmpLine = lines[2];
            Place = tmpLine.SplitLines(new[] { @"/" }).GetValueOfIndex(1);
        }
        
        public string AccountNumber { get; set; }
        public string IATACargoAgentCode { get; set; }
        public string IATACargoCassAddress { get; set; }
        public string ParticipantIdentifier { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
    }
}
