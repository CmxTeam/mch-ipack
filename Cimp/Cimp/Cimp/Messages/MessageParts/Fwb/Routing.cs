﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;

namespace Cimp.Messages.MessageParts.Fwb
{
    public class Routing
    {
        public string AirportCode { get; set; }
        public string CarrierCode { get; set; }

        public static List<Routing> GetRoutings(string line)
        {
            var parts = line.SplitLines(new[] { @"/" });
            parts.RemoveAt(0);
            return parts.Select(part => new Routing
                                {
                                    AirportCode = part.Substring<string>(0, 3), 
                                    CarrierCode = part.Substring<string>(3)
                                }).ToList();
        }
    }
}
