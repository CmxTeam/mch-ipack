﻿namespace Cimp.Messages.MessageParts
{
    public class DimensionsInformation : PartOfAirbillBase
    {
        public DimensionsInformation()
        {
        }

        public DimensionsInformation(string airbillPrefix, string awbSerialNumber, string line) : base(airbillPrefix, awbSerialNumber)
        {
            var parts = line.SplitLines(new[] { @"/" });
            WeightCode = parts.GetValueOfIndex(1).Substring<string>(0, 1);
            Weight = parts.GetValueOfIndex(1).Substring<decimal>(1, 7);
            
            var dimParts = parts.GetValueOfIndex(2).SplitLines(new []{@"-"});
            
            MeasurementUnitCode = dimParts.GetValueOfIndex(0).Substring<string>(0, 3);
            LengthDimension = dimParts.GetValueOfIndex(0).Substring<decimal>(3);
            WidthDimension = dimParts.GetValueOfIndex(1).Substring<decimal>(0);
            HeightDimension = dimParts.GetValueOfIndex(2).Substring<decimal>(0);
            NumberOfPieces = parts.GetValueOfIndex(3).Substring<int>(0);
        }
                
        public string WeightCode { get; set; }
        public decimal Weight { get; set; }
        public string MeasurementUnitCode { get; set; }
        public decimal LengthDimension { get; set; }
        public decimal WidthDimension { get; set; }
        public decimal HeightDimension { get; set; }
        public int NumberOfPieces { get; set; }
    }
}