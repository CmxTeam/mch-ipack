﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Cimp.Messages;

namespace Cimp
{
    public class RawMessageProcessor : BaseProcessor
    {
        private readonly string _folderPath;
        private readonly string _archivePath;
        private readonly string _connectionString;        

        public RawMessageProcessor(string connectionString, string folderPath, string archivePath)
        {
            _folderPath = folderPath;
            _archivePath = archivePath;
            _connectionString = connectionString;
        }

        public void ProcessFiles(string filter = "*.*")
        {
            if (string.IsNullOrEmpty(_folderPath)) return;
            var files = ReadFilesContent(_folderPath, filter);
            ProcessMessages(files);
        }

        private void ProcessMessages(IEnumerable<File> files)
        {
            foreach (var file in files)
            {
                var raw = new RawMessage(file.Content, _connectionString, file.Name);
                string errorMessage;
                raw.Save(out errorMessage);
                MoveToArchive(file.Name + file.FileExtension);
            }
        }

        private void MoveToArchive(string fileName)
        {
            if(string.IsNullOrEmpty(_archivePath)) return;

            if (!Directory.Exists(_archivePath))
                Directory.CreateDirectory(_archivePath);
            
            var archiveFileName = Path.Combine(_archivePath, fileName);

            if(System.IO.File.Exists(archiveFileName))            
                archiveFileName += DateTime.Now.ToString("ddMMMMyyyymmss");            

            System.IO.File.Move(Path.Combine(_folderPath, fileName), archiveFileName);
        }
    }
}
