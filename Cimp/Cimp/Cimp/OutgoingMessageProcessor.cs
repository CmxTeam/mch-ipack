﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Cimp
{
    public class OutgoingMessageProcessor
    {
        private readonly string _folderName;        
        private readonly string _connectionString;
        
        public OutgoingMessageProcessor(string connectionString, string folderName)
        {
            _folderName = folderName;
            _connectionString = connectionString;
        }

        public void ProcessMessages(string extension)
        {
            var files = GetMessagesFromDatabase();

            foreach (var outgoingFile in files)
            {
                var status = outgoingFile.Save(_folderName, extension);
                MarkRecordAsExecuted(outgoingFile.Id, status.Error);
            }
        }

        private IEnumerable<OutgoingFile> GetMessagesFromDatabase()
        {
            var dataTable = new DataTable();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "Select Id, MessageType, MailboxName, RawMessage, SenderMailbox, Priority, DownloadedTime From CIMPMessageLogs Where ProcessingStatus = 0 and Direction = N'OUT' order by SentTime";

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }

            return dataTable.Rows.OfType<DataRow>().Select(r => new OutgoingFile
                                                                         {
                                                                             Id = Convert.ToInt64(r["Id"]), 
                                                                             MessageType = r["RawMessage"].ToString(),
                                                                             MailboxName = r["MailboxName"].ToString(),
                                                                             RawMessage = r["RawMessage"].ToString(),
                                                                             SenderMailbox = r["SenderMailbox"].ToString(),
                                                                             Priority = r["Priority"].ToString(),
                                                                             Date = Convert.ToDateTime(r["DownloadedTime"])
                                                                         });            
        }

        private void MarkRecordAsExecuted(long id, string errorMessage)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand("Update CIMPMessageLogs Set ProcessingStatus = 1, ErrorMessage = @ErrorMessage Where Id = @Id", connection))
                {
                    command.CommandType = CommandType.Text;
                    try
                    {
                        command.Parameters.Add(new SqlParameter("@Id", id));
                        command.Parameters.Add(new SqlParameter("@ErrorMessage", string.IsNullOrEmpty(errorMessage) ? (object)DBNull.Value : errorMessage));                        

                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
        }
    }
}
