﻿using System;
using System.IO;
using System.Text;

namespace Cimp
{
    public class OutgoingFile
    {
        public long Id { get; set; }
        public string MessageType { get; set; }
        public string MailboxName { get; set; }
        public string RawMessage { get; set; }
        public string SenderMailbox { get; set; }
        public string Priority { get; set; }
        public DateTime Date { get; set; }

        public ExecutionStatus Save(string path, string extension)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            var filePath = Path.Combine(path, string.Format("OUT_{0}_{1}{2}", Id, DateTime.Now.ToString("ddMMMMyyyymmss"), extension));
            try
            {
                using (var fs = System.IO.File.Create(filePath))
                {
                    var info = new UTF8Encoding(true).GetBytes(GetFileContent());
                    fs.Write(info, 0, info.Length);
                }
                return new ExecutionStatus { Status = true };
            }
            catch (Exception ex)
            {
                return new ExecutionStatus { Status = false, Error = ex.Message};
            }
        }

        private string GetFileContent()
        {
            return string.Format("{0}{1}{2} {3}{0}.{4} {5}{0}{6}{7}{8}", Environment.NewLine, (char)1, Priority, MailboxName, SenderMailbox, Date.ToString("ddhhmm"), (char)2, RawMessage, (char)3);
        }
    }
}