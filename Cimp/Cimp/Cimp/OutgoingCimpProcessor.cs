﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Cimp
{
    public class OutgoingCimpProcessor : IDisposable
    {
        private readonly string _connectionString;

        public OutgoingCimpProcessor(string connection)
        {
            _connectionString = connection;
        }

        public void ProcessOutgoingCimp()
        {
            try
            {
                using (var connection = new SqlConnection(_connectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "ProcessCIMPOutbound";

                        command.Connection.Open();
                        var errMessage = (string)command.ExecuteScalar();
                        if (!string.IsNullOrEmpty(errMessage))
                        {
                            throw new ApplicationException(errMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Processing CIMP failed: " + ex.ToString());
            }

        }

        public void Dispose()
        {

        }
    }
}
