﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestUPSServices.ShippingService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ShippingFault", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
    [System.SerializableAttribute()]
    public partial class ShippingFault : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ExceptionCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ExceptionDescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ExceptionNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ExceptionSourceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string InnerExceptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private TestUPSServices.ShippingService.ExceptionSeverity SeverityField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ExceptionCode {
            get {
                return this.ExceptionCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ExceptionCodeField, value) != true)) {
                    this.ExceptionCodeField = value;
                    this.RaisePropertyChanged("ExceptionCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ExceptionDescription {
            get {
                return this.ExceptionDescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.ExceptionDescriptionField, value) != true)) {
                    this.ExceptionDescriptionField = value;
                    this.RaisePropertyChanged("ExceptionDescription");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ExceptionNumber {
            get {
                return this.ExceptionNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.ExceptionNumberField, value) != true)) {
                    this.ExceptionNumberField = value;
                    this.RaisePropertyChanged("ExceptionNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ExceptionSource {
            get {
                return this.ExceptionSourceField;
            }
            set {
                if ((object.ReferenceEquals(this.ExceptionSourceField, value) != true)) {
                    this.ExceptionSourceField = value;
                    this.RaisePropertyChanged("ExceptionSource");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string InnerException {
            get {
                return this.InnerExceptionField;
            }
            set {
                if ((object.ReferenceEquals(this.InnerExceptionField, value) != true)) {
                    this.InnerExceptionField = value;
                    this.RaisePropertyChanged("InnerException");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public TestUPSServices.ShippingService.ExceptionSeverity Severity {
            get {
                return this.SeverityField;
            }
            set {
                if ((this.SeverityField.Equals(value) != true)) {
                    this.SeverityField = value;
                    this.RaisePropertyChanged("Severity");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ExceptionSeverity", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
    public enum ExceptionSeverity : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Hard = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Transient = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Authentication = 2,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TrackingType", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
    public enum TrackingType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Shipment = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Package = 2,
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TrackingRequestType", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
    public enum TrackingRequestType : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Last = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        All = 1,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TrackActivity", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
    [System.SerializableAttribute()]
    public partial class TrackActivity : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ActivityCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ActivityDescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ActivityLocationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ActivityTimeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long PackageIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TrackingNumberField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ActivityCode {
            get {
                return this.ActivityCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ActivityCodeField, value) != true)) {
                    this.ActivityCodeField = value;
                    this.RaisePropertyChanged("ActivityCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ActivityDescription {
            get {
                return this.ActivityDescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.ActivityDescriptionField, value) != true)) {
                    this.ActivityDescriptionField = value;
                    this.RaisePropertyChanged("ActivityDescription");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ActivityLocation {
            get {
                return this.ActivityLocationField;
            }
            set {
                if ((object.ReferenceEquals(this.ActivityLocationField, value) != true)) {
                    this.ActivityLocationField = value;
                    this.RaisePropertyChanged("ActivityLocation");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime ActivityTime {
            get {
                return this.ActivityTimeField;
            }
            set {
                if ((this.ActivityTimeField.Equals(value) != true)) {
                    this.ActivityTimeField = value;
                    this.RaisePropertyChanged("ActivityTime");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long PackageId {
            get {
                return this.PackageIdField;
            }
            set {
                if ((this.PackageIdField.Equals(value) != true)) {
                    this.PackageIdField = value;
                    this.RaisePropertyChanged("PackageId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TrackingNumber {
            get {
                return this.TrackingNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.TrackingNumberField, value) != true)) {
                    this.TrackingNumberField = value;
                    this.RaisePropertyChanged("TrackingNumber");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="VoidResult", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
    [System.SerializableAttribute()]
    public partial class VoidResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long EntityIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsVoidedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StatusDescriptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string UPSNoField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long EntityId {
            get {
                return this.EntityIdField;
            }
            set {
                if ((this.EntityIdField.Equals(value) != true)) {
                    this.EntityIdField = value;
                    this.RaisePropertyChanged("EntityId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsVoided {
            get {
                return this.IsVoidedField;
            }
            set {
                if ((this.IsVoidedField.Equals(value) != true)) {
                    this.IsVoidedField = value;
                    this.RaisePropertyChanged("IsVoided");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StatusDescription {
            get {
                return this.StatusDescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.StatusDescriptionField, value) != true)) {
                    this.StatusDescriptionField = value;
                    this.RaisePropertyChanged("StatusDescription");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string UPSNo {
            get {
                return this.UPSNoField;
            }
            set {
                if ((object.ReferenceEquals(this.UPSNoField, value) != true)) {
                    this.UPSNoField = value;
                    this.RaisePropertyChanged("UPSNo");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ShippingService.IShippingWS")]
    public interface IShippingWS {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShippingWS/CreateShipmentForOnhand", ReplyAction="http://tempuri.org/IShippingWS/CreateShipmentForOnhandResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(TestUPSServices.ShippingService.ShippingFault), Action="http://tempuri.org/IShippingWS/CreateShipmentForOnhandShippingFaultFault", Name="ShippingFault", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
        bool CreateShipmentForOnhand(int carrierId, long onhandId, int carrierServiceTypeId, string userName, long taskId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShippingWS/GetTracking", ReplyAction="http://tempuri.org/IShippingWS/GetTrackingResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(TestUPSServices.ShippingService.ShippingFault), Action="http://tempuri.org/IShippingWS/GetTrackingShippingFaultFault", Name="ShippingFault", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
        TestUPSServices.ShippingService.TrackActivity[] GetTracking(int carrierId, long manifestId, TestUPSServices.ShippingService.TrackingType trackType, TestUPSServices.ShippingService.TrackingRequestType requestType, string userName, long taskId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShippingWS/GetLastTracking", ReplyAction="http://tempuri.org/IShippingWS/GetLastTrackingResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(TestUPSServices.ShippingService.ShippingFault), Action="http://tempuri.org/IShippingWS/GetLastTrackingShippingFaultFault", Name="ShippingFault", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
        TestUPSServices.ShippingService.TrackActivity[] GetLastTracking(int carrierId, long manifestId, TestUPSServices.ShippingService.TrackingType trackType, string userName, long taskId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShippingWS/VoidShipment", ReplyAction="http://tempuri.org/IShippingWS/VoidShipmentResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(TestUPSServices.ShippingService.ShippingFault), Action="http://tempuri.org/IShippingWS/VoidShipmentShippingFaultFault", Name="ShippingFault", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
        TestUPSServices.ShippingService.VoidResult VoidShipment(int carrierId, long onhandId, string userName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IShippingWS/TestVoidShipment", ReplyAction="http://tempuri.org/IShippingWS/TestVoidShipmentResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(TestUPSServices.ShippingService.ShippingFault), Action="http://tempuri.org/IShippingWS/TestVoidShipmentShippingFaultFault", Name="ShippingFault", Namespace="http://schemas.datacontract.org/2004/07/ShipmentEntities")]
        TestUPSServices.ShippingService.VoidResult[] TestVoidShipment(string shipment, string[] trackings, int carrierId, long onhandId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IShippingWSChannel : TestUPSServices.ShippingService.IShippingWS, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ShippingWSClient : System.ServiceModel.ClientBase<TestUPSServices.ShippingService.IShippingWS>, TestUPSServices.ShippingService.IShippingWS {
        
        public ShippingWSClient() {
        }
        
        public ShippingWSClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ShippingWSClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ShippingWSClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ShippingWSClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool CreateShipmentForOnhand(int carrierId, long onhandId, int carrierServiceTypeId, string userName, long taskId) {
            return base.Channel.CreateShipmentForOnhand(carrierId, onhandId, carrierServiceTypeId, userName, taskId);
        }
        
        public TestUPSServices.ShippingService.TrackActivity[] GetTracking(int carrierId, long manifestId, TestUPSServices.ShippingService.TrackingType trackType, TestUPSServices.ShippingService.TrackingRequestType requestType, string userName, long taskId) {
            return base.Channel.GetTracking(carrierId, manifestId, trackType, requestType, userName, taskId);
        }
        
        public TestUPSServices.ShippingService.TrackActivity[] GetLastTracking(int carrierId, long manifestId, TestUPSServices.ShippingService.TrackingType trackType, string userName, long taskId) {
            return base.Channel.GetLastTracking(carrierId, manifestId, trackType, userName, taskId);
        }
        
        public TestUPSServices.ShippingService.VoidResult VoidShipment(int carrierId, long onhandId, string userName) {
            return base.Channel.VoidShipment(carrierId, onhandId, userName);
        }
        
        public TestUPSServices.ShippingService.VoidResult[] TestVoidShipment(string shipment, string[] trackings, int carrierId, long onhandId) {
            return base.Channel.TestVoidShipment(shipment, trackings, carrierId, onhandId);
        }
    }
}
