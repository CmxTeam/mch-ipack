﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShipmentEntities;

namespace ShippingInterface
{
    public interface IQuantumViewService
    {
        List<QVActivity> GetQVData(string qvURL, Credentials creds);
    }
}
