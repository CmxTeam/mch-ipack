﻿using System.Collections.Generic;
using ShipmentEntities;

namespace ShippingInterface
{
    public interface IShippingService
    {
        CreatedShipment CreateShipment(Partner shipper, Partner shipFrom, Partner shipTo, string accountNumber, string serviceCode, List<ShipPackage> packages, Credentials creds);
    }
}
