﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.ServiceModel;
using ShipmentEntities;
using ShippingInterface;
using USPSBLL.USPSLabelService;

namespace USPSBLL.ShippingPackage
{
    public class USPSShippingService : IShippingService
    {

        public static string SendShipment(LabelRequest request, Credentials creds,out string label)
        {
            var labelService =
                new EwsLabelServiceSoapClient(new WSHttpBinding(), new EndpointAddress(ConfigurationManager.AppSettings["USPSServiceURL"]));

            LabelRequestResponse response = labelService.GetPostageLabel(request);

            //TODO: Fill shipInfo with data.
            if (response.Status == 0)
            {
                label = response.Base64LabelImage;
                return response.TrackingNumber;
            }
            else
            {
                throw new ShippingException(ExceptionSeverity.Hard, response.Status.ToString(CultureInfo.InvariantCulture),
                    response.ErrorMessage, MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void RecreditAccount(double amount, Credentials creds)
        {
            if (amount < 10 && amount > 99999.99)
                throw new ShippingException(ExceptionSeverity.Hard, "CMX",
                    "Amount should be greater than $10 and less than $99,999.99", "RecreditAccount");

            var srv =
                new EwsLabelServiceSoapClient(new WSHttpBinding(), new EndpointAddress(ConfigurationManager.AppSettings["USPSServiceURL"]));

            var request = new RecreditRequest
                          {
                              CertifiedIntermediary = new CertifiedIntermediary()
                              {
                                  AccountID = creds.Username,
                                  PassPhrase = creds.Password
                              },
                              RecreditAmount = Math.Round(amount, 2).ToString(CultureInfo.InvariantCulture),
                              RequesterID = creds.AccessKey,
                              RequestID = "CMX" + DateTime.Now.ToString("yyyyMMddHHss")
                          };

            var result = srv.BuyPostage(request);
        }

        public static void ChangePassword(string newPassword, Credentials creds)
        {
            if (string.IsNullOrEmpty(newPassword) || newPassword.Length < 5 || newPassword.Length > 64)
                throw new ShippingException(ExceptionSeverity.Hard, "CMX",
                    "New password should be at least 5 characters long and maximum 64 characters long", "ChangePassword");

            var srv =
               new EwsLabelServiceSoapClient(new WSHttpBinding(), new EndpointAddress(ConfigurationManager.AppSettings["USPSServiceURL"]));

            var request = new ChangePassPhraseRequest
                          {
                              CertifiedIntermediary = new CertifiedIntermediary()
                                                      {
                                                          AccountID = creds.Username,
                                                          PassPhrase = creds.Password
                                                      },
                              RequesterID = creds.AccessKey,
                              RequestID = "CMX" + DateTime.Now.ToString("yyyyMMddHHss")
                          };
            var result = srv.ChangePassPhrase(request);
        }

        public CreatedShipment CreateShipment(Partner shipper, Partner shipFrom, Partner shipTo, string accountNumber,
            string serviceCode, List<ShipPackage> packages, Credentials creds)
        {
            var request = new LabelRequest();

            //TODO: Fill request.

#if DEBUG
            request.Test = "YES";
#endif


            //USPS Sevice access.
            request.AccountID = creds.Username;
            request.PassPhrase = creds.Password;
            request.RequesterID = creds.AccessKey;


            //FROM Address
            request.ReturnAddress1 = shipFrom.PartnerAddress.AddressLine;
            request.FromCity = shipFrom.PartnerAddress.City;
            request.FromState = shipFrom.PartnerAddress.StateProvinceCode;
            request.FromPostalCode = shipFrom.PartnerAddress.PostalCode;
            request.FromCountry = shipFrom.PartnerAddress.CountryCode;
            request.FromPhone = shipFrom.PhoneNumber;
            request.FromCompany = shipFrom.Name;
            request.FromEMail = shipFrom.EmailAddress;

            //TO Address
            request.ToAddress1 = shipTo.PartnerAddress.AddressLine;
            request.ToCity = shipTo.PartnerAddress.City;
            request.ToState = shipTo.PartnerAddress.StateProvinceCode;
            request.ToPostalCode = shipTo.PartnerAddress.PostalCode;
            request.ToCountryCode = shipTo.PartnerAddress.CountryCode;
            request.ToPhone = shipTo.PhoneNumber;
            request.ToCompany = shipTo.Name;
            request.ToEMail = shipTo.EmailAddress;

            request.ImageFormat = "ZPLII";
            request.Services.DeliveryConfirmation = "ON";

            var ship = new CreatedShipment
                       {
                           PackageShippingLabels = new List<string>(),
                           TrackingNumbers = new List<string>()
                       };

            foreach (var pack in packages)
            {
                //USPS expects weight in ounces, so if needed, convert KG to LB, then convert to OZ.
                request.WeightOz = pack.WeightUOM.Code == "LB" ? pack.Weight * GlobalConstants.LBToOZCoeff : pack.Weight * GlobalConstants.KGToLBCoeff * GlobalConstants.LBToOZCoeff;

                //USPS expects dimentions in inches, so convert if necessary.
                request.MailpieceDimensions.Width = pack.PackageDims.DimensionUOM.Code == "IN" ? pack.PackageDims.Width : pack.PackageDims.Width * GlobalConstants.CMToINCoeff;
                request.MailpieceDimensions.Height = pack.PackageDims.DimensionUOM.Code == "IN" ? pack.PackageDims.Height : pack.PackageDims.Height * GlobalConstants.CMToINCoeff;
                request.MailpieceDimensions.Length = pack.PackageDims.DimensionUOM.Code == "IN" ? pack.PackageDims.Length : pack.PackageDims.Length * GlobalConstants.CMToINCoeff;
                string label;
                string track = SendShipment(request, creds, out label);
                ship.PackageShippingLabels.Add(label);
                ship.TrackingNumbers.Add(track);
            }

            return ship;
        }
    }
}
