﻿using System;
using System.Reflection;

namespace ShipmentEntities
{
    public class ShippingException : Exception
    {
        public ExceptionSeverity Severity { get; set; }
        public string ExceptionCode { get; set; }
        public string ExceptionDescription { get; set; }
        public string ExceptionSource { get; set; }

        public ShippingException()
            : base("Unknown exception in CMX. Severity: Hard")
        {
            ExceptionCode = "CMX";
            Severity = ExceptionSeverity.Hard;
            ExceptionSource = MethodBase.GetCurrentMethod().Name;
            ExceptionDescription = "Unknown exception";
        }

        public ShippingException(Exception ex)
            : base("Exception in CMX. Severity: Hard", ex)
        {
            ExceptionCode = "CMX";
            Severity = ExceptionSeverity.Hard;
            ExceptionSource = ex.StackTrace;
            ExceptionDescription = ex.Message;
        }

        public ShippingException(string message, Exception ex)
            : base(message, ex)
        {
            ExceptionCode = "CMX";
            Severity = ExceptionSeverity.Hard;
            ExceptionSource = ex.StackTrace;
            ExceptionDescription = message;
        }

        public ShippingException(string message, Exception ex, ExceptionSeverity severity)
            : base("Exception in CMX. Severity: Hard", ex)
        {
            ExceptionCode = "CMX";
            Severity = severity;
            ExceptionSource = ex.StackTrace;
            ExceptionDescription = message;
        }

        public ShippingException(ExceptionSeverity severity, string exCode, string exDescription, string source)
            : base(exDescription + ". In: " + source + ". Severity: " + severity.ToString())
        {
            Severity = severity;
            ExceptionSource = source;
            ExceptionDescription = exDescription;
            ExceptionCode = exCode;
        }

        public static ExceptionSeverity GetSeverity(string strSeverity)
        {
            switch (strSeverity.ToUpper())
            {
                case "TRANSIENT":
                    return ExceptionSeverity.Transient;
                case "AUTHENTICATION":
                    return ExceptionSeverity.Authentication;
                default:
                    return ExceptionSeverity.Hard;
            }
        }

    }

    public class ShippingFault
    {
        public ExceptionSeverity Severity { get; set; }
        public string ExceptionCode { get; set; }
        public string ExceptionDescription { get; set; }
        public string ExceptionSource { get; set; }
        public string InnerException { get; set; }
        public string ExceptionNumber { get; set; }

    }

    public enum ExceptionSeverity
    {
        Hard,
        Transient,
        Authentication
    }
}
