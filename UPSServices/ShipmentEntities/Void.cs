﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShipmentEntities
{
    public class VoidResult
    {
        public long EntityId { get; set; }
        public string UPSNo { get; set; }
        public bool IsVoided { get; set; }
        public string StatusDescription { get; set; }
    }
}
