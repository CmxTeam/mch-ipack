﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace ShipmentEntities
{

    [DataContract]
    public class Shipment
    {
        [DataMember]
        public string ShipmentDescription { get; set; }
        [DataMember]
        public string ReturnServiceCode { get; set; }
        [DataMember]
        public string ReturnServiceDescription { get; set; }
        [DataMember]
        public bool IsDocumentsOnly { get; set; }
        [DataMember]
        public Partner Shipper { get; set; }
        [DataMember]
        public Partner Consignee { get; set; }
        [DataMember]
        public Partner ShipFrom { get; set; }
        [DataMember]
        public PaymentInformation Payment { get; set; }
        [DataMember]
        public FRSPaymentInformation FRSPayment { get; set; }
        [DataMember]
        public bool GoodsNotInFreeCirculationIndicator { get; set; }
        [DataMember]
        public ShipmentRatingOptions RatingOptions { set; get; }
        [DataMember]
        public string MovementReferenceNumber { get; set; }
        [DataMember]
        public ReferenceNumber ShipmentReference { get; set; }
        [DataMember]
        public Service ShipmentService { get; set; }
        [DataMember]
        public InvoiceLineTotal InvoiceLine { get; set; }
        [DataMember]
        public int NumOfPiecesInShipment { get; set; }
        [DataMember]
        public bool ItemizedChargesRequestedIndicator { get; set; }
        [DataMember]
        public string USPSEndorsement { get; set; }
        [DataMember]
        public string MILabelCN22Indicator { get; set; }
        [DataMember]
        public string SubClassification { get; set; }
        [DataMember]
        public string CostCenter { get; set; }
        [DataMember]
        public string IrregularIndicator { get; set; }
        [DataMember]
        public ShipmentServiceOptions ServiceOptions { get; set; }
        [DataMember]
        public List<ShipPackage> Packages { get; set; }
        [DataMember]
        public Label LabelSpecification { get; set; }
        [DataMember]
        public Receipt ReceiptSpecification { get; set; }

    }

    [DataContract]
    public class Partner
    {
        private string _name;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value.SafeSubstring(0, 35); }
        }

        [DataMember]
        public string AttentionName { get; set; }
        [DataMember]
        public string CompanyDisplayableName { get; set; }
        [DataMember]
        public string TaxIdentificationNumber { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string PhoneExtension { get; set; }
        [DataMember]
        public string ShipperNumber { get; set; }
        [DataMember]
        public string FaxNumber { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public Address PartnerAddress { get; set; }
        [DataMember]
        public string LocationID { get; set; }
        [DataMember]
        public string Option { get; set; }

    }

    [DataContract]
    public class Address
    {
        private string _addressLine;

        [DataMember]
        public string AddressLine
        {
            get { return _addressLine; }
            set { _addressLine = value.SafeSubstring(0, 35); }
        }

        private string _city;

        [DataMember]
        public string City
        {
            get { return _city; }
            set { _city = value.SafeSubstring(0, 30); }
        }

        [DataMember]
        public string Town { get; set; }
        [DataMember]
        public string StateProvinceCode { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public bool ResidentialAddressIndicator { get; set; }
    }

    [DataContract]
    public class PaymentInformation
    {
        [DataMember]
        public ShipmentCharge Charge { get; set; }
        [DataMember]
        public bool SplitDutyVATIndicator { get; set; }

    }

    [DataContract]
    public class ShipmentCharge
    {
        [DataMember]
        public string ChargeType { get; set; }
        [DataMember]
        public Billing ChargeBilling { get; set; }
        [DataMember]
        public BillReceiver ChargeBillReceiver { get; set; }
        [DataMember]
        public BillShipper ChargeBillShipper { get; set; }
        [DataMember]
        public BillThirdParty ChargeBillThirdParty { get; set; }
        [DataMember]
        public bool ConsigneeBilledIndicator { get; set; }

    }

    [DataContract]
    public class BillShipper
    {
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public string CreditCardType { get; set; }
        [DataMember]
        public string CreditCardNumber { get; set; }

        private DateTime _cardExpireDate;
        [DataMember]
        public string CardExpireDate
        {
            get { return _cardExpireDate.ToString("MMYYYY"); }
            set { _cardExpireDate = DateTime.Parse(value); }
        }
        [DataMember]
        public string CardSecurityCode { get; set; }
        [DataMember]
        public Address CardBillingAddress { get; set; }

    }

    [DataContract]
    public class BillReceiver
    {
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public Address Address { get; set; }
    }

    [DataContract]
    public class BillThirdParty
    {
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public Address Address { get; set; }
    }

    [DataContract]
    public class FRSPaymentInformation
    {
        [DataMember]
        public string TypeCode { get; set; }
        [DataMember]
        public string TypeDescription { get; set; }
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public Address FRSAddress { get; set; }
    }

    public enum Billing
    {
        Shipper,
        Receiver,
        ThirdParty
    }

    [DataContract]
    public class ShipmentRatingOptions
    {
        [DataMember]
        public bool NegotiatedRates { get; set; }
        [DataMember]
        public bool FRSShipment { get; set; }
        [DataMember]
        public bool RateChart { get; set; }
    }

    [DataContract]
    public class ReferenceNumber
    {
        [DataMember]
        public bool BarcodeIndicator { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class Service
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public class InvoiceLineTotal
    {
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public string MonetaryValue { get; set; }
    }

    [DataContract]
    public class ShipmentServiceOptions
    {
        [DataMember]
        public bool SaturdayDeliveryIndicator { get; set; }
        [DataMember]
        public COD CODContainer { get; set; }
        [DataMember]
        public Notification ServiceNotification { get; set; }
        [DataMember]
        public LabelDelivery ServiceLabelDelivery { get; set; }
        [DataMember]
        public InternationalForms ServiceInternationalForms { get; set; }
        [DataMember]
        public DeliveryConfirmation DeliveryConfirmation { get; set; }
        [DataMember]
        public bool ReturnOfDocumentIndicator { get; set; }
        [DataMember]
        public bool ImportControlIndicator { get; set; }
        [DataMember]
        public string LabelMethodCode { get; set; }
        [DataMember]
        public string LabelMethodDescription { get; set; }
        [DataMember]
        public bool CommercialInvoiceRemovalIndicator { get; set; }
        [DataMember]
        public bool UPScarbonneutralIndicator { get; set; }
        [DataMember]
        public PreAlertNotification PreAlert { get; set; }
        [DataMember]
        public bool ExchangeForwardIndicator { get; set; }
        [DataMember]
        public bool HoldForPickupIndicator { get; set; }
        [DataMember]
        public bool DropoffAtUPSFacilityIndicator { get; set; }
        [DataMember]
        public bool LiftGateForPickUpIndicator { get; set; }
        [DataMember]
        public bool LiftGateForDeliveryIndicator { get; set; }
    }


    [DataContract]
    public class COD
    {
        [DataMember]
        public string CODFundsCode { get; set; }
        [DataMember]
        public string CODAmountCurrencyCode { get; set; }
        [DataMember]
        public string CODAmountMonetaryValue { get; set; }
    }

    [DataContract]
    public class Notification
    {
        [DataMember]
        public string NotificationCode { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string UndeliverableEmailAddress { get; set; }
        [DataMember]
        public string FromEmailAddress { get; set; }
        [DataMember]
        public string FromName { get; set; }
        [DataMember]
        public string EmailMemo { get; set; }
        [DataMember]
        public string EmaiSubject { get; set; }
        [DataMember]
        public string EmaiSubjectCode { get; set; }
    }

    [DataContract]
    public class LabelDelivery
    {
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string UndeliverableEmailAddress { get; set; }
        [DataMember]
        public string FromEmailAddress { get; set; }
        [DataMember]
        public string FromName { get; set; }
        [DataMember]
        public string EmailMemo { get; set; }
        [DataMember]
        public string EmaiSubject { get; set; }
        [DataMember]
        public string EmaiSubjectCode { get; set; }
        [DataMember]
        public bool LabelLinksIndicator { get; set; }

    }

    [DataContract]
    public class InternationalForms
    {
        [DataMember]
        public string FormType { get; set; }
        [DataMember]
        public CN22Form Cn22 { get; set; }
        [DataMember]
        public string[] UserCreatedFormDocumentIDs { get; set; }
        [DataMember]
        public bool AdditionalDocumentIndicator { get; set; }
        [DataMember]
        public string FormGroupIdName { get; set; }
        [DataMember]
        public string SEDFilingOption { get; set; }
        [DataMember]
        public FormsContacts Contact { get; set; }
        [DataMember]
        public Product ProductContainer { get; set; }
        [DataMember]
        public string InvoiceNumber { get; set; }

        private DateTime _invoiceDate;
        [DataMember]
        public string InvoiceDate
        {
            get { return _invoiceDate.ToString("yyyyMMdd"); }
            set { _invoiceDate = DateTime.Parse(value); }
        }

        [DataMember]
        public string PurchaseOrderNumber { get; set; }
        [DataMember]
        public TermsOfShipment TermsOfShip { get; set; }
        [DataMember]
        public ReasonForExport ExportReason { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string DeclarationStatement { get; set; }
        [DataMember]
        public double DiscountValue { get; set; }
        [DataMember]
        public double FreightCharges { get; set; }
        [DataMember]
        public double InsuranceCharges { get; set; }
        [DataMember]
        public double OtherCharges { get; set; }
        [DataMember]
        public string OtherChargesDescription { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }

        private DateTime _blanketPeriodBeginDate;
        [DataMember]
        public string BlanketPeriodBeginDate
        {
            get { return _blanketPeriodBeginDate.ToString("yyyyMMdd"); }
            set { _blanketPeriodBeginDate = DateTime.Parse(value); }
        }

        private DateTime _blanketPeriodEndDate;
        [DataMember]
        public string BlanketPeriodEndDate
        {
            get { return _blanketPeriodEndDate.ToString("yyyyMMdd"); }
            set { _blanketPeriodEndDate = DateTime.Parse(value); }
        }

        private DateTime _exportDate;
        [DataMember]
        public string ExportDate
        {
            get { return _exportDate.ToString("yyyyMMdd"); }
            set { _exportDate = DateTime.Parse(value); }
        }
        [DataMember]
        public string ExportingCarrierName { get; set; }
        [DataMember]
        public string CarrierID { get; set; }
        [DataMember]
        public string InBondCode { get; set; }
        [DataMember]
        public string EntryNumber { get; set; }
        [DataMember]
        public string PointOfOrigin { get; set; }
        [DataMember]
        public ModeOfTransport MOT { get; set; }
        [DataMember]
        public string PortOfExport { get; set; }
        [DataMember]
        public string PortOfUnloading { get; set; }
        [DataMember]
        public string LoadingPier { get; set; }
        [DataMember]
        public string PartiesToTransaction { get; set; }
        [DataMember]
        public bool RoutedExportTransactionIndicator { get; set; }
        [DataMember]
        public bool ContainerizedIndicator { get; set; }
        [DataMember]
        public string LicenseNumber { get; set; }

        private DateTime _licenseDate;
        [DataMember]
        public string LicenseDate
        {
            get { return _licenseDate.ToString("yyyyMMdd"); }
            set { _licenseDate = DateTime.Parse(value); }
        }
        [DataMember]
        public string LicenseExceptionCode { get; set; }
        [DataMember]
        public string ECCNNumber { get; set; }
        [DataMember]
        public string OverridePaperlessIndicator { get; set; }
        [DataMember]
        public string ShipperMemo { get; set; }
    }

    [DataContract]
    public class CN22Form
    {
        [DataMember]
        public string LabelSize { get; set; }
        [DataMember]
        public int PrintsPerPage { get; set; }
        [DataMember]
        public string LabelPrintType { get; set; }
        [DataMember]
        public string CN22Type { get; set; }
        [DataMember]
        public string CN22OtherDescription { get; set; }
        [DataMember]
        public string FoldHereText { get; set; }
        [DataMember]
        public List<CN22Content> CN22Container { get; set; }
    }

    [DataContract]
    public class CN22Content
    {
        [DataMember]
        public int CN22ContentQuantity { get; set; }
        [DataMember]
        public string CN22ContentDescription { get; set; }
        [DataMember]
        public ShipUOM CN22ContentUOM { get; set; }
        [DataMember]
        public double CN22ContentWeight { get; set; }
        [DataMember]
        public double CN22ContentTotalValue { get; set; }
        [DataMember]
        public string CN22ContentCurrencyCode { get; set; }
        [DataMember]
        public string CN22ContentCountryOfOrigin { get; set; }
        [DataMember]
        public string CN22ContentTariffNumber { get; set; }
    }

    [DataContract]
    public class UPSPremiumCareForm
    {
        private DateTime _shipmentDate;
        [DataMember]
        public string ShipmentDate
        {
            get { return _shipmentDate.ToString("yyyyMMdd"); }
            set { _shipmentDate = DateTime.Parse(value); }
        }
        [DataMember]
        public string PageSize { get; set; }
        [DataMember]
        public string PrintType { get; set; }
        [DataMember]
        public string NumOfCopies { get; set; }
        [DataMember]
        public UPSPremiumCareFormLanguage Language { get; set; }

    }
    [DataContract]
    public enum UPSPremiumCareFormLanguage
    {
        eng,
        fra
    }

    [DataContract]
    public class FormsContacts
    {
        [DataMember]
        public Partner ForwardAgent { get; set; }
        [DataMember]
        public Partner UltimateConsignee { get; set; }
        [DataMember]
        public Partner IntermediateConsignee { get; set; }
        [DataMember]
        public Partner Producer { get; set; }
        [DataMember]
        public Partner SoldTo { get; set; }
    }

    [DataContract]
    public class Product
    {
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public CommodityUnit Unit { get; set; }
        [DataMember]
        public string CommodityCode { get; set; }
        [DataMember]
        public string PartNumber { get; set; }
        [DataMember]
        public string OriginCountryCode { get; set; }
        [DataMember]
        public bool JointProductionIndicator { get; set; }
        [DataMember]
        public string NetCostCode { get; set; }
        private DateTime _netCostRangeBeginDate;
        [DataMember]
        public string NetCostRangeBeginDate
        {
            get { return _netCostRangeBeginDate.ToString("yyyyMMdd"); }
            set { _netCostRangeBeginDate = DateTime.Parse(value); }
        }

        private DateTime _netCostRangeEndDate;
        [DataMember]
        public string NetCostRangeEndDate
        {
            get { return _netCostRangeEndDate.ToString("yyyyMMdd"); }
            set { _netCostRangeEndDate = DateTime.Parse(value); }
        }
        [DataMember]
        public string PreferenceCriteria { get; set; }
        [DataMember]
        public string ProducerInfo { get; set; }
        [DataMember]
        public string MarksAndNumbers { get; set; }
        [DataMember]
        public int NumberOfPackagesPerCommodity { get; set; }
        [DataMember]
        public double ProductWeight { get; set; }
        [DataMember]
        public ShipUOM ProductWeightUOM { get; set; }
        [DataMember]
        public string VehicleID { get; set; }
        [DataMember]
        public string ScheduleBNumber { get; set; }
        [DataMember]
        public int ScheduleBQuantity { get; set; }
        [DataMember]
        public string ScheduleBUOMCode { get; set; }
        [DataMember]
        public string ScheduleBDescription { get; set; }
        [DataMember]
        public string ExportType { get; set; }
        [DataMember]
        public string SEDTotalValue { get; set; }
        [DataMember]
        public string[] ExcludeFromFormTypes { get; set; }
        [DataMember]
        public PackingList[] PackingListInfo { get; set; }
    }

    [DataContract]
    public class CommodityUnit
    {
        [DataMember]
        public int Number { get; set; }
        [DataMember]
        public ShipUOM CommodityUnitUOM { get; set; }
        [DataMember]
        public double Value { get; set; }
    }

    [DataContract]
    public class PackingList
    {
        [DataMember]
        public string PackageNumber { get; set; }
        [DataMember]
        public string Amount { get; set; }
    }
    [DataContract]
    public enum TermsOfShipment
    {
        CFR, //: Cost and Freight, 
        CIF, //: Cost,Insurance and Freight,
        CIP, //: Carriage and Insurance Paid, 
        CPT, //: Carriage Paid To, 
        DAF, //:Delivered at Frontier,
        DDP, //: Delivery Duty Paid, 
        DDU, //: Delivery Duty Unpaid, 
        DEQ, //: Delivered Ex Quay,
        DES, //: Delivered Ex Ship,
        EXW, //: Ex Works,
        FAS, //: Free Alongside Ship,
        FCA, //: Free Carrier, 
        FOB //: Free On Board.
    }
    [DataContract]
    public enum ReasonForExport
    {
        SALE,
        GIFT,
        SAMPLE,
        RETURN,
        REPAIR,
        INTERCOMPANYDATA,
        OTHER
    }
    [DataContract]
    public enum ModeOfTransport
    {
        Air,
        AirContainerized,
        Auto,
        FixedTransportInstallations,
        Mail,
        PassengerHandcarried,
        Pedestrian,
        Rail,
        RailContainerized,
        RoadOther,
        SeaBarge,
        SeaContainerized,
        SeaNoncontainerized,
        Truck,
        TruckContainerized
    }

    [DataContract]
    public class DeliveryConfirmation
    {
        [DataMember]
        public DeliveryConfirmationType Type { get; set; }
        [DataMember]
        public string Number { get; set; }
    }

    [DataContract]
    public class PackageDeliveryConfirmation
    {
        [DataMember]
        public PackageDeliveryConfirmationType Type { get; set; }
        [DataMember]
        public string Number { get; set; }
    }
    [DataContract]
    public enum DeliveryConfirmationType
    {
        SignatureRequired = 1,
        AdultSignatureRequired = 2
    }
    [DataContract]
    public enum PackageDeliveryConfirmationType
    {
        DeliveryConfirmation = 1,
        SignatureRequired = 2,
        AdultSignatureRequired = 3,
        USPSDeliveryConfirmation = 4
    }

    [DataContract]
    public class PreAlertNotification
    {
        [DataMember]
        public string EmailAddress { get; set; }
        [DataMember]
        public string UndeliverableEmailAddress { get; set; }
        [DataMember]
        public string VoiceMessagePhoneNumber { get; set; }
        [DataMember]
        public string TextMessagePhoneNumber { get; set; }
        [DataMember]
        public string LocaleLanguage { get; set; }
        [DataMember]
        public string LocaleDialect { get; set; }

    }

    [DataContract]
    public class ShipPackage
    {
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string PackagingCode { get; set; }
        [DataMember]
        public string PackagingDescription { get; set; }
        [DataMember]
        public Dimensions PackageDims { get; set; }
        [DataMember]
        public ShipUOM WeightUOM { get; set; }
        [DataMember]
        public double Weight { get; set; }
        [DataMember]
        public bool LargePackageIndicator { get; set; }
        [DataMember]
        public ReferenceNumber RefNumber { get; set; }
        [DataMember]
        public bool AdditionalHandlingIndicator { get; set; }
        [DataMember]
        public PackageServiceOptions ServiceOptions { get; set; }
        [DataMember]
        public PackageCommodity Commodity { get; set; }
    }

    [DataContract]
    public class Dimensions
    {
        [DataMember]
        public ShipUOM DimensionUOM { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
    }

    [DataContract]
    public class ShipUOM
    {
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public class PackageServiceOptions
    {
        [DataMember]
        public PackageDeliveryConfirmation DeliveryConfirmation { get; set; }
        [DataMember]
        public DeclaredValue PackageDeclaredValue { get; set; }
        [DataMember]
        public COD CODContainer { get; set; }
        [DataMember]
        public ContactInfo VerbalConfirmationContact { get; set; }
        [DataMember]
        public bool ShipperReleaseIndicator { get; set; }
        [DataMember]
        public Notification PackNotification { get; set; }
        [DataMember]
        public DryIceContainer DryIce { get; set; }

    }

    [DataContract]
    public class DeclaredValue
    {
        [DataMember]
        public string TypeCode { get; set; }
        [DataMember]
        public string TypeDescription { get; set; }
        [DataMember]
        public string CurrencyCode { get; set; }
        [DataMember]
        public string MonetaryValue { get; set; }
    }

    [DataContract]
    public class ContactInfo
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PhoneNumber { get; set; }
        [DataMember]
        public string PhoneExtention { get; set; }
    }

    [DataContract]
    public class DryIceContainer
    {
        [DataMember]
        public DryIceRegulations DryIceRegSet { get; set; }
        [DataMember]
        public ShipUOM DryIceWeightUOM { get; set; }
        [DataMember]
        public double DryIceWeight { get; set; }
        [DataMember]
        public bool MedicalUseIndicator { get; set; }
        [DataMember]
        public bool UPSPremiumCareIndicator { get; set; }

    }
    [DataContract]
    public enum DryIceRegulations
    {
        CFR, // HazMat regulated by US Dept of Transportation within the U.S. or ground shipments to Canada, 
        IATA //Worldwide Air movement
    }

    [DataContract]
    public class PackageCommodity
    {
        [DataMember]
        public string FreightClass { get; set; }
        [DataMember]
        public string NMFCPrimeCode { get; set; }
        [DataMember]
        public string NMFCSubCode { get; set; }
    }

    [DataContract]
    public class Label
    {
        [DataMember]
        public LabelFormat Format { get; set; }
        [DataMember]
        public string FormatDescription { get; set; }
        [DataMember]
        public string HTTPUserAgent { get; set; }
        [DataMember]
        public int LabelStockSizeHeight { get; set; }
        [DataMember]
        public int LabelStockSizeWidth { get; set; }
        [DataMember]
        public string LabelInstructionCode { get; set; }
        [DataMember]
        public string LabelInstructionDescription { get; set; }
    }

    [DataContract]
    public enum LabelFormat
    {
        GIF,
        ZPL,
        EPL,
        SPL,
        STARPL,
    }

    [DataContract]
    public class Receipt
    {
        [DataMember]
        public string ImageFormatCode { get; set; }
        [DataMember]
        public string ImageFormatDescription { get; set; }
    }

    internal static class Helper
    {
        public static string SafeSubstring(this string str, int start, int length = -1)
        {
            if (string.IsNullOrEmpty(str) || start > str.Length - 1 || start < 0 || length < -1)
                return "";
            else if (length == -1 || length + start > str.Length)
                return str.Substring(start);
            else
                return str.Substring(start, length);
        }
    }

    [DataContract]
    public class CreatedShipment
    {
        [DataMember]
        public string ShipmentIdentificationNumber { get; set; }
        [DataMember]
        public string LabelURL { get; set; }
        [DataMember]
        public List<string> TrackingNumbers { get; set; }
        [DataMember]
        public List<string> PackageShippingLabels { get; set; }
    }
}


