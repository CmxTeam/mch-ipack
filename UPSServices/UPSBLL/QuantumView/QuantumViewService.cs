﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ShipmentEntities;
using UPSBLL.QuantumView;
using UPSBLL.Utilities;
using ShippingInterface;
using UPSBLL.DDL;

namespace UPSBLL
{
    public class UPSQuantumViewService : IQuantumViewService
    {
        public List<QVActivity> GetQVData(string qvURL, Credentials creds)
        {
            var resultList = new List<QuantumViewResponse>();
            var qvRequest = new QuantumViewRequest();
            var requestBody = new Request { RequestAction = UPSConstants.QuantumViewRequestAction };
            qvRequest.Request = requestBody;

            string bookmark = "";

            do
            {
                qvRequest.Bookmark = bookmark;
                QuantumViewResponse response = SendQuantumViewRequest(qvRequest, qvURL, creds);

                if (response.Response.ResponseStatusCode == "0")
                {
                    if (response.Response.Error != null && response.Response.Error.Length > 0)
                    {
                        throw new ShippingException(ShippingException.GetSeverity(response.Response.Error[0].ErrorSeverity),
                            response.Response.Error[0].ErrorCode, response.Response.Error[0].ErrorDescription, "GetQVData");
                    }
                    else
                    {
                        throw new ShippingException();
                    }
                }

                bookmark = response.Bookmark;
                resultList.Add(response);
            } while (!string.IsNullOrEmpty(bookmark));

            var retVal = new List<QVActivity>();
            foreach (var resultSet in resultList)
            {
                foreach (var subscriptionEvent in resultSet.QuantumViewEvents.SubscriptionEvents)
                {
                    foreach (var subscriptionFile in subscriptionEvent.SubscriptionFile)
                    {
                        foreach (var item in subscriptionFile.Items)
                        {
                            if (item is ManifestType)
                            {
                                //ManifestType man = item as ManifestType;
                                ////TODO: Process manifest.
                                ////IGNORE (FOR NOW)
                                continue;
                            }
                            else if (item is OriginType)
                            {
                                OriginType org = item as OriginType;
                                //TODO: Process origin.
                                var activity = new QVActivity()
                                               {
                                                   MessageType = QVType.Origin,
                                                   TrackingNumber = org.TrackingNumber,
                                                   ActivityTime = TrackActivity.GetActivityTime(org.Date, org.Time),
                                                   ActivityDescription = "Origin",
                                                   ActivityLocation = ((org.ActivityLocation.AddressArtifactFormat.PoliticalDivision2 ?? "") + ", " + (org.ActivityLocation.AddressArtifactFormat.PoliticalDivision1 ?? "") + ", " + (org.ActivityLocation.AddressArtifactFormat.CountryCode ?? "")).Trim(new[] { ' ', ',' })
                                               };
                                retVal.Add(activity);

                            }
                            else if (item is ExceptionType)
                            {
                                ExceptionType exception = item as ExceptionType;
                                //TODO: Process Exception.

                                var activity = new QVActivity()
                                {
                                    TrackingNumber = exception.TrackingNumber,
                                    MessageType = QVType.Exception,
                                    ActivityTime = TrackActivity.GetActivityTime(exception.Date, exception.Time),
                                    ActivityCode = exception.StatusCode,
                                    ActivityDescription = exception.StatusDescription + (string.IsNullOrEmpty(exception.RescheduledDeliveryDate) ? "" : " " + "Rescheduled Delivery: " + exception.RescheduledDeliveryDate),
                                    ActivityLocation = ((exception.ActivityLocation.AddressArtifactFormat.PoliticalDivision2 ?? "") + ", " + (exception.ActivityLocation.AddressArtifactFormat.PoliticalDivision1 ?? "") + ", " + (exception.ActivityLocation.AddressArtifactFormat.CountryCode ?? "")).Trim(new[] { ' ', ',' })

                                };
                                retVal.Add(activity);
                            }
                            else if (item is DeliveryType)
                            {
                                DeliveryType delivery = item as DeliveryType;
                                //TODO: Process delivery.

                                var activity = new QVActivity()
                                {
                                    TrackingNumber = delivery.TrackingNumber,
                                    MessageType = QVType.Delivery,
                                    ActivityTime = TrackActivity.GetActivityTime(delivery.Date, delivery.Time),
                                    ActivityDescription = (delivery.DeliveryLocation.SignedForByName ?? (string.IsNullOrEmpty(delivery.DeliveryLocation.Description) ? "" : "LEFT AT: " + delivery.DeliveryLocation.Description)),
                                    ActivityLocation = ((delivery.ActivityLocation.AddressArtifactFormat.PoliticalDivision2 ?? "") + ", " + (delivery.ActivityLocation.AddressArtifactFormat.PoliticalDivision1 ?? "") + ", " + (delivery.ActivityLocation.AddressArtifactFormat.CountryCode ?? "")).Trim(new[] { ' ', ',' })
                                };
                                retVal.Add(activity);
                            }
                            else if (item is GenericType)
                            {
                                GenericType generic = item as GenericType;
                                //TODO: Process generic.
                                var activity = new QVActivity()
                                {
                                    TrackingNumber = generic.TrackingNumber,
                                    MessageType = QVType.Generic,
                                    ActivityTime = TrackActivity.GetActivityTime(generic.Activity.Date, generic.Activity.Time),
                                    ActivityCode = generic.ActivityType,
                                };
                                retVal.Add(activity);
                            }
                        }
                    }
                }
            }

            return retVal;
        }

        private QuantumViewResponse SendQuantumViewRequest(QuantumViewRequest qvRequest, string qvURL, Credentials creds)
        {
            string requestString = "";
            AccessRequest access = new AccessRequest()
            {
                AccessLicenseNumber = creds.AccessKey, //ConfigurationManager.AppSettings["UPSAccessKey"],
                UserId = creds.Username,//ConfigurationManager.AppSettings["UPSUserName"],
                Password = creds.Password
            };
            XmlSerializer serializer = new XmlSerializer(access.GetType());
            StringWriter writer = new StringWriter();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            serializer.Serialize(writer, access, ns);

            requestString = writer.ToString() + Environment.NewLine;

            serializer = new XmlSerializer(qvRequest.GetType());
            writer = new StringWriter();
            serializer.Serialize(writer, qvRequest, ns);
            requestString += writer.ToString();
            string responceStr = "";
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(qvURL);

                byte[] requestBytes = Encoding.ASCII.GetBytes(requestString);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml;charset=utf-8";
                webRequest.ContentLength = requestBytes.Length;
                Stream requestStream = webRequest.GetRequestStream();
                requestStream.Write(requestBytes, 0, requestBytes.Length);
                requestStream.Close();

                HttpWebResponse res = (HttpWebResponse)webRequest.GetResponse();
                StreamReader sr = new StreamReader(res.GetResponseStream(), System.Text.Encoding.Default);
                responceStr = sr.ReadToEnd();

            }
            catch (Exception ex)
            {
                //TODO: Process Exception.
                throw ex;
            }

            var response = new QuantumViewResponse();
            serializer = new XmlSerializer(response.GetType());
            response = (QuantumViewResponse)serializer.Deserialize(new StringReader(responceStr));

            //Save QV response to text file.
            if (response.Response.ResponseStatusCode != "0")
            {
                try
                {
                    if (ConfigurationManager.AppSettings["SaveQVResponse"].ToUpper() == "TRUE")
                    {

                        if (!Directory.Exists(ConfigurationManager.AppSettings["TempDirectory"]))
                            Directory.CreateDirectory(ConfigurationManager.AppSettings["TempDirectory"]);

                        string filePath = Path.Combine(ConfigurationManager.AppSettings["TempDirectory"],
                            "QV_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");
                        using (var fs = new FileStream(filePath, FileMode.Create))
                        {
                            using (var sw = new StreamWriter(fs))
                            {
                                sw.Write(responceStr);
                                sw.Close();
                                fs.Close();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                }
            }

            return response;

        }

    }
}
