﻿using System.Runtime.Serialization;

namespace UPSBLL.DDL
{
    [DataContract]
    public class CompanyAddress
    {
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string AttentionName { get; set; }
        [DataMember]
        public string Phone { get; set; }
    }
}