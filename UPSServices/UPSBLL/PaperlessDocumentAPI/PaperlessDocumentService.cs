﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using UPSBLL.UPSPaperlessDocumentService;


namespace UPSBLL.PaperlessDocumentAPI
{
    /// <summary>
    /// Provides inteface to UPS Paperless Document API service.
    /// </summary>
     public class PaperlessDocumentService
    {
        /// <summary>
        /// Send delete request.
        /// </summary>
        /// <param name="shipperNumber">Shipper number.</param>
        /// <param name="documentId">Document Id.</param>
        public static DeleteResponse ProcessDelete(string shipperNumber, string documentId)
        {
            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;


            PaperlessDocumentAPIService docAPIService = new PaperlessDocumentAPIService();
            docAPIService.Url = ConfigurationManager.AppSettings["PaperlessDocumentServiceURL"];

            DeleteRequest delRequest = new DeleteRequest();

            //Create Request
            RequestType request = new RequestType();
            String[] requestOption = { " " };
            request.RequestOption = requestOption;

            // Create TransactionRef
            TransactionReferenceType tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;

            delRequest.Request = request;

            delRequest.ShipperNumber = shipperNumber;
            delRequest.DocumentID = documentId;

            SSLValidator.OverrideValidation();

            DeleteResponse delResponse;
            try
            {
                delResponse = docAPIService.ProcessDeleting(delRequest);
            }
            catch (SoapException soapEx)
            {
                //TODO: Process UPS Exception.
                string exString = soapEx.Detail.InnerText;
                throw;
            }
            catch (Exception ex)
            {
                //TODO: Process general exception.
                throw;
            }
            return delResponse;
        }

        /// <summary>
        /// Push to image repository.
        /// </summary>
        /// <param name="shipperNumber">Shipper Number</param>
        /// <param name="shipmentId">Shipment Id</param>
        /// <param name="shipmentType">Shipment Type. 1- Package, 2- freight.</param>
        /// <param name="shipmentDate">Shipment date time. Needed if Shipment type = 1.</param>
        /// <param name="shipmentReferenceNumber">For shipment type = 1 - tracking number. For shipment type = 2 - pickup confirmation number.</param>
        /// <param name="docIds">Document Id</param>
        /// <param name="formsGroupId">Forms Group ID. Set the value to Forms Group ID if it's needed to update existing Forms Group ID with new Document ID and push it to Image Repository.</param>
        public static PushToImageRepositoryResponse ProcessPushToRepository(string shipperNumber, string shipmentId, string[] docIds, Utilities.ShipmentType shipmentType, DateTime shipmentDate, string shipmentReferenceNumber, string formsGroupId = "")
        {
            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;

            PaperlessDocumentAPIService docAPIService = new PaperlessDocumentAPIService();
            docAPIService.Url = ConfigurationManager.AppSettings["PaperlessDocumentServiceURL"];
            docAPIService.UPSSecurityValue = upss;

            PushToImageRepositoryRequest pushRequest = new PushToImageRepositoryRequest();


            // Create Request
            RequestType request = new RequestType();
            String[] requestOption = { " " };
            request.RequestOption = requestOption;

            // Create TransactionRef
            TransactionReferenceType tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;
            pushRequest.Request = request;

            pushRequest.ShipperNumber = shipperNumber;
            pushRequest.ShipmentIdentifier = shipmentId;
            pushRequest.ShipmentType = ((int)shipmentType).ToString(CultureInfo.InvariantCulture); // For small package shipment, set this field to value "1". For freight shipment, set this field to value "2".
            if (shipmentType == Utilities.ShipmentType.Package)
            {
                pushRequest.ShipmentDateAndTime = shipmentDate.ToString("yyyy-MM-dd-HH.mm.ss");
                List<String> trackNum = new List<String>();
                trackNum.Add(shipmentReferenceNumber);
                String[] trackingNumber = trackNum.ToArray();
                pushRequest.TrackingNumber = trackingNumber;
            }
            else if (shipmentType == Utilities.ShipmentType.Freight)
            {
                pushRequest.PRQConfirmationNumber = shipmentReferenceNumber;
            }

            if (!String.IsNullOrEmpty(formsGroupId))
            {
                pushRequest.FormsGroupID = formsGroupId; // Set the value to Forms Group ID if one needs to update existing Forms Group ID with new Document ID and push it to Image Repository.
            }

            pushRequest.FormsHistoryDocumentID = docIds;

            SSLValidator.OverrideValidation();
            PushToImageRepositoryResponse pushResponse;
            try
            {
                pushResponse = docAPIService.ProcessPushToImageRepository(pushRequest);
            }
            catch (SoapException soapEx)
            {
                //TODO: Process UPS Exception.
                string exString = soapEx.Detail.InnerText;
                throw;
            }
            catch (Exception ex)
            {
                //TODO: Process general exception.
                throw;
            }
            return pushResponse;
        }


        /// <summary>
        ///  Send upload request.
        /// </summary>
        /// <param name="shipperNumber">Shipper Number</param>
        /// <param name="docs">List of documents to upload.</param>
        /// <returns>Upload response.</returns>
        public static UploadResponse ProcessUpload(string shipperNumber, List<Document> docs)
        {

            if (docs.Count > 13)
            {
                throw new ArgumentException("Max allowed document count = 13.", "docs");
            }
            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;

            PaperlessDocumentAPIService docAPIService = new PaperlessDocumentAPIService();
            docAPIService.Url = ConfigurationManager.AppSettings["PaperlessDocumentServiceURL"];
            docAPIService.UPSSecurityValue = upss;

            UploadRequest uploadReq = new UploadRequest();

            // Create Request
            RequestType request = new RequestType();
            String[] requestOption = { " " };
            request.RequestOption = requestOption;

            // Create TransactionRef
            TransactionReferenceType tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;
            uploadReq.Request = request;

            uploadReq.ShipperNumber = shipperNumber;

            List<UserCreatedForm> ugf = new List<UserCreatedForm>();
            foreach (Document doc in docs)
            {
                UserCreatedForm ucf = new UserCreatedForm();
                ucf.UserCreatedFormFileName = Path.GetFileNameWithoutExtension(doc.FileName);
                string extension = Path.GetExtension(doc.FileName);
                if (extension != null)
                    ucf.UserCreatedFormFileFormat = extension.TrimStart(new char[] { '.' });

                List<String> docType = new List<String>();
                docType.Add(doc.FileType);
                String[] arrDocumentType = docType.ToArray();
                ucf.UserCreatedFormDocumentType = arrDocumentType;

                byte[] file = File.ReadAllBytes(doc.FileName);

                if (file.Length > 1024)
                {
                    throw new ArgumentException("File '" + doc.FileName + "' has size " + file.Length +
                                                " bytes, which exceeds max allowed size 1 MB", "docs");
                }

                ucf.UserCreatedFormFile = file;
                ugf.Add(ucf);
            }
           
            UserCreatedForm[] userCreatedForm = ugf.ToArray();
            uploadReq.UserCreatedForm = userCreatedForm;
            SSLValidator.OverrideValidation();

            UploadResponse uploadResponse;
            try
            {
                uploadResponse = docAPIService.ProcessUploading(uploadReq);
            }
            catch (SoapException soapEx)
            {
                //TODO: Process UPS Exception.
                string exString = soapEx.Detail.InnerText;
                throw;
            }
            catch (Exception ex)
            {
                //TODO: Process general exception.
                throw;
            }

            return uploadResponse;
        }
    }

    public struct Document
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
    }
}
