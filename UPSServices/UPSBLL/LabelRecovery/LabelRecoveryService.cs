﻿using System;
using System.Configuration;
using System.Web.Services.Protocols;
using UPSBLL.UPSLabelRecoveryService;

namespace UPSBLL.LabelRecovery
{
    /// <summary>
    /// Provides interface with UPS Label Recovery service.
    /// </summary>
    public static class LabelRecoveryService
    {
        /// <summary>
        /// Recovers label from UPS.
        /// </summary>
        /// <param name="trackingNumber">Tracking Number.</param>
        /// <param name="format">Label format.</param>
        /// <param name="httpUserAgent">User agent. Required if Format = GIF. Default value Mozilla/4.5</param>
        public static LabelRecoveryResponse ProcessLabelRecovery(string trackingNumber, bool isLink, Utilities.LabelFormat format, string httpUserAgent = @"Mozilla/4.5", string email = "")
        {
            LBRecovery lbSvc = new LBRecovery();
            lbSvc.Url = ConfigurationManager.AppSettings["LableRecoveryServiceURL"];

            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;
            lbSvc.UPSSecurityValue = upss;

            //Prepare reuest.
            LabelRecoveryRequest lrRequest = new LabelRecoveryRequest();
            RequestType request = new RequestType();
            lrRequest.Request = request;
            lrRequest.TrackingNumber = trackingNumber;

            lrRequest.LabelSpecification = new LabelSpecificationType()
                                            {
                                                LabelImageFormat =
                                                    new LabelImageFormatType() { Code = format.ToString() },
                                                HTTPUserAgent = format.ToString() == "GIF" ? httpUserAgent : ""
                                            };

            lrRequest.LabelDelivery = new LabelDeliveryType();
            if (!string.IsNullOrEmpty(email))
            {
                lrRequest.LabelDelivery.ResendEMailIndicator = "";
                lrRequest.LabelDelivery.EMailMessage = new EMailMessageType() {EMailAddress = email};
            }
            if (isLink)
            {
                lrRequest.LabelDelivery.LabelLinkIndicator = "";
            }

            SSLValidator.OverrideValidation();
            LabelRecoveryResponse rResponse;

            //Send Request
            try
            {
                rResponse = lbSvc.ProcessLabelRecovery(lrRequest);
            }
            catch (SoapException soapEx)
            {
                //TODO: Process UPS Exception.
                string exString = soapEx.Detail.InnerText;
                throw;
            }
            catch (Exception ex)
            {
                //TODO: Process general exception.
                throw;
            }

            return rResponse;
        }

        public static LabelRecoveryResponse ProcessLabelRecovery(string referenceNumber, string shipperNumber,bool isLink, Utilities.LabelFormat format, string httpUserAgent = @"Mozilla/4.5", string email = "")
        {
            LBRecovery lbSvc = new LBRecovery();
            lbSvc.Url = ConfigurationManager.AppSettings["LableRecoveryServiceURL"];

            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;
            lbSvc.UPSSecurityValue = upss;

            //Prepare reuest.
            LabelRecoveryRequest lrRequest = new LabelRecoveryRequest();
            RequestType request = new RequestType();
            lrRequest.Request = request;
            lrRequest.ReferenceValues = new ReferenceValuesType()
                                            {
                                                ReferenceNumber = new UPSBLL.UPSLabelRecoveryService.ReferenceNumberType() { Value = referenceNumber },
                                                ShipperNumber = shipperNumber
                                            };
            lrRequest.LabelSpecification = new LabelSpecificationType()
                                               {
                                                   LabelImageFormat =
                                                       new LabelImageFormatType() { Code = format.ToString() },
                                                   HTTPUserAgent = format.ToString() == "GIF" ? httpUserAgent : ""
                                               };

            lrRequest.LabelDelivery = new LabelDeliveryType();
            if (!string.IsNullOrEmpty(email))
            {
                lrRequest.LabelDelivery.ResendEMailIndicator = "";
                lrRequest.LabelDelivery.EMailMessage = new EMailMessageType() { EMailAddress = email };
            }
            if (isLink)
            {
                lrRequest.LabelDelivery.LabelLinkIndicator = "";
            }

            SSLValidator.OverrideValidation();
            LabelRecoveryResponse rResponse;

            //Send Request
            try
            {
                rResponse = lbSvc.ProcessLabelRecovery(lrRequest);

            }
            catch (SoapException soapEx)
            {
                //TODO: Process UPS Exception.
                string exString = soapEx.Detail.InnerText;
                throw;
            }
            catch (Exception ex)
            {
                //TODO: Process general exception.
                throw;
            }

            return rResponse;
        }

    }
}
