﻿using System;
using System.Configuration;
using System.Web.Services.Protocols;
using UPSBLL.DDL;
using UPSBLL.UPSAddressValidationService;
using UPSBLL.Utilities;


namespace UPSBLL.AddressValidation
{
    public class AddressValidationService
    {
        public static XAVResponse ValidateAddress(CompanyAddress address, ValidationType validation, int maximumCandidateListSize = 15, bool regionalRequestIndicator = false)
        {

            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;

            var xavSvc = new XAVService();
            xavSvc.Url = ConfigurationManager.AppSettings["UPSURL"];
            xavSvc.UPSSecurityValue = upss;

            XAVRequest xavRequest = new XAVRequest();

            RequestType request = new RequestType();
            String[] requestOption = { ((int)validation).ToString() };
            request.RequestOption = requestOption;
            // Create TransactionRef
            TransactionReferenceType tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;
            xavRequest.Request = request;
            xavRequest.MaximumCandidateListSize = maximumCandidateListSize.ToString();

            if (regionalRequestIndicator)
                xavRequest.RegionalRequestIndicator = "";

            
            AddressKeyFormatType addressKeyFormat = new AddressKeyFormatType();
            String[] addressLine = { address.Address };
            addressKeyFormat.AddressLine = addressLine;
            addressKeyFormat.PoliticalDivision1 = address.City;
            addressKeyFormat.PoliticalDivision2 = address.State;
            addressKeyFormat.ConsigneeName = address.CompanyName;
            addressKeyFormat.CountryCode = address.Country;
            xavRequest.AddressKeyFormat = addressKeyFormat;
            xavRequest.MaximumCandidateListSize = maximumCandidateListSize.ToString();
            SSLValidator.OverrideValidation();

            XAVResponse xavResponse = new XAVResponse();
            try
            {
                xavResponse = xavSvc.ProcessXAV(xavRequest);
            }
            catch (SoapException soapEx)
            {
                //TODO: Process UPS Exception.
                string exString = soapEx.Detail.InnerText;
                throw;
            }
            catch (Exception ex)
            {
                //TODO: Process general exception.
                throw;
            }

            return xavResponse;
        }
    }

    public enum ValidationType
    {
        Validation = 1,
        Classification = 2,
        ValidationAndClassification = 3
    }
}
