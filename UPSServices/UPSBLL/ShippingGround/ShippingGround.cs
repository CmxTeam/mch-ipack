﻿using System;
using System.Configuration;
using System.Web.Services.Protocols;
using UPSBLL.UPSFreightShippingService;

namespace UPSBLL.ShippingGround
{
    public class ShippingGround
    {
        public FreightShipResponse SendShipment(Utilities.ShippingType shipType, ShipmentType shipment)
        {
            FreightShipService freightShipService = new FreightShipService();
            freightShipService.Url = ConfigurationManager.AppSettings["ShipGroundServiceURL"];


            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;
            freightShipService.UPSSecurityValue = upss;

            FreightShipRequest freightShipRequest = new FreightShipRequest();
            RequestType request = new RequestType();
            String[] requestOption = { ((int)shipType).ToString() };

            request.RequestOption = requestOption;
            // Create TransactionRef
            TransactionReferenceType tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;
            freightShipRequest.Request = request;


            freightShipRequest.Shipment = shipment;
            SSLValidator.OverrideValidation();

            FreightShipResponse freightShipResponse;
            try
            {
                freightShipResponse = freightShipService.ProcessShipment(freightShipRequest);
            }
            catch (SoapException soapEx)
            {
                //TODO: Process UPS Exception.
                string ss = soapEx.Detail.InnerText;
                throw new Exception(ss);
            }
            catch (Exception ex)
            {
                //TODO: Process general exception.
                throw ex;
            }
            return freightShipResponse;
        }
    }
}
