﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ShipmentEntities;



namespace ShippingWS
{
    [ServiceContract]
    public interface IShippingWS
    {
        [OperationContract]
        [FaultContract(typeof(ShippingFault))]
        bool CreateShipmentForOnhand(int carrierId, long onhandId, int carrierServiceTypeId, string userName, long taskId);

        [OperationContract]
        [FaultContract(typeof(ShippingFault))]
        List<TrackActivity> GetTracking(int carrierId, long manifestId, TrackingType trackType, TrackingRequestType requestType, string userName, long taskId);

        [OperationContract]
        [FaultContract(typeof(ShippingFault))]
        List<TrackActivity> GetLastTracking(int carrierId, long manifestId, TrackingType trackType, string userName, long taskId);

        [OperationContract]
        [FaultContract(typeof(ShippingFault))]
        VoidResult VoidShipment(int carrierId, long onhandId, string userName);

        [OperationContract]
        [FaultContract(typeof(ShippingFault))]
        List<VoidResult> TestVoidShipment(string shipment, string[] trackings, int carrierId, long onhandId);
    }
}
