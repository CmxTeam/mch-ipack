﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Net.Sockets;
using System.Runtime.Remoting.Channels;
using System.Security.Cryptography;
using ShipmentEntities;
using UPSBLL.Utilities;
using ZebraPrint;

namespace ShippingWS
{
    public static class ShippingDDL
    {
        public static int GetAccountCarrierId(int carrierId, long onhandId)
        {
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
            return (from accCarrs in db.AccountCarriers
                    join onhands in db.Onhands on accCarrs.AccountId equals onhands.AccountId
                    where accCarrs.CarrierId.Equals(carrierId) && onhands.Id.Equals(onhandId)
                    select accCarrs.Id).FirstOrDefault();
        }

        public static int GetAccountCarrierIdByManifest(int carrierId, long manifestId)
        {
            var accCarrierId = 0;
            using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
            {
                var man = (from manifests in db.Manifests
                           where manifests.Id.Equals(manifestId)
                           select manifests).FirstOrDefault();
                if (man == null)
                    return 0;

                accCarrierId = (from accCarrier in db.AccountCarriers
                                where accCarrier.AccountId.Equals(man.AccountId) && accCarrier.CarrierId.Equals(carrierId)
                                select accCarrier.Id).FirstOrDefault();
            }
            return accCarrierId;
        }

        public static int GetShipperId(long onhandId)
        {
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
            return (from onhands in db.Onhands
                    where onhands.Id.Equals(onhandId)
                    select onhands.ShipperId).FirstOrDefault() ?? 0;
        }

        public static List<long> GetPackages(long onhandId)
        {
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
            return (from packs in db.Packages
                    where packs.OnhandId.Equals(onhandId)
                    select packs.Id).ToList();
        }

        public static int GetConsigneeId(long onhandId)
        {
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
            return (from onhands in db.Onhands
                    where onhands.Id.Equals(onhandId)
                    select onhands.ConsigneeId).FirstOrDefault() ?? 0;
        }

        public static AccountCarrierInfo GetAccountCarrierInfo(int accountCarrierId)
        {
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);

            var accCar = (from accCarrs in db.AccountCarriers
                          where accCarrs.Id.Equals(accountCarrierId)
                          select new AccountCarrierInfo()
                                     {
                                         CarrierCode = accCarrs.Carrier.Carrier3Code,
                                         AccountNumber = accCarrs.AccountNo,
                                         AccountId = accCarrs.AccountId,
                                         Username = accCarrs.AccountLogin,
                                         Password = accCarrs.AccountPassword,
                                         AccessKey = accCarrs.AccessKey
                                     }

                          ).FirstOrDefault();
            return accCar;
        }

        public static string GetServiceCodeById(int serviceTypeId)
        {
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
            return (from serviceTypes in db.CarrierServiceTypes
                    where serviceTypes.Id.Equals(serviceTypeId)
                    select serviceTypes.Code).FirstOrDefault();
        }

        public static string GetCarrierCode(int carrierId)
        {
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
            return (from carrs in db.Carriers
                    where carrs.Id.Equals(carrierId)
                    select carrs.Carrier3Code).FirstOrDefault();
        }

        public static List<ShipPackage> GetPackagesData(List<long> packageIds)
        {
            var upsPacks = new List<ShipPackage>();
            var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);

            try
            {
                foreach (var id in packageIds)
                {
                    var data = (from pack in db.Packages where pack.Id.Equals(id) select pack).FirstOrDefault();

                    if (data != null)
                    {

                        if (data.UOM == null)
                        {
                            throw new Exception("Missing DIM UOM for piece number " + data.PieceNumber);
                        }

                        if (data.UOM2 == null)
                        {
                            throw new Exception("Missing Weight UOM for piece number " + data.PieceNumber);
                        }

                        var pack = new ShipPackage()
                        {
                            Description = data.TrackingNumber,
                            RefNumber = new ReferenceNumber()
                            {
                                Value = data.TrackingNumber
                            },
                            PackagingCode = data.PackageType.Code,
                            PackagingDescription = data.PackageType.Name,
                            PackageDims = new Dimensions()
                            {
                                DimensionUOM = new ShipUOM()
                                {
                                    Code = data.UOM.ThreeCharCode,
                                    Description = data.UOM.UOMName
                                },
                                Width = (int)Math.Round(data.Width),
                                Height = (int)Math.Round(data.Height),
                                Length = (int)Math.Round(data.Length)
                            },
                            Weight = data.Weight,
                            WeightUOM = new ShipUOM()
                            {
                                Code = data.UOM2.ThreeCharCode,
                                Description = data.UOM2.UOMName
                            }

                        };

                        if (pack.WeightUOM.Code == "KGS")
                        {
                            //Transform to LBS.
                            pack.WeightUOM.Code = "LBS";
                            pack.WeightUOM.Description = "POUNDS";
                            pack.Weight = pack.Weight * GlobalConstants.KGToLBCoeff;
                        }

                        if (pack.PackageDims.DimensionUOM.Code == "CM")
                        {
                            //Transform to IN.
                            pack.PackageDims.DimensionUOM.Code = "IN";
                            pack.PackageDims.DimensionUOM.Description = "INCHES";
                            pack.PackageDims.Width = (int)Math.Round(pack.PackageDims.Width * GlobalConstants.CMToINCoeff);
                            pack.PackageDims.Height = (int)Math.Round(pack.PackageDims.Height * GlobalConstants.CMToINCoeff);
                            pack.PackageDims.Length = (int)Math.Round(pack.PackageDims.Length * GlobalConstants.CMToINCoeff);
                        }

                        if (pack.PackageDims.Width == 0)
                            pack.PackageDims.Width = 1;
                        if (pack.PackageDims.Height == 0)
                            pack.PackageDims.Height = 1;
                        if (pack.PackageDims.Length == 0)
                            pack.PackageDims.Length = 1;

                        upsPacks.Add(pack);
                    }
                }

                return upsPacks;
            }
            catch (Exception ex)
            {
                var shipEx = new ShippingException(ex.Message, ex);
                throw shipEx;
            }

        }

        public static Partner GetCustomer(int customerId)
        {
            try
            {
                var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
                Customer cust =
                    (from customers in db.Customers where customers.Id.Equals(customerId) select customers).FirstOrDefault();

                if (cust == null)
                    throw new ApplicationException("Missing customer info: " + customerId + ".");

                var tmpPartner = new Partner()
                {
                    Name = cust.Name,
                    AttentionName = cust.Contacts.Any() ? cust.Contacts[0].FullName : "",
                    PhoneNumber = cust.Contacts.Any() ? cust.Contacts[0].Phone : "",
                    PhoneExtension = "",
                    PartnerAddress = new Address()
                    {
                        AddressLine = cust.AddressLine1 + " " + cust.AddressLine2,
                        City = cust.City,
                        StateProvinceCode = cust.State.TwoCharacterCode,
                        PostalCode = cust.PostalCode,
                        CountryCode = cust.Country.TwoCharacterCode
                    }
                };
                return tmpPartner;
            }
            catch (Exception ex)
            {
                var shipEx = new ShippingException(ex.Message, ex);
                throw shipEx;
            }

        }

        public static List<long> SaveManifest(CreatedShipment ship, List<long> packIds, int carrierId, int carrierServiceTypeId, string userLogin, long taskId)
        {
            var retVal = new List<long>();
            try
            {
                using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
                {
                    var serviceType =
                         (from servTypes in db.CarrierServiceTypes
                          where servTypes.Id.Equals(carrierServiceTypeId)
                          select servTypes).FirstOrDefault();

                    if (serviceType == null)
                        throw new ApplicationException("Cannot save manifest. Invalid service type id: " + carrierServiceTypeId + ".");

                    Statuse stat =
                            (from stats in db.Statuses where stats.Name.ToUpper().Equals("PACKAGE MANIFESTED") select stats)
                                .FirstOrDefault();

                    EntityType entType =
                            (from types in db.EntityTypes where types.EntityName.Equals("Package") select types).FirstOrDefault();

                    TransactionAction trAct =
                            (from actions in db.TransactionActions
                             where actions.Code.ToUpper().Equals("MANIFEST CREATED")
                             select actions).FirstOrDefault();

                    EventCode code = (from codes in db.EventCodes
                                      where codes.EventName.ToUpper().Equals("PUT AWAY COMPLETE")
                                      select codes).FirstOrDefault();

                    string userName = (from users in db.UserProfiles
                                       where users.UserId.Equals(userLogin)
                                       select (users.FirstName ?? "" + " " + users.LastName ?? "").Trim()).FirstOrDefault();

                    long? onhandId = null;

                    var flights = new List<long>();

                    for (int i = 0; i < packIds.Count; i++)
                    {
                        var data = (from pack in db.Packages where pack.Id.Equals(packIds[i]) select pack).FirstOrDefault();

                        if (data == null) continue;

                        var man = new Manifest();
                        man.RecDate = DateTime.UtcNow;
                        man.Number = ship.TrackingNumbers[i];
                        man.TotalPieces = data.AvailablePieces;
                        man.TotalStc = data.AvailablePieces;
                        man.TotalWeight = data.Weight;
                        man.UomWeightId = data.WeightUOMId;
                        man.TotalVolume = data.Volume;
                        man.UomVolumeId = data.VolumeUOMId;
                        man.WeightUOMCombined = data.Weight.ToString(CultureInfo.InvariantCulture) + (data.UOM2 == null ? "" : " " + data.UOM2.ThreeCharCode);
                        man.VolumeUOMCombined = data.Volume.ToString(CultureInfo.InvariantCulture) + (data.UOM1 == null ? "" : " " + data.UOM1.ThreeCharCode);
                        man.VoyageFlightTruck = ship.ShipmentIdentificationNumber;
                        man.MOT = serviceType.MOT;
                        man.ManifestType = (from types in db.ManifestTypes
                                            where types.Name.ToUpper().
                                                        Equals(serviceType.MOT.MOT1.ToUpper() == "AIR" ? "AIR PACKAGE" : "GROUND PACKAGE")
                                            select types).FirstOrDefault();

                        man.OriginId = data.OriginId ?? 0;
                        man.DestinationId = data.DestinationId ?? 0;
                        man.CarrierId = carrierId;
                        man.CarrierServiceType = serviceType;
                        man.LocationId = data.WarehouseLocationId;
                        man.AccountId = data.AccountId;
                        man.Statuse = stat;
                        man.StatusTimestamp = DateTime.UtcNow;

                        TextReader tr = new StreamReader(new MemoryStream(Convert.FromBase64String(ship.PackageShippingLabels[i])));
                        man.LabelZPL = tr.ReadToEnd();

                        db.Manifests.InsertOnSubmit(man);

                        db.SubmitChanges();

                        retVal.Add(man.Id);

                        data.ManifestId = man.Id;

                        data.Statuse = stat;
                        onhandId = data.OnhandId;

                        var manTransaction = new Transaction();

                        manTransaction.EntityType = entType;
                        manTransaction.EntityId = data.Id;
                        manTransaction.Statuse = stat;
                        manTransaction.StatusTimestamp = DateTime.UtcNow;
                        manTransaction.Reference = data.TrackingNumber;
                        manTransaction.Description = ship.TrackingNumbers[i] + ": Outbound Local Delivery Created.";
                        manTransaction.UserName = userName;

                        Task manTask = (from tasks in db.Tasks where tasks.Id.Equals(taskId) select tasks).FirstOrDefault();
                        manTransaction.Task = manTask;
                        manTransaction.TransactionAction = trAct;

                        db.Transactions.InsertOnSubmit(manTransaction);

                        FlightManifest flMan = (from packDetails in db.ManifestPackageDetails
                                                where packDetails.PackageId.Equals(data.Id)
                                                select packDetails.FlightManifest).FirstOrDefault();

                        if (flMan != null)
                        {
                            if (!flights.Contains(flMan.Id))
                            {
                                flights.Add(flMan.Id);
                                var flTransaction = new FlightTransaction();
                                flTransaction.FlightManifest = flMan;
                                flTransaction.Statuse = stat;
                                flTransaction.StatusTimestamp = DateTime.Now;
                                flTransaction.Reference = data.Onhand.Number;
                                flTransaction.Description = data.Onhand.Number + ": Outbound Local Delivery Created.";
                                flTransaction.TaskTypeId = 7;
                                flTransaction.UserName = userName;
                                db.FlightTransactions.InsertOnSubmit(flTransaction);
                            }
                        }

                        var manEvent = new Event
                         {
                             RecDate = DateTime.Now,
                             EntityType = entType,
                             EntityId = data.Id,
                             EventCode = code,
                             EventReference = "UPS " + ship.TrackingNumbers[i] + (code == null ? "" : " " + code.Description),
                             UserName = userName,
                             AccountId = data.AccountId,
                             IsTransmitted = 0,
                             IsEmailed = 0
                         };

                        db.Events.InsertOnSubmit(manEvent);

                        db.SubmitChanges();
                    }


                    Onhand onh = (from onhand in db.Onhands
                                  where onhand.Id.Equals(onhandId.GetValueOrDefault())
                                  select onhand).FirstOrDefault();

                    if (onh != null)
                    {
                        onh.Statuse = stat;

                        var onhTransaction = new OnhandTransaction();
                        onhTransaction.Onhand = onh;
                        onhTransaction.Statuse = stat;
                        onhTransaction.StatusTimestamp = DateTime.Now;
                        onhTransaction.Reference = onh.Number;
                        onhTransaction.Description = onh.Number + ": Outbound Local Delivery Created.";
                        onhTransaction.TaskTypeId = 7;
                        onhTransaction.UserName = userName;
                        db.OnhandTransactions.InsertOnSubmit(onhTransaction);
                        db.SubmitChanges();
                    }

                }

                return retVal;
            }
            catch (Exception ex)
            {
                string packTrackings = "";
                for (int i = 0; i < packIds.Count; i++)
                {
                    TextReader tr = new StreamReader(new MemoryStream(Convert.FromBase64String(ship.PackageShippingLabels[i])));
                    packTrackings += Environment.NewLine + "Package: " + packIds[i] + " Tracking: " +
                                    ship.TrackingNumbers[i] + "  Label ZPL: " + tr.ReadToEnd();
                }
                Logger.LogError("Failed to save manifests: " + packTrackings, "", DateTime.Now);
                var shipEx = new ShippingException("Cannot save manifest. " + ex.Message, ex) { Severity = ExceptionSeverity.Transient };
                throw shipEx;
            }
        }

        public static void PrintLabels(List<long> manifestIds, string userName)
        {
            try
            {
                var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString);
                var prn = (from users in db.UserProfiles
                           join userPrinter in db.UserPrinters on users.Id equals userPrinter.UserId
                           join printer in db.Printers on userPrinter.PrinterId equals printer.Id
                           join srv in db.PrintServers on printer.ServerId equals srv.Id
                           where users.UserId.Equals(userName) &&
                                 printer.PrinterType.Code.Equals("L") &&
                                 userPrinter.IsDefault.Equals(true)
                           select @"\\" + srv.Name + @"\" + printer.Name).FirstOrDefault();

                if (prn == null)
                {
                    throw new ApplicationException("Missing Label printer data in Db for user " + userName);
                }

                var labels = (from mans in db.Manifests
                              where manifestIds.Contains(mans.Id) && mans.LabelZPL != null && mans.LabelZPL != ""
                              select mans.LabelZPL).ToList();

                foreach (var label in labels)
                {
                    ZebraPrinter.PrintLable(label, prn, 1);
                }
            }
            catch (Exception ex)
            {

                var shipex = new ShippingException("Print fail. " + ex.ToString(), ex, ExceptionSeverity.Transient);
                throw shipex;
            }

        }

        public static void SendErrorEvent(List<long> packageIds, string userLogin, long taskId, int? accountId, string upsError)
        {
            try
            {
                long? onhandId = 0;
                var flights = new List<long>();
                using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
                {
                    string userName = (from users in db.UserProfiles
                                       where users.UserId.Equals(userLogin)
                                       select (users.FirstName ?? "" + " " + users.LastName ?? "").Trim()).FirstOrDefault();

                    Statuse stat =
                            (from stats in db.Statuses where stats.Name.Equals("Manifesting Exception") select stats)
                                .FirstOrDefault();

                    EntityType entType =
                            (from types in db.EntityTypes where types.EntityName.Equals("Package") select types).FirstOrDefault();

                    EventCode code = (from codes in db.EventCodes
                                      where codes.EventName.Equals("Put Away Exception")
                                      select codes).FirstOrDefault();

                    foreach (long packId in packageIds)
                    {

                        var data = (from pack in db.Packages where pack.Id.Equals(packId) select pack).FirstOrDefault();
                        if (data != null)
                        {
                            var manTransaction = new Transaction();

                            manTransaction.EntityType = entType;
                            manTransaction.EntityId = data.Id;
                            manTransaction.Statuse = stat;
                            manTransaction.StatusTimestamp = DateTime.UtcNow;
                            manTransaction.Reference = data.TrackingNumber;
                            manTransaction.Description = data.TrackingNumber + ": Outbound Manifest Failed: " + upsError;
                            manTransaction.UserName = userName;

                            FlightManifest flMan = (from packDetails in db.ManifestPackageDetails
                                                    where packDetails.PackageId.Equals(data.Id)
                                                    select packDetails.FlightManifest).FirstOrDefault();

                            if (flMan != null)
                            {
                                if (!flights.Contains(flMan.Id))
                                {
                                    flights.Add(flMan.Id);
                                    var flTransaction = new FlightTransaction();
                                    flTransaction.FlightManifest = flMan;
                                    flTransaction.Statuse = stat;
                                    flTransaction.StatusTimestamp = DateTime.Now;
                                    flTransaction.Reference = flMan.FlightNumber;
                                    flTransaction.Description = flMan.FlightNumber + ": Outbound Manifest Failed: " + upsError;
                                    flTransaction.TaskTypeId = 7;
                                    flTransaction.UserName = userName;
                                    db.FlightTransactions.InsertOnSubmit(flTransaction);
                                }
                            }


                            Task manTask = (from tasks in db.Tasks where tasks.Id.Equals(taskId) select tasks).FirstOrDefault();
                            manTransaction.Task = manTask;

                            onhandId = data.OnhandId;
                            data.Statuse = stat;
                            db.Transactions.InsertOnSubmit(manTransaction);

                            if (code != null)
                            {
                                var manEvent = new Event
                                {
                                    RecDate = DateTime.Now,
                                    EntityType = entType,
                                    EntityId = packId,
                                    EventCode = code,
                                    EventReference = code.Description,
                                    UserName = userName,
                                    AccountId = accountId,
                                    IsTransmitted = 0,
                                    IsEmailed = 0
                                };
                                db.Events.InsertOnSubmit(manEvent);
                            }


                            db.SubmitChanges();
                        }
                    }

                    Onhand onh = (from onhand in db.Onhands
                                  where onhand.Id.Equals(onhandId.GetValueOrDefault())
                                  select onhand).FirstOrDefault();

                    if (onh != null)
                    {
                        onh.Statuse = stat;

                        var onhTransaction = new OnhandTransaction();
                        onhTransaction.Onhand = onh;
                        onhTransaction.StatusId = 37;
                        onhTransaction.StatusTimestamp = DateTime.Now;
                        onhTransaction.Reference = onh.Number;
                        onhTransaction.Description = onh.Number + ": Outbound Manifest Failed: " + upsError;
                        onhTransaction.UserName = userName;
                        onhTransaction.TaskTypeId = 7;
                        db.OnhandTransactions.InsertOnSubmit(onhTransaction);
                        db.SubmitChanges();
                    }
                }

            }
            catch (Exception ex) // This is called from catch clause, dont want any errors to be thrown from here.
            {

            }

        }

        internal static string GetInquiryNumber(long manifestId, TrackingType trackType)
        {
            using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
            {
                var manifest = (from manifests in db.Manifests
                                where manifests.Id.Equals(manifestId)
                                select manifests).FirstOrDefault();
                if (manifest != null)
                {
                    if (trackType == TrackingType.Package)
                        return manifest.Number;
                    else
                    {
                        return manifest.VoyageFlightTruck;
                    }
                }
            }
            return null;
        }

        public static void SaveTrackResult(List<TrackActivity> tracking, TrackingType trackType, long manifestId, string inquiryNumber)
        {
            try
            {
                using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
                {
                    EntityType entType =
                                (from types in db.EntityTypes where types.EntityName.Equals("Package") select types).FirstOrDefault();


                    List<Manifest> manifests;

                    if (trackType == TrackingType.Package)
                    {
                        manifests = (from mans in db.Manifests
                                     where mans.Id.Equals(manifestId)
                                     select mans).ToList();
                    }
                    else
                    {
                        manifests = (from mans in db.Manifests
                                     where mans.VoyageFlightTruck.Equals(inquiryNumber)
                                     select mans).ToList();
                    }

                    foreach (var activity in tracking)
                    {
                        var tmpManifest = (from mans in manifests
                                           where mans.Number.Equals(activity.TrackingNumber)
                                           select mans).FirstOrDefault();

                        if (tmpManifest == null || tmpManifest.Packages == null || tmpManifest.Packages.Count == 0)
                            continue;

                        activity.PackageId = tmpManifest.Packages[0].Id;
                        EventCode code = GetEventCode(activity.ActivityCode, db);
                        //TODO: 1
                        if (code == null)
                            continue;

                        var tmpEvent = new Event()
                                       {
                                           EventCode = code,
                                           RecDate = activity.ActivityTime,
                                           EntityType = entType,
                                           EntityId = tmpManifest.Packages[0].Id,
                                           EventReference = activity.ActivityDescription,
                                           EventLocation = activity.ActivityLocation,
                                           UserName = "UPS",
                                           AccountId = tmpManifest.Packages[0].AccountId,
                                           IsTransmitted = 0,
                                           IsEmailed = 0,
                                           EventPOD = (code.Id == 52 ? (activity.DeliveryPOD ?? "") : "")
                                       };
                        db.Events.InsertOnSubmit(tmpEvent);


                        //Save delivery infromation if delivery event.
                        if (code.Id == 52)
                        {
                            tmpManifest.DeliveredOn = activity.ActivityTime;
                            tmpManifest.DeliveredTo = activity.DeliveryPOD;
                        }

                        var onhTransaction = new OnhandTransaction
                                             {
                                                 Onhand = tmpManifest.Packages[0].Onhand,
                                                 StatusId = code.StatusId_,
                                                 StatusTimestamp = activity.ActivityTime,
                                                 Reference = activity.TrackingNumber,
                                                 Description = (activity.ActivityDescription ?? "" + ", " + activity.ActivityLocation ?? "").Trim(new[] { ' ', ',' }),
                                                 UserName = "UPS",
                                                 TaskTypeId = 7,
                                                 PackageId = activity.PackageId
                                             };
                        db.OnhandTransactions.InsertOnSubmit(onhTransaction);

                        if (code.StatusId_.HasValue)
                        {
                            tmpManifest.Packages[0].StatusId = code.StatusId_.Value;
                            tmpManifest.Packages[0].StatusTimestamp = activity.ActivityTime;
                        }

                        db.SubmitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to save tracking data.");
            }
        }

        private static EventCode GetEventCode(string activityCode, MchDbDataContext db)
        {
            //TODO: Hardcoding now. Chnage this. Probably move to Db.

            int eventCodeId = 0;
            switch (activityCode.ToUpper())
            {
                case "X1":
                    eventCodeId = 36;
                    break;
                case "X2":
                    eventCodeId = 37;
                    break;
                case "X3":
                    eventCodeId = 38;
                    break;
                case "X":
                    eventCodeId = 33;
                    break;
                case "I":
                    eventCodeId = 23;
                    break;
                case "D":
                    eventCodeId = 52;
                    break;
                case "P":
                    eventCodeId = 48;
                    break;
                case "M":
                    eventCodeId = 21;
                    break;
                case "MV":
                    eventCodeId = 46;
                    break;
                case "RS":
                    eventCodeId = 44;
                    break;
            }

            return (from eventCodes in db.EventCodes
                    where eventCodes.Id.Equals(eventCodeId)
                    select eventCodes).FirstOrDefault();
        }

        internal static string GetUPSShipmentForOnahnd(long onhandId)
        {
            try
            {
                using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
                {
                    return (from onh in db.Onhands
                            join pck in db.Packages on onh.Id equals pck.OnhandId
                            join mn in db.Manifests on pck.ManifestId equals mn.Id
                            where onh.Id.Equals(onhandId) && mn.VoyageFlightTruck != null
                            select mn.VoyageFlightTruck).
                                  FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to get UPS shipment number. " + ex.Message);
            }
        }

        internal static string[] GetPackageTrackingNumbers(List<long> packageIds)
        {
            using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
            {
                return (from pack in db.Packages
                        join mans in db.Manifests on pack.ManifestId equals mans.Id
                        where packageIds.Contains(pack.Id)
                        select mans.Number).ToArray();
            }
        }

        internal static void SaveVoidData(VoidResult result, List<VoidResult> packRes, long onhandId, string userLogin)
        {
            try
            {
                using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
                {
                    EventCode code = (from codes in db.EventCodes
                                      where codes.EventName.Equals("Shipment Information Voided")
                                      select codes).FirstOrDefault();

                    Statuse stat = (from stats in db.Statuses
                                    where stats.Name.Equals("Manifest Void Exception")
                                    select stats).FirstOrDefault();

                    EntityType entType =
                           (from types in db.EntityTypes where types.EntityName.Equals("Package") select types).FirstOrDefault();


                    string userName = (from users in db.UserProfiles
                                       where users.UserId.Equals(userLogin)
                                       select (users.FirstName ?? "" + " " + users.LastName ?? "").Trim()).FirstOrDefault();

                    Onhand onh = (from onhs in db.Onhands
                                  where onhs.Id.Equals(onhandId)
                                  select onhs).FirstOrDefault();

                    result.EntityId = onhandId;

                    if (onh == null)
                    {
                        throw new Exception("Missing onhand in Db.");
                    }

                    if (result.IsVoided)
                    {
                        onh.Statuse = code == null ? null : code.Statuse;
                    }
                    else
                    {
                        onh.Statuse = stat;
                    }

                    foreach (var pack in onh.Packages)
                    {
                        Package tmpPack = pack;
                        Manifest man = (from mans in db.Manifests
                                        where
                                            mans.Id.Equals(tmpPack.ManifestId)
                                        select mans).FirstOrDefault();


                        if (man == null)
                            continue;

                        VoidResult res = (from vRes in packRes
                                          where vRes.UPSNo.Equals(man.Number)
                                          select vRes).FirstOrDefault();

                        if (res == null)
                            continue;

                        if (res.IsVoided)
                        {
                            //TODO: save transaction. Status 47.

                            man.Statuse = code == null ? null : code.Statuse;
                            pack.StatusId = 1;
                            pack.StatusTimestamp = DateTime.Now;
                            pack.Manifest = null;

                            var trans = new Transaction
                                                 {
                                                     EntityType = entType,
                                                     EntityId = man.Packages[0].Id,
                                                     Statuse = code == null ? null : code.Statuse,
                                                     StatusTimestamp = DateTime.UtcNow,
                                                     Reference = man.Number,
                                                     Description =
                                                         man.Number + ": Shipment Information Voided.",
                                                     UserName = "UPS"
                                                 };

                            db.Transactions.InsertOnSubmit(trans);

                            //TODO: save onhand transaction. Status 47.
                            var onhTransaction = new OnhandTransaction
                                                 {
                                                     Onhand = man.Packages[0].Onhand,
                                                     Statuse = code == null ? null : code.Statuse,
                                                     StatusTimestamp = DateTime.Now,
                                                     Reference = man.Packages[0].Onhand.Number,
                                                     Description =
                                                         man.Packages[0].Onhand.Number +
                                                         ": Shipment Information Voided.",
                                                     TaskTypeId = 7,
                                                     UserName = userName,
                                                     Package = man.Packages[0]
                                                 };
                            db.OnhandTransactions.InsertOnSubmit(onhTransaction);

                            //TODO: save Event. Event code 46.

                            var packEvent = new Event
                            {
                                RecDate = DateTime.Now,
                                EntityType = entType,
                                EntityId = man.Packages[0].Id,
                                EventCode = code,
                                EventReference = "UPS " + man.Number + (code == null ? "" : " " + code.Description),
                                UserName = userName,
                                AccountId = man.Packages[0].AccountId,
                                IsTransmitted = 0,
                                IsEmailed = 0
                            };
                            db.Events.InsertOnSubmit(packEvent);
                        }
                        else
                        {
                            //TODO: Save onhand transaction. Status 49.
                            var onhTransaction = new OnhandTransaction
                            {
                                Onhand = onh,
                                Statuse = stat,
                                StatusTimestamp = DateTime.Now,
                                Reference = onh.Number,
                                Description =
                                    onh.Number +
                                    ": Manifest Void Exception- " + result.StatusDescription,
                                TaskTypeId = 7,
                                UserName = userName,
                                Package = man.Packages[0]
                            };
                            db.OnhandTransactions.InsertOnSubmit(onhTransaction);

                            var trans = new Transaction
                            {
                                EntityType = entType,
                                EntityId = man.Packages[0].Id,
                                Statuse = stat,
                                StatusTimestamp = DateTime.Now,
                                Reference = man.Number,
                                Description =
                                    man.Number + ": Manifest Void Exception- " + result.StatusDescription,
                                UserName = userName
                            };
                            db.Transactions.InsertOnSubmit(trans);
                        }
                        db.SubmitChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ShippingException(ExceptionSeverity.Transient, "CMX", "Unable to save void data in MCH DB: " + ex.Message, ex.StackTrace);
            }
        }

        public static void SendVoidError(long onhandId, string userName, string upsError)
        {
            try
            {
                using (var db = new MchDbDataContext(ConfigurationManager.ConnectionStrings["MCHDB"].ConnectionString))
                {
                    //TODO: Save UPS error.
                    Onhand onh = (from onhand in db.Onhands
                                  where onhand.Id.Equals(onhandId)
                                  select onhand).FirstOrDefault();

                    Statuse stat = (from stats in db.Statuses
                                    where stats.Name.Equals("Manifest Void Exception")
                                    select stats).FirstOrDefault();

                    EntityType entType =
                           (from types in db.EntityTypes where types.EntityName.Equals("Package") select types).FirstOrDefault();

                    if (onh != null)
                    {
                        onh.Statuse = stat;

                        var onhTransaction = new OnhandTransaction
                        {
                            Onhand = onh,
                            Statuse = stat,
                            StatusTimestamp = DateTime.Now,
                            Reference = onh.Number,
                            Description =
                                onh.Number + ": Manifest Void Exception- " +
                                upsError,
                            UserName = userName,
                            TaskTypeId = 7
                        };
                        db.OnhandTransactions.InsertOnSubmit(onhTransaction);
                        db.SubmitChanges();


                        foreach (var pack in onh.Packages)
                        {
                            if (pack.ManifestId == null)
                                continue;

                            var transaction = new Transaction
                                                 {
                                                     EntityType = entType,
                                                     EntityId = pack.Id,
                                                     Statuse = stat,
                                                     StatusTimestamp = DateTime.UtcNow,
                                                     Reference = pack.TrackingNumber,
                                                     Description =
                                                         pack.TrackingNumber +
                                                         ": Manifest Void Exception- " + upsError,
                                                     UserName = userName
                                                 };

                            db.Transactions.InsertOnSubmit(transaction);
                        }
                        db.SubmitChanges();
                    }
                }
            }
            catch (Exception ex) //This is called from catch clause, dont want any errors to be thrown from here.
            { }
        }
    }

    public class AccountCarrierInfo
    {
        public string CarrierCode { get; set; }
        public string AccountNumber { get; set; }
        public int? AccountId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AccessKey { get; set; }
    }
}