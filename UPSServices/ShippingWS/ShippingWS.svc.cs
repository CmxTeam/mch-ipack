﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ShipmentEntities;
using ShippingInterface;
using UPSBLL.ShippingPackage;
using UPSBLL.Tracking;
using UPSBLL.Void;

namespace ShippingWS
{
    public class ShippingWS : IShippingWS
    {
        public bool CreateShipment(int accountCarrierId, int carrierId, int shipperCustomerId, int shipFromCustomerId, int shipToCustomerId, int carrierServiceTypeId,
             List<long> packageIds, string userName, long taskId)
        {
            AccountCarrierInfo acc = null;
            try
            {
                IShippingService srv;

                acc = ShippingDDL.GetAccountCarrierInfo(accountCarrierId);

                if (acc == null)
                {
                    throw new FaultException(
                        "Missing account carrier info for Id = " + accountCarrierId.ToString() + ".",
                        new FaultCode("Missing data exception."));
                }

                string carrier = acc.CarrierCode;
                string accountNumber = acc.AccountNumber;
                string serviceCode = ShippingDDL.GetServiceCodeById(carrierServiceTypeId);
                if (String.IsNullOrEmpty(serviceCode))
                {
                    throw new FaultException("Incorrect service type Id = " + accountCarrierId.ToString() + ".",
                                             new FaultCode("Missing data exception."));
                }

                if (carrier == "UPS")
                {
                    srv = new UPSShippingService();
                }
                else
                {
                    throw new FaultException("Carrier not supported.", new FaultCode("Not supported exception."));
                }

                List<ShipPackage> packages = ShippingDDL.GetPackagesData(packageIds);

                Partner shipper = ShippingDDL.GetCustomer(shipperCustomerId);
                shipper.ShipperNumber = accountNumber;

                Partner shipFrom = ShippingDDL.GetCustomer(shipFromCustomerId);

                Partner shipTo = ShippingDDL.GetCustomer(shipToCustomerId);

                CreatedShipment ship = srv.CreateShipment(shipper, shipFrom, shipTo, accountNumber, serviceCode, packages, new Credentials() { Username = acc.Username, Password = acc.Password, AccessKey = acc.AccessKey });

                List<long> manifestIds = ShippingDDL.SaveManifest(ship, packageIds, carrierId, carrierServiceTypeId, userName, taskId);

                ShippingDDL.PrintLabels(manifestIds, userName);

                return true;
            }
            catch (ShippingException shipEx)
            {
                string errorNumber = "";
                if (shipEx.ExceptionCode != "CMX")
                {
                    ShippingDDL.SendErrorEvent(packageIds, userName, taskId, acc.AccountId, shipEx.ExceptionDescription);
                }
                else
                {
                    errorNumber = Logger.LogError(shipEx.ExceptionDescription, shipEx.ExceptionSource, DateTime.Now);
                }
                var fault = new ShippingFault()
                {
                    ExceptionCode = shipEx.ExceptionCode,
                    ExceptionDescription = shipEx.ExceptionDescription,
                    ExceptionSource = shipEx.ExceptionSource,
                    Severity = shipEx.Severity,
                    ExceptionNumber = errorNumber
                };
                throw new FaultException<ShippingFault>(fault);
            }
            catch (Exception ex)
            {
                string errorNumber = Logger.LogError(ex.Message, ex.StackTrace, DateTime.Now);
                var fault = new ShippingFault()
                                          {
                                              ExceptionCode = "CMX",
                                              ExceptionDescription = ex.ToString(),
                                              ExceptionSource = ex.StackTrace,
                                              Severity = ExceptionSeverity.Hard,
                                              ExceptionNumber = errorNumber
                                          };
                throw new FaultException<ShippingFault>(fault);
            }

        }

        public bool CreateShipmentForOnhand(int carrierId, long onhandId, int carrierServiceTypeId,
            string userName, long taskId)
        {
            try
            {
                int accountCarrierId = ShippingDDL.GetAccountCarrierId(carrierId, onhandId);
                int shipToCustomerId = ShippingDDL.GetConsigneeId(onhandId);
                int shipperCustomerId = shipToCustomerId;
                int shipFromCustomerId = ShippingDDL.GetShipperId(onhandId);
                List<long> packageIds = ShippingDDL.GetPackages(onhandId);

                if (packageIds.Count == 0)
                {
                    throw new Exception("Onhand has no packages.");
                }

                return CreateShipment(accountCarrierId, carrierId, shipperCustomerId, shipFromCustomerId, shipToCustomerId,
                    carrierServiceTypeId, packageIds, userName, taskId);
            }
            catch (FaultException<ShippingFault>)
            {
                throw;
            }
            catch (Exception ex)
            {
                string errorNumber = Logger.LogError(ex.Message, ex.StackTrace, DateTime.Now);
                var fault = new ShippingFault()
                {
                    ExceptionCode = "CMX",
                    ExceptionDescription = ex.ToString(),
                    ExceptionSource = ex.StackTrace,
                    Severity = ExceptionSeverity.Hard,
                    ExceptionNumber = errorNumber
                };
                throw new FaultException<ShippingFault>(fault);
            }
        }

        public List<TrackActivity> GetTracking(int carrierId, long manifestId, TrackingType trackType, TrackingRequestType requestType, string userName,
            long taskId)
        {
            ITrackingService srv;

            try
            {
                string inquiryNumber = ShippingDDL.GetInquiryNumber(manifestId, trackType);

                if (inquiryNumber == null)
                    throw new FaultException("Missing inquiry number.");

                int accountCarrierId = ShippingDDL.GetAccountCarrierIdByManifest(carrierId, manifestId);

                if (accountCarrierId == 0)
                    throw new FaultException("Missing account carrier info.");

                AccountCarrierInfo acc = ShippingDDL.GetAccountCarrierInfo(accountCarrierId);

                List<TrackActivity> res;

                if (acc.CarrierCode.ToUpper().Equals("UPS"))
                {
                    srv = new UPSTrackingService();

                    res = srv.GetTracking(inquiryNumber, trackType, requestType, DateTime.MinValue, DateTime.MinValue, new Credentials() { AccessKey = acc.AccessKey, Password = acc.Password, Username = acc.Username });

                    ShippingDDL.SaveTrackResult(res, trackType, manifestId, inquiryNumber);
                }
                else
                {
                    throw new FaultException("Carrier not supported.", new FaultCode("Not supported exception."));
                }

                return res;
            }
            catch (ShippingException shipEx)
            {
                string errorNumber = "";
                if (shipEx.ExceptionCode == "CMX")
                {
                    errorNumber = Logger.LogError(shipEx.ExceptionDescription, shipEx.ExceptionSource, DateTime.Now);
                }
                var fault = new ShippingFault()
                {
                    ExceptionCode = shipEx.ExceptionCode,
                    ExceptionDescription = shipEx.ExceptionDescription,
                    ExceptionSource = shipEx.ExceptionSource,
                    Severity = shipEx.Severity,
                    ExceptionNumber = errorNumber
                };
                throw new FaultException<ShippingFault>(fault);
            }
            catch (Exception ex)
            {
                string errorNumber = Logger.LogError(ex.Message, ex.StackTrace, DateTime.Now);
                var fault = new ShippingFault()
                {
                    ExceptionCode = "CMX",
                    ExceptionDescription = ex.ToString(),
                    ExceptionSource = ex.StackTrace,
                    Severity = ExceptionSeverity.Hard,
                    ExceptionNumber = errorNumber

                };
                throw new FaultException<ShippingFault>(fault);
            }

        }

        public List<TrackActivity> GetLastTracking(int carrierId, long manifestId, TrackingType trackType, string userName,
            long taskId)
        {
            return GetTracking(carrierId, manifestId, trackType, TrackingRequestType.Last, userName, taskId);
        }

        public VoidResult VoidShipment(int carrierId, long onhandId, string userName)
        {
            AccountCarrierInfo acc = null;
            try
            {
                int accountCarrierId = ShippingDDL.GetAccountCarrierId(carrierId, onhandId);
                acc = ShippingDDL.GetAccountCarrierInfo(accountCarrierId);
                string shipIdentificationNumber = ShippingDDL.GetUPSShipmentForOnahnd(onhandId);

                if (string.IsNullOrEmpty(shipIdentificationNumber))
                {
                    throw new ApplicationException("Missing UPS shipment data in Db.");
                }

                IVoidService srv;

                if (acc.CarrierCode.ToUpper().Equals("UPS"))
                {
                    srv = new UPSVoidService();

                    List<VoidResult> packRes;
                    VoidResult result = srv.VoidShipment(new Credentials() { AccessKey = acc.AccessKey, Password = acc.Password, Username = acc.Username },
                         shipIdentificationNumber, out packRes);
                    ShippingDDL.SaveVoidData(result, packRes, onhandId, userName);

                    return result;
                }
                else
                {
                    throw new ApplicationException("Carrier not supported.");
                }
            }
            catch (ShippingException shipEx)
            {
                string errorNumber = "";
                if (shipEx.ExceptionCode != "CMX")
                {
                    ShippingDDL.SendVoidError(onhandId, userName, shipEx.ExceptionDescription);
                }
                else
                {
                    errorNumber = Logger.LogError("Failed voiding for OnhandId = :" + onhandId + ". " + shipEx.ExceptionDescription, shipEx.ExceptionSource, DateTime.Now);
                }
                var fault = new ShippingFault()
                {
                    ExceptionCode = shipEx.ExceptionCode,
                    ExceptionDescription = shipEx.ExceptionDescription,
                    ExceptionSource = shipEx.ExceptionSource,
                    Severity = shipEx.Severity,
                    ExceptionNumber = errorNumber
                };
                throw new FaultException<ShippingFault>(fault);
            }
            catch (Exception ex)
            {
                string errorNumber = Logger.LogError("Failed voiding for OnhandId = :" + onhandId + ". " + ex.Message, ex.StackTrace, DateTime.Now);
                var fault = new ShippingFault()
                {
                    ExceptionCode = "CMX",
                    ExceptionDescription = ex.ToString(),
                    ExceptionSource = ex.StackTrace,
                    Severity = ExceptionSeverity.Hard,
                    ExceptionNumber = errorNumber
                };
                throw new FaultException<ShippingFault>(fault);
            }
        }

        public List<VoidResult> TestVoidShipment(string shipment, string[] trackings, int carrierId, long onhandId)
        {
            AccountCarrierInfo acc = null;
            int accountCarrierId = ShippingDDL.GetAccountCarrierId(carrierId, onhandId);
            acc = ShippingDDL.GetAccountCarrierInfo(accountCarrierId);

            UPSVoidService srv = new UPSVoidService();
            List<VoidResult> packRes;
            srv.VoidShipment(new Credentials()
                             {
                                 AccessKey = acc.AccessKey,
                                 Password = acc.Password,
                                 Username = acc.Username
                             },
                shipment, out packRes, trackings);
            return null;
        }
    }
}
