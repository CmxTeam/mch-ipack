﻿using System;
using QVProcessing;
namespace TestUPSConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionStr =
                "Data Source=cmxsecure.com,41433;Initial Catalog=MCHDBTest;Persist Security Info=True;User ID=appcon;Password=AlphaRomeo880";
            string QVUrl = "https://wwwcie.ups.com/ups.app/xml/QVEvents";
            //string QVUrl = "https://onlinetools.ups.com/ups.app/xml/QVEvents";


            int accountCarrierId = 2;

            var qv = new QVProcess(connectionStr, QVUrl, accountCarrierId);

            qv.ProcessQVEvents();

            Console.WriteLine();
        }
    }
}
