﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;

namespace MailClient
{
    public class MailSender : IDisposable
    {
        private readonly string _connectionString;
        private readonly string _smtp;
        private readonly string _smtpAccount;
        private readonly string _smtpPassword;

        public MailSender(string smtp, string smtpAccount, string smtpPassword, string connctionString)
        {
            _smtp = smtp;
            _connectionString = connctionString;
            _smtpAccount = smtpAccount;
            _smtpPassword = smtpPassword;
        }

        public void SendEmails()
        {
            var envelopes = GetEnvelopes();
            var successfullySentEmails = new Dictionary<long, string>();

            foreach (var envelope in envelopes)
            {
                try
                {
                    SendEmailImpl(envelope);
                    successfullySentEmails.Add(envelope.Id, string.Empty);
                }
                catch (Exception e)
                {
                    List<SqlParameter> pars = new List<SqlParameter> {new SqlParameter("@ERROR", e.Message)};
                    ExecuteScalar(string.Format("Update HostPlus_Emails Set Tries = isnull(Tries, 0) + 1, MailDate = getutcdate(), TransmitionError = @ERROR where RecId = {0}", envelope.Id), pars);
                }
            }

            if (successfullySentEmails.Count > 0)
            {
                var ids = string.Join(",", successfullySentEmails.Select(m => m.Key));
                ExecuteScalar(string.Format("Update HostPlus_Emails Set Status = 1, Tries = isnull(Tries, 0) + 1, MailDate = getutcdate() where RecId in ({0})", ids));
            }
        }

        private void SendEmailImpl(Envelope envelope)
        {
            var mailMessage = new MailMessage(_smtpAccount, envelope.To)
                                  {
                                      Subject = envelope.Subject,
                                      Priority = MailPriority.High,
                                      Body = envelope.Body
                                  };
            if (!string.IsNullOrEmpty(envelope.Cc))
                mailMessage.CC.Add(envelope.Cc);
            if (!string.IsNullOrEmpty(envelope.Bcc))
                mailMessage.Bcc.Add(envelope.Bcc);
            if (envelope.Attachments != null && envelope.Attachments.Count > 0)
            {
                foreach (string attachment in envelope.Attachments)
                {
                    mailMessage.Attachments.Add(new Attachment(envelope.IsZip ? Utility.CompressFile(attachment) : attachment, MediaTypeNames.Application.Octet));
                }
            }

            if (!string.IsNullOrEmpty(_smtpAccount))
            {
                mailMessage.IsBodyHtml = false;
                SmtpClient client = new SmtpClient();
                client.Host = _smtp;
                client.Credentials = new System.Net.NetworkCredential(_smtpAccount, _smtpPassword);
                client.Port = 587;
                client.EnableSsl = true;
                client.Send(mailMessage);
            }
                        
            Thread.Sleep(2000);
        }

        private IEnumerable<Envelope> GetEnvelopes()
        {
            var dataset = Fill("Select RecId, MailFrom, MailTo, MailCc, MailBcc, Subject, Body, IsZip from HostPlus_Emails where Status = 0 and Tries < 5");
            IEnumerable<Envelope> envelopes = dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new Envelope
                 {
                     Id = Convert.ToInt64(r["RecId"].ToString()),
                     From = r["MailFrom"].ToString(),
                     To = r["MailTo"].ToString(),
                     Bcc = r["MailBcc"].ToString(),
                     Cc = r["MailCc"].ToString(),
                     Subject = r["Subject"].ToString(),
                     Body = r["Body"].ToString(),
                     IsZip = r["IsZip"] != null && (int)r["IsZip"] != 0
                 }).ToList();

            foreach (var env in envelopes)
            {
                var attachmentDataset = Fill("SELECT FileName FROM HostPlus_EmailAttachments WHERE EmailId = " + env.Id);
                List<string> attachments =
                    attachmentDataset.Tables[0].Rows.OfType<DataRow>().Select(r => r["FileName"].ToString()).ToList();

                env.Attachments = attachments;
            }

            return envelopes;
        }

        private void ExecuteScalar(string commandString)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(commandString, connection))
                {
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
        }

        private void ExecuteScalar(string commandString, List<SqlParameter> queryparams)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(commandString, connection))
                {
                    try
                    {
                        command.Parameters.Clear();
                        command.Parameters.AddRange(queryparams.ToArray());
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                    }
                }
            }
        }

        private DataSet Fill(string commandString)
        {
            var dataSet = new DataSet();
            using (var connection = new SqlConnection(_connectionString))
            {
                using (var command = new SqlCommand(commandString, connection))
                {
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSet);
                    }
                }
            }
            return dataSet;
        }

        public void Dispose()
        {

        }
    }
}
