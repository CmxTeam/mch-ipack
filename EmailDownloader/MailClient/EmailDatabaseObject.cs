﻿namespace MailClient
{
    public class EmailDatabaseObject
    {
        public EmailDatabaseObject(string connectionName, string tableName, string columnBody, 
            string columnDownloadedDate, string columSentDate, string columnName, string columnProcessingStatus)
        {
            ConnectionName = connectionName;
            TableName = tableName;
            ColumnBody = columnBody;
            ColumDownloadedDate = columnDownloadedDate;
            ColumSentDate = columSentDate;
            ColumnName = columnName;
            ColumnProcessingStatus = columnProcessingStatus;
        }

        public string ConnectionName { get; private set; }
        public string TableName { get; private set; }
        public string ColumnBody { get; private set; }
        public string ColumDownloadedDate { get; private set; }
        public string ColumSentDate { get; private set; }
        public string ColumnName { get; private set; }
        public string ColumnProcessingStatus { get; private set; }
    }
}