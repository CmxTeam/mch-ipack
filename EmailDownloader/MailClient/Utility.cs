﻿using System;
using System.IO;
using System.IO.Compression;

namespace MailClient
{
    internal class Utility
    {
        internal static string CompressFile(string filePath)
        {
            try
            {
                //Create a name for new archive file.
                string zipFileName = filePath + ".zip";

                using (var inFile = new FileStream(filePath, FileMode.Open))
                {
                    using (FileStream outFile = File.Create(zipFileName))
                    {
                        using (var compress = new GZipStream(outFile, CompressionMode.Compress))
                        {
                            // Copy the source file into the compression stream.
                            inFile.CopyTo(compress);
                            compress.Close();
                        }
                        outFile.Close();
                    }
                    inFile.Close();
                }

                return zipFileName;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}
