﻿using OpenPop.Pop3;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace MailClient
{
    public class MailDownloader : IDisposable
    {
        private int _port = 110;
        private readonly string _userName;
        private readonly Pop3Client _client;
        private bool _disposed;
        private readonly DateTime _startFrom;
        private readonly bool _deleteFromServer;

        public bool UseSsl { get; set; }
        public int Port { get { return _port; } set { _port = value; } }
        public List<string> From { get; set; }

        public MailDownloader(string hostName, string userName, string password, string startFrom, bool deleteFromServer)
        {
            _deleteFromServer = deleteFromServer;
            _userName = userName;
            _startFrom = ConvertToDate(startFrom);
            _client = new Pop3Client();
            _client.Connect(hostName, 110, UseSsl);
            _client.Authenticate(userName, password, AuthenticationMethod.UsernameAndPassword);
        }

        ~MailDownloader()
        {
            Dispose(true);
        }

        public IEnumerable<Email> Download(out DateTime lastDate)
        {
            return DownloadImpl(out lastDate);
        }

        public DateTime DownloadIntoFolderAndSaveIntoDatabase(string path, string connectionString)
        {
            DateTime lastDate;
            var emails = DownloadImpl(out lastDate);
            DownloadIntoFolder(path, emails);
            DownloadIntoDatabase(connectionString, emails);
            DeleteMessagesFromServer(emails);
            return lastDate;
        }

        public DateTime DownloadIntoFolder(string path)
        {
            DateTime lastDate;
            var emails = DownloadImpl(out lastDate);
            DownloadIntoFolder(path, emails);
            DeleteMessagesFromServer(emails);
            return lastDate;
        }

        public DateTime DownloadIntoDatabase(string connectionString)
        {
            DateTime lastDate;
            var emails = DownloadImpl(out lastDate);
            DownloadIntoDatabase(connectionString, emails);
            DeleteMessagesFromServer(emails);
            return lastDate;
        }

        private IEnumerable<Email> DownloadImpl(out DateTime lastDate)
        {
            var messagesCount = _client.GetMessageCount();
            var dateToStartFrom = _startFrom.AddHours(-4);
            lastDate = dateToStartFrom;
            var startIndexToDownload = FindStartIndexToDownload(1, messagesCount, dateToStartFrom);
            var result = new List<Email>();

            for (var messageNumber = startIndexToDownload; messageNumber <= messagesCount; ++messageNumber)
            {
                if (From != null)
                {
                    var headers = _client.GetMessageHeaders(messageNumber);
                    if (!From.Select(f => f.ToLower()).Contains(headers.From.Address.ToLower())) continue;
                }

                var message = _client.GetMessage(messageNumber);

                result.Add(new Email
                {
                    DateSent = message.Headers.DateSent,
                    Body = message.FindFirstPlainTextVersion().GetBodyAsText(),
                    Uid = message.Headers.MessageId,
                    Subject = message.Headers.Subject
                });
                lastDate = message.Headers.DateSent;
            }
            return result;
        }

        private void DeleteMessagesFromServer(IEnumerable<Email> emails)
        {
            if(!_deleteFromServer) return;

            var messageCount = _client.GetMessageCount();
            var ids = emails.Select(e => e.Uid).ToList();
            for (var messageItem = messageCount; messageItem > 0; messageItem--)
            {
                if (ids.Contains(_client.GetMessageHeaders(messageItem).MessageId))
                {
                    _client.DeleteMessage(messageItem);
                }
            }
        }

        private static void DownloadIntoFolder(string path, IEnumerable<Email> emails)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            foreach (var email in emails)
            {
                var fileFullName = GetFileFullName(path, email);
                if (File.Exists(fileFullName)) continue;

                var fileStream = File.Create(fileFullName);
                var bytes = new UTF8Encoding(true).GetBytes(email.Body);
                fileStream.Position = fileStream.Length;
                fileStream.Write(bytes, 0, bytes.Length);
                fileStream.Flush();
                fileStream.Close();
            }
        }

        private static IEnumerable<IEnumerable<T>> Chunk<T>(IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }

        private void DownloadIntoDatabase(string connectionString, IEnumerable<Email> emails)
        {
            if (emails.Count() == 0)
                return;

            foreach (var cunk in Chunk(emails, 150))
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;

                        var commandBuilder = new StringBuilder();
                        commandBuilder.AppendLine("Insert Into CIMPMessageLogs (FileName, RawMessage, ProcessingStatus, DownloadedTime, SentTime, MailboxName, MessageType)");
                        var selectList = new List<string>();

                        var ems = cunk.ToList();                        

                        for (var i = 0; i < ems.Count(); ++i)
                        {
                            var email = ems[i];
                            selectList.Add(string.Format(@"Select @FileName{0}, @RawMessage{0}, 0, getdate(), @SentTime{0}, @MailboxName{0}, @MessageType{0}
                                        where not exists (Select Top 1 1 from CIMPMessageLogs where FileName = @FileName{0})", i));
                            command.Parameters.Add(new SqlParameter(string.Format("@FileName{0}", i), email.Uid));
                            command.Parameters.Add(new SqlParameter(string.Format("@RawMessage{0}", i), email.Body));
                            command.Parameters.Add(new SqlParameter(string.Format("@SentTime{0}", i), email.DateSent));
                            command.Parameters.Add(new SqlParameter(string.Format("@MailboxName{0}", i), _userName));
                            command.Parameters.Add(new SqlParameter(string.Format("@MessageType{0}", i), email.Subject.Length <= 20 ? email.Subject : email.Subject.Substring(0, 20)));
                        }

                        commandBuilder.Append(string.Join(" union ", selectList));

                        command.CommandText = commandBuilder.ToString();
                        connection.Open();
                        command.ExecuteScalar();
                        if (connection.State == ConnectionState.Closed)
                            return;
                        connection.Close();
                    }
                }
            }
        }

        private static DateTime ConvertToDate(string date)
        {
            DateTime result;
            return DateTime.TryParse(date, out result) ? result : DateTime.MinValue;
        }

        private static string GetFileFullName(string path, Email email)
        {
            return Path.Combine(path, email.Uid) + email.DateSent.ToString().Replace('/', '.').Replace(" ", "-").Replace(':', '_') + ".txt";
        }

        private int FindStartIndexToDownload(int startIndex, int endIndex, DateTime dateToStartFrom)
        {
            if (startIndex == endIndex || endIndex - startIndex == 1)
                return startIndex;

            var devidIndex = (endIndex + startIndex) / 2;
            var startHeader = _client.GetMessageHeaders(devidIndex);

            return dateToStartFrom > startHeader.DateSent
                       ? FindStartIndexToDownload(devidIndex, endIndex, dateToStartFrom)
                       : FindStartIndexToDownload(startIndex, devidIndex, dateToStartFrom);
        }

        protected void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing && _client != null)
            {
                try
                {
                    _client.Disconnect();
                    _client.Dispose();
                }
                catch (Exception)
                {
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}
