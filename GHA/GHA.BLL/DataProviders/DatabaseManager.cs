﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace GHA.BLL.DataProviders
{
    public class DatabaseManager
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(DatabaseManager));
        public static string ExecuteScalar1(string commandName, IEnumerable<KeyValuePair<string, object>> parameters, string databaseName = "WFSDBData")
        {
            logger.Info("Start of " + commandName + " with parameters " + string.Join(", ", parameters.Select(p => p.Key + ": " + p.Value)));
            object result = null;
            var connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(commandName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (var param in parameters)
                        command.Parameters.Add(new SqlParameter(param.Key, param.Value));

                    try
                    {
                        connection.Open();
                        result = command.ExecuteScalar();
                        return result == null ? null : result.ToString();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(commandName + ": " + ex.Message);
                        return string.Empty;
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                        logger.Info("End of " + commandName + ".");
                    }
                }
            }
        }

        public static T Fill<T>(string commandName, IEnumerable<KeyValuePair<string, object>> parameters, Func<DataSet, T> extractor, string databaseName = "WFSDBData")
        {
            var dataSet = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(commandName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if (parameters != null)
                    {
                        foreach (var param in parameters)
                            command.Parameters.Add(new SqlParameter(param.Key, param.Value));
                    }
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSet);
                    }
                }
            }
            return extractor(dataSet);
        }

        public static void ExecuteScalar(string commandName, IEnumerable<KeyValuePair<string, object>> parameters, string databaseName = "WFSDBData")
        {            
            object result = null;
            var connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(commandName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (var param in parameters)
                        command.Parameters.Add(new SqlParameter(param.Key, param.Value));

                    try
                    {
                        connection.Open();
                        result = command.ExecuteScalar();         
                    }
                    catch (Exception ex)
                    {                        
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();                     
                    }
                }
            }
        }
    }
}