﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class OnhandStageLocation
    {
        public string UserName { get; set; }
        public long OnhandId { get; set; }
        public int LocationId { get; set; }
        public long TaskId { get; set; }
    }
}
