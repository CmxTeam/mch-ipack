﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class BreakdownAwbs
    {
        public long AwbManifestId { get; set; }
        public long AwbId { get; set; }        
        public string StatusImagePath { get; set; }
        public string Carrier3Code { get; set; }
        public string AwbSerialNumber { get; set; }
        public int PercentReceived { get; set; }
        public int ReceivedCount { get; set; }
        public int LoadCount { get; set; }
        public int IsTransfer { get; set; }
        public int UldReceivedPieces {get; set;}
        public int UldTotalPieces{get; set;}
        public int UldPercentReceived { get; set; }
        public int HasHwb { get; set; }
    }
}
