﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class DeleteEntityReference
    {
        public long TaskId { get; set; }
        public long ReferenceId { get; set; }
        public string UserName { get; set; }
    }
}
