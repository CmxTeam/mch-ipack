﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class BreakdownHwb
    {
        public long HwbId { get; set; }
        public long ManifestDetailId { get; set; }
        public string HwbSerialNumber { get; set; }
        public int HwbReceivedCount { get; set; }
        public int ForkliftPieces { get; set; }
        public int PercentPutAway { get; set; }
        public int PercentReceived { get; set; }
        public int PutAwayPieces { get; set; }
        public string OriginIata { get; set; }
        public string DestinationIata { get; set; }
        public string StatusImagePath { get; set; }
        public int AwbReceivedCount { get; set; }
        public int AwbLoadCount { get; set; }
        public int AwbPercentReceived { get; set; }
        public int HwbTotalPcs { get; set; }
        public bool IsSplitFlight { get; set; }
        public bool IsSplitUld { get; set; }

    }
}
