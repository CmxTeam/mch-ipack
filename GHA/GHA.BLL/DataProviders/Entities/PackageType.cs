﻿namespace GHA.BLL.DataProviders.Entities
{
    public class PackageType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}