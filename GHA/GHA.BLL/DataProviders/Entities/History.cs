﻿using System;

namespace GHA.BLL.DataProviders.Entities
{
    public class History
    {
        public string Date { get; set; }
        public string Reference { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
    }
}
