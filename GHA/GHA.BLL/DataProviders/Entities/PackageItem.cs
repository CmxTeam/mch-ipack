﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class PackageItem
    {
        public int PackageNumber { get; set; }
        public long PackageId { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string DimUOM { get; set; }
        public float Weight { get; set; }
        public string WeightUOM { get; set; }
        public string PackageType { get; set; }
        public bool IsHazmat { get; set; }

        public int PackageTypeId { get; set; }

    }
}
