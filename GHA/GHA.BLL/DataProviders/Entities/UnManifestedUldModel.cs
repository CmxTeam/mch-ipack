﻿namespace GHA.BLL.DataProviders.Entities
{
    public class UnManifestedUldModel
    {
        public float FlightManifestId { get; set; }
        public float TaskId { get; set; }
        public int UnitTypeId { get; set; }
        public string SerialNumber { get; set; }
        public int TotalPieces { get; set; }
        public float Weight { get; set; }
        public string UserName { get; set; }
    }
}