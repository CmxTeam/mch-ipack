﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class BreakdownUld
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public int TotalUnitCount { get; set; }
        public int TotalReceivedCount { get; set; }
        public int TotalPieces { get; set; }
        public int ReceivedPieces { get; set; }
        public int PutAwayPieces { get; set; }
        public double PercentRecovered { get; set; }
        public double PercentReceived { get; set; }
        public int FlightReceivedPiecs { get; set; }
        public int FlightTotalReceivedPieces { get; set; }
        public int FlightPercentReceived { get; set; }
        public string StatusImagePath { get; set; }
        public string CarrierCode { get; set; }
        public string Code { get; set; }        
    }
}
