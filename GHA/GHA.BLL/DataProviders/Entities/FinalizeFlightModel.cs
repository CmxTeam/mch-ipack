﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class FinalizeFlightModel
    {
        public long FlightId { get; set; }
        public long TaskId { get; set; }
        public string Username { get; set; }
    }
}
