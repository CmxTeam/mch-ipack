﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class OnhandDetailsModel
    {
        public long OnhandId { get; set; }
        public string OnhandNumber { get; set; }
        public string GoodsDesc { get; set; }
        public string Shipper { get; set; }
        public string Consignee { get; set; }
        public int DestinationId { get; set; }
        public string DestinationCode { get; set; }
    }
}
