﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class NewReferenceModel
    {
        public long? ReferenceId { get; set; }
        public int EntityTypeId { get; set; }
        public long EntityId { get; set; }
        public int ReferenceTypeId { get; set; }
        public string ReferenceValue { get; set; }
        public string UserName { get; set; }
    }
}
