﻿namespace LightSwitchApplication.WebApi.Models
{
    public class DeleteOnhandPackage
    {
        public long OnhandId { get; set; }
        public long PackageId { get; set; }
        public string UserName { get; set; }
    }
}