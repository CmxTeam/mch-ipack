﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class DGRChecklistStatus
    {
        public int TotalQuestions { get; set; }
        public int Answered { get; set; }
        public bool IsChecklistFinalized { get; set; }
    }
}
