﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class NewPackageModel
    {
        public string UserName { get; set; }
        public long OnhandId { get; set; }
        public int Count { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int DimUOMId { get; set; }
        public float Weight { get; set; }
        public int WeightUOMId { get; set; }
        public int PackageTypeId { get; set; }
        public bool IsHazmat { get; set; }
        public long? PackageId { get; set; }
        
    }
}
