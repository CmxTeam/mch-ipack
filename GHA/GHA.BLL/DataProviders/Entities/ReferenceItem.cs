﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class ReferenceItem
    {
        public long ReferenceId { get; set; }
        public int ReferenceTypeId { get; set; }
        public string ReferenceType { get; set; }
        public string Value { get; set; }
        public string UserName { get; set; }
    }
}
