﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class DGRChecklistComment
    {
        public int EntityTypeId { get; set; }
        public long EntityId { get; set; }
        public int CheckListItemId { get; set; }
        public string Comment { get; set; }
        public string UserName { get; set; }
        public long TaskId { get; set; }
    }
}
