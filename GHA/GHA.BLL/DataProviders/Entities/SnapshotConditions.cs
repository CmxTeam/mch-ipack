﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class SnapshotConditions
    {

        public int Id { get; set; }
        public string ConditionName { get; set; }
        public bool IsDefault { get; set; }

    }
}
