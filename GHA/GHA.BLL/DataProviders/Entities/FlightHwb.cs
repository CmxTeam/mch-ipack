﻿namespace GHA.BLL.DataProviders.Entities
{
    public class FlightHwb
    {
        public string Hwb { get; set; }
        public int TotalPieces { get; set; }
        public string ServiceType { get; set; }
        public string Destination { get; set; }
    }
}