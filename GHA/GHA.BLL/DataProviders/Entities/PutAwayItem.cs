﻿namespace GHA.BLL.DataProviders.Entities
{
    public class PutAwayItem
    {
        public string Type { get; set; }
        public long EntityId { get; set; }
        public string RefNumber { get; set; }
        public string RefNumber1 { get; set; }
        public bool IsBUP { get; set; }
        public int PutAwayPieces { get; set; }
        public int TotalPieces { get; set; }
        public int PercentPutAway { get; set; }
        public int ForkliftPieces { get; set; }
        public int Status { get; set; }
        public string StatusImagePath { get; set; }
        public string PaId { get; set; }
        public long FlightManifestId { get; set; }
        public int FlightTotalPieces { get; set; }
        public int FlightPutAwayPieces { get; set; }
        public int FlightPercentPutAway { get; set; }
    }
}