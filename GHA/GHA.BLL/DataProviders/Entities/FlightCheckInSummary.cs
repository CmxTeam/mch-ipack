﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class FlightCheckInSummary
    {
        public string StageLocation { get; set; }
        public string StatusName { get; set; }
        public int TotalPieces { get; set; }
        public int ReceivedPieces { get; set; }
        public float PercentReceived { get; set; }
    }
}
