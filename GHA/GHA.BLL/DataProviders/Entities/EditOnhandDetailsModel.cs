﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class EditOnhandDetailsModel
    {
        public long OnhandId { get; set; }
        public string ShipperName { get; set; }
        public string ConsigneeName { get; set; }
        public string DestinationCode { get; set; }
        public int DestinationId { get; set; }
        public string GoodsDescription { get; set; }
        public string UserName { get; set; }
    }
}
