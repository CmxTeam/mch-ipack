﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string AccountCode { get; set; }

        public string AccountLogoPath { get; set; }
    }
}
