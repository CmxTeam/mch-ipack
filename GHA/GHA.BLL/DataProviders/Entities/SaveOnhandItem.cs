﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class SaveOnhandItem
    {
        public int accountId { get; set; }
        public string truckerPro { get; set; }
        public int carrierId { get; set; }
        public long driverId { get; set; }
        public int idType1 { get; set; }
        public bool matchingPhoto1 { get; set; }
        public int? idType2 { get; set; }
        public bool? matchingPhoto2 { get; set; }
        public int totalPieces { get; set; }
        public float totalWeight { get; set; }
        public int uomId { get; set; }
        public int receivedPieces { get; set; }
        public string userName { get; set; }
        public int warehouseId { get; set; }
    }
}
