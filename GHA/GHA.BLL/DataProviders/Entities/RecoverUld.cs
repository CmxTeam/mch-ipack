﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class RecoverUld
    {
        public long Id { get; set; }
        public string SerialNumber{get; set;}
        public string CarrierCode { get; set; }
        public string CarrierName { get; set; }
        public string Carrier3Code { get; set; }
        public string Code { get; set; }
        public int PercentRecovered { get; set; }
        public int FlightTotalUlds { get; set; }
        public int FlightPercentRecovered { get; set; }
        public int FlightRecoveredUlds { get; set; }
        public int TotalReceivedCount { get; set; }
        public int TotalUnitCount { get; set; }
    }
}
