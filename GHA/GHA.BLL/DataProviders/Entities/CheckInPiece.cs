﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class CheckInPiece
    {
        public long TaskId { get; set; }
        public string UserName { get; set; }
        public string PackageIds { get; set; }
    }
}
