﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class SnapshotItem
    {
        public long Id { get; set; }
        public string ImagePath { get; set; }
        public int Count { get; set; }
        public string UserName { get; set; }
        public string Timestamp { get; set; }
        public long EntityTypeId { get; set; }
        public long EntityId { get; set; }
        public string DisplayText { get; set; }
    }
}
