﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class UpdateEtaModel
    {
        public long FlightId { get; set; }
        public long PortId { get; set; }
        public DateTime Eta { get; set; }
        public string Username { get; set; }
    }
}