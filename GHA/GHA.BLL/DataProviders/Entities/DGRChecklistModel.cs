﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class DGRChecklistModel
    {
        public int ItemId { get; set; }
        public string ListItemHeader { get; set; }
        public string ListItemNo { get; set; }
        public string SubLineNo { get; set; }
        public string ListItem { get; set; }
        public string ValueList { get; set; }

    }
}
