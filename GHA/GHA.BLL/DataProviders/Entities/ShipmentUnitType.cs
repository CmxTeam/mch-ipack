﻿namespace GHA.BLL.DataProviders.Entities
{
    public class ShipmentUnitType
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }
}