﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class CheckInItem
    {
        public long PackageId { get; set; }
        public string RefNumber { get; set; }
        public int TotalPieces { get; set; }
        public int ReceivedPcs { get; set; }
        public string Location { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string ULDNumber { get; set; }
        public string HWBSerialNumber { get; set; }
    }
}
