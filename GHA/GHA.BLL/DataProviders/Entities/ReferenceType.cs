﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class ReferenceType
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
