﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GHA.BLL.DataProviders.Entities
{
    public class ReceiverFlight
    {
        public Int64 FlightManifestId { get; set; }
        public string OrgFlightNumber { get; set; }
        public string ETA { get; set; }
        public string ArrivalTime { get; set; }
        public int TotalULDs { get; set; }
        public int TotalAWBs { get; set; }
        public int TotalPieces { get; set; }
        public string CarrierName { get; set; }
        public string FlightNumber { get; set; }
        public int RecoveredULDs { get; set; }
        public int ReceivedPieces { get; set; }
        public string RecoveredBy { get; set; }
        public string RecoveredOn { get; set; }
        public bool IsNilCargo { get; set; }
        public int PortId { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string CarrierLogoPath { get; set; }
        public string StatusImagePath { get; set; }
        public string Location { get; set; }
        public int UnloadingId { get; set; }
        public bool IsComplete { get; set; }
        public string Origin { get; set; }
        public string AccountCode { get; set; }

    }
}