﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class PutedAwayItemCustom
    {
        public string Type { get; set; }
        public long EntityId { get; set; }
        public string Reference { get; set; }
        public string Reference1 { get; set; }
        public bool IsBUP { get; set; }
        public int WarehouseLocationId { get; set; }
        public int Count { get; set; }
        public string Location { get; set; }
        public long DispositionId { get; set; }
        public int Status { get; set; }
        public string StatusImagePath { get; set; }
        public string PaId { get; set; }        
    }
}
