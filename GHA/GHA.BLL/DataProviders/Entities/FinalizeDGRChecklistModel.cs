﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class FinalizeDGRChecklistModel
    {
        public int EntityTypeId { get; set; }
        public long EntityId { get; set; }
        public string Comments { get; set; }
        public string Place { get; set; }
        public string SignaturePath { get; set; }
        public string UserName { get; set; }
        public long TaskId { get; set; }
    }
}
