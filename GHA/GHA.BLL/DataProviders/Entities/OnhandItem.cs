﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GHA.BLL.DataProviders.Entities
{
    public class OnhandItem
    {
        public long OnhandId { get; set; }
        public string OnhandNumber { get; set; }
        public string TruckerPro { get; set; }
        public int TotalPieces { get; set; }
        public float TotalWeight { get; set; }
        public int ReceivedPieces { get; set; }
        public string CarrierName { get; set; }
        public string StatusImagePath { get; set; }
        public string AccountCode { get; set; }
        public int AccountId { get; set; }
        public string ReceivedBy { get; set; }
        public string StagedLocation { get; set; }
        public string ReceivedDate { get; set; }
        public long TaskId { get; set; }

    }
}
