﻿using GHA.BLL.DataProviders;
using GHA.BLL.DataProviders.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GHA.Bll.DataProviders
{
    public class CargoReceiver
    {
        public static List<ReceiverFlight> GetFlights()
        {
            return DatabaseManager.Fill<IEnumerable<ReceiverFlight>>("GetReceiverFlights", null, dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new ReceiverFlight
                {
                    ArrivalTime = r["ArrivalTime"].ToString(),
                    CarrierLogoPath = r["CarrierLogoPath"].ToString(),
                    CarrierName = r["CarrierName"].ToString(),
                    ETA = Convert.ToDateTime(r["ETA"]).ToString(),
                    FlightManifestId = Convert.ToInt64(r["FlightManifestId"]),
                    FlightNumber = r["FlightNumber"].ToString(),
                    IsComplete = Convert.ToBoolean(r["IsComplete"]),
                    IsNilCargo = Convert.ToBoolean(r["IsNilCargo"]),
                    Location = r["Location"].ToString(),
                    OrgFlightNumber = r["OrgFlightNumber"].ToString(),
                    Origin = r["Origin"].ToString(),
                    PortId = Convert.ToInt32(r["PortId"]),
                    ReceivedPieces = Convert.ToInt32(r["ReceivedPieces"]),
                    RecoveredBy = r["RecoveredBy"].ToString(),
                    RecoveredOn = r["RecoveredOn"].ToString(),
                    RecoveredULDs = Convert.ToInt32(r["RecoveredULDs"]),
                    StatusId = Convert.ToInt32(r["StatusId"]),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    StatusName = r["StatusName"].ToString(),
                    TotalAWBs = Convert.ToInt32(r["TotalAWBs"]),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    TotalULDs = Convert.ToInt32(r["TotalULDs"]),
                    UnloadingId = Convert.ToInt32(r["UnloadingId"])
                })).ToList();
        }

        public static List<ReceiverFlight> GetCargoReceiverFlights(string userId, int rowPerPage, int pageNumber, bool isComplete, string filter, DateTime? dateFilter)
        {
            return DatabaseManager.Fill<IEnumerable<ReceiverFlight>>("GetCargoReceiverFlights",
                new[] { 
                new KeyValuePair<string, object>("@UserId", userId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),
                new KeyValuePair<string, object>("@IsComplete", isComplete),
                new KeyValuePair<string, object>("@Filter", filter),
                new KeyValuePair<string, object>("@DateFilter", dateFilter)
                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new ReceiverFlight
                {
                    ArrivalTime = r["ArrivalTime"].ToString(),
                    CarrierLogoPath = r["CarrierLogoPath"].ToString(),
                    CarrierName = r["CarrierName"].ToString(),
                    ETA = Convert.ToDateTime(r["ETA"]).ToString("dd-MMM-yy HH:mm"),
                    FlightManifestId = Convert.ToInt64(r["FlightManifestId"]),
                    FlightNumber = r["FlightNumber"].ToString(),
                    IsComplete = Convert.ToBoolean(r["IsComplete"]),
                    IsNilCargo = Convert.ToBoolean(r["IsNilCargo"]),
                    Location = r["Location"].ToString(),
                    OrgFlightNumber = r["OrgFlightNumber"].ToString(),
                    Origin = r["Origin"].ToString(),
                    PortId = Convert.ToInt32(r["PortId"]),
                    ReceivedPieces = Convert.ToInt32(r["ReceivedPieces"]),
                    RecoveredBy = r["RecoveredBy"].ToString(),
                    RecoveredOn = r["RecoveredOn"].ToString(),
                    RecoveredULDs = Convert.ToInt32(r["RecoveredULDs"]),
                    StatusId = Convert.ToInt32(r["StatusId"]),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    StatusName = r["StatusName"].ToString(),
                    TotalAWBs = Convert.ToInt32(r["TotalAWBs"]),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    TotalULDs = Convert.ToInt32(r["TotalULDs"]),
                    UnloadingId = Convert.ToInt32(r["UnloadingId"]),
                    AccountCode = r["Account"].ToString()
                })).ToList();
        }

        public static List<ReceiverFlight> GetCheckinFlights(string userId, int rowPerPage, int pageNumber, bool isComplete, string filter, DateTime? dateFilter)
        {
            return DatabaseManager.Fill<IEnumerable<ReceiverFlight>>("GetCheckInFlights",
                new[] { 
                new KeyValuePair<string, object>("@UserId", userId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),
                new KeyValuePair<string, object>("@IsComplete", isComplete),
                new KeyValuePair<string, object>("@Filter", filter),
                new KeyValuePair<string, object>("@DateFilter", dateFilter)
                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new ReceiverFlight
                {
                    ArrivalTime = r["ArrivalTime"].ToString(),
                    CarrierLogoPath = r["CarrierLogoPath"].ToString(),
                    CarrierName = r["CarrierName"].ToString(),
                    ETA = Convert.ToDateTime(r["ETA"]).ToString("dd-MMM-yy HH:mm"),
                    FlightManifestId = Convert.ToInt64(r["FlightManifestId"]),
                    FlightNumber = r["FlightNumber"].ToString(),
                    IsComplete = Convert.ToBoolean(r["IsComplete"]),
                    IsNilCargo = Convert.ToBoolean(r["IsNilCargo"]),
                    Location = r["Location"].ToString(),
                    OrgFlightNumber = r["OrgFlightNumber"].ToString(),
                    Origin = r["Origin"].ToString(),
                    PortId = Convert.ToInt32(r["PortId"]),
                    ReceivedPieces = Convert.ToInt32(r["ReceivedPieces"]),
                    RecoveredBy = r["RecoveredBy"].ToString(),
                    RecoveredOn = r["RecoveredOn"].ToString(),
                    RecoveredULDs = Convert.ToInt32(r["RecoveredULDs"]),
                    StatusId = Convert.ToInt32(r["StatusId"]),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    StatusName = r["StatusName"].ToString(),
                    TotalAWBs = Convert.ToInt32(r["TotalAWBs"]),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    TotalULDs = Convert.ToInt32(r["TotalULDs"]),
                    UnloadingId = Convert.ToInt32(r["UnloadingId"]),
                    AccountCode = r["Account"].ToString()
                })).ToList();
        }

        public static List<PutAwayItem> GetPutAwayList(long manifetId, int rowPerPage, int pageNumber, string refNumber)
        {
            return DatabaseManager.Fill<IEnumerable<PutAwayItem>>("GetPutAwayList", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", manifetId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),
                new KeyValuePair<string, object>("@RefNumber", refNumber)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new PutAwayItem
                {
                    Type = r["Type"].ToString(),
                    EntityId = Convert.ToInt64(r["EntityId"]),
                    RefNumber = r["RefNumber"].ToString(),
                    RefNumber1 = r["RefNumber1"].ToString(),
                    IsBUP = Convert.ToBoolean(r["IsBUP"]),
                    PutAwayPieces = Convert.ToInt32(r["PutAwayPieces"]),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    PercentPutAway = Convert.ToInt32(r["PercentPutAway"]),
                    ForkliftPieces = Convert.ToInt32(r["ForkliftPieces"]),
                    Status = Convert.ToInt32(r["Status"]),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    PaId = r["PaId"].ToString(),
                    FlightManifestId = manifetId,
                    FlightTotalPieces = Convert.ToInt32(r["FlightTotalPieces"]),
                    FlightPutAwayPieces = Convert.ToInt32(r["FlightPutAwayPieces"]),
                    FlightPercentPutAway = Convert.ToInt32(r["FlightPercentPutAway"])
                })).ToList();
        }

        public static List<PutedAwayItemCustom> GetPutedAwayList(long manifestId, int rowPerPage, int pageNumber, string refNumber)
        {
            return DatabaseManager.Fill<IEnumerable<PutedAwayItemCustom>>("GetPutedAwayList", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", manifestId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),
                new KeyValuePair<string, object>("@RefNumber", refNumber)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new PutedAwayItemCustom
                {
                    Type = r["Type"].ToString(),
                    EntityId = Convert.ToInt64(r["EntityId"]),
                    Reference = r["Reference"].ToString(),
                    Reference1 = r["Reference1"].ToString(),
                    IsBUP = Convert.ToBoolean(r["IsBUP"]),
                    WarehouseLocationId = Convert.ToInt32(r["WarehouseLocationId"]),
                    Count = Convert.ToInt32(r["Count"]),
                    Location = r["Location"].ToString(),
                    DispositionId = Convert.ToInt64(r["DispositionId"]),
                    Status = Convert.ToInt32(r["Status"]),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    PaId = r["PaId"].ToString(),
                })).ToList();
        }

        public static List<BreakdownUld> GetBreakdownUlds(long manifestId, int rowPerPage, int pageNumber, bool showComplete, string filter)
        {
            return DatabaseManager.Fill<IEnumerable<BreakdownUld>>("GetBreakdownUlds", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", manifestId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),
                new KeyValuePair<string, object>("@ShowComplete", showComplete),
                new KeyValuePair<string, object>("@Filter", filter)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new BreakdownUld
                {
                    Id = Convert.ToInt64(r["Id"]),
                    FlightPercentReceived = Convert.ToInt32(r["FlightPercentReceived"]),
                    FlightReceivedPiecs = Convert.ToInt32(r["FlightReceivedPiecs"]),
                    FlightTotalReceivedPieces = Convert.ToInt32(r["FlightTotalReceivedPieces"]),
                    PercentReceived = Convert.ToInt32(r["PercentReceived"]),
                    PercentRecovered = Convert.ToInt32(r["PercentRecovered"]),
                    PutAwayPieces = Convert.ToInt32(r["PutAwayPieces"]),
                    ReceivedPieces = Convert.ToInt32(r["ReceivedPieces"]),
                    SerialNumber = r["SerialNumber"].ToString(),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    TotalReceivedCount = Convert.ToInt32(r["TotalReceivedCount"]),
                    TotalUnitCount = Convert.ToInt32(r["TotalUnitCount"]),
                    CarrierCode = r["CarrierCode"].ToString(),
                    Code = r["Code"].ToString()
                })).ToList();
        }

        public static List<BreakdownAwbs> GetBreakdownAwbs(long manifestId, long uldId, int rowPerPage, int pageNumber, bool showComplete, bool showAll, string filter)
        {
            return DatabaseManager.Fill<IEnumerable<BreakdownAwbs>>("GetBreakdownAwbs", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", manifestId),
                new KeyValuePair<string, object>("@UldId", uldId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),
                new KeyValuePair<string, object>("@ShowComplete", showComplete),
                new KeyValuePair<string, object>("@ShowAll", showAll),
                new KeyValuePair<string, object>("@Filter", filter)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new BreakdownAwbs
                {
                    AwbManifestId = Convert.ToInt64(r["AwbManifestId"]),
                    AwbId = Convert.ToInt64(r["AwbId"]),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    Carrier3Code = r["Carrier3Code"].ToString(),
                    AwbSerialNumber = r["AwbSerialNumber"].ToString(),
                    PercentReceived = Convert.ToInt32(r["PercentReceived"]),
                    ReceivedCount = Convert.ToInt32(r["ReceivedCount"]),
                    LoadCount = Convert.ToInt32(r["LoadCount"]),
                    IsTransfer = Convert.ToInt32(r["IsTransfer"]),
                    UldReceivedPieces = Convert.ToInt32(r["UldReceivedPieces"]),
                    UldTotalPieces = Convert.ToInt32(r["UldTotalPieces"]),
                    UldPercentReceived = Convert.ToInt32(r["UldPercentReceived"]),
                    HasHwb = Convert.ToInt32(r["HasHwb"])
                })).ToList();
        }

        public static List<BreakdownHwb> GetBreakdownHwbs(long manifestId, long uldId, long manifestAWBDetailId, int rowPerPage, int pageNumber, string filter)
        {
            return DatabaseManager.Fill<IEnumerable<BreakdownHwb>>("GetBreakdownHwbs", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", manifestId),
                new KeyValuePair<string, object>("@UldId", uldId),
                new KeyValuePair<string, object>("@ManifestAWBDetailId", manifestAWBDetailId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),                
                new KeyValuePair<string, object>("@Filter", filter)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new BreakdownHwb
                {
                    HwbId = Convert.ToInt64(r["HwbId"]),
                    ManifestDetailId = Convert.ToInt64(r["ManifestDetailId"]),
                    HwbSerialNumber = r["HwbSerialNumber"].ToString(),
                    HwbReceivedCount = Convert.ToInt32(r["HwbReceivedCount"]),
                    ForkliftPieces = Convert.ToInt32(r["ForkliftPieces"]),
                    PercentPutAway = Convert.ToInt32(r["PercentPutAway"]),
                    PercentReceived = Convert.ToInt32(r["PercentReceived"]),
                    PutAwayPieces = Convert.ToInt32(r["PutAwayPieces"]),
                    DestinationIata = r["DestinationIata"].ToString(),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    AwbReceivedCount = Convert.ToInt32(r["AwbReceivedCount"]),
                    AwbLoadCount = Convert.ToInt32(r["AwbLoadCount"]),
                    AwbPercentReceived = Convert.ToInt32(r["AwbPercentReceived"]),
                    HwbTotalPcs = Convert.ToInt32(r["HwbTotalPcs"]),
                    IsSplitFlight = Convert.ToBoolean(r["IsSplitFlight"]),
                    IsSplitUld = Convert.ToBoolean(r["IsSplitUld"])
                })).ToList();
        }

        public static List<SnapshotItem> GetSnapshotItems(long entityTypeId, long entityId)
        {
            return DatabaseManager.Fill<IEnumerable<SnapshotItem>>("GetSnapshotImages", new[] { 
                new KeyValuePair<string, object>("@EntityTypeId", entityTypeId),
                new KeyValuePair<string, object>("@EntityId", entityId)        
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new SnapshotItem
                {
                    Id = Convert.ToInt64(r["Id"]),
                    Count = Convert.ToInt32(r["Count"]),
                    ImagePath = r["ImagePath"].ToString(),
                    UserName = r["UserName"].ToString(),
                    Timestamp = r["Timestamp"].ToString(),
                    EntityTypeId = Convert.ToInt64(r["EntityTypeId"]),
                    EntityId = Convert.ToInt64(r["EntityId"]),
                    DisplayText = r["DisplayText"].ToString()
                })).ToList();
        }

        public static void SaveSnapshotImage(string imagePath, string conditions, long entityTypeId, int count, long entityId,
            string userName, long taskId, long warehouseId, long flightId, string displayText)
        {
            DatabaseManager.ExecuteScalar("SaveSnapshotImage", new[] 
            {
                new KeyValuePair<string, object>("@ImagePath", imagePath),
                new KeyValuePair<string, object>("@Conditions", conditions),
                new KeyValuePair<string, object>("@EntityTypeId", entityTypeId),
                new KeyValuePair<string, object>("@Count", count),
                new KeyValuePair<string, object>("@EntityId", entityId),
                new KeyValuePair<string, object>("@UserName", userName),
                new KeyValuePair<string, object>("@TaskId", taskId),
                new KeyValuePair<string, object>("@WarehouseId", warehouseId),
                new KeyValuePair<string, object>("@FlightId", flightId),
                new KeyValuePair<string, object>("@DisplayText", displayText)
            });
        }

        public static IEnumerable<History> GetHistory(long flightManifestId, string filter, int rowsPerPage, int pageNumber)
        {
            return DatabaseManager.Fill<IEnumerable<History>>("GetFlightHistoryApi", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", flightManifestId),
                new KeyValuePair<string, object>("@Filter", filter),
                new KeyValuePair<string, object>("@RowsPerPage", rowsPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new History
                {
                    Date = Convert.ToDateTime(r["Date"]).ToString("MMM-dd HH:mm"),
                    Description = r["Description"].ToString(),
                    Reference = r["Reference"].ToString(),
                    UserName = r["UserName"].ToString()
                })).ToList();
        }

        public static void UpdateFlightEta(long flightId, long portId, DateTime eta, string userName)
        {
            DatabaseManager.ExecuteScalar("UpdateFlightEta", new[] 
            {
                new KeyValuePair<string, object>("@FlightManifestId", flightId),
                new KeyValuePair<string, object>("@PortId", portId),
                new KeyValuePair<string, object>("@ETA", eta),
                new KeyValuePair<string, object>("@UserName", userName)                
            });
        }

        public static IEnumerable<RecoverUld> GetRecoverUldsForFlight(long manifestId, bool isRecovered, int rowsPerPage, int pageNumber, string filter)
        {
            return DatabaseManager.Fill<IEnumerable<RecoverUld>>("GetRecoverUldsForFlight", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", manifestId),
                new KeyValuePair<string, object>("@IsRecovered", isRecovered),
                new KeyValuePair<string, object>("@Filter", filter),
                new KeyValuePair<string, object>("@RowsPerPage", rowsPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new RecoverUld
                {
                    Id = Convert.ToInt64(r["Id"]),
                    SerialNumber = r["SerialNumber"].ToString(),
                    CarrierCode = r["CarrierCode"].ToString(),
                    Carrier3Code = r["Carrier3Code"].ToString(),
                    Code = r["Code"].ToString(),
                    CarrierName = r["CarrierName"].ToString(),
                    PercentRecovered = Convert.ToInt32(r["PercentRecovered"]),
                    FlightTotalUlds = Convert.ToInt32(r["FlightTotalUlds"]),
                    FlightRecoveredUlds = Convert.ToInt32(r["FlightRecoveredUlds"]),
                    FlightPercentRecovered = Convert.ToInt32(r["FlightPercentRecovered"]),
                    TotalReceivedCount = Convert.ToInt32(r["TotalReceivedCount"]),
                    TotalUnitCount = Convert.ToInt32(r["TotalUnitCount"]),
                })).ToList();
        }

        public static List<SnapshotConditions> GetConditionItems()
        {
            return DatabaseManager.Fill<IEnumerable<SnapshotConditions>>("GetConditionLists", null, dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new SnapshotConditions
                {
                    Id = Convert.ToInt32(r["Id"]),
                    ConditionName = r["Condition"].ToString(),
                    IsDefault = Convert.ToBoolean(r["IsDefault"])
                })).ToList();
        }

        public static void FinalizeRecoverFlight(long flightId, long taskId, string userName)
        {
            DatabaseManager.ExecuteScalar("FinalizeRecoverFlight", new[] 
            {
                new KeyValuePair<string, object>("@FlightId", flightId),
                new KeyValuePair<string, object>("@TaskId", taskId),
                new KeyValuePair<string, object>("@CurrentUser", userName)                
            });
        }

        public static void ReopenRecoverFlight(long flightId, long taskId, string userName)
        {
            DatabaseManager.ExecuteScalar("ReopenRecoverFlight", new[] 
            {
                new KeyValuePair<string, object>("@FlightId", flightId),
                new KeyValuePair<string, object>("@TaskId", taskId),
                new KeyValuePair<string, object>("@CurrentUser", userName)                
            });
        }

        public static List<AccountModel> GetAccountList()
        {
            return DatabaseManager.Fill<IEnumerable<AccountModel>>("GetAccounts", null, dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new AccountModel
                {
                    Id = Convert.ToInt32(r["Id"]),
                    AccountName = r["AccountName"].ToString(),
                    AccountCode = r["AccountCode"].ToString(),
                    AccountLogoPath = r["AccountLogoPath"].ToString()
                })).ToList();
        }

        public static List<OnhandItem> GetOnhandList(string userId, int rowPerPage, int pageNumber, bool isComplete, string filter, DateTime? dateFilter)
        {
            return DatabaseManager.Fill<IEnumerable<OnhandItem>>("GetOnhands",
                new[] { 
                new KeyValuePair<string, object>("@UserId", userId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),
                new KeyValuePair<string, object>("@IsComplete", isComplete),
                new KeyValuePair<string, object>("@Filter", filter),
                new KeyValuePair<string, object>("@DateFilter", dateFilter)
                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new OnhandItem
                {
                    OnhandId = Convert.ToInt64(r["OnhandId"]),
                    OnhandNumber = r["OnhandNumber"].ToString(),
                    TruckerPro = r["PRO"].ToString(),
                    TotalPieces = Convert.ToInt32(r["TTLPCS"]),
                    ReceivedPieces = Convert.ToInt32(r["RCVPCS"]),
                    CarrierName = r["Carrier"].ToString(),
                    StatusImagePath = r["StatusImagePath"].ToString(),
                    AccountCode = r["AccountCode"].ToString(),
                    StagedLocation = r["StageLocation"].ToString(),
                    AccountId = Convert.ToInt32(r["AccountId"]),
                    TaskId = Convert.ToInt64(r["TaskId"]),
                    ReceivedBy = r["RCVBY"].ToString(),
                    ReceivedDate = r["RCVDT"].ToString(),
                    TotalWeight = Convert.ToSingle(r["TTLWT"])

                })).ToList();
        }

        public static string SaveOnhandReceipt(int accountId, string truckerPro, int carrierId, long driverId,
            int idType1, bool matchingPhoto1, int? idType2, bool? matchingPhoto2, int totalPieces,
            float totalWeight, int uomId, int receivedPieces, string userName, int warehouseId)
        {
            return DatabaseManager.ExecuteScalar1("SaveOnhandReceipt", new[] 
            {
                new KeyValuePair<string, object>("@AccountId", accountId),
                new KeyValuePair<string, object>("@TruckerPro", truckerPro),
                new KeyValuePair<string, object>("@CarrierId", carrierId),
                new KeyValuePair<string, object>("@DriverId", driverId),
                new KeyValuePair<string, object>("@DriverID1TypeId", idType1),
                new KeyValuePair<string, object>("@MatchingPhoto1", matchingPhoto1),
                new KeyValuePair<string, object>("@DriverID2TypeId", idType2),
                new KeyValuePair<string, object>("@MatchingPhoto2", matchingPhoto2),
                new KeyValuePair<string, object>("@TotalPieces", totalPieces),  
                new KeyValuePair<string, object>("@TotalWeight", totalWeight),
                new KeyValuePair<string, object>("@WeightUOMId", uomId),
                new KeyValuePair<string, object>("@ReceivedPieces", receivedPieces),
                new KeyValuePair<string, object>("@UserName", userName),
                new KeyValuePair<string, object>("@WarehouseId ", warehouseId)  
                });

        }

        public static void AddEditOnhandPackage(string UserName, long OnhandId, int Count, int Length, int Width, int Height,
            int DimUOMId, float Weight, int WeightUOMId, int PackageTypeId, bool IsHazmat, long? PackageId
            )
        {

            DatabaseManager.ExecuteScalar("AddEditOnhandPackage", new[] 
            {
                new KeyValuePair<string, object>("@UserName", UserName),
                new KeyValuePair<string, object>("@OnhandId", OnhandId),
                new KeyValuePair<string, object>("@Count", Count),
                new KeyValuePair<string, object>("@Length", Length),
                new KeyValuePair<string, object>("@Width", Width),
                new KeyValuePair<string, object>("@Height", Height),
                new KeyValuePair<string, object>("@DimUOMId", DimUOMId),
                new KeyValuePair<string, object>("@Weight", Weight),
                new KeyValuePair<string, object>("@WeightUOMId", WeightUOMId),
                new KeyValuePair<string, object>("@PackageTypeId", PackageTypeId),
                new KeyValuePair<string, object>("@IsHazmat", IsHazmat),
                new KeyValuePair<string, object>("@PackageId", PackageId)      
         
            });


        }

        public static List<PackageItem> GetOnhandPackages(long OnhandId, string userId, int rowPerPage, int pageNumber, string filter)
        {
            return DatabaseManager.Fill<IEnumerable<PackageItem>>("GetOnhandPackages",
                new[] { 

                new KeyValuePair<string, object>("@UserId", userId),
                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber),               
                new KeyValuePair<string, object>("@OnhandId", OnhandId),
                new KeyValuePair<string, object>("@Filter", filter)
                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new PackageItem
                {
                    PackageNumber = Convert.ToInt32(r["PackageNumber"]),
                    PackageId = Convert.ToInt64(r["PackageId"]),
                    Length = Convert.ToInt32(r["Length"]),
                    Width = Convert.ToInt32(r["Width"]),
                    Height = Convert.ToInt32(r["Height"]),
                    DimUOM = r["DimUOM"].ToString(),
                    Weight = Convert.ToSingle(r["Weight"]),
                    WeightUOM = r["WeightUOM"].ToString(),
                    PackageType = r["PackageType"].ToString(),
                    IsHazmat = Convert.ToBoolean(r["IsHazmat"]),
                    PackageTypeId = Convert.ToInt32(r["PackageTypeId"])

                })).ToList();
        }

        public static void DeleteOnhandPackage(long onhandId, long packageId, string userName)
        {
            DatabaseManager.ExecuteScalar("DeleteOnhandPackage", new[] 
            {
                new KeyValuePair<string, object>("@OnhandId", onhandId),
                new KeyValuePair<string, object>("@PackageId", packageId),
                new KeyValuePair<string, object>("@UserName", userName)                
            });
        }

        public static void EditOnhandDetails(long OnhandId, string ShipperName, string ConsigneeName, int DestinationId, string GoodsDescription, string UserName)
        {

            DatabaseManager.ExecuteScalar("EditOnhandDetails", new[] 
            {

                new KeyValuePair<string, object>("@OnhandId", OnhandId),
                new KeyValuePair<string, object>("@ShipperName", ShipperName),
                new KeyValuePair<string, object>("@ConsigneeName", ConsigneeName),
                new KeyValuePair<string, object>("@DestinationId", DestinationId),
                new KeyValuePair<string, object>("@GoodsDescription", GoodsDescription),
                new KeyValuePair<string, object>("@UserName", UserName)
         
            });


        }

        public static List<DGRChecklistModel> GetDGRCheckListItems(int rowPerPage, int pageNumber)
        {
            return DatabaseManager.Fill<IEnumerable<DGRChecklistModel>>("GetDGRCheckListItems",
                new[] { 

                new KeyValuePair<string, object>("@RowsPerPage", rowPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber)              

                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new DGRChecklistModel
                {
                    ItemId = Convert.ToInt32(r["ItemId"]),
                    ListItem = r["ListItem"].ToString(),
                    ListItemHeader = r["ListItemHeader"].ToString(),
                    ListItemNo = r["ListItemNo"].ToString(),
                    SubLineNo = r["SubLineNo"].ToString(),
                    ValueList = r["ValueList"].ToString()

                })).ToList();
        }

        public static OnhandDetailsModel GetOnhandDetails(long OnhandId)
        {
            return DatabaseManager.Fill<IEnumerable<OnhandDetailsModel>>("GetOnhandDetails",
                new[] { new KeyValuePair<string, object>("@OnhandId", OnhandId) },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new OnhandDetailsModel
                {
                    OnhandId = Convert.ToInt64(r["OnhandId"]),
                    OnhandNumber = r["OnhandNumber"].ToString(),
                    Shipper = r["Shipper"].ToString(),
                    Consignee = r["Consignee"].ToString(),
                    GoodsDesc = r["GoodsDesc"].ToString(),
                    DestinationCode = r["DestinationCode"].ToString(),
                    DestinationId = Convert.ToInt32(r["DestinationId"])

                })).First();
        }


        public static IEnumerable<ShipmentUnitType> GetShipmentUnitTypes()
        {
            return DatabaseManager.Fill<IEnumerable<ShipmentUnitType>>("GetShipmentUnitTypes", null,
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new ShipmentUnitType
                {
                    Id = Convert.ToInt64(r["Id"]),
                    Code = r["Code"].ToString()
                }));
        }

        public static void AddUnManifestedUls(UnManifestedUldModel model)
        {
            DatabaseManager.ExecuteScalar("AddUnManifestedULD", new[] 
            {
                new KeyValuePair<string, object>("@FlightManifestId", model.FlightManifestId),
                new KeyValuePair<string, object>("@SerialNumber", model.SerialNumber),
                new KeyValuePair<string, object>("@TotalPieces", model.TotalPieces),
                new KeyValuePair<string, object>("@UnitTypeId", model.UnitTypeId),
                new KeyValuePair<string, object>("@UserName", model.UserName),
                new KeyValuePair<string, object>("@Weight", model.Weight),
                new KeyValuePair<string, object>("@TaskId", model.TaskId)
            });
        }

        public static List<ReferenceItem> GetEntityReferences(long? ReferenceId, int EntityTypeId, long EntityId, int RowsPerPage, int PageNumber, string Filter)
        {
            return DatabaseManager.Fill<IEnumerable<ReferenceItem>>("GetEntityReferences",
                new[] { 

                new KeyValuePair<string, object>("@ReferenceId", ReferenceId),
                new KeyValuePair<string, object>("@EntityTypeId", EntityTypeId),
                new KeyValuePair<string, object>("@EntityId", EntityId),
                new KeyValuePair<string, object>("@RowsPerPage", RowsPerPage),
                new KeyValuePair<string, object>("@PageNumber", PageNumber),
                new KeyValuePair<string, object>("@Filter", Filter)
                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new ReferenceItem
                {
                    ReferenceId = Convert.ToInt64(r["ReferenceId"]),
                    ReferenceTypeId = Convert.ToInt32(r["ReferenceTypeId"]),
                    ReferenceType = r["ReferenceType"].ToString(),
                    UserName = r["UserName"].ToString(),
                    Value = r["Value"].ToString(),

                })).ToList();
        }

        public static void AddEditEntityReferences(NewReferenceModel refModel)            
        {

            DatabaseManager.ExecuteScalar("AddEditEntityReferences", new[] 
            {

                new KeyValuePair<string, object>("@ReferenceId", refModel.ReferenceId),
	            new KeyValuePair<string, object>("@EntityTypeId", refModel.EntityTypeId),
	            new KeyValuePair<string, object>("@EntityId", refModel.EntityId),
	            new KeyValuePair<string, object>("@ReferenceTypeId", refModel.ReferenceTypeId),
	            new KeyValuePair<string, object>("@ReferenceValue", refModel.ReferenceValue),
	            new KeyValuePair<string, object>("@UserName", refModel.UserName)     
         
            });

        }

        public static IEnumerable<ReferenceType> GetReferenceTypes(int? AccountId)
        {
            return DatabaseManager.Fill<IEnumerable<ReferenceType>>("GetReferenceTypes", new[]{
            new KeyValuePair<string, object>("@AccountId", AccountId)},
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new ReferenceType
                {
                    Id = Convert.ToInt32(r["Id"]),
                    Name = r["ReferenceType"].ToString()
                })).ToList();
        }

        public static List<CheckInItem> GetCheckInList(string UserName, long FlightManifestId, long TaskId, int RowsPerPage, int PageNumber, bool IsComplete, string Filter)
        {
            return DatabaseManager.Fill<IEnumerable<CheckInItem>>("GetCheckInList",
                new[] { 
          
                        new KeyValuePair<string, object>("@UserName",UserName),
                        new KeyValuePair<string, object>("@FlightManifestId",FlightManifestId),
                        new KeyValuePair<string, object>("@TaskId",TaskId),
                        new KeyValuePair<string, object>("@RowsPerPage",RowsPerPage),
                        new KeyValuePair<string, object>("@PageNumber",PageNumber),
                        new KeyValuePair<string, object>("@IsComplete",IsComplete),
                        new KeyValuePair<string, object>("@Filter",Filter)

                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new CheckInItem
                {

                    PackageId = Convert.ToInt64(r["PackageId"]),
                    RefNumber = r["RefNumber"].ToString(),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    ReceivedPcs = Convert.ToInt32(r["ReceivedPcs"]),
                    Location = r["Location"].ToString(),
                    StatusId = Convert.ToInt32(r["StatusId"]),
                    StatusName = r["StatusName"].ToString(),
                    ULDNumber = r["ULDNumber"].ToString(),
                    HWBSerialNumber = r["HWBSerialNumber"].ToString()                

                })).ToList();
        }

        public static IEnumerable<PackageType> GetPackageTypes()
        {
            return DatabaseManager.Fill<IEnumerable<PackageType>>("GetPackageTypes", null,
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new PackageType
                {
                    Id = Convert.ToInt32(r["Id"]),
                    Name = r["Name"].ToString(),
                    Code = r["Code"].ToString()
                }));
        }
        public static void DeleteEntityReferences(DeleteEntityReference entityReference)
        {
            DatabaseManager.ExecuteScalar("DeleteEntityReferences", new[] 
            {
                new KeyValuePair<string, object>("@TaskId", entityReference.TaskId),
                new KeyValuePair<string, object>("@ReferenceId", entityReference.ReferenceId),
                new KeyValuePair<string, object>("@UserName", entityReference.UserName)
            });
        }

        public static FlightCheckInSummary GetFlightCheckInSummary(long TaskId)
        {
            return DatabaseManager.Fill<IEnumerable<FlightCheckInSummary>>("GetFlightCheckInSummary",
                new[] { 

                new KeyValuePair<string, object>("@TaskId", TaskId)
                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new FlightCheckInSummary
                {
                    StageLocation = r["StageLocation"].ToString(),
                    StatusName = r["StatusName"].ToString(),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    ReceivedPieces = Convert.ToInt32(r["ReceivedPieces"]),
                    PercentReceived = Convert.ToSingle(r["PercentReceived"])

                })).First();
        }

        public static void CheckInFlightPieces(CheckInPiece cPiece)
        {

            DatabaseManager.ExecuteScalar("CheckInFlightPieces", new[] 
            {
                new KeyValuePair<string, object>("@TaskId", cPiece.TaskId),              
                new KeyValuePair<string, object>("@UserName", cPiece.UserName),
                new KeyValuePair<string, object>("@PackageIds", cPiece.PackageIds)
            });
        }

        public static void UnCheckInFlightPieces(CheckInPiece cPiece)
        {

            DatabaseManager.ExecuteScalar("UnCheckInFlightPieces", new[] 
            {
                new KeyValuePair<string, object>("@TaskId", cPiece.TaskId),              
                new KeyValuePair<string, object>("@UserName", cPiece.UserName),
                new KeyValuePair<string, object>("@PackageIds", cPiece.PackageIds)
            });
        }

        public static IEnumerable<FlightAwb> GetFlightAwbs(long flightManifestId, int rowsPerPage, int pageNumber, string filter)
        {
            return DatabaseManager.Fill<IEnumerable<FlightAwb>>("GetFlightAwbs",
                new[] {                                   
                        new KeyValuePair<string, object>("@FlightManifestId",flightManifestId),                        
                        new KeyValuePair<string, object>("@RowsPerPage",rowsPerPage),
                        new KeyValuePair<string, object>("@PageNumber",pageNumber),                        
                        new KeyValuePair<string, object>("@Filter",filter)
                }
                , dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new FlightAwb
                {
                    AwbNumber = r["AWB"].ToString()                    
                })).ToList();
        }

        public static IEnumerable<History> GetOnhandHistory(long onhandId, string filter, int rowsPerPage, int pageNumber)
        {
            return DatabaseManager.Fill<IEnumerable<History>>("GetOnhandHistory", new[] { 
                new KeyValuePair<string, object>("@OnhandId", onhandId),
                new KeyValuePair<string, object>("@Filter", filter),
                new KeyValuePair<string, object>("@RowsPerPage", rowsPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new History
                {
                    Date = Convert.ToDateTime(r["Date"]).ToString("MMM-dd HH:mm"),
                    Description = r["Description"].ToString(),
                    Reference = r["Reference"].ToString(),
                    UserName = r["UserName"].ToString()
                })).ToList();
        }

        public static IEnumerable<FlightHwb> GetFlightHwbs(long flightManifestId, string filter, int rowsPerPage, int pageNumber)
        {
            return DatabaseManager.Fill<IEnumerable<FlightHwb>>("GetFlightHwbs", new[] { 
                new KeyValuePair<string, object>("@FlightManifestId", flightManifestId),
                new KeyValuePair<string, object>("@Filter", filter),
                new KeyValuePair<string, object>("@RowsPerPage", rowsPerPage),
                new KeyValuePair<string, object>("@PageNumber", pageNumber)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new FlightHwb
                {
                    Hwb = r["HWB"].ToString(),
                    TotalPieces = Convert.ToInt32(r["TotalPieces"]),
                    ServiceType = r["ServiceType"].ToString(),
                    Destination = r["Destination"].ToString()
                })).ToList();
        }

        public static void SaveDGRCheckListResults(SaveDGRChecklistItem dgrItem)
        {

            DatabaseManager.ExecuteScalar("SaveDGRCheckListResults", new[] 
            {

                new KeyValuePair<string, object>("@EntityTypeId",dgrItem.EntityTypeId),
                new KeyValuePair<string, object>("@EntityId", dgrItem.EntityId),
                new KeyValuePair<string, object>("@Value", dgrItem.Value),
                new KeyValuePair<string, object>("@UserName",dgrItem.UserName),
                new KeyValuePair<string, object>("@TaskId",dgrItem.TaskId)

            });
        }

        public static IEnumerable<DGRChecklistResult> GetDGRCheckListResults(int EntityTypeId, long EntityId, int RowsPerPage, int PageNumber, long TaskId)
        {
            return DatabaseManager.Fill<IEnumerable<DGRChecklistResult>>("GetDGRCheckListResults", new[] { 
                new KeyValuePair<string, object>("@EntityTypeId", EntityTypeId),
                new KeyValuePair<string, object>("@EntityId ", EntityId),
                new KeyValuePair<string, object>("@RowsPerPage", RowsPerPage),
                new KeyValuePair<string, object>("@PageNumber", PageNumber),
                new KeyValuePair<string, object>("@TaskId", TaskId)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new DGRChecklistResult
                {

                    ItemId = Convert.ToInt32(r["ItemId"]),
                    ListItem = r["ListItem"].ToString(),
                    ListItemHeader = r["ListItemHeader"].ToString(),
                    ListItemNo = r["ListItemNo"].ToString(),
                    ResultValue = r["ResultValue"].ToString(),
                    SubLineNo = r["SubLineNo"].ToString(),
                    UserName = r["UserName"].ToString(),
                    ValueList = r["ValueList"].ToString(),
                    ListItemComment = r["Comment"].ToString()

                })).ToList();
        }

        public static void FinalizeDGRChecklist(FinalizeDGRChecklistModel dgrItem)
        {

            DatabaseManager.ExecuteScalar("FinalizeDGRChecklist", new[] 
            {

                new KeyValuePair<string, object>("@EntityTypeID",dgrItem.EntityTypeId),
                new KeyValuePair<string, object>("@EntityId", dgrItem.EntityId),
                new KeyValuePair<string, object>("@Comments", dgrItem.Comments),
                new KeyValuePair<string, object>("@Place", dgrItem.Place),
                new KeyValuePair<string, object>("@SignaturePath", dgrItem.SignaturePath),
                new KeyValuePair<string, object>("@UserName",dgrItem.UserName),
                new KeyValuePair<string, object>("@TaskId",dgrItem.TaskId)

            });
        }

        public static DGRChecklistStatus CheckForDGRCheckListStatus(int EntityTypeId, long EntityId, long TaskId, string UserName)
        {
            return DatabaseManager.Fill<IEnumerable<DGRChecklistStatus>>("CheckForDGRCheckListStatus", new[] { 
                new KeyValuePair<string, object>("@EntityTypeID", EntityTypeId),
                new KeyValuePair<string, object>("@EntityId", EntityId),
                new KeyValuePair<string, object>("@UserName",UserName),
                new KeyValuePair<string, object>("@TaskId",TaskId)
            },
                dataset => dataset.Tables[0].Rows.OfType<DataRow>().
                Select(r => new DGRChecklistStatus
                {
                    TotalQuestions = Convert.ToInt32(r["TotalQuestions"]),
                    Answered = Convert.ToInt32(r["Answered"]),
                    IsChecklistFinalized = Convert.ToBoolean(r["IsChecklistFinalized"])
                })).First();
        }

        public static string CheckOnhandForHazmat(long OnhandId)
        {
            return DatabaseManager.ExecuteScalar1("CheckOnhandForHazmat",
                new[] {new KeyValuePair<string, object>("@OnhandId", OnhandId)});
        }

        public static void SaveDGRCheckListItemComment(DGRChecklistComment dgrComment)
        {

            DatabaseManager.ExecuteScalar("SaveDGRCheckListItemComment", new[] 
            {

                new KeyValuePair<string, object>("@EntityTypeId",dgrComment.EntityTypeId),
                new KeyValuePair<string, object>("@EntityId", dgrComment.EntityId),
                new KeyValuePair<string, object>("@CheckListItemId", dgrComment.CheckListItemId),
                new KeyValuePair<string, object>("@Comment", dgrComment.Comment),
                new KeyValuePair<string, object>("@UserName",dgrComment.UserName),
                new KeyValuePair<string, object>("@TaskId",dgrComment.TaskId)

            });
        }

        public static void SetOnhandStagingLocation(OnhandStageLocation oLocation)
        {

            DatabaseManager.ExecuteScalar("SetOnhandStagingLocation", new[] 
            {

                new KeyValuePair<string, object>("@UserName",oLocation.UserName),
                new KeyValuePair<string, object>("@OnhandId",oLocation.OnhandId),
                new KeyValuePair<string, object>("@LocationId", oLocation.LocationId),
                new KeyValuePair<string, object>("@TaskId", oLocation.TaskId)

            });
        }

        public static void FinalizeOnhand(FinalizeOnhand fOnhand)
        {

            DatabaseManager.ExecuteScalar("FinalizeOnhand", new[] 
            {

                new KeyValuePair<string, object>("@UserName",fOnhand.UserName),
                new KeyValuePair<string, object>("@OnhandId",fOnhand.OnhandId),
                new KeyValuePair<string, object>("@TaskId", fOnhand.TaskId)

            });
        }

        public static string GetDGRCheckListItemComment(int EntityTypeId, long EntityId, long TaskId, int CheckListItemId)
        {

            return DatabaseManager.ExecuteScalar1("GetDGRCheckListItemComment", new[] { 
                new KeyValuePair<string, object>("@EntityTypeID", EntityTypeId),
                new KeyValuePair<string, object>("@EntityId", EntityId),
                new KeyValuePair<string, object>("@CheckListItemId",CheckListItemId),
                new KeyValuePair<string, object>("@TaskId",TaskId)
            });
        }

        
    }
}