﻿using GHAWebAPI.Filters;
using System.Web;
using System.Web.Mvc;

namespace GHAWebAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}