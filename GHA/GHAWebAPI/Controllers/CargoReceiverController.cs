﻿using GHA.Bll.DataProviders;
using GHA.BLL.DataProviders.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GHAWebAPI.Controllers
{
    public class CargoReceiverController : ApiController
    {
        // GET api/cargoreceiver
        public IEnumerable<ReceiverFlight> Get()
        {
            return CargoReceiver.GetFlights();
        }

        public IEnumerable<PutAwayItem> GetPutAwayList(long flightId, int rowPerPage, int pageNumber, string refNumber)
        {
            return CargoReceiver.GetPutAwayList(flightId, rowPerPage, pageNumber, refNumber);
        }
    }
}