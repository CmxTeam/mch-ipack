﻿using GHAWebAPI.Filters;
using GHAWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;

namespace GHAWebAPI.Controllers.API
{
    public class AuthController : ApiController
    {
        [HttpPost]
        [ValidateModel]
        public AuthResult Authenticate(UserLoginModel model)
        {
            AuthResult authResult = new AuthResult();
            var tmpUser = Membership.GetUser(model.Username);
            if (tmpUser == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            authResult.IsUserLocked = tmpUser.IsLockedOut;
            authResult.IsUserApproved = tmpUser.IsApproved;

            if (Membership.ValidateUser(model.Username, model.Password))
            {
                authResult.Username = tmpUser.UserName;
                authResult.IsUserValid = true;
            }
            return authResult;
        }

        [HttpPost]
        [ValidateModel]
        public bool ChangePassword(ChangePassModel model)
        {
            bool result = false;
            var authResult = this.Authenticate(new UserLoginModel() { Username = model.Username, Password = model.OldPassword });
            if (authResult != null && authResult.IsUserValid)
            {
                //tmpUser cannot be null, it's got authenticated above
                MembershipUser tmpUser = Membership.GetUser(model.Username, false);
                result = tmpUser.ChangePassword(model.OldPassword, model.NewPassword);
            }
            return result;
        }

        [HttpPost]
        [ValidateModel]
        public bool ChangePasswordSecret(ChangePasswordSecretModel model) 
        {
            bool changePasswordResult = false;
            var authResult = this.Authenticate(new UserLoginModel() { Username = model.Username, Password = model.Password });
            if (authResult != null && authResult.IsUserValid) 
            {
                MembershipUser tmpUser = Membership.GetUser(model.Username);
                changePasswordResult = tmpUser.ChangePasswordQuestionAndAnswer(model.Password, model.PasswordQuestion, model.PasswordAnswer);
            }
            return changePasswordResult;
        }

        [HttpPost]
        [ValidateModel]
        public bool UnlockUser(UserLoginModel model) 
        {
            bool unlockResult = false;
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, ModelState));
            }
            var authResult = this.Authenticate(model);
            if (authResult != null && authResult.IsUserValid) 
            {
                var tmpUser = Membership.GetUser(model.Username);
                unlockResult = tmpUser.UnlockUser();
            }
            return unlockResult;
        }

        [HttpPost]
        [ValidateModel]
        public RegResult Register(UserRegistrationModel model)
        {
            RegResult tmpResult = new RegResult();
            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest,ModelState));
            }
            MembershipCreateStatus tmpCreationStatus = MembershipCreateStatus.Success;
            MembershipUser memUser = Membership.CreateUser(model.Username, model.Password, model.Email, model.PasswordQuestion, model.PasswordAnswer, model.IsApproved, out tmpCreationStatus);
            switch (tmpCreationStatus)
            {
                case MembershipCreateStatus.DuplicateEmail:
                    tmpResult.DuplicateEmail = true;
                    break;
                case MembershipCreateStatus.DuplicateUserName:
                    tmpResult.DuplicateUsername = true;
                    break;
                case MembershipCreateStatus.InvalidAnswer:
                    tmpResult.InvalidPasswordAnswer = true;
                    break;
                case MembershipCreateStatus.InvalidEmail:
                    tmpResult.InvalidEmail = true;
                    break;
                case MembershipCreateStatus.InvalidPassword:
                    tmpResult.InvalidPassword = true;
                    break;
                case MembershipCreateStatus.InvalidQuestion:
                    tmpResult.InvalidPasswordQuestion = true;
                    break;
                case MembershipCreateStatus.InvalidUserName:
                    tmpResult.InvalidUsername = true;
                    break;
                case MembershipCreateStatus.Success:
                    tmpResult = new RegResult() { Username = memUser.UserName };
                    break;
                case MembershipCreateStatus.UserRejected:
                case MembershipCreateStatus.ProviderError:
                case MembershipCreateStatus.InvalidProviderUserKey:
                case MembershipCreateStatus.DuplicateProviderUserKey:
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.InternalServerError, "User rejected, unspecified server error."));
                default:
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest));
            }
            return tmpResult;
        }
    }
}
