﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GHAWebAPI.Models
{
    public class RegResult
    {
        public string Username { get; set; }

        public bool DuplicateEmail { get; set; }

        public bool InvalidUsername { get; set; }

        public bool InvalidPasswordQuestion { get; set; }

        public bool InvalidPassword { get; set; }

        public bool InvalidEmail { get; set; }

        public bool InvalidPasswordAnswer { get; set; }

        public bool DuplicateUsername { get; set; }
    }
}