﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GHAWebAPI.Models
{
    public class ChangePassModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }

    public class ChangePasswordSecretModel
    {
        [Required]
        public string Password { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string PasswordQuestion { get; set; }
        [Required]
        public string PasswordAnswer { get; set; }
    }
}