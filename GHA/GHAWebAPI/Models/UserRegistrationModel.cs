﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GHAWebAPI.Models
{
    public class UserRegistrationModel
    {
        
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string Email { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }

        private bool isApproved = true;
        public bool IsApproved
        {
            get { return isApproved; }
            set { isApproved = value; }
        }

    }
}