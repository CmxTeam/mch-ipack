﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GHAWebAPI.Models
{
    public class AuthResult
    {
        public string Username { get; set; }
        public bool IsUserLocked { get; set; }
        public bool IsUserApproved { get; set; }
        public bool IsUserValid { get; set; }
    }
}