﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Security;

namespace GHAMobileService
{
    public class AuthenticationService : IAuthenticationService
    {

        public bool Authenticate(string username, string password)
        {
            return Membership.ValidateUser(username, password);
        }

        public bool ChangePassword(string username , string oldPassword, string newPassword)
        {
            MembershipUser tmpUser = Membership.GetUser(username, false);
            return tmpUser.ChangePassword(oldPassword, newPassword);
        }

        public string Register(string username, string password)
        {
            DTO.User tmpUser = null;

            MembershipCreateStatus tmpCreationStatus = MembershipCreateStatus.Success;
            MembershipUser memUser = Membership.CreateUser(username, password, null, null, null, true, out tmpCreationStatus);
            if (tmpCreationStatus == MembershipCreateStatus.Success)
            {
                tmpUser = new DTO.User();
                tmpUser.Username = memUser.UserName;
            }
            else 
            {
                return "Failed";
            }

           return JsonConvert.SerializeObject(tmpUser);  
        }
    }
}
