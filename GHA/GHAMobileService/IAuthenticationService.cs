﻿using GHAMobileService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace GHAMobileService
{
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        bool Authenticate(string username, string password);
        [OperationContract]
        bool ChangePassword(string username, string oldPassword, string newPassword);
        [OperationContract]
        string Register(string username, string password);
    }
}
