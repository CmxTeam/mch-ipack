﻿using System;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using Microsoft.LightSwitch;
using Microsoft.LightSwitch.Framework.Client;
using Microsoft.LightSwitch.Presentation;
using Microsoft.LightSwitch.Presentation.Extensions;
using System.Windows;
using System.Windows.Controls;

namespace LightSwitchApplication
{
    public partial class DischargeCharges
    {
        partial void DischargeCharges_Created()
        {
            this.FindControl("grid").ControlAvailable += HideHeaderForGrid;

        }

        private void HideHeaderForGrid(object sender, ControlAvailableEventArgs e)
        {
            this.FindControl("grid").ControlAvailable -= HideHeaderForGrid;

            DataGrid aGrid = e.Control as DataGrid;

            Grid bGrid = (Grid)((FrameworkElement)((FrameworkElement)(((FrameworkElement)aGrid).Parent)).Parent).Parent;

            bGrid.Children[0].Visibility = System.Windows.Visibility.Collapsed;
            //bGrid.Children[1].Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
