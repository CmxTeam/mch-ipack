﻿using System;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using Microsoft.LightSwitch;
using Microsoft.LightSwitch.Framework.Client;
using Microsoft.LightSwitch.Presentation;
using Microsoft.LightSwitch.Presentation.Extensions;
using System.Windows;
using System.Windows.Controls;

namespace LightSwitchApplication
{
    public partial class TableMenu
    {
        partial void ShowTableView_Execute()
        {
            TableList t = this.TableLists.SelectedItem;
            var screenName = this.Application.GetType().GetMethod("Show" + t.ScreenName.Trim());
            if(screenName != null)
            {
                screenName.Invoke(this.Application, null);
            }
            else
            {
                this.ShowMessageBox(t.ScreenName.Trim() + " Not Found");
            }

        }

        partial void TableMenu_Created()
        {
            this.FindControl("grid").ControlAvailable += HideHeaderForGrid;
        }

        private void HideHeaderForGrid(object sender, ControlAvailableEventArgs e)
        {
            this.FindControl("grid").ControlAvailable -= HideHeaderForGrid;

            DataGrid aGrid = e.Control as DataGrid;

            Grid bGrid = (Grid)((FrameworkElement)((FrameworkElement)(((FrameworkElement)aGrid).Parent)).Parent).Parent;

            bGrid.Children[0].Visibility = System.Windows.Visibility.Collapsed;
            //bGrid.Children[1].Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
