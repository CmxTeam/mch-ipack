﻿using LightSwitchApplication.Helpers;
using System.Collections.Generic;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for CheckForAvailHWB
    /// </summary>
    public class CheckForAvailHWB : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tFltId = context.Request["FltId"];
            var tAwbId = context.Request["AwbId"];
            
            var result = DatabaseManager.ExecuteScalar("CheckForAvailHWB", new[] 
            {
                new KeyValuePair<string, object>("@FlightManifestId", tFltId),
                new KeyValuePair<string, object>("@AwbId", tAwbId)
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}