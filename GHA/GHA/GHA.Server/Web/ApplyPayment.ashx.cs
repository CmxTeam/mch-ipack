﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for ApplyPayment
    /// </summary>
    public class ApplyPayment : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            #region Variables
            int tmpRctr = 0;
            decimal pmtAmtLeft = 0;
            long dischargeId = 0;
            long[] rId;
            List<DischargeCharge> cUpdatedList = new List<DischargeCharge>();
            #endregion

            #region Get Parameters
            var chargeList = context.Request["cList"];
            var sDischarge = context.Request["dId"];
            var applyAmt = context.Request["ttlAmt"];
            var tPayTypes = context.Request["pTypes"];
            #endregion

            //Parse Payment Amount to Decimal
            bool success1 = decimal.TryParse(applyAmt, out pmtAmtLeft);
            
           
            if (success1)
            {
                //Parse Discharge Id to Long
                bool success2 = long.TryParse(sDischarge, out dischargeId);

                if (success2)
                {
                    //Split Charge Ids from string to string array
                    var itemList = chargeList.Split(',');
                    var pList = tPayTypes.Split(',');
                    //Set Long array to item list count
                    rId = new long[itemList.Count()];

                    //Parse Charge Ids to string
                    foreach (var item in itemList)
                    {
                        long tmpItem = 0;
                        bool success = long.TryParse(item, out tmpItem);
                        if (success)
                        {
                            rId[tmpRctr] = tmpItem;
                           
                        }
                        tmpRctr++;
                    }

                    using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
                    {

                        #region Get Queries

                        //Get Charge List
                        var chList = from x in contextApi.DataWorkspace.WFSDBData.DischargeCharges.GetQuery().Execute()
                                     where rId.Contains(x.Id)
                                     select x;
                        //Get Discharge
                        var dItem = (from d in contextApi.DataWorkspace.WFSDBData.Discharges.GetQuery().Execute()
                                     where d.Id == dischargeId
                                     select d).FirstOrDefault();

                         PaymentType payType = (from p in contextApi.DataWorkspace.WFSDBData.PaymentTypes.GetQuery().Execute()
                                       where p.Id == 5
                                       select p).FirstOrDefault();

                        if(pList.Count() == 1)
                        {
                            int tmpId = 0;
                            bool succ = int.TryParse(pList[0], out tmpId);
                            if(succ)
                            {
                                payType = (from p in contextApi.DataWorkspace.WFSDBData.PaymentTypes.GetQuery().Execute()
                                           where p.Id == tmpId
                                           select p).FirstOrDefault();
                            }
                        }

                        #endregion

                        #region Add paid amount to total paid amount for discharge

                        if (dItem.TotalAmountPaid != null)
                        {
                            dItem.TotalAmountPaid += pmtAmtLeft;
                        }
                        else
                        {
                            dItem.TotalAmountPaid = pmtAmtLeft;
                        }

                        #endregion

                        #region Apply paid amount for each charge until paid amount is 0 or return remaining if paid more than charge total.

                        
                        foreach (DischargeCharge dc in chList)
                        {
                            if (pmtAmtLeft > 0)
                            {
                                if (dc.ChargePaid == null)
                                {
                                    if (pmtAmtLeft == dc.ChargeAmount)
                                    {
                                        dc.ChargePaid = dc.ChargeAmount;
                                        pmtAmtLeft = 0;
                                        dc.PaymentType = payType;
                                    }
                                    else if (pmtAmtLeft < dc.ChargeAmount)
                                    {

                                        dc.ChargePaid = pmtAmtLeft;
                                        pmtAmtLeft = 0;
                                        dc.PaymentType = payType;

                                    }
                                    else
                                    {

                                        dc.ChargePaid = dc.ChargeAmount;
                                        pmtAmtLeft = pmtAmtLeft - (decimal)dc.ChargeAmount;
                                        dc.PaymentType = payType;

                                    }
                                }
                                else
                                {
                                    decimal amtPending = ((decimal)dc.ChargeAmount - (decimal)dc.ChargePaid);
                                    
                                    if (pmtAmtLeft == amtPending)
                                    {
                                        dc.ChargePaid = amtPending;
                                        pmtAmtLeft = 0;
                                        dc.PaymentType = payType;
                                    }
                                    else if (pmtAmtLeft < amtPending)
                                    {
                                        dc.ChargePaid = pmtAmtLeft;
                                        pmtAmtLeft = 0;
                                        dc.PaymentType = payType;
                                    }
                                    else
                                    {
                                        dc.ChargePaid = amtPending;
                                        pmtAmtLeft = pmtAmtLeft - amtPending;
                                        dc.PaymentType = payType;
                                    }

                                }
                            }

                        }

                        #endregion

                        #region Save changes
                        contextApi.DataWorkspace.WFSDBData.SaveChanges();
                        #endregion

                        context.Response.ContentType = "text/plain";
                        context.Response.Write("1");
                    }
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}