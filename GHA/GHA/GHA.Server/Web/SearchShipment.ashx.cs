﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for SearchShipment
    /// </summary>
    public class SearchShipment : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var carrier = context.Request["carrier"];
            var awb = context.Request["awb"];
            var hwb = context.Request["hwb"];
            var uldId = context.Request["uldId"];

            var dataset = Helpers.DatabaseManager.Fill("SearchShipment", new[] 
            { 
                new KeyValuePair<string, object>("@Carrier", carrier),
                new KeyValuePair<string, object>("@AwbNumber", awb),
                new KeyValuePair<string, object>("@HwbNumber", hwb),
                new KeyValuePair<string, object>("@UldId", uldId)
            });

            var result = dataset.Tables.OfType<DataTable>().
                SelectMany<DataTable, DataRow>(t => t.Rows.OfType<DataRow>()).
                Select(r => r[0].ToString()).ToList();

            var json = result.Count == 0 ? new { sId = "", hId = "" } : result.Count == 1 ? 
                new { sId = result[0], hId = "" } : new { sId = result[0], hId = result[1] };            
            var jResult = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            context.Response.ContentType = "text/plain";
            context.Response.Write(jResult);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}