﻿using LightSwitchApplication.Helpers;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetTask
    /// </summary>
    public class GetTask : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region Parameters

            var tTaskTypeId = context.Request["tTypeId"];
            var tEntityTypeId = context.Request["eTypeId"];
            var tEntityId = context.Request["eId"];

            #endregion

            var value = DatabaseManager.GetValue(string.Format("Select top 1 Id from Tasks where EntityTypeid = {0} and TaskTypeId = {1} and EntityId = {2}", tEntityTypeId, tTaskTypeId, tEntityId), "Id", "0");

            
            context.Response.ContentType = "text/plain";
            context.Response.Write(value);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}