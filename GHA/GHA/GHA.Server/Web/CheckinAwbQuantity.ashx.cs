﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for CheckinAwbQuantity
    /// </summary>
    public class CheckinAwbQuantity : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var awbId = context.Request["awbId"];
            var quantity = context.Request["quantity"];
            var userName = context.Request["userName"];
            var taskId = context.Request["taskId"];

            Helpers.DatabaseManager.ExecuteScalar("CheckinAwbQuantity", new []
            {
                new KeyValuePair<string, object>("@AwbId", awbId),
                new KeyValuePair<string, object>("@Quantity", quantity),
                new KeyValuePair<string, object>("@UserName", userName),
                new KeyValuePair<string, object>("@SelectedTaskId", taskId)                
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}