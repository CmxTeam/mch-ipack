﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for PutAwayPieces
    /// </summary>
    public class PutAwayPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            #region Parameters

            var tmpIsEdit = context.Request["isEdit"];

            var tmpFltId = context.Request["FltId"];
            var tmpHId = context.Request["HwbId"];
            var tmpSLoc = context.Request["hLoc"];
            var tmpPLoc = context.Request["hLocPrev"];

            var tmpQty = context.Request["pQty"];

            #endregion

            #region Variables

            //bool success;
            long fltId;
            long hwbId;
            string cUser;
            DateTime tStamp;
            int sLocId;
            int pLocId;
            int pQty;
            int uRemQty;
            int pRemQty;
            int mRemQty;
            int sCtr = 0;

            //int ttlPutAwayPieces = 0;
            //int ttlRemPutAwayPieces = 0;
            //int ttlPieces = 0;
            //double percentPutAway = 0;

            string putAwayDetails = "";

            #endregion

            #region Cast Entity Id's

            if (long.TryParse(tmpFltId, out fltId))
                sCtr++;

            if (long.TryParse(tmpHId, out hwbId))
                sCtr++;

            if (int.TryParse(tmpSLoc, out sLocId))
                sCtr++;

            if (int.TryParse(tmpQty, out pQty))
                sCtr++;

            if (tmpIsEdit == "1" && int.TryParse(tmpPLoc, out pLocId))
                sCtr++;

            #endregion

            #region Updating Current Disposition Record

            if (tmpIsEdit == "1")
            {
                if (sCtr == 5)
                {
                    using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("1");
                    }
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("0");
                }
            }

            #endregion

            #region Inserting New Disposition Record

            else
            {
                if (sCtr == 4)
                {
                    using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
                    {

                        #region Get Queries


                        var tHWB = contextApi.DataWorkspace.WFSDBData.HWBs.OfType<HWB>().FirstOrDefault(h => h.Id == hwbId);
                        var tItem = contextApi.DataWorkspace.WFSDBData.AWBs_HWBs.OfType<AWBs_HWB>().FirstOrDefault(b => b.HWB.Id == hwbId);

                        AWB tAWB = null;
                        if (tItem != null)
                            tAWB = tItem.AWB;

                        var tFlt = contextApi.DataWorkspace.WFSDBData.FlightManifests.OfType<FlightManifest>().FirstOrDefault(f => f.Id == fltId);
                        var tLoc = contextApi.DataWorkspace.WFSDBData.WarehouseLocations.OfType<WarehouseLocation>().FirstOrDefault(l => l.Id == sLocId);
                        var tDisp = contextApi.DataWorkspace.WFSDBData.Dispositions.OfType<Disposition>().FirstOrDefault(d => d.HWB.Id == hwbId);
                        var mList = contextApi.DataWorkspace.WFSDBData.ManifestDetails.OfType<ManifestDetail>().Where(m => m.FlightManifest.Id == fltId && m.HWB.Id == hwbId).ToList();
                        var maList = contextApi.DataWorkspace.WFSDBData.ManifestAWBDetails.OfType<ManifestAWBDetail>().Where(ma => ma.AWB.Id == tAWB.Id && ma.FlightManifest.Id == fltId).ToList();
                        var sList = contextApi.DataWorkspace.WFSDBData.Statuses.OfType<Status>().ToList();
                        var tList = contextApi.DataWorkspace.WFSDBData.EntityTypes.OfType<EntityType>().ToList();
                        var uList = contextApi.DataWorkspace.WFSDBData.TaskTypes.OfType<TaskType>().FirstOrDefault(u => u.Id == 1);
                        var aList = contextApi.DataWorkspace.WFSDBData.TransactionActions.OfType<TransactionAction>().ToList();

                        #endregion

                        if (tHWB != null && tAWB != null && tFlt != null && tLoc != null && tDisp != null &&
                            maList != null && sList != null && tList != null && uList != null && aList != null)
                        {

                            #region Get User and Create Timestamp

                            cUser = contextApi.Application.User.PersonId;
                            tStamp = DateTime.UtcNow;
                            uRemQty = pQty;
                            pRemQty = pQty;
                            mRemQty = pQty;

                            #endregion

                            #region Update HWB Put Away Pieces

                            tHWB.PutAwayPieces += pQty;
                            tHWB.PercentPutAway = (((double)tHWB.PutAwayPieces / (double)tHWB.Pieces) * (double)100);

                            Disposition nDisposition = new Disposition()
                            {
                                HWB = tHWB,
                                UserName = cUser,
                                Timestamp = tStamp,
                                WarehouseLocation = tLoc,
                                Count = pQty
                            };

                            #endregion

                            #region Update AWB Put Away Pieces

                            tAWB.PutAwayPieces += pQty;
                            tAWB.PercentPutAway = (((double)tAWB.PutAwayPieces / (double)tAWB.TotalPieces) * (double)100);

                            #endregion

                            #region Update FLT Put Away Pieces

                            tFlt.PutAwayPieces += pQty;
                            tFlt.PercentPutAway = (((double)tFlt.PutAwayPieces / (double)tFlt.TotalPieces) * (double)100);

                            if (!tFlt.PercentRecovered.HasValue)
                            {
                                tFlt.PercentRecovered = 0;
                            }

                            if (!tFlt.PercentReceived.HasValue)
                            {
                                tFlt.PercentRecovered = 0;
                            }

                            if (!tFlt.PercentPutAway.HasValue)
                            {
                                tFlt.PercentPutAway = 0;
                            }

                            tFlt.PercentOverall = Math.Round(((((double)tFlt.PercentRecovered + (double)tFlt.PercentReceived + (double)tFlt.PercentPutAway) / (double)300) * (double)100), 0);

                            #endregion

                            #region Update ULD and ManifestDetails Put Away Pieces

                            //List<ULD> UldList = new List<ULD>();
                            //List<int> UldCount = new List<int>();

                            //List<ManifestDetail> MList = new List<ManifestDetail>();
                            //List<int> MCount = new List<int>();

                            //foreach (ManifestDetail md in mList)
                            //{
                            //    #region Update ULD Put Away Pieces

                            //    if (uRemQty != 0)
                            //    {
                            //        ULD tUld = md.ULD;
                            //        //if (tUld.TotalPieces > tUld.PutAwayPieces)
                            //        //{
                            //            if (tUld.PutAwayPieces != null && tUld.TotalPieces != null && tUld.TotalPieces > 0)
                            //            {
                            //                int remQty = (int)tUld.TotalPieces - (int)tUld.PutAwayPieces;

                            //                if (remQty == uRemQty)
                            //                {
                            //                    UldList.Add(tUld);
                            //                    UldCount.Add(uRemQty);

                            //                    if (tUld.PutAwayPieces != null)
                            //                    {
                            //                        tUld.PutAwayPieces += uRemQty;
                            //                    }
                            //                    else
                            //                    {
                            //                        tUld.PutAwayPieces = uRemQty;
                            //                    }
                            //                    uRemQty = 0;                                             
                            //                }
                            //                else if (remQty > uRemQty)
                            //                {
                            //                    UldList.Add(tUld);
                            //                    UldCount.Add(uRemQty);

                            //                    if (tUld.PutAwayPieces != null)
                            //                    {
                            //                        tUld.PutAwayPieces += uRemQty;
                            //                    }
                            //                    else
                            //                    {
                            //                        tUld.PutAwayPieces = uRemQty;
                            //                    }
                            //                    uRemQty = 0;
                            //                }
                            //                else
                            //                {
                            //                    UldList.Add(tUld);
                            //                    UldCount.Add(remQty);

                            //                    tUld.PutAwayPieces += uRemQty;
                            //                    uRemQty -= remQty;
                            //                }
                            //            }
                            //            else if( tUld.PutAwayPieces == null && tUld.TotalPieces != null && tUld.TotalPieces > 0)
                            //            {
                            //                int remQty = (int)tUld.TotalPieces;

                            //                if (remQty == uRemQty)
                            //                {
                            //                    UldList.Add(tUld);
                            //                    UldCount.Add(uRemQty);

                            //                    tUld.PutAwayPieces = uRemQty;
                            //                    uRemQty = 0; 
                            //                }
                            //                else if (remQty > uRemQty)
                            //                {
                            //                    UldList.Add(tUld);
                            //                    UldCount.Add(uRemQty);

                            //                    tUld.PutAwayPieces = uRemQty;
                            //                    uRemQty = 0;
                            //                }
                            //                else
                            //                {
                            //                    UldList.Add(tUld);
                            //                    UldCount.Add(remQty);

                            //                    tUld.PutAwayPieces = uRemQty;
                            //                    uRemQty -= remQty;
                            //                }
                            //            }

                            //        //}
                            //    }

                            //    #endregion

                            //    #region Update ManifestDetail Put Away Pieces

                            //    if (mRemQty != 0)
                            //    {
                            //            if (md.PutAwayPieces != null && md.LoadCount != null && md.LoadCount > 0)
                            //            {
                            //                int remQty = (int)md.LoadCount - (int)md.PutAwayPieces;

                            //                if (remQty == mRemQty)
                            //                {
                            //                    MList.Add(md);
                            //                    MCount.Add(mRemQty);

                            //                    md.PutAwayPieces += mRemQty;
                            //                    mRemQty = 0;
                            //                }
                            //                else if (remQty > mRemQty)
                            //                {
                            //                    MList.Add(md);
                            //                    MCount.Add(mRemQty);

                            //                    md.PutAwayPieces += mRemQty;
                            //                    mRemQty = 0;
                            //                }
                            //                else
                            //                {
                            //                    MList.Add(md);
                            //                    MCount.Add(remQty);

                            //                    md.PutAwayPieces += mRemQty;
                            //                    mRemQty -= remQty;
                            //                }

                            //            }
                            //            else if(md.PutAwayPieces == null && md.LoadCount != null && md.LoadCount > 0)
                            //            {
                            //                int remQty = (int)md.LoadCount;

                            //                if (remQty == mRemQty)
                            //                {
                            //                    MList.Add(md);
                            //                    MCount.Add(mRemQty);

                            //                    md.PutAwayPieces = mRemQty;
                            //                    mRemQty = 0;
                            //                }
                            //                else if (remQty > mRemQty)
                            //                {
                            //                    MList.Add(md);
                            //                    MCount.Add(mRemQty);

                            //                    md.PutAwayPieces = mRemQty;
                            //                    mRemQty = 0;
                            //                }
                            //                else
                            //                {
                            //                    MList.Add(md);
                            //                    MCount.Add(remQty);

                            //                    md.PutAwayPieces = mRemQty;
                            //                    mRemQty -= remQty;
                            //                }

                            //            }

                            //            md.PercentPutAway = Math.Round((((double)md.PutAwayPieces / (double)md.LoadCount) * (double)100),0);
                            //    }

                            //    #endregion

                            //    #region Calculate Remaining Put Away Pieces

                            //    if (md.LoadCount.HasValue)
                            //    {
                            //        ttlPieces += (int)md.LoadCount;
                            //    }

                            //    if (md.PutAwayPieces.HasValue)
                            //    {
                            //        ttlPutAwayPieces += (int)md.PutAwayPieces;
                            //    }

                            //    #endregion

                            //}

                            //#region Create Put Away Return Values

                            //ttlRemPutAwayPieces = ttlPieces - ttlPutAwayPieces;

                            //if (ttlPieces != 0 && ttlPutAwayPieces != 0)
                            //{
                            //    percentPutAway = Math.Round((((double)ttlPutAwayPieces / (double)ttlPieces) * (double)100), 0);
                            //}
                            //else
                            //{
                            //    percentPutAway = 0;
                            //}

                            //putAwayDetails = ttlRemPutAwayPieces.ToString() + "," +
                            //                 ttlPutAwayPieces.ToString() + "," +
                            //                 ttlPieces.ToString() + "," +
                            //                 percentPutAway.ToString();

                            //#endregion

                            #endregion

                            #region Update ManifestAWBDetails Put Away Pieces

                            List<ManifestAWBDetail> MaList = new List<ManifestAWBDetail>();
                            List<int> MaCount = new List<int>();

                            foreach (ManifestAWBDetail mad in maList)
                            {
                                if (pRemQty != 0)
                                {
                                    if (mad.PutAwayPieces != null && mad.LoadCount != null && mad.LoadCount > 0)
                                    {
                                        int remQty = (int)mad.LoadCount - (int)mad.PutAwayPieces;

                                        if (remQty == pRemQty)
                                        {
                                            MaList.Add(mad);
                                            MaCount.Add(pRemQty);

                                            mad.PutAwayPieces += pRemQty;
                                            pRemQty = 0;
                                        }
                                        else if (remQty > pRemQty)
                                        {
                                            MaList.Add(mad);
                                            MaCount.Add(pRemQty);

                                            mad.PutAwayPieces += pRemQty;
                                            pRemQty = 0;
                                        }
                                        else
                                        {
                                            MaList.Add(mad);
                                            MaCount.Add(remQty);

                                            mad.PutAwayPieces += pRemQty;
                                            pRemQty -= remQty;
                                        }

                                    }
                                    else if (mad.PutAwayPieces == null && mad.LoadCount != null && mad.LoadCount > 0)
                                    {
                                        int remQty = (int)mad.LoadCount;

                                        if (remQty == pRemQty)
                                        {
                                            MaList.Add(mad);
                                            MaCount.Add(pRemQty);

                                            mad.PutAwayPieces = pRemQty;
                                            pRemQty = 0;
                                        }
                                        else if (remQty > pRemQty)
                                        {
                                            MaList.Add(mad);
                                            MaCount.Add(pRemQty);

                                            mad.PutAwayPieces = pRemQty;
                                            pRemQty = 0;
                                        }
                                        else
                                        {
                                            MaList.Add(mad);
                                            MaCount.Add(remQty);

                                            mad.PutAwayPieces = pRemQty;
                                            pRemQty -= remQty;
                                        }
                                    }
                                    mad.PercentPutAway = Math.Round((((double)mad.PutAwayPieces / (double)mad.LoadCount) * (double)100), 0);
                                }
                            }

                            #endregion

                            #region Insert FLT Put Away Complete Transaction

                            if (tFlt.TotalPieces == tFlt.PutAwayPieces)
                            {
                                var fStatus = sList.Where(a => a.Id == 9).FirstOrDefault();
                                var fEntity = tList.Where(b => b.Id == 1).FirstOrDefault();
                                var fAction = aList.Where(c => c.Id == 15).FirstOrDefault();

                                if (fStatus != null && fEntity != null && fAction != null)
                                {
                                    if (tFlt.Status.Id < 9 && tFlt.Status != fStatus)
                                    {
                                        tFlt.Status = fStatus;
                                    }
                                    Transaction nFltTran = new Transaction()
                                    {
                                        StatusTimestamp = tStamp,
                                        UserName = cUser,
                                        Description = "Put Away Complete",
                                        Reference = "FLT# " + tFlt.Carrier.CarrierCode + tFlt.FlightNumber,
                                        Status = fStatus,
                                        EntityId = fltId,
                                        EntityType = fEntity,
                                        TaskType = uList,
                                        TransactionAction = fAction
                                    };
                                }

                            }
                            else
                            {
                                var fStatus = sList.Where(a => a.Id == 8).FirstOrDefault();

                                if (tFlt.Status.Id < 8 && tFlt.Status != fStatus)
                                {
                                    tFlt.Status = fStatus;
                                }

                            }

                            #endregion

                            #region Insert ULD Put Away Complete Transaction and Disposition Record, If It does NOT Exist

                            //if (UldList.Count() > 0)
                            //{
                            //    int uCtr = 0;

                            //    foreach (ULD u in UldList)
                            //    {
                            //        if(u.TotalPieces == u.PutAwayPieces)
                            //        {
                            //            var uStatus = sList.Where(a => a.Id == 9).FirstOrDefault();
                            //            var uEntity = tList.Where(b => b.Id == 4).FirstOrDefault();
                            //            var cAction = aList.Where(c => c.Id == 15).FirstOrDefault();

                            //            if(u.Status != uStatus)
                            //            {
                            //                u.Status = uStatus;                                           
                            //            }

                            //            Transaction uldTran = new Transaction()
                            //            {
                            //                StatusTimestamp = tStamp,
                            //                UserName = cUser,
                            //                Description = "Put Away Complete",
                            //                Reference = "ULD# " + u.ShipmentUnitType.Code + " " + u.SerialNumber,
                            //                Status = uStatus,
                            //                EntityId = u.Id,
                            //                EntityType = uEntity,
                            //                TaskType = uList,
                            //                TransactionAction = cAction
                            //            };

                            //        }

                            //Disposition nDisposition = new Disposition()
                            //{
                            //ULD = u,
                            //    HWB = tHWB,
                            //    UserName = cUser,
                            //    Timestamp = tStamp,
                            //    WarehouseLocation = tLoc,
                            //    Count = pQty
                            //};
                            //uCtr++;
                            //}
                            //}

                            #endregion

                            #region Insert AWB Put Away Complete Transaction

                            int tTotalCnt = 0;
                            int tPutAwayCnt = 0;
                            foreach (ManifestAWBDetail mad in maList)
                            {
                                if (mad.LoadCount != null)
                                {
                                    tTotalCnt += (int)mad.LoadCount;
                                }

                                if (mad.PutAwayPieces != null)
                                {
                                    tPutAwayCnt += (int)mad.PutAwayPieces;
                                }

                            }

                            if (tTotalCnt == tPutAwayCnt)
                            {
                                var aStatus = sList.Where(a => a.Id == 9).FirstOrDefault();
                                var aEntity = tList.Where(b => b.Id == 2).FirstOrDefault();
                                var cAction = aList.Where(c => c.Id == 15).FirstOrDefault();

                                Transaction awbTran = new Transaction()
                                {
                                    StatusTimestamp = tStamp,
                                    UserName = cUser,
                                    Description = "Put Away Complete",
                                    Reference = "AWB# " + tAWB.Carrier.Carrier3Code + "-" + tAWB.AWBSerialNumber,
                                    Status = aStatus,
                                    EntityId = tAWB.Id,
                                    EntityType = aEntity,
                                    TaskType = uList,
                                    TransactionAction = cAction
                                };
                            }

                            #endregion

                            #region Insert HWB Put Away and Complete Transaction

                            if (tHWB.Pieces == tHWB.PutAwayPieces)
                            {
                                var hStatus = sList.Where(a => a.Id == 9).FirstOrDefault();
                                var hEntity = tList.Where(b => b.Id == 3).FirstOrDefault();
                                var tAction = aList.Where(c => c.Id == 12).FirstOrDefault();
                                var cAction = aList.Where(d => d.Id == 15).FirstOrDefault();

                                if (hStatus != null && hEntity != null && tAction != null && cAction != null)
                                {
                                    if (tHWB.Status != hStatus)
                                    {
                                        tHWB.Status = hStatus;
                                        tHWB.StatusTimestamp = tStamp;
                                    }

                                    Transaction cHwbTran = new Transaction()
                                    {
                                        StatusTimestamp = tStamp,
                                        UserName = cUser,
                                        Description = "Put Away Complete",
                                        Reference = "HWB# " + tHWB.HWBSerialNumber,
                                        Status = hStatus,
                                        EntityId = hwbId,
                                        EntityType = hEntity,
                                        TaskType = uList,
                                        TransactionAction = cAction
                                    };

                                    Transaction nHwbTran = new Transaction()
                                    {
                                        StatusTimestamp = tStamp,
                                        UserName = cUser,
                                        Description = "Put Away " + pQty + " Pieces At " + tLoc.Location,
                                        Reference = "HWB# " + tHWB.HWBSerialNumber,
                                        Status = hStatus,
                                        EntityId = hwbId,
                                        EntityType = hEntity,
                                        TaskType = uList,
                                        TransactionAction = tAction
                                    };
                                }
                            }
                            else
                            {
                                var hStatus = sList.Where(a => a.Id == 8).FirstOrDefault();
                                var hEntity = tList.Where(b => b.Id == 3).FirstOrDefault();
                                var tAction = aList.Where(c => c.Id == 12).FirstOrDefault();

                                if (hStatus != null && hEntity != null && tAction != null)
                                {
                                    if (tHWB.Status != hStatus)
                                    {
                                        tHWB.Status = hStatus;
                                        tHWB.StatusTimestamp = tStamp;
                                    }

                                    Transaction nHwbTran = new Transaction()
                                    {
                                        StatusTimestamp = tStamp,
                                        UserName = cUser,
                                        Description = "Put Away " + pQty + " Pieces At " + tLoc.Location,
                                        Reference = "HWB# " + tHWB.HWBSerialNumber,
                                        Status = hStatus,
                                        EntityId = hwbId,
                                        EntityType = hEntity,
                                        TaskType = uList,
                                        TransactionAction = tAction
                                    };
                                }
                            }
                            #endregion

                            #region Save Changes

                            contextApi.DataWorkspace.WFSDBData.SaveChanges();

                            #endregion

                            context.Response.ContentType = "text/plain";
                            context.Response.Write(putAwayDetails);

                        }
                        else
                        {
                            context.Response.ContentType = "text/plain";
                            context.Response.Write("0");
                        }

                    }
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("0");
                }
            }

            #endregion


            //===============================================================================================================

            //            #region HWB Put Away Pieces Complete

            //            if (tHWB.Pieces == (totalPutAwayPieces + hwbQty))
            //            {
            //                var hStatus = sList.Where(s => s.Id == 9).FirstOrDefault();
            //                if (hStatus != null)
            //                {
            //                    var hEntity = tList.Where(t => t.Id == 3).FirstOrDefault();
            //                    if (hEntity != null)
            //                    {
            //                        var hType = uList.Where(u => u.Id == 1).FirstOrDefault();
            //                        if (hType != null)
            //                        {
            //                            var tAction = aList.Where(a => a.Id == 13).FirstOrDefault();
            //                            {
            //                                if (tAction != null)
            //                                {
            //                                    tHWB.PutAwayPieces += hwbQty;
            //                                    tHWB.PercentPutAway = (((float)tHWB.PutAwayPieces / (float)tHWB.Pieces) * (float)100);
            //                                    tHWB.Status = hStatus;
            //                                    tHWB.StatusTimestamp = tStamp;

            //                                    #region Apply Pieces to available ULDs
            //                                    int uPcsCtr = hwbQty;
            //                                    List<ULD> uldList = new List<ULD>();
            //                                    List<int> uldCount = new List<int>();
            //                                    foreach(ManifestDetail md in mList)
            //                                    {
            //                                        ULD tmpUld = md.ULD;
            //                                        if(tmpUld.PutAwayPieces != tmpUld.TotalPieces)
            //                                        {
            //                                            if (uPcsCtr != 0)
            //                                            {
            //                                                if (tmpUld.PutAwayPieces != null && tmpUld.TotalPieces != null)
            //                                                {
            //                                                    int tmpRem = (int)tmpUld.TotalPieces - (int)tmpUld.PutAwayPieces;

            //                                                    #region ULD remaining put away pieces == remaining put away pieces
            //                                                    if (tmpRem == uPcsCtr)
            //                                                    {
            //                                                        if (tmpUld.PutAwayPieces != null)
            //                                                        {
            //                                                            tmpUld.PutAwayPieces += uPcsCtr;                                                                       
            //                                                            uldList.Add(tmpUld);
            //                                                            uldCount.Add(uPcsCtr);
            //                                                            uPcsCtr = 0;
            //                                                        }
            //                                                        else
            //                                                        {
            //                                                            tmpUld.PutAwayPieces = uPcsCtr;
            //                                                            uldList.Add(tmpUld);
            //                                                            uldCount.Add(uPcsCtr);
            //                                                            uPcsCtr = 0;
            //                                                        }

            //                                                        if (tmpUld.TotalPieces == tmpUld.PutAwayPieces)
            //                                                        {
            //                                                            var uEntity = tList.Where(a => a.Id == 4).FirstOrDefault();
            //                                                            if (uEntity != null)
            //                                                            {
            //                                                                Transaction nTransaction2 = new Transaction()
            //                                                                {
            //                                                                    StatusTimestamp = tStamp,
            //                                                                    UserName = cUser,
            //                                                                    Description = "Put Away Complete",
            //                                                                    Reference = "ULD# " + tmpUld.ShipmentUnitType.Code + " " + tmpUld.SerialNumber,
            //                                                                    Status = hStatus,
            //                                                                    EntityId = tmpUld.Id,
            //                                                                    EntityType = uEntity,
            //                                                                    TaskType = hType,
            //                                                                    TransactionAction = tAction
            //                                                                };
            //                                                            }

            //                                                        }
            //                                                    }
            //                                                    #endregion

            //                                                    #region ULD remaining put away pieces > remaining put away pieces
            //                                                    else if (tmpRem > uPcsCtr)
            //                                                    {
            //                                                        if (tmpUld.PutAwayPieces != null)
            //                                                        {
            //                                                            tmpUld.PutAwayPieces += uPcsCtr;
            //                                                            uldCount.Add(uPcsCtr);
            //                                                            uldList.Add(tmpUld);
            //                                                            uPcsCtr = 0;
            //                                                        }
            //                                                        else
            //                                                        {
            //                                                            tmpUld.PutAwayPieces = uPcsCtr;
            //                                                            uldList.Add(tmpUld);
            //                                                            uldCount.Add(uPcsCtr);
            //                                                            uPcsCtr = 0;
            //                                                        }

            //                                                        if (tmpUld.TotalPieces == tmpUld.PutAwayPieces)
            //                                                        {
            //                                                            var uEntity = tList.Where(a => a.Id == 4).FirstOrDefault();
            //                                                            if (uEntity != null)
            //                                                            {
            //                                                                Transaction nTransaction2 = new Transaction()
            //                                                                {
            //                                                                    StatusTimestamp = tStamp,
            //                                                                    UserName = cUser,
            //                                                                    Description = "Put Away Complete",
            //                                                                    Reference = "ULD# " + tmpUld.ShipmentUnitType.Code + " " + tmpUld.SerialNumber,
            //                                                                    Status = hStatus,
            //                                                                    EntityId = tmpUld.Id,
            //                                                                    EntityType = uEntity,
            //                                                                    TaskType = hType,
            //                                                                    TransactionAction = tAction
            //                                                                };
            //                                                            }

            //                                                        }
            //                                                    }
            //                                                    #endregion

            //                                                    #region ULD remaining put away pieces < remaining put away pieces
            //                                                    else
            //                                                    {
            //                                                        if (tmpUld.PutAwayPieces != null)
            //                                                        {
            //                                                            tmpUld.PutAwayPieces += uPcsCtr;

            //                                                            uldList.Add(tmpUld);
            //                                                            uldCount.Add(tmpRem);
            //                                                            uPcsCtr -= tmpRem;
            //                                                        }
            //                                                        else
            //                                                        {
            //                                                            tmpUld.PutAwayPieces = uPcsCtr;

            //                                                            uldList.Add(tmpUld);
            //                                                            uldCount.Add(tmpRem);
            //                                                            uPcsCtr -= tmpRem;
            //                                                        }

            //                                                        if (tmpUld.TotalPieces == tmpUld.PutAwayPieces)
            //                                                        {
            //                                                            var uEntity = tList.Where(a => a.Id == 4).FirstOrDefault();
            //                                                            if (uEntity != null)
            //                                                            {
            //                                                                Transaction nTransaction2 = new Transaction()
            //                                                                {
            //                                                                    StatusTimestamp = tStamp,
            //                                                                    UserName = cUser,
            //                                                                    Description = "Put Away Complete",
            //                                                                    Reference = "ULD# " + tmpUld.ShipmentUnitType.Code + " " + tmpUld.SerialNumber,
            //                                                                    Status = hStatus,
            //                                                                    EntityId = tmpUld.Id,
            //                                                                    EntityType = uEntity,
            //                                                                    TaskType = hType,
            //                                                                    TransactionAction = tAction
            //                                                                };
            //                                                            }

            //                                                        }
            //                                                    }
            //                                                    #endregion

            //                                                }
            //                                            }

            //                                        }

            //                                    }
            //                                    #endregion

            //                                    #region Create Disposition Records
            //                                    if (uldList.Count() > 0)
            //                                    {
            //                                        int uCtr = 0;

            //                                        foreach (ULD u in uldList)
            //                                        {
            //                                            Disposition nDisposition = new Disposition()
            //                                            {
            //                                                ULD = u,
            //                                                HWB = tHWB,
            //                                                UserName = cUser,
            //                                                Timestamp = tStamp,
            //                                                WarehouseLocation = tLoc,
            //                                                Count = uldCount[uCtr]
            //                                            };
            //                                            uCtr++;
            //                                        }
            //                                    }
            //                                    #endregion

            //                                    #region Create Put Away Transactions
            //                                    Transaction nTransaction = new Transaction()
            //                                    {
            //                                        StatusTimestamp = tStamp,
            //                                        UserName =cUser,
            //                                        Description = "Put Away " + tmpQty + " Pieces At " + tLoc.Location,
            //                                        Reference = "HWB# " + tHWB.HWBSerialNumber,
            //                                        Status = hStatus,
            //                                        EntityId = hId,
            //                                        EntityType = hEntity,
            //                                        TaskType = hType,
            //                                        TransactionAction = tAction
            //                                    };
            //                                    #endregion

            //                                    #region Create Put Away Complete Transaction
            //                                    var tAction1 = aList.Where(a => a.Id == 15).FirstOrDefault();
            //                                    if (tAction1 != null)
            //                                    {
            //                                        Transaction nTransaction1 = new Transaction()
            //                                        {
            //                                            StatusTimestamp = tStamp,
            //                                            UserName = cUser,
            //                                            Description = "Put Away Complete",
            //                                            Reference = "HWB# " + tHWB.HWBSerialNumber,
            //                                            Status = hStatus,
            //                                            EntityId = hId,
            //                                            EntityType = hEntity,
            //                                            TaskType = hType,
            //                                            TransactionAction = tAction1
            //                                        };

            //                                    #endregion

            //                                        #region Apply Pieces to AWB

            //                                        AWB tAWB = mList.FirstOrDefault().AWB;

            //                                        if (tAWB.PutAwayPieces != null)
            //                                        {        
            //                                            tAWB.PutAwayPieces += hwbQty;
            //                                            tAWB.PercentPutAway = (((float)tAWB.PutAwayPieces / (float)tAWB.TotalPieces) * (float)100);
            //                                        }
            //                                        else
            //                                        {
            //                                            tAWB.PutAwayPieces = hwbQty;
            //                                            tAWB.PercentPutAway = (((float)tAWB.PutAwayPieces / (float)tAWB.TotalPieces) * (float)100);
            //                                        }

            //                                        if (tAWB.TotalPieces == tAWB.PutAwayPieces)
            //                                        {
            //                                            var aEntity = tList.Where(a => a.Id == 2).FirstOrDefault();
            //                                            if (aEntity != null)
            //                                            {
            //                                                tAWB.Status = hStatus;
            //                                                tAWB.StatusTimestamp = tStamp;

            //                                                Transaction nTransaction2 = new Transaction()
            //                                                {
            //                                                    StatusTimestamp = tStamp,
            //                                                    UserName = cUser,
            //                                                    Description = "Put Away Complete",
            //                                                    Reference = "AWB# " + tAWB.Carrier.Carrier3Code + "-" + tAWB.AWBSerialNumber,
            //                                                    Status = hStatus,
            //                                                    EntityId = tAWB.Id,
            //                                                    EntityType = aEntity,
            //                                                    TaskType = hType,
            //                                                    TransactionAction = tAction1
            //                                                };
            //                                            }

            //                                        }
            //                                        else
            //                                        {

            //                                            var iStatus = sList.Where(s => s.Id == 8).FirstOrDefault();
            //                                            if(iStatus != null)
            //                                            {

            //                                                tAWB.Status = iStatus;

            //                                            }

            //                                        }

            //                                        #endregion

            //                                        #region Apply Pieces to FLT

            //                                        FlightManifest tFLT = mList.FirstOrDefault().FlightManifest;

            //                                        if (tFLT.PutAwayPieces != null)
            //                                        {
            //                                            tFLT.PutAwayPieces += hwbQty;
            //                                            tFLT.PercentPutAway = (((float)tFLT.PutAwayPieces / (float)tFLT.TotalPieces) * (float)100);
            //                                        }
            //                                        else
            //                                        {
            //                                            tFLT.PutAwayPieces = hwbQty;
            //                                            tFLT.PercentPutAway = (((float)tFLT.PutAwayPieces / (float)tFLT.TotalPieces) * (float)100);
            //                                        }

            //                                        if (tFLT.TotalPieces == tFLT.PutAwayPieces)
            //                                        {
            //                                            var fEntity = tList.Where(a => a.Id == 1).FirstOrDefault();
            //                                            if (fEntity != null)
            //                                            {
            //                                                tFLT.Status = hStatus;
            //                                                tFLT.StatusTimestamp = tStamp;

            //                                                Transaction nTransaction2 = new Transaction()
            //                                                {
            //                                                    StatusTimestamp = tStamp,
            //                                                    UserName = cUser,
            //                                                    Description = "Put Away Complete",
            //                                                    Reference = "FLT# " + tFLT.Carrier.CarrierCode + tFLT.FlightNumber,
            //                                                    Status = hStatus,
            //                                                    EntityId = tFLT.Id,
            //                                                    EntityType = fEntity,
            //                                                    TaskType = hType,
            //                                                    TransactionAction = tAction1
            //                                                };
            //                                            }

            //                                        }
            //                                        else
            //                                        {

            //                                            var iStatus = sList.Where(s => s.Id == 8).FirstOrDefault();
            //                                            if (iStatus != null)
            //                                            {

            //                                                tFLT.Status = iStatus;

            //                                            }

            //                                        }
            //                                        #endregion
            //                                    }                                                    
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //            #endregion

            //            #region HWB Put Away Pieces In Progress
            //            else
            //            {
            //                var hStatus = sList.Where(s => s.Id == 8).FirstOrDefault();
            //                if (hStatus != null)
            //                {
            //                    var hEntity = tList.Where(t => t.Id == 3).FirstOrDefault();
            //                    if (hEntity != null)
            //                    {
            //                        var hType = uList.Where(u => u.Id == 1).FirstOrDefault();
            //                        if (hType != null)
            //                        {
            //                            var tAction = aList.Where(a => a.Id == 13).FirstOrDefault();
            //                            {
            //                                tHWB.PutAwayPieces += hwbQty;
            //                                tHWB.PercentPutAway = (((float)tHWB.PutAwayPieces / (float)tHWB.Pieces) * (float)100);
            //                                tHWB.Status = hStatus;
            //                                tHWB.StatusTimestamp = tStamp;

            //                                #region Apply Pieces to available ULDs
            //                                int uPcsCtr = hwbQty;
            //                                List<ULD> uldList = new List<ULD>();
            //                                List<int> uldCount = new List<int>();
            //                                foreach (ManifestDetail md in mList)
            //                                {
            //                                    ULD tmpUld = md.ULD;
            //                                    if (tmpUld.PutAwayPieces != tmpUld.TotalPieces)
            //                                    {
            //                                        if (uPcsCtr != 0)
            //                                        {
            //                                            if (tmpUld.PutAwayPieces != null && tmpUld.TotalPieces != null)
            //                                            {
            //                                                int tmpRem = (int)tmpUld.TotalPieces - (int)tmpUld.PutAwayPieces;

            //                                                #region ULD remaining put away pieces == remaining put away pieces
            //                                                if (tmpRem == uPcsCtr)
            //                                                {
            //                                                    if (tmpUld.PutAwayPieces != null)
            //                                                    {
            //                                                        tmpUld.PutAwayPieces += uPcsCtr;
            //                                                        uldList.Add(tmpUld);
            //                                                        uldCount.Add(uPcsCtr);
            //                                                        uPcsCtr = 0;
            //                                                    }
            //                                                    else
            //                                                    {
            //                                                        tmpUld.PutAwayPieces = uPcsCtr;
            //                                                        uldList.Add(tmpUld);
            //                                                        uldCount.Add(uPcsCtr);
            //                                                        uPcsCtr = 0;
            //                                                    }
            //                                                }
            //                                                #endregion

            //                                                #region ULD remaining put away pieces > remaining put away pieces
            //                                                else if (tmpRem > uPcsCtr)
            //                                                {
            //                                                    if (tmpUld.PutAwayPieces != null)
            //                                                    {
            //                                                        tmpUld.PutAwayPieces += uPcsCtr;
            //                                                        uldList.Add(tmpUld);
            //                                                        uldCount.Add(uPcsCtr);
            //                                                        uPcsCtr = 0;
            //                                                    }
            //                                                    else
            //                                                    {
            //                                                        tmpUld.PutAwayPieces = uPcsCtr;
            //                                                        uldList.Add(tmpUld);
            //                                                        uldCount.Add(uPcsCtr);
            //                                                        uPcsCtr = 0;
            //                                                    }
            //                                                }
            //                                                #endregion

            //                                                #region ULD remaining put away pieces < remaining put away pieces
            //                                                else
            //                                                {
            //                                                    if (tmpUld.PutAwayPieces != null)
            //                                                    {
            //                                                        tmpUld.PutAwayPieces += uPcsCtr;

            //                                                        uldList.Add(tmpUld);
            //                                                        uldCount.Add(tmpRem);
            //                                                        uPcsCtr -= tmpRem;
            //                                                    }
            //                                                    else
            //                                                    {
            //                                                        tmpUld.PutAwayPieces = uPcsCtr;

            //                                                        uldList.Add(tmpUld);
            //                                                        uldCount.Add(tmpRem);
            //                                                        uPcsCtr -= tmpRem;
            //                                                    }
            //                                                }
            //                                                #endregion

            //                                            }
            //                                        }
            //                                    }

            //                                }
            //                                #endregion

            //                                #region Create Disposition Records
            //                                if (uldList.Count() > 0)
            //                                {
            //                                    int uCtr = 0;

            //                                    foreach (ULD u in uldList)
            //                                    {
            //                                        Disposition nDisposition = new Disposition()
            //                                        {
            //                                            ULD = u,
            //                                            HWB = tHWB,
            //                                            UserName = cUser,
            //                                            Timestamp = tStamp,
            //                                            WarehouseLocation = tLoc,
            //                                            Count = uldCount[uCtr]
            //                                        };
            //                                        uCtr++;
            //                                    }
            //                                }
            //                                #endregion

            //                                #region Create Put Away Transaction

            //                                Transaction nTransaction = new Transaction()
            //                                {
            //                                    StatusTimestamp = tStamp,
            //                                    UserName = cUser,
            //                                    Description = "Put Away " + tmpQty + " Pieces At " + tLoc.Location,
            //                                    Reference = "HWB# " + tHWB.HWBSerialNumber,
            //                                    Status = hStatus,
            //                                    EntityId = hId,
            //                                    EntityType = hEntity,
            //                                    TaskType = hType,
            //                                    TransactionAction = tAction
            //                                };

            //                                #endregion

            //                                #region Apply Pieces to AWB

            //                                AWB tAWB = mList.FirstOrDefault().AWB;

            //                                if (tAWB.PutAwayPieces != null)
            //                                {
            //                                    tAWB.Status = hStatus;
            //                                    tAWB.PutAwayPieces += hwbQty;
            //                                    tAWB.PercentPutAway = (((float)tAWB.PutAwayPieces / (float)tAWB.TotalPieces) * (float)100);
            //                                }
            //                                else
            //                                {
            //                                    tAWB.Status = hStatus;
            //                                    tAWB.PutAwayPieces = hwbQty;
            //                                    tAWB.PercentPutAway = (((float)tAWB.PutAwayPieces / (float)tAWB.TotalPieces) * (float)100);
            //                                }

            //                                //if (tAWB.TotalPieces == tAWB.PutAwayPieces)
            //                                //{
            //                                //    var aEntity = tList.Where(a => a.Id == 2).FirstOrDefault();
            //                                //    if (aEntity != null)
            //                                //    {
            //                                //        Transaction nTransaction2 = new Transaction()
            //                                //        {
            //                                //            StatusTimestamp = tStamp,
            //                                //            UserName = cUser,
            //                                //            Description = "Put Away Complete",
            //                                //            Reference = "AWB # " + tAWB.Carrier.Carrier3Code + "-" + tAWB.AWBSerialNumber,
            //                                //            Status = hStatus,
            //                                //            EntityId = tAWB.Id,
            //                                //            EntityType = aEntity,
            //                                //            TaskType = hType,
            //                                //            TransactionAction = tAction
            //                                //        };
            //                                //    }

            //                                //}
            //                                #endregion

            //                                #region Apply Pieces to FLT

            //                                FlightManifest tFLT = mList.FirstOrDefault().FlightManifest;

            //                                if (tFLT.PutAwayPieces != null)
            //                                {
            //                                    tFLT.Status = hStatus;
            //                                    tFLT.PutAwayPieces += hwbQty;
            //                                    tFLT.PercentPutAway = (((float)tFLT.PutAwayPieces / (float)tFLT.TotalPieces) * (float)100);
            //                                }
            //                                else
            //                                {
            //                                    tFLT.Status = hStatus;
            //                                    tFLT.PutAwayPieces = hwbQty;
            //                                    tFLT.PercentPutAway = (((float)tFLT.PutAwayPieces / (float)tFLT.TotalPieces) * (float)100);
            //                                }

            //                                //if (tFLT.TotalPieces == tFLT.PutAwayPieces)
            //                                //{
            //                                //    var fEntity = tList.Where(a => a.Id == 1).FirstOrDefault();
            //                                //    if (fEntity != null)
            //                                //    {
            //                                //        Transaction nTransaction2 = new Transaction()
            //                                //        {
            //                                //            StatusTimestamp = tStamp,
            //                                //            UserName = cUser,
            //                                //            Description = "Put Away Complete",
            //                                //            Reference = "FLT # " + tFLT.Carrier.CarrierCode + " " + tFLT.FlightNumber,
            //                                //            Status = hStatus,
            //                                //            EntityId = tFLT.Id,
            //                                //            EntityType = fEntity,
            //                                //            TaskType = hType,
            //                                //            TransactionAction = tAction
            //                                //        };
            //                                //    }

            //                                //}
            //                                #endregion

            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //            #endregion

            //            #region Save Records and Send Success Response
            //            contextApi.DataWorkspace.WFSDBData.SaveChanges();

            //            context.Response.ContentType = "text/plain";
            //            context.Response.Write("1");
            //            #endregion

            //        }
            //    }
            //    else
            //    {
            //        context.Response.ContentType = "text/plain";
            //        context.Response.Write("0");
            //    }

            //}
            //#endregion

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}