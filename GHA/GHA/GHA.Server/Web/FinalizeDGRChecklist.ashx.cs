﻿using LightSwitchApplication.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using System.Web.Http;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for FinalizeDGRChecklist
    /// </summary>
    public class FinalizeDGRChecklist : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region Parameters

            var tEntityTypeId = context.Request["EntityTypeId"];
            var tEntityId = context.Request["EntityId"];
            var tComments = context.Request["Comments"];
            var tPlace = context.Request["Place"];
            var tUserName = context.Request["UserName"];
            var tTaskId = context.Request["TaskId"];
            var image = context.Request["SigImage"];

            #endregion

            #region Variables

            bool success = false;
            int eTypeId;
            long eId;
            long tId;
            int sCtr = 0;

            #endregion

            #region Cast Task Id

            success = int.TryParse(tEntityTypeId, out eTypeId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = long.TryParse(tEntityId, out eId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = long.TryParse(tTaskId, out tId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            #endregion

            #region Save Image

            var startIndex = image.IndexOf("base64");
            image = image.Substring(startIndex + 7);
            byte[] bytes = Convert.FromBase64String(image);

            Image img;
            string fileName;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                img = Image.FromStream(ms);
                var datePath = DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss");
                fileName = (new Random().Next()) + "_" + datePath + ".png";
                var fileFullName = ConfigurationManager.AppSettings["SignatureImagePath"] + fileName;
                img.Save(fileFullName, System.Drawing.Imaging.ImageFormat.Png);
            }

            var signPath = ConfigurationManager.AppSettings["SignatureRelativeImagePath"] + fileName;

            #endregion

            if (sCtr == 3)
            {
                DatabaseManager.ExecuteScalar("FinalizeDGRChecklist", new[] 
            {

                new KeyValuePair<string, object>("@EntityTypeID",eTypeId),
                new KeyValuePair<string, object>("@EntityId", eId),
                new KeyValuePair<string, object>("@Comments", tComments),
                new KeyValuePair<string, object>("@Place", tPlace),
                new KeyValuePair<string, object>("@SignaturePath", signPath),
                new KeyValuePair<string, object>("@UserName", tUserName),
                new KeyValuePair<string, object>("@TaskId", tId)

            });
                context.Response.ContentType = "text/plain";
                context.Response.Write("1");
            }

            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("F");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}