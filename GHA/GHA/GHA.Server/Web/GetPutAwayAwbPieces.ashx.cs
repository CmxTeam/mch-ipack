﻿using LightSwitchApplication.Helpers;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetPutAwayAwbPieces
    /// </summary>
    public class GetPutAwayAwbPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tAwbId = context.Request["AwbId"];

            var result = DatabaseManager.GetValue(string.Format("Select Top 1 isnull(TotalPieces, 0) - (isnull(PutAwayPieces, 0) + isnull(ForkliftPieces, 0)) as P from AWBs where Id = {0}", tAwbId), "P", "0");
            context.Response.ContentType = "text/plain";
            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}