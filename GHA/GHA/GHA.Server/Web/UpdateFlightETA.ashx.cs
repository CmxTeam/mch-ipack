﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for UpdateFlightETA
    /// </summary>
    public class UpdateFlightETA : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region Parameters

            var fltId = Int64.Parse(context.Request["fltId"]);
            var portId = int.Parse(context.Request["prtId"]);
            var tEta = context.Request["ETA"];
            var cUser = context.Request["cUser"];

            var sEta = DateTime.Parse(tEta);

            #endregion

            Helpers.DatabaseManager.ExecuteScalar("UpdateFlightETA", new[] 
            {
                new KeyValuePair<string, object>("@FlightManifestId", fltId),
                new KeyValuePair<string, object>("@PortId", portId),
                new KeyValuePair<string, object>("@ETA", sEta),
                new KeyValuePair<string, object>("@UserName", cUser)
         
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}