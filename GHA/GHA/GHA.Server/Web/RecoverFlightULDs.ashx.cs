﻿using LightSwitchApplication.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for RecoverFlightULDs
    /// </summary>
    public class RecoverFlightULDs : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var flightId = context.Request["FltId"];
            var uldIds = JArray.Parse(context.Request["UldIds"]);
            var cUser = context.Request["cUser"];

            foreach (var id in uldIds) 
            {
                Common.RecoverFlightUldAdapter.ProcessUld(long.Parse(flightId), long.Parse(id.ToString()), cUser);               
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}