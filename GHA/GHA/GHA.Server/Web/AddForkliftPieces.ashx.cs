﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AddForkliftPieces
    /// </summary>
    public class AddForkliftPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region Parameters

            var tEntityTypeId = context.Request["eTypeId"];
            var tEntityId = context.Request["eId"];
            var tQty = context.Request["pQty"];
            var tTaskId = context.Request["tId"];
            var cUser = context.Request["cUser"];

            #endregion

            Helpers.DatabaseManager.ExecuteScalar("AddForklistPieces", new[] 
            {
                new KeyValuePair<string, object>("@EntityId", tEntityId),
                new KeyValuePair<string, object>("@TaskId", tTaskId),
                new KeyValuePair<string, object>("@Quantity", tQty),
                new KeyValuePair<string, object>("@CurrentUser", cUser),
                new KeyValuePair<string, object>("@EntityTypeId", tEntityTypeId)           
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("X");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}