﻿using LightSwitchApplication.Helpers;
using System.Collections.Generic;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for SetStagingLocation
    /// </summary>
    public class SetStagingLocation : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tFltId = context.Request["FltId"];
            var tLocId = context.Request["LocId"];
            var cUser = context.Request["cUser"];
            var tTypeId = context.Request["tType"];

            if (tTypeId == "1")
            {
                DatabaseManager.ExecuteScalar("RecoverFlight", new[]
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@FlightManifestId", tFltId),
                new KeyValuePair<string, object>("@LocationId", tLocId)
            });
            }
            else
            {
                DatabaseManager.ExecuteScalar("SetStagingLocation", new[]
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@FlightManifestId", tFltId),
                new KeyValuePair<string, object>("@LocationId", tLocId)
            });
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}