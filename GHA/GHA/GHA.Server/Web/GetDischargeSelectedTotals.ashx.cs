﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetDischargeSelectedTotals
    /// </summary>
    public class GetDischargeSelectedTotals : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            long[] rId;
            int tmpRctr = 0;
            decimal ttlCharges = 0;
            decimal ttlPaid = 0;
            decimal ttlDue = 0;
            string result = "";

            var chargeList = context.Request["cList"];
            var itemList = chargeList.Split(',');
            rId = new long[itemList.Count()];

            foreach (var item in itemList)
            {
                long tmpItem = 0;
                bool success = long.TryParse(item, out tmpItem);
                if (success)
                {
                    rId[tmpRctr] = tmpItem;

                }
                tmpRctr++;
            }

            using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
            {
                var chList = from x in contextApi.DataWorkspace.WFSDBData.DischargeCharges.GetQuery().Execute()
                             where rId.Contains(x.Id)
                             select x;

                foreach (DischargeCharge dc in chList)
                {
                    if(dc.ChargeAmount != null)
                    {
                        ttlCharges += (decimal)dc.ChargeAmount;
                    }
                    if(dc.ChargePaid != null)
                    {
                        ttlPaid += (decimal)dc.ChargePaid;
                    }
                }

                ttlDue = ttlCharges - ttlPaid;

                result = ttlCharges + "," + ttlPaid + "," + ttlDue;
                context.Response.ContentType = "text/plain";
                context.Response.Write(result);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}