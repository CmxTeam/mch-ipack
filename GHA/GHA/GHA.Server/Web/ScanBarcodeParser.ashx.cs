﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for ScanBarcodeParser
    /// </summary>
    public class ScanBarcodeParser : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string tbcScan = context.Request["bcScan"];

            tbcScan = (tbcScan.Replace("-", "")).Trim();
            
            var result = DatabaseManager.ExecuteScalar("BarcodeParser", new[] 
            {
                new KeyValuePair<string, object>("@Scan", tbcScan)   
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write(string.IsNullOrEmpty(result) ? "0" : result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}