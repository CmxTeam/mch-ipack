﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for UnCheckInAWBPieces
    /// </summary>
    public class UnCheckInAWBPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tFltId = context.Request["FltId"];
            var tMawbId = context.Request["MawbId"];
            var tCheckQty = context.Request["cQty"];
            var cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("UnCheckInAWBPieces", new[]
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@FlightManifestId", tFltId),
                new KeyValuePair<string, object>("@ManifestAWBDetailId", tMawbId),
                new KeyValuePair<string, object>("@Quantity", tCheckQty)
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}