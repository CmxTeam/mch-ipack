﻿using LightSwitchApplication.Helpers;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for LoggedUser
    /// </summary>
    public class LoggedUser : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string cUser;
            string cName;
            using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
            {
                cUser = contextApi.Application.User.PersonId;
                cName = contextApi.Application.User.FullName;
            }
            var value = DatabaseManager.GetValue(string.Format(@"Select top 1 PortId from UserStations inner join UserProfiles on UserProfiles.Id = UserStations.UserId 
                                where IsDefault = 1 and UserProfiles.UserId = '{0}'", cUser), "PortId", string.Empty);

            var value1 = DatabaseManager.GetValue(string.Format(@"Select top 1 WarehouseId from UserWarehouses inner join UserProfiles on UserProfiles.Id = UserWarehouses.UserId
                                where IsDefault = 1 and UserProfiles.UserId = '{0}'", cUser), "WarehouseId", string.Empty);

            var userDetails = cUser + (string.IsNullOrEmpty(value) ? string.Empty : "," + value) + (string.IsNullOrEmpty(value1) ? string.Empty : "," + value1) + "," + cName;
            context.Response.ContentType = "text/plain";
            context.Response.Write(userDetails);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}