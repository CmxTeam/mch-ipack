﻿using LightSwitchApplication.Helpers;
using System.Collections.Generic;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for RemoveForkliftPieces
    /// </summary>
    public class RemoveForkliftPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var fId = context.Request["ForkliftDetailId"];
            var c = context.Request["Count"];            

            long forkliftDetailId;
            int count;
            if(!long.TryParse(fId, out forkliftDetailId) || !int.TryParse(c, out count) || count <= 0)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("0");
                return;
            }

            DatabaseManager.ExecuteScalar("RemoveForkliftPieces", new [] 
            {
                new KeyValuePair<string, object>("@ForkliftDetailId", forkliftDetailId),
                new KeyValuePair<string, object>("@Cnt", count)                
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }        

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}