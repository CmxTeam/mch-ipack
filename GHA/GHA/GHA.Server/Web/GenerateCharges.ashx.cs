﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GenerateCharges
    /// </summary>
    public class GenerateCharges : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            bool success;
            bool success1;
            long hId;
            //long aId;
            long dId;
            decimal ttlCharges = 0;
            var tAWB = context.Request["aId"];
            var tHWB = context.Request["hId"];
            var tDetail = context.Request["dId"];

            using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
            {
                
                success = long.TryParse(tDetail, out dId);
                if (success)
                {

                    var dDetail = (from b in contextApi.DataWorkspace.WFSDBData.DischargeDetails.GetQuery().Execute()
                                   where b.Id == dId
                                   select b).FirstOrDefault();

                    if (dDetail != null)
                    {
                        if (tHWB != null)
                        {

                            success1 = long.TryParse(tHWB, out hId);
                            if (success1)
                            {
                                var hItem = (from h in contextApi.DataWorkspace.WFSDBData.HWBs.GetQuery().Execute()
                                             where h.Id == hId
                                             select h).FirstOrDefault();

                                var aItem = (from a in contextApi.DataWorkspace.WFSDBData.ManifestDetails.GetQuery().Execute()
                                             where a.HWB.Id == hId
                                             select a).FirstOrDefault();

                                if (hItem != null && aItem != null)
                                {
                                    //Get HWB CheckIn Complete Status from Transaction
                                    var arrDate = (from d in contextApi.DataWorkspace.WFSDBData.Transactions.GetQuery().Execute()
                                                   where d.EntityType.Id == 3 && d.EntityId == 3 && d.Status.Id == 6
                                                   select d).FirstOrDefault();

                                    if (arrDate != null)
                                    {
                                        //Get Charge Types
                                        var cTypes = from s in contextApi.DataWorkspace.WFSDBData.ChargeTypes.GetQuery().Execute()
                                                     select s;

                                        var impFeeRate = cTypes.Where(a => a.Id == 1).FirstOrDefault();
                                        var storeRate = cTypes.Where(b => b.Id == 2).FirstOrDefault();
                                        var colRate = cTypes.Where(c => c.Id == 3).FirstOrDefault();

                                        if (cTypes != null && impFeeRate != null && storeRate != null && colRate != null)
                                        {


                                            DateTime tStamp = DateTime.UtcNow;

                                            #region Calculate Storage Time

                                            #region Clock Starts at Arrival
                                            if (storeRate.IsClockStartAtArrival == true)
                                            {



                                            }
                                            #endregion

                                            #region Clock does NOT start at Arrival
                                            else
                                            {

                                                DateTime aDate = (DateTime)arrDate.StatusTimestamp;
                                                DateTime adjDate = aDate.AddDays(1);
                                                TimeSpan t = new TimeSpan(0, 0, 0, 0);
                                                adjDate = adjDate.Date + t;

                                                // If ClockStartTime is null, default to midnight
                                                int diffTime = 0;

                                                if (storeRate.ClockStartTime != null)
                                                {
                                                    diffTime = (int)storeRate.ClockStartTime;
                                                }

                                                var tmpHours = (tStamp - adjDate).TotalHours;

                                                var chargeDays = Math.Ceiling((tmpHours - (double)storeRate.FreeTimeDuration) / 24);

                                                //Storage Fee
                                                double storageCharge = 0;
                                                if (chargeDays > 0)
                                                {
                                                    storageCharge = Math.Round(((double)chargeDays * ((double)hItem.Weight * (double)storeRate.DefaultChargeAmount)), 2);
                                                    ttlCharges += Math.Round((decimal)storageCharge,2);

                                                    DischargeCharge dc = new DischargeCharge()
                                                    {

                                                        DischargeDetail = dDetail,
                                                        ChargeType = storeRate,
                                                        ChargeAmount = (decimal)storageCharge

                                                    };
                                                }

                                                //Collect Fee
                                                double colFee = 0;
                                                if (hItem != null)
                                                {
                                                    colFee = Math.Round((double)hItem.CollectFee, 2);
                                                    ttlCharges += Math.Round((decimal)colFee, 2);

                                                    DischargeCharge dc1 = new DischargeCharge()
                                                    {

                                                        DischargeDetail = dDetail,
                                                        ChargeType = colRate,
                                                        ChargeAmount = (decimal)colFee

                                                    };
                                                }

                                                //Import Service Fee
                                                double impFee = 0;
                                                if (impFeeRate.DefaultChargeAmount != null)
                                                {
                                                    impFee = Math.Round((double)impFeeRate.DefaultChargeAmount, 2);
                                                    ttlCharges += Math.Round((decimal)impFee, 2);

                                                    DischargeCharge dc2 = new DischargeCharge()
                                                    {

                                                        DischargeDetail = dDetail,
                                                        ChargeType = impFeeRate,
                                                        ChargeAmount = (decimal)impFee

                                                    };
                                                }

                                                dDetail.AWB = aItem.AWB;

                                                if(dDetail.TotalAmountDue != null)
                                                {
                                                    dDetail.TotalAmountDue += ttlCharges;           
                                                }
                                                else
                                                {
                                                    dDetail.TotalAmountDue = ttlCharges;
                                                }

                                                if(dDetail.Discharge.TotalAmountDue != null)
                                                {
                                                    dDetail.Discharge.TotalAmountDue += ttlCharges;
                                                }
                                                else
                                                {
                                                    dDetail.Discharge.TotalAmountDue = ttlCharges;
                                                }

                                                if(dDetail.Discharge.TotalDeliveryOrders != null)
                                                {
                                                    dDetail.Discharge.TotalDeliveryOrders += 1;
                                                }
                                                else
                                                {
                                                    dDetail.Discharge.TotalDeliveryOrders = 1;
                                                }

                                                dDetail.DisplayText = "HWB# " + hItem.HWBSerialNumber +
                                                            " AWB# " + aItem.AWB.Carrier.Carrier3Code + "-" + aItem.AWB.AWBSerialNumber +
                                                            " SLAC: " + dDetail.TotalPiecesRequested.ToString();

                                                contextApi.DataWorkspace.WFSDBData.SaveChanges();

                                                context.Response.ContentType = "text/plain";
                                                context.Response.Write("1");

                                            }
                                            #endregion

                                            #endregion


                                        }
                                    }
                                }
                            }
                        }
                        else
                        {


                        }
                    }
                }
            }         
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}