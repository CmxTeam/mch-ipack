﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AddDamageTransaction
    /// </summary>
    public class AddDamageTransaction : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var conditions = context.Request["condition"];
            var entityType = Int64.Parse(context.Request["eType"]);   
            var entityId = Int64.Parse(context.Request["entityId"]);
            var count = int.Parse(context.Request["count"]);
            var userName = context.Request["userName"];
            //var taskTypeId = context.Request["taskTypeId"];
            var taskId = context.Request["taskId"];
            var image = context.Request["image"];
            var text = context.Request["txt"];
            var wId = context.Request["wId"];
            var fId = context.Request["fId"];

            DatabaseManager.ExecuteScalar("AddSnapshotImage", new[]
            {                
                new KeyValuePair<string, object>("@Count", count),
                new KeyValuePair<string, object>("@UserName", userName),
                new KeyValuePair<string, object>("@EntityTypeId", entityType),
                new KeyValuePair<string, object>("@EntityId", entityId),
                new KeyValuePair<string, object>("@DisplayText", text),                
            },
            new[] 
            {                
                new Tuple<string, SqlDbType, object>("@Image", SqlDbType.VarBinary, Convert.FromBase64String(image))
            },
            "SnapshotDBData");

            DatabaseManager.ExecuteScalar("AddDamageTransaction", new[] 
            { 
                new KeyValuePair<string, object>("@Conditions", conditions),
                new KeyValuePair<string, object>("@EntityType", entityType),
                new KeyValuePair<string, object>("@EntityId", entityId),
                new KeyValuePair<string, object>("@Count", count),
                new KeyValuePair<string, object>("@UserName", userName),
                //new KeyValuePair<string, object>("@TaskTypeId", taskTypeId),
                new KeyValuePair<string, object>("@TaskId", taskId),
                new KeyValuePair<string, object>("@WarehouseId", wId),
                new KeyValuePair<string, object>("@FlightManifestId", fId)                
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("Success");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}