﻿using LightSwitchApplication.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AssignLocationToAwbs
    /// </summary>
    public class AssignLocationToAwbs : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {            
            var lId = context.Request["LocationId"];
            var itms = context.Request["items"];
            var tid = context.Request["taskId"];
            var userName = context.Request["userName"];

            int locationId;
            long taskId;
            long hwbId;            

            if (!int.TryParse(lId, out locationId)) return;
            if (!long.TryParse(tid, out taskId)) return;

            var items = JArray.Parse(itms).ToObject<List<AssignLocationInformation>>();
            if (items.Count == 0) return;


            foreach (var item in items)
            {
                string iType = item.ForkliftDetailId.ToString().Substring(0, 3);
                string iDetail = item.ForkliftDetailId.ToString().Substring(3);
                if (!long.TryParse(iDetail, out hwbId)) return;

                var itemId = hwbId;

                var parameters = new[]
                        {
                            new KeyValuePair<string, object>("@UserName", userName),
                            new KeyValuePair<string, object>("@ItemId", itemId),
                            new KeyValuePair<string, object>("@TaskId", taskId),
                            new KeyValuePair<string, object>("@LocationId", locationId),
                            new KeyValuePair<string, object>("@Cnt", item.Count),
                            new KeyValuePair<string, object>("@CurrentUser", userName)
                        };

                switch (iType)
                {
                    case "AWB":
                        DatabaseManager.ExecuteScalar("ProcessAwbPutAway", parameters);                        
                        break;
                    case "HWB":
                        DatabaseManager.ExecuteScalar("ProcessHwbPutAway", parameters);                        
                        break;
                    case "ULD":
                        DatabaseManager.ExecuteScalar("ProcessUldPutAway", parameters);                        
                        break;
                }
            }

            var result = DatabaseManager.GetValue(string.Format("Select Sum(Pieces) as P from ForkliftDetails where UserName = '{0}' and TaskId = {1}", userName, tid), "P", "0");

            if(string.IsNullOrEmpty(result))
            {
                result = "0";
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private class AssignLocationInformation
        {
            public string ForkliftDetailId { get; set; }
            public int Count { get; set; }
        }
    }
}