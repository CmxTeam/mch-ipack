﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for InventoryScan
    /// </summary>
    public class InventoryScan : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string tbcScan = context.Request["bcScan"];
            string taskId = context.Request["tId"];
            string locId = context.Request["lId"];

            var result = DatabaseManager.ExecuteScalar("InventoryScan", new[] 
            {
                new KeyValuePair<string, object>("@TaskId", taskId),
                new KeyValuePair<string, object>("@LocationId", locId),
                new KeyValuePair<string, object>("@Scan", tbcScan)   
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write(string.IsNullOrEmpty(result) ? "0" : result);         
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}