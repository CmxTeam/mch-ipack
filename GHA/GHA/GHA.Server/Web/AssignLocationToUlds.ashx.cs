﻿using LightSwitchApplication.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AssignLocationToUlds
    /// </summary>
    public class AssignLocationToUlds : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var lId = context.Request["LocationId"];
            var itms = context.Request["items"];
            var tid = context.Request["taskId"];
            var userName = context.Request["userName"];

            decimal locationId;
            decimal taskId;

            if (!decimal.TryParse(lId, out locationId)) return;
            if (!decimal.TryParse(tid, out taskId)) return;

            var ids = JArray.Parse(itms).ToObject<List<long>>();
            if (ids.Count == 0) return;

            foreach (var id in ids)
            {
                DatabaseManager.ExecuteScalar("AssignLocationToUlds", new[]
                    {
                        new KeyValuePair<string, object>("@UserName", userName),                        
                        new KeyValuePair<string, object>("@TaskId", tid),
                        new KeyValuePair<string, object>("@LocationId", lId),
                        new KeyValuePair<string, object>("@ForkliftDetailId", id)
                    });
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}