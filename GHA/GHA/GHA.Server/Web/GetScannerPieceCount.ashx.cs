﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetScannerPieceCount
    /// </summary>
    public class GetScannerPieceCount : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tTaskId = context.Request["tId"];
            var cUser = context.Request["cUser"];

            var result = DatabaseManager.GetValue(string.Format(@"Select Count(Pieces) as P from ForkliftDetails where TaskId = {0} and UserName = '{1}'", tTaskId, cUser), "P", "0");
            context.Response.ContentType = "text/plain";
            context.Response.Write(result);           
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}