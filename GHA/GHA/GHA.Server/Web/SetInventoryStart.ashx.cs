﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for SetInventoryStart
    /// </summary>
    public class SetInventoryStart : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string tTaskId = context.Request["tId"];
            string cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("SetInventoryStart", new[] 
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@TaskId", tTaskId)                
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}