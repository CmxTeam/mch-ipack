﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for UnAssignLocationToAwbs
    /// </summary>
    public class UnAssignLocationToAwbs : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var iType = context.Request["eType"];
            var dId = context.Request["eId"];
            var tId = context.Request["tId"];
            var tQty = context.Request["uQty"];
            var tFltId = context.Request["fId"];
            var cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("UnAssignLocationToAwbs", new []
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@EntityType", iType),
                new KeyValuePair<string, object>("@DispositionId", dId),
                new KeyValuePair<string, object>("@TaskId", tId),
                new KeyValuePair<string, object>("@Quantity", tQty),
                new KeyValuePair<string, object>("@FlightManifestId", tFltId),
            });
	 
            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }
    
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}