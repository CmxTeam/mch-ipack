﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for FinalizeFlight
    /// </summary>
    public class FinalizeFlight : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            #region Parameters

            string tFlt = context.Request["fId"];
            string tTask = context.Request["tId"];
            string currentUser = context.Request["cUser"];

            #endregion

            #region Variables

            bool success = false;
            long fId;
            long tId;
            int sCtr = 0;

            #endregion

            #region Cast Ids

            success = long.TryParse(tFlt, out fId);

            if (success)
            {
                success = false;
                sCtr++;
            }

            success = long.TryParse(tTask, out tId);

            if (success)
            {
                success = false;
                sCtr++;
            }

            #endregion

            #region Finalize Flight

            if (sCtr == 2)
            {
                Helpers.DatabaseManager.ExecuteScalar("FinalizeFlight", new[] 
                { 
                    new KeyValuePair<string, object>("@FlightId", fId), 
                    new KeyValuePair<string, object>("@TaskId", tId), 
                    new KeyValuePair<string, object>("@CurrentUser", currentUser) 
                });

                context.Response.ContentType = "text/plain";
                context.Response.Write("1");
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("0");
            }

            #endregion
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}