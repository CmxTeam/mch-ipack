﻿using LightSwitchApplication.Helpers;
using System.Collections.Generic;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for UnRecoverFlightULD
    /// </summary>
    public class UnRecoverFlightULD : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tFltId = context.Request["FltId"];
            var tUldId = context.Request["UldId"];
            var cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("UnRecoverFlightULD", new[] 
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@FlightManifestId", tFltId),                
                new KeyValuePair<string, object>("@UldId", tUldId)
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}