﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetTrackingDetails
    /// </summary>
    public class GetTrackingDetails : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string cCode = context.Request["Carrier"];
            string awbNum = context.Request["Awb"];
            string hwbNum = context.Request["Hwb"];

            var result = DatabaseManager.ExecuteScalar("GetTrackingDetails", new[] 
            {
                new KeyValuePair<string, object>("@Carrier", cCode),
                new KeyValuePair<string, object>("@AwbNumber", awbNum),
                new KeyValuePair<string, object>("@HwbNumber", hwbNum)   
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write(string.IsNullOrEmpty(result) ? "0" : result);    
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}