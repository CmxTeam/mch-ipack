﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for RemoveForkliftUlds
    /// </summary>
    public class RemoveForkliftUlds : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var fids = context.Request["items"];
            var forkliftDetailIds = JArray.Parse(fids).ToObject<List<long>>();

            using (var contextApi = ServerApplicationContext.CreateContext())
            {
                var details = (from ForkliftDetail f in contextApi.DataWorkspace.WFSDBData.ForkliftDetails
                              where forkliftDetailIds.Contains(f.Id)
                              select f).ToList();


                foreach (var detail in details)
                    detail.Delete();

                contextApi.DataWorkspace.WFSDBData.SaveChanges();
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}