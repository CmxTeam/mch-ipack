﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AddNewInventoryTask
    /// </summary>
    public class AddNewInventoryTask : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string invName = context.Request["InvName"];
            string cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar(string.Format(@"Insert Into Tasks (TaskTypeId, StatusId, TaskReference, TaskCreationDate, TaskCreator, 
	                                            TaskOwner, TaskDate, StatusTimestamp, WarehouseId)
                Values (2, 17, '{0}', getutcdate(), '{1}', '{1}', getutcdate(), getutcdate(), (Select Top 1 Warehouses.Id from Warehouses Inner Join UserStations on UserStations.PortId = Warehouses.PortId Inner Join
                UserProfiles on UserProfiles.Id = UserStations.UserId Where UserProfiles.UserId = '{1}'))", invName, cUser));

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}