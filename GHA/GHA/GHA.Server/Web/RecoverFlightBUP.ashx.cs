﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for RecoverFlightBUP
    /// </summary>
    public class RecoverFlightBUP : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var tFltId = context.Request["FltId"];
            var tUldId = context.Request["UldId"];
            var currentUser = context.Request["cUser"].ToString();

            Common.RecoverFlightUldAdapter.ProcessUldAsBup(long.Parse(tFltId), long.Parse(tUldId), currentUser);

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}