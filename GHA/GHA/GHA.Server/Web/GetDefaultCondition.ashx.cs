﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetDefaultCondition
    /// </summary>
    public class GetDefaultCondition : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var result = DatabaseManager.GetValue("SELECT dbo.GetDefaultCondition() AS MyCondList", "MyCondList", "");

            context.Response.ContentType = "text/plain";
            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}