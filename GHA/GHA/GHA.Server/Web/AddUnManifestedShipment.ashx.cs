﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AddUnManifestedShipment
    /// </summary>
    public class AddUnManifestedShipment : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var carrier = context.Request["carrier"];
            var awbNumber = context.Request["awbNumber"];
            var awbOriginId = context.Request["awbOriginId"];
            var awbDestinationId = context.Request["awbDestinationId"];
            var hwbNumber = context.Request["hwbNumber"];
            var hwbOriginId = context.Request["hwbOriginId"];
            var hwbDestinationId = context.Request["hwbDestinationId"];
            var quantity = context.Request["quantity"];
            var userName = context.Request["userName"];
            var taskId = context.Request["taskId"];
            var uldId = context.Request["uldId"];
            var manifestId = context.Request["manifestId"];            

            Helpers.DatabaseManager.ExecuteScalar("AddUnManifestedShipment", new [] 
            {
                new KeyValuePair<string, object>("@Carrier", carrier),
                new KeyValuePair<string, object>("@AwbNumber", awbNumber),
                new KeyValuePair<string, object>("@AwbOriginId", awbOriginId),
                new KeyValuePair<string, object>("@AwbDestinationId", awbDestinationId),
                new KeyValuePair<string, object>("@HwbNumber", hwbNumber),
                new KeyValuePair<string, object>("@HwbOriginId", hwbOriginId),
                new KeyValuePair<string, object>("@HwbDestinationId", hwbDestinationId),
                new KeyValuePair<string, object>("@Quantity", quantity),
                new KeyValuePair<string, object>("@UserName", userName),
                new KeyValuePair<string, object>("@SelectedTaskId", taskId),
                new KeyValuePair<string, object>("@UldId", uldId),
                new KeyValuePair<string, object>("@ManifestId", manifestId) 
            });
                 
            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}