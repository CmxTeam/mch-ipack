﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AssignInventoryTask
    /// </summary>
    public class AssignInventoryTask : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string invName = context.Request["iName"];
            string cUser = context.Request["sUser"];

            DatabaseManager.ExecuteScalar("UnRecoverFlightULD", new[] 
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@InventoryName", invName)                
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}