﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AddForkliftUlds
    /// </summary>
    public class AddForkliftUlds : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region Parameters

            var tEntityTypeId = context.Request["eTypeId"];
            var tEntityId = context.Request["eId"];
            var tTaskId = context.Request["tId"];

            #endregion

            #region Variables

            bool success = false;
            int eTypeId;
            long eId;
            long tId;
            string cUser;

            DateTime tStamp;
            int sCtr = 0;

            #endregion

            #region Cast Id's and Qty

            success = long.TryParse(tTaskId, out tId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tEntityTypeId, out eTypeId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = long.TryParse(tEntityId, out eId);

            if (success)
            {
                sCtr++;
                success = false;
            }


            #endregion

            if (sCtr == 3)
            {

                using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
                {

                    #region Get Current User and create Timestamp

                    cUser = contextApi.Application.User.PersonId;
                    tStamp = DateTime.UtcNow;

                    #endregion

                    #region Get Queries

                    var fTask = (from t in contextApi.DataWorkspace.WFSDBData.Tasks.GetQuery().Execute()
                                 where t.Id == tId
                                 select t).FirstOrDefault();

                    #endregion

                    if (fTask != null)
                    {
                        switch (eTypeId)
                        {
                            case 1:
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                            case 4:

                                var nUld = (from u in contextApi.DataWorkspace.WFSDBData.ULDs.GetQuery().Execute()
                                            where u.Id == eId
                                            select u).FirstOrDefault();

                                var forkliftItem = (from fl in contextApi.DataWorkspace.WFSDBData.ForkliftDetails.GetQuery().Execute()
                                                    where fl.ULD.Id == eId && fl.Task.Id == tId && fl.UserName == cUser
                                                    select fl).FirstOrDefault();

                                if (forkliftItem == null)
                                {
                                    ForkliftDetail forkliftPiece = new ForkliftDetail()
                                    {
                                        ULD = nUld,
                                        Pieces = 1,
                                        Task = fTask,
                                        UserName = cUser
                                    };
                                }
                                else
                                {
                                    if (forkliftItem.Pieces.HasValue)
                                    {
                                        forkliftItem.Pieces += 1;
                                    }
                                }

                                contextApi.DataWorkspace.WFSDBData.SaveChanges();

                                context.Response.ContentType = "text/plain";
                                context.Response.Write("1");

                                break;

                        }
                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("0");
                    }

                }

            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("0");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}