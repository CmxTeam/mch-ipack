﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for PutAwayULD
    /// </summary>
    public class PutAwayULD : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tmpFltId = context.Request["FltId"];
            var tmpUldId = context.Request["ULdId"];
            var tmpSLoc = context.Request["uLoc"];
            var cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("PutAwayULD", new[] 
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@UldId", tmpUldId),
                new KeyValuePair<string, object>("@FlightManifestId", tmpFltId),
                new KeyValuePair<string, object>("@LocationId", tmpSLoc)
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}