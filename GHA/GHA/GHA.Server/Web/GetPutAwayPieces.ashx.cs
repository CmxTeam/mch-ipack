﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetPutAwayPieces
    /// </summary>
    public class GetPutAwayPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tFltId = context.Request["FltId"];
            var tHwbId = context.Request["HwbId"];

            var result = DatabaseManager.GetValue(string.Format("Select Top 1 isnull(Pieces, 0) - (isnull(PutAwayPieces, 0) + isnull(ForkliftPieces, 0)) as P from HWBs where Id = {0}", tHwbId), "P", "0");
            context.Response.ContentType = "text/plain";
            context.Response.Write(result);
        }
                   
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}