﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for RecoverFlightULDsBup
    /// </summary>
    public class RecoverFlightULDsBup : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var flightId = context.Request["FltId"];
            var uldIds = JArray.Parse(context.Request["UldIds"]);
            var currentUser = context.Request["cUser"];

            foreach (var id in uldIds)
            {
                Common.RecoverFlightUldAdapter.ProcessUldAsBup(long.Parse(flightId), long.Parse(id.ToString()), currentUser.ToString());
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}