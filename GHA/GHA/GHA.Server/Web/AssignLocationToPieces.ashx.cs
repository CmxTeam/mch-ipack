﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Microsoft.LightSwitch;
using LightSwitchApplication.Helpers;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AssignLocationToPieces
    /// </summary>
    public class AssignLocationToPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var lId = context.Request["LocationId"];
            var itms = context.Request["items"];
            var tid = context.Request["taskId"];
            var userName = context.Request["userName"];

            decimal locationId;
            decimal taskId;

            if (!decimal.TryParse(lId, out locationId)) return;
            if (!decimal.TryParse(tid, out taskId)) return;

            var items = JArray.Parse(itms).ToObject<List<AssignLocationInformation>>();
            if (items.Count == 0) return;
            foreach (var item in items)
            {
                DatabaseManager.ExecuteScalar("AssignLocationToPieces", new[]
                    {
                        new KeyValuePair<string, object>("@UserName", userName),
                        new KeyValuePair<string, object>("@ForkliftDetailId", item.ForkliftDetailId),
                        new KeyValuePair<string, object>("@TaskId", tid),
                        new KeyValuePair<string, object>("@LocationId", lId),
                        new KeyValuePair<string, object>("@Cnt", item.Count)
                    });
            }

            var result = DatabaseManager.GetValue(string.Format("Select Sum(Pieces) as P from ForkliftDetails where UserName = '{0}' and TaskId = {1}", userName, tid), "P", "0");

            if(string.IsNullOrEmpty(result))
            {
                result = "0";
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(result);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private class AssignLocationInformation
        {
            public decimal ForkliftDetailId { get; set; }
            public int Count { get; set; }
        }
    }
}