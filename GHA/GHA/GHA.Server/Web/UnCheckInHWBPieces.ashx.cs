﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for UnCheckInHWBPieces
    /// </summary>
    public class UnCheckInHWBPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tFltId = context.Request["FltId"];
            var tMawbId = context.Request["MawbId"];
            var tMhwbId = context.Request["MhwbId"];
            var tCheckQty = context.Request["cQty"];
            var cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("UnCheckInHWBPieces", new[]
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@FlightManifestId", tFltId),
                new KeyValuePair<string, object>("@ManifestAwbDetailId", tMawbId),
                new KeyValuePair<string, object>("@ManifestDetailId", tMhwbId),
                new KeyValuePair<string, object>("@Quantity", tCheckQty)
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}