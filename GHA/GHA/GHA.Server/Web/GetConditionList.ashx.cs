﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetConditionList
    /// </summary>
    public class GetConditionList : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string hCode = "";
            
            var result = SSDatabaseManager.GetValue("SELECT dbo.GetSSConditionList() AS MyCondList", "MyCondList", "");
            
            if(!string.IsNullOrEmpty(result))
            {
                var firstItem = "";
                var otherItems = "";
                var cList = result.Split(',');
                hCode = "<div id=\"conditionlist\" class=\"condContainer\"><fieldset data-role=\"controlgroup\" data-type=\"vertical\">";

                for (int i = 0; i < cList.Length; i++)
                {

                        if (cList[i] == "5")
                        {
                            firstItem += "<input type=\"checkbox\" name=\"" + cList[i + 1] + "\" id=\"" + cList[i + 1] + "\" value=\"" + cList[i] + "\" class=\"mycheckbox\" />" +
                        "<label for=\"" + cList[i+1] + "\">" + cList[i+1] + "</label>";
                        }
                        else
                        {
                            otherItems += "<input type=\"checkbox\" name=\"" + cList[i + 1] + "\" id=\"" + cList[i + 1] + "\" value=\"" + cList[i] + "\" class=\"mycheckbox\" />" +
                                                "<label for=\"" + cList[i + 1] + "\">" + cList[i + 1] + "</label>";
                        }
                        i += 1;
                }

                hCode += firstItem + otherItems;
                hCode += "</fieldset></<div>";
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(hCode); 
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}