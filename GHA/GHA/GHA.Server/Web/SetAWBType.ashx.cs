﻿using LightSwitchApplication.Helpers;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for SetAWBType
    /// </summary>
    public class SetAWBType : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var tAwbId = context.Request["AwbId"];
            var tTypeId = context.Request["TypeId"];

            DatabaseManager.ExecuteScalar(string.Format("Update Awbs Set IsTransfer = {1} Where Id = {0}", tAwbId, tTypeId));
            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}