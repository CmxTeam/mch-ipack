﻿using LightSwitchApplication.Helpers;
using System.Collections.Generic;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for AddScannerPieces
    /// </summary>
    public class AddScannerPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string tType = context.Request["eType"];
            string tEId = context.Request["eId"];
            string tTId = context.Request["tId"];
            string tQty = context.Request["sQty"];
            string cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("AddScannerPieces", new[] 
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@TaskType", tType),                
                new KeyValuePair<string, object>("@EntityId", tEId),
                new KeyValuePair<string, object>("@TaskId", tTId),
                new KeyValuePair<string, object>("@Quantity", tQty)
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");             
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}