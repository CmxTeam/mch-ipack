﻿using LightSwitchApplication.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for ScanPutAwayPieces
    /// </summary>
    public class ScanPutAwayPieces : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region Parameters

            var tbcScan = context.Request["bcScan"];
            var tTaskId = context.Request["tId"];
            var tFlt = context.Request["fId"];
            var cUser = context.Request["cUser"];

            #endregion

            #region Variables

            bool success = false;            
            long tId;
            long fId;            

            int sCtr = 0;

            #endregion

            #region Cast Task Id

            success = long.TryParse(tTaskId, out tId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = long.TryParse(tFlt, out fId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            #endregion

            if (sCtr == 2)
            {
                var result = DatabaseManager.ExecuteScalar("ScanPutAwayPieces", new[]
                        {
                            new KeyValuePair<string, object>("@UserName", cUser),
                            new KeyValuePair<string, object>("@TaskId", tId),
                            new KeyValuePair<string, object>("@FlightManifestId", fId),
                            new KeyValuePair<string, object>("@Scan", tbcScan)                            
                        });
                context.Response.ContentType = "text/plain";
                context.Response.Write(result); 
            }

            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("F");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }        
    }
}