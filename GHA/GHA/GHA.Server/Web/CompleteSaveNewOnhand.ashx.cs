﻿using LightSwitchApplication.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using System.Web.Http;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for CompleteSaveNewOnhand
    /// </summary>
    public class CompleteSaveNewOnhand : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region Parameters

            var tAccountId = context.Request["accountId"];
            var truckerPro = context.Request["truckerPro"];
            var tCarrierId = context.Request["carrierId"];
            var tDriverId = context.Request["driverId"];
            var tIdType1 = context.Request["idType1"];
            var tMatchingPhoto1 = context.Request["matchingPhoto1"];
            var tIdType2 = context.Request["idType2"];
            var tMatchingPhoto2 = context.Request["matchingPhoto2"];
            var tTotalPieces = context.Request["totalPieces"];
            var tTotalWeight = context.Request["totalWeight"];
            var tUomId = context.Request["uomId"];
            var tReceivedPieces = context.Request["receivedPieces"];
            var userName = context.Request["userName"];
            var tWarehouseId = context.Request["warehouseId"];
            var tOnhandId = context.Request["onhandId"];
            var image = context.Request["sigImage"];

            #endregion

            #region Variables

            bool success = false;
            long driverId;
            int accountId;
            int carrierId;
            int idType1;
            int? idType2;
            int idType2_1;
            bool matchingPhoto1;
            bool? matchingPhoto2;
            bool matchingPhoto2_1;
            int totalPieces;
            float totalWeight;
            int uomId;
            int receivedPieces;
            int warehouseId;
            long onhandId;


            int sCtr = 0;

            #endregion

            #region Cast Task Id

            success = long.TryParse(tDriverId, out driverId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tAccountId, out accountId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tCarrierId, out carrierId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tIdType1, out idType1);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tIdType2, out idType2_1);

            if (success)
            {
                idType2 = idType2_1;
                sCtr++;
                success = false;
            }
            else
            {
                idType2 = null;
            }

            success = bool.TryParse(tMatchingPhoto1, out matchingPhoto1);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = bool.TryParse(tMatchingPhoto2, out matchingPhoto2_1);

            if (success)
            {
                matchingPhoto2 = matchingPhoto2_1;
                sCtr++;
                success = false;
            }
            else
            {
                matchingPhoto2 = null;
            }

            success = int.TryParse(tTotalPieces, out totalPieces);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = float.TryParse(tTotalWeight, out totalWeight);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tUomId, out uomId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tReceivedPieces, out receivedPieces);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = int.TryParse(tWarehouseId, out warehouseId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            success = long.TryParse(tOnhandId, out onhandId);

            if (success)
            {
                sCtr++;
                success = false;
            }

            #endregion

            #region Save Image

            var startIndex = image.IndexOf("base64");
            image = image.Substring(startIndex + 7);
            byte[] bytes = Convert.FromBase64String(image);

            Image img;
            string fileName;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                img = Image.FromStream(ms);
                var datePath = DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss");
                fileName = (new Random().Next()) + "_" + datePath + ".png";
                var fileFullName = ConfigurationManager.AppSettings["SignatureImagePath"] + fileName;
                img.Save(fileFullName, System.Drawing.Imaging.ImageFormat.Png);
            }

            var signPath = ConfigurationManager.AppSettings["SignatureRelativeImagePath"] + fileName;

            #endregion

            if (sCtr == 11 || sCtr == 13)
            {
                var result = DatabaseManager.ExecuteScalar("SaveOnhandReceipt", new[] 
            {
                new KeyValuePair<string, object>("@AccountId", accountId),
                new KeyValuePair<string, object>("@TruckerPro", truckerPro),
                new KeyValuePair<string, object>("@CarrierId", carrierId),
                new KeyValuePair<string, object>("@DriverId", driverId),
                new KeyValuePair<string, object>("@DriverID1TypeId", idType1),
                new KeyValuePair<string, object>("@MatchingPhoto1", matchingPhoto1),
                new KeyValuePair<string, object>("@DriverID2TypeId", idType2),
                new KeyValuePair<string, object>("@MatchingPhoto2", matchingPhoto2),
                new KeyValuePair<string, object>("@TotalPieces", totalPieces),  
                new KeyValuePair<string, object>("@TotalWeight", totalWeight),
                new KeyValuePair<string, object>("@WeightUOMId", uomId),
                new KeyValuePair<string, object>("@ReceivedPieces", receivedPieces),
                new KeyValuePair<string, object>("@UserName", userName),
                new KeyValuePair<string, object>("@WarehouseId ", warehouseId), 
                new KeyValuePair<string, object>("@OnhandId ", onhandId),  
                new KeyValuePair<string, object>("@SignaturePath ", signPath)  
                });
                context.Response.ContentType = "text/plain";
                context.Response.Write(result);
            }

            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("F");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}