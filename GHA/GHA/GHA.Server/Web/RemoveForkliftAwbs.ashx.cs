﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for RemoveForkliftAwbs
    /// </summary>
    public class RemoveForkliftAwbs : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var fId = context.Request["ForkliftDetailId"];
            var c = context.Request["Count"];
            var cUser = context.Request["cUser"];

            //string forkliftDetailId = fId;
            long eId = 0;
            int count;
            if (!int.TryParse(c, out count) || count <= 0)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("0");
                return;
            }

            string eType = fId.Substring(0, 3);
            string entityId = fId.Substring(3);


            DatabaseManager.ExecuteScalar("RemoveForkliftAwbs", new []
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@EntityId", entityId),
                new KeyValuePair<string, object>("@Quantity", c),
                new KeyValuePair<string, object>("@EntityType", eType)
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}