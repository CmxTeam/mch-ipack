﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for CloseInventoryLocation
    /// </summary>
    public class CloseInventoryLocation : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string tTaskId = context.Request["tId"];
            string tLocId = context.Request["sLocId"];
            string cUser = context.Request["cUser"];

            DatabaseManager.ExecuteScalar("ProcessInventory", new[] 
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@TaskId", tTaskId),                
                new KeyValuePair<string, object>("@LocationId", tLocId)                
            });

            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}