﻿using LightSwitchApplication.Helpers;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetForkliftPieceCount
    /// </summary>
    public class GetForkliftPieceCount : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var tTaskId = context.Request["tId"];
            var cUser = context.Request["cUser"];

            var result = DatabaseManager.GetValue(string.Format("Select Sum(Pieces) as P from ForkliftDetails where UserName = '{0}' and TaskId = {1}", cUser, tTaskId), "P", "0");
            if(string.IsNullOrEmpty(result))
            {
                result = "0";
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write(result);            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}