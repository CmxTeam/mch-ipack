﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Web
{
    /// <summary>
    /// Summary description for GetDischargeTotals
    /// </summary>
    public class GetDischargeTotals : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            long dischargeId;
            bool success;
            decimal ttlCharges = 0;
            decimal ttlPaid = 0;
            decimal ttlDue = 0;
            var result = "";

            var tDischarge = context.Request["dId"];

            success = long.TryParse(tDischarge, out dischargeId);

            if(success)
            {
                using (ServerApplicationContext contextApi = ServerApplicationContext.CreateContext())
                {
                    var sDischargeDetails = from d in contextApi.DataWorkspace.WFSDBData.DischargeDetails.GetQuery().Execute()
                                     where d.Discharge.Id == dischargeId
                                     select d;

                    if(sDischargeDetails != null)
                    {
                        foreach(DischargeDetail dd in sDischargeDetails)
                        {
                            foreach(DischargeCharge dc in dd.DischargeCharges)
                            {
                                if (dc.ChargeAmount != null)
                                {
                                    ttlCharges += (decimal)dc.ChargeAmount;
                                }
                                if (dc.ChargePaid != null)
                                {
                                    ttlPaid += (decimal)dc.ChargePaid;
                                }
                            }
                        }
                    }

                    ttlDue = ttlCharges - ttlPaid;

                    result = ttlCharges + "," + ttlPaid + "," + ttlDue;

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(result);
                }
            }

            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}