﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LightSwitchApplication
{
    /// <summary>
    /// Defines the names of the application permissions.
    /// </summary>
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public static class Permissions
    {
        /// <summary>
        /// Provides the ability to manage security for the application.
        /// </summary>
        public const string SecurityAdministration = global::Microsoft.LightSwitch.Security.ApplicationPermissions.SecurityAdministration;
        /// <summary>
        /// Provides the ability to modify records in Cargo Receiver.
        /// </summary>
        public const string ModifyCargoReceiver = "LightSwitchApplication:ModifyCargoReceiver";
        /// <summary>
        /// Provides the ability to view records only in Cargo Receiver.
        /// </summary>
        public const string ReadOnlyCargoReceiver = "LightSwitchApplication:ReadOnlyCargoReceiver";
        /// <summary>
        /// Provides the ability to modify records in Cargo Discharge.
        /// </summary>
        public const string ModifyCargoDischarge = "LightSwitchApplication:ModifyCargoDischarge";
        /// <summary>
        /// Provides the ability to view records only in Cargo Discharge.
        /// </summary>
        public const string ReadOnlyCargoDischarge = "LightSwitchApplication:ReadOnlyCargoDischarge";
        /// <summary>
        /// Provides the ability to modify records in Cargo Inventory.
        /// </summary>
        public const string ModifyCargoInventory = "LightSwitchApplication:ModifyCargoInventory";
        /// <summary>
        /// Provides the ability to view records only in Cargo Inventory.
        /// </summary>
        public const string ReadOnlyCargoInventory = "LightSwitchApplication:ReadOnlyCargoInventory";
        /// <summary>
        /// Provides the ability to modify settings in Customer Track and Trace.
        /// </summary>
        public const string ModifyCustomerTrackTrace = "LightSwitchApplication:ModifyCustomerTrackTrace";
        /// <summary>
        /// Provides the ability to view records only in Customer Track & Trace.
        /// </summary>
        public const string ReadOnlyCustomerTrackTrace = "LightSwitchApplication:ReadOnlyCustomerTrackTrace";
        /// <summary>
        /// Provides the ability to modify records in Cargo Task Manager.
        /// </summary>
        public const string ModifyCargoTaskManager = "LightSwitchApplication:ModifyCargoTaskManager";
        /// <summary>
        /// Provides the ability to view records only in Cargo Task Manager.
        /// </summary>
        public const string ReadOnlyCargoTaskManager = "LightSwitchApplication:ReadOnlyCargoTaskManager";
        /// <summary>
        /// Provides the ability to modify records in System Configuration.
        /// </summary>
        public const string ModifySystemConfig = "LightSwitchApplication:ModifySystemConfig";
        /// <summary>
        /// Provides the ability to view records only in System Configuration.
        /// </summary>
        public const string ReadOnlySystemConfig = "LightSwitchApplication:ReadOnlySystemConfig";
        /// <summary>
        /// Provides the ability to assign inventory tasks in Task Manager.
        /// </summary>
        public const string AssignInventoryTasks = "LightSwitchApplication:AssignInventoryTasks";
        /// <summary>
        /// Provides the ability to modify records in Discharge Check In.
        /// </summary>
        public const string ModifyDischargeCheckIn = "LightSwitchApplication:ModifyDischargeCheckIn";
        /// <summary>
        /// Provides the ability to view records only in Discharge Check In.
        /// </summary>
        public const string ReadOnlyDischargeCheckIn = "LightSwitchApplication:ReadOnlyDischargeCheckIn";
        /// <summary>
        /// Provides the ability to modify records in Cargo Snapshot.
        /// </summary>
        public const string ModifyCargoSnapshot = "LightSwitchApplication:ModifyCargoSnapshot";
        /// <summary>
        /// Provides the ability to view records only in Cargo Snapshot.
        /// </summary>
        public const string ReadOnlyCargoSnapshot = "LightSwitchApplication:ReadOnlyCargoSnapshot";
        /// <summary>
        /// Provides the ability to modify flight ETA in Cargo Receiver.
        /// </summary>
        public const string ModifyFlightETA = "LightSwitchApplication:ModifyFlightETA";
        /// <summary>
        /// Provides the ability to modify records in Onhand Receipt.
        /// </summary>
        public const string ModifyOnhandReceipt = "LightSwitchApplication:ModifyOnhandReceipt";
        /// <summary>
        /// Provides the ability to view records only in Onhand Receipt.
        /// </summary>
        public const string ReadOnlyOnhandReceipt = "LightSwitchApplication:ReadOnlyOnhandReceipt";

        /// <summary>
        /// Gets all permissions defined for the application.  This includes system and user-defined permissions.
        /// </summary>
        public static global::System.Collections.ObjectModel.ReadOnlyCollection<string> AllPermissions { get { return global::Microsoft.LightSwitch.Security.ApplicationPermissions.AllPermissions; } }
    }
}
