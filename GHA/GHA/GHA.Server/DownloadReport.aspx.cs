﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LightSwitchApplication.Reporting;
using Microsoft.Reporting.WebForms;

namespace LightSwitchApplication
{
    public partial class DownloadReport : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var rp = new ReportProvider();
                uxReportViewer.ServerReport.ReportServerCredentials = rp.Credentials;
                uxReportViewer.ServerReport.ReportServerUrl = rp.ReportServerUrl;
                uxReportViewer.ServerReport.ReportPath = "/GHA/InventoryReport";                
                uxReportViewer.ServerReport.Refresh();
            }
        }

        private void RenderReport(string reportName)
        {

        }
    }
}