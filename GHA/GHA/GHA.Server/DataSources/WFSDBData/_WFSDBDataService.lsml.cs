﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.LightSwitch;
using Microsoft.LightSwitch.Security.Server;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace LightSwitchApplication
{
    public partial class WFSDBDataService
    {
        partial void FlightHWBs_PreprocessQuery(long? FlightManifestId, ref IQueryable<HWB> query)
        {
            if (FlightManifestId != null)
            {
                List<HWB> hList = new List<HWB>();
                long[] hId;
                int hctr = 0;

                var mDetails = this.ManifestDetails.Where(a => a.FlightManifest.Id == FlightManifestId);

                if (mDetails != null)
                {

                    foreach (ManifestDetail md in mDetails)
                    {
                        HWB hb = md.HWB;
                        if (md.AWB.IsTransfer == 1)
                        {
                            if (!hList.Contains(hb))
                            {
                                hList.Add(hb);
                            }
                        }
                    }

                    hId = new long[hList.Count()];

                    foreach (HWB h in hList)
                    {
                        hId[hctr] = h.Id;
                        hctr++;
                    }

                    query = query.Where(b => hId.Contains(b.Id));

                }
            }
        }

        partial void FlightHistory_PreprocessQuery(long? FlightManifestId, ref IQueryable<Transaction> query)
        {
            if (FlightManifestId != null)
            {
                var recList = GetData<long, long?>(FlightManifestId, "@FlightManifestId", "GetFlightHistory", "Id");
                query = query.Where(h => recList.Contains(h.Id));
            }
        }

        partial void UldHistory_PreprocessQuery(long? FlightManifestId, long? UldId, ref IQueryable<Transaction> query)
        {
            if (FlightManifestId != null && UldId != null)
            {
                List<long> recList = new List<long>();
                List<long> awbList = new List<long>();
                List<long> hwbList = new List<long>();
                List<long> uldList = new List<long>();

                var eList = this.EntityTypes;

                foreach (EntityType et in eList)
                {

                    switch (et.Id)
                    {

                        case 2:

                            var aList = this.ManifestAWBDetails.Where(b => b.FlightManifest.Id == FlightManifestId && b.ULD.Id == UldId);
                            if (aList != null)
                            {
                                foreach (ManifestAWBDetail aId in aList)
                                {
                                    if (!awbList.Contains(aId.AWB.Id))
                                    {

                                        awbList.Add(aId.AWB.Id);

                                    }
                                }
                            }

                            var tAWB = this.Transactions.Where(c => awbList.Contains((long)c.EntityId) && c.EntityType.Id == et.Id);
                            if (tAWB != null)
                            {
                                foreach (Transaction ta in tAWB)
                                {
                                    if (!recList.Contains(ta.Id))
                                    {

                                        recList.Add(ta.Id);

                                    }
                                }
                            }

                            break;
                        case 3:

                            var hList = this.ManifestDetails.Where(d => d.FlightManifest.Id == FlightManifestId && d.ULD.Id == UldId);
                            if (hList != null)
                            {
                                foreach (ManifestDetail hId in hList)
                                {
                                    if (!hwbList.Contains(hId.HWB.Id))
                                    {

                                        hwbList.Add(hId.HWB.Id);

                                    }
                                }
                            }

                            var tHWB = this.Transactions.Where(e => hwbList.Contains((long)e.EntityId) && e.EntityType.Id == et.Id);
                            if (tHWB != null)
                            {
                                foreach (Transaction th in tHWB)
                                {
                                    if (!recList.Contains(th.Id))
                                    {

                                        recList.Add(th.Id);

                                    }
                                }
                            }

                            break;
                        case 4:

                            var tULD = this.Transactions.Where(g => g.EntityId == UldId && g.EntityType.Id == et.Id);
                            if (tULD != null)
                            {
                                foreach (Transaction th in tULD)
                                {
                                    if (!recList.Contains(th.Id))
                                    {

                                        recList.Add(th.Id);

                                    }
                                }
                            }

                            break;
                        default:
                            break;

                    }
                }

                query = query.Where(h => recList.Contains(h.Id));

            }
        }

        partial void AwbHistory_PreprocessQuery(long? FlightManifestId, long? AwbId, ref IQueryable<Transaction> query)
        {

            if (FlightManifestId != null && AwbId != null)
            {
                List<long> recList = new List<long>();
                List<long> awbList = new List<long>();
                List<long> hwbList = new List<long>();

                var eList = this.EntityTypes;

                foreach (EntityType et in eList)
                {

                    switch (et.Id)
                    {
                        case 2:

                            var tAWB = this.Transactions.Where(c => c.EntityId == AwbId && c.EntityType.Id == et.Id);
                            if (tAWB != null)
                            {
                                foreach (Transaction ta in tAWB)
                                {
                                    if (!recList.Contains(ta.Id))
                                    {

                                        recList.Add(ta.Id);

                                    }
                                }
                            }

                            break;
                        case 3:

                            var hList = this.ManifestDetails.Where(d => d.FlightManifest.Id == FlightManifestId && d.AWB.Id == AwbId);
                            if (hList != null)
                            {
                                foreach (ManifestDetail hId in hList)
                                {
                                    if (!hwbList.Contains(hId.HWB.Id))
                                    {

                                        hwbList.Add(hId.HWB.Id);

                                    }
                                }
                            }

                            var tHWB = this.Transactions.Where(e => hwbList.Contains((long)e.EntityId) && e.EntityType.Id == et.Id);
                            if (tHWB != null)
                            {
                                foreach (Transaction th in tHWB)
                                {
                                    if (!recList.Contains(th.Id))
                                    {

                                        recList.Add(th.Id);

                                    }
                                }
                            }

                            break;

                        default:
                            break;

                    }

                }

                query = query.Where(h => recList.Contains(h.Id));

            }

        }

        partial void AllDischargeCharges_PreprocessQuery(long? DischargeId, ref IQueryable<DischargeCharge> query)
        {
            if (DischargeId != null)
            {
                List<long> dList = new List<long>();
                List<long> cList = new List<long>();
                var dDetails = this.DischargeDetails.Where(a => a.Discharge.Id == DischargeId);
                if (dDetails != null)
                {

                    foreach (DischargeDetail dd in dDetails)
                    {

                        if (!dList.Contains(dd.Id))
                        {
                            dList.Add(dd.Id);
                        }

                    }

                    if (dList.Count() > 0)
                    {

                        var cDetails = this.DischargeCharges.Where(b => dList.Contains(b.DischargeDetail.Id));
                        if (cDetails != null)
                        {

                            foreach (DischargeCharge dc in cDetails)
                            {

                                if (!cList.Contains(dc.Id))
                                {

                                    cList.Add(dc.Id);

                                }

                            }

                        }

                        if (cList.Count() > 0)
                        {

                            query = query.Where(c => cList.Contains(c.Id));

                        }
                        else
                        {

                            query = query.Where(d => d.Id == -1);

                        }
                    }
                    else
                    {

                        query = query.Where(d => d.Id == -1);

                    }
                }
                else
                {

                    query = query.Where(d => d.Id == -1);

                }
            }
            else
            {
                query = query.Where(d => d.Id == -1);
            }
        }

        partial void AllDischargePayments_PreprocessQuery(long? DischargeId, ref IQueryable<DischargePayment> query)
        {
            //if (DischargeId != null)
            //{
            //    List<long> dList = new List<long>();
            //    List<long> cList = new List<long>();
            //    var dDetails = this.DischargeDetails.Where(a => a.Discharge.Id == DischargeId);
            //    if (dDetails != null)
            //    {

            //        foreach (DischargeDetail dd in dDetails)
            //        {

            //            if (!dList.Contains(dd.Id))
            //            {
            //                dList.Add(dd.Id);
            //            }

            //        }

            //        if (dList.Count() > 0)
            //        {

            //            var cDetails = this.DischargePayments.Where(b => dList.Contains(b.DischargeCharge.DischargeDetail.Id));
            //            if (cDetails != null)
            //            {

            //                foreach (DischargePayment dp in cDetails)
            //                {

            //                    if (!cList.Contains(dp.Id))
            //                    {

            //                        cList.Add(dp.Id);

            //                    }

            //                }

            //            }

            //            if (cList.Count() > 0)
            //            {

            //                query = query.Where(c => cList.Contains(c.Id));

            //            }
            //            else
            //            {

            //                query = query.Where(d => d.Id == -1);

            //            }
            //        }
            //        else
            //        {

            //            query = query.Where(d => d.Id == -1);

            //        }
            //    }
            //    else
            //    {

            //        query = query.Where(d => d.Id == -1);

            //    }
            //}
            //else
            //{

            //    query = query.Where(d => d.Id == -1);

            //}
        }

        partial void DischargeAWBs_PreprocessQuery(long? DischargeId, ref IQueryable<AWB> query)
        {
            if (DischargeId != null)
            {
                var dDetails = this.DataWorkspace.WFSDBData.DischargeDetails.Where(a => a.Discharge.Id == DischargeId && a.HWB.Id == null);

                if (dDetails != null)
                {
                    List<long> aList = new List<long>();

                    foreach (DischargeDetail dd in dDetails)
                    {

                        if (!aList.Contains(dd.AWB.Id))
                        {

                            aList.Add(dd.AWB.Id);

                        }

                    }

                    query = query.Where(b => aList.Contains(b.Id));
                }
            }
            else
            {

                query = null;

            }
        }

        partial void DischargeHWBs_PreprocessQuery(long? DischargeId, ref IQueryable<HWB> query)
        {
            if (DischargeId != null)
            {
                var dDetails = this.DataWorkspace.WFSDBData.DischargeDetails.Where(a => a.Discharge.Id == DischargeId);

                if (dDetails != null)
                {
                    List<long> hList = new List<long>();

                    foreach (DischargeDetail dd in dDetails)
                    {

                        if (!hList.Contains(dd.HWB.Id))
                        {

                            hList.Add(dd.HWB.Id);

                        }

                    }

                    query = query.Where(b => hList.Contains(b.Id));
                }

            }
            else
            {

                query = null;

            }
        }

        //partial void FlightList_PreprocessQuery(ref IQueryable<UnloadingPort> query)
        //{
        //    List<long> fltIdList = new List<long>();

        //    #region Get Current Logged-In User

        //    var cUser = this.Application.User.PersonId;
        //    if (cUser != null)
        //    {
        //        #region Get User's Profile

        //        var uProfile = this.DataWorkspace.WFSDBData.UserProfiles.Where(a => a.UserId == cUser).FirstOrDefault();
        //        if (uProfile != null)
        //        {

        //            #region Get User's Default Station

        //            var uStation = this.DataWorkspace.WFSDBData.UserStations.Where(b => b.UserProfile.Id == uProfile.Id && b.IsDefault == true).FirstOrDefault();
        //            if (uStation != null)
        //            {
        //                DateTime EndOfDay = DateTime.Today.AddDays(1);
        //                query = query.Where(a => a.Port.Id == uStation.Port.Id && a.FlightManifest.IsComplete == true && a.POU_ETA <= EndOfDay && a.FlightManifest.Status.Id != 10);

        //            }

        //            #endregion

        //        }

        //        #endregion

        //    }

        //    #endregion
        //}

        partial void FlightULDs_PreprocessQuery(int? UnloadingPortId, long? FlightManifestId, ref IQueryable<ULD> query)
        {

            if (UnloadingPortId.HasValue && FlightManifestId.HasValue)
            {
                List<long> uList = new List<long>();

                var mDetails = this.DataWorkspace.WFSDBData.ManifestAWBDetails.Where(a => a.UnloadingPort.Id == UnloadingPortId && a.FlightManifest.Id == FlightManifestId);

                if (mDetails != null)
                {

                    foreach (ManifestAWBDetail mad in mDetails)
                    {

                        if (!uList.Contains(mad.ULD.Id))
                        {
                            uList.Add(mad.ULD.Id);
                        }

                    }

                    query = query.Where(b => uList.Contains(b.Id));

                }


            }
        }

        partial void FlightManifestList_PreprocessQuery(bool? IsComplete, ref IQueryable<UnloadingPort> query)
        {            
            #region Get Current Logged-In User

            var cUser = this.Application.User.PersonId;
            if (cUser != null)
            {
                #region Get User's Default Station

                var uStation = GetData<int, string>(cUser, "@UserId", "GetPortIds", "PortId").FirstOrDefault();
                if (uStation != 0)
                {
                    DateTime EndOfDay = DateTime.Today.AddDays(1);
                    if (IsComplete == true)
                    {
                        query = query.Where(a => a.Port.Id == uStation && a.FlightManifest.IsComplete == true && a.POU_ETA <= EndOfDay && a.FlightManifest.Status.Id > 9);
                    }
                    else
                    {
                        query = query.Where(a => a.Port.Id == uStation && a.FlightManifest.IsComplete == true && a.POU_ETA <= EndOfDay && a.FlightManifest.Status.Id < 10);
                    }
                }

                #endregion
            }

            #endregion
        }

        partial void FlightAWBs_PreprocessQuery(long? FlightManifestId, ref IQueryable<AWB> query)
        {
            //if (FlightManifestId != null)
            //{
            //    List<AWB> aList = new List<AWB>();
            //    long[] aId;
            //    int actr = 0;

            //    var mDetails = this.ManifestAWBDetails.Where(a => a.FlightManifest.Id == FlightManifestId);

            //    if (mDetails != null)
            //    {

            //        foreach (ManifestAWBDetail md in mDetails)
            //        {
            //            AWB ab = md.AWB;

            //            if (ab.IsTransfer != 1)
            //            {
            //                if (!aList.Contains(ab))
            //                {
            //                    aList.Add(ab);
            //                }
            //            }
            //        }

            //        aId = new long[aList.Count()];

            //        foreach (AWB a in aList)
            //        {
            //            aId[actr] = a.Id;
            //            actr++;
            //        }

            //        query = query.Where(b => aId.Contains(b.Id));

            //    }
            //}
        }

        partial void FlightPutAwayList_PreprocessQuery(long? FlightManifestId, ref IQueryable<PutAwayList> query)
        {
            //var paList = GetData<string, long?>(FlightManifestId, "@ManifestId", "GetFlightPutAwayList", "Pas");
            //    query = query.Where(pa => paList.Contains(pa.PaId) && pa.PutAwayPieces < pa.TotalPieces);
            query = query.Where(pa => pa.FlightManifestId == FlightManifestId && pa.PutAwayPieces < pa.TotalPieces);
        }

        partial void ActiveForkliftView_PreprocessQuery(long? TaskId, ref IQueryable<ForkliftView> query)
        {
            //var paList = GetData<string, long?>(FlightManifestId, "@ManifestId", "GetFlightPutAwayList", "Pas");
            query = query.Where(pa => pa.TaskId == TaskId);
        }

        partial void BreakdownUlds_PreprocessQuery(bool? ShowComplete, long? FlightManifestId, ref IQueryable<ULD> query)
        {
            if (ShowComplete == true)
            {
                query = query.Where(a => a.FlightManifest.Id == FlightManifestId && a.IsBUP == false && a.TotalReceivedCount > 0 && a.ReceivedPieces >= a.TotalPieces);
            }
            else
            {
                query = query.Where(a => a.FlightManifest.Id == FlightManifestId && a.IsBUP == false && a.TotalReceivedCount > 0 && a.ReceivedPieces < a.TotalPieces);
            }
        }

        partial void BreakdownAwbs_PreprocessQuery(bool? ShowComplete, bool? ShowAll, long? UldId, long? FlightManifestId, ref IQueryable<ManifestAWBDetail> query)
        {
            if (ShowAll == true)
            {
                query = query.Where(a => a.FlightManifest.Id == FlightManifestId);
            }
            else
            {
                if (ShowComplete == true)
                {
                    query = query.Where(a => a.FlightManifest.Id == FlightManifestId && a.ULD.Id == UldId && a.ReceivedCount >= a.LoadCount);
                }
                else
                {
                    query = query.Where(a => a.FlightManifest.Id == FlightManifestId && a.ULD.Id == UldId && a.ReceivedCount < a.LoadCount);
                }

            }
        }

        partial void BreakdownHwbs_PreprocessQuery(long? FlightManifestId, long? UldId, long? AwbId, bool? ShowComplete, ref IQueryable<ManifestDetail> query)
        {
            query = query.Where(a => a.FlightManifest.Id == FlightManifestId && a.ULD.Id == UldId && a.AWB.Id == AwbId);

            //if (ShowComplete == true)
            //{
            //    query = query.Where(a => a.FlightManifest.Id == FlightManifestId && a.ULD.Id == UldId && a.AWB.Id == AwbId && a.ReceivedCount >= a.LoadCount);
            //}
            //else
            //{
            //    query = query.Where(a => a.FlightManifest.Id == FlightManifestId && a.ULD.Id == UldId && a.AWB.Id == AwbId && a.ReceivedCount < a.LoadCount);
            //}
        }

        #region [ -- Helpers --]

        private List<T> GetData<T, U>(U parameterValue, string parameterName, string storedProcedureName, string columnName)
        {
            var dataTable = new DataTable();
            var connectionString = ConfigurationManager.ConnectionStrings["WFSDBData"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(storedProcedureName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter(parameterName, parameterValue));

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataTable);
                    }
                }
            }

            return dataTable.Rows.OfType<DataRow>().Select(r => (T)Convert.ChangeType(r[columnName], typeof(T))).ToList();
        }

        #endregion 

        partial void FlightPutedAwayList_PreprocessQuery(long? FlightManifestId, ref IQueryable<PutedAwayItem> query)
        {
            var paList = GetData<string, long?>(FlightManifestId, "@ManifestId", "GetFlightPutAwayList", "Pas");
            query = query.Where(pa => paList.Contains(pa.PaId));
        }

        partial void ActiveScannerView_PreprocessQuery(long? TaskId, ref IQueryable<ForkliftView> query)
        {
            query = query.Where(a => a.TaskId == TaskId);
        }

        partial void InventoryHistory_PreprocessQuery(long? TaskId, ref IQueryable<Transaction> query)
        {
            query = query.Where(a => a.Task.Id == TaskId);
        }

        partial void ActiveTrackTraceView_PreprocessQuery(string Reference, ref IQueryable<TrackTraceView> query)
        {
            query = query.Where(a => a.Reference == Reference);
        }

        partial void ReceiverFlightList_PreprocessQuery(bool? IsComplete, string SearchFilter, DateTime? DateFilter, ref IQueryable<ReceiverFlightView> query)
        {
            #region Get Current Logged-In User

            if (DateFilter != null || !string.IsNullOrEmpty(SearchFilter))
            {

                var cUser = this.Application.User.PersonId;
                if (cUser != null)
                {
                    #region Get User's Default Station

                    var uStation = GetData<int, string>(cUser, "@UserId", "GetPortIds", "PortId").FirstOrDefault();
                    if (uStation != 0)
                    {

                        DateTime EndOfDay = DateTime.Today.AddDays(1);
                        if (IsComplete == true)
                        {
                            query = query.Where(a => a.PortId == uStation && a.IsComplete == true && a.ETA <= EndOfDay && a.StatusId > 9);
                        }
                        else
                        {
                            query = query.Where(a => a.PortId == uStation && a.IsComplete == true && a.ETA <= EndOfDay && a.StatusId < 10);
                        }
                    }

                    #endregion
                }

            }
            else
            {
                query = query.Where(a => a.FlightManifestId == 0);
            }

            #endregion
        }

        partial void ReceiverUldList_PreprocessQuery(long? FlightManifestId, ref IQueryable<ReceiverUldManifestList> query)
        {
            if (FlightManifestId.HasValue)
            {

                    var rList = GetData<long, long?>(FlightManifestId, "@ManifestId", "GetReceiverUldList", "Pas");

                    query = query.Where(b => rList.Contains(b.Id));

            }
        }

        partial void ReceiverAwbList_PreprocessQuery(long? FlightManifestId, ref IQueryable<ReceiverAwbManifestList> query)
        {
            if (FlightManifestId.HasValue)
            {

                var rList = GetData<long, long?>(FlightManifestId, "@ManifestId", "GetReceiverAwbList", "Pas");

                query = query.Where(b => rList.Contains(b.Id));

            }
        }

        partial void BreakdownUldList_PreprocessQuery(long? FlightManifestId, bool? ShowComplete, ref IQueryable<ReceiverUldManifestList> query)
        {
            if (FlightManifestId.HasValue)
            {
                var rList = GetData<long, long?>(FlightManifestId, "@ManifestId", "GetReceiverUldList", "Pas");
                
                if (ShowComplete == true)
                {
                    query = query.Where(a => rList.Contains(a.Id) && a.IsBUP == false && a.TotalReceivedCount > 0 && a.ReceivedPieces >= a.TotalPieces);
                }
                else
                {
                    query = query.Where(a => rList.Contains(a.Id) && a.IsBUP == false && a.TotalReceivedCount > 0 && a.ReceivedPieces < a.TotalPieces);
                }
            }
        }

        partial void SnapshotTasks_PreprocessQuery(string EntityType, bool? IsComplete, ref IQueryable<SnapshotTasksView> query)
        {
            #region Get Current Logged-In User

            var cUser = this.Application.User.PersonId;
            if (cUser != null)
            {
                #region Get User's Default Station

                var uStation = GetData<int, string>(cUser, "@UserId", "GetPortIds", "PortId").FirstOrDefault();
                if (uStation != 0)
                {
                    if (IsComplete == true)
                    {
                        query = query.Where(a => a.PortId == uStation && a.EndDate != null && a.TaskStatusId == 21);
                    }
                    else
                    {
                        query = query.Where(a => a.PortId == uStation && a.EndDate == null && a.TaskStatusId != 21);
                    }
                }

                #endregion
            }

            #endregion
        }

        partial void ActiveCheckInList_PreprocessQuery(bool? IsComplete, string SearchFilter, DateTime? DateFilter, ref IQueryable<DischargeCheckInList> query)
        {
            if (DateFilter != null || !string.IsNullOrEmpty(SearchFilter))
            {
                var cUser = this.Application.User.PersonId;
                if (cUser != null)
                {

                    var uStation = GetData<int, string>(cUser, "@UserId", "GetPortIds", "PortId").FirstOrDefault();
                    if (uStation != 0)
                    {
                        if (IsComplete == true)
                        {
                            query = query.Where(a => a.WarehouseId == uStation && a.StatusId > 12);
                        }
                        else
                        {
                            query = query.Where(a => a.WarehouseId == uStation && a.StatusId < 13);
                        }
                    }
                    else
                    {
                        query = query.Where(a => a.TaskId == 0);
                    }


                }
                else
                {
                    query = query.Where(a => a.TaskId == 0);
                }
                
            }
            else
            {
                query = query.Where(a => a.TaskId == 0);
            }
        }

        partial void AvailablePackageTypes_PreprocessQuery(ref IQueryable<PackageType> query)
        {
            query = query.Where(a => a.Carrier.Id == null);
        }
    }
}
