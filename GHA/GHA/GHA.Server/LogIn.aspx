﻿<%@ Page Inherits="Microsoft.LightSwitch.Security.ServerGenerated.Implementation.LogInPageBase" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no" />
    <title>Log In</title>
    <style type="text/css">
        /* Here you can customize your login screen */
        html {
            background: #ffffff;
        }

        html,
        body,
        .labelStyle {
            color: #444444;
        }

        h1 {
            color: #444444;
        }

        .requiredStyle {
            color: #FF1B1B;
        }

        input.buttonStyle {
            color: #444444;
            background-color: #f1f1f1;
            border: 1px solid #ababab;
            
        }

            input.buttonStyle:hover {
                background-color: #dfebf4;
                color: white !important;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                border-radius: 10px;
            }

            input.buttonStyle:active {
                background-color: #e3e3e3;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                border-radius: 10px;
            }

        .textBoxStyle {
            color: #444444;
            background-color: #fcfcfc;
            border: 1px solid #ababab;
        }

        .failureNotification {
            color: #ff0000;
        }

        /* login layout styling */
        * {
            margin: 0px;
        }

        html {
            height: 100%;
            width: 100%;
        }

        html, body {
            font-family: Arial,'Segoe UI','Frutiger','Helvetica Neue',Helvetica,sans-serif;
            font-size: 16px;
            font-weight: normal;
        }

        h1 {
            font-family: Arial,'Segoe Light','Segoe UI Light','Frutiger','Helvetica Neue',Helvetica,sans-serif;
            font-size: 30px;
            text-align: left;
            letter-spacing: -1pt;
            font-weight: bold !important;
            margin-bottom: 15px;
        }

        .accountInfo {
            width: 360px;
            background: url("../images/wfs_logo.jpg") no-repeat top;
            background-size: 350px auto;
            padding-top: 120px;
            text-align: center;
            margin: 15% auto 0 auto;
        }

        @media only screen and (min-width: 768px) {
            .accountInfo {
                margin: 5% auto 0 auto !important;
            }
        }

        .login_container {
            position: relative;
            padding: 30px;
            border: 10px solid #edf2f6;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }

        .login_positioning {
            width: 250px;
            margin: 0 auto;
        }

        .labelStyle {
            font-family: Arial,'Segoe UI Semibold', 'Frutiger','Helvetica Neue Semibold',Helvetica,sans-serif;
            font-weight: 700;
            float: left;
        }

        .requiredStyle {
            font-size: 24px;
            line-height: 14px;
            height: 12px;
            vertical-align: bottom;
            margin-left: 5px;
        }

        input.buttonStyle {
            font-family: Arial,'Segoe UI','Frutiger','Helvetica Neue',Helvetica,sans-serif;
            padding: 5px 10px;
            font-weight: bold;
            border-radius: 0px;
            font-size: 16px;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }

        .textBoxStyle {
            background-image: none;
            font-size: 16px;
            display: block;
            outline: 0;
            height: 36px;
            padding: 1px 8px;
            margin: 0px;
            width: 100%;
            max-width: 292px;
            line-height: 36px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }

        .submit-login {
        }

        .rememberme {
            margin-bottom: 10px;
        }

        input[type=checkbox] {
            margin: 0px 6px 0px 0px;
            vertical-align: -1px;
            cursor: pointer;
        }

        .checkStyle input {
            float: left;
            margin-top: 6px;
        }

        .checkStyle label {
            font-size: 12px;
        }

        @media (max-width: 540px) {
            /*Smaller and smaller...*/
            .login_container {
                position: relative;
                padding: 20px;
                border: 10px solid #edf2f6;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                border-radius: 10px;
            }

            .accountInfo {
                width: 360px;
                background: url("../images/wfs_logo.jpg") no-repeat top;
                background-size: 350px auto;
                padding-top: 100px;
                margin: 15% auto 0 auto;
            }

            .textBoxStyle {
                background-image: none;
                font-size: 16px;
                display: block;
                outline: 0;
                height: 36px;
                padding: 1px 8px;
                margin: 0px;
                max-width: 280px;
                line-height: 36px;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                border-radius: 10px;
            }
        }

        @media (max-width: 320px) {
            /*IPhone portrait and smaller. You can probably stop on 320px*/

            .login_container {
            position: relative;
            padding: 20px;
            border: 10px solid #edf2f6;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }

            .accountInfo {
                width: 250px;
                background: url("../images/wfs_logo.jpg") no-repeat top;
                background-size: 240px auto;
                padding-top: 70px;
                margin: 15% auto 0 auto;
            }

            .textBoxStyle {
                background-image: none;
                font-size: 16px;
                display: block;
                outline: 0;
                height: 36px;
                padding: 1px 8px;
                margin: 0px;
                max-width: 200px;
                line-height: 36px;
                -moz-border-radius: 10px;
                -webkit-border-radius: 10px;
                border-radius: 10px;
            }
        }
    </style>
</head>
<body>
    <form id="LogInForm" runat="server">
        <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false">
            <LayoutTemplate>
                <div class="accountInfo">
                    <div class="login_container">
                        <div class="login_positioning">
                            <h1>Log In</h1>
                            <div style="margin-bottom:10px">
                                <asp:Label ID="UsernameLabel" runat="server" AssociatedControlID="Username" Text="Username:" CssClass="labelStyle" />
                                <asp:RequiredFieldValidator ID="UsernameRequired" runat="server" ControlToValidate="Username" ValidationGroup="LoginUserValidationGroup" Text="*" ToolTip="Username is required" CssClass="requiredStyle" />
                                <asp:TextBox ID="Username" runat="server" CssClass="textBoxStyle" />
                            </div>
                            <div style="margin-bottom:5px">
                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" Text="Password:" CssClass="labelStyle" />
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ValidationGroup="LoginUserValidationGroup" Text="*" ToolTip="Password is required" CssClass="requiredStyle" />
                                <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="textBoxStyle" />
                            </div>
                            <div class="submit-login">
                                <div class="rememberme">
                                    <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." CssClass="checkStyle" />
                                </div>
                                <div style="margin-bottom: 5px;" class="logInBtn">
                                    <asp:Button ID="LoginButton" runat="server" CommandName="Login" ValidationGroup="LoginUserValidationGroup" Text="LOG IN" Width="112" Height="38" CssClass="buttonStyle" />
                                </div>
                            </div>
                            <span class="failureNotification">
                                <asp:Literal ID="FailureText" runat="server" />
                            </span>
                            <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" ValidationGroup="LoginUserValidationGroup" />
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </form>
</body>
</html>
