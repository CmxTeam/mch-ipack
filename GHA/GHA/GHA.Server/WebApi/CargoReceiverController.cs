﻿using GHA.Bll.DataProviders;
using GHA.BLL.DataProviders.Entities;
using LightSwitchApplication.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LightSwitchApplication.WebApi
{
    public class CargoReceiverController : ApiController
    {
        [HttpGet]
        public IEnumerable<ReceiverFlight> GetCargoReceiverFlights(string userId, int rowPerPage, int pageNumber, bool isComplete, string filter, DateTime? dateFilter)
        {
            return CargoReceiver.GetCargoReceiverFlights(userId, rowPerPage, pageNumber, isComplete, filter, dateFilter);
        }

        [HttpGet]
        public IEnumerable<ReceiverFlight> GetCheckinFlights(string userId, int rowPerPage, int pageNumber, bool isComplete, string filter, DateTime? dateFilter)
        {
            return CargoReceiver.GetCheckinFlights(userId, rowPerPage, pageNumber, isComplete, filter, dateFilter);
        }

        [HttpGet]
        public IEnumerable<PutAwayItem> GetPutAwayList(long flightId, int rowPerPage, int pageNumber, string refNumber)
        {
            return CargoReceiver.GetPutAwayList(flightId, rowPerPage, pageNumber, refNumber);
        }

        [HttpGet]
        public IEnumerable<PutedAwayItemCustom> GetPutedAwayList(long flightId, int rowPerPage, int pageNumber, string refNumber)
        {
            return CargoReceiver.GetPutedAwayList(flightId, rowPerPage, pageNumber, refNumber);
        }

        [HttpGet]
        public IEnumerable<BreakdownUld> GetBreakdownUlds(long flightId, int rowPerPage, int pageNumber, bool showComplete, string filter)
        {
            return CargoReceiver.GetBreakdownUlds(flightId, rowPerPage, pageNumber, showComplete, filter);
        }

        [HttpGet]
        public IEnumerable<BreakdownAwbs> GetBreakdownAwbs(long flightId, long uldId, int rowPerPage, int pageNumber, bool showComplete, bool showAll, string filter)
        {
            return CargoReceiver.GetBreakdownAwbs(flightId, uldId, rowPerPage, pageNumber, showComplete, showAll, filter);
        }

        [HttpGet]
        public IEnumerable<BreakdownHwb> GetBreakdownHwbs(long flightId, long uldId, long awbId, int rowPerPage, int pageNumber, string filter)
        {
            return CargoReceiver.GetBreakdownHwbs(flightId, uldId, awbId, rowPerPage, pageNumber, filter);
        }

        [HttpGet]
        public IEnumerable<SnapshotItem> GetSnapshotItems(long entityTypeId, long entityId)
        {
            //D:\TfsProjects\GHAMobileSolution\GHA\GHA\bin\Debug\Images\snapshotimages
            return CargoReceiver.GetSnapshotItems(entityTypeId, entityId);
        }

        [HttpGet]
        public IEnumerable<History> GetFlightHistory(long flightManifestId, string filter, int rowsPerPage, int pageNumber) 
        {
            return CargoReceiver.GetHistory(flightManifestId, filter, rowsPerPage, pageNumber);
        }

        [HttpPost]
        public void SaveSnapshotImage(SnapshotSaveItem itemToSave)
        {
            var image = itemToSave.Image;
            var startIndex = image.IndexOf("base64");
            image = image.Substring(startIndex + 7);
            byte[] bytes = Convert.FromBase64String(image);

            Image img;
            string fileName;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                img = Image.FromStream(ms);
                var datePath = DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss");                
                fileName = (new Random().Next()) + "_" + datePath + ".jpg";
                var fileFullName = ConfigurationManager.AppSettings["SnapshotImagePath"] + fileName;
                img.Save(fileFullName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            var relativePath = ConfigurationManager.AppSettings["SnapshotRelativeImagePath"] + fileName;

            CargoReceiver.SaveSnapshotImage(relativePath, itemToSave.Conditions, itemToSave.EntityTypeId, itemToSave.Count, itemToSave.EntityId,
                itemToSave.UserName, itemToSave.TaskId, itemToSave.WarehouseId, itemToSave.FlightId, itemToSave.DisplayText);
        }

        [HttpPost]
        public void UpdateFlightEta(UpdateEtaModel updateEtaModel)
        {
            CargoReceiver.UpdateFlightEta(updateEtaModel.FlightId, updateEtaModel.PortId, updateEtaModel.Eta, updateEtaModel.Username);
        } 

        [HttpGet]
        public IEnumerable<SnapshotConditions> GetConditionItems()
        {
            return CargoReceiver.GetConditionItems();
        }

        [HttpGet]
        public IEnumerable<RecoverUld> GetRecoverUldsForFlight(long flightId, bool isRecovered, int rowsPerPage, int pageNumber, string filter ) 
        {
            return CargoReceiver.GetRecoverUldsForFlight(flightId, isRecovered, rowsPerPage, pageNumber, filter);
        }

        [HttpPost]
        public void FinalizeRecoverFlight(FinalizeFlightModel finalizeFlightModel)
        {
            CargoReceiver.FinalizeRecoverFlight(finalizeFlightModel.FlightId, finalizeFlightModel.TaskId, finalizeFlightModel.Username);
        }

        [HttpPost]
        public void ReopenRecoverFlight(FinalizeFlightModel finalizeFlightModel)
        {
            CargoReceiver.ReopenRecoverFlight(finalizeFlightModel.FlightId, finalizeFlightModel.TaskId, finalizeFlightModel.Username);
        }

        [HttpGet]
        public IEnumerable<AccountModel> GetAccountList()
        {
            return CargoReceiver.GetAccountList();
        }

        [HttpGet]
        public IEnumerable<OnhandItem> GetOnhandList(string userId, int rowPerPage, int pageNumber, bool isComplete, string filter, DateTime? dateFilter)
        {
            return CargoReceiver.GetOnhandList(userId, rowPerPage, pageNumber, isComplete, filter, dateFilter);
        }

        [HttpPost]
        public string SaveOnhandReceipt(SaveOnhandItem saveItem)
        {
            return CargoReceiver.SaveOnhandReceipt(saveItem.accountId, saveItem.truckerPro, saveItem.carrierId, saveItem.driverId,
            saveItem.idType1, saveItem.matchingPhoto1, saveItem.idType2, saveItem.matchingPhoto2, saveItem.totalPieces,
            saveItem.totalWeight, saveItem.uomId, saveItem.receivedPieces, saveItem.userName, saveItem.warehouseId);
        }

        [HttpPost]
        public void AddEditOnhandPackage(NewPackageModel newPackage)
        {
            CargoReceiver.AddEditOnhandPackage(newPackage.UserName, newPackage.OnhandId, newPackage.Count, newPackage.Length, newPackage.Width, newPackage.Height,
                 newPackage.DimUOMId, newPackage.Weight, newPackage.WeightUOMId, newPackage.PackageTypeId, newPackage.IsHazmat, newPackage.PackageId);
        }

        [HttpGet]
        public IEnumerable<PackageItem> GetOnhandPackages(long OnhandId, string userId, int rowPerPage, int pageNumber, string filter)
        {
            return CargoReceiver.GetOnhandPackages(OnhandId, userId, rowPerPage, pageNumber, filter);
        }

        [HttpPost]
        public void DeleteOnhandPackage(DeleteOnhandPackage deleteModel)
        {
            CargoReceiver.DeleteOnhandPackage(deleteModel.OnhandId, deleteModel.PackageId, deleteModel.UserName);
        }

        [HttpPost]
        public void EditOnhandDetails(EditOnhandDetailsModel editonhand)
        {
            CargoReceiver.EditOnhandDetails(editonhand.OnhandId, editonhand.ShipperName, editonhand.ConsigneeName, editonhand.DestinationId, editonhand.GoodsDescription, editonhand.UserName);
        }

        [HttpGet]
        public OnhandDetailsModel GetOnhandDetails(long onhandId)
        {
            return CargoReceiver.GetOnhandDetails(onhandId);
        }

        [HttpGet]
        public IEnumerable<GHA.BLL.DataProviders.Entities.ShipmentUnitType> GetShipmentUnitTypes() 
        {
            return CargoReceiver.GetShipmentUnitTypes();
        }

        [HttpPost]
        public void AddUnManifestedUls(UnManifestedUldModel model)
        {
            CargoReceiver.AddUnManifestedUls(model);
        }

        [HttpGet]
        public IEnumerable<DGRChecklistModel> GetDGRCheckListItems(int rowPerPage, int pageNumber)
        {
            return CargoReceiver.GetDGRCheckListItems(rowPerPage, pageNumber);
        }

        [HttpGet]
        public IEnumerable<ReferenceItem> GetEntityReferences(long? ReferenceId, int EntityTypeId, long EntityId, int RowsPerPage, int PageNumber, string Filter)
        {
            return CargoReceiver.GetEntityReferences(ReferenceId, EntityTypeId, EntityId, RowsPerPage, PageNumber, Filter);
        }

        [HttpPost]
        public void AddEditEntityReferences(NewReferenceModel refmodel)
        {
            CargoReceiver.AddEditEntityReferences(refmodel);
        }

        [HttpGet]
        public IEnumerable<GHA.BLL.DataProviders.Entities.ReferenceType> GetReferenceTypes(int? AccountId)
        {
            return CargoReceiver.GetReferenceTypes(AccountId);
        }

        [HttpGet]
        public IEnumerable<CheckInItem> GetCheckInList(string UserName, long FlightManifestId, long TaskId, int RowsPerPage, int PageNumber, bool IsComplete, string Filter)
        {
            return CargoReceiver.GetCheckInList(UserName, FlightManifestId, TaskId, RowsPerPage, PageNumber, IsComplete, Filter);
        }

        [HttpPost]
        public void DeleteEntityReferences(DeleteEntityReference refmodel)
        {
            CargoReceiver.DeleteEntityReferences(refmodel);
        }

        [HttpGet]
        public IEnumerable<GHA.BLL.DataProviders.Entities.PackageType> GetPackageTypes()
        {
            return CargoReceiver.GetPackageTypes();
        }

        [HttpGet]
        public FlightCheckInSummary GetFlightCheckInSummary(long TaskId)
        {
            return CargoReceiver.GetFlightCheckInSummary(TaskId);
        }

        [HttpPost]
        public void CheckInFlightPieces(CheckInPiece cPiece)
        {
            CargoReceiver.CheckInFlightPieces(cPiece);
        }

        [HttpPost]
        public void UnCheckInFlightPieces(CheckInPiece cPiece)
        {
            CargoReceiver.UnCheckInFlightPieces(cPiece);
        }

        [HttpGet]
        public IEnumerable<FlightAwb> GetFlightAwbs(long flightManifestId, int rowsPerPage, int pageNumber, string filter)
        {
            return CargoReceiver.GetFlightAwbs(flightManifestId, rowsPerPage, pageNumber, filter);
        }

        [HttpGet]
        public IEnumerable<History> GetOnhandHistory(long onhandId, string filter, int rowsPerPage, int pageNumber)
        {
            return CargoReceiver.GetOnhandHistory(onhandId, filter, rowsPerPage, pageNumber);
        }

        [HttpGet]
        public IEnumerable<FlightHwb> GetFlightHwbs(long flightManifestId, string filter, int rowsPerPage, int pageNumber)
        {
            return CargoReceiver.GetFlightHwbs(flightManifestId, filter, rowsPerPage, pageNumber);
        }

        [HttpPost]
        public void SaveDGRCheckListResults(SaveDGRChecklistItem dgrItem)
        {
            CargoReceiver.SaveDGRCheckListResults(dgrItem);
        }

        [HttpGet]
        public IEnumerable<DGRChecklistResult> GetDGRCheckListResults(int EntityTypeId, long EntityId, int RowsPerPage, int PageNumber, long TaskId)
        {
            return CargoReceiver.GetDGRCheckListResults(EntityTypeId, EntityId, RowsPerPage, PageNumber, TaskId);
        }

        [HttpPost]
        public void FinalizeDGRChecklist(FinalizeDGRChecklistModel dgrModel)
        {
            CargoReceiver.FinalizeDGRChecklist(dgrModel);
        }

        [HttpGet]
        public DGRChecklistStatus CheckForDGRCheckListStatus(int EntityTypeId, long EntityId, long TaskId, string UserName)
        {
            return CargoReceiver.CheckForDGRCheckListStatus(EntityTypeId, EntityId, TaskId, UserName);
        }

        [HttpGet]
        public string CheckOnhandForHazmat(long OnhandId)
        {
            return CargoReceiver.CheckOnhandForHazmat(OnhandId);
        }

        [HttpPost]
        public void SaveDGRCheckListItemComment(DGRChecklistComment dgrComment)
        {
            CargoReceiver.SaveDGRCheckListItemComment(dgrComment);
        }

        [HttpPost]
        public void SetOnhandStagingLocation(OnhandStageLocation oLocation)
        {
            CargoReceiver.SetOnhandStagingLocation(oLocation);
        }

        [HttpPost]
        public void FinalizeOnhand(FinalizeOnhand fOnhand)
        {
            CargoReceiver.FinalizeOnhand(fOnhand);
        }

        [HttpGet]
        public string GetDGRCheckListItemComment(int EntityTypeId, long EntityId, long TaskId, int CheckListItemId)
        {
            return CargoReceiver.GetDGRCheckListItemComment(EntityTypeId, EntityId, TaskId, CheckListItemId);
        }
    }    
}