﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.WebApi
{
    public class SnapshotSaveItem
    {
        public string Image { get; set; }
        public string Conditions { get; set; }
        public long EntityTypeId { get; set; }
        public int Count { get; set; }
        public long EntityId { get; set; }
        public string UserName { get; set; }
        public long TaskId { get; set; }
        public long WarehouseId { get; set; }
        public long FlightId { get; set; }
        public string DisplayText { get; set; }
    }
}