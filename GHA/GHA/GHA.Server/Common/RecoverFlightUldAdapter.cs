﻿using LightSwitchApplication.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Common
{
    public class RecoverFlightUldAdapter
    {
        public static void ProcessUld(long fltId, long uldId, string cUser)
        {
            DatabaseManager.ExecuteScalar("RecoverFlightULD", new[] 
            {
                new KeyValuePair<string, object>("@UserName", cUser),
                new KeyValuePair<string, object>("@FlightManifestId", fltId),                
                new KeyValuePair<string, object>("@UldId", uldId)
            });
        }

        public static void ProcessUldAsBup(long fltId, long uldId, string currentUser)
        {
            DatabaseManager.ExecuteScalar("ProcessUldAsBup", new[] 
            {
                new KeyValuePair<string, object>("@UldId", uldId),
                new KeyValuePair<string, object>("@FltId", fltId),
                new KeyValuePair<string, object>("@CurrentUser", currentUser)
            });
        }

    }
}