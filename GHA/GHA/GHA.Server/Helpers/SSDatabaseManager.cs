﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Helpers
{
    class SSDatabaseManager
    {

        public static string ExecuteScalar(string commandName, IEnumerable<KeyValuePair<string, object>> parameters,
            IEnumerable<Tuple<string, SqlDbType, object>> additionalParameters)
        {
            return DatabaseManager.ExecuteScalar(commandName, parameters, additionalParameters, "SnapshotDBData");
        }

        public static string ExecuteScalar(string commandName, IEnumerable<KeyValuePair<string, object>> parameters)
        {
            return DatabaseManager.ExecuteScalar(commandName, parameters, "SnapshotDBData");
        }

        public static string ExecuteScalar(string queryString)
        {
            return DatabaseManager.ExecuteScalar(queryString, "SnapshotDBData");
        }

        public static DataSet Fill(string commandName, IEnumerable<KeyValuePair<string, object>> parameters)
        {
            return DatabaseManager.Fill(commandName, parameters, "SnapshotDBData");
        }

        public static string GetValue(string queryString, string columnName, string defaultValue)
        {
            var data = DatabaseManager.FillTable(queryString, "SnapshotDBData");
            return data.Rows.Count > 0 ? data.Rows[0][columnName].ToString() : defaultValue;
        }

        public static DataSet FillDataSet(string queryString)
        {
            return DatabaseManager.FillDataSet(queryString, "SnapshotDBData");
        }

        public static DataTable FillTable(string queryString)
        {
            return DatabaseManager.FillTable(queryString, "SnapshotDBData");
        }
    }
}
