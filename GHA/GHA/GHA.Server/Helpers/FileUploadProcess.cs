﻿// Copyright (c) 2010
// by OpenLight Group
// http://openlightgroup.net/
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
// to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions 
// of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//
// CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE SOFTWARE.


using System;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Collections.Generic;
using System.Configuration;
using LightSwitchApplication;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace SimpleMVVMFileUpload
{
    public delegate void FileUploadCompletedEvent(object sender, FileUploadCompletedEventArgs args);

    #region FileUploadCompletedEventArgs
    public class FileUploadCompletedEventArgs
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }

        public FileUploadCompletedEventArgs() { }

        public FileUploadCompletedEventArgs(string fileName, string filePath)
        {
            FileName = fileName;
            FilePath = filePath;
        }
    }
    #endregion

    #region class FileUploadProcess
    public class FileUploadProcess
    {
        public event FileUploadCompletedEvent FileUploadCompleted;

        #region ProcessRequest
        public void ProcessRequest(HttpContext context)
        {

            //string strFileDirectory = @"C:\Users\cray.CARGOMATRIX\Desktop\Snapshot";
            string strFileDirectory = WebConfigurationManager.AppSettings["SnapshotImagePath"].ToString();
            //string strFileDirectory = @"D:\CargoMatrix\CargoSnapshot";
            string imgdata = context.Request["filename"];

            byte[] iData = Convert.FromBase64String(imgdata);

            string fName = "\\Snapshot_" + DateTime.Now.Ticks + ".png";

            File.WriteAllBytes(strFileDirectory + fName, iData);

        }
        #endregion

        
    }
    #endregion
}
