﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LightSwitchApplication.Helpers
{
    public class DatabaseManager
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(DatabaseManager));
        public static string ExecuteScalar(string commandName, IEnumerable<KeyValuePair<string, object>> parameters,
            IEnumerable<Tuple<string, SqlDbType, object>> additionalParameters,
            string databaseName = "WFSDBData")
        {
            logger.Info("Start of " + commandName + " with parameters " + string.Join(", ", parameters.Select(p => p.Key + ": " + p.Value)));
            var connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(commandName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (var param in parameters)
                        command.Parameters.Add(new SqlParameter(param.Key, param.Value));

                    foreach (var param in additionalParameters)
                    {
                        var p = new SqlParameter(param.Item1, param.Item2);
                        p.Value = param.Item3;
                        command.Parameters.Add(p);
                    }

                    try
                    {
                        connection.Open();
                        var result = command.ExecuteScalar();
                        return result == null ? null : result.ToString();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(commandName + ": " + ex.Message);
                        return string.Empty;
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                        logger.Info("End of " + commandName + ".");
                    }
                }
            }
        }

        public static string ExecuteScalar(string commandName, IEnumerable<KeyValuePair<string, object>> parameters, string databaseName = "WFSDBData")
        {
            logger.Info("Start of " + commandName + " with parameters " + string.Join(", ", parameters.Select(p => p.Key + ": " + p.Value)));
            object result = null;
            var connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(commandName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (var param in parameters)
                        command.Parameters.Add(new SqlParameter(param.Key, param.Value));

                    try
                    {
                        connection.Open();
                        result = command.ExecuteScalar();
                        return result == null ? null : result.ToString();
                    }
                    catch (Exception ex)
                    {
                        logger.Error(commandName + ": " + ex.Message);
                        return string.Empty;
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                        logger.Info("End of " + commandName + ".");
                    }
                }
            }
        }

        public static string ExecuteScalar(string queryString, string databaseName = "WFSDBData")
        {
            logger.Info("Start of query: " + queryString);
            var connectionString = ConfigurationManager.ConnectionStrings["WFSDBData"].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    try
                    {
                        connection.Open();
                        var result = command.ExecuteScalar();
                        return result == null ? null : result.ToString();
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Query: " + queryString + " failed: " + ex.Message);
                        return string.Empty;
                    }
                    finally
                    {
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                        logger.Info("End of query " + queryString + ".");
                    }
                }
            }
        }

        public static DataSet Fill(string commandName, IEnumerable<KeyValuePair<string, object>> parameters, string databaseName = "WFSDBData")
        {
            logger.Info("Start of " + commandName + " with parameters " + string.Join(", ", parameters.Select(p => p.Key + ": " + p.Value)));
            var dataSet = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(commandName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (var param in parameters)
                        command.Parameters.Add(new SqlParameter(param.Key, param.Value));

                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSet);
                    }
                }
            }
            logger.Info("End of:" + commandName);

            return dataSet;
        }

        public static string GetValue(string queryString, string columnName, string defaultValue, string databaseName = "WFSDBData")
        {
            DataTable data;
            logger.Info("Start of query: " + queryString);
            try
            {
                data = FillTable(queryString, databaseName);
                logger.Info("End of query. Reslt is " + data.Rows[0][columnName]);
            }
            catch (Exception ex)
            {
                logger.Error("Query ended with error: " + ex.Message);
                return defaultValue;
            }

            return data.Rows.Count > 0 ? data.Rows[0][columnName].ToString() : defaultValue;
        }

        public static DataSet FillDataSet(string queryString, string databaseName = "WFSDBData")
        {
            return FillImpl(queryString, databaseName);
        }

        public static DataTable FillTable(string queryString, string databaseName = "WFSDBData")
        {
            return FillImpl(queryString, databaseName).Tables[0];
        }

        private static DataSet FillImpl(string queryString, string databaseName)
        {
            var dataSet = new DataSet();
            var connectionString = ConfigurationManager.ConnectionStrings[databaseName].ConnectionString;
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(queryString, connection))
                {
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSet);
                    }
                }
            }
            return dataSet;
        }
    }
}