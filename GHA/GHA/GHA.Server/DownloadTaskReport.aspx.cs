﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LightSwitchApplication.Reporting;
using Microsoft.Reporting.WebForms;

namespace LightSwitchApplication
{
    public partial class DownloadTaskReport : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String tId = Request.QueryString["TaskId"];
                var rp = new ReportProvider();
                uxReportViewer.ServerReport.ReportServerCredentials = rp.Credentials;
                uxReportViewer.ServerReport.ReportServerUrl = rp.ReportServerUrl;
                uxReportViewer.ServerReport.ReportPath = "/GHA/InventoryTaskReport";
                uxReportViewer.ServerReport.SetParameters(new ReportParameter("TaskId", tId));
                uxReportViewer.ServerReport.Refresh();
            }
        }

    }
}