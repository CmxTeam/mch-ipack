﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace LightSwitchApplication.Reporting
{
    class ReportProvider
    {

        /*- Private Fields ----------------------------------------------------------------------*/
        //ReportingService2010 _webService;

        /*- Constructor -------------------------------------------------------------------------*/
        public ReportProvider()
        {
            bool defaultCredentials = (ConfigurationManager.AppSettings["SsrsDefaultCredentials"]
                                           == "true");

            UserName = ConfigurationManager.AppSettings["SsrsUserName"];
            Password = ConfigurationManager.AppSettings["SsrsPassword"];
            Domain = ConfigurationManager.AppSettings["SsrsDomain"];
            ReportServerRoot = ConfigurationManager.AppSettings["SsrsServerRoot"];
            ReportServerPath = ConfigurationManager.AppSettings["SsrsReportServerPath"];
            ReportServerUrl = new System.Uri(ReportServerRoot + ReportServerPath);

            //var url = ReportServerRoot + ConfigurationManager.AppSettings["SsrsWebServicePath"];
            //_webService = new ReportingService2010();
            //_webService.Url = url;

            //if (defaultCredentials)
            //    _webService.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            //else
            //    _webService.Credentials = new System.Net.NetworkCredential(username, password, domain);
        }

        /*- Private Properties ------------------------------------------------------------------*/
        private string UserName { get; set; }
        private string Password { get; set; }
        private string Domain { get; set; }
        private string ReportServerRoot { get; set; }
        private string ReportServerPath { get; set; }

        /*- Public Properties -------------------------------------------------------------------*/
        public System.Uri ReportServerUrl { get; set; }

        public ReportCredentials Credentials
        {
            get
            {
                ReportCredentials rc = new ReportCredentials(UserName, Password, Domain);
                return rc;
            }
        }

        /*- Public Methods ----------------------------------------------------------------------*/
        //public List<CatalogItem> GetReportList(string folder, bool includeBlank)
        //{
        //    List<CatalogItem> result = new List<CatalogItem>();

        //    if (folder.Length > 0 && !folder.StartsWith("/"))
        //        folder = "/" + folder;

        //    if (includeBlank)
        //        result.Add(new CatalogItem { Name = "(select)", Path = "" });

        //    List<CatalogItem> items = _webService
        //        .ListChildren(folder, false)
        //        .Where(o => o.TypeName == "Report")
        //        .ToList<CatalogItem>();

        //    result.AddRange(items);

        //    return result;
        //}


        /*- Nested Classes ----------------------------------------------------------------------*/
        [Serializable]
        public class ReportCredentials : Microsoft.Reporting.WebForms.IReportServerCredentials
        {
            string _userName, _password, _domain;

            public ReportCredentials(string userName, string password, string domain)
            {
                _userName = userName;
                _password = password;
                _domain = domain;
            }
            public System.Security.Principal.WindowsIdentity ImpersonationUser
            {
                get
                {
                    return null;
                }
            }
            public System.Net.ICredentials NetworkCredentials
            {
                get
                {
                    return new System.Net.NetworkCredential(_userName, _password, _domain);
                }
            }
            public bool GetFormsCredentials(out System.Net.Cookie authCoki, out string userName,
                out string password, out string authority)
            {
                userName = _userName;
                password = _password;
                authority = _domain;
                authCoki = new System.Net.Cookie(".ASPXAUTH", ".ASPXAUTH", "/", "Domain");
                return true;
            }

        }

    }
}
