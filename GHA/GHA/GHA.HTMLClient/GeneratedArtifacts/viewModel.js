﻿/// <reference path="data.js" />

(function (lightSwitchApplication) {

    var $Screen = msls.Screen,
        $defineScreen = msls._defineScreen,
        $DataServiceQuery = msls.DataServiceQuery,
        $toODataString = msls._toODataString,
        $defineShowScreen = msls._defineShowScreen;

    function AddCarrierName(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddCarrierName screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddCarrierName.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddCarrierName", parameters);
    }

    function AddDischargeChargeOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddDischargeChargeOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedDischarge" type="msls.application.Discharge">
        /// Gets or sets the selectedDischarge for this screen.
        /// </field>
        /// <field name="SelectedCharge" type="msls.application.DischargeCharge">
        /// Gets or sets the selectedCharge for this screen.
        /// </field>
        /// <field name="DischargeCharges" type="msls.VisualCollection" elementType="msls.application.DischargeCharge">
        /// Gets the dischargeCharges for this screen.
        /// </field>
        /// <field name="ChargeTypes" type="msls.VisualCollection" elementType="msls.application.ChargeType">
        /// Gets the chargeTypes for this screen.
        /// </field>
        /// <field name="DischargeHWBs" type="msls.VisualCollection" elementType="msls.application.HWB">
        /// Gets the dischargeHWBs for this screen.
        /// </field>
        /// <field name="DischargeAWBs" type="msls.VisualCollection" elementType="msls.application.AWB">
        /// Gets the dischargeAWBs for this screen.
        /// </field>
        /// <field name="SelectedHWB" type="msls.application.HWB">
        /// Gets or sets the selectedHWB for this screen.
        /// </field>
        /// <field name="SelectedAWB" type="msls.application.AWB">
        /// Gets or sets the selectedAWB for this screen.
        /// </field>
        /// <field name="ToggleView" type="String">
        /// Gets or sets the toggleView for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddDischargeChargeOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddDischargeChargeOld", parameters);
    }

    function AddDischargePaymentsOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddDischargePaymentsOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="InitialAmount" type="String">
        /// Gets or sets the initialAmount for this screen.
        /// </field>
        /// <field name="TotalAmountDue" type="String">
        /// Gets or sets the totalAmountDue for this screen.
        /// </field>
        /// <field name="TotalAmountPaid" type="String">
        /// Gets or sets the totalAmountPaid for this screen.
        /// </field>
        /// <field name="SelectedCharges" type="String">
        /// Gets or sets the selectedCharges for this screen.
        /// </field>
        /// <field name="SelectedPayMethods" type="String">
        /// Gets or sets the selectedPayMethods for this screen.
        /// </field>
        /// <field name="SelectedDischarge" type="msls.application.Discharge">
        /// Gets or sets the selectedDischarge for this screen.
        /// </field>
        /// <field name="DischargePayments" type="msls.VisualCollection" elementType="msls.application.DischargePayment">
        /// Gets the dischargePayments for this screen.
        /// </field>
        /// <field name="EnteredPayment" type="msls.application.DischargePayment">
        /// Gets or sets the enteredPayment for this screen.
        /// </field>
        /// <field name="AllDischargePayments" type="msls.VisualCollection" elementType="msls.application.DischargePayment">
        /// Gets the allDischargePayments for this screen.
        /// </field>
        /// <field name="PaymentTypes" type="msls.VisualCollection" elementType="msls.application.PaymentType">
        /// Gets the paymentTypes for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddDischargePaymentsOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddDischargePaymentsOld", parameters);
    }

    function AddEditCarrier(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddEditCarrier screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddEditCarrier.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddEditCarrier", parameters);
    }

    function AddEditCondition(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddEditCondition screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Condition" type="msls.application.Condition">
        /// Gets or sets the condition for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddEditCondition.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddEditCondition", parameters);
    }

    function AddEditDriver(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddEditDriver screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Driver" type="msls.application.Driver">
        /// Gets or sets the driver for this screen.
        /// </field>
        /// <field name="Carriers" type="msls.VisualCollection" elementType="msls.application.Carrier">
        /// Gets the carriers for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddEditDriver.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddEditDriver", parameters);
    }

    function AddEditStatus(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddEditStatus screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddEditStatus.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddEditStatus", parameters);
    }

    function AddEditWarehouseLocation(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the AddEditWarehouseLocation screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="WarehouseLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the warehouseLocation for this screen.
        /// </field>
        /// <field name="details" type="msls.application.AddEditWarehouseLocation.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "AddEditWarehouseLocation", parameters);
    }

    function BarcodeParser(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BarcodeParser screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="BarcodeScanText" type="String">
        /// Gets or sets the barcodeScanText for this screen.
        /// </field>
        /// <field name="Listener" type="String">
        /// Gets or sets the listener for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BarcodeParser.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BarcodeParser", parameters);
    }

    function BreakdownAWB(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BreakdownAWB screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedUld" type="msls.application.ULD">
        /// Gets or sets the selectedUld for this screen.
        /// </field>
        /// <field name="SelectedFlt" type="msls.application.FlightManifest">
        /// Gets or sets the selectedFlt for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="ShowComplete" type="Boolean">
        /// Gets or sets the showComplete for this screen.
        /// </field>
        /// <field name="ShowAll" type="Boolean">
        /// Gets or sets the showAll for this screen.
        /// </field>
        /// <field name="AwbEntityType" type="msls.application.EntityType1">
        /// Gets or sets the awbEntityType for this screen.
        /// </field>
        /// <field name="FlightHistory" type="msls.VisualCollection" elementType="msls.application.Transaction">
        /// Gets the flightHistory for this screen.
        /// </field>
        /// <field name="HistoryFilter" type="String">
        /// Gets or sets the historyFilter for this screen.
        /// </field>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="CheckInAWBPopupHeader" type="String">
        /// Gets or sets the checkInAWBPopupHeader for this screen.
        /// </field>
        /// <field name="CheckInQuantity" type="Number">
        /// Gets or sets the checkInQuantity for this screen.
        /// </field>
        /// <field name="MissingHWBsPopupHeader" type="String">
        /// Gets or sets the missingHWBsPopupHeader for this screen.
        /// </field>
        /// <field name="AWBHyphen" type="String">
        /// Gets or sets the aWBHyphen for this screen.
        /// </field>
        /// <field name="ListImage" type="String">
        /// Gets or sets the listImage for this screen.
        /// </field>
        /// <field name="AWBSearch" type="String">
        /// Gets or sets the aWBSearch for this screen.
        /// </field>
        /// <field name="HWBSearch" type="String">
        /// Gets or sets the hWBSearch for this screen.
        /// </field>
        /// <field name="CarrierSearch" type="String">
        /// Gets or sets the carrierSearch for this screen.
        /// </field>
        /// <field name="UnManifestedHeader" type="String">
        /// Gets or sets the unManifestedHeader for this screen.
        /// </field>
        /// <field name="SearchShipmentHeader" type="String">
        /// Gets or sets the searchShipmentHeader for this screen.
        /// </field>
        /// <field name="AddCarrier" type="String">
        /// Gets or sets the addCarrier for this screen.
        /// </field>
        /// <field name="AddAWBSerialNumber" type="String">
        /// Gets or sets the addAWBSerialNumber for this screen.
        /// </field>
        /// <field name="AddHWBSerialNumber" type="String">
        /// Gets or sets the addHWBSerialNumber for this screen.
        /// </field>
        /// <field name="AddPieces" type="Number">
        /// Gets or sets the addPieces for this screen.
        /// </field>
        /// <field name="AddAOrigin" type="msls.application.Port">
        /// Gets or sets the addAOrigin for this screen.
        /// </field>
        /// <field name="AddADestination" type="msls.application.Port">
        /// Gets or sets the addADestination for this screen.
        /// </field>
        /// <field name="AddHOrigin" type="msls.application.Port">
        /// Gets or sets the addHOrigin for this screen.
        /// </field>
        /// <field name="AddHDestination" type="msls.application.Port">
        /// Gets or sets the addHDestination for this screen.
        /// </field>
        /// <field name="SearchHistoryFilter" type="String">
        /// Gets or sets the searchHistoryFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BreakdownAWB.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BreakdownAWB", parameters);
    }

    function BreakdownULD(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BreakdownULD screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedFlt" type="msls.application.FlightManifest">
        /// Gets or sets the selectedFlt for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="UldEntityType" type="msls.application.EntityType1">
        /// Gets or sets the uldEntityType for this screen.
        /// </field>
        /// <field name="FlightHistory" type="msls.VisualCollection" elementType="msls.application.Transaction">
        /// Gets the flightHistory for this screen.
        /// </field>
        /// <field name="HistoryFilter" type="String">
        /// Gets or sets the historyFilter for this screen.
        /// </field>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="ShowComplete" type="Boolean">
        /// Gets or sets the showComplete for this screen.
        /// </field>
        /// <field name="SearchHistoryFilter" type="String">
        /// Gets or sets the searchHistoryFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BreakdownULD.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BreakdownULD", parameters);
    }

    function BrowseCarriers(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BrowseCarriers screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Carriers" type="msls.VisualCollection" elementType="msls.application.Carrier">
        /// Gets the carriers for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BrowseCarriers.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BrowseCarriers", parameters);
    }

    function BrowseConditions(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BrowseConditions screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Conditions" type="msls.VisualCollection" elementType="msls.application.Condition">
        /// Gets the conditions for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BrowseConditions.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BrowseConditions", parameters);
    }

    function BrowseStatuses(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BrowseStatuses screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Statuses" type="msls.VisualCollection" elementType="msls.application.Status">
        /// Gets the statuses for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BrowseStatuses.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BrowseStatuses", parameters);
    }

    function BrowseWarehouseLocations(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the BrowseWarehouseLocations screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="WarehouseLocations" type="msls.VisualCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.BrowseWarehouseLocations.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "BrowseWarehouseLocations", parameters);
    }

    function CargoDischarge(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CargoDischarge screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CargoDischarge.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CargoDischarge", parameters);
    }

    function CargoInventory(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CargoInventory screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="InventoryTasks" type="msls.VisualCollection" elementType="msls.application.Task">
        /// Gets the inventoryTasks for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="InventoryName" type="String">
        /// Gets or sets the inventoryName for this screen.
        /// </field>
        /// <field name="InventoryNameLabel" type="String">
        /// Gets or sets the inventoryNameLabel for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CargoInventory.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CargoInventory", parameters);
    }

    function CargoInventoryTasks(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CargoInventoryTasks screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="ActiveInventory" type="msls.VisualCollection" elementType="msls.application.InventoryView">
        /// Gets the activeInventory for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="AllInventoryTasks" type="msls.VisualCollection" elementType="msls.application.Task">
        /// Gets the allInventoryTasks for this screen.
        /// </field>
        /// <field name="PromptViewHeader" type="String">
        /// Gets or sets the promptViewHeader for this screen.
        /// </field>
        /// <field name="PromptViewText" type="String">
        /// Gets or sets the promptViewText for this screen.
        /// </field>
        /// <field name="AssignTaskHeader" type="String">
        /// Gets or sets the assignTaskHeader for this screen.
        /// </field>
        /// <field name="UserList" type="msls.VisualCollection" elementType="msls.application.UserProfile">
        /// Gets the userList for this screen.
        /// </field>
        /// <field name="InventoryReference" type="String">
        /// Gets or sets the inventoryReference for this screen.
        /// </field>
        /// <field name="UserFilter" type="String">
        /// Gets or sets the userFilter for this screen.
        /// </field>
        /// <field name="TaskFilter" type="String">
        /// Gets or sets the taskFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CargoInventoryTasks.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CargoInventoryTasks", parameters);
    }

    function CargoReceiver(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CargoReceiver screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="CompletedFlights" type="Boolean">
        /// Gets or sets the completedFlights for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="ETALabel" type="String">
        /// Gets or sets the eTALabel for this screen.
        /// </field>
        /// <field name="ULDLabel" type="String">
        /// Gets or sets the uLDLabel for this screen.
        /// </field>
        /// <field name="AWBLabel" type="String">
        /// Gets or sets the aWBLabel for this screen.
        /// </field>
        /// <field name="PCSLabel" type="String">
        /// Gets or sets the pCSLabel for this screen.
        /// </field>
        /// <field name="DateFilter" type="Date">
        /// Gets or sets the dateFilter for this screen.
        /// </field>
        /// <field name="DateStart" type="Date">
        /// Gets or sets the dateStart for this screen.
        /// </field>
        /// <field name="DateEnd" type="Date">
        /// Gets or sets the dateEnd for this screen.
        /// </field>
        /// <field name="SearchDateFilter" type="Date">
        /// Gets or sets the searchDateFilter for this screen.
        /// </field>
        /// <field name="SearchFilter" type="String">
        /// Gets or sets the searchFilter for this screen.
        /// </field>
        /// <field name="UpdateEtaHeader" type="String">
        /// Gets or sets the updateEtaHeader for this screen.
        /// </field>
        /// <field name="SelectedEta" type="Date">
        /// Gets or sets the selectedEta for this screen.
        /// </field>
        /// <field name="SelectedFlight" type="String">
        /// Gets or sets the selectedFlight for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CargoReceiver.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CargoReceiver", parameters);
    }

    function CargoReceiverTasks(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CargoReceiverTasks screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="TaskFlightReceivers" type="msls.VisualCollection" elementType="msls.application.TaskFlightReceiver">
        /// Gets the taskFlightReceivers for this screen.
        /// </field>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CargoReceiverTasks.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CargoReceiverTasks", parameters);
    }

    function CargoSnapshot(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CargoSnapshot screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="CompletedFlights" type="Boolean">
        /// Gets or sets the completedFlights for this screen.
        /// </field>
        /// <field name="EntityTypeSelected" type="String">
        /// Gets or sets the entityTypeSelected for this screen.
        /// </field>
        /// <field name="SnapshotTasks" type="msls.VisualCollection" elementType="msls.application.SnapshotTasksView">
        /// Gets the snapshotTasks for this screen.
        /// </field>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="PiecesLabel" type="String">
        /// Gets or sets the piecesLabel for this screen.
        /// </field>
        /// <field name="WeightLabel" type="String">
        /// Gets or sets the weightLabel for this screen.
        /// </field>
        /// <field name="ConditionLabel" type="String">
        /// Gets or sets the conditionLabel for this screen.
        /// </field>
        /// <field name="LocationLabel" type="String">
        /// Gets or sets the locationLabel for this screen.
        /// </field>
        /// <field name="ConfirmPopupHeader" type="String">
        /// Gets or sets the confirmPopupHeader for this screen.
        /// </field>
        /// <field name="ConfirmText" type="String">
        /// Gets or sets the confirmText for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CargoSnapshot.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CargoSnapshot", parameters);
    }

    function CargoSnapshot_old(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CargoSnapshot_old screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Conditions" type="msls.VisualCollection" elementType="msls.application.Condition1">
        /// Gets the conditions for this screen.
        /// </field>
        /// <field name="Snapshots" type="msls.VisualCollection" elementType="msls.application.Snapshot1">
        /// Gets the snapshots for this screen.
        /// </field>
        /// <field name="SelectedEntityType" type="msls.application.EntityType1">
        /// Gets or sets the selectedEntityType for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="SelectedEntityId" type="String">
        /// Gets or sets the selectedEntityId for this screen.
        /// </field>
        /// <field name="EntityDescription" type="String">
        /// Gets or sets the entityDescription for this screen.
        /// </field>
        /// <field name="CurrentSelectedConditions" type="String">
        /// Gets or sets the currentSelectedConditions for this screen.
        /// </field>
        /// <field name="CurrentConditions" type="String">
        /// Gets or sets the currentConditions for this screen.
        /// </field>
        /// <field name="CurrentQuantity" type="Number">
        /// Gets or sets the currentQuantity for this screen.
        /// </field>
        /// <field name="SelectedPhoto" type="String">
        /// Gets or sets the selectedPhoto for this screen.
        /// </field>
        /// <field name="CapturedImage" type="String">
        /// Gets or sets the capturedImage for this screen.
        /// </field>
        /// <field name="SelectConditionPopupHeader" type="String">
        /// Gets or sets the selectConditionPopupHeader for this screen.
        /// </field>
        /// <field name="AddSnapshotPopupHeader" type="String">
        /// Gets or sets the addSnapshotPopupHeader for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CargoSnapshot_old.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CargoSnapshot_old", parameters);
    }

    function CheckInAWBHWB(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the CheckInAWBHWB screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedFlt" type="msls.application.FlightManifest">
        /// Gets or sets the selectedFlt for this screen.
        /// </field>
        /// <field name="SelectedUld" type="msls.application.ULD">
        /// Gets or sets the selectedUld for this screen.
        /// </field>
        /// <field name="SelectedMawb" type="msls.application.ManifestAWBDetail">
        /// Gets or sets the selectedMawb for this screen.
        /// </field>
        /// <field name="SelectedAwb" type="msls.application.AWB">
        /// Gets or sets the selectedAwb for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="ShowComplete" type="Boolean">
        /// Gets or sets the showComplete for this screen.
        /// </field>
        /// <field name="HwbEntityType" type="msls.application.EntityType1">
        /// Gets or sets the hwbEntityType for this screen.
        /// </field>
        /// <field name="FlightHistory" type="msls.VisualCollection" elementType="msls.application.Transaction">
        /// Gets the flightHistory for this screen.
        /// </field>
        /// <field name="HistoryFilter" type="String">
        /// Gets or sets the historyFilter for this screen.
        /// </field>
        /// <field name="CheckInQuantity" type="Number">
        /// Gets or sets the checkInQuantity for this screen.
        /// </field>
        /// <field name="CheckInHWBPopupHeader" type="String">
        /// Gets or sets the checkInHWBPopupHeader for this screen.
        /// </field>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="HwbHyphen" type="String">
        /// Gets or sets the hwbHyphen for this screen.
        /// </field>
        /// <field name="SearchHistoryFilter" type="String">
        /// Gets or sets the searchHistoryFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.CheckInAWBHWB.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "CheckInAWBHWB", parameters);
    }

    function Config(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the Config screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="details" type="msls.application.Config.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "Config", parameters);
    }

    function DeliveryOrderDetailsOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the DeliveryOrderDetailsOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="DischargeDetails" type="msls.VisualCollection" elementType="msls.application.DischargeDetail">
        /// Gets the dischargeDetails for this screen.
        /// </field>
        /// <field name="SelectedHWB" type="msls.application.HWB">
        /// Gets or sets the selectedHWB for this screen.
        /// </field>
        /// <field name="SelectedAWB" type="msls.application.AWB">
        /// Gets or sets the selectedAWB for this screen.
        /// </field>
        /// <field name="CustomsNotifications" type="msls.VisualCollection" elementType="msls.application.CustomsNotification">
        /// Gets the customsNotifications for this screen.
        /// </field>
        /// <field name="SelectedDischargeDetail" type="msls.application.DischargeDetail">
        /// Gets or sets the selectedDischargeDetail for this screen.
        /// </field>
        /// <field name="AvailableSlac" type="Number">
        /// Gets or sets the availableSlac for this screen.
        /// </field>
        /// <field name="SelectedDischarge" type="msls.application.Discharge">
        /// Gets or sets the selectedDischarge for this screen.
        /// </field>
        /// <field name="ImportFeeChargeType" type="msls.application.ChargeType">
        /// Gets or sets the importFeeChargeType for this screen.
        /// </field>
        /// <field name="StorageChargeType" type="msls.application.ChargeType">
        /// Gets or sets the storageChargeType for this screen.
        /// </field>
        /// <field name="details" type="msls.application.DeliveryOrderDetailsOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "DeliveryOrderDetailsOld", parameters);
    }

    function DischargeCheckIn(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the DischargeCheckIn screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="ActiveCheckInList" type="msls.VisualCollection" elementType="msls.application.DischargeCheckInList">
        /// Gets the activeCheckInList for this screen.
        /// </field>
        /// <field name="TotalPiecesLabel" type="String">
        /// Gets or sets the totalPiecesLabel for this screen.
        /// </field>
        /// <field name="TotalDOLabel" type="String">
        /// Gets or sets the totalDOLabel for this screen.
        /// </field>
        /// <field name="TotalBalanceLabel" type="String">
        /// Gets or sets the totalBalanceLabel for this screen.
        /// </field>
        /// <field name="DateFilter" type="Date">
        /// Gets or sets the dateFilter for this screen.
        /// </field>
        /// <field name="DateStart" type="Date">
        /// Gets or sets the dateStart for this screen.
        /// </field>
        /// <field name="DateEnd" type="Date">
        /// Gets or sets the dateEnd for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="SearchFilter" type="String">
        /// Gets or sets the searchFilter for this screen.
        /// </field>
        /// <field name="SearchDateFilter" type="Date">
        /// Gets or sets the searchDateFilter for this screen.
        /// </field>
        /// <field name="IsComplete" type="Boolean">
        /// Gets or sets the isComplete for this screen.
        /// </field>
        /// <field name="NewDischargeLabel" type="String">
        /// Gets or sets the newDischargeLabel for this screen.
        /// </field>
        /// <field name="SelectCarrierLabel" type="String">
        /// Gets or sets the selectCarrierLabel for this screen.
        /// </field>
        /// <field name="SelectDriverLabel" type="String">
        /// Gets or sets the selectDriverLabel for this screen.
        /// </field>
        /// <field name="AddEditCarrierLabel" type="String">
        /// Gets or sets the addEditCarrierLabel for this screen.
        /// </field>
        /// <field name="AddEditDriverLabel" type="String">
        /// Gets or sets the addEditDriverLabel for this screen.
        /// </field>
        /// <field name="SelectedCarrier" type="msls.application.Carrier">
        /// Gets or sets the selectedCarrier for this screen.
        /// </field>
        /// <field name="SelectedDriver" type="msls.application.CarrierDriver">
        /// Gets or sets the selectedDriver for this screen.
        /// </field>
        /// <field name="SearchDriverFilter" type="String">
        /// Gets or sets the searchDriverFilter for this screen.
        /// </field>
        /// <field name="Carriers" type="msls.VisualCollection" elementType="msls.application.Carrier">
        /// Gets the carriers for this screen.
        /// </field>
        /// <field name="SearchCarrierFilter" type="String">
        /// Gets or sets the searchCarrierFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.DischargeCheckIn.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "DischargeCheckIn", parameters);
    }

    function DischargeCheckInOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the DischargeCheckInOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Discharge" type="msls.application.Discharge">
        /// Gets or sets the discharge for this screen.
        /// </field>
        /// <field name="DriverName" type="String">
        /// Gets or sets the driverName for this screen.
        /// </field>
        /// <field name="SelectedDriver" type="msls.application.Driver">
        /// Gets or sets the selectedDriver for this screen.
        /// </field>
        /// <field name="DischargeDetails" type="msls.VisualCollection" elementType="msls.application.DischargeDetail">
        /// Gets the dischargeDetails for this screen.
        /// </field>
        /// <field name="DischargeTaskType" type="msls.application.TaskType">
        /// Gets or sets the dischargeTaskType for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="AwbEntityType" type="msls.application.EntityType">
        /// Gets or sets the awbEntityType for this screen.
        /// </field>
        /// <field name="HwbEntityType" type="msls.application.EntityType">
        /// Gets or sets the hwbEntityType for this screen.
        /// </field>
        /// <field name="AmountDue" type="String">
        /// Gets or sets the amountDue for this screen.
        /// </field>
        /// <field name="details" type="msls.application.DischargeCheckInOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "DischargeCheckInOld", parameters);
    }

    function DischargeOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the DischargeOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Discharges" type="msls.VisualCollection" elementType="msls.application.Discharge">
        /// Gets the discharges for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="Statuses" type="msls.VisualCollection" elementType="msls.application.Status">
        /// Gets the statuses for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="StatusImage" type="String">
        /// Gets or sets the statusImage for this screen.
        /// </field>
        /// <field name="ActiveCheckIns" type="msls.VisualCollection" elementType="msls.application.Discharge">
        /// Gets the activeCheckIns for this screen.
        /// </field>
        /// <field name="details" type="msls.application.DischargeOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "DischargeOld", parameters);
    }

    function DischargePaymentsOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the DischargePaymentsOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AllDischargeCharges" type="msls.VisualCollection" elementType="msls.application.DischargeCharge">
        /// Gets the allDischargeCharges for this screen.
        /// </field>
        /// <field name="SelectedDischarge" type="msls.application.Discharge">
        /// Gets or sets the selectedDischarge for this screen.
        /// </field>
        /// <field name="TotalCharges" type="String">
        /// Gets or sets the totalCharges for this screen.
        /// </field>
        /// <field name="TotalAmountDue" type="String">
        /// Gets or sets the totalAmountDue for this screen.
        /// </field>
        /// <field name="TotalAmountPaid" type="String">
        /// Gets or sets the totalAmountPaid for this screen.
        /// </field>
        /// <field name="details" type="msls.application.DischargePaymentsOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "DischargePaymentsOld", parameters);
    }

    function DischargeShipmentsOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the DischargeShipmentsOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="SelectedDischarge" type="msls.application.Discharge">
        /// Gets or sets the selectedDischarge for this screen.
        /// </field>
        /// <field name="HWBs" type="msls.VisualCollection" elementType="msls.application.HWB">
        /// Gets the hWBs for this screen.
        /// </field>
        /// <field name="AWBs" type="msls.VisualCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this screen.
        /// </field>
        /// <field name="Filter1" type="String">
        /// Gets or sets the filter1 for this screen.
        /// </field>
        /// <field name="details" type="msls.application.DischargeShipmentsOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "DischargeShipmentsOld", parameters);
    }

    function Home(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the Home screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="details" type="msls.application.Home.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "Home", parameters);
    }

    function InventoryTask(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the InventoryTask screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedInventoryTask" type="msls.application.Task">
        /// Gets or sets the selectedInventoryTask for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="ScanLocationLabel" type="String">
        /// Gets or sets the scanLocationLabel for this screen.
        /// </field>
        /// <field name="ScanShipmentLabel" type="String">
        /// Gets or sets the scanShipmentLabel for this screen.
        /// </field>
        /// <field name="SelectedLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the selectedLocation for this screen.
        /// </field>
        /// <field name="ActiveLocationItems" type="msls.VisualCollection" elementType="msls.application.InventoryLocationItem">
        /// Gets the activeLocationItems for this screen.
        /// </field>
        /// <field name="CountLabel" type="String">
        /// Gets or sets the countLabel for this screen.
        /// </field>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="AwbEntityType" type="msls.application.EntityType">
        /// Gets or sets the awbEntityType for this screen.
        /// </field>
        /// <field name="HwbEntityType" type="msls.application.EntityType">
        /// Gets or sets the hwbEntityType for this screen.
        /// </field>
        /// <field name="UldEntityType" type="msls.application.EntityType">
        /// Gets or sets the uldEntityType for this screen.
        /// </field>
        /// <field name="ScannerCount" type="String">
        /// Gets or sets the scannerCount for this screen.
        /// </field>
        /// <field name="ActiveScannerView" type="msls.VisualCollection" elementType="msls.application.ForkliftView">
        /// Gets the activeScannerView for this screen.
        /// </field>
        /// <field name="ScannerViewPopupHeader" type="String">
        /// Gets or sets the scannerViewPopupHeader for this screen.
        /// </field>
        /// <field name="PiecesLabel" type="String">
        /// Gets or sets the piecesLabel for this screen.
        /// </field>
        /// <field name="AddToScannerHeader" type="String">
        /// Gets or sets the addToScannerHeader for this screen.
        /// </field>
        /// <field name="SelectedQuantity" type="String">
        /// Gets or sets the selectedQuantity for this screen.
        /// </field>
        /// <field name="ConfirmPopupHeader" type="String">
        /// Gets or sets the confirmPopupHeader for this screen.
        /// </field>
        /// <field name="ConfirmText" type="String">
        /// Gets or sets the confirmText for this screen.
        /// </field>
        /// <field name="EntityType" type="String">
        /// Gets or sets the entityType for this screen.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this screen.
        /// </field>
        /// <field name="EntityCount" type="String">
        /// Gets or sets the entityCount for this screen.
        /// </field>
        /// <field name="AddToScannerText" type="String">
        /// Gets or sets the addToScannerText for this screen.
        /// </field>
        /// <field name="PromptPopupHeader" type="String">
        /// Gets or sets the promptPopupHeader for this screen.
        /// </field>
        /// <field name="PromptPopupText" type="String">
        /// Gets or sets the promptPopupText for this screen.
        /// </field>
        /// <field name="SelectLocationPopupHeader" type="String">
        /// Gets or sets the selectLocationPopupHeader for this screen.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.VisualCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this screen.
        /// </field>
        /// <field name="LocationFilter" type="String">
        /// Gets or sets the locationFilter for this screen.
        /// </field>
        /// <field name="CloseLocationHeader" type="String">
        /// Gets or sets the closeLocationHeader for this screen.
        /// </field>
        /// <field name="CloseLocationText" type="String">
        /// Gets or sets the closeLocationText for this screen.
        /// </field>
        /// <field name="TempLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the tempLocation for this screen.
        /// </field>
        /// <field name="InventoryHistory" type="msls.VisualCollection" elementType="msls.application.Transaction">
        /// Gets the inventoryHistory for this screen.
        /// </field>
        /// <field name="HistoryFilter" type="String">
        /// Gets or sets the historyFilter for this screen.
        /// </field>
        /// <field name="FinalizeInventoryHeader" type="String">
        /// Gets or sets the finalizeInventoryHeader for this screen.
        /// </field>
        /// <field name="FinalizeInventoryText" type="String">
        /// Gets or sets the finalizeInventoryText for this screen.
        /// </field>
        /// <field name="SearchShipmentHeader" type="String">
        /// Gets or sets the searchShipmentHeader for this screen.
        /// </field>
        /// <field name="SearchLocationItems" type="msls.VisualCollection" elementType="msls.application.InventoryLocationItem">
        /// Gets the searchLocationItems for this screen.
        /// </field>
        /// <field name="SearchCriteria" type="String">
        /// Gets or sets the searchCriteria for this screen.
        /// </field>
        /// <field name="CountLabel1" type="String">
        /// Gets or sets the countLabel1 for this screen.
        /// </field>
        /// <field name="LocationLabel" type="String">
        /// Gets or sets the locationLabel for this screen.
        /// </field>
        /// <field name="details" type="msls.application.InventoryTask.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "InventoryTask", parameters);
    }

    function OnhandReceipt(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the OnhandReceipt screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="details" type="msls.application.OnhandReceipt.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "OnhandReceipt", parameters);
    }

    function PutAwayPieces(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the PutAwayPieces screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedFlt" type="msls.application.FlightManifest">
        /// Gets or sets the selectedFlt for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="SelectedLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the selectedLocation for this screen.
        /// </field>
        /// <field name="UldEntityType" type="msls.application.EntityType1">
        /// Gets or sets the uldEntityType for this screen.
        /// </field>
        /// <field name="HwbEntityType" type="msls.application.EntityType1">
        /// Gets or sets the hwbEntityType for this screen.
        /// </field>
        /// <field name="AwbEntityType" type="msls.application.EntityType1">
        /// Gets or sets the awbEntityType for this screen.
        /// </field>
        /// <field name="FltEntityType" type="msls.application.EntityType">
        /// Gets or sets the fltEntityType for this screen.
        /// </field>
        /// <field name="ActiveForkliftView" type="msls.VisualCollection" elementType="msls.application.ForkliftView">
        /// Gets the activeForkliftView for this screen.
        /// </field>
        /// <field name="ReceiverTaskType" type="msls.application.TaskType">
        /// Gets or sets the receiverTaskType for this screen.
        /// </field>
        /// <field name="CompleteStatus" type="msls.application.Status">
        /// Gets or sets the completeStatus for this screen.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.VisualCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this screen.
        /// </field>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="SnapshotImage1" type="String">
        /// Gets or sets the snapshotImage1 for this screen.
        /// </field>
        /// <field name="InitRemPutAwayPieces" type="Number">
        /// Gets or sets the initRemPutAwayPieces for this screen.
        /// </field>
        /// <field name="EnteredQuantity" type="String">
        /// Gets or sets the enteredQuantity for this screen.
        /// </field>
        /// <field name="EnteredLocation" type="String">
        /// Gets or sets the enteredLocation for this screen.
        /// </field>
        /// <field name="TotalPutAwayPieces" type="Number">
        /// Gets or sets the totalPutAwayPieces for this screen.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this screen.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this screen.
        /// </field>
        /// <field name="LocationFilter" type="String">
        /// Gets or sets the locationFilter for this screen.
        /// </field>
        /// <field name="SelectLocationPopupHeader" type="String">
        /// Gets or sets the selectLocationPopupHeader for this screen.
        /// </field>
        /// <field name="ForkLiftCount" type="String">
        /// Gets or sets the forkLiftCount for this screen.
        /// </field>
        /// <field name="ForkliftDetails" type="msls.VisualCollection" elementType="msls.application.ForkliftDetail">
        /// Gets the forkliftDetails for this screen.
        /// </field>
        /// <field name="AddToForkliftHeader" type="String">
        /// Gets or sets the addToForkliftHeader for this screen.
        /// </field>
        /// <field name="ForkliftViewPopupHeader" type="String">
        /// Gets or sets the forkliftViewPopupHeader for this screen.
        /// </field>
        /// <field name="SelectedQuantity" type="Number">
        /// Gets or sets the selectedQuantity for this screen.
        /// </field>
        /// <field name="PiecesLabel" type="String">
        /// Gets or sets the piecesLabel for this screen.
        /// </field>
        /// <field name="ConfirmPopupHeader" type="String">
        /// Gets or sets the confirmPopupHeader for this screen.
        /// </field>
        /// <field name="ConfirmText" type="String">
        /// Gets or sets the confirmText for this screen.
        /// </field>
        /// <field name="FinalizePopupHeader" type="String">
        /// Gets or sets the finalizePopupHeader for this screen.
        /// </field>
        /// <field name="FinalizeText" type="String">
        /// Gets or sets the finalizeText for this screen.
        /// </field>
        /// <field name="LocationLabel" type="String">
        /// Gets or sets the locationLabel for this screen.
        /// </field>
        /// <field name="CountLabel" type="String">
        /// Gets or sets the countLabel for this screen.
        /// </field>
        /// <field name="UnAssignCount" type="String">
        /// Gets or sets the unAssignCount for this screen.
        /// </field>
        /// <field name="UnAssignLocationPopupHeader" type="String">
        /// Gets or sets the unAssignLocationPopupHeader for this screen.
        /// </field>
        /// <field name="UnAssignText" type="String">
        /// Gets or sets the unAssignText for this screen.
        /// </field>
        /// <field name="UnAssignULDText" type="String">
        /// Gets or sets the unAssignULDText for this screen.
        /// </field>
        /// <field name="EntityType" type="String">
        /// Gets or sets the entityType for this screen.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this screen.
        /// </field>
        /// <field name="AddToForkliftText" type="String">
        /// Gets or sets the addToForkliftText for this screen.
        /// </field>
        /// <field name="UpdatePopupHeader" type="String">
        /// Gets or sets the updatePopupHeader for this screen.
        /// </field>
        /// <field name="UpdateText" type="String">
        /// Gets or sets the updateText for this screen.
        /// </field>
        /// <field name="MaxPutAwayPieces" type="Number">
        /// Gets or sets the maxPutAwayPieces for this screen.
        /// </field>
        /// <field name="SearchHistoryFilter" type="String">
        /// Gets or sets the searchHistoryFilter for this screen.
        /// </field>
        /// <field name="SearchLocationFilter" type="String">
        /// Gets or sets the searchLocationFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.PutAwayPieces.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "PutAwayPieces", parameters);
    }

    function RecoverFlight(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the RecoverFlight screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedFlight" type="msls.application.FlightManifest">
        /// Gets or sets the selectedFlight for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="FlightHistory" type="msls.VisualCollection" elementType="msls.application.Transaction">
        /// Gets the flightHistory for this screen.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.VisualCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this screen.
        /// </field>
        /// <field name="UldEntityType" type="msls.application.EntityType">
        /// Gets or sets the uldEntityType for this screen.
        /// </field>
        /// <field name="UldEntityType1" type="msls.application.EntityType1">
        /// Gets or sets the uldEntityType1 for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="HistoryFilter" type="String">
        /// Gets or sets the historyFilter for this screen.
        /// </field>
        /// <field name="RecoverQuantity" type="Number">
        /// Gets or sets the recoverQuantity for this screen.
        /// </field>
        /// <field name="LocationFilter" type="String">
        /// Gets or sets the locationFilter for this screen.
        /// </field>
        /// <field name="SetStagePopupHeader" type="String">
        /// Gets or sets the setStagePopupHeader for this screen.
        /// </field>
        /// <field name="RecoverPopupHeader" type="String">
        /// Gets or sets the recoverPopupHeader for this screen.
        /// </field>
        /// <field name="RecoverULDLabel" type="String">
        /// Gets or sets the recoverULDLabel for this screen.
        /// </field>
        /// <field name="StatusId" type="Number">
        /// Gets or sets the statusId for this screen.
        /// </field>
        /// <field name="StatusId1" type="Number">
        /// Gets or sets the statusId1 for this screen.
        /// </field>
        /// <field name="StatusId2" type="Number">
        /// Gets or sets the statusId2 for this screen.
        /// </field>
        /// <field name="ConfirmPopupHeader" type="String">
        /// Gets or sets the confirmPopupHeader for this screen.
        /// </field>
        /// <field name="ConfirmText" type="String">
        /// Gets or sets the confirmText for this screen.
        /// </field>
        /// <field name="IsSelected" type="String">
        /// Gets or sets the isSelected for this screen.
        /// </field>
        /// <field name="RecoverMultipleUldsHeader" type="String">
        /// Gets or sets the recoverMultipleUldsHeader for this screen.
        /// </field>
        /// <field name="SearchFilter" type="String">
        /// Gets or sets the searchFilter for this screen.
        /// </field>
        /// <field name="SearchHistoryFilter" type="String">
        /// Gets or sets the searchHistoryFilter for this screen.
        /// </field>
        /// <field name="SearchLocationFilter" type="String">
        /// Gets or sets the searchLocationFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.RecoverFlight.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "RecoverFlight", parameters);
    }

    function ScanPutAwayPieces(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the ScanPutAwayPieces screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedFlt" type="msls.application.FlightManifest">
        /// Gets or sets the selectedFlt for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="MaxPutAwayPieces" type="Number">
        /// Gets or sets the maxPutAwayPieces for this screen.
        /// </field>
        /// <field name="ForkliftCount" type="String">
        /// Gets or sets the forkliftCount for this screen.
        /// </field>
        /// <field name="details" type="msls.application.ScanPutAwayPieces.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "ScanPutAwayPieces", parameters);
    }

    function SelectCarrier(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the SelectCarrier screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Carriers" type="msls.VisualCollection" elementType="msls.application.Carrier">
        /// Gets the carriers for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="SelectedDriver" type="msls.application.Driver">
        /// Gets or sets the selectedDriver for this screen.
        /// </field>
        /// <field name="SelectedCarrierName" type="msls.application.Carrier">
        /// Gets or sets the selectedCarrierName for this screen.
        /// </field>
        /// <field name="details" type="msls.application.SelectCarrier.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "SelectCarrier", parameters);
    }

    function SelectDriver(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the SelectDriver screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="Drivers" type="msls.VisualCollection" elementType="msls.application.Driver">
        /// Gets the drivers for this screen.
        /// </field>
        /// <field name="SelectedDischarge" type="msls.application.Discharge">
        /// Gets or sets the selectedDischarge for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.SelectDriver.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "SelectDriver", parameters);
    }

    function Snapshot(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the Snapshot screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedEntityType" type="msls.application.EntityType1">
        /// Gets or sets the selectedEntityType for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="SelectedEntityId" type="String">
        /// Gets or sets the selectedEntityId for this screen.
        /// </field>
        /// <field name="EntityDescription" type="String">
        /// Gets or sets the entityDescription for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="Snapshots" type="msls.VisualCollection" elementType="msls.application.Snapshot1">
        /// Gets the snapshots for this screen.
        /// </field>
        /// <field name="Quantity" type="Number">
        /// Gets or sets the quantity for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="SelectConditionLabel" type="String">
        /// Gets or sets the selectConditionLabel for this screen.
        /// </field>
        /// <field name="Condition" type="String">
        /// Gets or sets the condition for this screen.
        /// </field>
        /// <field name="SelectedConditionIds" type="String">
        /// Gets or sets the selectedConditionIds for this screen.
        /// </field>
        /// <field name="DefaultCondition" type="msls.application.Condition1">
        /// Gets or sets the defaultCondition for this screen.
        /// </field>
        /// <field name="details" type="msls.application.Snapshot.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "Snapshot", parameters);
    }

    function SnapshotImager(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the SnapshotImager screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SnapshotImage" type="String">
        /// Gets or sets the snapshotImage for this screen.
        /// </field>
        /// <field name="EffectedQuantity" type="Number">
        /// Gets or sets the effectedQuantity for this screen.
        /// </field>
        /// <field name="details" type="msls.application.SnapshotImager.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "SnapshotImager", parameters);
    }

    function TaskManager(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the TaskManager screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="TaskTypes" type="msls.VisualCollection" elementType="msls.application.TaskType">
        /// Gets the taskTypes for this screen.
        /// </field>
        /// <field name="details" type="msls.application.TaskManager.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "TaskManager", parameters);
    }

    function TrackAndTrace(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the TrackAndTrace screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="CarrierCode" type="String">
        /// Gets or sets the carrierCode for this screen.
        /// </field>
        /// <field name="AwbNumber" type="String">
        /// Gets or sets the awbNumber for this screen.
        /// </field>
        /// <field name="HwbNumber" type="String">
        /// Gets or sets the hwbNumber for this screen.
        /// </field>
        /// <field name="AWBDash" type="String">
        /// Gets or sets the aWBDash for this screen.
        /// </field>
        /// <field name="AWBs" type="msls.VisualCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this screen.
        /// </field>
        /// <field name="HWBs" type="msls.VisualCollection" elementType="msls.application.HWB">
        /// Gets the hWBs for this screen.
        /// </field>
        /// <field name="ShipmentReference" type="String">
        /// Gets or sets the shipmentReference for this screen.
        /// </field>
        /// <field name="ShipmentPieces" type="String">
        /// Gets or sets the shipmentPieces for this screen.
        /// </field>
        /// <field name="ShipmentWeight" type="String">
        /// Gets or sets the shipmentWeight for this screen.
        /// </field>
        /// <field name="ShipmentStatus" type="String">
        /// Gets or sets the shipmentStatus for this screen.
        /// </field>
        /// <field name="ShipmentCharges" type="String">
        /// Gets or sets the shipmentCharges for this screen.
        /// </field>
        /// <field name="ShipmentChargesPlus1" type="String">
        /// Gets or sets the shipmentChargesPlus1 for this screen.
        /// </field>
        /// <field name="ShipmentChargesPlus2" type="String">
        /// Gets or sets the shipmentChargesPlus2 for this screen.
        /// </field>
        /// <field name="ShipmentClearance" type="String">
        /// Gets or sets the shipmentClearance for this screen.
        /// </field>
        /// <field name="SearchCarrier" type="String">
        /// Gets or sets the searchCarrier for this screen.
        /// </field>
        /// <field name="SearchAwb" type="String">
        /// Gets or sets the searchAwb for this screen.
        /// </field>
        /// <field name="SearchHwb" type="String">
        /// Gets or sets the searchHwb for this screen.
        /// </field>
        /// <field name="ShipmentHwbCount" type="String">
        /// Gets or sets the shipmentHwbCount for this screen.
        /// </field>
        /// <field name="RecordNotFound" type="String">
        /// Gets or sets the recordNotFound for this screen.
        /// </field>
        /// <field name="ShipmentReceivedDate" type="String">
        /// Gets or sets the shipmentReceivedDate for this screen.
        /// </field>
        /// <field name="ActiveTrackTraceView" type="msls.VisualCollection" elementType="msls.application.TrackTraceView">
        /// Gets the activeTrackTraceView for this screen.
        /// </field>
        /// <field name="SearchReference" type="String">
        /// Gets or sets the searchReference for this screen.
        /// </field>
        /// <field name="SelectedReference" type="String">
        /// Gets or sets the selectedReference for this screen.
        /// </field>
        /// <field name="details" type="msls.application.TrackAndTrace.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "TrackAndTrace", parameters);
    }

    function ViewDischargePaymentsOld(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the ViewDischargePaymentsOld screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="AllDischargePayments" type="msls.VisualCollection" elementType="msls.application.DischargePayment">
        /// Gets the allDischargePayments for this screen.
        /// </field>
        /// <field name="SelectedDischarge" type="msls.application.Discharge">
        /// Gets or sets the selectedDischarge for this screen.
        /// </field>
        /// <field name="details" type="msls.application.ViewDischargePaymentsOld.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "ViewDischargePaymentsOld", parameters);
    }

    function ViewFlightManifest(parameters, dataWorkspace) {
        /// <summary>
        /// Represents the ViewFlightManifest screen.
        /// </summary>
        /// <param name="parameters" type="Array">
        /// An array of screen parameter values.
        /// </param>
        /// <param name="dataWorkspace" type="msls.application.DataWorkspace" optional="true">
        /// An existing data workspace for this screen to use. By default, a new data workspace is created.
        /// </param>
        /// <field name="SelectedFlightUnloadPort" type="msls.application.ReceiverFlightView">
        /// Gets or sets the selectedFlightUnloadPort for this screen.
        /// </field>
        /// <field name="CurrentUser" type="String">
        /// Gets or sets the currentUser for this screen.
        /// </field>
        /// <field name="CurrentUserStation" type="msls.application.Port">
        /// Gets or sets the currentUserStation for this screen.
        /// </field>
        /// <field name="CurrentUserWarehouse" type="msls.application.Warehouse">
        /// Gets or sets the currentUserWarehouse for this screen.
        /// </field>
        /// <field name="AjaxTimeout" type="Number">
        /// Gets or sets the ajaxTimeout for this screen.
        /// </field>
        /// <field name="SelectedTask" type="msls.application.Task">
        /// Gets or sets the selectedTask for this screen.
        /// </field>
        /// <field name="FlightManifest" type="msls.application.FlightManifest">
        /// Gets or sets the flightManifest for this screen.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.VisualCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this screen.
        /// </field>
        /// <field name="Filter" type="String">
        /// Gets or sets the filter for this screen.
        /// </field>
        /// <field name="FlightHistory" type="msls.VisualCollection" elementType="msls.application.Transaction">
        /// Gets the flightHistory for this screen.
        /// </field>
        /// <field name="HistoryFilter" type="String">
        /// Gets or sets the historyFilter for this screen.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.VisualCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this screen.
        /// </field>
        /// <field name="LocationFilter" type="String">
        /// Gets or sets the locationFilter for this screen.
        /// </field>
        /// <field name="SetStageLocationPopupHeader" type="String">
        /// Gets or sets the setStageLocationPopupHeader for this screen.
        /// </field>
        /// <field name="ReceiverAwbList" type="msls.VisualCollection" elementType="msls.application.ReceiverAwbManifestList">
        /// Gets the receiverAwbList for this screen.
        /// </field>
        /// <field name="AwbFilter" type="String">
        /// Gets or sets the awbFilter for this screen.
        /// </field>
        /// <field name="ReceiverUldList" type="msls.VisualCollection" elementType="msls.application.ReceiverUldManifestList">
        /// Gets the receiverUldList for this screen.
        /// </field>
        /// <field name="UldFilter" type="String">
        /// Gets or sets the uldFilter for this screen.
        /// </field>
        /// <field name="SearchUldFilter" type="String">
        /// Gets or sets the searchUldFilter for this screen.
        /// </field>
        /// <field name="SearchAwbFilter" type="String">
        /// Gets or sets the searchAwbFilter for this screen.
        /// </field>
        /// <field name="SearchHistoryFilter" type="String">
        /// Gets or sets the searchHistoryFilter for this screen.
        /// </field>
        /// <field name="SearchLocationFilter" type="String">
        /// Gets or sets the searchLocationFilter for this screen.
        /// </field>
        /// <field name="details" type="msls.application.ViewFlightManifest.Details">
        /// Gets the details for this screen.
        /// </field>
        if (!dataWorkspace) {
            dataWorkspace = new lightSwitchApplication.DataWorkspace();
        }
        $Screen.call(this, dataWorkspace, "ViewFlightManifest", parameters);
    }

    msls._addToNamespace("msls.application", {

        AddCarrierName: $defineScreen(AddCarrierName, [
            { name: "Carrier", kind: "local", type: lightSwitchApplication.Carrier }
        ], [
        ]),

        AddDischargeChargeOld: $defineScreen(AddDischargeChargeOld, [
            { name: "SelectedDischarge", kind: "local", type: lightSwitchApplication.Discharge },
            { name: "SelectedCharge", kind: "local", type: lightSwitchApplication.DischargeCharge },
            {
                name: "DischargeCharges", kind: "collection", elementType: lightSwitchApplication.DischargeCharge,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.DischargeCharges;
                }
            },
            {
                name: "ChargeTypes", kind: "collection", elementType: lightSwitchApplication.ChargeType,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.ChargeTypes.filter("(Id eq 5) eq false").orderBy("ChargeType1");
                }
            },
            {
                name: "DischargeHWBs", kind: "collection", elementType: lightSwitchApplication.HWB,
                createQuery: function (DischargeId) {
                    return this.dataWorkspace.WFSDBData.DischargeHWBs(DischargeId).orderBy("HWBSerialNumber");
                }
            },
            {
                name: "DischargeAWBs", kind: "collection", elementType: lightSwitchApplication.AWB,
                createQuery: function (DischargeId) {
                    return this.dataWorkspace.WFSDBData.DischargeAWBs(DischargeId).orderBy("AWBSerialNumber").expand("Carrier");
                }
            },
            { name: "SelectedHWB", kind: "local", type: lightSwitchApplication.HWB },
            { name: "SelectedAWB", kind: "local", type: lightSwitchApplication.AWB },
            { name: "ToggleView", kind: "local", type: String }
        ], [
            { name: "SaveAndCloseCharge" },
            { name: "SaveCharge" }
        ]),

        AddDischargePaymentsOld: $defineScreen(AddDischargePaymentsOld, [
            { name: "InitialAmount", kind: "local", type: String },
            { name: "TotalAmountDue", kind: "local", type: String },
            { name: "TotalAmountPaid", kind: "local", type: String },
            { name: "SelectedCharges", kind: "local", type: String },
            { name: "SelectedPayMethods", kind: "local", type: String },
            { name: "SelectedDischarge", kind: "local", type: lightSwitchApplication.Discharge },
            {
                name: "DischargePayments", kind: "collection", elementType: lightSwitchApplication.DischargePayment,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.DischargePayments;
                }
            },
            { name: "EnteredPayment", kind: "local", type: lightSwitchApplication.DischargePayment },
            {
                name: "AllDischargePayments", kind: "collection", elementType: lightSwitchApplication.DischargePayment,
                createQuery: function (DischargeId) {
                    return this.dataWorkspace.WFSDBData.AllDischargePayments(DischargeId).expand("PaymentType");
                }
            },
            {
                name: "PaymentTypes", kind: "collection", elementType: lightSwitchApplication.PaymentType,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.PaymentTypes.filter("(Id eq 5) eq false").orderBy("PaymentType1");
                }
            }
        ], [
            { name: "Generate" },
            { name: "AddPayment" },
            { name: "ApplyPayment" }
        ]),

        AddEditCarrier: $defineScreen(AddEditCarrier, [
            { name: "Carrier", kind: "local", type: lightSwitchApplication.Carrier }
        ], [
        ]),

        AddEditCondition: $defineScreen(AddEditCondition, [
            { name: "Condition", kind: "local", type: lightSwitchApplication.Condition }
        ], [
        ]),

        AddEditDriver: $defineScreen(AddEditDriver, [
            { name: "Driver", kind: "local", type: lightSwitchApplication.Driver },
            {
                name: "Carriers", kind: "collection", elementType: lightSwitchApplication.Carrier,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.Carriers.filter("MOT/Id eq 3").orderBy("CarrierName");
                }
            }
        ], [
            { name: "AddCarrier" },
            { name: "SelectCarrier" }
        ]),

        AddEditStatus: $defineScreen(AddEditStatus, [
            { name: "Status", kind: "local", type: lightSwitchApplication.Status }
        ], [
        ]),

        AddEditWarehouseLocation: $defineScreen(AddEditWarehouseLocation, [
            { name: "WarehouseLocation", kind: "local", type: lightSwitchApplication.WarehouseLocation }
        ], [
        ]),

        BarcodeParser: $defineScreen(BarcodeParser, [
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "BarcodeScanText", kind: "local", type: String },
            { name: "Listener", kind: "local", type: String }
        ], [
        ]),

        BreakdownAWB: $defineScreen(BreakdownAWB, [
            { name: "SelectedUld", kind: "local", type: lightSwitchApplication.ULD },
            { name: "SelectedFlt", kind: "local", type: lightSwitchApplication.FlightManifest },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "ShowComplete", kind: "local", type: Boolean },
            { name: "ShowAll", kind: "local", type: Boolean },
            { name: "AwbEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            {
                name: "FlightHistory", kind: "collection", elementType: lightSwitchApplication.Transaction,
                createQuery: function (FlightManifestId, Description, Reference, UserName) {
                    return this.dataWorkspace.WFSDBData.FlightHistory(FlightManifestId).filter("(" + ((Description === undefined || Description === null) ? "true" : "substringof(" + $toODataString(Description, "String?") + ", Description)") + " or " + ((Reference === undefined || Reference === null) ? "true" : "substringof(" + $toODataString(Reference, "String?") + ", Reference)") + ") or " + ((UserName === undefined || UserName === null) ? "true" : "substringof(" + $toODataString(UserName, "String?") + ", UserName)") + "").orderByDescending("StatusTimestamp");
                }
            },
            { name: "HistoryFilter", kind: "local", type: String },
            { name: "SnapshotImage", kind: "local", type: String },
            { name: "CheckInAWBPopupHeader", kind: "local", type: String },
            { name: "CheckInQuantity", kind: "local", type: Number },
            { name: "MissingHWBsPopupHeader", kind: "local", type: String },
            { name: "AWBHyphen", kind: "local", type: String },
            { name: "ListImage", kind: "local", type: String },
            { name: "AWBSearch", kind: "local", type: String },
            { name: "HWBSearch", kind: "local", type: String },
            { name: "CarrierSearch", kind: "local", type: String },
            { name: "UnManifestedHeader", kind: "local", type: String },
            { name: "SearchShipmentHeader", kind: "local", type: String },
            { name: "AddCarrier", kind: "local", type: String },
            { name: "AddAWBSerialNumber", kind: "local", type: String },
            { name: "AddHWBSerialNumber", kind: "local", type: String },
            { name: "AddPieces", kind: "local", type: Number },
            { name: "AddAOrigin", kind: "local", type: lightSwitchApplication.Port },
            { name: "AddADestination", kind: "local", type: lightSwitchApplication.Port },
            { name: "AddHOrigin", kind: "local", type: lightSwitchApplication.Port },
            { name: "AddHDestination", kind: "local", type: lightSwitchApplication.Port },
            { name: "SearchHistoryFilter", kind: "local", type: String }
        ], [
            { name: "CheckInAWBPieces" },
            { name: "AddUnManifestedAWB" },
            { name: "GoToHome" },
            { name: "ResetFilter" },
            { name: "Recover" },
            { name: "PutAway" },
            { name: "Reset" },
            { name: "Breakdown" },
            { name: "ShowAwbSnapshot" },
            { name: "ShowNotCheckedIn" },
            { name: "ShowCheckedIn" },
            { name: "ShowAllAwbs" },
            { name: "CheckIn" },
            { name: "AsssignTransfer" },
            { name: "AssignSimpleAwb" },
            { name: "AssignMissingHwb" },
            { name: "UnCheckIn" },
            { name: "OpenAWBTypePopup" },
            { name: "AssignBreakdownAwb" },
            { name: "SearchShipment" },
            { name: "SearchReset" },
            { name: "AddShipment" },
            { name: "AddCancel" },
            { name: "CheckInAwbQuantity" },
            { name: "InterlineTransfer" },
            { name: "SearchBreakdownAwb" },
            { name: "Search" }
        ]),

        BreakdownULD: $defineScreen(BreakdownULD, [
            { name: "SelectedFlt", kind: "local", type: lightSwitchApplication.FlightManifest },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "UldEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            {
                name: "FlightHistory", kind: "collection", elementType: lightSwitchApplication.Transaction,
                createQuery: function (FlightManifestId, Description, Reference, UserName) {
                    return this.dataWorkspace.WFSDBData.FlightHistory(FlightManifestId).filter("(" + ((Description === undefined || Description === null) ? "true" : "substringof(" + $toODataString(Description, "String?") + ", Description)") + " or " + ((Reference === undefined || Reference === null) ? "true" : "substringof(" + $toODataString(Reference, "String?") + ", Reference)") + ") or " + ((UserName === undefined || UserName === null) ? "true" : "substringof(" + $toODataString(UserName, "String?") + ", UserName)") + "").orderByDescending("StatusTimestamp");
                }
            },
            { name: "HistoryFilter", kind: "local", type: String },
            { name: "SnapshotImage", kind: "local", type: String },
            { name: "ShowComplete", kind: "local", type: Boolean },
            { name: "SearchHistoryFilter", kind: "local", type: String }
        ], [
            { name: "BreakdownAWB" },
            { name: "GoToHome" },
            { name: "Recover" },
            { name: "PutAway" },
            { name: "Search" },
            { name: "Reset" },
            { name: "Reset1" },
            { name: "ShowUldSnapshot" },
            { name: "ShowNotCheckedIn" },
            { name: "ShowCheckedIn" },
            { name: "SearchBreakdownUld" }
        ]),

        BrowseCarriers: $defineScreen(BrowseCarriers, [
            {
                name: "Carriers", kind: "collection", elementType: lightSwitchApplication.Carrier,
                createQuery: function (CarrierName, Carrier3Code, CarrierCode) {
                    return this.dataWorkspace.WFSDBData.Carriers.filter("(" + ((CarrierName === undefined || CarrierName === null) ? "true" : "substringof(" + $toODataString(CarrierName, "String?") + ", CarrierName)") + " or " + ((Carrier3Code === undefined || Carrier3Code === null) ? "true" : "substringof(" + $toODataString(Carrier3Code, "String?") + ", Carrier3Code)") + ") or " + ((CarrierCode === undefined || CarrierCode === null) ? "true" : "substringof(" + $toODataString(CarrierCode, "String?") + ", CarrierCode)") + "").orderBy("CarrierName");
                }
            },
            { name: "Filter", kind: "local", type: String }
        ], [
            { name: "Search" },
            { name: "Reset" }
        ]),

        BrowseConditions: $defineScreen(BrowseConditions, [
            {
                name: "Conditions", kind: "collection", elementType: lightSwitchApplication.Condition,
                createQuery: function (Condition1) {
                    return this.dataWorkspace.WFSDBData.Conditions.filter("" + ((Condition1 === undefined || Condition1 === null) ? "true" : "substringof(" + $toODataString(Condition1, "String?") + ", Condition1)") + "").orderBy("Condition1");
                }
            },
            { name: "Filter", kind: "local", type: String }
        ], [
            { name: "Search" },
            { name: "Reset" }
        ]),

        BrowseStatuses: $defineScreen(BrowseStatuses, [
            {
                name: "Statuses", kind: "collection", elementType: lightSwitchApplication.Status,
                createQuery: function (Name) {
                    return this.dataWorkspace.WFSDBData.Statuses.filter("" + ((Name === undefined || Name === null) ? "true" : "substringof(" + $toODataString(Name, "String?") + ", Name)") + "").orderBy("Name");
                }
            },
            { name: "Filter", kind: "local", type: String }
        ], [
            { name: "Search" },
            { name: "Reset" }
        ]),

        BrowseWarehouseLocations: $defineScreen(BrowseWarehouseLocations, [
            {
                name: "WarehouseLocations", kind: "collection", elementType: lightSwitchApplication.WarehouseLocation,
                createQuery: function (Type, Warehouse, Zone, Location) {
                    return this.dataWorkspace.WFSDBData.WarehouseLocations.filter("((" + ((Type === undefined || Type === null) ? "true" : "substringof(" + $toODataString(Type, "String?") + ", WarehouseLocationType/LocationType)") + " or " + ((Warehouse === undefined || Warehouse === null) ? "true" : "substringof(" + $toODataString(Warehouse, "String?") + ", Warehouse1/Code)") + ") or " + ((Zone === undefined || Zone === null) ? "true" : "substringof(" + $toODataString(Zone, "String?") + ", WarehouseZone/Zone)") + ") or " + ((Location === undefined || Location === null) ? "true" : "substringof(" + $toODataString(Location, "String?") + ", Location)") + "").orderBy("WarehouseLocationType/LocationType").thenBy("Location").expand("WarehouseLocationType").expand("Warehouse1").expand("WarehouseZone");
                }
            },
            { name: "Filter", kind: "local", type: String }
        ], [
            { name: "Search" },
            { name: "Reset" }
        ]),

        CargoDischarge: $defineScreen(CargoDischarge, [
            { name: "Filter", kind: "local", type: String },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port }
        ], [
            { name: "Reset" },
            { name: "ShowNotReleased" },
            { name: "ShowReleased" },
            { name: "ShowHolds" }
        ]),

        CargoInventory: $defineScreen(CargoInventory, [
            { name: "AjaxTimeout", kind: "local", type: Number },
            {
                name: "InventoryTasks", kind: "collection", elementType: lightSwitchApplication.Task,
                createQuery: function (UserName) {
                    return this.dataWorkspace.WFSDBData.InventoryTasks(UserName).expand("Status");
                }
            },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "InventoryName", kind: "local", type: String },
            { name: "InventoryNameLabel", kind: "local", type: String }
        ], [
            { name: "CreateWithName" },
            { name: "SkipName" },
            { name: "CancelInventoryTask" },
            { name: "ShowInventoryNamePopup" },
            { name: "GoToInventoryTask" },
            { name: "GoToHome" }
        ]),

        CargoInventoryTasks: $defineScreen(CargoInventoryTasks, [
            {
                name: "ActiveInventory", kind: "collection", elementType: lightSwitchApplication.InventoryView,
                createQuery: function (Reference, Reference1, Location, LocationType, UserName) {
                    return this.dataWorkspace.WFSDBData.ActiveInventory().filter("(((" + ((Reference === undefined || Reference === null) ? "true" : "substringof(" + $toODataString(Reference, "String?") + ", Reference)") + " or " + ((Reference1 === undefined || Reference1 === null) ? "true" : "substringof(" + $toODataString(Reference1, "String?") + ", Reference1)") + ") or " + ((Location === undefined || Location === null) ? "true" : "substringof(" + $toODataString(Location, "String?") + ", Location)") + ") or " + ((LocationType === undefined || LocationType === null) ? "true" : "substringof(" + $toODataString(LocationType, "String?") + ", LocationType)") + ") or " + ((UserName === undefined || UserName === null) ? "true" : "substringof(" + $toODataString(UserName, "String?") + ", UserName)") + "").orderBy("Reference");
                }
            },
            { name: "Filter", kind: "local", type: String },
            {
                name: "AllInventoryTasks", kind: "collection", elementType: lightSwitchApplication.Task,
                createQuery: function (TaskReference, TaskCreator, TaskOwner) {
                    return this.dataWorkspace.WFSDBData.AllInventoryTasks().filter("(" + ((TaskReference === undefined || TaskReference === null) ? "true" : "substringof(" + $toODataString(TaskReference, "String?") + ", TaskReference)") + " or " + ((TaskCreator === undefined || TaskCreator === null) ? "true" : "substringof(" + $toODataString(TaskCreator, "String?") + ", TaskCreator)") + ") or " + ((TaskOwner === undefined || TaskOwner === null) ? "true" : "substringof(" + $toODataString(TaskOwner, "String?") + ", TaskOwner)") + "").orderByDescending("TaskCreationDate");
                }
            },
            { name: "PromptViewHeader", kind: "local", type: String },
            { name: "PromptViewText", kind: "local", type: String },
            { name: "AssignTaskHeader", kind: "local", type: String },
            {
                name: "UserList", kind: "collection", elementType: lightSwitchApplication.UserProfile,
                createQuery: function (FirstName, LastName, UserId) {
                    return this.dataWorkspace.WFSDBData.UserList().filter("(" + ((FirstName === undefined || FirstName === null) ? "true" : "substringof(" + $toODataString(FirstName, "String?") + ", FirstName)") + " or " + ((LastName === undefined || LastName === null) ? "true" : "substringof(" + $toODataString(LastName, "String?") + ", LastName)") + ") or " + ((UserId === undefined || UserId === null) ? "true" : "substringof(" + $toODataString(UserId, "String?") + ", UserId)") + "").orderBy("FirstName").thenBy("LastName").thenBy("UserId");
                }
            },
            { name: "InventoryReference", kind: "local", type: String },
            { name: "UserFilter", kind: "local", type: String },
            { name: "TaskFilter", kind: "local", type: String }
        ], [
            { name: "GoToHome" },
            { name: "Reset" },
            { name: "DownloadReport" },
            { name: "GoToHome1" },
            { name: "PromptViewReport" },
            { name: "ConfirmYes" },
            { name: "ConfirmNo" },
            { name: "AssignTask" },
            { name: "Reset1" },
            { name: "Assign" },
            { name: "Cancel" },
            { name: "Reset2" }
        ]),

        CargoReceiver: $defineScreen(CargoReceiver, [
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "CompletedFlights", kind: "local", type: Boolean },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "Filter", kind: "local", type: String },
            { name: "ETALabel", kind: "local", type: String },
            { name: "ULDLabel", kind: "local", type: String },
            { name: "AWBLabel", kind: "local", type: String },
            { name: "PCSLabel", kind: "local", type: String },
            { name: "DateFilter", kind: "local", type: Date },
            { name: "DateStart", kind: "local", type: Date },
            { name: "DateEnd", kind: "local", type: Date },
            { name: "SearchDateFilter", kind: "local", type: Date },
            { name: "SearchFilter", kind: "local", type: String },
            { name: "UpdateEtaHeader", kind: "local", type: String },
            { name: "SelectedEta", kind: "local", type: Date },
            { name: "SelectedFlight", kind: "local", type: String }
        ], [
            { name: "GoToHome" },
            { name: "Search" },
            { name: "Reset" },
            { name: "ViewFlightManifest" },
            { name: "Incomplete" },
            { name: "Complete" },
            { name: "UpdateFlightEta" },
            { name: "Reset1" }
        ]),

        CargoReceiverTasks: $defineScreen(CargoReceiverTasks, [
            {
                name: "TaskFlightReceivers", kind: "collection", elementType: lightSwitchApplication.TaskFlightReceiver,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.TaskFlightReceivers;
                }
            },
            { name: "SnapshotImage", kind: "local", type: String }
        ], [
            { name: "ShowSnapshots" },
            { name: "GoToHome" }
        ]),

        CargoSnapshot: $defineScreen(CargoSnapshot, [
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "Filter", kind: "local", type: String },
            { name: "CompletedFlights", kind: "local", type: Boolean },
            { name: "EntityTypeSelected", kind: "local", type: String },
            {
                name: "SnapshotTasks", kind: "collection", elementType: lightSwitchApplication.SnapshotTasksView,
                createQuery: function (EntityType, IsComplete, RefNumber, RefNumber1) {
                    return this.dataWorkspace.WFSDBData.SnapshotTasks(EntityType, IsComplete).filter("" + ((RefNumber === undefined || RefNumber === null) ? "true" : "substringof(" + $toODataString(RefNumber, "String?") + ", RefNumber)") + " or " + ((RefNumber1 === undefined || RefNumber1 === null) ? "true" : "substringof(" + $toODataString(RefNumber1, "String?") + ", RefNumber1)") + "");
                }
            },
            { name: "SnapshotImage", kind: "local", type: String },
            { name: "PiecesLabel", kind: "local", type: String },
            { name: "WeightLabel", kind: "local", type: String },
            { name: "ConditionLabel", kind: "local", type: String },
            { name: "LocationLabel", kind: "local", type: String },
            { name: "ConfirmPopupHeader", kind: "local", type: String },
            { name: "ConfirmText", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse }
        ], [
            { name: "NewSnapshot" },
            { name: "GoToHome" },
            { name: "HWB" },
            { name: "AWB" },
            { name: "ULD" },
            { name: "Incomplete" },
            { name: "Complete" },
            { name: "Reset" },
            { name: "ConfirmYes" },
            { name: "ConfirmNo" },
            { name: "SnapshotImage_Tap" }
        ]),

        CargoSnapshot_old: $defineScreen(CargoSnapshot_old, [
            {
                name: "Conditions", kind: "collection", elementType: lightSwitchApplication.Condition1,
                createQuery: function () {
                    return this.dataWorkspace.SnapshotDBData.Conditions.orderBy("Condition2");
                }
            },
            {
                name: "Snapshots", kind: "collection", elementType: lightSwitchApplication.Snapshot1,
                createQuery: function (EntityId, EntityTypeId) {
                    return this.dataWorkspace.SnapshotDBData.Snapshots.filter("" + ((EntityId === undefined || EntityId === null) ? "false" : "((EntityId ne null) and (EntityId eq " + $toODataString(EntityId, "Int64?") + "))") + " and " + ((EntityTypeId === undefined || EntityTypeId === null) ? "false" : "(EntityType/Id eq " + $toODataString(EntityTypeId, "Int32?") + ")") + "").orderByDescending("Timestamp");
                }
            },
            { name: "SelectedEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "SelectedEntityId", kind: "local", type: String },
            { name: "EntityDescription", kind: "local", type: String },
            { name: "CurrentSelectedConditions", kind: "local", type: String },
            { name: "CurrentConditions", kind: "local", type: String },
            { name: "CurrentQuantity", kind: "local", type: Number },
            { name: "SelectedPhoto", kind: "local", type: String },
            { name: "CapturedImage", kind: "local", type: String },
            { name: "SelectConditionPopupHeader", kind: "local", type: String },
            { name: "AddSnapshotPopupHeader", kind: "local", type: String }
        ], [
            { name: "OpenSelectCondition" },
            { name: "Accept" },
            { name: "ShowSnapshot" },
            { name: "AddSnapshot" },
            { name: "TakePicture" },
            { name: "SelectCondition" },
            { name: "SelectConditionItem" }
        ]),

        CheckInAWBHWB: $defineScreen(CheckInAWBHWB, [
            { name: "SelectedFlt", kind: "local", type: lightSwitchApplication.FlightManifest },
            { name: "SelectedUld", kind: "local", type: lightSwitchApplication.ULD },
            { name: "SelectedMawb", kind: "local", type: lightSwitchApplication.ManifestAWBDetail },
            { name: "SelectedAwb", kind: "local", type: lightSwitchApplication.AWB },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "ShowComplete", kind: "local", type: Boolean },
            { name: "HwbEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            {
                name: "FlightHistory", kind: "collection", elementType: lightSwitchApplication.Transaction,
                createQuery: function (FlightManifestId, Description, Reference, UserName) {
                    return this.dataWorkspace.WFSDBData.FlightHistory(FlightManifestId).filter("(" + ((Description === undefined || Description === null) ? "true" : "substringof(" + $toODataString(Description, "String?") + ", Description)") + " or " + ((Reference === undefined || Reference === null) ? "true" : "substringof(" + $toODataString(Reference, "String?") + ", Reference)") + ") or " + ((UserName === undefined || UserName === null) ? "true" : "substringof(" + $toODataString(UserName, "String?") + ", UserName)") + "").orderByDescending("StatusTimestamp");
                }
            },
            { name: "HistoryFilter", kind: "local", type: String },
            { name: "CheckInQuantity", kind: "local", type: Number },
            { name: "CheckInHWBPopupHeader", kind: "local", type: String },
            { name: "SnapshotImage", kind: "local", type: String },
            { name: "HwbHyphen", kind: "local", type: String },
            { name: "SearchHistoryFilter", kind: "local", type: String }
        ], [
            { name: "Reset" },
            { name: "CheckInHWB" },
            { name: "GoToHome" },
            { name: "Recover" },
            { name: "PutAwayPieces" },
            { name: "Reset1" },
            { name: "CheckIn" },
            { name: "Breakdown" },
            { name: "ShowHwbSnapshot" },
            { name: "ShowNotCheckedIn" },
            { name: "ShowCheckedIn" },
            { name: "Search1" },
            { name: "Search" }
        ]),

        Config: $defineScreen(Config, [
            { name: "AjaxTimeout", kind: "local", type: Number }
        ], [
            { name: "GoToHome" }
        ]),

        DeliveryOrderDetailsOld: $defineScreen(DeliveryOrderDetailsOld, [
            {
                name: "DischargeDetails", kind: "collection", elementType: lightSwitchApplication.DischargeDetail,
                createQuery: function (DischargeDetailId, DischargeId) {
                    return this.dataWorkspace.WFSDBData.DischargeDetails.filter("" + ((DischargeDetailId === undefined || DischargeDetailId === null) ? "true" : "(Id eq " + $toODataString(DischargeDetailId, "Int64?") + ")") + " and " + ((DischargeId === undefined || DischargeId === null) ? "true" : "(Discharge/Id eq " + $toODataString(DischargeId, "Int64?") + ")") + "");
                }
            },
            { name: "SelectedHWB", kind: "local", type: lightSwitchApplication.HWB },
            { name: "SelectedAWB", kind: "local", type: lightSwitchApplication.AWB },
            {
                name: "CustomsNotifications", kind: "collection", elementType: lightSwitchApplication.CustomsNotification,
                createQuery: function (AWBId, HWBId) {
                    return this.dataWorkspace.WFSDBData.CustomsNotifications.filter("" + ((AWBId === undefined || AWBId === null) ? "false" : "(AWB/Id eq " + $toODataString(AWBId, "Int64?") + ")") + " or " + ((HWBId === undefined || HWBId === null) ? "false" : "(HWB/Id eq " + $toODataString(HWBId, "Int64?") + ")") + "").orderByDescending("StatusTimestamp");
                }
            },
            { name: "SelectedDischargeDetail", kind: "local", type: lightSwitchApplication.DischargeDetail },
            { name: "AvailableSlac", kind: "local", type: Number },
            { name: "SelectedDischarge", kind: "local", type: lightSwitchApplication.Discharge },
            { name: "ImportFeeChargeType", kind: "local", type: lightSwitchApplication.ChargeType },
            { name: "StorageChargeType", kind: "local", type: lightSwitchApplication.ChargeType }
        ], [
            { name: "SaveDeliveryOrder" }
        ]),

        DischargeCheckIn: $defineScreen(DischargeCheckIn, [
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            {
                name: "ActiveCheckInList", kind: "collection", elementType: lightSwitchApplication.DischargeCheckInList,
                createQuery: function (IsComplete, SearchFilter, DateFilter, DriverName, CarrierName, DateStart, DateEnd) {
                    return this.dataWorkspace.WFSDBData.ActiveCheckInList(IsComplete, SearchFilter, DateFilter).filter("(" + ((CarrierName === undefined || CarrierName === null) ? "true" : "substringof(" + $toODataString(CarrierName, "String?") + ", CarrierName)") + " or " + ((DriverName === undefined || DriverName === null) ? "true" : "substringof(" + $toODataString(DriverName, "String?") + ", DriverName)") + ") and " + ((DateStart === undefined || DateStart === null) ? "true" : ((DateEnd === undefined || DateEnd === null) ? "true" : "(((CheckInTimestamp ne null) and (CheckInTimestamp ge " + $toODataString(DateStart, "DateTime?") + ")) and ((CheckInTimestamp ne null) and (CheckInTimestamp le " + $toODataString(DateEnd, "DateTime?") + ")))")) + "").orderBy("CheckInTimestamp").thenBy("CarrierName").thenBy("DriverName");
                }
            },
            { name: "TotalPiecesLabel", kind: "local", type: String },
            { name: "TotalDOLabel", kind: "local", type: String },
            { name: "TotalBalanceLabel", kind: "local", type: String },
            { name: "DateFilter", kind: "local", type: Date },
            { name: "DateStart", kind: "local", type: Date },
            { name: "DateEnd", kind: "local", type: Date },
            { name: "Filter", kind: "local", type: String },
            { name: "SearchFilter", kind: "local", type: String },
            { name: "SearchDateFilter", kind: "local", type: Date },
            { name: "IsComplete", kind: "local", type: Boolean },
            { name: "NewDischargeLabel", kind: "local", type: String },
            { name: "SelectCarrierLabel", kind: "local", type: String },
            { name: "SelectDriverLabel", kind: "local", type: String },
            { name: "AddEditCarrierLabel", kind: "local", type: String },
            { name: "AddEditDriverLabel", kind: "local", type: String },
            { name: "SelectedCarrier", kind: "local", type: lightSwitchApplication.Carrier },
            { name: "SelectedDriver", kind: "local", type: lightSwitchApplication.CarrierDriver },
            { name: "SearchDriverFilter", kind: "local", type: String },
            {
                name: "Carriers", kind: "collection", elementType: lightSwitchApplication.Carrier,
                createQuery: function (CarrierName) {
                    return this.dataWorkspace.WFSDBData.Carriers.filter("(MOT/Id eq 3) and " + ((CarrierName === undefined || CarrierName === null) ? "true" : "substringof(" + $toODataString(CarrierName, "String?") + ", CarrierName)") + "").orderBy("CarrierName");
                }
            },
            { name: "SearchCarrierFilter", kind: "local", type: String }
        ], [
            { name: "ShowNotCheckedIn" },
            { name: "ShowCheckedIn" },
            { name: "Reset" },
            { name: "Search" },
            { name: "NewDischarge" },
            { name: "LookUpShipment" },
            { name: "GoToHome" },
            { name: "CreateDischarge" },
            { name: "CancelDischarge" },
            { name: "SearchCarrier" },
            { name: "SearchDriver" },
            { name: "FilterDriverSearch" },
            { name: "FilterDriverReset" },
            { name: "FilterCarrierSearch" },
            { name: "FilterCarrierReset" },
            { name: "FilterCarrierCancel" }
        ]),

        DischargeCheckInOld: $defineScreen(DischargeCheckInOld, [
            { name: "Discharge", kind: "local", type: lightSwitchApplication.Discharge },
            { name: "DriverName", kind: "local", type: String },
            { name: "SelectedDriver", kind: "local", type: lightSwitchApplication.Driver },
            {
                name: "DischargeDetails", kind: "collection", elementType: lightSwitchApplication.DischargeDetail,
                getNavigationProperty: function () {
                    if (this.owner.Discharge) {
                        return this.owner.Discharge.details.properties.DischargeDetails;
                    }
                    return null;
                },
                appendQuery: function () {
                    return this;
                }
            },
            { name: "DischargeTaskType", kind: "local", type: lightSwitchApplication.TaskType },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "AwbEntityType", kind: "local", type: lightSwitchApplication.EntityType },
            { name: "HwbEntityType", kind: "local", type: lightSwitchApplication.EntityType },
            { name: "AmountDue", kind: "local", type: String }
        ], [
            { name: "CollectPayment" },
            { name: "SelectDriver" },
            { name: "AddDeliveryOrder" },
            { name: "CompleteCheckIn" },
            { name: "GoToHome" }
        ]),

        DischargeOld: $defineScreen(DischargeOld, [
            {
                name: "Discharges", kind: "collection", elementType: lightSwitchApplication.Discharge,
                createQuery: function (FirstName, LastName, CarrierName, LicenseNumber, StatusId) {
                    return this.dataWorkspace.WFSDBData.Discharges.filter("" + ((StatusId === undefined || StatusId === null) ? "true" : "(Status/Id eq " + $toODataString(StatusId, "Int32?") + ")") + " and (((" + ((FirstName === undefined || FirstName === null) ? "true" : "substringof(" + $toODataString(FirstName, "String?") + ", CarrierDriver/FirstName)") + " or " + ((LastName === undefined || LastName === null) ? "true" : "substringof(" + $toODataString(LastName, "String?") + ", CarrierDriver/LastName)") + ") or " + ((CarrierName === undefined || CarrierName === null) ? "true" : "substringof(" + $toODataString(CarrierName, "String?") + ", CarrierDriver/Carrier/CarrierName)") + ") or " + ((LicenseNumber === undefined || LicenseNumber === null) ? "true" : "substringof(" + $toODataString(LicenseNumber, "String?") + ", CarrierDriver/LicenseNumber)") + ")").orderBy("CheckInTimestamp").expand("Status");
                }
            },
            { name: "Filter", kind: "local", type: String },
            {
                name: "Statuses", kind: "collection", elementType: lightSwitchApplication.Status,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.Statuses.filter("(Id ge 12) and (Id le 13)").orderBy("Id");
                }
            },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "StatusImage", kind: "local", type: String },
            {
                name: "ActiveCheckIns", kind: "collection", elementType: lightSwitchApplication.Discharge,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.ActiveCheckIns().expand("Status");
                }
            }
        ], [
            { name: "GoToHome" },
            { name: "Search" },
            { name: "Reset" },
            { name: "AddDischarge" }
        ]),

        DischargePaymentsOld: $defineScreen(DischargePaymentsOld, [
            {
                name: "AllDischargeCharges", kind: "collection", elementType: lightSwitchApplication.DischargeCharge,
                createQuery: function (DischargeId) {
                    return this.dataWorkspace.WFSDBData.AllDischargeCharges(DischargeId).orderBy("DischargeDetail/AWB/AWBSerialNumber").thenBy("DischargeDetail/HWB/HWBSerialNumber").expand("DischargeDetail").expand("DischargeDetail.AWB").expand("DischargeDetail.AWB.Carrier").expand("DischargeDetail.HWB").expand("ChargeType").expand("PaymentType");
                }
            },
            { name: "SelectedDischarge", kind: "local", type: lightSwitchApplication.Discharge },
            { name: "TotalCharges", kind: "local", type: String },
            { name: "TotalAmountDue", kind: "local", type: String },
            { name: "TotalAmountPaid", kind: "local", type: String }
        ], [
            { name: "ViewPayments" },
            { name: "AddPayment" },
            { name: "AddCharge" },
            { name: "GoToHome" }
        ]),

        DischargeShipmentsOld: $defineScreen(DischargeShipmentsOld, [
            { name: "Filter", kind: "local", type: String },
            { name: "SelectedDischarge", kind: "local", type: lightSwitchApplication.Discharge },
            {
                name: "HWBs", kind: "collection", elementType: lightSwitchApplication.HWB,
                createQuery: function (HWBSerialNumber) {
                    return this.dataWorkspace.WFSDBData.HWBs.filter("" + ((HWBSerialNumber === undefined || HWBSerialNumber === null) ? "true" : "substringof(" + $toODataString(HWBSerialNumber, "String?") + ", HWBSerialNumber)") + "").orderBy("HWBSerialNumber");
                }
            },
            {
                name: "AWBs", kind: "collection", elementType: lightSwitchApplication.AWB,
                createQuery: function (AWBSerialNumber, Carrier3Code) {
                    return this.dataWorkspace.WFSDBData.AWBs.filter("" + ((AWBSerialNumber === undefined || AWBSerialNumber === null) ? "true" : "substringof(" + $toODataString(AWBSerialNumber, "String?") + ", AWBSerialNumber)") + " or " + ((Carrier3Code === undefined || Carrier3Code === null) ? "true" : "substringof(" + $toODataString(Carrier3Code, "String?") + ", Carrier/Carrier3Code)") + "").orderBy("Carrier/Carrier3Code").thenBy("AWBSerialNumber").expand("Carrier");
                }
            },
            { name: "Filter1", kind: "local", type: String }
        ], [
            { name: "Search" },
            { name: "Reset" },
            { name: "Search1" },
            { name: "Reset1" },
            { name: "CreateHWBDO" },
            { name: "CreateAWBDO" }
        ]),

        Home: $defineScreen(Home, [
            { name: "AjaxTimeout", kind: "local", type: Number }
        ], [
            { name: "Config" }
        ]),

        InventoryTask: $defineScreen(InventoryTask, [
            { name: "SelectedInventoryTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "ScanLocationLabel", kind: "local", type: String },
            { name: "ScanShipmentLabel", kind: "local", type: String },
            { name: "SelectedLocation", kind: "local", type: lightSwitchApplication.WarehouseLocation },
            {
                name: "ActiveLocationItems", kind: "collection", elementType: lightSwitchApplication.InventoryLocationItem,
                createQuery: function (WarehouseLocationId) {
                    return this.dataWorkspace.WFSDBData.ActiveLocationItems().filter("" + ((WarehouseLocationId === undefined || WarehouseLocationId === null) ? "false" : "((WarehouseLocationId ne null) and (WarehouseLocationId eq " + $toODataString(WarehouseLocationId, "Int32?") + "))") + "");
                }
            },
            { name: "CountLabel", kind: "local", type: String },
            { name: "SnapshotImage", kind: "local", type: String },
            { name: "AwbEntityType", kind: "local", type: lightSwitchApplication.EntityType },
            { name: "HwbEntityType", kind: "local", type: lightSwitchApplication.EntityType },
            { name: "UldEntityType", kind: "local", type: lightSwitchApplication.EntityType },
            { name: "ScannerCount", kind: "local", type: String },
            {
                name: "ActiveScannerView", kind: "collection", elementType: lightSwitchApplication.ForkliftView,
                createQuery: function (TaskId, UserName) {
                    return this.dataWorkspace.WFSDBData.ActiveScannerView(TaskId).filter("" + ((UserName === undefined || UserName === null) ? "true" : "(UserName eq " + $toODataString(UserName, "String?") + ")") + "").orderBy("RefNumber").thenBy("RefNumber1");
                }
            },
            { name: "ScannerViewPopupHeader", kind: "local", type: String },
            { name: "PiecesLabel", kind: "local", type: String },
            { name: "AddToScannerHeader", kind: "local", type: String },
            { name: "SelectedQuantity", kind: "local", type: String },
            { name: "ConfirmPopupHeader", kind: "local", type: String },
            { name: "ConfirmText", kind: "local", type: String },
            { name: "EntityType", kind: "local", type: String },
            { name: "EntityId", kind: "local", type: String },
            { name: "EntityCount", kind: "local", type: String },
            { name: "AddToScannerText", kind: "local", type: String },
            { name: "PromptPopupHeader", kind: "local", type: String },
            { name: "PromptPopupText", kind: "local", type: String },
            { name: "SelectLocationPopupHeader", kind: "local", type: String },
            {
                name: "WarehouseLocations", kind: "collection", elementType: lightSwitchApplication.WarehouseLocation,
                createQuery: function (Location, LocationType) {
                    return this.dataWorkspace.WFSDBData.WarehouseLocations.filter("" + ((Location === undefined || Location === null) ? "true" : "substringof(" + $toODataString(Location, "String?") + ", Location)") + " or " + ((LocationType === undefined || LocationType === null) ? "true" : "substringof(" + $toODataString(LocationType, "String?") + ", WarehouseLocationType/LocationType)") + "").orderBy("WarehouseLocationType/LocationType").thenBy("Location").expand("WarehouseLocationType");
                }
            },
            { name: "LocationFilter", kind: "local", type: String },
            { name: "CloseLocationHeader", kind: "local", type: String },
            { name: "CloseLocationText", kind: "local", type: String },
            { name: "TempLocation", kind: "local", type: lightSwitchApplication.WarehouseLocation },
            {
                name: "InventoryHistory", kind: "collection", elementType: lightSwitchApplication.Transaction,
                createQuery: function (TaskId, Reference, UserName, Description) {
                    return this.dataWorkspace.WFSDBData.InventoryHistory(TaskId).filter("(" + ((Reference === undefined || Reference === null) ? "true" : "substringof(" + $toODataString(Reference, "String?") + ", Reference)") + " or " + ((Description === undefined || Description === null) ? "true" : "substringof(" + $toODataString(Description, "String?") + ", Description)") + ") or " + ((UserName === undefined || UserName === null) ? "true" : "substringof(" + $toODataString(UserName, "String?") + ", UserName)") + "").orderByDescending("StatusTimestamp");
                }
            },
            { name: "HistoryFilter", kind: "local", type: String },
            { name: "FinalizeInventoryHeader", kind: "local", type: String },
            { name: "FinalizeInventoryText", kind: "local", type: String },
            { name: "SearchShipmentHeader", kind: "local", type: String },
            {
                name: "SearchLocationItems", kind: "collection", elementType: lightSwitchApplication.InventoryLocationItem,
                createQuery: function (RefNumber, RefNumber1) {
                    return this.dataWorkspace.WFSDBData.SearchLocationItems().filter("" + ((RefNumber === undefined || RefNumber === null) ? "false" : "substringof(" + $toODataString(RefNumber, "String?") + ", RefNumber)") + " or " + ((RefNumber1 === undefined || RefNumber1 === null) ? "false" : "substringof(" + $toODataString(RefNumber1, "String?") + ", RefNumber1)") + "").orderBy("RefNumber").thenBy("RefNumber1").expand("WarehouseLocation");
                }
            },
            { name: "SearchCriteria", kind: "local", type: String },
            { name: "CountLabel1", kind: "local", type: String },
            { name: "LocationLabel", kind: "local", type: String }
        ], [
            { name: "ShowSnapshot" },
            { name: "GoToHome" },
            { name: "AddToScanner" },
            { name: "Reset" },
            { name: "ShowAddToScannerPopup" },
            { name: "ConfirmYes" },
            { name: "ConfirmNo" },
            { name: "PromptOk" },
            { name: "Reset1" },
            { name: "SelectLocation" },
            { name: "SetLocation" },
            { name: "ConfirmYes1" },
            { name: "ConfirmNo1" },
            { name: "GoToHome1" },
            { name: "Reset2" },
            { name: "ConfirmYes2" },
            { name: "ConfirmNo2" },
            { name: "Finalize" },
            { name: "Reset3" },
            { name: "Shipment" },
            { name: "SearchSelectedShipment" }
        ]),

        OnhandReceipt: $defineScreen(OnhandReceipt, [
        ], [
        ]),

        PutAwayPieces: $defineScreen(PutAwayPieces, [
            { name: "SelectedFlt", kind: "local", type: lightSwitchApplication.FlightManifest },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "SelectedLocation", kind: "local", type: lightSwitchApplication.WarehouseLocation },
            { name: "UldEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            { name: "HwbEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            { name: "AwbEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            { name: "FltEntityType", kind: "local", type: lightSwitchApplication.EntityType },
            {
                name: "ActiveForkliftView", kind: "collection", elementType: lightSwitchApplication.ForkliftView,
                createQuery: function (TaskId, UserName) {
                    return this.dataWorkspace.WFSDBData.ActiveForkliftView(TaskId).filter("" + ((UserName === undefined || UserName === null) ? "true" : "(UserName eq " + $toODataString(UserName, "String?") + ")") + "").orderBy("RefNumber");
                }
            },
            { name: "ReceiverTaskType", kind: "local", type: lightSwitchApplication.TaskType },
            { name: "CompleteStatus", kind: "local", type: lightSwitchApplication.Status },
            {
                name: "WarehouseLocations", kind: "collection", elementType: lightSwitchApplication.WarehouseLocation,
                createQuery: function (LocationType, Location) {
                    return this.dataWorkspace.WFSDBData.WarehouseLocations.filter("" + ((LocationType === undefined || LocationType === null) ? "true" : "substringof(" + $toODataString(LocationType, "String?") + ", WarehouseLocationType/LocationType)") + " or " + ((Location === undefined || Location === null) ? "true" : "substringof(" + $toODataString(Location, "String?") + ", Location)") + "").orderBy("WarehouseLocationType/LocationType").thenBy("Location").expand("WarehouseLocationType");
                }
            },
            { name: "SnapshotImage", kind: "local", type: String },
            { name: "SnapshotImage1", kind: "local", type: String },
            { name: "InitRemPutAwayPieces", kind: "local", type: Number },
            { name: "EnteredQuantity", kind: "local", type: String },
            { name: "EnteredLocation", kind: "local", type: String },
            { name: "TotalPutAwayPieces", kind: "local", type: Number },
            { name: "TotalPieces", kind: "local", type: Number },
            { name: "PercentPutAway", kind: "local", type: Number },
            { name: "LocationFilter", kind: "local", type: String },
            { name: "SelectLocationPopupHeader", kind: "local", type: String },
            { name: "ForkLiftCount", kind: "local", type: String },
            {
                name: "ForkliftDetails", kind: "collection", elementType: lightSwitchApplication.ForkliftDetail,
                createQuery: function (TaskId) {
                    return this.dataWorkspace.WFSDBData.ForkliftDetails.filter("" + ((TaskId === undefined || TaskId === null) ? "true" : "(Task/Id eq " + $toODataString(TaskId, "Int64?") + ")") + " and (HWB/Id gt 0)").orderBy("HWB/HWBSerialNumber");
                }
            },
            { name: "AddToForkliftHeader", kind: "local", type: String },
            { name: "ForkliftViewPopupHeader", kind: "local", type: String },
            { name: "SelectedQuantity", kind: "local", type: Number },
            { name: "PiecesLabel", kind: "local", type: String },
            { name: "ConfirmPopupHeader", kind: "local", type: String },
            { name: "ConfirmText", kind: "local", type: String },
            { name: "FinalizePopupHeader", kind: "local", type: String },
            { name: "FinalizeText", kind: "local", type: String },
            { name: "LocationLabel", kind: "local", type: String },
            { name: "CountLabel", kind: "local", type: String },
            { name: "UnAssignCount", kind: "local", type: String },
            { name: "UnAssignLocationPopupHeader", kind: "local", type: String },
            { name: "UnAssignText", kind: "local", type: String },
            { name: "UnAssignULDText", kind: "local", type: String },
            { name: "EntityType", kind: "local", type: String },
            { name: "EntityId", kind: "local", type: String },
            { name: "AddToForkliftText", kind: "local", type: String },
            { name: "UpdatePopupHeader", kind: "local", type: String },
            { name: "UpdateText", kind: "local", type: String },
            { name: "MaxPutAwayPieces", kind: "local", type: Number },
            { name: "SearchHistoryFilter", kind: "local", type: String },
            { name: "SearchLocationFilter", kind: "local", type: String }
        ], [
            { name: "GoToHome" },
            { name: "Recover" },
            { name: "Breakdown" },
            { name: "FinalizeFlight" },
            { name: "PutAway" },
            { name: "Reset" },
            { name: "Reset1" },
            { name: "PutPiecesAway" },
            { name: "SelectPutAwayLocation" },
            { name: "Reset3" },
            { name: "SelectLocation" },
            { name: "PutULDAway" },
            { name: "PutAwayPieces1" },
            { name: "PutAway1" },
            { name: "ShowAddToForkliftPopup" },
            { name: "ShowSnapshot" },
            { name: "ConfirmYes" },
            { name: "ConfirmNo" },
            { name: "FinalizeYes" },
            { name: "FinalizeNo" },
            { name: "Reset2" },
            { name: "ShowSnapshot1" },
            { name: "ShowUnAssignLocationPopup" },
            { name: "Reset4" },
            { name: "UpdateOk" },
            { name: "Search" },
            { name: "Search1" },
            { name: "Search2" }
        ]),

        RecoverFlight: $defineScreen(RecoverFlight, [
            { name: "SelectedFlight", kind: "local", type: lightSwitchApplication.FlightManifest },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "AjaxTimeout", kind: "local", type: Number },
            {
                name: "FlightHistory", kind: "collection", elementType: lightSwitchApplication.Transaction,
                createQuery: function (FlightManifestId, Description, Reference, UserName) {
                    return this.dataWorkspace.WFSDBData.FlightHistory(FlightManifestId).filter("(" + ((Description === undefined || Description === null) ? "true" : "substringof(" + $toODataString(Description, "String?") + ", Description)") + " or " + ((Reference === undefined || Reference === null) ? "true" : "substringof(" + $toODataString(Reference, "String?") + ", Reference)") + ") or " + ((UserName === undefined || UserName === null) ? "true" : "substringof(" + $toODataString(UserName, "String?") + ", UserName)") + "").orderByDescending("StatusTimestamp");
                }
            },
            {
                name: "WarehouseLocations", kind: "collection", elementType: lightSwitchApplication.WarehouseLocation,
                createQuery: function (Location, LocationType) {
                    return this.dataWorkspace.WFSDBData.WarehouseLocations.filter("" + ((Location === undefined || Location === null) ? "true" : "substringof(" + $toODataString(Location, "String?") + ", Location)") + " or " + ((LocationType === undefined || LocationType === null) ? "true" : "substringof(" + $toODataString(LocationType, "String?") + ", WarehouseLocationType/LocationType)") + "").orderBy("WarehouseLocationType/LocationType").thenBy("Location").expand("WarehouseLocationType");
                }
            },
            { name: "UldEntityType", kind: "local", type: lightSwitchApplication.EntityType },
            { name: "UldEntityType1", kind: "local", type: lightSwitchApplication.EntityType1 },
            { name: "Filter", kind: "local", type: String },
            { name: "HistoryFilter", kind: "local", type: String },
            { name: "RecoverQuantity", kind: "local", type: Number },
            { name: "LocationFilter", kind: "local", type: String },
            { name: "SetStagePopupHeader", kind: "local", type: String },
            { name: "RecoverPopupHeader", kind: "local", type: String },
            { name: "RecoverULDLabel", kind: "local", type: String },
            { name: "StatusId", kind: "local", type: Number },
            { name: "StatusId1", kind: "local", type: Number },
            { name: "StatusId2", kind: "local", type: Number },
            { name: "ConfirmPopupHeader", kind: "local", type: String },
            { name: "ConfirmText", kind: "local", type: String },
            { name: "IsSelected", kind: "local", type: String },
            { name: "RecoverMultipleUldsHeader", kind: "local", type: String },
            { name: "SearchFilter", kind: "local", type: String },
            { name: "SearchHistoryFilter", kind: "local", type: String },
            { name: "SearchLocationFilter", kind: "local", type: String }
        ], [
            { name: "Recover" },
            { name: "Breakdown" },
            { name: "ChangeStageLocation" },
            { name: "GoToHome" },
            { name: "BreakdownAvailULDs" },
            { name: "PutAway" },
            { name: "Search" },
            { name: "Reset" },
            { name: "Search1" },
            { name: "Reset1" },
            { name: "SetStageLocation" },
            { name: "Search2" },
            { name: "Reset2" },
            { name: "RecoverBUP" },
            { name: "OpenRecoverPopup" },
            { name: "ShowNotRecovered" },
            { name: "ShowRecovered" },
            { name: "UnRecoverUld" },
            { name: "ConfirmYes" },
            { name: "ConfirmNo" },
            { name: "RecoverSelected" },
            { name: "RecoverUlds" },
            { name: "RecoverUldsAsBUP" }
        ]),

        ScanPutAwayPieces: $defineScreen(ScanPutAwayPieces, [
            { name: "SelectedFlt", kind: "local", type: lightSwitchApplication.FlightManifest },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "MaxPutAwayPieces", kind: "local", type: Number },
            { name: "ForkliftCount", kind: "local", type: String }
        ], [
        ]),

        SelectCarrier: $defineScreen(SelectCarrier, [
            {
                name: "Carriers", kind: "collection", elementType: lightSwitchApplication.Carrier,
                createQuery: function (CarrierName) {
                    return this.dataWorkspace.WFSDBData.Carriers.filter("(MOT/Id eq 3) and " + ((CarrierName === undefined || CarrierName === null) ? "true" : "substringof(" + $toODataString(CarrierName, "String?") + ", CarrierName)") + "").orderBy("CarrierName");
                }
            },
            { name: "Filter", kind: "local", type: String },
            { name: "SelectedDriver", kind: "local", type: lightSwitchApplication.Driver },
            { name: "SelectedCarrierName", kind: "local", type: lightSwitchApplication.Carrier }
        ], [
            { name: "EditCarrier" },
            { name: "SelectedCarrier" },
            { name: "Search" },
            { name: "Reset" }
        ]),

        SelectDriver: $defineScreen(SelectDriver, [
            {
                name: "Drivers", kind: "collection", elementType: lightSwitchApplication.Driver,
                createQuery: function (FirstName, LastName, LicenseNumber, CarrierName) {
                    return this.dataWorkspace.WFSDBData.Drivers.filter("((" + ((FirstName === undefined || FirstName === null) ? "true" : "substringof(" + $toODataString(FirstName, "String?") + ", FirstName)") + " or " + ((LastName === undefined || LastName === null) ? "true" : "substringof(" + $toODataString(LastName, "String?") + ", LastName)") + ") or " + ((LicenseNumber === undefined || LicenseNumber === null) ? "true" : "substringof(" + $toODataString(LicenseNumber, "String?") + ", LicenseNumber)") + ") or " + ((CarrierName === undefined || CarrierName === null) ? "true" : "substringof(" + $toODataString(CarrierName, "String?") + ", Carrier/CarrierName)") + "").orderBy("FirstName").thenBy("LastName").expand("Carrier");
                }
            },
            { name: "SelectedDischarge", kind: "local", type: lightSwitchApplication.Discharge },
            { name: "Filter", kind: "local", type: String }
        ], [
            { name: "SelectDischargeDriver" },
            { name: "EditDriver" },
            { name: "Search" },
            { name: "Reset" }
        ]),

        Snapshot: $defineScreen(Snapshot, [
            { name: "SelectedEntityType", kind: "local", type: lightSwitchApplication.EntityType1 },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "SelectedEntityId", kind: "local", type: String },
            { name: "EntityDescription", kind: "local", type: String },
            { name: "AjaxTimeout", kind: "local", type: Number },
            {
                name: "Snapshots", kind: "collection", elementType: lightSwitchApplication.Snapshot1,
                createQuery: function (EntityTypeId, EntityId) {
                    return this.dataWorkspace.SnapshotDBData.Snapshots.filter("" + ((EntityTypeId === undefined || EntityTypeId === null) ? "true" : "(EntityType/Id eq " + $toODataString(EntityTypeId, "Int32?") + ")") + " and " + ((EntityId === undefined || EntityId === null) ? "true" : "((EntityId ne null) and (EntityId eq " + $toODataString(EntityId, "Int64?") + "))") + "").orderByDescending("Timestamp");
                }
            },
            { name: "Quantity", kind: "local", type: Number },
            { name: "Filter", kind: "local", type: String },
            { name: "SelectConditionLabel", kind: "local", type: String },
            { name: "Condition", kind: "local", type: String },
            { name: "SelectedConditionIds", kind: "local", type: String },
            { name: "DefaultCondition", kind: "local", type: lightSwitchApplication.Condition1 }
        ], [
            { name: "OpenImageViewer" },
            { name: "TakePicture" },
            { name: "SelectCondition" },
            { name: "SelectConditions" }
        ]),

        SnapshotImager: $defineScreen(SnapshotImager, [
            { name: "SnapshotImage", kind: "local", type: String },
            { name: "EffectedQuantity", kind: "local", type: Number }
        ], [
            { name: "TakePicture" },
            { name: "NewPhoto" },
            { name: "NewPhoto_Tap" },
            { name: "TakePicture_Tap" },
            { name: "DeleteImage" },
            { name: "Conditions" },
            { name: "SelectCondition" }
        ]),

        TaskManager: $defineScreen(TaskManager, [
            {
                name: "TaskTypes", kind: "collection", elementType: lightSwitchApplication.TaskType,
                createQuery: function () {
                    return this.dataWorkspace.WFSDBData.TaskTypes.filter("(IsActive ne null) and (IsActive eq true)").orderBy("ActionName");
                }
            }
        ], [
            { name: "SelectTaskType" },
            { name: "GoToHome" }
        ]),

        TrackAndTrace: $defineScreen(TrackAndTrace, [
            { name: "CarrierCode", kind: "local", type: String },
            { name: "AwbNumber", kind: "local", type: String },
            { name: "HwbNumber", kind: "local", type: String },
            { name: "AWBDash", kind: "local", type: String },
            {
                name: "AWBs", kind: "collection", elementType: lightSwitchApplication.AWB,
                createQuery: function (Carrier3Code, AWBSerialNumber) {
                    return this.dataWorkspace.WFSDBData.AWBs.filter("" + ((Carrier3Code === undefined || Carrier3Code === null) ? "false" : "(Carrier/Carrier3Code eq " + $toODataString(Carrier3Code, "String?") + ")") + " and " + ((AWBSerialNumber === undefined || AWBSerialNumber === null) ? "false" : "(AWBSerialNumber eq " + $toODataString(AWBSerialNumber, "String?") + ")") + "");
                }
            },
            {
                name: "HWBs", kind: "collection", elementType: lightSwitchApplication.HWB,
                createQuery: function (HWBSerialNumber) {
                    return this.dataWorkspace.WFSDBData.HWBs.filter("" + ((HWBSerialNumber === undefined || HWBSerialNumber === null) ? "false" : "(HWBSerialNumber eq " + $toODataString(HWBSerialNumber, "String?") + ")") + "");
                }
            },
            { name: "ShipmentReference", kind: "local", type: String },
            { name: "ShipmentPieces", kind: "local", type: String },
            { name: "ShipmentWeight", kind: "local", type: String },
            { name: "ShipmentStatus", kind: "local", type: String },
            { name: "ShipmentCharges", kind: "local", type: String },
            { name: "ShipmentChargesPlus1", kind: "local", type: String },
            { name: "ShipmentChargesPlus2", kind: "local", type: String },
            { name: "ShipmentClearance", kind: "local", type: String },
            { name: "SearchCarrier", kind: "local", type: String },
            { name: "SearchAwb", kind: "local", type: String },
            { name: "SearchHwb", kind: "local", type: String },
            { name: "ShipmentHwbCount", kind: "local", type: String },
            { name: "RecordNotFound", kind: "local", type: String },
            { name: "ShipmentReceivedDate", kind: "local", type: String },
            {
                name: "ActiveTrackTraceView", kind: "collection", elementType: lightSwitchApplication.TrackTraceView,
                createQuery: function (Reference) {
                    return this.dataWorkspace.WFSDBData.ActiveTrackTraceView(Reference);
                }
            },
            { name: "SearchReference", kind: "local", type: String },
            { name: "SelectedReference", kind: "local", type: String }
        ], [
            { name: "Reset" },
            { name: "GoToHome" },
            { name: "SearchShipment" },
            { name: "ResetSearch" }
        ]),

        ViewDischargePaymentsOld: $defineScreen(ViewDischargePaymentsOld, [
            {
                name: "AllDischargePayments", kind: "collection", elementType: lightSwitchApplication.DischargePayment,
                createQuery: function (DischargeId) {
                    return this.dataWorkspace.WFSDBData.AllDischargePayments(DischargeId).expand("PaymentType");
                }
            },
            { name: "SelectedDischarge", kind: "local", type: lightSwitchApplication.Discharge }
        ], [
            { name: "GoToHome" }
        ]),

        ViewFlightManifest: $defineScreen(ViewFlightManifest, [
            { name: "SelectedFlightUnloadPort", kind: "local", type: lightSwitchApplication.ReceiverFlightView },
            { name: "CurrentUser", kind: "local", type: String },
            { name: "CurrentUserStation", kind: "local", type: lightSwitchApplication.Port },
            { name: "CurrentUserWarehouse", kind: "local", type: lightSwitchApplication.Warehouse },
            { name: "AjaxTimeout", kind: "local", type: Number },
            { name: "SelectedTask", kind: "local", type: lightSwitchApplication.Task },
            { name: "FlightManifest", kind: "local", type: lightSwitchApplication.FlightManifest },
            {
                name: "ManifestAWBDetails", kind: "collection", elementType: lightSwitchApplication.ManifestAWBDetail,
                getNavigationProperty: function () {
                    if (this.owner.FlightManifest) {
                        return this.owner.FlightManifest.details.properties.ManifestAWBDetails;
                    }
                    return null;
                },
                appendQuery: function (UnitType, SerialNumber, AWBSerialNumber, UnloadingPortId) {
                    return this.filter("" + ((UnloadingPortId === undefined || UnloadingPortId === null) ? "false" : "(UnloadingPort/Port/Id eq " + $toODataString(UnloadingPortId, "Int32?") + ")") + " and ((" + ((UnitType === undefined || UnitType === null) ? "true" : "substringof(" + $toODataString(UnitType, "String?") + ", ULD/ShipmentUnitType/Code)") + " or " + ((SerialNumber === undefined || SerialNumber === null) ? "true" : "substringof(" + $toODataString(SerialNumber, "String?") + ", ULD/SerialNumber)") + ") or " + ((AWBSerialNumber === undefined || AWBSerialNumber === null) ? "true" : "substringof(" + $toODataString(AWBSerialNumber, "String?") + ", AWB/AWBSerialNumber)") + ")").orderBy("ULD/ShipmentUnitType/Code").thenBy("ULD/SerialNumber").thenBy("AWB/AWBSerialNumber");
                }
            },
            { name: "Filter", kind: "local", type: String },
            {
                name: "FlightHistory", kind: "collection", elementType: lightSwitchApplication.Transaction,
                createQuery: function (FlightManifestId, Description, Reference, UserName) {
                    return this.dataWorkspace.WFSDBData.FlightHistory(FlightManifestId).filter("(" + ((Description === undefined || Description === null) ? "true" : "substringof(" + $toODataString(Description, "String?") + ", Description)") + " or " + ((Reference === undefined || Reference === null) ? "true" : "substringof(" + $toODataString(Reference, "String?") + ", Reference)") + ") or " + ((UserName === undefined || UserName === null) ? "true" : "substringof(" + $toODataString(UserName, "String?") + ", UserName)") + "").orderByDescending("StatusTimestamp");
                }
            },
            { name: "HistoryFilter", kind: "local", type: String },
            {
                name: "WarehouseLocations", kind: "collection", elementType: lightSwitchApplication.WarehouseLocation,
                createQuery: function (Location, LocationType) {
                    return this.dataWorkspace.WFSDBData.WarehouseLocations.filter("" + ((Location === undefined || Location === null) ? "true" : "substringof(" + $toODataString(Location, "String?") + ", Location)") + " or " + ((LocationType === undefined || LocationType === null) ? "true" : "substringof(" + $toODataString(LocationType, "String?") + ", WarehouseLocationType/LocationType)") + "").orderBy("WarehouseLocationType/LocationType").thenBy("Location").expand("WarehouseLocationType");
                }
            },
            { name: "LocationFilter", kind: "local", type: String },
            { name: "SetStageLocationPopupHeader", kind: "local", type: String },
            {
                name: "ReceiverAwbList", kind: "collection", elementType: lightSwitchApplication.ReceiverAwbManifestList,
                createQuery: function (FlightManifestId, AwbNumber) {
                    return this.dataWorkspace.WFSDBData.ReceiverAwbList(FlightManifestId).filter("" + ((AwbNumber === undefined || AwbNumber === null) ? "true" : "substringof(" + $toODataString(AwbNumber, "String?") + ", AwbNumber)") + "").orderBy("AwbNumber");
                }
            },
            { name: "AwbFilter", kind: "local", type: String },
            {
                name: "ReceiverUldList", kind: "collection", elementType: lightSwitchApplication.ReceiverUldManifestList,
                createQuery: function (FlightManifestId, UldNumber) {
                    return this.dataWorkspace.WFSDBData.ReceiverUldList(FlightManifestId).filter("" + ((UldNumber === undefined || UldNumber === null) ? "true" : "substringof(" + $toODataString(UldNumber, "String?") + ", UldNumber)") + "").orderBy("UldNumber");
                }
            },
            { name: "UldFilter", kind: "local", type: String },
            { name: "SearchUldFilter", kind: "local", type: String },
            { name: "SearchAwbFilter", kind: "local", type: String },
            { name: "SearchHistoryFilter", kind: "local", type: String },
            { name: "SearchLocationFilter", kind: "local", type: String }
        ], [
            { name: "PutAway" },
            { name: "Recover" },
            { name: "Breakdown" },
            { name: "GoToHome" },
            { name: "Search" },
            { name: "Reset" },
            { name: "Search1" },
            { name: "Reset1" },
            { name: "Search2" },
            { name: "Reset2" },
            { name: "SetStageLocation" },
            { name: "Reset3" },
            { name: "Reset4" },
            { name: "Search3" }
        ]),

        showAddCarrierName: $defineShowScreen(function showAddCarrierName(Carrier, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddCarrierName screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("AddCarrierName", parameters, options);
        }),

        showAddDischargeChargeOld: $defineShowScreen(function showAddDischargeChargeOld(SelectedDischarge, SelectedCharge, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddDischargeChargeOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 2);
            return lightSwitchApplication.showScreen("AddDischargeChargeOld", parameters, options);
        }),

        showAddDischargePaymentsOld: $defineShowScreen(function showAddDischargePaymentsOld(TotalAmountDue, SelectedCharges, SelectedDischarge, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddDischargePaymentsOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 3);
            return lightSwitchApplication.showScreen("AddDischargePaymentsOld", parameters, options);
        }),

        showAddEditCarrier: $defineShowScreen(function showAddEditCarrier(Carrier, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddEditCarrier screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("AddEditCarrier", parameters, options);
        }),

        showAddEditCondition: $defineShowScreen(function showAddEditCondition(Condition, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddEditCondition screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("AddEditCondition", parameters, options);
        }),

        showAddEditDriver: $defineShowScreen(function showAddEditDriver(Driver, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddEditDriver screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("AddEditDriver", parameters, options);
        }),

        showAddEditStatus: $defineShowScreen(function showAddEditStatus(Status, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddEditStatus screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("AddEditStatus", parameters, options);
        }),

        showAddEditWarehouseLocation: $defineShowScreen(function showAddEditWarehouseLocation(WarehouseLocation, options) {
            /// <summary>
            /// Asynchronously navigates forward to the AddEditWarehouseLocation screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("AddEditWarehouseLocation", parameters, options);
        }),

        showBarcodeParser: $defineShowScreen(function showBarcodeParser(AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the BarcodeParser screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("BarcodeParser", parameters, options);
        }),

        showBreakdownAWB: $defineShowScreen(function showBreakdownAWB(SelectedUld, SelectedFlt, CurrentUser, CurrentUserStation, CurrentUserWarehouse, SelectedTask, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the BreakdownAWB screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 7);
            return lightSwitchApplication.showScreen("BreakdownAWB", parameters, options);
        }),

        showBreakdownULD: $defineShowScreen(function showBreakdownULD(SelectedFlt, CurrentUser, CurrentUserStation, CurrentUserWarehouse, SelectedTask, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the BreakdownULD screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 6);
            return lightSwitchApplication.showScreen("BreakdownULD", parameters, options);
        }),

        showBrowseCarriers: $defineShowScreen(function showBrowseCarriers(options) {
            /// <summary>
            /// Asynchronously navigates forward to the BrowseCarriers screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("BrowseCarriers", parameters, options);
        }),

        showBrowseConditions: $defineShowScreen(function showBrowseConditions(options) {
            /// <summary>
            /// Asynchronously navigates forward to the BrowseConditions screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("BrowseConditions", parameters, options);
        }),

        showBrowseStatuses: $defineShowScreen(function showBrowseStatuses(options) {
            /// <summary>
            /// Asynchronously navigates forward to the BrowseStatuses screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("BrowseStatuses", parameters, options);
        }),

        showBrowseWarehouseLocations: $defineShowScreen(function showBrowseWarehouseLocations(options) {
            /// <summary>
            /// Asynchronously navigates forward to the BrowseWarehouseLocations screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("BrowseWarehouseLocations", parameters, options);
        }),

        showCargoDischarge: $defineShowScreen(function showCargoDischarge(options) {
            /// <summary>
            /// Asynchronously navigates forward to the CargoDischarge screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("CargoDischarge", parameters, options);
        }),

        showCargoInventory: $defineShowScreen(function showCargoInventory(AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the CargoInventory screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("CargoInventory", parameters, options);
        }),

        showCargoInventoryTasks: $defineShowScreen(function showCargoInventoryTasks(options) {
            /// <summary>
            /// Asynchronously navigates forward to the CargoInventoryTasks screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("CargoInventoryTasks", parameters, options);
        }),

        showCargoReceiver: $defineShowScreen(function showCargoReceiver(AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the CargoReceiver screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("CargoReceiver", parameters, options);
        }),

        showCargoReceiverTasks: $defineShowScreen(function showCargoReceiverTasks(options) {
            /// <summary>
            /// Asynchronously navigates forward to the CargoReceiverTasks screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("CargoReceiverTasks", parameters, options);
        }),

        showCargoSnapshot: $defineShowScreen(function showCargoSnapshot(AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the CargoSnapshot screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("CargoSnapshot", parameters, options);
        }),

        showCargoSnapshot_old: $defineShowScreen(function showCargoSnapshot_old(SelectedEntityType, CurrentUser, SelectedEntityId, EntityDescription, options) {
            /// <summary>
            /// Asynchronously navigates forward to the CargoSnapshot_old screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 4);
            return lightSwitchApplication.showScreen("CargoSnapshot_old", parameters, options);
        }),

        showCheckInAWBHWB: $defineShowScreen(function showCheckInAWBHWB(SelectedFlt, SelectedUld, SelectedMawb, CurrentUser, CurrentUserStation, CurrentUserWarehouse, SelectedTask, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the CheckInAWBHWB screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 8);
            return lightSwitchApplication.showScreen("CheckInAWBHWB", parameters, options);
        }),

        showConfig: $defineShowScreen(function showConfig(AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the Config screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("Config", parameters, options);
        }),

        showDeliveryOrderDetailsOld: $defineShowScreen(function showDeliveryOrderDetailsOld(SelectedHWB, SelectedAWB, SelectedDischarge, options) {
            /// <summary>
            /// Asynchronously navigates forward to the DeliveryOrderDetailsOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 3);
            return lightSwitchApplication.showScreen("DeliveryOrderDetailsOld", parameters, options);
        }),

        showDischargeCheckIn: $defineShowScreen(function showDischargeCheckIn(options) {
            /// <summary>
            /// Asynchronously navigates forward to the DischargeCheckIn screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("DischargeCheckIn", parameters, options);
        }),

        showDischargeCheckInOld: $defineShowScreen(function showDischargeCheckInOld(Discharge, CurrentUser, options) {
            /// <summary>
            /// Asynchronously navigates forward to the DischargeCheckInOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 2);
            return lightSwitchApplication.showScreen("DischargeCheckInOld", parameters, options);
        }),

        showDischargeOld: $defineShowScreen(function showDischargeOld(options) {
            /// <summary>
            /// Asynchronously navigates forward to the DischargeOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("DischargeOld", parameters, options);
        }),

        showDischargePaymentsOld: $defineShowScreen(function showDischargePaymentsOld(SelectedDischarge, options) {
            /// <summary>
            /// Asynchronously navigates forward to the DischargePaymentsOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("DischargePaymentsOld", parameters, options);
        }),

        showDischargeShipmentsOld: $defineShowScreen(function showDischargeShipmentsOld(SelectedDischarge, options) {
            /// <summary>
            /// Asynchronously navigates forward to the DischargeShipmentsOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("DischargeShipmentsOld", parameters, options);
        }),

        showHome: $defineShowScreen(function showHome(options) {
            /// <summary>
            /// Asynchronously navigates forward to the Home screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("Home", parameters, options);
        }),

        showInventoryTask: $defineShowScreen(function showInventoryTask(SelectedInventoryTask, CurrentUser, CurrentUserStation, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the InventoryTask screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 4);
            return lightSwitchApplication.showScreen("InventoryTask", parameters, options);
        }),

        showOnhandReceipt: $defineShowScreen(function showOnhandReceipt(options) {
            /// <summary>
            /// Asynchronously navigates forward to the OnhandReceipt screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("OnhandReceipt", parameters, options);
        }),

        showPutAwayPieces: $defineShowScreen(function showPutAwayPieces(SelectedFlt, CurrentUser, CurrentUserStation, CurrentUserWarehouse, SelectedTask, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the PutAwayPieces screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 6);
            return lightSwitchApplication.showScreen("PutAwayPieces", parameters, options);
        }),

        showRecoverFlight: $defineShowScreen(function showRecoverFlight(SelectedFlight, CurrentUser, CurrentUserStation, CurrentUserWarehouse, SelectedTask, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the RecoverFlight screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 6);
            return lightSwitchApplication.showScreen("RecoverFlight", parameters, options);
        }),

        showScanPutAwayPieces: $defineShowScreen(function showScanPutAwayPieces(SelectedFlt, SelectedTask, CurrentUser, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the ScanPutAwayPieces screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 4);
            return lightSwitchApplication.showScreen("ScanPutAwayPieces", parameters, options);
        }),

        showSelectCarrier: $defineShowScreen(function showSelectCarrier(SelectedDriver, options) {
            /// <summary>
            /// Asynchronously navigates forward to the SelectCarrier screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("SelectCarrier", parameters, options);
        }),

        showSelectDriver: $defineShowScreen(function showSelectDriver(SelectedDischarge, options) {
            /// <summary>
            /// Asynchronously navigates forward to the SelectDriver screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("SelectDriver", parameters, options);
        }),

        showSnapshot: $defineShowScreen(function showSnapshot(SelectedEntityType, SelectedTask, CurrentUser, CurrentUserWarehouse, SelectedEntityId, EntityDescription, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the Snapshot screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 7);
            return lightSwitchApplication.showScreen("Snapshot", parameters, options);
        }),

        showSnapshotImager: $defineShowScreen(function showSnapshotImager(options) {
            /// <summary>
            /// Asynchronously navigates forward to the SnapshotImager screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("SnapshotImager", parameters, options);
        }),

        showTaskManager: $defineShowScreen(function showTaskManager(options) {
            /// <summary>
            /// Asynchronously navigates forward to the TaskManager screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("TaskManager", parameters, options);
        }),

        showTrackAndTrace: $defineShowScreen(function showTrackAndTrace(options) {
            /// <summary>
            /// Asynchronously navigates forward to the TrackAndTrace screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 0);
            return lightSwitchApplication.showScreen("TrackAndTrace", parameters, options);
        }),

        showViewDischargePaymentsOld: $defineShowScreen(function showViewDischargePaymentsOld(SelectedDischarge, options) {
            /// <summary>
            /// Asynchronously navigates forward to the ViewDischargePaymentsOld screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 1);
            return lightSwitchApplication.showScreen("ViewDischargePaymentsOld", parameters, options);
        }),

        showViewFlightManifest: $defineShowScreen(function showViewFlightManifest(SelectedFlightUnloadPort, CurrentUser, CurrentUserStation, CurrentUserWarehouse, AjaxTimeout, options) {
            /// <summary>
            /// Asynchronously navigates forward to the ViewFlightManifest screen.
            /// </summary>
            /// <param name="options" optional="true">
            /// An object that provides one or more of the following options:<br/>- beforeShown: a function that is called after boundary behavior has been applied but before the screen is shown.<br/>+ Signature: beforeShown(screen)<br/>- afterClosed: a function that is called after boundary behavior has been applied and the screen has been closed.<br/>+ Signature: afterClosed(screen, action : msls.NavigateBackAction)
            /// </param>
            /// <returns type="WinJS.Promise" />
            var parameters = Array.prototype.slice.call(arguments, 0, 5);
            return lightSwitchApplication.showScreen("ViewFlightManifest", parameters, options);
        })

    });

}(msls.application));
