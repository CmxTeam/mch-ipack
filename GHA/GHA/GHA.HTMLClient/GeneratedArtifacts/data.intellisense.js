﻿/// <reference path="data.js" />

(function (lightSwitchApplication) {

    msls._addEntryPoints(lightSwitchApplication.Agent, {
        /// <field>
        /// Called when a new agent is created.
        /// <br/>created(msls.application.Agent entity)
        /// </field>
        created: [lightSwitchApplication.Agent]
    });

    msls._addEntryPoints(lightSwitchApplication.AWB, {
        /// <field>
        /// Called when a new aWB is created.
        /// <br/>created(msls.application.AWB entity)
        /// </field>
        created: [lightSwitchApplication.AWB]
    });

    msls._addEntryPoints(lightSwitchApplication.AWBs_HWB, {
        /// <field>
        /// Called when a new aWBs_HWB is created.
        /// <br/>created(msls.application.AWBs_HWB entity)
        /// </field>
        created: [lightSwitchApplication.AWBs_HWB]
    });

    msls._addEntryPoints(lightSwitchApplication.CarrierDriver, {
        /// <field>
        /// Called when a new carrierDriver is created.
        /// <br/>created(msls.application.CarrierDriver entity)
        /// </field>
        created: [lightSwitchApplication.CarrierDriver]
    });

    msls._addEntryPoints(lightSwitchApplication.CarrierRoute, {
        /// <field>
        /// Called when a new carrierRoute is created.
        /// <br/>created(msls.application.CarrierRoute entity)
        /// </field>
        created: [lightSwitchApplication.CarrierRoute]
    });

    msls._addEntryPoints(lightSwitchApplication.Carrier, {
        /// <field>
        /// Called when a new carrier is created.
        /// <br/>created(msls.application.Carrier entity)
        /// </field>
        created: [lightSwitchApplication.Carrier]
    });

    msls._addEntryPoints(lightSwitchApplication.ChargeDeclaration, {
        /// <field>
        /// Called when a new chargeDeclaration is created.
        /// <br/>created(msls.application.ChargeDeclaration entity)
        /// </field>
        created: [lightSwitchApplication.ChargeDeclaration]
    });

    msls._addEntryPoints(lightSwitchApplication.ChargeType, {
        /// <field>
        /// Called when a new chargeType is created.
        /// <br/>created(msls.application.ChargeType entity)
        /// </field>
        created: [lightSwitchApplication.ChargeType]
    });

    msls._addEntryPoints(lightSwitchApplication.Condition, {
        /// <field>
        /// Called when a new condition is created.
        /// <br/>created(msls.application.Condition entity)
        /// </field>
        created: [lightSwitchApplication.Condition]
    });

    msls._addEntryPoints(lightSwitchApplication.ConsignmentCode, {
        /// <field>
        /// Called when a new consignmentCode is created.
        /// <br/>created(msls.application.ConsignmentCode entity)
        /// </field>
        created: [lightSwitchApplication.ConsignmentCode]
    });

    msls._addEntryPoints(lightSwitchApplication.Contact, {
        /// <field>
        /// Called when a new contact is created.
        /// <br/>created(msls.application.Contact entity)
        /// </field>
        created: [lightSwitchApplication.Contact]
    });

    msls._addEntryPoints(lightSwitchApplication.Country, {
        /// <field>
        /// Called when a new country is created.
        /// <br/>created(msls.application.Country entity)
        /// </field>
        created: [lightSwitchApplication.Country]
    });

    msls._addEntryPoints(lightSwitchApplication.Currency, {
        /// <field>
        /// Called when a new currency is created.
        /// <br/>created(msls.application.Currency entity)
        /// </field>
        created: [lightSwitchApplication.Currency]
    });

    msls._addEntryPoints(lightSwitchApplication.Customer, {
        /// <field>
        /// Called when a new customer is created.
        /// <br/>created(msls.application.Customer entity)
        /// </field>
        created: [lightSwitchApplication.Customer]
    });

    msls._addEntryPoints(lightSwitchApplication.CustomsInfoIdentifier, {
        /// <field>
        /// Called when a new customsInfoIdentifier is created.
        /// <br/>created(msls.application.CustomsInfoIdentifier entity)
        /// </field>
        created: [lightSwitchApplication.CustomsInfoIdentifier]
    });

    msls._addEntryPoints(lightSwitchApplication.CustomsNotification, {
        /// <field>
        /// Called when a new customsNotification is created.
        /// <br/>created(msls.application.CustomsNotification entity)
        /// </field>
        created: [lightSwitchApplication.CustomsNotification]
    });

    msls._addEntryPoints(lightSwitchApplication.DensityGroup, {
        /// <field>
        /// Called when a new densityGroup is created.
        /// <br/>created(msls.application.DensityGroup entity)
        /// </field>
        created: [lightSwitchApplication.DensityGroup]
    });

    msls._addEntryPoints(lightSwitchApplication.DescriptionOfGood, {
        /// <field>
        /// Called when a new descriptionOfGood is created.
        /// <br/>created(msls.application.DescriptionOfGood entity)
        /// </field>
        created: [lightSwitchApplication.DescriptionOfGood]
    });

    msls._addEntryPoints(lightSwitchApplication.Dimension, {
        /// <field>
        /// Called when a new dimension is created.
        /// <br/>created(msls.application.Dimension entity)
        /// </field>
        created: [lightSwitchApplication.Dimension]
    });

    msls._addEntryPoints(lightSwitchApplication.DischargeCharge, {
        /// <field>
        /// Called when a new dischargeCharge is created.
        /// <br/>created(msls.application.DischargeCharge entity)
        /// </field>
        created: [lightSwitchApplication.DischargeCharge]
    });

    msls._addEntryPoints(lightSwitchApplication.DischargeCheckInList, {
        /// <field>
        /// Called when a new dischargeCheckInList is created.
        /// <br/>created(msls.application.DischargeCheckInList entity)
        /// </field>
        created: [lightSwitchApplication.DischargeCheckInList]
    });

    msls._addEntryPoints(lightSwitchApplication.DischargeDetail, {
        /// <field>
        /// Called when a new dischargeDetail is created.
        /// <br/>created(msls.application.DischargeDetail entity)
        /// </field>
        created: [lightSwitchApplication.DischargeDetail]
    });

    msls._addEntryPoints(lightSwitchApplication.DischargePayment, {
        /// <field>
        /// Called when a new dischargePayment is created.
        /// <br/>created(msls.application.DischargePayment entity)
        /// </field>
        created: [lightSwitchApplication.DischargePayment]
    });

    msls._addEntryPoints(lightSwitchApplication.Discharge, {
        /// <field>
        /// Called when a new discharge is created.
        /// <br/>created(msls.application.Discharge entity)
        /// </field>
        created: [lightSwitchApplication.Discharge]
    });

    msls._addEntryPoints(lightSwitchApplication.Disposition, {
        /// <field>
        /// Called when a new disposition is created.
        /// <br/>created(msls.application.Disposition entity)
        /// </field>
        created: [lightSwitchApplication.Disposition]
    });

    msls._addEntryPoints(lightSwitchApplication.Driver, {
        /// <field>
        /// Called when a new driver is created.
        /// <br/>created(msls.application.Driver entity)
        /// </field>
        created: [lightSwitchApplication.Driver]
    });

    msls._addEntryPoints(lightSwitchApplication.EntityType, {
        /// <field>
        /// Called when a new entityType is created.
        /// <br/>created(msls.application.EntityType entity)
        /// </field>
        created: [lightSwitchApplication.EntityType]
    });

    msls._addEntryPoints(lightSwitchApplication.FlightManifest, {
        /// <field>
        /// Called when a new flightManifest is created.
        /// <br/>created(msls.application.FlightManifest entity)
        /// </field>
        created: [lightSwitchApplication.FlightManifest]
    });

    msls._addEntryPoints(lightSwitchApplication.ForkliftDetail, {
        /// <field>
        /// Called when a new forkliftDetail is created.
        /// <br/>created(msls.application.ForkliftDetail entity)
        /// </field>
        created: [lightSwitchApplication.ForkliftDetail]
    });

    msls._addEntryPoints(lightSwitchApplication.ForkliftView, {
        /// <field>
        /// Called when a new forkliftView is created.
        /// <br/>created(msls.application.ForkliftView entity)
        /// </field>
        created: [lightSwitchApplication.ForkliftView]
    });

    msls._addEntryPoints(lightSwitchApplication.FSN, {
        /// <field>
        /// Called when a new fSN is created.
        /// <br/>created(msls.application.FSN entity)
        /// </field>
        created: [lightSwitchApplication.FSN]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_OutboundQueue, {
        /// <field>
        /// Called when a new hostPlus_OutboundQueue is created.
        /// <br/>created(msls.application.HostPlus_OutboundQueue entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_OutboundQueue]
    });

    msls._addEntryPoints(lightSwitchApplication.HWBHTSSet, {
        /// <field>
        /// Called when a new hWBHTSSet is created.
        /// <br/>created(msls.application.HWBHTSSet entity)
        /// </field>
        created: [lightSwitchApplication.HWBHTSSet]
    });

    msls._addEntryPoints(lightSwitchApplication.HWB, {
        /// <field>
        /// Called when a new hWB is created.
        /// <br/>created(msls.application.HWB entity)
        /// </field>
        created: [lightSwitchApplication.HWB]
    });

    msls._addEntryPoints(lightSwitchApplication.ImportExportCode, {
        /// <field>
        /// Called when a new importExportCode is created.
        /// <br/>created(msls.application.ImportExportCode entity)
        /// </field>
        created: [lightSwitchApplication.ImportExportCode]
    });

    msls._addEntryPoints(lightSwitchApplication.InventoryLocationItem, {
        /// <field>
        /// Called when a new inventoryLocationItem is created.
        /// <br/>created(msls.application.InventoryLocationItem entity)
        /// </field>
        created: [lightSwitchApplication.InventoryLocationItem]
    });

    msls._addEntryPoints(lightSwitchApplication.InventoryView, {
        /// <field>
        /// Called when a new inventoryView is created.
        /// <br/>created(msls.application.InventoryView entity)
        /// </field>
        created: [lightSwitchApplication.InventoryView]
    });

    msls._addEntryPoints(lightSwitchApplication.LegSegment, {
        /// <field>
        /// Called when a new legSegment is created.
        /// <br/>created(msls.application.LegSegment entity)
        /// </field>
        created: [lightSwitchApplication.LegSegment]
    });

    msls._addEntryPoints(lightSwitchApplication.LineIdentifier, {
        /// <field>
        /// Called when a new lineIdentifier is created.
        /// <br/>created(msls.application.LineIdentifier entity)
        /// </field>
        created: [lightSwitchApplication.LineIdentifier]
    });

    msls._addEntryPoints(lightSwitchApplication.LoadingIndicator, {
        /// <field>
        /// Called when a new loadingIndicator is created.
        /// <br/>created(msls.application.LoadingIndicator entity)
        /// </field>
        created: [lightSwitchApplication.LoadingIndicator]
    });

    msls._addEntryPoints(lightSwitchApplication.ManifestAWBDetail, {
        /// <field>
        /// Called when a new manifestAWBDetail is created.
        /// <br/>created(msls.application.ManifestAWBDetail entity)
        /// </field>
        created: [lightSwitchApplication.ManifestAWBDetail]
    });

    msls._addEntryPoints(lightSwitchApplication.ManifestDetail, {
        /// <field>
        /// Called when a new manifestDetail is created.
        /// <br/>created(msls.application.ManifestDetail entity)
        /// </field>
        created: [lightSwitchApplication.ManifestDetail]
    });

    msls._addEntryPoints(lightSwitchApplication.MOT, {
        /// <field>
        /// Called when a new mOT is created.
        /// <br/>created(msls.application.MOT entity)
        /// </field>
        created: [lightSwitchApplication.MOT]
    });

    msls._addEntryPoints(lightSwitchApplication.MovementPriorityCode, {
        /// <field>
        /// Called when a new movementPriorityCode is created.
        /// <br/>created(msls.application.MovementPriorityCode entity)
        /// </field>
        created: [lightSwitchApplication.MovementPriorityCode]
    });

    msls._addEntryPoints(lightSwitchApplication.OCI, {
        /// <field>
        /// Called when a new oCI is created.
        /// <br/>created(msls.application.OCI entity)
        /// </field>
        created: [lightSwitchApplication.OCI]
    });

    msls._addEntryPoints(lightSwitchApplication.ParticipantIdentifier, {
        /// <field>
        /// Called when a new participantIdentifier is created.
        /// <br/>created(msls.application.ParticipantIdentifier entity)
        /// </field>
        created: [lightSwitchApplication.ParticipantIdentifier]
    });

    msls._addEntryPoints(lightSwitchApplication.PaymentType, {
        /// <field>
        /// Called when a new paymentType is created.
        /// <br/>created(msls.application.PaymentType entity)
        /// </field>
        created: [lightSwitchApplication.PaymentType]
    });

    msls._addEntryPoints(lightSwitchApplication.Port, {
        /// <field>
        /// Called when a new port is created.
        /// <br/>created(msls.application.Port entity)
        /// </field>
        created: [lightSwitchApplication.Port]
    });

    msls._addEntryPoints(lightSwitchApplication.PutAwayList, {
        /// <field>
        /// Called when a new putAwayList is created.
        /// <br/>created(msls.application.PutAwayList entity)
        /// </field>
        created: [lightSwitchApplication.PutAwayList]
    });

    msls._addEntryPoints(lightSwitchApplication.PutedAwayItem, {
        /// <field>
        /// Called when a new putedAwayItem is created.
        /// <br/>created(msls.application.PutedAwayItem entity)
        /// </field>
        created: [lightSwitchApplication.PutedAwayItem]
    });

    msls._addEntryPoints(lightSwitchApplication.ReceiverAwbManifestList, {
        /// <field>
        /// Called when a new receiverAwbManifestList is created.
        /// <br/>created(msls.application.ReceiverAwbManifestList entity)
        /// </field>
        created: [lightSwitchApplication.ReceiverAwbManifestList]
    });

    msls._addEntryPoints(lightSwitchApplication.ReceiverFlightView, {
        /// <field>
        /// Called when a new receiverFlightView is created.
        /// <br/>created(msls.application.ReceiverFlightView entity)
        /// </field>
        created: [lightSwitchApplication.ReceiverFlightView]
    });

    msls._addEntryPoints(lightSwitchApplication.ReceiverUldManifestList, {
        /// <field>
        /// Called when a new receiverUldManifestList is created.
        /// <br/>created(msls.application.ReceiverUldManifestList entity)
        /// </field>
        created: [lightSwitchApplication.ReceiverUldManifestList]
    });

    msls._addEntryPoints(lightSwitchApplication.ShipmentUnitType, {
        /// <field>
        /// Called when a new shipmentUnitType is created.
        /// <br/>created(msls.application.ShipmentUnitType entity)
        /// </field>
        created: [lightSwitchApplication.ShipmentUnitType]
    });

    msls._addEntryPoints(lightSwitchApplication.SnapshotTasksView, {
        /// <field>
        /// Called when a new snapshotTasksView is created.
        /// <br/>created(msls.application.SnapshotTasksView entity)
        /// </field>
        created: [lightSwitchApplication.SnapshotTasksView]
    });

    msls._addEntryPoints(lightSwitchApplication.SpecialHandlingCode, {
        /// <field>
        /// Called when a new specialHandlingCode is created.
        /// <br/>created(msls.application.SpecialHandlingCode entity)
        /// </field>
        created: [lightSwitchApplication.SpecialHandlingCode]
    });

    msls._addEntryPoints(lightSwitchApplication.SpecialHandling, {
        /// <field>
        /// Called when a new specialHandling is created.
        /// <br/>created(msls.application.SpecialHandling entity)
        /// </field>
        created: [lightSwitchApplication.SpecialHandling]
    });

    msls._addEntryPoints(lightSwitchApplication.State, {
        /// <field>
        /// Called when a new state is created.
        /// <br/>created(msls.application.State entity)
        /// </field>
        created: [lightSwitchApplication.State]
    });

    msls._addEntryPoints(lightSwitchApplication.Status, {
        /// <field>
        /// Called when a new status is created.
        /// <br/>created(msls.application.Status entity)
        /// </field>
        created: [lightSwitchApplication.Status]
    });

    msls._addEntryPoints(lightSwitchApplication.TableList, {
        /// <field>
        /// Called when a new tableList is created.
        /// <br/>created(msls.application.TableList entity)
        /// </field>
        created: [lightSwitchApplication.TableList]
    });

    msls._addEntryPoints(lightSwitchApplication.TaskFlightReceiver, {
        /// <field>
        /// Called when a new taskFlightReceiver is created.
        /// <br/>created(msls.application.TaskFlightReceiver entity)
        /// </field>
        created: [lightSwitchApplication.TaskFlightReceiver]
    });

    msls._addEntryPoints(lightSwitchApplication.Task, {
        /// <field>
        /// Called when a new task is created.
        /// <br/>created(msls.application.Task entity)
        /// </field>
        created: [lightSwitchApplication.Task]
    });

    msls._addEntryPoints(lightSwitchApplication.TaskType, {
        /// <field>
        /// Called when a new taskType is created.
        /// <br/>created(msls.application.TaskType entity)
        /// </field>
        created: [lightSwitchApplication.TaskType]
    });

    msls._addEntryPoints(lightSwitchApplication.TrackTraceView, {
        /// <field>
        /// Called when a new trackTraceView is created.
        /// <br/>created(msls.application.TrackTraceView entity)
        /// </field>
        created: [lightSwitchApplication.TrackTraceView]
    });

    msls._addEntryPoints(lightSwitchApplication.TransactionAction, {
        /// <field>
        /// Called when a new transactionAction is created.
        /// <br/>created(msls.application.TransactionAction entity)
        /// </field>
        created: [lightSwitchApplication.TransactionAction]
    });

    msls._addEntryPoints(lightSwitchApplication.Transaction, {
        /// <field>
        /// Called when a new transaction is created.
        /// <br/>created(msls.application.Transaction entity)
        /// </field>
        created: [lightSwitchApplication.Transaction]
    });

    msls._addEntryPoints(lightSwitchApplication.ULD, {
        /// <field>
        /// Called when a new uLD is created.
        /// <br/>created(msls.application.ULD entity)
        /// </field>
        created: [lightSwitchApplication.ULD]
    });

    msls._addEntryPoints(lightSwitchApplication.ULDVolumeCode, {
        /// <field>
        /// Called when a new uLDVolumeCode is created.
        /// <br/>created(msls.application.ULDVolumeCode entity)
        /// </field>
        created: [lightSwitchApplication.ULDVolumeCode]
    });

    msls._addEntryPoints(lightSwitchApplication.UnloadingPort, {
        /// <field>
        /// Called when a new unloadingPort is created.
        /// <br/>created(msls.application.UnloadingPort entity)
        /// </field>
        created: [lightSwitchApplication.UnloadingPort]
    });

    msls._addEntryPoints(lightSwitchApplication.UOM, {
        /// <field>
        /// Called when a new uOM is created.
        /// <br/>created(msls.application.UOM entity)
        /// </field>
        created: [lightSwitchApplication.UOM]
    });

    msls._addEntryPoints(lightSwitchApplication.UOMSystem, {
        /// <field>
        /// Called when a new uOMSystem is created.
        /// <br/>created(msls.application.UOMSystem entity)
        /// </field>
        created: [lightSwitchApplication.UOMSystem]
    });

    msls._addEntryPoints(lightSwitchApplication.UOMType, {
        /// <field>
        /// Called when a new uOMType is created.
        /// <br/>created(msls.application.UOMType entity)
        /// </field>
        created: [lightSwitchApplication.UOMType]
    });

    msls._addEntryPoints(lightSwitchApplication.UserProfile, {
        /// <field>
        /// Called when a new userProfile is created.
        /// <br/>created(msls.application.UserProfile entity)
        /// </field>
        created: [lightSwitchApplication.UserProfile]
    });

    msls._addEntryPoints(lightSwitchApplication.UserStation, {
        /// <field>
        /// Called when a new userStation is created.
        /// <br/>created(msls.application.UserStation entity)
        /// </field>
        created: [lightSwitchApplication.UserStation]
    });

    msls._addEntryPoints(lightSwitchApplication.UserWarehous, {
        /// <field>
        /// Called when a new userWarehous is created.
        /// <br/>created(msls.application.UserWarehous entity)
        /// </field>
        created: [lightSwitchApplication.UserWarehous]
    });

    msls._addEntryPoints(lightSwitchApplication.WarehouseLocation, {
        /// <field>
        /// Called when a new warehouseLocation is created.
        /// <br/>created(msls.application.WarehouseLocation entity)
        /// </field>
        created: [lightSwitchApplication.WarehouseLocation]
    });

    msls._addEntryPoints(lightSwitchApplication.WarehouseLocationType, {
        /// <field>
        /// Called when a new warehouseLocationType is created.
        /// <br/>created(msls.application.WarehouseLocationType entity)
        /// </field>
        created: [lightSwitchApplication.WarehouseLocationType]
    });

    msls._addEntryPoints(lightSwitchApplication.Warehouse, {
        /// <field>
        /// Called when a new warehouse is created.
        /// <br/>created(msls.application.Warehouse entity)
        /// </field>
        created: [lightSwitchApplication.Warehouse]
    });

    msls._addEntryPoints(lightSwitchApplication.WarehouseZone, {
        /// <field>
        /// Called when a new warehouseZone is created.
        /// <br/>created(msls.application.WarehouseZone entity)
        /// </field>
        created: [lightSwitchApplication.WarehouseZone]
    });

    msls._addEntryPoints(lightSwitchApplication.Condition1, {
        /// <field>
        /// Called when a new condition1 is created.
        /// <br/>created(msls.application.Condition1 entity)
        /// </field>
        created: [lightSwitchApplication.Condition1]
    });

    msls._addEntryPoints(lightSwitchApplication.EntityCondition1, {
        /// <field>
        /// Called when a new entityCondition1 is created.
        /// <br/>created(msls.application.EntityCondition1 entity)
        /// </field>
        created: [lightSwitchApplication.EntityCondition1]
    });

    msls._addEntryPoints(lightSwitchApplication.EntitySnapshot1, {
        /// <field>
        /// Called when a new entitySnapshot1 is created.
        /// <br/>created(msls.application.EntitySnapshot1 entity)
        /// </field>
        created: [lightSwitchApplication.EntitySnapshot1]
    });

    msls._addEntryPoints(lightSwitchApplication.EntityType1, {
        /// <field>
        /// Called when a new entityType1 is created.
        /// <br/>created(msls.application.EntityType1 entity)
        /// </field>
        created: [lightSwitchApplication.EntityType1]
    });

    msls._addEntryPoints(lightSwitchApplication.Snapshot1, {
        /// <field>
        /// Called when a new snapshot1 is created.
        /// <br/>created(msls.application.Snapshot1 entity)
        /// </field>
        created: [lightSwitchApplication.Snapshot1]
    });

    msls._addEntryPoints(lightSwitchApplication.CIMPMessageLog, {
        /// <field>
        /// Called when a new cIMPMessageLog is created.
        /// <br/>created(msls.application.CIMPMessageLog entity)
        /// </field>
        created: [lightSwitchApplication.CIMPMessageLog]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_AuditTrail, {
        /// <field>
        /// Called when a new hostPlus_AuditTrail is created.
        /// <br/>created(msls.application.HostPlus_AuditTrail entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_AuditTrail]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_Email, {
        /// <field>
        /// Called when a new hostPlus_Email is created.
        /// <br/>created(msls.application.HostPlus_Email entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_Email]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_Location, {
        /// <field>
        /// Called when a new hostPlus_Location is created.
        /// <br/>created(msls.application.HostPlus_Location entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_Location]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_Log, {
        /// <field>
        /// Called when a new hostPlus_Log is created.
        /// <br/>created(msls.application.HostPlus_Log entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_Log]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_Parameter, {
        /// <field>
        /// Called when a new hostPlus_Parameter is created.
        /// <br/>created(msls.application.HostPlus_Parameter entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_Parameter]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_SessionParameter, {
        /// <field>
        /// Called when a new hostPlus_SessionParameter is created.
        /// <br/>created(msls.application.HostPlus_SessionParameter entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_SessionParameter]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_Session, {
        /// <field>
        /// Called when a new hostPlus_Session is created.
        /// <br/>created(msls.application.HostPlus_Session entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_Session]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_SessionTaskParameter, {
        /// <field>
        /// Called when a new hostPlus_SessionTaskParameter is created.
        /// <br/>created(msls.application.HostPlus_SessionTaskParameter entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_SessionTaskParameter]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_SessionTask, {
        /// <field>
        /// Called when a new hostPlus_SessionTask is created.
        /// <br/>created(msls.application.HostPlus_SessionTask entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_SessionTask]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_SessionType, {
        /// <field>
        /// Called when a new hostPlus_SessionType is created.
        /// <br/>created(msls.application.HostPlus_SessionType entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_SessionType]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_Task, {
        /// <field>
        /// Called when a new hostPlus_Task is created.
        /// <br/>created(msls.application.HostPlus_Task entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_Task]
    });

    msls._addEntryPoints(lightSwitchApplication.HostPlus_TimestampsCode, {
        /// <field>
        /// Called when a new hostPlus_TimestampsCode is created.
        /// <br/>created(msls.application.HostPlus_TimestampsCode entity)
        /// </field>
        created: [lightSwitchApplication.HostPlus_TimestampsCode]
    });

}(msls.application));
