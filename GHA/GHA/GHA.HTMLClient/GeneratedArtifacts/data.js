﻿/// <reference path="../Scripts/msls.js" />

window.myapp = msls.application;

(function (lightSwitchApplication) {

    var $Entity = msls.Entity,
        $DataService = msls.DataService,
        $DataWorkspace = msls.DataWorkspace,
        $defineEntity = msls._defineEntity,
        $defineDataService = msls._defineDataService,
        $defineDataWorkspace = msls._defineDataWorkspace,
        $DataServiceQuery = msls.DataServiceQuery,
        $toODataString = msls._toODataString;

    function Agent(entitySet) {
        /// <summary>
        /// Represents the Agent entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this agent.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this agent.
        /// </field>
        /// <field name="AccountNumber" type="String">
        /// Gets or sets the accountNumber for this agent.
        /// </field>
        /// <field name="NumericCode" type="String">
        /// Gets or sets the numericCode for this agent.
        /// </field>
        /// <field name="CASSAddress" type="String">
        /// Gets or sets the cASSAddress for this agent.
        /// </field>
        /// <field name="AWBs" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this agent.
        /// </field>
        /// <field name="details" type="msls.application.Agent.Details">
        /// Gets the details for this agent.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function AWB(entitySet) {
        /// <summary>
        /// Represents the AWB entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this aWB.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this aWB.
        /// </field>
        /// <field name="AWBSerialNumber" type="String">
        /// Gets or sets the aWBSerialNumber for this aWB.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this aWB.
        /// </field>
        /// <field name="TotalWeight" type="Number">
        /// Gets or sets the totalWeight for this aWB.
        /// </field>
        /// <field name="TotalVolume" type="Number">
        /// Gets or sets the totalVolume for this aWB.
        /// </field>
        /// <field name="DescriptionOfGoods" type="String">
        /// Gets or sets the descriptionOfGoods for this aWB.
        /// </field>
        /// <field name="HBCount" type="Number">
        /// Gets or sets the hBCount for this aWB.
        /// </field>
        /// <field name="LoadType" type="String">
        /// Gets or sets the loadType for this aWB.
        /// </field>
        /// <field name="ULDCount" type="Number">
        /// Gets or sets the uLDCount for this aWB.
        /// </field>
        /// <field name="ReceivedPieces" type="Number">
        /// Gets or sets the receivedPieces for this aWB.
        /// </field>
        /// <field name="PutAwayPieces" type="Number">
        /// Gets or sets the putAwayPieces for this aWB.
        /// </field>
        /// <field name="PercentReceived" type="Number">
        /// Gets or sets the percentReceived for this aWB.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this aWB.
        /// </field>
        /// <field name="ForkliftPieces" type="Number">
        /// Gets or sets the forkliftPieces for this aWB.
        /// </field>
        /// <field name="AvailablePieces" type="Number">
        /// Gets or sets the availablePieces for this aWB.
        /// </field>
        /// <field name="ReleasedPieces" type="Number">
        /// Gets or sets the releasedPieces for this aWB.
        /// </field>
        /// <field name="OtherServiceInfo1" type="String">
        /// Gets or sets the otherServiceInfo1 for this aWB.
        /// </field>
        /// <field name="OtherServiceInfo2" type="String">
        /// Gets or sets the otherServiceInfo2 for this aWB.
        /// </field>
        /// <field name="CustomsOrigin" type="String">
        /// Gets or sets the customsOrigin for this aWB.
        /// </field>
        /// <field name="CustomsReference" type="String">
        /// Gets or sets the customsReference for this aWB.
        /// </field>
        /// <field name="DensityIndicator" type="Boolean">
        /// Gets or sets the densityIndicator for this aWB.
        /// </field>
        /// <field name="SpecialServiceRequest1" type="String">
        /// Gets or sets the specialServiceRequest1 for this aWB.
        /// </field>
        /// <field name="SpecialServiceRequest2" type="String">
        /// Gets or sets the specialServiceRequest2 for this aWB.
        /// </field>
        /// <field name="SpecialServiceRequest3" type="String">
        /// Gets or sets the specialServiceRequest3 for this aWB.
        /// </field>
        /// <field name="IsTransfer" type="Number">
        /// Gets or sets the isTransfer for this aWB.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this aWB.
        /// </field>
        /// <field name="StatusTimestamp" type="Date">
        /// Gets or sets the statusTimestamp for this aWB.
        /// </field>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this aWB.
        /// </field>
        /// <field name="Shipper" type="msls.application.Customer">
        /// Gets or sets the shipper for this aWB.
        /// </field>
        /// <field name="Consignee" type="msls.application.Customer">
        /// Gets or sets the consignee for this aWB.
        /// </field>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this aWB.
        /// </field>
        /// <field name="DischargeDetails" type="msls.EntityCollection" elementType="msls.application.DischargeDetail">
        /// Gets the dischargeDetails for this aWB.
        /// </field>
        /// <field name="ForkliftDetails" type="msls.EntityCollection" elementType="msls.application.ForkliftDetail">
        /// Gets the forkliftDetails for this aWB.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this aWB.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this aWB.
        /// </field>
        /// <field name="Dispositions" type="msls.EntityCollection" elementType="msls.application.Disposition">
        /// Gets the dispositions for this aWB.
        /// </field>
        /// <field name="CustomsNotifications" type="msls.EntityCollection" elementType="msls.application.CustomsNotification">
        /// Gets the customsNotifications for this aWB.
        /// </field>
        /// <field name="Agent" type="msls.application.Agent">
        /// Gets or sets the agent for this aWB.
        /// </field>
        /// <field name="MovementPriorityCode" type="msls.application.MovementPriorityCode">
        /// Gets or sets the movementPriorityCode for this aWB.
        /// </field>
        /// <field name="AwbOrigin" type="msls.application.Port">
        /// Gets or sets the awbOrigin for this aWB.
        /// </field>
        /// <field name="AwbDestination" type="msls.application.Port">
        /// Gets or sets the awbDestination for this aWB.
        /// </field>
        /// <field name="WgtUom" type="msls.application.UOM">
        /// Gets or sets the wgtUom for this aWB.
        /// </field>
        /// <field name="VolUom" type="msls.application.UOM">
        /// Gets or sets the volUom for this aWB.
        /// </field>
        /// <field name="AWBs_HWBs" type="msls.EntityCollection" elementType="msls.application.AWBs_HWB">
        /// Gets the aWBs_HWBs for this aWB.
        /// </field>
        /// <field name="ChargeDeclarations" type="msls.EntityCollection" elementType="msls.application.ChargeDeclaration">
        /// Gets the chargeDeclarations for this aWB.
        /// </field>
        /// <field name="Dimensions" type="msls.EntityCollection" elementType="msls.application.Dimension">
        /// Gets the dimensions for this aWB.
        /// </field>
        /// <field name="OCIs" type="msls.EntityCollection" elementType="msls.application.OCI">
        /// Gets the oCIs for this aWB.
        /// </field>
        /// <field name="SpecialHandlings" type="msls.EntityCollection" elementType="msls.application.SpecialHandling">
        /// Gets the specialHandlings for this aWB.
        /// </field>
        /// <field name="HostPlus_OutboundQueues" type="msls.EntityCollection" elementType="msls.application.HostPlus_OutboundQueue">
        /// Gets the hostPlus_OutboundQueues for this aWB.
        /// </field>
        /// <field name="details" type="msls.application.AWB.Details">
        /// Gets the details for this aWB.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function AWBs_HWB(entitySet) {
        /// <summary>
        /// Represents the AWBs_HWB entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this aWBs_HWB.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this aWBs_HWB.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this aWBs_HWB.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this aWBs_HWB.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this aWBs_HWB.
        /// </field>
        /// <field name="details" type="msls.application.AWBs_HWB.Details">
        /// Gets the details for this aWBs_HWB.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function CarrierDriver(entitySet) {
        /// <summary>
        /// Represents the CarrierDriver entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this carrierDriver.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this carrierDriver.
        /// </field>
        /// <field name="FirstName" type="String">
        /// Gets or sets the firstName for this carrierDriver.
        /// </field>
        /// <field name="LastName" type="String">
        /// Gets or sets the lastName for this carrierDriver.
        /// </field>
        /// <field name="DriverPhotoPath" type="String">
        /// Gets or sets the driverPhotoPath for this carrierDriver.
        /// </field>
        /// <field name="LicenseNumber" type="String">
        /// Gets or sets the licenseNumber for this carrierDriver.
        /// </field>
        /// <field name="LicenseImagePath" type="String">
        /// Gets or sets the licenseImagePath for this carrierDriver.
        /// </field>
        /// <field name="LicenseExpiry" type="Date">
        /// Gets or sets the licenseExpiry for this carrierDriver.
        /// </field>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this carrierDriver.
        /// </field>
        /// <field name="Discharges" type="msls.EntityCollection" elementType="msls.application.Discharge">
        /// Gets the discharges for this carrierDriver.
        /// </field>
        /// <field name="details" type="msls.application.CarrierDriver.Details">
        /// Gets the details for this carrierDriver.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function CarrierRoute(entitySet) {
        /// <summary>
        /// Represents the CarrierRoute entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this carrierRoute.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this carrierRoute.
        /// </field>
        /// <field name="FlightNumber" type="String">
        /// Gets or sets the flightNumber for this carrierRoute.
        /// </field>
        /// <field name="DepartureTime" type="Object">
        /// Gets or sets the departureTime for this carrierRoute.
        /// </field>
        /// <field name="ArrivalTime" type="Object">
        /// Gets or sets the arrivalTime for this carrierRoute.
        /// </field>
        /// <field name="Offset" type="Number">
        /// Gets or sets the offset for this carrierRoute.
        /// </field>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this carrierRoute.
        /// </field>
        /// <field name="Port" type="msls.application.Port">
        /// Gets or sets the port for this carrierRoute.
        /// </field>
        /// <field name="Port1" type="msls.application.Port">
        /// Gets or sets the port1 for this carrierRoute.
        /// </field>
        /// <field name="Port2" type="msls.application.Port">
        /// Gets or sets the port2 for this carrierRoute.
        /// </field>
        /// <field name="details" type="msls.application.CarrierRoute.Details">
        /// Gets the details for this carrierRoute.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Carrier(entitySet) {
        /// <summary>
        /// Represents the Carrier entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this carrier.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this carrier.
        /// </field>
        /// <field name="CarrierName" type="String">
        /// Gets or sets the carrierName for this carrier.
        /// </field>
        /// <field name="CarrierCode" type="String">
        /// Gets or sets the carrierCode for this carrier.
        /// </field>
        /// <field name="Carrier3Code" type="String">
        /// Gets or sets the carrier3Code for this carrier.
        /// </field>
        /// <field name="AWBs" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this carrier.
        /// </field>
        /// <field name="Drivers" type="msls.EntityCollection" elementType="msls.application.Driver">
        /// Gets the drivers for this carrier.
        /// </field>
        /// <field name="FlightManifests" type="msls.EntityCollection" elementType="msls.application.FlightManifest">
        /// Gets the flightManifests for this carrier.
        /// </field>
        /// <field name="ULDs" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs for this carrier.
        /// </field>
        /// <field name="MOT" type="msls.application.MOT">
        /// Gets or sets the mOT for this carrier.
        /// </field>
        /// <field name="LegSegments" type="msls.EntityCollection" elementType="msls.application.LegSegment">
        /// Gets the legSegments for this carrier.
        /// </field>
        /// <field name="CarrierRoutes" type="msls.EntityCollection" elementType="msls.application.CarrierRoute">
        /// Gets the carrierRoutes for this carrier.
        /// </field>
        /// <field name="CarrierLogoPath" type="String">
        /// Gets or sets the carrierLogoPath for this carrier.
        /// </field>
        /// <field name="CarrierDrivers" type="msls.EntityCollection" elementType="msls.application.CarrierDriver">
        /// Gets the carrierDrivers for this carrier.
        /// </field>
        /// <field name="details" type="msls.application.Carrier.Details">
        /// Gets the details for this carrier.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ChargeDeclaration(entitySet) {
        /// <summary>
        /// Represents the ChargeDeclaration entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this chargeDeclaration.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this chargeDeclaration.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this chargeDeclaration.
        /// </field>
        /// <field name="IsWeightCollect" type="Boolean">
        /// Gets or sets the isWeightCollect for this chargeDeclaration.
        /// </field>
        /// <field name="IsOtherChargesCollect" type="Boolean">
        /// Gets or sets the isOtherChargesCollect for this chargeDeclaration.
        /// </field>
        /// <field name="CarriageDeclaredValue" type="Number">
        /// Gets or sets the carriageDeclaredValue for this chargeDeclaration.
        /// </field>
        /// <field name="CustomsDeclaredValue" type="Number">
        /// Gets or sets the customsDeclaredValue for this chargeDeclaration.
        /// </field>
        /// <field name="InsuranceDeclaredValue" type="Number">
        /// Gets or sets the insuranceDeclaredValue for this chargeDeclaration.
        /// </field>
        /// <field name="IsNoCarriageValue" type="Boolean">
        /// Gets or sets the isNoCarriageValue for this chargeDeclaration.
        /// </field>
        /// <field name="IsNoCustomsValue" type="Boolean">
        /// Gets or sets the isNoCustomsValue for this chargeDeclaration.
        /// </field>
        /// <field name="IsNoInsuranceValue" type="Boolean">
        /// Gets or sets the isNoInsuranceValue for this chargeDeclaration.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this chargeDeclaration.
        /// </field>
        /// <field name="Currency" type="msls.application.Currency">
        /// Gets or sets the currency for this chargeDeclaration.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this chargeDeclaration.
        /// </field>
        /// <field name="details" type="msls.application.ChargeDeclaration.Details">
        /// Gets the details for this chargeDeclaration.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ChargeType(entitySet) {
        /// <summary>
        /// Represents the ChargeType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this chargeType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this chargeType.
        /// </field>
        /// <field name="ChargeType1" type="String">
        /// Gets or sets the chargeType1 for this chargeType.
        /// </field>
        /// <field name="DefaultChargeAmount" type="String">
        /// Gets or sets the defaultChargeAmount for this chargeType.
        /// </field>
        /// <field name="IsClockStartAtArrival" type="Boolean">
        /// Gets or sets the isClockStartAtArrival for this chargeType.
        /// </field>
        /// <field name="FreeTimeDuration" type="Number">
        /// Gets or sets the freeTimeDuration for this chargeType.
        /// </field>
        /// <field name="ClockStartTime" type="Number">
        /// Gets or sets the clockStartTime for this chargeType.
        /// </field>
        /// <field name="IsPerUnit" type="Boolean">
        /// Gets or sets the isPerUnit for this chargeType.
        /// </field>
        /// <field name="DischargeCharges" type="msls.EntityCollection" elementType="msls.application.DischargeCharge">
        /// Gets the dischargeCharges for this chargeType.
        /// </field>
        /// <field name="MinChargeAmount" type="String">
        /// Gets or sets the minChargeAmount for this chargeType.
        /// </field>
        /// <field name="details" type="msls.application.ChargeType.Details">
        /// Gets the details for this chargeType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Condition(entitySet) {
        /// <summary>
        /// Represents the Condition entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this condition.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this condition.
        /// </field>
        /// <field name="Condition1" type="String">
        /// Gets or sets the condition1 for this condition.
        /// </field>
        /// <field name="isDamage" type="Boolean">
        /// Gets or sets the isDamage for this condition.
        /// </field>
        /// <field name="ImagePath" type="String">
        /// Gets or sets the imagePath for this condition.
        /// </field>
        /// <field name="details" type="msls.application.Condition.Details">
        /// Gets the details for this condition.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ConsignmentCode(entitySet) {
        /// <summary>
        /// Represents the ConsignmentCode entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this consignmentCode.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this consignmentCode.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this consignmentCode.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this consignmentCode.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this consignmentCode.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this consignmentCode.
        /// </field>
        /// <field name="details" type="msls.application.ConsignmentCode.Details">
        /// Gets the details for this consignmentCode.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Contact(entitySet) {
        /// <summary>
        /// Represents the Contact entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this contact.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this contact.
        /// </field>
        /// <field name="FullName" type="String">
        /// Gets or sets the fullName for this contact.
        /// </field>
        /// <field name="FirstName" type="String">
        /// Gets or sets the firstName for this contact.
        /// </field>
        /// <field name="MiddleName" type="String">
        /// Gets or sets the middleName for this contact.
        /// </field>
        /// <field name="LastName" type="String">
        /// Gets or sets the lastName for this contact.
        /// </field>
        /// <field name="JobTitle" type="String">
        /// Gets or sets the jobTitle for this contact.
        /// </field>
        /// <field name="Phone" type="String">
        /// Gets or sets the phone for this contact.
        /// </field>
        /// <field name="Mobile" type="String">
        /// Gets or sets the mobile for this contact.
        /// </field>
        /// <field name="Fax" type="String">
        /// Gets or sets the fax for this contact.
        /// </field>
        /// <field name="Email" type="String">
        /// Gets or sets the email for this contact.
        /// </field>
        /// <field name="Telex" type="String">
        /// Gets or sets the telex for this contact.
        /// </field>
        /// <field name="Photo" type="String">
        /// Gets or sets the photo for this contact.
        /// </field>
        /// <field name="Customer" type="msls.application.Customer">
        /// Gets or sets the customer for this contact.
        /// </field>
        /// <field name="details" type="msls.application.Contact.Details">
        /// Gets the details for this contact.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Country(entitySet) {
        /// <summary>
        /// Represents the Country entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this country.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this country.
        /// </field>
        /// <field name="Country1" type="String">
        /// Gets or sets the country1 for this country.
        /// </field>
        /// <field name="TwoCharacterCode" type="String">
        /// Gets or sets the twoCharacterCode for this country.
        /// </field>
        /// <field name="Ports" type="msls.EntityCollection" elementType="msls.application.Port">
        /// Gets the ports for this country.
        /// </field>
        /// <field name="States" type="msls.EntityCollection" elementType="msls.application.State">
        /// Gets the states for this country.
        /// </field>
        /// <field name="Warehouses" type="msls.EntityCollection" elementType="msls.application.Warehouse">
        /// Gets the warehouses for this country.
        /// </field>
        /// <field name="Customers" type="msls.EntityCollection" elementType="msls.application.Customer">
        /// Gets the customers for this country.
        /// </field>
        /// <field name="Currencies" type="msls.EntityCollection" elementType="msls.application.Currency">
        /// Gets the currencies for this country.
        /// </field>
        /// <field name="OCIs" type="msls.EntityCollection" elementType="msls.application.OCI">
        /// Gets the oCIs for this country.
        /// </field>
        /// <field name="details" type="msls.application.Country.Details">
        /// Gets the details for this country.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Currency(entitySet) {
        /// <summary>
        /// Represents the Currency entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this currency.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this currency.
        /// </field>
        /// <field name="Currency3Code" type="String">
        /// Gets or sets the currency3Code for this currency.
        /// </field>
        /// <field name="Currency1" type="String">
        /// Gets or sets the currency1 for this currency.
        /// </field>
        /// <field name="Country" type="msls.application.Country">
        /// Gets or sets the country for this currency.
        /// </field>
        /// <field name="ChargeDeclarations" type="msls.EntityCollection" elementType="msls.application.ChargeDeclaration">
        /// Gets the chargeDeclarations for this currency.
        /// </field>
        /// <field name="CurrencySign" type="String">
        /// Gets or sets the currencySign for this currency.
        /// </field>
        /// <field name="Discharges" type="msls.EntityCollection" elementType="msls.application.Discharge">
        /// Gets the discharges for this currency.
        /// </field>
        /// <field name="details" type="msls.application.Currency.Details">
        /// Gets the details for this currency.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Customer(entitySet) {
        /// <summary>
        /// Represents the Customer entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this customer.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this customer.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this customer.
        /// </field>
        /// <field name="AddressLine1" type="String">
        /// Gets or sets the addressLine1 for this customer.
        /// </field>
        /// <field name="AddressLine2" type="String">
        /// Gets or sets the addressLine2 for this customer.
        /// </field>
        /// <field name="City" type="String">
        /// Gets or sets the city for this customer.
        /// </field>
        /// <field name="PostalCode" type="String">
        /// Gets or sets the postalCode for this customer.
        /// </field>
        /// <field name="Country1" type="msls.application.Country">
        /// Gets or sets the country1 for this customer.
        /// </field>
        /// <field name="State" type="msls.application.State">
        /// Gets or sets the state for this customer.
        /// </field>
        /// <field name="AWBs" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this customer.
        /// </field>
        /// <field name="AWBs1" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs1 for this customer.
        /// </field>
        /// <field name="HWBs" type="msls.EntityCollection" elementType="msls.application.HWB">
        /// Gets the hWBs for this customer.
        /// </field>
        /// <field name="HWBs1" type="msls.EntityCollection" elementType="msls.application.HWB">
        /// Gets the hWBs1 for this customer.
        /// </field>
        /// <field name="Contacts" type="msls.EntityCollection" elementType="msls.application.Contact">
        /// Gets the contacts for this customer.
        /// </field>
        /// <field name="details" type="msls.application.Customer.Details">
        /// Gets the details for this customer.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function CustomsInfoIdentifier(entitySet) {
        /// <summary>
        /// Represents the CustomsInfoIdentifier entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this customsInfoIdentifier.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this customsInfoIdentifier.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this customsInfoIdentifier.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this customsInfoIdentifier.
        /// </field>
        /// <field name="OCIs" type="msls.EntityCollection" elementType="msls.application.OCI">
        /// Gets the oCIs for this customsInfoIdentifier.
        /// </field>
        /// <field name="details" type="msls.application.CustomsInfoIdentifier.Details">
        /// Gets the details for this customsInfoIdentifier.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function CustomsNotification(entitySet) {
        /// <summary>
        /// Represents the CustomsNotification entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this customsNotification.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this customsNotification.
        /// </field>
        /// <field name="CustomsCode" type="String">
        /// Gets or sets the customsCode for this customsNotification.
        /// </field>
        /// <field name="CodeDescription" type="String">
        /// Gets or sets the codeDescription for this customsNotification.
        /// </field>
        /// <field name="StatusTimestamp" type="Date">
        /// Gets or sets the statusTimestamp for this customsNotification.
        /// </field>
        /// <field name="Part" type="String">
        /// Gets or sets the part for this customsNotification.
        /// </field>
        /// <field name="FlightNumber" type="String">
        /// Gets or sets the flightNumber for this customsNotification.
        /// </field>
        /// <field name="Slac" type="Number">
        /// Gets or sets the slac for this customsNotification.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this customsNotification.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this customsNotification.
        /// </field>
        /// <field name="details" type="msls.application.CustomsNotification.Details">
        /// Gets the details for this customsNotification.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function DensityGroup(entitySet) {
        /// <summary>
        /// Represents the DensityGroup entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this densityGroup.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this densityGroup.
        /// </field>
        /// <field name="DensityGroupCode" type="Number">
        /// Gets or sets the densityGroupCode for this densityGroup.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this densityGroup.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this densityGroup.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this densityGroup.
        /// </field>
        /// <field name="details" type="msls.application.DensityGroup.Details">
        /// Gets the details for this densityGroup.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function DescriptionOfGood(entitySet) {
        /// <summary>
        /// Represents the DescriptionOfGood entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this descriptionOfGood.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this descriptionOfGood.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this descriptionOfGood.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this descriptionOfGood.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this descriptionOfGood.
        /// </field>
        /// <field name="details" type="msls.application.DescriptionOfGood.Details">
        /// Gets the details for this descriptionOfGood.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Dimension(entitySet) {
        /// <summary>
        /// Represents the Dimension entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this dimension.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this dimension.
        /// </field>
        /// <field name="Pieces" type="Number">
        /// Gets or sets the pieces for this dimension.
        /// </field>
        /// <field name="Weight" type="Number">
        /// Gets or sets the weight for this dimension.
        /// </field>
        /// <field name="Length" type="Number">
        /// Gets or sets the length for this dimension.
        /// </field>
        /// <field name="Width" type="Number">
        /// Gets or sets the width for this dimension.
        /// </field>
        /// <field name="Height" type="Number">
        /// Gets or sets the height for this dimension.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this dimension.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this dimension.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this dimension.
        /// </field>
        /// <field name="UOM" type="msls.application.UOM">
        /// Gets or sets the uOM for this dimension.
        /// </field>
        /// <field name="UOM1" type="msls.application.UOM">
        /// Gets or sets the uOM1 for this dimension.
        /// </field>
        /// <field name="details" type="msls.application.Dimension.Details">
        /// Gets the details for this dimension.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function DischargeCharge(entitySet) {
        /// <summary>
        /// Represents the DischargeCharge entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this dischargeCharge.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this dischargeCharge.
        /// </field>
        /// <field name="ChargeAmount" type="String">
        /// Gets or sets the chargeAmount for this dischargeCharge.
        /// </field>
        /// <field name="ChargePaid" type="String">
        /// Gets or sets the chargePaid for this dischargeCharge.
        /// </field>
        /// <field name="ChargeType" type="msls.application.ChargeType">
        /// Gets or sets the chargeType for this dischargeCharge.
        /// </field>
        /// <field name="DischargeDetail" type="msls.application.DischargeDetail">
        /// Gets or sets the dischargeDetail for this dischargeCharge.
        /// </field>
        /// <field name="PaymentType" type="msls.application.PaymentType">
        /// Gets or sets the paymentType for this dischargeCharge.
        /// </field>
        /// <field name="DischargePayments" type="msls.EntityCollection" elementType="msls.application.DischargePayment">
        /// Gets the dischargePayments for this dischargeCharge.
        /// </field>
        /// <field name="details" type="msls.application.DischargeCharge.Details">
        /// Gets the details for this dischargeCharge.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function DischargeCheckInList(entitySet) {
        /// <summary>
        /// Represents the DischargeCheckInList entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this dischargeCheckInList.
        /// </param>
        /// <field name="TaskId" type="String">
        /// Gets or sets the taskId for this dischargeCheckInList.
        /// </field>
        /// <field name="DischargeId" type="String">
        /// Gets or sets the dischargeId for this dischargeCheckInList.
        /// </field>
        /// <field name="CheckInTimestamp" type="Date">
        /// Gets or sets the checkInTimestamp for this dischargeCheckInList.
        /// </field>
        /// <field name="DriverName" type="String">
        /// Gets or sets the driverName for this dischargeCheckInList.
        /// </field>
        /// <field name="DriverImage" type="String">
        /// Gets or sets the driverImage for this dischargeCheckInList.
        /// </field>
        /// <field name="CarrierName" type="String">
        /// Gets or sets the carrierName for this dischargeCheckInList.
        /// </field>
        /// <field name="StatusImagePath" type="String">
        /// Gets or sets the statusImagePath for this dischargeCheckInList.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this dischargeCheckInList.
        /// </field>
        /// <field name="TotalDeliveryOrders" type="Number">
        /// Gets or sets the totalDeliveryOrders for this dischargeCheckInList.
        /// </field>
        /// <field name="BalanceDue" type="String">
        /// Gets or sets the balanceDue for this dischargeCheckInList.
        /// </field>
        /// <field name="WarehouseId" type="Number">
        /// Gets or sets the warehouseId for this dischargeCheckInList.
        /// </field>
        /// <field name="StatusId" type="Number">
        /// Gets or sets the statusId for this dischargeCheckInList.
        /// </field>
        /// <field name="details" type="msls.application.DischargeCheckInList.Details">
        /// Gets the details for this dischargeCheckInList.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function DischargeDetail(entitySet) {
        /// <summary>
        /// Represents the DischargeDetail entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this dischargeDetail.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this dischargeDetail.
        /// </field>
        /// <field name="TotalPiecesRequested" type="Number">
        /// Gets or sets the totalPiecesRequested for this dischargeDetail.
        /// </field>
        /// <field name="TotalSLACRequested" type="Number">
        /// Gets or sets the totalSLACRequested for this dischargeDetail.
        /// </field>
        /// <field name="TotalPiecesReleased" type="Number">
        /// Gets or sets the totalPiecesReleased for this dischargeDetail.
        /// </field>
        /// <field name="TotalSLACReleased" type="Number">
        /// Gets or sets the totalSLACReleased for this dischargeDetail.
        /// </field>
        /// <field name="DisplayText" type="String">
        /// Gets or sets the displayText for this dischargeDetail.
        /// </field>
        /// <field name="TotalAmountDue" type="String">
        /// Gets or sets the totalAmountDue for this dischargeDetail.
        /// </field>
        /// <field name="TotalAmountPaid" type="String">
        /// Gets or sets the totalAmountPaid for this dischargeDetail.
        /// </field>
        /// <field name="DischargeCharges" type="msls.EntityCollection" elementType="msls.application.DischargeCharge">
        /// Gets the dischargeCharges for this dischargeDetail.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this dischargeDetail.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this dischargeDetail.
        /// </field>
        /// <field name="Discharge" type="msls.application.Discharge">
        /// Gets or sets the discharge for this dischargeDetail.
        /// </field>
        /// <field name="DOFilePath" type="String">
        /// Gets or sets the dOFilePath for this dischargeDetail.
        /// </field>
        /// <field name="details" type="msls.application.DischargeDetail.Details">
        /// Gets the details for this dischargeDetail.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function DischargePayment(entitySet) {
        /// <summary>
        /// Represents the DischargePayment entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this dischargePayment.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this dischargePayment.
        /// </field>
        /// <field name="PaymentAmount" type="String">
        /// Gets or sets the paymentAmount for this dischargePayment.
        /// </field>
        /// <field name="Reference" type="String">
        /// Gets or sets the reference for this dischargePayment.
        /// </field>
        /// <field name="VoucherNumber" type="String">
        /// Gets or sets the voucherNumber for this dischargePayment.
        /// </field>
        /// <field name="CreditCardNumber" type="String">
        /// Gets or sets the creditCardNumber for this dischargePayment.
        /// </field>
        /// <field name="CreditCardExpiryMonth" type="String">
        /// Gets or sets the creditCardExpiryMonth for this dischargePayment.
        /// </field>
        /// <field name="CreditCardExpiryYear" type="String">
        /// Gets or sets the creditCardExpiryYear for this dischargePayment.
        /// </field>
        /// <field name="DischargeCharge" type="msls.application.DischargeCharge">
        /// Gets or sets the dischargeCharge for this dischargePayment.
        /// </field>
        /// <field name="PaymentType" type="msls.application.PaymentType">
        /// Gets or sets the paymentType for this dischargePayment.
        /// </field>
        /// <field name="Discharge" type="msls.application.Discharge">
        /// Gets or sets the discharge for this dischargePayment.
        /// </field>
        /// <field name="details" type="msls.application.DischargePayment.Details">
        /// Gets the details for this dischargePayment.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Discharge(entitySet) {
        /// <summary>
        /// Represents the Discharge entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this discharge.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this discharge.
        /// </field>
        /// <field name="CheckInTimestamp" type="Date">
        /// Gets or sets the checkInTimestamp for this discharge.
        /// </field>
        /// <field name="Agent" type="String">
        /// Gets or sets the agent for this discharge.
        /// </field>
        /// <field name="TotalPiecesRequested" type="Number">
        /// Gets or sets the totalPiecesRequested for this discharge.
        /// </field>
        /// <field name="TotalSlacRequested" type="Number">
        /// Gets or sets the totalSlacRequested for this discharge.
        /// </field>
        /// <field name="TotalDeliveryOrders" type="Number">
        /// Gets or sets the totalDeliveryOrders for this discharge.
        /// </field>
        /// <field name="TotalAmountDue" type="String">
        /// Gets or sets the totalAmountDue for this discharge.
        /// </field>
        /// <field name="TotalAmountPaid" type="String">
        /// Gets or sets the totalAmountPaid for this discharge.
        /// </field>
        /// <field name="StatusTimestamp" type="Date">
        /// Gets or sets the statusTimestamp for this discharge.
        /// </field>
        /// <field name="TotalPiecesReleased" type="Number">
        /// Gets or sets the totalPiecesReleased for this discharge.
        /// </field>
        /// <field name="TotalSlacReleased" type="Number">
        /// Gets or sets the totalSlacReleased for this discharge.
        /// </field>
        /// <field name="DischargeDetails" type="msls.EntityCollection" elementType="msls.application.DischargeDetail">
        /// Gets the dischargeDetails for this discharge.
        /// </field>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this discharge.
        /// </field>
        /// <field name="DischargePayments" type="msls.EntityCollection" elementType="msls.application.DischargePayment">
        /// Gets the dischargePayments for this discharge.
        /// </field>
        /// <field name="CarrierDriver" type="msls.application.CarrierDriver">
        /// Gets or sets the carrierDriver for this discharge.
        /// </field>
        /// <field name="Currency" type="msls.application.Currency">
        /// Gets or sets the currency for this discharge.
        /// </field>
        /// <field name="details" type="msls.application.Discharge.Details">
        /// Gets the details for this discharge.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Disposition(entitySet) {
        /// <summary>
        /// Represents the Disposition entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this disposition.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this disposition.
        /// </field>
        /// <field name="Count" type="Number">
        /// Gets or sets the count for this disposition.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this disposition.
        /// </field>
        /// <field name="Timestamp" type="Date">
        /// Gets or sets the timestamp for this disposition.
        /// </field>
        /// <field name="IsAdjust" type="Boolean">
        /// Gets or sets the isAdjust for this disposition.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this disposition.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this disposition.
        /// </field>
        /// <field name="Task" type="msls.application.Task">
        /// Gets or sets the task for this disposition.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this disposition.
        /// </field>
        /// <field name="WarehouseLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the warehouseLocation for this disposition.
        /// </field>
        /// <field name="details" type="msls.application.Disposition.Details">
        /// Gets the details for this disposition.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Driver(entitySet) {
        /// <summary>
        /// Represents the Driver entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this driver.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this driver.
        /// </field>
        /// <field name="FirstName" type="String">
        /// Gets or sets the firstName for this driver.
        /// </field>
        /// <field name="LastName" type="String">
        /// Gets or sets the lastName for this driver.
        /// </field>
        /// <field name="Photo" type="String">
        /// Gets or sets the photo for this driver.
        /// </field>
        /// <field name="LicenseNumber" type="String">
        /// Gets or sets the licenseNumber for this driver.
        /// </field>
        /// <field name="LicenseImage" type="String">
        /// Gets or sets the licenseImage for this driver.
        /// </field>
        /// <field name="LicenseExpiry" type="Date">
        /// Gets or sets the licenseExpiry for this driver.
        /// </field>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this driver.
        /// </field>
        /// <field name="details" type="msls.application.Driver.Details">
        /// Gets the details for this driver.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function EntityType(entitySet) {
        /// <summary>
        /// Represents the EntityType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this entityType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this entityType.
        /// </field>
        /// <field name="EntityName" type="String">
        /// Gets or sets the entityName for this entityType.
        /// </field>
        /// <field name="Transactions" type="msls.EntityCollection" elementType="msls.application.Transaction">
        /// Gets the transactions for this entityType.
        /// </field>
        /// <field name="Tasks" type="msls.EntityCollection" elementType="msls.application.Task">
        /// Gets the tasks for this entityType.
        /// </field>
        /// <field name="EntityTypeImagePath" type="String">
        /// Gets or sets the entityTypeImagePath for this entityType.
        /// </field>
        /// <field name="details" type="msls.application.EntityType.Details">
        /// Gets the details for this entityType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function FlightManifest(entitySet) {
        /// <summary>
        /// Represents the FlightManifest entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this flightManifest.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this flightManifest.
        /// </field>
        /// <field name="FlightNumber" type="String">
        /// Gets or sets the flightNumber for this flightManifest.
        /// </field>
        /// <field name="ETA" type="Date">
        /// Gets or sets the eTA for this flightManifest.
        /// </field>
        /// <field name="ETD" type="Date">
        /// Gets or sets the eTD for this flightManifest.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this flightManifest.
        /// </field>
        /// <field name="TotalULDs" type="Number">
        /// Gets or sets the totalULDs for this flightManifest.
        /// </field>
        /// <field name="TotalAWBs" type="Number">
        /// Gets or sets the totalAWBs for this flightManifest.
        /// </field>
        /// <field name="StatusTimestamp" type="Date">
        /// Gets or sets the statusTimestamp for this flightManifest.
        /// </field>
        /// <field name="ReceivedPieces" type="Number">
        /// Gets or sets the receivedPieces for this flightManifest.
        /// </field>
        /// <field name="RecoveredULDs" type="Number">
        /// Gets or sets the recoveredULDs for this flightManifest.
        /// </field>
        /// <field name="PutAwayPieces" type="Number">
        /// Gets or sets the putAwayPieces for this flightManifest.
        /// </field>
        /// <field name="PercentRecovered" type="Number">
        /// Gets or sets the percentRecovered for this flightManifest.
        /// </field>
        /// <field name="PercentReceived" type="Number">
        /// Gets or sets the percentReceived for this flightManifest.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this flightManifest.
        /// </field>
        /// <field name="IsOSD" type="Boolean">
        /// Gets or sets the isOSD for this flightManifest.
        /// </field>
        /// <field name="AircraftRegistration" type="String">
        /// Gets or sets the aircraftRegistration for this flightManifest.
        /// </field>
        /// <field name="ArrivalPortETA" type="Date">
        /// Gets or sets the arrivalPortETA for this flightManifest.
        /// </field>
        /// <field name="IsComplete" type="Boolean">
        /// Gets or sets the isComplete for this flightManifest.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this flightManifest.
        /// </field>
        /// <field name="RecoveredBy" type="String">
        /// Gets or sets the recoveredBy for this flightManifest.
        /// </field>
        /// <field name="RecoveredOn" type="Date">
        /// Gets or sets the recoveredOn for this flightManifest.
        /// </field>
        /// <field name="ULDs" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs for this flightManifest.
        /// </field>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this flightManifest.
        /// </field>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this flightManifest.
        /// </field>
        /// <field name="WarehouseLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the warehouseLocation for this flightManifest.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this flightManifest.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this flightManifest.
        /// </field>
        /// <field name="FltOrigin" type="msls.application.Port">
        /// Gets or sets the fltOrigin for this flightManifest.
        /// </field>
        /// <field name="FltDestination" type="msls.application.Port">
        /// Gets or sets the fltDestination for this flightManifest.
        /// </field>
        /// <field name="UnloadingPorts" type="msls.EntityCollection" elementType="msls.application.UnloadingPort">
        /// Gets the unloadingPorts for this flightManifest.
        /// </field>
        /// <field name="TaskFlightReceivers" type="msls.EntityCollection" elementType="msls.application.TaskFlightReceiver">
        /// Gets the taskFlightReceivers for this flightManifest.
        /// </field>
        /// <field name="PercentOverall" type="Number">
        /// Gets or sets the percentOverall for this flightManifest.
        /// </field>
        /// <field name="HostPlus_OutboundQueues" type="msls.EntityCollection" elementType="msls.application.HostPlus_OutboundQueue">
        /// Gets the hostPlus_OutboundQueues for this flightManifest.
        /// </field>
        /// <field name="details" type="msls.application.FlightManifest.Details">
        /// Gets the details for this flightManifest.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ForkliftDetail(entitySet) {
        /// <summary>
        /// Represents the ForkliftDetail entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this forkliftDetail.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this forkliftDetail.
        /// </field>
        /// <field name="Pieces" type="Number">
        /// Gets or sets the pieces for this forkliftDetail.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this forkliftDetail.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this forkliftDetail.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this forkliftDetail.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this forkliftDetail.
        /// </field>
        /// <field name="Task" type="msls.application.Task">
        /// Gets or sets the task for this forkliftDetail.
        /// </field>
        /// <field name="details" type="msls.application.ForkliftDetail.Details">
        /// Gets the details for this forkliftDetail.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ForkliftView(entitySet) {
        /// <summary>
        /// Represents the ForkliftView entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this forkliftView.
        /// </param>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this forkliftView.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this forkliftView.
        /// </field>
        /// <field name="RefNumber" type="String">
        /// Gets or sets the refNumber for this forkliftView.
        /// </field>
        /// <field name="Pieces" type="Number">
        /// Gets or sets the pieces for this forkliftView.
        /// </field>
        /// <field name="IsBUP" type="Number">
        /// Gets or sets the isBUP for this forkliftView.
        /// </field>
        /// <field name="PaId" type="String">
        /// Gets or sets the paId for this forkliftView.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this forkliftView.
        /// </field>
        /// <field name="RefNumber1" type="String">
        /// Gets or sets the refNumber1 for this forkliftView.
        /// </field>
        /// <field name="TaskId" type="String">
        /// Gets or sets the taskId for this forkliftView.
        /// </field>
        /// <field name="details" type="msls.application.ForkliftView.Details">
        /// Gets the details for this forkliftView.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function FSN(entitySet) {
        /// <summary>
        /// Represents the FSN entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this fSN.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this fSN.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this fSN.
        /// </field>
        /// <field name="LastViewed" type="Date">
        /// Gets or sets the lastViewed for this fSN.
        /// </field>
        /// <field name="CCL_ArrivalAirportCode" type="String">
        /// Gets or sets the cCL_ArrivalAirportCode for this fSN.
        /// </field>
        /// <field name="CCL_CargoTerminalOperator" type="String">
        /// Gets or sets the cCL_CargoTerminalOperator for this fSN.
        /// </field>
        /// <field name="AWB_CarrierCode" type="String">
        /// Gets or sets the aWB_CarrierCode for this fSN.
        /// </field>
        /// <field name="AWB_MasterbillNumber" type="String">
        /// Gets or sets the aWB_MasterbillNumber for this fSN.
        /// </field>
        /// <field name="AWB_ConsolidationIdentifier" type="String">
        /// Gets or sets the aWB_ConsolidationIdentifier for this fSN.
        /// </field>
        /// <field name="AWB_HousebillNumber" type="String">
        /// Gets or sets the aWB_HousebillNumber for this fSN.
        /// </field>
        /// <field name="AWB_PackageTrackingIdentifier" type="String">
        /// Gets or sets the aWB_PackageTrackingIdentifier for this fSN.
        /// </field>
        /// <field name="ARR_ImportingCarrier" type="String">
        /// Gets or sets the aRR_ImportingCarrier for this fSN.
        /// </field>
        /// <field name="ARR_FlightNumber" type="String">
        /// Gets or sets the aRR_FlightNumber for this fSN.
        /// </field>
        /// <field name="ARR_ScheduledArrivalDay" type="String">
        /// Gets or sets the aRR_ScheduledArrivalDay for this fSN.
        /// </field>
        /// <field name="ARR_ScheduledArrivalMonth" type="String">
        /// Gets or sets the aRR_ScheduledArrivalMonth for this fSN.
        /// </field>
        /// <field name="ARR_PartArrivalReference" type="String">
        /// Gets or sets the aRR_PartArrivalReference for this fSN.
        /// </field>
        /// <field name="CSN_ActionCode" type="String">
        /// Gets or sets the cSN_ActionCode for this fSN.
        /// </field>
        /// <field name="CSN_NumberOfPieces" type="String">
        /// Gets or sets the cSN_NumberOfPieces for this fSN.
        /// </field>
        /// <field name="CSN_TransactionDay" type="String">
        /// Gets or sets the cSN_TransactionDay for this fSN.
        /// </field>
        /// <field name="CSN_TransactionMonth" type="String">
        /// Gets or sets the cSN_TransactionMonth for this fSN.
        /// </field>
        /// <field name="CSN_TransactionHour" type="String">
        /// Gets or sets the cSN_TransactionHour for this fSN.
        /// </field>
        /// <field name="CSN_TransactionMinute" type="String">
        /// Gets or sets the cSN_TransactionMinute for this fSN.
        /// </field>
        /// <field name="CSN_EntryType" type="String">
        /// Gets or sets the cSN_EntryType for this fSN.
        /// </field>
        /// <field name="CSN_EntryNumber" type="String">
        /// Gets or sets the cSN_EntryNumber for this fSN.
        /// </field>
        /// <field name="CSN_Remarks" type="String">
        /// Gets or sets the cSN_Remarks for this fSN.
        /// </field>
        /// <field name="ASN_StatusCode" type="String">
        /// Gets or sets the aSN_StatusCode for this fSN.
        /// </field>
        /// <field name="ASN_ActionExplanation" type="String">
        /// Gets or sets the aSN_ActionExplanation for this fSN.
        /// </field>
        /// <field name="details" type="msls.application.FSN.Details">
        /// Gets the details for this fSN.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_OutboundQueue(entitySet) {
        /// <summary>
        /// Represents the HostPlus_OutboundQueue entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_OutboundQueue.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="Timestamp" type="Date">
        /// Gets or sets the timestamp for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="MessageType" type="String">
        /// Gets or sets the messageType for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="StatusCode" type="String">
        /// Gets or sets the statusCode for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="Transmitted" type="Boolean">
        /// Gets or sets the transmitted for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="Emailed" type="Boolean">
        /// Gets or sets the emailed for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="ErrorMessage" type="String">
        /// Gets or sets the errorMessage for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="FlightManifest" type="msls.application.FlightManifest">
        /// Gets or sets the flightManifest for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this hostPlus_OutboundQueue.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_OutboundQueue.Details">
        /// Gets the details for this hostPlus_OutboundQueue.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HWBHTSSet(entitySet) {
        /// <summary>
        /// Represents the HWBHTSSet entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hWBHTSSet.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this hWBHTSSet.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hWBHTSSet.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this hWBHTSSet.
        /// </field>
        /// <field name="ImportExportCode" type="msls.application.ImportExportCode">
        /// Gets or sets the importExportCode for this hWBHTSSet.
        /// </field>
        /// <field name="details" type="msls.application.HWBHTSSet.Details">
        /// Gets the details for this hWBHTSSet.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HWB(entitySet) {
        /// <summary>
        /// Represents the HWB entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hWB.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this hWB.
        /// </field>
        /// <field name="HWBSerialNumber" type="String">
        /// Gets or sets the hWBSerialNumber for this hWB.
        /// </field>
        /// <field name="Pieces" type="Number">
        /// Gets or sets the pieces for this hWB.
        /// </field>
        /// <field name="Weight" type="Number">
        /// Gets or sets the weight for this hWB.
        /// </field>
        /// <field name="SLAC" type="Number">
        /// Gets or sets the sLAC for this hWB.
        /// </field>
        /// <field name="DescriptionOfGoods" type="String">
        /// Gets or sets the descriptionOfGoods for this hWB.
        /// </field>
        /// <field name="StatusTimestamp" type="Date">
        /// Gets or sets the statusTimestamp for this hWB.
        /// </field>
        /// <field name="ReceivedPieces" type="Number">
        /// Gets or sets the receivedPieces for this hWB.
        /// </field>
        /// <field name="PutAwayPieces" type="Number">
        /// Gets or sets the putAwayPieces for this hWB.
        /// </field>
        /// <field name="PercentReceived" type="Number">
        /// Gets or sets the percentReceived for this hWB.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this hWB.
        /// </field>
        /// <field name="ForkliftPieces" type="Number">
        /// Gets or sets the forkliftPieces for this hWB.
        /// </field>
        /// <field name="ReleasedPieces" type="Number">
        /// Gets or sets the releasedPieces for this hWB.
        /// </field>
        /// <field name="AvailablePieces" type="Number">
        /// Gets or sets the availablePieces for this hWB.
        /// </field>
        /// <field name="IsOSD" type="Boolean">
        /// Gets or sets the isOSD for this hWB.
        /// </field>
        /// <field name="CollectFee" type="String">
        /// Gets or sets the collectFee for this hWB.
        /// </field>
        /// <field name="IsCollect" type="Boolean">
        /// Gets or sets the isCollect for this hWB.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hWB.
        /// </field>
        /// <field name="Shipper" type="msls.application.Customer">
        /// Gets or sets the shipper for this hWB.
        /// </field>
        /// <field name="Consignee" type="msls.application.Customer">
        /// Gets or sets the consignee for this hWB.
        /// </field>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this hWB.
        /// </field>
        /// <field name="WarehouseLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the warehouseLocation for this hWB.
        /// </field>
        /// <field name="HwbOrigin" type="msls.application.Port">
        /// Gets or sets the hwbOrigin for this hWB.
        /// </field>
        /// <field name="HwbDestination" type="msls.application.Port">
        /// Gets or sets the hwbDestination for this hWB.
        /// </field>
        /// <field name="WgtUom" type="msls.application.UOM">
        /// Gets or sets the wgtUom for this hWB.
        /// </field>
        /// <field name="DischargeDetails" type="msls.EntityCollection" elementType="msls.application.DischargeDetail">
        /// Gets the dischargeDetails for this hWB.
        /// </field>
        /// <field name="ForkliftDetails" type="msls.EntityCollection" elementType="msls.application.ForkliftDetail">
        /// Gets the forkliftDetails for this hWB.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this hWB.
        /// </field>
        /// <field name="Dispositions" type="msls.EntityCollection" elementType="msls.application.Disposition">
        /// Gets the dispositions for this hWB.
        /// </field>
        /// <field name="CustomsNotifications" type="msls.EntityCollection" elementType="msls.application.CustomsNotification">
        /// Gets the customsNotifications for this hWB.
        /// </field>
        /// <field name="DescriptionOfGoods1" type="msls.EntityCollection" elementType="msls.application.DescriptionOfGood">
        /// Gets the descriptionOfGoods1 for this hWB.
        /// </field>
        /// <field name="HWBHTSSet" type="msls.EntityCollection" elementType="msls.application.HWBHTSSet">
        /// Gets the hWBHTSSet for this hWB.
        /// </field>
        /// <field name="AWBs_HWBs" type="msls.EntityCollection" elementType="msls.application.AWBs_HWB">
        /// Gets the aWBs_HWBs for this hWB.
        /// </field>
        /// <field name="ChargeDeclarations" type="msls.EntityCollection" elementType="msls.application.ChargeDeclaration">
        /// Gets the chargeDeclarations for this hWB.
        /// </field>
        /// <field name="OCIs" type="msls.EntityCollection" elementType="msls.application.OCI">
        /// Gets the oCIs for this hWB.
        /// </field>
        /// <field name="SpecialHandlings" type="msls.EntityCollection" elementType="msls.application.SpecialHandling">
        /// Gets the specialHandlings for this hWB.
        /// </field>
        /// <field name="HostPlus_OutboundQueues" type="msls.EntityCollection" elementType="msls.application.HostPlus_OutboundQueue">
        /// Gets the hostPlus_OutboundQueues for this hWB.
        /// </field>
        /// <field name="details" type="msls.application.HWB.Details">
        /// Gets the details for this hWB.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ImportExportCode(entitySet) {
        /// <summary>
        /// Represents the ImportExportCode entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this importExportCode.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this importExportCode.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this importExportCode.
        /// </field>
        /// <field name="ShortDescription" type="String">
        /// Gets or sets the shortDescription for this importExportCode.
        /// </field>
        /// <field name="LongDescription" type="String">
        /// Gets or sets the longDescription for this importExportCode.
        /// </field>
        /// <field name="UOM" type="String">
        /// Gets or sets the uOM for this importExportCode.
        /// </field>
        /// <field name="UnitOfQty" type="String">
        /// Gets or sets the unitOfQty for this importExportCode.
        /// </field>
        /// <field name="Sitc" type="String">
        /// Gets or sets the sitc for this importExportCode.
        /// </field>
        /// <field name="EndUseClassification" type="String">
        /// Gets or sets the endUseClassification for this importExportCode.
        /// </field>
        /// <field name="UsdaProductCode" type="String">
        /// Gets or sets the usdaProductCode for this importExportCode.
        /// </field>
        /// <field name="NaicsClassification" type="String">
        /// Gets or sets the naicsClassification for this importExportCode.
        /// </field>
        /// <field name="HiTechClassification" type="String">
        /// Gets or sets the hiTechClassification for this importExportCode.
        /// </field>
        /// <field name="CodeType" type="String">
        /// Gets or sets the codeType for this importExportCode.
        /// </field>
        /// <field name="UsedForAes" type="Boolean">
        /// Gets or sets the usedForAes for this importExportCode.
        /// </field>
        /// <field name="HWBHTSSet" type="msls.EntityCollection" elementType="msls.application.HWBHTSSet">
        /// Gets the hWBHTSSet for this importExportCode.
        /// </field>
        /// <field name="details" type="msls.application.ImportExportCode.Details">
        /// Gets the details for this importExportCode.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function InventoryLocationItem(entitySet) {
        /// <summary>
        /// Represents the InventoryLocationItem entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this inventoryLocationItem.
        /// </param>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this inventoryLocationItem.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this inventoryLocationItem.
        /// </field>
        /// <field name="DispositionId" type="String">
        /// Gets or sets the dispositionId for this inventoryLocationItem.
        /// </field>
        /// <field name="TaskId" type="String">
        /// Gets or sets the taskId for this inventoryLocationItem.
        /// </field>
        /// <field name="Count" type="Number">
        /// Gets or sets the count for this inventoryLocationItem.
        /// </field>
        /// <field name="WarehouseLocationId" type="Number">
        /// Gets or sets the warehouseLocationId for this inventoryLocationItem.
        /// </field>
        /// <field name="RefNumber" type="String">
        /// Gets or sets the refNumber for this inventoryLocationItem.
        /// </field>
        /// <field name="RefNumber1" type="String">
        /// Gets or sets the refNumber1 for this inventoryLocationItem.
        /// </field>
        /// <field name="PaId" type="String">
        /// Gets or sets the paId for this inventoryLocationItem.
        /// </field>
        /// <field name="WarehouseLocation" type="msls.application.WarehouseLocation">
        /// Gets or sets the warehouseLocation for this inventoryLocationItem.
        /// </field>
        /// <field name="details" type="msls.application.InventoryLocationItem.Details">
        /// Gets the details for this inventoryLocationItem.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function InventoryView(entitySet) {
        /// <summary>
        /// Represents the InventoryView entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this inventoryView.
        /// </param>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this inventoryView.
        /// </field>
        /// <field name="Reference" type="String">
        /// Gets or sets the reference for this inventoryView.
        /// </field>
        /// <field name="Reference1" type="String">
        /// Gets or sets the reference1 for this inventoryView.
        /// </field>
        /// <field name="Count" type="Number">
        /// Gets or sets the count for this inventoryView.
        /// </field>
        /// <field name="Location" type="String">
        /// Gets or sets the location for this inventoryView.
        /// </field>
        /// <field name="LocationType" type="String">
        /// Gets or sets the locationType for this inventoryView.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this inventoryView.
        /// </field>
        /// <field name="Timestamp" type="Date">
        /// Gets or sets the timestamp for this inventoryView.
        /// </field>
        /// <field name="WarehouseId" type="Number">
        /// Gets or sets the warehouseId for this inventoryView.
        /// </field>
        /// <field name="PortId" type="Number">
        /// Gets or sets the portId for this inventoryView.
        /// </field>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this inventoryView.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this inventoryView.
        /// </field>
        /// <field name="EntityId1" type="String">
        /// Gets or sets the entityId1 for this inventoryView.
        /// </field>
        /// <field name="details" type="msls.application.InventoryView.Details">
        /// Gets the details for this inventoryView.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function LegSegment(entitySet) {
        /// <summary>
        /// Represents the LegSegment entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this legSegment.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this legSegment.
        /// </field>
        /// <field name="FlightNumber" type="String">
        /// Gets or sets the flightNumber for this legSegment.
        /// </field>
        /// <field name="ETD" type="Date">
        /// Gets or sets the eTD for this legSegment.
        /// </field>
        /// <field name="Sequence" type="Number">
        /// Gets or sets the sequence for this legSegment.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this legSegment.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this legSegment.
        /// </field>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this legSegment.
        /// </field>
        /// <field name="Port" type="msls.application.Port">
        /// Gets or sets the port for this legSegment.
        /// </field>
        /// <field name="ManifestAWBDetail" type="msls.application.ManifestAWBDetail">
        /// Gets or sets the manifestAWBDetail for this legSegment.
        /// </field>
        /// <field name="details" type="msls.application.LegSegment.Details">
        /// Gets the details for this legSegment.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function LineIdentifier(entitySet) {
        /// <summary>
        /// Represents the LineIdentifier entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this lineIdentifier.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this lineIdentifier.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this lineIdentifier.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this lineIdentifier.
        /// </field>
        /// <field name="OCIs" type="msls.EntityCollection" elementType="msls.application.OCI">
        /// Gets the oCIs for this lineIdentifier.
        /// </field>
        /// <field name="details" type="msls.application.LineIdentifier.Details">
        /// Gets the details for this lineIdentifier.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function LoadingIndicator(entitySet) {
        /// <summary>
        /// Represents the LoadingIndicator entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this loadingIndicator.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this loadingIndicator.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this loadingIndicator.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this loadingIndicator.
        /// </field>
        /// <field name="ULDs" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs for this loadingIndicator.
        /// </field>
        /// <field name="details" type="msls.application.LoadingIndicator.Details">
        /// Gets the details for this loadingIndicator.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ManifestAWBDetail(entitySet) {
        /// <summary>
        /// Represents the ManifestAWBDetail entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this manifestAWBDetail.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this manifestAWBDetail.
        /// </field>
        /// <field name="Part" type="String">
        /// Gets or sets the part for this manifestAWBDetail.
        /// </field>
        /// <field name="LoadCount" type="Number">
        /// Gets or sets the loadCount for this manifestAWBDetail.
        /// </field>
        /// <field name="ReceivedCount" type="Number">
        /// Gets or sets the receivedCount for this manifestAWBDetail.
        /// </field>
        /// <field name="Weight" type="Number">
        /// Gets or sets the weight for this manifestAWBDetail.
        /// </field>
        /// <field name="Volume" type="Number">
        /// Gets or sets the volume for this manifestAWBDetail.
        /// </field>
        /// <field name="DensityIndicator" type="Boolean">
        /// Gets or sets the densityIndicator for this manifestAWBDetail.
        /// </field>
        /// <field name="IsBUP" type="Boolean">
        /// Gets or sets the isBUP for this manifestAWBDetail.
        /// </field>
        /// <field name="IsOSD" type="Boolean">
        /// Gets or sets the isOSD for this manifestAWBDetail.
        /// </field>
        /// <field name="PercentReceived" type="Number">
        /// Gets or sets the percentReceived for this manifestAWBDetail.
        /// </field>
        /// <field name="PutAwayPieces" type="Number">
        /// Gets or sets the putAwayPieces for this manifestAWBDetail.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this manifestAWBDetail.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this manifestAWBDetail.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this manifestAWBDetail.
        /// </field>
        /// <field name="FlightManifest" type="msls.application.FlightManifest">
        /// Gets or sets the flightManifest for this manifestAWBDetail.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this manifestAWBDetail.
        /// </field>
        /// <field name="UnloadingPort" type="msls.application.UnloadingPort">
        /// Gets or sets the unloadingPort for this manifestAWBDetail.
        /// </field>
        /// <field name="ConsignmentCode" type="msls.application.ConsignmentCode">
        /// Gets or sets the consignmentCode for this manifestAWBDetail.
        /// </field>
        /// <field name="DensityGroup" type="msls.application.DensityGroup">
        /// Gets or sets the densityGroup for this manifestAWBDetail.
        /// </field>
        /// <field name="UOM" type="msls.application.UOM">
        /// Gets or sets the uOM for this manifestAWBDetail.
        /// </field>
        /// <field name="UOM1" type="msls.application.UOM">
        /// Gets or sets the uOM1 for this manifestAWBDetail.
        /// </field>
        /// <field name="LegSegments" type="msls.EntityCollection" elementType="msls.application.LegSegment">
        /// Gets the legSegments for this manifestAWBDetail.
        /// </field>
        /// <field name="details" type="msls.application.ManifestAWBDetail.Details">
        /// Gets the details for this manifestAWBDetail.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ManifestDetail(entitySet) {
        /// <summary>
        /// Represents the ManifestDetail entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this manifestDetail.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this manifestDetail.
        /// </field>
        /// <field name="Part" type="String">
        /// Gets or sets the part for this manifestDetail.
        /// </field>
        /// <field name="LoadCount" type="Number">
        /// Gets or sets the loadCount for this manifestDetail.
        /// </field>
        /// <field name="Weight" type="Number">
        /// Gets or sets the weight for this manifestDetail.
        /// </field>
        /// <field name="Volume" type="Number">
        /// Gets or sets the volume for this manifestDetail.
        /// </field>
        /// <field name="DensityIndicator" type="Boolean">
        /// Gets or sets the densityIndicator for this manifestDetail.
        /// </field>
        /// <field name="PercentReceived" type="Number">
        /// Gets or sets the percentReceived for this manifestDetail.
        /// </field>
        /// <field name="ReceivedCount" type="Number">
        /// Gets or sets the receivedCount for this manifestDetail.
        /// </field>
        /// <field name="PutAwayPieces" type="Number">
        /// Gets or sets the putAwayPieces for this manifestDetail.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this manifestDetail.
        /// </field>
        /// <field name="IsBUP" type="Boolean">
        /// Gets or sets the isBUP for this manifestDetail.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this manifestDetail.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this manifestDetail.
        /// </field>
        /// <field name="FlightManifest" type="msls.application.FlightManifest">
        /// Gets or sets the flightManifest for this manifestDetail.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this manifestDetail.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this manifestDetail.
        /// </field>
        /// <field name="ConsignmentCode" type="msls.application.ConsignmentCode">
        /// Gets or sets the consignmentCode for this manifestDetail.
        /// </field>
        /// <field name="DensityGroup" type="msls.application.DensityGroup">
        /// Gets or sets the densityGroup for this manifestDetail.
        /// </field>
        /// <field name="WgtUom" type="msls.application.UOM">
        /// Gets or sets the wgtUom for this manifestDetail.
        /// </field>
        /// <field name="VolUom" type="msls.application.UOM">
        /// Gets or sets the volUom for this manifestDetail.
        /// </field>
        /// <field name="UnloadingPort" type="msls.application.UnloadingPort">
        /// Gets or sets the unloadingPort for this manifestDetail.
        /// </field>
        /// <field name="details" type="msls.application.ManifestDetail.Details">
        /// Gets the details for this manifestDetail.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function MOT(entitySet) {
        /// <summary>
        /// Represents the MOT entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this mOT.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this mOT.
        /// </field>
        /// <field name="MOT1" type="String">
        /// Gets or sets the mOT1 for this mOT.
        /// </field>
        /// <field name="Carriers" type="msls.EntityCollection" elementType="msls.application.Carrier">
        /// Gets the carriers for this mOT.
        /// </field>
        /// <field name="Ports" type="msls.EntityCollection" elementType="msls.application.Port">
        /// Gets the ports for this mOT.
        /// </field>
        /// <field name="details" type="msls.application.MOT.Details">
        /// Gets the details for this mOT.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function MovementPriorityCode(entitySet) {
        /// <summary>
        /// Represents the MovementPriorityCode entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this movementPriorityCode.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this movementPriorityCode.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this movementPriorityCode.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this movementPriorityCode.
        /// </field>
        /// <field name="AWBs" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this movementPriorityCode.
        /// </field>
        /// <field name="details" type="msls.application.MovementPriorityCode.Details">
        /// Gets the details for this movementPriorityCode.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function OCI(entitySet) {
        /// <summary>
        /// Represents the OCI entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this oCI.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this oCI.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this oCI.
        /// </field>
        /// <field name="SupCustomsInfo" type="String">
        /// Gets or sets the supCustomsInfo for this oCI.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this oCI.
        /// </field>
        /// <field name="Country" type="msls.application.Country">
        /// Gets or sets the country for this oCI.
        /// </field>
        /// <field name="CustomsInfoIdentifier" type="msls.application.CustomsInfoIdentifier">
        /// Gets or sets the customsInfoIdentifier for this oCI.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this oCI.
        /// </field>
        /// <field name="LineIdentifier" type="msls.application.LineIdentifier">
        /// Gets or sets the lineIdentifier for this oCI.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this oCI.
        /// </field>
        /// <field name="details" type="msls.application.OCI.Details">
        /// Gets the details for this oCI.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ParticipantIdentifier(entitySet) {
        /// <summary>
        /// Represents the ParticipantIdentifier entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this participantIdentifier.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this participantIdentifier.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this participantIdentifier.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this participantIdentifier.
        /// </field>
        /// <field name="details" type="msls.application.ParticipantIdentifier.Details">
        /// Gets the details for this participantIdentifier.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function PaymentType(entitySet) {
        /// <summary>
        /// Represents the PaymentType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this paymentType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this paymentType.
        /// </field>
        /// <field name="PaymentType1" type="String">
        /// Gets or sets the paymentType1 for this paymentType.
        /// </field>
        /// <field name="IsAutoGenerated" type="Boolean">
        /// Gets or sets the isAutoGenerated for this paymentType.
        /// </field>
        /// <field name="DischargeCharges" type="msls.EntityCollection" elementType="msls.application.DischargeCharge">
        /// Gets the dischargeCharges for this paymentType.
        /// </field>
        /// <field name="DischargePayments" type="msls.EntityCollection" elementType="msls.application.DischargePayment">
        /// Gets the dischargePayments for this paymentType.
        /// </field>
        /// <field name="details" type="msls.application.PaymentType.Details">
        /// Gets the details for this paymentType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Port(entitySet) {
        /// <summary>
        /// Represents the Port entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this port.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this port.
        /// </field>
        /// <field name="IATACode" type="String">
        /// Gets or sets the iATACode for this port.
        /// </field>
        /// <field name="Port1" type="String">
        /// Gets or sets the port1 for this port.
        /// </field>
        /// <field name="IsGateway" type="Boolean">
        /// Gets or sets the isGateway for this port.
        /// </field>
        /// <field name="FlightManifests" type="msls.EntityCollection" elementType="msls.application.FlightManifest">
        /// Gets the flightManifests for this port.
        /// </field>
        /// <field name="FlightManifests1" type="msls.EntityCollection" elementType="msls.application.FlightManifest">
        /// Gets the flightManifests1 for this port.
        /// </field>
        /// <field name="Country" type="msls.application.Country">
        /// Gets or sets the country for this port.
        /// </field>
        /// <field name="MOT" type="msls.application.MOT">
        /// Gets or sets the mOT for this port.
        /// </field>
        /// <field name="State" type="msls.application.State">
        /// Gets or sets the state for this port.
        /// </field>
        /// <field name="AWBs" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this port.
        /// </field>
        /// <field name="AWBs1" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs1 for this port.
        /// </field>
        /// <field name="HWBs" type="msls.EntityCollection" elementType="msls.application.HWB">
        /// Gets the hWBs for this port.
        /// </field>
        /// <field name="HWBs1" type="msls.EntityCollection" elementType="msls.application.HWB">
        /// Gets the hWBs1 for this port.
        /// </field>
        /// <field name="Warehouses" type="msls.EntityCollection" elementType="msls.application.Warehouse">
        /// Gets the warehouses for this port.
        /// </field>
        /// <field name="UnloadingPorts" type="msls.EntityCollection" elementType="msls.application.UnloadingPort">
        /// Gets the unloadingPorts for this port.
        /// </field>
        /// <field name="LegSegments" type="msls.EntityCollection" elementType="msls.application.LegSegment">
        /// Gets the legSegments for this port.
        /// </field>
        /// <field name="UserStations" type="msls.EntityCollection" elementType="msls.application.UserStation">
        /// Gets the userStations for this port.
        /// </field>
        /// <field name="CarrierRoutes" type="msls.EntityCollection" elementType="msls.application.CarrierRoute">
        /// Gets the carrierRoutes for this port.
        /// </field>
        /// <field name="CarrierRoutes1" type="msls.EntityCollection" elementType="msls.application.CarrierRoute">
        /// Gets the carrierRoutes1 for this port.
        /// </field>
        /// <field name="CarrierRoutes2" type="msls.EntityCollection" elementType="msls.application.CarrierRoute">
        /// Gets the carrierRoutes2 for this port.
        /// </field>
        /// <field name="details" type="msls.application.Port.Details">
        /// Gets the details for this port.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function PutAwayList(entitySet) {
        /// <summary>
        /// Represents the PutAwayList entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this putAwayList.
        /// </param>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this putAwayList.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this putAwayList.
        /// </field>
        /// <field name="RefNumber" type="String">
        /// Gets or sets the refNumber for this putAwayList.
        /// </field>
        /// <field name="PutAwayPieces" type="Number">
        /// Gets or sets the putAwayPieces for this putAwayList.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this putAwayList.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this putAwayList.
        /// </field>
        /// <field name="IsBUP" type="Number">
        /// Gets or sets the isBUP for this putAwayList.
        /// </field>
        /// <field name="Status" type="Number">
        /// Gets or sets the status for this putAwayList.
        /// </field>
        /// <field name="PaId" type="String">
        /// Gets or sets the paId for this putAwayList.
        /// </field>
        /// <field name="Status1" type="msls.application.Status">
        /// Gets or sets the status1 for this putAwayList.
        /// </field>
        /// <field name="RefNumber1" type="String">
        /// Gets or sets the refNumber1 for this putAwayList.
        /// </field>
        /// <field name="ForkliftPieces" type="Number">
        /// Gets or sets the forkliftPieces for this putAwayList.
        /// </field>
        /// <field name="FlightManifestId" type="String">
        /// Gets or sets the flightManifestId for this putAwayList.
        /// </field>
        /// <field name="details" type="msls.application.PutAwayList.Details">
        /// Gets the details for this putAwayList.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function PutedAwayItem(entitySet) {
        /// <summary>
        /// Represents the PutedAwayItem entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this putedAwayItem.
        /// </param>
        /// <field name="Reference" type="String">
        /// Gets or sets the reference for this putedAwayItem.
        /// </field>
        /// <field name="Reference1" type="String">
        /// Gets or sets the reference1 for this putedAwayItem.
        /// </field>
        /// <field name="WarehouseLocationId" type="Number">
        /// Gets or sets the warehouseLocationId for this putedAwayItem.
        /// </field>
        /// <field name="Count" type="Number">
        /// Gets or sets the count for this putedAwayItem.
        /// </field>
        /// <field name="Location" type="String">
        /// Gets or sets the location for this putedAwayItem.
        /// </field>
        /// <field name="DispositionId" type="String">
        /// Gets or sets the dispositionId for this putedAwayItem.
        /// </field>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this putedAwayItem.
        /// </field>
        /// <field name="IsBUP" type="Number">
        /// Gets or sets the isBUP for this putedAwayItem.
        /// </field>
        /// <field name="Status" type="Number">
        /// Gets or sets the status for this putedAwayItem.
        /// </field>
        /// <field name="PaId" type="String">
        /// Gets or sets the paId for this putedAwayItem.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this putedAwayItem.
        /// </field>
        /// <field name="Status1" type="msls.application.Status">
        /// Gets or sets the status1 for this putedAwayItem.
        /// </field>
        /// <field name="details" type="msls.application.PutedAwayItem.Details">
        /// Gets the details for this putedAwayItem.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ReceiverAwbManifestList(entitySet) {
        /// <summary>
        /// Represents the ReceiverAwbManifestList entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this receiverAwbManifestList.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this receiverAwbManifestList.
        /// </field>
        /// <field name="FlightManifestId" type="String">
        /// Gets or sets the flightManifestId for this receiverAwbManifestList.
        /// </field>
        /// <field name="AwbNumber" type="String">
        /// Gets or sets the awbNumber for this receiverAwbManifestList.
        /// </field>
        /// <field name="details" type="msls.application.ReceiverAwbManifestList.Details">
        /// Gets the details for this receiverAwbManifestList.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ReceiverFlightView(entitySet) {
        /// <summary>
        /// Represents the ReceiverFlightView entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this receiverFlightView.
        /// </param>
        /// <field name="FlightManifestId" type="String">
        /// Gets or sets the flightManifestId for this receiverFlightView.
        /// </field>
        /// <field name="OrgFlightNumber" type="String">
        /// Gets or sets the orgFlightNumber for this receiverFlightView.
        /// </field>
        /// <field name="ETA" type="Date">
        /// Gets or sets the eTA for this receiverFlightView.
        /// </field>
        /// <field name="ArrivalTime" type="String">
        /// Gets or sets the arrivalTime for this receiverFlightView.
        /// </field>
        /// <field name="TotalULDs" type="Number">
        /// Gets or sets the totalULDs for this receiverFlightView.
        /// </field>
        /// <field name="TotalAWBs" type="Number">
        /// Gets or sets the totalAWBs for this receiverFlightView.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this receiverFlightView.
        /// </field>
        /// <field name="CarrierName" type="String">
        /// Gets or sets the carrierName for this receiverFlightView.
        /// </field>
        /// <field name="FlightNumber" type="String">
        /// Gets or sets the flightNumber for this receiverFlightView.
        /// </field>
        /// <field name="RecoveredULDs" type="Number">
        /// Gets or sets the recoveredULDs for this receiverFlightView.
        /// </field>
        /// <field name="ReceivedPieces" type="Number">
        /// Gets or sets the receivedPieces for this receiverFlightView.
        /// </field>
        /// <field name="RecoveredBy" type="String">
        /// Gets or sets the recoveredBy for this receiverFlightView.
        /// </field>
        /// <field name="RecoveredOn" type="String">
        /// Gets or sets the recoveredOn for this receiverFlightView.
        /// </field>
        /// <field name="IsNilCargo" type="Boolean">
        /// Gets or sets the isNilCargo for this receiverFlightView.
        /// </field>
        /// <field name="PortId" type="Number">
        /// Gets or sets the portId for this receiverFlightView.
        /// </field>
        /// <field name="StatusId" type="Number">
        /// Gets or sets the statusId for this receiverFlightView.
        /// </field>
        /// <field name="StatusName" type="String">
        /// Gets or sets the statusName for this receiverFlightView.
        /// </field>
        /// <field name="CarrierLogoPath" type="String">
        /// Gets or sets the carrierLogoPath for this receiverFlightView.
        /// </field>
        /// <field name="StatusImagePath" type="String">
        /// Gets or sets the statusImagePath for this receiverFlightView.
        /// </field>
        /// <field name="Location" type="String">
        /// Gets or sets the location for this receiverFlightView.
        /// </field>
        /// <field name="UnloadingId" type="Number">
        /// Gets or sets the unloadingId for this receiverFlightView.
        /// </field>
        /// <field name="IsComplete" type="Boolean">
        /// Gets or sets the isComplete for this receiverFlightView.
        /// </field>
        /// <field name="Origin" type="String">
        /// Gets or sets the origin for this receiverFlightView.
        /// </field>
        /// <field name="details" type="msls.application.ReceiverFlightView.Details">
        /// Gets the details for this receiverFlightView.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ReceiverUldManifestList(entitySet) {
        /// <summary>
        /// Represents the ReceiverUldManifestList entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this receiverUldManifestList.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this receiverUldManifestList.
        /// </field>
        /// <field name="FlightManifestId" type="String">
        /// Gets or sets the flightManifestId for this receiverUldManifestList.
        /// </field>
        /// <field name="UldNumber" type="String">
        /// Gets or sets the uldNumber for this receiverUldManifestList.
        /// </field>
        /// <field name="ULDId" type="String">
        /// Gets or sets the uLDId for this receiverUldManifestList.
        /// </field>
        /// <field name="IsBUP" type="Boolean">
        /// Gets or sets the isBUP for this receiverUldManifestList.
        /// </field>
        /// <field name="TotalReceivedCount" type="Number">
        /// Gets or sets the totalReceivedCount for this receiverUldManifestList.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this receiverUldManifestList.
        /// </field>
        /// <field name="ReceivedPieces" type="Number">
        /// Gets or sets the receivedPieces for this receiverUldManifestList.
        /// </field>
        /// <field name="StatusId" type="Number">
        /// Gets or sets the statusId for this receiverUldManifestList.
        /// </field>
        /// <field name="CarrierCode" type="String">
        /// Gets or sets the carrierCode for this receiverUldManifestList.
        /// </field>
        /// <field name="StatusImagePath" type="String">
        /// Gets or sets the statusImagePath for this receiverUldManifestList.
        /// </field>
        /// <field name="PercentReceived" type="Number">
        /// Gets or sets the percentReceived for this receiverUldManifestList.
        /// </field>
        /// <field name="details" type="msls.application.ReceiverUldManifestList.Details">
        /// Gets the details for this receiverUldManifestList.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ShipmentUnitType(entitySet) {
        /// <summary>
        /// Represents the ShipmentUnitType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this shipmentUnitType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this shipmentUnitType.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this shipmentUnitType.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this shipmentUnitType.
        /// </field>
        /// <field name="ULDs" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs for this shipmentUnitType.
        /// </field>
        /// <field name="details" type="msls.application.ShipmentUnitType.Details">
        /// Gets the details for this shipmentUnitType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function SnapshotTasksView(entitySet) {
        /// <summary>
        /// Represents the SnapshotTasksView entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this snapshotTasksView.
        /// </param>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this snapshotTasksView.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this snapshotTasksView.
        /// </field>
        /// <field name="RefNumber" type="String">
        /// Gets or sets the refNumber for this snapshotTasksView.
        /// </field>
        /// <field name="RefNumber1" type="String">
        /// Gets or sets the refNumber1 for this snapshotTasksView.
        /// </field>
        /// <field name="TaskCreator" type="String">
        /// Gets or sets the taskCreator for this snapshotTasksView.
        /// </field>
        /// <field name="TaskOwner" type="String">
        /// Gets or sets the taskOwner for this snapshotTasksView.
        /// </field>
        /// <field name="Condition" type="String">
        /// Gets or sets the condition for this snapshotTasksView.
        /// </field>
        /// <field name="Pieces" type="Number">
        /// Gets or sets the pieces for this snapshotTasksView.
        /// </field>
        /// <field name="Weight" type="String">
        /// Gets or sets the weight for this snapshotTasksView.
        /// </field>
        /// <field name="TaskStatus" type="String">
        /// Gets or sets the taskStatus for this snapshotTasksView.
        /// </field>
        /// <field name="StatusImagePath" type="String">
        /// Gets or sets the statusImagePath for this snapshotTasksView.
        /// </field>
        /// <field name="EntityTypeImagePath" type="String">
        /// Gets or sets the entityTypeImagePath for this snapshotTasksView.
        /// </field>
        /// <field name="PortId" type="Number">
        /// Gets or sets the portId for this snapshotTasksView.
        /// </field>
        /// <field name="TaskStatusId" type="Number">
        /// Gets or sets the taskStatusId for this snapshotTasksView.
        /// </field>
        /// <field name="EndDate" type="Date">
        /// Gets or sets the endDate for this snapshotTasksView.
        /// </field>
        /// <field name="WarehouseLocation" type="String">
        /// Gets or sets the warehouseLocation for this snapshotTasksView.
        /// </field>
        /// <field name="SnapshotCount" type="Number">
        /// Gets or sets the snapshotCount for this snapshotTasksView.
        /// </field>
        /// <field name="details" type="msls.application.SnapshotTasksView.Details">
        /// Gets the details for this snapshotTasksView.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function SpecialHandlingCode(entitySet) {
        /// <summary>
        /// Represents the SpecialHandlingCode entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this specialHandlingCode.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this specialHandlingCode.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this specialHandlingCode.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this specialHandlingCode.
        /// </field>
        /// <field name="IsHazmatCode" type="Boolean">
        /// Gets or sets the isHazmatCode for this specialHandlingCode.
        /// </field>
        /// <field name="SpecialHandlings" type="msls.EntityCollection" elementType="msls.application.SpecialHandling">
        /// Gets the specialHandlings for this specialHandlingCode.
        /// </field>
        /// <field name="details" type="msls.application.SpecialHandlingCode.Details">
        /// Gets the details for this specialHandlingCode.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function SpecialHandling(entitySet) {
        /// <summary>
        /// Represents the SpecialHandling entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this specialHandling.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this specialHandling.
        /// </field>
        /// <field name="AWB" type="msls.application.AWB">
        /// Gets or sets the aWB for this specialHandling.
        /// </field>
        /// <field name="HWB" type="msls.application.HWB">
        /// Gets or sets the hWB for this specialHandling.
        /// </field>
        /// <field name="SpecialHandlingCode" type="msls.application.SpecialHandlingCode">
        /// Gets or sets the specialHandlingCode for this specialHandling.
        /// </field>
        /// <field name="ULD" type="msls.application.ULD">
        /// Gets or sets the uLD for this specialHandling.
        /// </field>
        /// <field name="details" type="msls.application.SpecialHandling.Details">
        /// Gets the details for this specialHandling.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function State(entitySet) {
        /// <summary>
        /// Represents the State entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this state.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this state.
        /// </field>
        /// <field name="StateProvince" type="String">
        /// Gets or sets the stateProvince for this state.
        /// </field>
        /// <field name="TwoCharacterCode" type="String">
        /// Gets or sets the twoCharacterCode for this state.
        /// </field>
        /// <field name="Ports" type="msls.EntityCollection" elementType="msls.application.Port">
        /// Gets the ports for this state.
        /// </field>
        /// <field name="Country" type="msls.application.Country">
        /// Gets or sets the country for this state.
        /// </field>
        /// <field name="Warehouses" type="msls.EntityCollection" elementType="msls.application.Warehouse">
        /// Gets the warehouses for this state.
        /// </field>
        /// <field name="Customers" type="msls.EntityCollection" elementType="msls.application.Customer">
        /// Gets the customers for this state.
        /// </field>
        /// <field name="details" type="msls.application.State.Details">
        /// Gets the details for this state.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Status(entitySet) {
        /// <summary>
        /// Represents the Status entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this status.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this status.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this status.
        /// </field>
        /// <field name="AWBs" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this status.
        /// </field>
        /// <field name="FlightManifests" type="msls.EntityCollection" elementType="msls.application.FlightManifest">
        /// Gets the flightManifests for this status.
        /// </field>
        /// <field name="HWBs" type="msls.EntityCollection" elementType="msls.application.HWB">
        /// Gets the hWBs for this status.
        /// </field>
        /// <field name="ULDs" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs for this status.
        /// </field>
        /// <field name="Transactions" type="msls.EntityCollection" elementType="msls.application.Transaction">
        /// Gets the transactions for this status.
        /// </field>
        /// <field name="Tasks" type="msls.EntityCollection" elementType="msls.application.Task">
        /// Gets the tasks for this status.
        /// </field>
        /// <field name="Discharges" type="msls.EntityCollection" elementType="msls.application.Discharge">
        /// Gets the discharges for this status.
        /// </field>
        /// <field name="PutAwayLists" type="msls.EntityCollection" elementType="msls.application.PutAwayList">
        /// Gets the putAwayLists for this status.
        /// </field>
        /// <field name="StatusImagePath" type="String">
        /// Gets or sets the statusImagePath for this status.
        /// </field>
        /// <field name="PutedAwayItems" type="msls.EntityCollection" elementType="msls.application.PutedAwayItem">
        /// Gets the putedAwayItems for this status.
        /// </field>
        /// <field name="details" type="msls.application.Status.Details">
        /// Gets the details for this status.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function TableList(entitySet) {
        /// <summary>
        /// Represents the TableList entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this tableList.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this tableList.
        /// </field>
        /// <field name="DisplayName" type="String">
        /// Gets or sets the displayName for this tableList.
        /// </field>
        /// <field name="TableName" type="String">
        /// Gets or sets the tableName for this tableList.
        /// </field>
        /// <field name="ScreenName" type="String">
        /// Gets or sets the screenName for this tableList.
        /// </field>
        /// <field name="DBName" type="String">
        /// Gets or sets the dBName for this tableList.
        /// </field>
        /// <field name="details" type="msls.application.TableList.Details">
        /// Gets the details for this tableList.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function TaskFlightReceiver(entitySet) {
        /// <summary>
        /// Represents the TaskFlightReceiver entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this taskFlightReceiver.
        /// </param>
        /// <field name="FlightManifestId" type="String">
        /// Gets or sets the flightManifestId for this taskFlightReceiver.
        /// </field>
        /// <field name="TaskId" type="String">
        /// Gets or sets the taskId for this taskFlightReceiver.
        /// </field>
        /// <field name="CarrierCode" type="String">
        /// Gets or sets the carrierCode for this taskFlightReceiver.
        /// </field>
        /// <field name="FlightNumber" type="String">
        /// Gets or sets the flightNumber for this taskFlightReceiver.
        /// </field>
        /// <field name="RecoveredBy" type="String">
        /// Gets or sets the recoveredBy for this taskFlightReceiver.
        /// </field>
        /// <field name="StartDate" type="Date">
        /// Gets or sets the startDate for this taskFlightReceiver.
        /// </field>
        /// <field name="EndDate" type="Date">
        /// Gets or sets the endDate for this taskFlightReceiver.
        /// </field>
        /// <field name="TotalULDs" type="Number">
        /// Gets or sets the totalULDs for this taskFlightReceiver.
        /// </field>
        /// <field name="TotalAWBs" type="Number">
        /// Gets or sets the totalAWBs for this taskFlightReceiver.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this taskFlightReceiver.
        /// </field>
        /// <field name="IsOSD" type="Boolean">
        /// Gets or sets the isOSD for this taskFlightReceiver.
        /// </field>
        /// <field name="FlightManifest" type="msls.application.FlightManifest">
        /// Gets or sets the flightManifest for this taskFlightReceiver.
        /// </field>
        /// <field name="Task" type="msls.application.Task">
        /// Gets or sets the task for this taskFlightReceiver.
        /// </field>
        /// <field name="PercentOverall" type="Number">
        /// Gets or sets the percentOverall for this taskFlightReceiver.
        /// </field>
        /// <field name="ETA" type="Date">
        /// Gets or sets the eTA for this taskFlightReceiver.
        /// </field>
        /// <field name="details" type="msls.application.TaskFlightReceiver.Details">
        /// Gets the details for this taskFlightReceiver.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Task(entitySet) {
        /// <summary>
        /// Represents the Task entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this task.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this task.
        /// </field>
        /// <field name="TaskCreationDate" type="Date">
        /// Gets or sets the taskCreationDate for this task.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this task.
        /// </field>
        /// <field name="TaskCreator" type="String">
        /// Gets or sets the taskCreator for this task.
        /// </field>
        /// <field name="TaskOwner" type="String">
        /// Gets or sets the taskOwner for this task.
        /// </field>
        /// <field name="TaskReference" type="String">
        /// Gets or sets the taskReference for this task.
        /// </field>
        /// <field name="TaskDate" type="Date">
        /// Gets or sets the taskDate for this task.
        /// </field>
        /// <field name="DueDate" type="Date">
        /// Gets or sets the dueDate for this task.
        /// </field>
        /// <field name="StartDate" type="Date">
        /// Gets or sets the startDate for this task.
        /// </field>
        /// <field name="EndDate" type="Date">
        /// Gets or sets the endDate for this task.
        /// </field>
        /// <field name="StatusTimestamp" type="Date">
        /// Gets or sets the statusTimestamp for this task.
        /// </field>
        /// <field name="Warehouse" type="msls.application.Warehouse">
        /// Gets or sets the warehouse for this task.
        /// </field>
        /// <field name="Dispositions" type="msls.EntityCollection" elementType="msls.application.Disposition">
        /// Gets the dispositions for this task.
        /// </field>
        /// <field name="EntityType" type="msls.application.EntityType">
        /// Gets or sets the entityType for this task.
        /// </field>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this task.
        /// </field>
        /// <field name="TaskType" type="msls.application.TaskType">
        /// Gets or sets the taskType for this task.
        /// </field>
        /// <field name="TaskFlightReceivers" type="msls.EntityCollection" elementType="msls.application.TaskFlightReceiver">
        /// Gets the taskFlightReceivers for this task.
        /// </field>
        /// <field name="ForkliftDetails" type="msls.EntityCollection" elementType="msls.application.ForkliftDetail">
        /// Gets the forkliftDetails for this task.
        /// </field>
        /// <field name="Transactions" type="msls.EntityCollection" elementType="msls.application.Transaction">
        /// Gets the transactions for this task.
        /// </field>
        /// <field name="details" type="msls.application.Task.Details">
        /// Gets the details for this task.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function TaskType(entitySet) {
        /// <summary>
        /// Represents the TaskType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this taskType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this taskType.
        /// </field>
        /// <field name="ActionName" type="String">
        /// Gets or sets the actionName for this taskType.
        /// </field>
        /// <field name="Category" type="String">
        /// Gets or sets the category for this taskType.
        /// </field>
        /// <field name="IsActive" type="Boolean">
        /// Gets or sets the isActive for this taskType.
        /// </field>
        /// <field name="TaskIcon" type="String">
        /// Gets or sets the taskIcon for this taskType.
        /// </field>
        /// <field name="Transactions" type="msls.EntityCollection" elementType="msls.application.Transaction">
        /// Gets the transactions for this taskType.
        /// </field>
        /// <field name="Tasks" type="msls.EntityCollection" elementType="msls.application.Task">
        /// Gets the tasks for this taskType.
        /// </field>
        /// <field name="details" type="msls.application.TaskType.Details">
        /// Gets the details for this taskType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function TrackTraceView(entitySet) {
        /// <summary>
        /// Represents the TrackTraceView entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this trackTraceView.
        /// </param>
        /// <field name="ArrivalTime" type="String">
        /// Gets or sets the arrivalTime for this trackTraceView.
        /// </field>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this trackTraceView.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this trackTraceView.
        /// </field>
        /// <field name="Reference" type="String">
        /// Gets or sets the reference for this trackTraceView.
        /// </field>
        /// <field name="Part" type="String">
        /// Gets or sets the part for this trackTraceView.
        /// </field>
        /// <field name="HBCount" type="Number">
        /// Gets or sets the hBCount for this trackTraceView.
        /// </field>
        /// <field name="ReceivedPieces" type="Number">
        /// Gets or sets the receivedPieces for this trackTraceView.
        /// </field>
        /// <field name="Weight" type="String">
        /// Gets or sets the weight for this trackTraceView.
        /// </field>
        /// <field name="Weight1" type="Number">
        /// Gets or sets the weight1 for this trackTraceView.
        /// </field>
        /// <field name="GeneralOrder" type="String">
        /// Gets or sets the generalOrder for this trackTraceView.
        /// </field>
        /// <field name="CustomsCode" type="String">
        /// Gets or sets the customsCode for this trackTraceView.
        /// </field>
        /// <field name="ISC" type="String">
        /// Gets or sets the iSC for this trackTraceView.
        /// </field>
        /// <field name="StorageChargesPerDay" type="Number">
        /// Gets or sets the storageChargesPerDay for this trackTraceView.
        /// </field>
        /// <field name="CurrentStorageCharges" type="Number">
        /// Gets or sets the currentStorageCharges for this trackTraceView.
        /// </field>
        /// <field name="OutstandingBalance" type="Number">
        /// Gets or sets the outstandingBalance for this trackTraceView.
        /// </field>
        /// <field name="Carrier" type="String">
        /// Gets or sets the carrier for this trackTraceView.
        /// </field>
        /// <field name="FlightNumber" type="String">
        /// Gets or sets the flightNumber for this trackTraceView.
        /// </field>
        /// <field name="StorageStartDate" type="String">
        /// Gets or sets the storageStartDate for this trackTraceView.
        /// </field>
        /// <field name="GeneralOrderDate" type="String">
        /// Gets or sets the generalOrderDate for this trackTraceView.
        /// </field>
        /// <field name="UnloadingPort" type="String">
        /// Gets or sets the unloadingPort for this trackTraceView.
        /// </field>
        /// <field name="FirmCode" type="String">
        /// Gets or sets the firmCode for this trackTraceView.
        /// </field>
        /// <field name="FinalDestination" type="String">
        /// Gets or sets the finalDestination for this trackTraceView.
        /// </field>
        /// <field name="details" type="msls.application.TrackTraceView.Details">
        /// Gets the details for this trackTraceView.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function TransactionAction(entitySet) {
        /// <summary>
        /// Represents the TransactionAction entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this transactionAction.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this transactionAction.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this transactionAction.
        /// </field>
        /// <field name="TaskType" type="Number">
        /// Gets or sets the taskType for this transactionAction.
        /// </field>
        /// <field name="Transactions" type="msls.EntityCollection" elementType="msls.application.Transaction">
        /// Gets the transactions for this transactionAction.
        /// </field>
        /// <field name="details" type="msls.application.TransactionAction.Details">
        /// Gets the details for this transactionAction.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Transaction(entitySet) {
        /// <summary>
        /// Represents the Transaction entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this transaction.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this transaction.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this transaction.
        /// </field>
        /// <field name="StatusTimestamp" type="Date">
        /// Gets or sets the statusTimestamp for this transaction.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this transaction.
        /// </field>
        /// <field name="Reference" type="String">
        /// Gets or sets the reference for this transaction.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this transaction.
        /// </field>
        /// <field name="EntityType" type="msls.application.EntityType">
        /// Gets or sets the entityType for this transaction.
        /// </field>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this transaction.
        /// </field>
        /// <field name="TaskType" type="msls.application.TaskType">
        /// Gets or sets the taskType for this transaction.
        /// </field>
        /// <field name="TransactionAction" type="msls.application.TransactionAction">
        /// Gets or sets the transactionAction for this transaction.
        /// </field>
        /// <field name="Task" type="msls.application.Task">
        /// Gets or sets the task for this transaction.
        /// </field>
        /// <field name="details" type="msls.application.Transaction.Details">
        /// Gets the details for this transaction.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ULD(entitySet) {
        /// <summary>
        /// Represents the ULD entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this uLD.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this uLD.
        /// </field>
        /// <field name="SerialNumber" type="String">
        /// Gets or sets the serialNumber for this uLD.
        /// </field>
        /// <field name="TotalUnitCount" type="Number">
        /// Gets or sets the totalUnitCount for this uLD.
        /// </field>
        /// <field name="TotalReceivedCount" type="Number">
        /// Gets or sets the totalReceivedCount for this uLD.
        /// </field>
        /// <field name="TotalPieces" type="Number">
        /// Gets or sets the totalPieces for this uLD.
        /// </field>
        /// <field name="TotalWeight" type="Number">
        /// Gets or sets the totalWeight for this uLD.
        /// </field>
        /// <field name="TotalVolume" type="Number">
        /// Gets or sets the totalVolume for this uLD.
        /// </field>
        /// <field name="RecoveredBy" type="String">
        /// Gets or sets the recoveredBy for this uLD.
        /// </field>
        /// <field name="ReceivedPieces" type="Number">
        /// Gets or sets the receivedPieces for this uLD.
        /// </field>
        /// <field name="PutAwayPieces" type="Number">
        /// Gets or sets the putAwayPieces for this uLD.
        /// </field>
        /// <field name="PercentRecovered" type="Number">
        /// Gets or sets the percentRecovered for this uLD.
        /// </field>
        /// <field name="PercentReceived" type="Number">
        /// Gets or sets the percentReceived for this uLD.
        /// </field>
        /// <field name="ForkliftPieces" type="Number">
        /// Gets or sets the forkliftPieces for this uLD.
        /// </field>
        /// <field name="PercentPutAway" type="Number">
        /// Gets or sets the percentPutAway for this uLD.
        /// </field>
        /// <field name="RecoveredOn" type="Date">
        /// Gets or sets the recoveredOn for this uLD.
        /// </field>
        /// <field name="Remarks" type="String">
        /// Gets or sets the remarks for this uLD.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this uLD.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this uLD.
        /// </field>
        /// <field name="Carrier" type="msls.application.Carrier">
        /// Gets or sets the carrier for this uLD.
        /// </field>
        /// <field name="FlightManifest" type="msls.application.FlightManifest">
        /// Gets or sets the flightManifest for this uLD.
        /// </field>
        /// <field name="ForkliftDetails" type="msls.EntityCollection" elementType="msls.application.ForkliftDetail">
        /// Gets the forkliftDetails for this uLD.
        /// </field>
        /// <field name="Status" type="msls.application.Status">
        /// Gets or sets the status for this uLD.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this uLD.
        /// </field>
        /// <field name="Dispositions" type="msls.EntityCollection" elementType="msls.application.Disposition">
        /// Gets the dispositions for this uLD.
        /// </field>
        /// <field name="LoadingIndicator" type="msls.application.LoadingIndicator">
        /// Gets or sets the loadingIndicator for this uLD.
        /// </field>
        /// <field name="ShipmentUnitType" type="msls.application.ShipmentUnitType">
        /// Gets or sets the shipmentUnitType for this uLD.
        /// </field>
        /// <field name="ULDVolumeCode" type="msls.application.ULDVolumeCode">
        /// Gets or sets the uLDVolumeCode for this uLD.
        /// </field>
        /// <field name="WgtUom" type="msls.application.UOM">
        /// Gets or sets the wgtUom for this uLD.
        /// </field>
        /// <field name="VolUom" type="msls.application.UOM">
        /// Gets or sets the volUom for this uLD.
        /// </field>
        /// <field name="Dimensions" type="msls.EntityCollection" elementType="msls.application.Dimension">
        /// Gets the dimensions for this uLD.
        /// </field>
        /// <field name="LegSegments" type="msls.EntityCollection" elementType="msls.application.LegSegment">
        /// Gets the legSegments for this uLD.
        /// </field>
        /// <field name="OCIs" type="msls.EntityCollection" elementType="msls.application.OCI">
        /// Gets the oCIs for this uLD.
        /// </field>
        /// <field name="SpecialHandlings" type="msls.EntityCollection" elementType="msls.application.SpecialHandling">
        /// Gets the specialHandlings for this uLD.
        /// </field>
        /// <field name="IsBUP" type="Boolean">
        /// Gets or sets the isBUP for this uLD.
        /// </field>
        /// <field name="HostPlus_OutboundQueues" type="msls.EntityCollection" elementType="msls.application.HostPlus_OutboundQueue">
        /// Gets the hostPlus_OutboundQueues for this uLD.
        /// </field>
        /// <field name="details" type="msls.application.ULD.Details">
        /// Gets the details for this uLD.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function ULDVolumeCode(entitySet) {
        /// <summary>
        /// Represents the ULDVolumeCode entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this uLDVolumeCode.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this uLDVolumeCode.
        /// </field>
        /// <field name="Code" type="Number">
        /// Gets or sets the code for this uLDVolumeCode.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this uLDVolumeCode.
        /// </field>
        /// <field name="ULDs" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs for this uLDVolumeCode.
        /// </field>
        /// <field name="details" type="msls.application.ULDVolumeCode.Details">
        /// Gets the details for this uLDVolumeCode.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function UnloadingPort(entitySet) {
        /// <summary>
        /// Represents the UnloadingPort entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this unloadingPort.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this unloadingPort.
        /// </field>
        /// <field name="POU_ETD" type="Date">
        /// Gets or sets the pOU_ETD for this unloadingPort.
        /// </field>
        /// <field name="POU_ETA" type="Date">
        /// Gets or sets the pOU_ETA for this unloadingPort.
        /// </field>
        /// <field name="IsNilCargo" type="Boolean">
        /// Gets or sets the isNilCargo for this unloadingPort.
        /// </field>
        /// <field name="FlightManifest" type="msls.application.FlightManifest">
        /// Gets or sets the flightManifest for this unloadingPort.
        /// </field>
        /// <field name="Port" type="msls.application.Port">
        /// Gets or sets the port for this unloadingPort.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this unloadingPort.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this unloadingPort.
        /// </field>
        /// <field name="details" type="msls.application.UnloadingPort.Details">
        /// Gets the details for this unloadingPort.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function UOM(entitySet) {
        /// <summary>
        /// Represents the UOM entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this uOM.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this uOM.
        /// </field>
        /// <field name="ThreeCharCode" type="String">
        /// Gets or sets the threeCharCode for this uOM.
        /// </field>
        /// <field name="TwoCharCode" type="String">
        /// Gets or sets the twoCharCode for this uOM.
        /// </field>
        /// <field name="OneCharCode" type="String">
        /// Gets or sets the oneCharCode for this uOM.
        /// </field>
        /// <field name="UOMName" type="String">
        /// Gets or sets the uOMName for this uOM.
        /// </field>
        /// <field name="AWBs" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs for this uOM.
        /// </field>
        /// <field name="AWBs1" type="msls.EntityCollection" elementType="msls.application.AWB">
        /// Gets the aWBs1 for this uOM.
        /// </field>
        /// <field name="UOMSystem" type="msls.application.UOMSystem">
        /// Gets or sets the uOMSystem for this uOM.
        /// </field>
        /// <field name="UOMType" type="msls.application.UOMType">
        /// Gets or sets the uOMType for this uOM.
        /// </field>
        /// <field name="HWBs" type="msls.EntityCollection" elementType="msls.application.HWB">
        /// Gets the hWBs for this uOM.
        /// </field>
        /// <field name="ULDs" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs for this uOM.
        /// </field>
        /// <field name="ULDs1" type="msls.EntityCollection" elementType="msls.application.ULD">
        /// Gets the uLDs1 for this uOM.
        /// </field>
        /// <field name="Dimensions" type="msls.EntityCollection" elementType="msls.application.Dimension">
        /// Gets the dimensions for this uOM.
        /// </field>
        /// <field name="Dimensions1" type="msls.EntityCollection" elementType="msls.application.Dimension">
        /// Gets the dimensions1 for this uOM.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails for this uOM.
        /// </field>
        /// <field name="ManifestDetails1" type="msls.EntityCollection" elementType="msls.application.ManifestDetail">
        /// Gets the manifestDetails1 for this uOM.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails for this uOM.
        /// </field>
        /// <field name="ManifestAWBDetails1" type="msls.EntityCollection" elementType="msls.application.ManifestAWBDetail">
        /// Gets the manifestAWBDetails1 for this uOM.
        /// </field>
        /// <field name="details" type="msls.application.UOM.Details">
        /// Gets the details for this uOM.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function UOMSystem(entitySet) {
        /// <summary>
        /// Represents the UOMSystem entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this uOMSystem.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this uOMSystem.
        /// </field>
        /// <field name="System" type="String">
        /// Gets or sets the system for this uOMSystem.
        /// </field>
        /// <field name="UOMs" type="msls.EntityCollection" elementType="msls.application.UOM">
        /// Gets the uOMs for this uOMSystem.
        /// </field>
        /// <field name="details" type="msls.application.UOMSystem.Details">
        /// Gets the details for this uOMSystem.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function UOMType(entitySet) {
        /// <summary>
        /// Represents the UOMType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this uOMType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this uOMType.
        /// </field>
        /// <field name="Type" type="String">
        /// Gets or sets the type for this uOMType.
        /// </field>
        /// <field name="UOMs" type="msls.EntityCollection" elementType="msls.application.UOM">
        /// Gets the uOMs for this uOMType.
        /// </field>
        /// <field name="details" type="msls.application.UOMType.Details">
        /// Gets the details for this uOMType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function UserProfile(entitySet) {
        /// <summary>
        /// Represents the UserProfile entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this userProfile.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this userProfile.
        /// </field>
        /// <field name="UserGuid" type="String">
        /// Gets or sets the userGuid for this userProfile.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this userProfile.
        /// </field>
        /// <field name="Title" type="String">
        /// Gets or sets the title for this userProfile.
        /// </field>
        /// <field name="FirstName" type="String">
        /// Gets or sets the firstName for this userProfile.
        /// </field>
        /// <field name="MiddleName" type="String">
        /// Gets or sets the middleName for this userProfile.
        /// </field>
        /// <field name="LastName" type="String">
        /// Gets or sets the lastName for this userProfile.
        /// </field>
        /// <field name="JobTitle" type="String">
        /// Gets or sets the jobTitle for this userProfile.
        /// </field>
        /// <field name="Phone" type="String">
        /// Gets or sets the phone for this userProfile.
        /// </field>
        /// <field name="Mobile" type="String">
        /// Gets or sets the mobile for this userProfile.
        /// </field>
        /// <field name="Fax" type="String">
        /// Gets or sets the fax for this userProfile.
        /// </field>
        /// <field name="Email" type="String">
        /// Gets or sets the email for this userProfile.
        /// </field>
        /// <field name="Photo" type="String">
        /// Gets or sets the photo for this userProfile.
        /// </field>
        /// <field name="UserStations" type="msls.EntityCollection" elementType="msls.application.UserStation">
        /// Gets the userStations for this userProfile.
        /// </field>
        /// <field name="UserWarehouses" type="msls.EntityCollection" elementType="msls.application.UserWarehous">
        /// Gets the userWarehouses for this userProfile.
        /// </field>
        /// <field name="details" type="msls.application.UserProfile.Details">
        /// Gets the details for this userProfile.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function UserStation(entitySet) {
        /// <summary>
        /// Represents the UserStation entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this userStation.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this userStation.
        /// </field>
        /// <field name="IsDefault" type="Boolean">
        /// Gets or sets the isDefault for this userStation.
        /// </field>
        /// <field name="Port" type="msls.application.Port">
        /// Gets or sets the port for this userStation.
        /// </field>
        /// <field name="UserProfile" type="msls.application.UserProfile">
        /// Gets or sets the userProfile for this userStation.
        /// </field>
        /// <field name="details" type="msls.application.UserStation.Details">
        /// Gets the details for this userStation.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function UserWarehous(entitySet) {
        /// <summary>
        /// Represents the UserWarehous entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this userWarehous.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this userWarehous.
        /// </field>
        /// <field name="IsDefault" type="Boolean">
        /// Gets or sets the isDefault for this userWarehous.
        /// </field>
        /// <field name="UserProfile" type="msls.application.UserProfile">
        /// Gets or sets the userProfile for this userWarehous.
        /// </field>
        /// <field name="Warehouse" type="msls.application.Warehouse">
        /// Gets or sets the warehouse for this userWarehous.
        /// </field>
        /// <field name="details" type="msls.application.UserWarehous.Details">
        /// Gets the details for this userWarehous.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function WarehouseLocation(entitySet) {
        /// <summary>
        /// Represents the WarehouseLocation entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this warehouseLocation.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this warehouseLocation.
        /// </field>
        /// <field name="Location" type="String">
        /// Gets or sets the location for this warehouseLocation.
        /// </field>
        /// <field name="Barcode" type="String">
        /// Gets or sets the barcode for this warehouseLocation.
        /// </field>
        /// <field name="FlightManifests" type="msls.EntityCollection" elementType="msls.application.FlightManifest">
        /// Gets the flightManifests for this warehouseLocation.
        /// </field>
        /// <field name="HWBs" type="msls.EntityCollection" elementType="msls.application.HWB">
        /// Gets the hWBs for this warehouseLocation.
        /// </field>
        /// <field name="Dispositions" type="msls.EntityCollection" elementType="msls.application.Disposition">
        /// Gets the dispositions for this warehouseLocation.
        /// </field>
        /// <field name="WarehouseLocationType" type="msls.application.WarehouseLocationType">
        /// Gets or sets the warehouseLocationType for this warehouseLocation.
        /// </field>
        /// <field name="Warehouse1" type="msls.application.Warehouse">
        /// Gets or sets the warehouse1 for this warehouseLocation.
        /// </field>
        /// <field name="WarehouseZone" type="msls.application.WarehouseZone">
        /// Gets or sets the warehouseZone for this warehouseLocation.
        /// </field>
        /// <field name="InventoryLocationItems" type="msls.EntityCollection" elementType="msls.application.InventoryLocationItem">
        /// Gets the inventoryLocationItems for this warehouseLocation.
        /// </field>
        /// <field name="details" type="msls.application.WarehouseLocation.Details">
        /// Gets the details for this warehouseLocation.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function WarehouseLocationType(entitySet) {
        /// <summary>
        /// Represents the WarehouseLocationType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this warehouseLocationType.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this warehouseLocationType.
        /// </field>
        /// <field name="LocationType" type="String">
        /// Gets or sets the locationType for this warehouseLocationType.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this warehouseLocationType.
        /// </field>
        /// <field name="BarcodeIdentifier" type="String">
        /// Gets or sets the barcodeIdentifier for this warehouseLocationType.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this warehouseLocationType.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.EntityCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this warehouseLocationType.
        /// </field>
        /// <field name="details" type="msls.application.WarehouseLocationType.Details">
        /// Gets the details for this warehouseLocationType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Warehouse(entitySet) {
        /// <summary>
        /// Represents the Warehouse entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this warehouse.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this warehouse.
        /// </field>
        /// <field name="WarehouseName" type="String">
        /// Gets or sets the warehouseName for this warehouse.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this warehouse.
        /// </field>
        /// <field name="Address" type="String">
        /// Gets or sets the address for this warehouse.
        /// </field>
        /// <field name="City" type="String">
        /// Gets or sets the city for this warehouse.
        /// </field>
        /// <field name="PostalCode" type="String">
        /// Gets or sets the postalCode for this warehouse.
        /// </field>
        /// <field name="Phone" type="String">
        /// Gets or sets the phone for this warehouse.
        /// </field>
        /// <field name="FirmCode" type="String">
        /// Gets or sets the firmCode for this warehouse.
        /// </field>
        /// <field name="Supervisor" type="String">
        /// Gets or sets the supervisor for this warehouse.
        /// </field>
        /// <field name="SupervisorCode" type="String">
        /// Gets or sets the supervisorCode for this warehouse.
        /// </field>
        /// <field name="Tasks" type="msls.EntityCollection" elementType="msls.application.Task">
        /// Gets the tasks for this warehouse.
        /// </field>
        /// <field name="Country1" type="msls.application.Country">
        /// Gets or sets the country1 for this warehouse.
        /// </field>
        /// <field name="Port" type="msls.application.Port">
        /// Gets or sets the port for this warehouse.
        /// </field>
        /// <field name="State1" type="msls.application.State">
        /// Gets or sets the state1 for this warehouse.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.EntityCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this warehouse.
        /// </field>
        /// <field name="WarehouseZones" type="msls.EntityCollection" elementType="msls.application.WarehouseZone">
        /// Gets the warehouseZones for this warehouse.
        /// </field>
        /// <field name="UserWarehouses" type="msls.EntityCollection" elementType="msls.application.UserWarehous">
        /// Gets the userWarehouses for this warehouse.
        /// </field>
        /// <field name="details" type="msls.application.Warehouse.Details">
        /// Gets the details for this warehouse.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function WarehouseZone(entitySet) {
        /// <summary>
        /// Represents the WarehouseZone entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this warehouseZone.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this warehouseZone.
        /// </field>
        /// <field name="Zone" type="String">
        /// Gets or sets the zone for this warehouseZone.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.EntityCollection" elementType="msls.application.WarehouseLocation">
        /// Gets the warehouseLocations for this warehouseZone.
        /// </field>
        /// <field name="Warehouse" type="msls.application.Warehouse">
        /// Gets or sets the warehouse for this warehouseZone.
        /// </field>
        /// <field name="details" type="msls.application.WarehouseZone.Details">
        /// Gets the details for this warehouseZone.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Condition1(entitySet) {
        /// <summary>
        /// Represents the Condition1 entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this condition1.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this condition1.
        /// </field>
        /// <field name="Condition2" type="String">
        /// Gets or sets the condition2 for this condition1.
        /// </field>
        /// <field name="IsDamage" type="Boolean">
        /// Gets or sets the isDamage for this condition1.
        /// </field>
        /// <field name="EntityConditions" type="msls.EntityCollection" elementType="msls.application.EntityCondition1">
        /// Gets the entityConditions for this condition1.
        /// </field>
        /// <field name="ImagePath" type="String">
        /// Gets or sets the imagePath for this condition1.
        /// </field>
        /// <field name="details" type="msls.application.Condition1.Details">
        /// Gets the details for this condition1.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function EntityCondition1(entitySet) {
        /// <summary>
        /// Represents the EntityCondition1 entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this entityCondition1.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this entityCondition1.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this entityCondition1.
        /// </field>
        /// <field name="Count" type="Number">
        /// Gets or sets the count for this entityCondition1.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this entityCondition1.
        /// </field>
        /// <field name="Timestamp" type="Date">
        /// Gets or sets the timestamp for this entityCondition1.
        /// </field>
        /// <field name="TaskId" type="String">
        /// Gets or sets the taskId for this entityCondition1.
        /// </field>
        /// <field name="TaskTypeId" type="Number">
        /// Gets or sets the taskTypeId for this entityCondition1.
        /// </field>
        /// <field name="Condition" type="msls.application.Condition1">
        /// Gets or sets the condition for this entityCondition1.
        /// </field>
        /// <field name="EntityType" type="msls.application.EntityType1">
        /// Gets or sets the entityType for this entityCondition1.
        /// </field>
        /// <field name="EntitySnapshots" type="msls.EntityCollection" elementType="msls.application.EntitySnapshot1">
        /// Gets the entitySnapshots for this entityCondition1.
        /// </field>
        /// <field name="details" type="msls.application.EntityCondition1.Details">
        /// Gets the details for this entityCondition1.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function EntitySnapshot1(entitySet) {
        /// <summary>
        /// Represents the EntitySnapshot1 entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this entitySnapshot1.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this entitySnapshot1.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this entitySnapshot1.
        /// </field>
        /// <field name="Timestamp" type="Date">
        /// Gets or sets the timestamp for this entitySnapshot1.
        /// </field>
        /// <field name="EntityCondition" type="msls.application.EntityCondition1">
        /// Gets or sets the entityCondition for this entitySnapshot1.
        /// </field>
        /// <field name="Snapshot" type="msls.application.Snapshot1">
        /// Gets or sets the snapshot for this entitySnapshot1.
        /// </field>
        /// <field name="details" type="msls.application.EntitySnapshot1.Details">
        /// Gets the details for this entitySnapshot1.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function EntityType1(entitySet) {
        /// <summary>
        /// Represents the EntityType1 entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this entityType1.
        /// </param>
        /// <field name="Id" type="Number">
        /// Gets or sets the id for this entityType1.
        /// </field>
        /// <field name="EntityName" type="String">
        /// Gets or sets the entityName for this entityType1.
        /// </field>
        /// <field name="EntityConditions" type="msls.EntityCollection" elementType="msls.application.EntityCondition1">
        /// Gets the entityConditions for this entityType1.
        /// </field>
        /// <field name="Snapshots" type="msls.EntityCollection" elementType="msls.application.Snapshot1">
        /// Gets the snapshots for this entityType1.
        /// </field>
        /// <field name="details" type="msls.application.EntityType1.Details">
        /// Gets the details for this entityType1.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function Snapshot1(entitySet) {
        /// <summary>
        /// Represents the Snapshot1 entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this snapshot1.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this snapshot1.
        /// </field>
        /// <field name="Count" type="Number">
        /// Gets or sets the count for this snapshot1.
        /// </field>
        /// <field name="UserName" type="String">
        /// Gets or sets the userName for this snapshot1.
        /// </field>
        /// <field name="Timestamp" type="Date">
        /// Gets or sets the timestamp for this snapshot1.
        /// </field>
        /// <field name="EntityId" type="String">
        /// Gets or sets the entityId for this snapshot1.
        /// </field>
        /// <field name="DisplayText" type="String">
        /// Gets or sets the displayText for this snapshot1.
        /// </field>
        /// <field name="EntitySnapshots" type="msls.EntityCollection" elementType="msls.application.EntitySnapshot1">
        /// Gets the entitySnapshots for this snapshot1.
        /// </field>
        /// <field name="EntityType" type="msls.application.EntityType1">
        /// Gets or sets the entityType for this snapshot1.
        /// </field>
        /// <field name="ImagePath" type="String">
        /// Gets or sets the imagePath for this snapshot1.
        /// </field>
        /// <field name="details" type="msls.application.Snapshot1.Details">
        /// Gets the details for this snapshot1.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function CIMPMessageLog(entitySet) {
        /// <summary>
        /// Represents the CIMPMessageLog entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this cIMPMessageLog.
        /// </param>
        /// <field name="Id" type="String">
        /// Gets or sets the id for this cIMPMessageLog.
        /// </field>
        /// <field name="Timestamp" type="Date">
        /// Gets or sets the timestamp for this cIMPMessageLog.
        /// </field>
        /// <field name="FileName" type="String">
        /// Gets or sets the fileName for this cIMPMessageLog.
        /// </field>
        /// <field name="MessageType" type="String">
        /// Gets or sets the messageType for this cIMPMessageLog.
        /// </field>
        /// <field name="MailboxName" type="String">
        /// Gets or sets the mailboxName for this cIMPMessageLog.
        /// </field>
        /// <field name="RawMessage" type="String">
        /// Gets or sets the rawMessage for this cIMPMessageLog.
        /// </field>
        /// <field name="ProcessingStatus" type="Boolean">
        /// Gets or sets the processingStatus for this cIMPMessageLog.
        /// </field>
        /// <field name="ErrorMessage" type="String">
        /// Gets or sets the errorMessage for this cIMPMessageLog.
        /// </field>
        /// <field name="DownloadedTime" type="Date">
        /// Gets or sets the downloadedTime for this cIMPMessageLog.
        /// </field>
        /// <field name="SentTime" type="Date">
        /// Gets or sets the sentTime for this cIMPMessageLog.
        /// </field>
        /// <field name="details" type="msls.application.CIMPMessageLog.Details">
        /// Gets the details for this cIMPMessageLog.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_AuditTrail(entitySet) {
        /// <summary>
        /// Represents the HostPlus_AuditTrail entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_AuditTrail.
        /// </param>
        /// <field name="RecId" type="String">
        /// Gets or sets the recId for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="TransactionId" type="String">
        /// Gets or sets the transactionId for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="DBUser" type="String">
        /// Gets or sets the dBUser for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="AppName" type="String">
        /// Gets or sets the appName for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="Operation" type="String">
        /// Gets or sets the operation for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="TableName" type="String">
        /// Gets or sets the tableName for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="Before" type="String">
        /// Gets or sets the before for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="After" type="String">
        /// Gets or sets the after for this hostPlus_AuditTrail.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_AuditTrail.Details">
        /// Gets the details for this hostPlus_AuditTrail.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_Email(entitySet) {
        /// <summary>
        /// Represents the HostPlus_Email entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_Email.
        /// </param>
        /// <field name="RecId" type="String">
        /// Gets or sets the recId for this hostPlus_Email.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_Email.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this hostPlus_Email.
        /// </field>
        /// <field name="Status" type="Number">
        /// Gets or sets the status for this hostPlus_Email.
        /// </field>
        /// <field name="MailForm" type="String">
        /// Gets or sets the mailForm for this hostPlus_Email.
        /// </field>
        /// <field name="MailTo" type="String">
        /// Gets or sets the mailTo for this hostPlus_Email.
        /// </field>
        /// <field name="MailCc" type="String">
        /// Gets or sets the mailCc for this hostPlus_Email.
        /// </field>
        /// <field name="MailBcc" type="String">
        /// Gets or sets the mailBcc for this hostPlus_Email.
        /// </field>
        /// <field name="Body" type="String">
        /// Gets or sets the body for this hostPlus_Email.
        /// </field>
        /// <field name="Subject" type="String">
        /// Gets or sets the subject for this hostPlus_Email.
        /// </field>
        /// <field name="Gateway" type="String">
        /// Gets or sets the gateway for this hostPlus_Email.
        /// </field>
        /// <field name="MailDate" type="Date">
        /// Gets or sets the mailDate for this hostPlus_Email.
        /// </field>
        /// <field name="Attachment" type="String">
        /// Gets or sets the attachment for this hostPlus_Email.
        /// </field>
        /// <field name="TransmitionError" type="String">
        /// Gets or sets the transmitionError for this hostPlus_Email.
        /// </field>
        /// <field name="IsAutoEmail" type="Number">
        /// Gets or sets the isAutoEmail for this hostPlus_Email.
        /// </field>
        /// <field name="Tries" type="Number">
        /// Gets or sets the tries for this hostPlus_Email.
        /// </field>
        /// <field name="IsZip" type="Number">
        /// Gets or sets the isZip for this hostPlus_Email.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_Email.Details">
        /// Gets the details for this hostPlus_Email.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_Location(entitySet) {
        /// <summary>
        /// Represents the HostPlus_Location entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_Location.
        /// </param>
        /// <field name="RecId" type="Number">
        /// Gets or sets the recId for this hostPlus_Location.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_Location.
        /// </field>
        /// <field name="Location" type="String">
        /// Gets or sets the location for this hostPlus_Location.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_Location.
        /// </field>
        /// <field name="Connection" type="String">
        /// Gets or sets the connection for this hostPlus_Location.
        /// </field>
        /// <field name="TimeDifference" type="Number">
        /// Gets or sets the timeDifference for this hostPlus_Location.
        /// </field>
        /// <field name="WebConnectionName" type="String">
        /// Gets or sets the webConnectionName for this hostPlus_Location.
        /// </field>
        /// <field name="EntityID" type="Number">
        /// Gets or sets the entityID for this hostPlus_Location.
        /// </field>
        /// <field name="DBName" type="String">
        /// Gets or sets the dBName for this hostPlus_Location.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_Location.Details">
        /// Gets the details for this hostPlus_Location.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_Log(entitySet) {
        /// <summary>
        /// Represents the HostPlus_Log entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_Log.
        /// </param>
        /// <field name="RecId" type="Number">
        /// Gets or sets the recId for this hostPlus_Log.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_Log.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_Log.
        /// </field>
        /// <field name="StackTrace" type="String">
        /// Gets or sets the stackTrace for this hostPlus_Log.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_Log.Details">
        /// Gets the details for this hostPlus_Log.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_Parameter(entitySet) {
        /// <summary>
        /// Represents the HostPlus_Parameter entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_Parameter.
        /// </param>
        /// <field name="RecId" type="String">
        /// Gets or sets the recId for this hostPlus_Parameter.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_Parameter.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this hostPlus_Parameter.
        /// </field>
        /// <field name="DefaultValue" type="String">
        /// Gets or sets the defaultValue for this hostPlus_Parameter.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_Parameter.
        /// </field>
        /// <field name="AllowBlank" type="Boolean">
        /// Gets or sets the allowBlank for this hostPlus_Parameter.
        /// </field>
        /// <field name="DataType" type="String">
        /// Gets or sets the dataType for this hostPlus_Parameter.
        /// </field>
        /// <field name="TaskId" type="String">
        /// Gets or sets the taskId for this hostPlus_Parameter.
        /// </field>
        /// <field name="SessionTypeId" type="String">
        /// Gets or sets the sessionTypeId for this hostPlus_Parameter.
        /// </field>
        /// <field name="DataEnumeration" type="String">
        /// Gets or sets the dataEnumeration for this hostPlus_Parameter.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_Parameter.Details">
        /// Gets the details for this hostPlus_Parameter.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_SessionParameter(entitySet) {
        /// <summary>
        /// Represents the HostPlus_SessionParameter entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_SessionParameter.
        /// </param>
        /// <field name="RecId" type="String">
        /// Gets or sets the recId for this hostPlus_SessionParameter.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_SessionParameter.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this hostPlus_SessionParameter.
        /// </field>
        /// <field name="SessionId" type="String">
        /// Gets or sets the sessionId for this hostPlus_SessionParameter.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this hostPlus_SessionParameter.
        /// </field>
        /// <field name="Value" type="String">
        /// Gets or sets the value for this hostPlus_SessionParameter.
        /// </field>
        /// <field name="ParameterId" type="String">
        /// Gets or sets the parameterId for this hostPlus_SessionParameter.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_SessionParameter.Details">
        /// Gets the details for this hostPlus_SessionParameter.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_Session(entitySet) {
        /// <summary>
        /// Represents the HostPlus_Session entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_Session.
        /// </param>
        /// <field name="RecId" type="Number">
        /// Gets or sets the recId for this hostPlus_Session.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_Session.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this hostPlus_Session.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this hostPlus_Session.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_Session.
        /// </field>
        /// <field name="SessionTypeId" type="Number">
        /// Gets or sets the sessionTypeId for this hostPlus_Session.
        /// </field>
        /// <field name="IsActive" type="Boolean">
        /// Gets or sets the isActive for this hostPlus_Session.
        /// </field>
        /// <field name="Server" type="String">
        /// Gets or sets the server for this hostPlus_Session.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_Session.Details">
        /// Gets the details for this hostPlus_Session.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_SessionTaskParameter(entitySet) {
        /// <summary>
        /// Represents the HostPlus_SessionTaskParameter entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_SessionTaskParameter.
        /// </param>
        /// <field name="RecId" type="String">
        /// Gets or sets the recId for this hostPlus_SessionTaskParameter.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_SessionTaskParameter.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this hostPlus_SessionTaskParameter.
        /// </field>
        /// <field name="SessionTaskId" type="String">
        /// Gets or sets the sessionTaskId for this hostPlus_SessionTaskParameter.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this hostPlus_SessionTaskParameter.
        /// </field>
        /// <field name="Value" type="String">
        /// Gets or sets the value for this hostPlus_SessionTaskParameter.
        /// </field>
        /// <field name="ParameterId" type="String">
        /// Gets or sets the parameterId for this hostPlus_SessionTaskParameter.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_SessionTaskParameter.Details">
        /// Gets the details for this hostPlus_SessionTaskParameter.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_SessionTask(entitySet) {
        /// <summary>
        /// Represents the HostPlus_SessionTask entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_SessionTask.
        /// </param>
        /// <field name="RecID" type="Number">
        /// Gets or sets the recID for this hostPlus_SessionTask.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_SessionTask.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this hostPlus_SessionTask.
        /// </field>
        /// <field name="SessionId" type="Number">
        /// Gets or sets the sessionId for this hostPlus_SessionTask.
        /// </field>
        /// <field name="TaskId" type="Number">
        /// Gets or sets the taskId for this hostPlus_SessionTask.
        /// </field>
        /// <field name="LastDate" type="Date">
        /// Gets or sets the lastDate for this hostPlus_SessionTask.
        /// </field>
        /// <field name="NextDate" type="Date">
        /// Gets or sets the nextDate for this hostPlus_SessionTask.
        /// </field>
        /// <field name="Timer" type="String">
        /// Gets or sets the timer for this hostPlus_SessionTask.
        /// </field>
        /// <field name="TimerWeekDays" type="String">
        /// Gets or sets the timerWeekDays for this hostPlus_SessionTask.
        /// </field>
        /// <field name="TimerType" type="String">
        /// Gets or sets the timerType for this hostPlus_SessionTask.
        /// </field>
        /// <field name="IsActive" type="Boolean">
        /// Gets or sets the isActive for this hostPlus_SessionTask.
        /// </field>
        /// <field name="Status" type="String">
        /// Gets or sets the status for this hostPlus_SessionTask.
        /// </field>
        /// <field name="Trace" type="String">
        /// Gets or sets the trace for this hostPlus_SessionTask.
        /// </field>
        /// <field name="IsTraceable" type="Boolean">
        /// Gets or sets the isTraceable for this hostPlus_SessionTask.
        /// </field>
        /// <field name="Progress" type="String">
        /// Gets or sets the progress for this hostPlus_SessionTask.
        /// </field>
        /// <field name="c_Error" type="String">
        /// Gets or sets the c_Error for this hostPlus_SessionTask.
        /// </field>
        /// <field name="LocationId" type="Number">
        /// Gets or sets the locationId for this hostPlus_SessionTask.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_SessionTask.
        /// </field>
        /// <field name="Retries" type="Number">
        /// Gets or sets the retries for this hostPlus_SessionTask.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_SessionTask.Details">
        /// Gets the details for this hostPlus_SessionTask.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_SessionType(entitySet) {
        /// <summary>
        /// Represents the HostPlus_SessionType entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_SessionType.
        /// </param>
        /// <field name="RecId" type="Number">
        /// Gets or sets the recId for this hostPlus_SessionType.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_SessionType.
        /// </field>
        /// <field name="UserId" type="String">
        /// Gets or sets the userId for this hostPlus_SessionType.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this hostPlus_SessionType.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_SessionType.
        /// </field>
        /// <field name="AuthRequired" type="Boolean">
        /// Gets or sets the authRequired for this hostPlus_SessionType.
        /// </field>
        /// <field name="GroupName" type="String">
        /// Gets or sets the groupName for this hostPlus_SessionType.
        /// </field>
        /// <field name="Icon" type="String">
        /// Gets or sets the icon for this hostPlus_SessionType.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_SessionType.Details">
        /// Gets the details for this hostPlus_SessionType.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_Task(entitySet) {
        /// <summary>
        /// Represents the HostPlus_Task entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_Task.
        /// </param>
        /// <field name="RecId" type="Number">
        /// Gets or sets the recId for this hostPlus_Task.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_Task.
        /// </field>
        /// <field name="SessionTypeId" type="Number">
        /// Gets or sets the sessionTypeId for this hostPlus_Task.
        /// </field>
        /// <field name="Name" type="String">
        /// Gets or sets the name for this hostPlus_Task.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_Task.
        /// </field>
        /// <field name="Icon" type="String">
        /// Gets or sets the icon for this hostPlus_Task.
        /// </field>
        /// <field name="RetriesOnFail" type="Number">
        /// Gets or sets the retriesOnFail for this hostPlus_Task.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_Task.Details">
        /// Gets the details for this hostPlus_Task.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function HostPlus_TimestampsCode(entitySet) {
        /// <summary>
        /// Represents the HostPlus_TimestampsCode entity type.
        /// </summary>
        /// <param name="entitySet" type="msls.EntitySet" optional="true">
        /// The entity set that should contain this hostPlus_TimestampsCode.
        /// </param>
        /// <field name="RecID" type="Number">
        /// Gets or sets the recID for this hostPlus_TimestampsCode.
        /// </field>
        /// <field name="RecDate" type="Date">
        /// Gets or sets the recDate for this hostPlus_TimestampsCode.
        /// </field>
        /// <field name="Code" type="String">
        /// Gets or sets the code for this hostPlus_TimestampsCode.
        /// </field>
        /// <field name="Description" type="String">
        /// Gets or sets the description for this hostPlus_TimestampsCode.
        /// </field>
        /// <field name="details" type="msls.application.HostPlus_TimestampsCode.Details">
        /// Gets the details for this hostPlus_TimestampsCode.
        /// </field>
        $Entity.call(this, entitySet);
    }

    function WFSDBData(dataWorkspace) {
        /// <summary>
        /// Represents the WFSDBData data service.
        /// </summary>
        /// <param name="dataWorkspace" type="msls.DataWorkspace">
        /// The data workspace that created this data service.
        /// </param>
        /// <field name="Agents" type="msls.EntitySet">
        /// Gets the Agents entity set.
        /// </field>
        /// <field name="AWBs" type="msls.EntitySet">
        /// Gets the AWBs entity set.
        /// </field>
        /// <field name="AWBs_HWBs" type="msls.EntitySet">
        /// Gets the AWBs_HWBs entity set.
        /// </field>
        /// <field name="CarrierDrivers" type="msls.EntitySet">
        /// Gets the CarrierDrivers entity set.
        /// </field>
        /// <field name="CarrierRoutes" type="msls.EntitySet">
        /// Gets the CarrierRoutes entity set.
        /// </field>
        /// <field name="Carriers" type="msls.EntitySet">
        /// Gets the Carriers entity set.
        /// </field>
        /// <field name="ChargeDeclarations" type="msls.EntitySet">
        /// Gets the ChargeDeclarations entity set.
        /// </field>
        /// <field name="ChargeTypes" type="msls.EntitySet">
        /// Gets the ChargeTypes entity set.
        /// </field>
        /// <field name="Conditions" type="msls.EntitySet">
        /// Gets the Conditions entity set.
        /// </field>
        /// <field name="ConsignmentCodes" type="msls.EntitySet">
        /// Gets the ConsignmentCodes entity set.
        /// </field>
        /// <field name="Contacts" type="msls.EntitySet">
        /// Gets the Contacts entity set.
        /// </field>
        /// <field name="Countries" type="msls.EntitySet">
        /// Gets the Countries entity set.
        /// </field>
        /// <field name="Currencies" type="msls.EntitySet">
        /// Gets the Currencies entity set.
        /// </field>
        /// <field name="Customers" type="msls.EntitySet">
        /// Gets the Customers entity set.
        /// </field>
        /// <field name="CustomsInfoIdentifiers" type="msls.EntitySet">
        /// Gets the CustomsInfoIdentifiers entity set.
        /// </field>
        /// <field name="CustomsNotifications" type="msls.EntitySet">
        /// Gets the CustomsNotifications entity set.
        /// </field>
        /// <field name="DensityGroups" type="msls.EntitySet">
        /// Gets the DensityGroups entity set.
        /// </field>
        /// <field name="DescriptionOfGoods" type="msls.EntitySet">
        /// Gets the DescriptionOfGoods entity set.
        /// </field>
        /// <field name="Dimensions" type="msls.EntitySet">
        /// Gets the Dimensions entity set.
        /// </field>
        /// <field name="DischargeCharges" type="msls.EntitySet">
        /// Gets the DischargeCharges entity set.
        /// </field>
        /// <field name="DischargeCheckInLists" type="msls.EntitySet">
        /// Gets the DischargeCheckInLists entity set.
        /// </field>
        /// <field name="DischargeDetails" type="msls.EntitySet">
        /// Gets the DischargeDetails entity set.
        /// </field>
        /// <field name="DischargePayments" type="msls.EntitySet">
        /// Gets the DischargePayments entity set.
        /// </field>
        /// <field name="Discharges" type="msls.EntitySet">
        /// Gets the Discharges entity set.
        /// </field>
        /// <field name="Dispositions" type="msls.EntitySet">
        /// Gets the Dispositions entity set.
        /// </field>
        /// <field name="Drivers" type="msls.EntitySet">
        /// Gets the Drivers entity set.
        /// </field>
        /// <field name="EntityTypes" type="msls.EntitySet">
        /// Gets the EntityTypes entity set.
        /// </field>
        /// <field name="FlightManifests" type="msls.EntitySet">
        /// Gets the FlightManifests entity set.
        /// </field>
        /// <field name="ForkliftDetails" type="msls.EntitySet">
        /// Gets the ForkliftDetails entity set.
        /// </field>
        /// <field name="ForkliftViews" type="msls.EntitySet">
        /// Gets the ForkliftViews entity set.
        /// </field>
        /// <field name="FSNs" type="msls.EntitySet">
        /// Gets the FSNs entity set.
        /// </field>
        /// <field name="HostPlus_OutboundQueues" type="msls.EntitySet">
        /// Gets the HostPlus_OutboundQueues entity set.
        /// </field>
        /// <field name="HWBHTSSet" type="msls.EntitySet">
        /// Gets the HWBHTSSet entity set.
        /// </field>
        /// <field name="HWBs" type="msls.EntitySet">
        /// Gets the HWBs entity set.
        /// </field>
        /// <field name="ImportExportCodes" type="msls.EntitySet">
        /// Gets the ImportExportCodes entity set.
        /// </field>
        /// <field name="InventoryLocationItems" type="msls.EntitySet">
        /// Gets the InventoryLocationItems entity set.
        /// </field>
        /// <field name="InventoryViews" type="msls.EntitySet">
        /// Gets the InventoryViews entity set.
        /// </field>
        /// <field name="LegSegments" type="msls.EntitySet">
        /// Gets the LegSegments entity set.
        /// </field>
        /// <field name="LineIdentifiers" type="msls.EntitySet">
        /// Gets the LineIdentifiers entity set.
        /// </field>
        /// <field name="LoadingIndicators" type="msls.EntitySet">
        /// Gets the LoadingIndicators entity set.
        /// </field>
        /// <field name="ManifestAWBDetails" type="msls.EntitySet">
        /// Gets the ManifestAWBDetails entity set.
        /// </field>
        /// <field name="ManifestDetails" type="msls.EntitySet">
        /// Gets the ManifestDetails entity set.
        /// </field>
        /// <field name="MOTs" type="msls.EntitySet">
        /// Gets the MOTs entity set.
        /// </field>
        /// <field name="MovementPriorityCodes" type="msls.EntitySet">
        /// Gets the MovementPriorityCodes entity set.
        /// </field>
        /// <field name="OCIs" type="msls.EntitySet">
        /// Gets the OCIs entity set.
        /// </field>
        /// <field name="ParticipantIdentifiers" type="msls.EntitySet">
        /// Gets the ParticipantIdentifiers entity set.
        /// </field>
        /// <field name="PaymentTypes" type="msls.EntitySet">
        /// Gets the PaymentTypes entity set.
        /// </field>
        /// <field name="Ports" type="msls.EntitySet">
        /// Gets the Ports entity set.
        /// </field>
        /// <field name="PutAwayLists" type="msls.EntitySet">
        /// Gets the PutAwayLists entity set.
        /// </field>
        /// <field name="PutedAwayItems" type="msls.EntitySet">
        /// Gets the PutedAwayItems entity set.
        /// </field>
        /// <field name="ReceiverAwbManifestLists" type="msls.EntitySet">
        /// Gets the ReceiverAwbManifestLists entity set.
        /// </field>
        /// <field name="ReceiverFlightViews" type="msls.EntitySet">
        /// Gets the ReceiverFlightViews entity set.
        /// </field>
        /// <field name="ReceiverUldManifestLists" type="msls.EntitySet">
        /// Gets the ReceiverUldManifestLists entity set.
        /// </field>
        /// <field name="ShipmentUnitTypes" type="msls.EntitySet">
        /// Gets the ShipmentUnitTypes entity set.
        /// </field>
        /// <field name="SnapshotTasksViews" type="msls.EntitySet">
        /// Gets the SnapshotTasksViews entity set.
        /// </field>
        /// <field name="SpecialHandlingCodes" type="msls.EntitySet">
        /// Gets the SpecialHandlingCodes entity set.
        /// </field>
        /// <field name="SpecialHandlings" type="msls.EntitySet">
        /// Gets the SpecialHandlings entity set.
        /// </field>
        /// <field name="States" type="msls.EntitySet">
        /// Gets the States entity set.
        /// </field>
        /// <field name="Statuses" type="msls.EntitySet">
        /// Gets the Statuses entity set.
        /// </field>
        /// <field name="TableLists" type="msls.EntitySet">
        /// Gets the TableLists entity set.
        /// </field>
        /// <field name="TaskFlightReceivers" type="msls.EntitySet">
        /// Gets the TaskFlightReceivers entity set.
        /// </field>
        /// <field name="Tasks" type="msls.EntitySet">
        /// Gets the Tasks entity set.
        /// </field>
        /// <field name="TaskTypes" type="msls.EntitySet">
        /// Gets the TaskTypes entity set.
        /// </field>
        /// <field name="TrackTraceViews" type="msls.EntitySet">
        /// Gets the TrackTraceViews entity set.
        /// </field>
        /// <field name="TransactionActions" type="msls.EntitySet">
        /// Gets the TransactionActions entity set.
        /// </field>
        /// <field name="Transactions" type="msls.EntitySet">
        /// Gets the Transactions entity set.
        /// </field>
        /// <field name="ULDs" type="msls.EntitySet">
        /// Gets the ULDs entity set.
        /// </field>
        /// <field name="ULDVolumeCodes" type="msls.EntitySet">
        /// Gets the ULDVolumeCodes entity set.
        /// </field>
        /// <field name="UnloadingPorts" type="msls.EntitySet">
        /// Gets the UnloadingPorts entity set.
        /// </field>
        /// <field name="UOMs" type="msls.EntitySet">
        /// Gets the UOMs entity set.
        /// </field>
        /// <field name="UOMSystems" type="msls.EntitySet">
        /// Gets the UOMSystems entity set.
        /// </field>
        /// <field name="UOMTypes" type="msls.EntitySet">
        /// Gets the UOMTypes entity set.
        /// </field>
        /// <field name="UserProfiles" type="msls.EntitySet">
        /// Gets the UserProfiles entity set.
        /// </field>
        /// <field name="UserStations" type="msls.EntitySet">
        /// Gets the UserStations entity set.
        /// </field>
        /// <field name="UserWarehouses" type="msls.EntitySet">
        /// Gets the UserWarehouses entity set.
        /// </field>
        /// <field name="WarehouseLocations" type="msls.EntitySet">
        /// Gets the WarehouseLocations entity set.
        /// </field>
        /// <field name="WarehouseLocationTypes" type="msls.EntitySet">
        /// Gets the WarehouseLocationTypes entity set.
        /// </field>
        /// <field name="Warehouses" type="msls.EntitySet">
        /// Gets the Warehouses entity set.
        /// </field>
        /// <field name="WarehouseZones" type="msls.EntitySet">
        /// Gets the WarehouseZones entity set.
        /// </field>
        /// <field name="details" type="msls.application.WFSDBData.Details">
        /// Gets the details for this data service.
        /// </field>
        $DataService.call(this, dataWorkspace);
    };

    function SnapshotDBData(dataWorkspace) {
        /// <summary>
        /// Represents the SnapshotDBData data service.
        /// </summary>
        /// <param name="dataWorkspace" type="msls.DataWorkspace">
        /// The data workspace that created this data service.
        /// </param>
        /// <field name="Conditions" type="msls.EntitySet">
        /// Gets the Conditions entity set.
        /// </field>
        /// <field name="EntityConditions" type="msls.EntitySet">
        /// Gets the EntityConditions entity set.
        /// </field>
        /// <field name="EntitySnapshots" type="msls.EntitySet">
        /// Gets the EntitySnapshots entity set.
        /// </field>
        /// <field name="EntityTypes" type="msls.EntitySet">
        /// Gets the EntityTypes entity set.
        /// </field>
        /// <field name="Snapshots" type="msls.EntitySet">
        /// Gets the Snapshots entity set.
        /// </field>
        /// <field name="details" type="msls.application.SnapshotDBData.Details">
        /// Gets the details for this data service.
        /// </field>
        $DataService.call(this, dataWorkspace);
    };

    function MCHCoreDBData(dataWorkspace) {
        /// <summary>
        /// Represents the MCHCoreDBData data service.
        /// </summary>
        /// <param name="dataWorkspace" type="msls.DataWorkspace">
        /// The data workspace that created this data service.
        /// </param>
        /// <field name="CIMPMessageLogs" type="msls.EntitySet">
        /// Gets the CIMPMessageLogs entity set.
        /// </field>
        /// <field name="HostPlus_AuditTrails" type="msls.EntitySet">
        /// Gets the HostPlus_AuditTrails entity set.
        /// </field>
        /// <field name="HostPlus_Emails" type="msls.EntitySet">
        /// Gets the HostPlus_Emails entity set.
        /// </field>
        /// <field name="HostPlus_Locations" type="msls.EntitySet">
        /// Gets the HostPlus_Locations entity set.
        /// </field>
        /// <field name="HostPlus_Logs" type="msls.EntitySet">
        /// Gets the HostPlus_Logs entity set.
        /// </field>
        /// <field name="HostPlus_Parameters" type="msls.EntitySet">
        /// Gets the HostPlus_Parameters entity set.
        /// </field>
        /// <field name="HostPlus_SessionParameters" type="msls.EntitySet">
        /// Gets the HostPlus_SessionParameters entity set.
        /// </field>
        /// <field name="HostPlus_Sessions" type="msls.EntitySet">
        /// Gets the HostPlus_Sessions entity set.
        /// </field>
        /// <field name="HostPlus_SessionTaskParameters" type="msls.EntitySet">
        /// Gets the HostPlus_SessionTaskParameters entity set.
        /// </field>
        /// <field name="HostPlus_SessionTasks" type="msls.EntitySet">
        /// Gets the HostPlus_SessionTasks entity set.
        /// </field>
        /// <field name="HostPlus_SessionTypes" type="msls.EntitySet">
        /// Gets the HostPlus_SessionTypes entity set.
        /// </field>
        /// <field name="HostPlus_Tasks" type="msls.EntitySet">
        /// Gets the HostPlus_Tasks entity set.
        /// </field>
        /// <field name="HostPlus_TimestampsCodes" type="msls.EntitySet">
        /// Gets the HostPlus_TimestampsCodes entity set.
        /// </field>
        /// <field name="details" type="msls.application.MCHCoreDBData.Details">
        /// Gets the details for this data service.
        /// </field>
        $DataService.call(this, dataWorkspace);
    };
    function DataWorkspace() {
        /// <summary>
        /// Represents the data workspace.
        /// </summary>
        /// <field name="WFSDBData" type="msls.application.WFSDBData">
        /// Gets the WFSDBData data service.
        /// </field>
        /// <field name="SnapshotDBData" type="msls.application.SnapshotDBData">
        /// Gets the SnapshotDBData data service.
        /// </field>
        /// <field name="MCHCoreDBData" type="msls.application.MCHCoreDBData">
        /// Gets the MCHCoreDBData data service.
        /// </field>
        /// <field name="details" type="msls.application.DataWorkspace.Details">
        /// Gets the details for this data workspace.
        /// </field>
        $DataWorkspace.call(this);
    };

    msls._addToNamespace("msls.application", {

        Agent: $defineEntity(Agent, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "AccountNumber", type: String },
            { name: "NumericCode", type: String },
            { name: "CASSAddress", type: String },
            { name: "AWBs", kind: "collection", elementType: AWB }
        ]),

        AWB: $defineEntity(AWB, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "AWBSerialNumber", type: String },
            { name: "TotalPieces", type: Number },
            { name: "TotalWeight", type: Number },
            { name: "TotalVolume", type: Number },
            { name: "DescriptionOfGoods", type: String },
            { name: "HBCount", type: Number },
            { name: "LoadType", type: String },
            { name: "ULDCount", type: Number },
            { name: "ReceivedPieces", type: Number },
            { name: "PutAwayPieces", type: Number },
            { name: "PercentReceived", type: Number },
            { name: "PercentPutAway", type: Number },
            { name: "ForkliftPieces", type: Number },
            { name: "AvailablePieces", type: Number },
            { name: "ReleasedPieces", type: Number },
            { name: "OtherServiceInfo1", type: String },
            { name: "OtherServiceInfo2", type: String },
            { name: "CustomsOrigin", type: String },
            { name: "CustomsReference", type: String },
            { name: "DensityIndicator", type: Boolean },
            { name: "SpecialServiceRequest1", type: String },
            { name: "SpecialServiceRequest2", type: String },
            { name: "SpecialServiceRequest3", type: String },
            { name: "IsTransfer", type: Number },
            { name: "RecDate", type: Date },
            { name: "StatusTimestamp", type: Date },
            { name: "Carrier", kind: "reference", type: Carrier },
            { name: "Shipper", kind: "reference", type: Customer },
            { name: "Consignee", kind: "reference", type: Customer },
            { name: "Status", kind: "reference", type: Status },
            { name: "DischargeDetails", kind: "collection", elementType: DischargeDetail },
            { name: "ForkliftDetails", kind: "collection", elementType: ForkliftDetail },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail },
            { name: "ManifestAWBDetails", kind: "collection", elementType: ManifestAWBDetail },
            { name: "Dispositions", kind: "collection", elementType: Disposition },
            { name: "CustomsNotifications", kind: "collection", elementType: CustomsNotification },
            { name: "Agent", kind: "reference", type: Agent },
            { name: "MovementPriorityCode", kind: "reference", type: MovementPriorityCode },
            { name: "AwbOrigin", kind: "reference", type: Port },
            { name: "AwbDestination", kind: "reference", type: Port },
            { name: "WgtUom", kind: "reference", type: UOM },
            { name: "VolUom", kind: "reference", type: UOM },
            { name: "AWBs_HWBs", kind: "collection", elementType: AWBs_HWB },
            { name: "ChargeDeclarations", kind: "collection", elementType: ChargeDeclaration },
            { name: "Dimensions", kind: "collection", elementType: Dimension },
            { name: "OCIs", kind: "collection", elementType: OCI },
            { name: "SpecialHandlings", kind: "collection", elementType: SpecialHandling },
            { name: "HostPlus_OutboundQueues", kind: "collection", elementType: HostPlus_OutboundQueue }
        ]),

        AWBs_HWB: $defineEntity(AWBs_HWB, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "HWB", kind: "reference", type: HWB }
        ]),

        CarrierDriver: $defineEntity(CarrierDriver, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "FirstName", type: String },
            { name: "LastName", type: String },
            { name: "DriverPhotoPath", type: String },
            { name: "LicenseNumber", type: String },
            { name: "LicenseImagePath", type: String },
            { name: "LicenseExpiry", type: Date },
            { name: "Carrier", kind: "reference", type: Carrier },
            { name: "Discharges", kind: "collection", elementType: Discharge }
        ]),

        CarrierRoute: $defineEntity(CarrierRoute, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "FlightNumber", type: String },
            { name: "DepartureTime", type: Object },
            { name: "ArrivalTime", type: Object },
            { name: "Offset", type: Number },
            { name: "Carrier", kind: "reference", type: Carrier },
            { name: "Port", kind: "reference", type: Port },
            { name: "Port1", kind: "reference", type: Port },
            { name: "Port2", kind: "reference", type: Port }
        ]),

        Carrier: $defineEntity(Carrier, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "CarrierName", type: String },
            { name: "CarrierCode", type: String },
            { name: "Carrier3Code", type: String },
            { name: "AWBs", kind: "collection", elementType: AWB },
            { name: "Drivers", kind: "collection", elementType: Driver },
            { name: "FlightManifests", kind: "collection", elementType: FlightManifest },
            { name: "ULDs", kind: "collection", elementType: ULD },
            { name: "MOT", kind: "reference", type: MOT },
            { name: "LegSegments", kind: "collection", elementType: LegSegment },
            { name: "CarrierRoutes", kind: "collection", elementType: CarrierRoute },
            { name: "CarrierLogoPath", type: String },
            { name: "CarrierDrivers", kind: "collection", elementType: CarrierDriver }
        ]),

        ChargeDeclaration: $defineEntity(ChargeDeclaration, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "IsWeightCollect", type: Boolean },
            { name: "IsOtherChargesCollect", type: Boolean },
            { name: "CarriageDeclaredValue", type: Number },
            { name: "CustomsDeclaredValue", type: Number },
            { name: "InsuranceDeclaredValue", type: Number },
            { name: "IsNoCarriageValue", type: Boolean },
            { name: "IsNoCustomsValue", type: Boolean },
            { name: "IsNoInsuranceValue", type: Boolean },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "Currency", kind: "reference", type: Currency },
            { name: "HWB", kind: "reference", type: HWB }
        ]),

        ChargeType: $defineEntity(ChargeType, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "ChargeType1", type: String },
            { name: "DefaultChargeAmount", type: String },
            { name: "IsClockStartAtArrival", type: Boolean },
            { name: "FreeTimeDuration", type: Number },
            { name: "ClockStartTime", type: Number },
            { name: "IsPerUnit", type: Boolean },
            { name: "DischargeCharges", kind: "collection", elementType: DischargeCharge },
            { name: "MinChargeAmount", type: String }
        ]),

        Condition: $defineEntity(Condition, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Condition1", type: String },
            { name: "isDamage", type: Boolean },
            { name: "ImagePath", type: String }
        ]),

        ConsignmentCode: $defineEntity(ConsignmentCode, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Name", type: String },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail },
            { name: "ManifestAWBDetails", kind: "collection", elementType: ManifestAWBDetail }
        ]),

        Contact: $defineEntity(Contact, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "FullName", type: String },
            { name: "FirstName", type: String },
            { name: "MiddleName", type: String },
            { name: "LastName", type: String },
            { name: "JobTitle", type: String },
            { name: "Phone", type: String },
            { name: "Mobile", type: String },
            { name: "Fax", type: String },
            { name: "Email", type: String },
            { name: "Telex", type: String },
            { name: "Photo", type: String },
            { name: "Customer", kind: "reference", type: Customer }
        ]),

        Country: $defineEntity(Country, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Country1", type: String },
            { name: "TwoCharacterCode", type: String },
            { name: "Ports", kind: "collection", elementType: Port },
            { name: "States", kind: "collection", elementType: State },
            { name: "Warehouses", kind: "collection", elementType: Warehouse },
            { name: "Customers", kind: "collection", elementType: Customer },
            { name: "Currencies", kind: "collection", elementType: Currency },
            { name: "OCIs", kind: "collection", elementType: OCI }
        ]),

        Currency: $defineEntity(Currency, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Currency3Code", type: String },
            { name: "Currency1", type: String },
            { name: "Country", kind: "reference", type: Country },
            { name: "ChargeDeclarations", kind: "collection", elementType: ChargeDeclaration },
            { name: "CurrencySign", type: String },
            { name: "Discharges", kind: "collection", elementType: Discharge }
        ]),

        Customer: $defineEntity(Customer, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Name", type: String },
            { name: "AddressLine1", type: String },
            { name: "AddressLine2", type: String },
            { name: "City", type: String },
            { name: "PostalCode", type: String },
            { name: "Country1", kind: "reference", type: Country },
            { name: "State", kind: "reference", type: State },
            { name: "AWBs", kind: "collection", elementType: AWB },
            { name: "AWBs1", kind: "collection", elementType: AWB },
            { name: "HWBs", kind: "collection", elementType: HWB },
            { name: "HWBs1", kind: "collection", elementType: HWB },
            { name: "Contacts", kind: "collection", elementType: Contact }
        ]),

        CustomsInfoIdentifier: $defineEntity(CustomsInfoIdentifier, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Description", type: String },
            { name: "OCIs", kind: "collection", elementType: OCI }
        ]),

        CustomsNotification: $defineEntity(CustomsNotification, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "CustomsCode", type: String },
            { name: "CodeDescription", type: String },
            { name: "StatusTimestamp", type: Date },
            { name: "Part", type: String },
            { name: "FlightNumber", type: String },
            { name: "Slac", type: Number },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "HWB", kind: "reference", type: HWB }
        ]),

        DensityGroup: $defineEntity(DensityGroup, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "DensityGroupCode", type: Number },
            { name: "Description", type: String },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail },
            { name: "ManifestAWBDetails", kind: "collection", elementType: ManifestAWBDetail }
        ]),

        DescriptionOfGood: $defineEntity(DescriptionOfGood, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "Description", type: String },
            { name: "HWB", kind: "reference", type: HWB }
        ]),

        Dimension: $defineEntity(Dimension, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Pieces", type: Number },
            { name: "Weight", type: Number },
            { name: "Length", type: Number },
            { name: "Width", type: Number },
            { name: "Height", type: Number },
            { name: "RecDate", type: Date },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "ULD", kind: "reference", type: ULD },
            { name: "UOM", kind: "reference", type: UOM },
            { name: "UOM1", kind: "reference", type: UOM }
        ]),

        DischargeCharge: $defineEntity(DischargeCharge, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "ChargeAmount", type: String },
            { name: "ChargePaid", type: String },
            { name: "ChargeType", kind: "reference", type: ChargeType },
            { name: "DischargeDetail", kind: "reference", type: DischargeDetail },
            { name: "PaymentType", kind: "reference", type: PaymentType },
            { name: "DischargePayments", kind: "collection", elementType: DischargePayment }
        ]),

        DischargeCheckInList: $defineEntity(DischargeCheckInList, [
            { name: "TaskId", type: String },
            { name: "DischargeId", type: String },
            { name: "CheckInTimestamp", type: Date },
            { name: "DriverName", type: String },
            { name: "DriverImage", type: String },
            { name: "CarrierName", type: String },
            { name: "StatusImagePath", type: String },
            { name: "TotalPieces", type: Number },
            { name: "TotalDeliveryOrders", type: Number },
            { name: "BalanceDue", type: String },
            { name: "WarehouseId", type: Number },
            { name: "StatusId", type: Number }
        ]),

        DischargeDetail: $defineEntity(DischargeDetail, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "TotalPiecesRequested", type: Number },
            { name: "TotalSLACRequested", type: Number },
            { name: "TotalPiecesReleased", type: Number },
            { name: "TotalSLACReleased", type: Number },
            { name: "DisplayText", type: String },
            { name: "TotalAmountDue", type: String },
            { name: "TotalAmountPaid", type: String },
            { name: "DischargeCharges", kind: "collection", elementType: DischargeCharge },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "Discharge", kind: "reference", type: Discharge },
            { name: "DOFilePath", type: String }
        ]),

        DischargePayment: $defineEntity(DischargePayment, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "PaymentAmount", type: String },
            { name: "Reference", type: String },
            { name: "VoucherNumber", type: String },
            { name: "CreditCardNumber", type: String },
            { name: "CreditCardExpiryMonth", type: String },
            { name: "CreditCardExpiryYear", type: String },
            { name: "DischargeCharge", kind: "reference", type: DischargeCharge },
            { name: "PaymentType", kind: "reference", type: PaymentType },
            { name: "Discharge", kind: "reference", type: Discharge }
        ]),

        Discharge: $defineEntity(Discharge, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "CheckInTimestamp", type: Date },
            { name: "Agent", type: String },
            { name: "TotalPiecesRequested", type: Number },
            { name: "TotalSlacRequested", type: Number },
            { name: "TotalDeliveryOrders", type: Number },
            { name: "TotalAmountDue", type: String },
            { name: "TotalAmountPaid", type: String },
            { name: "StatusTimestamp", type: Date },
            { name: "TotalPiecesReleased", type: Number },
            { name: "TotalSlacReleased", type: Number },
            { name: "DischargeDetails", kind: "collection", elementType: DischargeDetail },
            { name: "Status", kind: "reference", type: Status },
            { name: "DischargePayments", kind: "collection", elementType: DischargePayment },
            { name: "CarrierDriver", kind: "reference", type: CarrierDriver },
            { name: "Currency", kind: "reference", type: Currency }
        ]),

        Disposition: $defineEntity(Disposition, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "Count", type: Number },
            { name: "UserName", type: String },
            { name: "Timestamp", type: Date },
            { name: "IsAdjust", type: Boolean },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "Task", kind: "reference", type: Task },
            { name: "ULD", kind: "reference", type: ULD },
            { name: "WarehouseLocation", kind: "reference", type: WarehouseLocation }
        ]),

        Driver: $defineEntity(Driver, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "FirstName", type: String },
            { name: "LastName", type: String },
            { name: "Photo", type: String },
            { name: "LicenseNumber", type: String },
            { name: "LicenseImage", type: String },
            { name: "LicenseExpiry", type: Date },
            { name: "Carrier", kind: "reference", type: Carrier }
        ]),

        EntityType: $defineEntity(EntityType, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "EntityName", type: String },
            { name: "Transactions", kind: "collection", elementType: Transaction },
            { name: "Tasks", kind: "collection", elementType: Task },
            { name: "EntityTypeImagePath", type: String }
        ]),

        FlightManifest: $defineEntity(FlightManifest, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "FlightNumber", type: String },
            { name: "ETA", type: Date },
            { name: "ETD", type: Date },
            { name: "TotalPieces", type: Number },
            { name: "TotalULDs", type: Number },
            { name: "TotalAWBs", type: Number },
            { name: "StatusTimestamp", type: Date },
            { name: "ReceivedPieces", type: Number },
            { name: "RecoveredULDs", type: Number },
            { name: "PutAwayPieces", type: Number },
            { name: "PercentRecovered", type: Number },
            { name: "PercentReceived", type: Number },
            { name: "PercentPutAway", type: Number },
            { name: "IsOSD", type: Boolean },
            { name: "AircraftRegistration", type: String },
            { name: "ArrivalPortETA", type: Date },
            { name: "IsComplete", type: Boolean },
            { name: "RecDate", type: Date },
            { name: "RecoveredBy", type: String },
            { name: "RecoveredOn", type: Date },
            { name: "ULDs", kind: "collection", elementType: ULD },
            { name: "Carrier", kind: "reference", type: Carrier },
            { name: "Status", kind: "reference", type: Status },
            { name: "WarehouseLocation", kind: "reference", type: WarehouseLocation },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail },
            { name: "ManifestAWBDetails", kind: "collection", elementType: ManifestAWBDetail },
            { name: "FltOrigin", kind: "reference", type: Port },
            { name: "FltDestination", kind: "reference", type: Port },
            { name: "UnloadingPorts", kind: "collection", elementType: UnloadingPort },
            { name: "TaskFlightReceivers", kind: "collection", elementType: TaskFlightReceiver },
            { name: "PercentOverall", type: Number },
            { name: "HostPlus_OutboundQueues", kind: "collection", elementType: HostPlus_OutboundQueue }
        ]),

        ForkliftDetail: $defineEntity(ForkliftDetail, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "Pieces", type: Number },
            { name: "UserName", type: String },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "ULD", kind: "reference", type: ULD },
            { name: "Task", kind: "reference", type: Task }
        ]),

        ForkliftView: $defineEntity(ForkliftView, [
            { name: "Type", type: String },
            { name: "EntityId", type: String },
            { name: "RefNumber", type: String },
            { name: "Pieces", type: Number },
            { name: "IsBUP", type: Number },
            { name: "PaId", type: String },
            { name: "UserName", type: String },
            { name: "RefNumber1", type: String },
            { name: "TaskId", type: String }
        ]),

        FSN: $defineEntity(FSN, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "LastViewed", type: Date },
            { name: "CCL_ArrivalAirportCode", type: String },
            { name: "CCL_CargoTerminalOperator", type: String },
            { name: "AWB_CarrierCode", type: String },
            { name: "AWB_MasterbillNumber", type: String },
            { name: "AWB_ConsolidationIdentifier", type: String },
            { name: "AWB_HousebillNumber", type: String },
            { name: "AWB_PackageTrackingIdentifier", type: String },
            { name: "ARR_ImportingCarrier", type: String },
            { name: "ARR_FlightNumber", type: String },
            { name: "ARR_ScheduledArrivalDay", type: String },
            { name: "ARR_ScheduledArrivalMonth", type: String },
            { name: "ARR_PartArrivalReference", type: String },
            { name: "CSN_ActionCode", type: String },
            { name: "CSN_NumberOfPieces", type: String },
            { name: "CSN_TransactionDay", type: String },
            { name: "CSN_TransactionMonth", type: String },
            { name: "CSN_TransactionHour", type: String },
            { name: "CSN_TransactionMinute", type: String },
            { name: "CSN_EntryType", type: String },
            { name: "CSN_EntryNumber", type: String },
            { name: "CSN_Remarks", type: String },
            { name: "ASN_StatusCode", type: String },
            { name: "ASN_ActionExplanation", type: String }
        ]),

        HostPlus_OutboundQueue: $defineEntity(HostPlus_OutboundQueue, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "Timestamp", type: Date },
            { name: "MessageType", type: String },
            { name: "StatusCode", type: String },
            { name: "Transmitted", type: Boolean },
            { name: "Emailed", type: Boolean },
            { name: "ErrorMessage", type: String },
            { name: "UserName", type: String },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "FlightManifest", kind: "reference", type: FlightManifest },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "ULD", kind: "reference", type: ULD }
        ]),

        HWBHTSSet: $defineEntity(HWBHTSSet, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "ImportExportCode", kind: "reference", type: ImportExportCode }
        ]),

        HWB: $defineEntity(HWB, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "HWBSerialNumber", type: String },
            { name: "Pieces", type: Number },
            { name: "Weight", type: Number },
            { name: "SLAC", type: Number },
            { name: "DescriptionOfGoods", type: String },
            { name: "StatusTimestamp", type: Date },
            { name: "ReceivedPieces", type: Number },
            { name: "PutAwayPieces", type: Number },
            { name: "PercentReceived", type: Number },
            { name: "PercentPutAway", type: Number },
            { name: "ForkliftPieces", type: Number },
            { name: "ReleasedPieces", type: Number },
            { name: "AvailablePieces", type: Number },
            { name: "IsOSD", type: Boolean },
            { name: "CollectFee", type: String },
            { name: "IsCollect", type: Boolean },
            { name: "RecDate", type: Date },
            { name: "Shipper", kind: "reference", type: Customer },
            { name: "Consignee", kind: "reference", type: Customer },
            { name: "Status", kind: "reference", type: Status },
            { name: "WarehouseLocation", kind: "reference", type: WarehouseLocation },
            { name: "HwbOrigin", kind: "reference", type: Port },
            { name: "HwbDestination", kind: "reference", type: Port },
            { name: "WgtUom", kind: "reference", type: UOM },
            { name: "DischargeDetails", kind: "collection", elementType: DischargeDetail },
            { name: "ForkliftDetails", kind: "collection", elementType: ForkliftDetail },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail },
            { name: "Dispositions", kind: "collection", elementType: Disposition },
            { name: "CustomsNotifications", kind: "collection", elementType: CustomsNotification },
            { name: "DescriptionOfGoods1", kind: "collection", elementType: DescriptionOfGood },
            { name: "HWBHTSSet", kind: "collection", elementType: HWBHTSSet },
            { name: "AWBs_HWBs", kind: "collection", elementType: AWBs_HWB },
            { name: "ChargeDeclarations", kind: "collection", elementType: ChargeDeclaration },
            { name: "OCIs", kind: "collection", elementType: OCI },
            { name: "SpecialHandlings", kind: "collection", elementType: SpecialHandling },
            { name: "HostPlus_OutboundQueues", kind: "collection", elementType: HostPlus_OutboundQueue }
        ]),

        ImportExportCode: $defineEntity(ImportExportCode, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "ShortDescription", type: String },
            { name: "LongDescription", type: String },
            { name: "UOM", type: String },
            { name: "UnitOfQty", type: String },
            { name: "Sitc", type: String },
            { name: "EndUseClassification", type: String },
            { name: "UsdaProductCode", type: String },
            { name: "NaicsClassification", type: String },
            { name: "HiTechClassification", type: String },
            { name: "CodeType", type: String },
            { name: "UsedForAes", type: Boolean },
            { name: "HWBHTSSet", kind: "collection", elementType: HWBHTSSet }
        ]),

        InventoryLocationItem: $defineEntity(InventoryLocationItem, [
            { name: "Type", type: String },
            { name: "EntityId", type: String },
            { name: "DispositionId", type: String },
            { name: "TaskId", type: String },
            { name: "Count", type: Number },
            { name: "WarehouseLocationId", type: Number },
            { name: "RefNumber", type: String },
            { name: "RefNumber1", type: String },
            { name: "PaId", type: String },
            { name: "WarehouseLocation", kind: "reference", type: WarehouseLocation }
        ]),

        InventoryView: $defineEntity(InventoryView, [
            { name: "Type", type: String },
            { name: "Reference", type: String },
            { name: "Reference1", type: String },
            { name: "Count", type: Number },
            { name: "Location", type: String },
            { name: "LocationType", type: String },
            { name: "UserName", type: String },
            { name: "Timestamp", type: Date },
            { name: "WarehouseId", type: Number },
            { name: "PortId", type: Number },
            { name: "Id", type: String },
            { name: "EntityId", type: String },
            { name: "EntityId1", type: String }
        ]),

        LegSegment: $defineEntity(LegSegment, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "FlightNumber", type: String },
            { name: "ETD", type: Date },
            { name: "Sequence", type: Number },
            { name: "RecDate", type: Date },
            { name: "ULD", kind: "reference", type: ULD },
            { name: "Carrier", kind: "reference", type: Carrier },
            { name: "Port", kind: "reference", type: Port },
            { name: "ManifestAWBDetail", kind: "reference", type: ManifestAWBDetail }
        ]),

        LineIdentifier: $defineEntity(LineIdentifier, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Description", type: String },
            { name: "OCIs", kind: "collection", elementType: OCI }
        ]),

        LoadingIndicator: $defineEntity(LoadingIndicator, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Description", type: String },
            { name: "ULDs", kind: "collection", elementType: ULD }
        ]),

        ManifestAWBDetail: $defineEntity(ManifestAWBDetail, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "Part", type: String },
            { name: "LoadCount", type: Number },
            { name: "ReceivedCount", type: Number },
            { name: "Weight", type: Number },
            { name: "Volume", type: Number },
            { name: "DensityIndicator", type: Boolean },
            { name: "IsBUP", type: Boolean },
            { name: "IsOSD", type: Boolean },
            { name: "PercentReceived", type: Number },
            { name: "PutAwayPieces", type: Number },
            { name: "PercentPutAway", type: Number },
            { name: "RecDate", type: Date },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "FlightManifest", kind: "reference", type: FlightManifest },
            { name: "ULD", kind: "reference", type: ULD },
            { name: "UnloadingPort", kind: "reference", type: UnloadingPort },
            { name: "ConsignmentCode", kind: "reference", type: ConsignmentCode },
            { name: "DensityGroup", kind: "reference", type: DensityGroup },
            { name: "UOM", kind: "reference", type: UOM },
            { name: "UOM1", kind: "reference", type: UOM },
            { name: "LegSegments", kind: "collection", elementType: LegSegment }
        ]),

        ManifestDetail: $defineEntity(ManifestDetail, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "Part", type: String },
            { name: "LoadCount", type: Number },
            { name: "Weight", type: Number },
            { name: "Volume", type: Number },
            { name: "DensityIndicator", type: Boolean },
            { name: "PercentReceived", type: Number },
            { name: "ReceivedCount", type: Number },
            { name: "PutAwayPieces", type: Number },
            { name: "PercentPutAway", type: Number },
            { name: "IsBUP", type: Boolean },
            { name: "RecDate", type: Date },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "FlightManifest", kind: "reference", type: FlightManifest },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "ULD", kind: "reference", type: ULD },
            { name: "ConsignmentCode", kind: "reference", type: ConsignmentCode },
            { name: "DensityGroup", kind: "reference", type: DensityGroup },
            { name: "WgtUom", kind: "reference", type: UOM },
            { name: "VolUom", kind: "reference", type: UOM },
            { name: "UnloadingPort", kind: "reference", type: UnloadingPort }
        ]),

        MOT: $defineEntity(MOT, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "MOT1", type: String },
            { name: "Carriers", kind: "collection", elementType: Carrier },
            { name: "Ports", kind: "collection", elementType: Port }
        ]),

        MovementPriorityCode: $defineEntity(MovementPriorityCode, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Description", type: String },
            { name: "AWBs", kind: "collection", elementType: AWB }
        ]),

        OCI: $defineEntity(OCI, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "SupCustomsInfo", type: String },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "Country", kind: "reference", type: Country },
            { name: "CustomsInfoIdentifier", kind: "reference", type: CustomsInfoIdentifier },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "LineIdentifier", kind: "reference", type: LineIdentifier },
            { name: "ULD", kind: "reference", type: ULD }
        ]),

        ParticipantIdentifier: $defineEntity(ParticipantIdentifier, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Description", type: String }
        ]),

        PaymentType: $defineEntity(PaymentType, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "PaymentType1", type: String },
            { name: "IsAutoGenerated", type: Boolean },
            { name: "DischargeCharges", kind: "collection", elementType: DischargeCharge },
            { name: "DischargePayments", kind: "collection", elementType: DischargePayment }
        ]),

        Port: $defineEntity(Port, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "IATACode", type: String },
            { name: "Port1", type: String },
            { name: "IsGateway", type: Boolean },
            { name: "FlightManifests", kind: "collection", elementType: FlightManifest },
            { name: "FlightManifests1", kind: "collection", elementType: FlightManifest },
            { name: "Country", kind: "reference", type: Country },
            { name: "MOT", kind: "reference", type: MOT },
            { name: "State", kind: "reference", type: State },
            { name: "AWBs", kind: "collection", elementType: AWB },
            { name: "AWBs1", kind: "collection", elementType: AWB },
            { name: "HWBs", kind: "collection", elementType: HWB },
            { name: "HWBs1", kind: "collection", elementType: HWB },
            { name: "Warehouses", kind: "collection", elementType: Warehouse },
            { name: "UnloadingPorts", kind: "collection", elementType: UnloadingPort },
            { name: "LegSegments", kind: "collection", elementType: LegSegment },
            { name: "UserStations", kind: "collection", elementType: UserStation },
            { name: "CarrierRoutes", kind: "collection", elementType: CarrierRoute },
            { name: "CarrierRoutes1", kind: "collection", elementType: CarrierRoute },
            { name: "CarrierRoutes2", kind: "collection", elementType: CarrierRoute }
        ]),

        PutAwayList: $defineEntity(PutAwayList, [
            { name: "Type", type: String },
            { name: "EntityId", type: String },
            { name: "RefNumber", type: String },
            { name: "PutAwayPieces", type: Number },
            { name: "TotalPieces", type: Number },
            { name: "PercentPutAway", type: Number },
            { name: "IsBUP", type: Number },
            { name: "Status", type: Number },
            { name: "PaId", type: String },
            { name: "Status1", kind: "reference", type: Status },
            { name: "RefNumber1", type: String },
            { name: "ForkliftPieces", type: Number },
            { name: "FlightManifestId", type: String }
        ]),

        PutedAwayItem: $defineEntity(PutedAwayItem, [
            { name: "Reference", type: String },
            { name: "Reference1", type: String },
            { name: "WarehouseLocationId", type: Number },
            { name: "Count", type: Number },
            { name: "Location", type: String },
            { name: "DispositionId", type: String },
            { name: "Type", type: String },
            { name: "IsBUP", type: Number },
            { name: "Status", type: Number },
            { name: "PaId", type: String },
            { name: "EntityId", type: String },
            { name: "Status1", kind: "reference", type: Status }
        ]),

        ReceiverAwbManifestList: $defineEntity(ReceiverAwbManifestList, [
            { name: "Id", type: String },
            { name: "FlightManifestId", type: String },
            { name: "AwbNumber", type: String }
        ]),

        ReceiverFlightView: $defineEntity(ReceiverFlightView, [
            { name: "FlightManifestId", type: String },
            { name: "OrgFlightNumber", type: String },
            { name: "ETA", type: Date },
            { name: "ArrivalTime", type: String },
            { name: "TotalULDs", type: Number },
            { name: "TotalAWBs", type: Number },
            { name: "TotalPieces", type: Number },
            { name: "CarrierName", type: String },
            { name: "FlightNumber", type: String },
            { name: "RecoveredULDs", type: Number },
            { name: "ReceivedPieces", type: Number },
            { name: "RecoveredBy", type: String },
            { name: "RecoveredOn", type: String },
            { name: "IsNilCargo", type: Boolean },
            { name: "PortId", type: Number },
            { name: "StatusId", type: Number },
            { name: "StatusName", type: String },
            { name: "CarrierLogoPath", type: String },
            { name: "StatusImagePath", type: String },
            { name: "Location", type: String },
            { name: "UnloadingId", type: Number },
            { name: "IsComplete", type: Boolean },
            { name: "Origin", type: String }
        ]),

        ReceiverUldManifestList: $defineEntity(ReceiverUldManifestList, [
            { name: "Id", type: String },
            { name: "FlightManifestId", type: String },
            { name: "UldNumber", type: String },
            { name: "ULDId", type: String },
            { name: "IsBUP", type: Boolean },
            { name: "TotalReceivedCount", type: Number },
            { name: "TotalPieces", type: Number },
            { name: "ReceivedPieces", type: Number },
            { name: "StatusId", type: Number },
            { name: "CarrierCode", type: String },
            { name: "StatusImagePath", type: String },
            { name: "PercentReceived", type: Number }
        ]),

        ShipmentUnitType: $defineEntity(ShipmentUnitType, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Description", type: String },
            { name: "ULDs", kind: "collection", elementType: ULD }
        ]),

        SnapshotTasksView: $defineEntity(SnapshotTasksView, [
            { name: "Type", type: String },
            { name: "EntityId", type: String },
            { name: "RefNumber", type: String },
            { name: "RefNumber1", type: String },
            { name: "TaskCreator", type: String },
            { name: "TaskOwner", type: String },
            { name: "Condition", type: String },
            { name: "Pieces", type: Number },
            { name: "Weight", type: String },
            { name: "TaskStatus", type: String },
            { name: "StatusImagePath", type: String },
            { name: "EntityTypeImagePath", type: String },
            { name: "PortId", type: Number },
            { name: "TaskStatusId", type: Number },
            { name: "EndDate", type: Date },
            { name: "WarehouseLocation", type: String },
            { name: "SnapshotCount", type: Number }
        ]),

        SpecialHandlingCode: $defineEntity(SpecialHandlingCode, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "Description", type: String },
            { name: "IsHazmatCode", type: Boolean },
            { name: "SpecialHandlings", kind: "collection", elementType: SpecialHandling }
        ]),

        SpecialHandling: $defineEntity(SpecialHandling, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "AWB", kind: "reference", type: AWB },
            { name: "HWB", kind: "reference", type: HWB },
            { name: "SpecialHandlingCode", kind: "reference", type: SpecialHandlingCode },
            { name: "ULD", kind: "reference", type: ULD }
        ]),

        State: $defineEntity(State, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "StateProvince", type: String },
            { name: "TwoCharacterCode", type: String },
            { name: "Ports", kind: "collection", elementType: Port },
            { name: "Country", kind: "reference", type: Country },
            { name: "Warehouses", kind: "collection", elementType: Warehouse },
            { name: "Customers", kind: "collection", elementType: Customer }
        ]),

        Status: $defineEntity(Status, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Name", type: String },
            { name: "AWBs", kind: "collection", elementType: AWB },
            { name: "FlightManifests", kind: "collection", elementType: FlightManifest },
            { name: "HWBs", kind: "collection", elementType: HWB },
            { name: "ULDs", kind: "collection", elementType: ULD },
            { name: "Transactions", kind: "collection", elementType: Transaction },
            { name: "Tasks", kind: "collection", elementType: Task },
            { name: "Discharges", kind: "collection", elementType: Discharge },
            { name: "PutAwayLists", kind: "collection", elementType: PutAwayList },
            { name: "StatusImagePath", type: String },
            { name: "PutedAwayItems", kind: "collection", elementType: PutedAwayItem }
        ]),

        TableList: $defineEntity(TableList, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "DisplayName", type: String },
            { name: "TableName", type: String },
            { name: "ScreenName", type: String },
            { name: "DBName", type: String }
        ]),

        TaskFlightReceiver: $defineEntity(TaskFlightReceiver, [
            { name: "FlightManifestId", type: String },
            { name: "TaskId", type: String },
            { name: "CarrierCode", type: String },
            { name: "FlightNumber", type: String },
            { name: "RecoveredBy", type: String },
            { name: "StartDate", type: Date },
            { name: "EndDate", type: Date },
            { name: "TotalULDs", type: Number },
            { name: "TotalAWBs", type: Number },
            { name: "TotalPieces", type: Number },
            { name: "IsOSD", type: Boolean },
            { name: "FlightManifest", kind: "reference", type: FlightManifest },
            { name: "Task", kind: "reference", type: Task },
            { name: "PercentOverall", type: Number },
            { name: "ETA", type: Date }
        ]),

        Task: $defineEntity(Task, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "TaskCreationDate", type: Date },
            { name: "EntityId", type: String },
            { name: "TaskCreator", type: String },
            { name: "TaskOwner", type: String },
            { name: "TaskReference", type: String },
            { name: "TaskDate", type: Date },
            { name: "DueDate", type: Date },
            { name: "StartDate", type: Date },
            { name: "EndDate", type: Date },
            { name: "StatusTimestamp", type: Date },
            { name: "Warehouse", kind: "reference", type: Warehouse },
            { name: "Dispositions", kind: "collection", elementType: Disposition },
            { name: "EntityType", kind: "reference", type: EntityType },
            { name: "Status", kind: "reference", type: Status },
            { name: "TaskType", kind: "reference", type: TaskType },
            { name: "TaskFlightReceivers", kind: "collection", elementType: TaskFlightReceiver },
            { name: "ForkliftDetails", kind: "collection", elementType: ForkliftDetail },
            { name: "Transactions", kind: "collection", elementType: Transaction }
        ]),

        TaskType: $defineEntity(TaskType, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "ActionName", type: String },
            { name: "Category", type: String },
            { name: "IsActive", type: Boolean },
            { name: "TaskIcon", type: String },
            { name: "Transactions", kind: "collection", elementType: Transaction },
            { name: "Tasks", kind: "collection", elementType: Task }
        ]),

        TrackTraceView: $defineEntity(TrackTraceView, [
            { name: "ArrivalTime", type: String },
            { name: "Type", type: String },
            { name: "EntityId", type: String },
            { name: "Reference", type: String },
            { name: "Part", type: String },
            { name: "HBCount", type: Number },
            { name: "ReceivedPieces", type: Number },
            { name: "Weight", type: String },
            { name: "Weight1", type: Number },
            { name: "GeneralOrder", type: String },
            { name: "CustomsCode", type: String },
            { name: "ISC", type: String },
            { name: "StorageChargesPerDay", type: Number },
            { name: "CurrentStorageCharges", type: Number },
            { name: "OutstandingBalance", type: Number },
            { name: "Carrier", type: String },
            { name: "FlightNumber", type: String },
            { name: "StorageStartDate", type: String },
            { name: "GeneralOrderDate", type: String },
            { name: "UnloadingPort", type: String },
            { name: "FirmCode", type: String },
            { name: "FinalDestination", type: String }
        ]),

        TransactionAction: $defineEntity(TransactionAction, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: String },
            { name: "TaskType", type: Number },
            { name: "Transactions", kind: "collection", elementType: Transaction }
        ]),

        Transaction: $defineEntity(Transaction, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "EntityId", type: String },
            { name: "StatusTimestamp", type: Date },
            { name: "UserName", type: String },
            { name: "Reference", type: String },
            { name: "Description", type: String },
            { name: "EntityType", kind: "reference", type: EntityType },
            { name: "Status", kind: "reference", type: Status },
            { name: "TaskType", kind: "reference", type: TaskType },
            { name: "TransactionAction", kind: "reference", type: TransactionAction },
            { name: "Task", kind: "reference", type: Task }
        ]),

        ULD: $defineEntity(ULD, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "SerialNumber", type: String },
            { name: "TotalUnitCount", type: Number },
            { name: "TotalReceivedCount", type: Number },
            { name: "TotalPieces", type: Number },
            { name: "TotalWeight", type: Number },
            { name: "TotalVolume", type: Number },
            { name: "RecoveredBy", type: String },
            { name: "ReceivedPieces", type: Number },
            { name: "PutAwayPieces", type: Number },
            { name: "PercentRecovered", type: Number },
            { name: "PercentReceived", type: Number },
            { name: "ForkliftPieces", type: Number },
            { name: "PercentPutAway", type: Number },
            { name: "RecoveredOn", type: Date },
            { name: "Remarks", type: String },
            { name: "RecDate", type: Date },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail },
            { name: "Carrier", kind: "reference", type: Carrier },
            { name: "FlightManifest", kind: "reference", type: FlightManifest },
            { name: "ForkliftDetails", kind: "collection", elementType: ForkliftDetail },
            { name: "Status", kind: "reference", type: Status },
            { name: "ManifestAWBDetails", kind: "collection", elementType: ManifestAWBDetail },
            { name: "Dispositions", kind: "collection", elementType: Disposition },
            { name: "LoadingIndicator", kind: "reference", type: LoadingIndicator },
            { name: "ShipmentUnitType", kind: "reference", type: ShipmentUnitType },
            { name: "ULDVolumeCode", kind: "reference", type: ULDVolumeCode },
            { name: "WgtUom", kind: "reference", type: UOM },
            { name: "VolUom", kind: "reference", type: UOM },
            { name: "Dimensions", kind: "collection", elementType: Dimension },
            { name: "LegSegments", kind: "collection", elementType: LegSegment },
            { name: "OCIs", kind: "collection", elementType: OCI },
            { name: "SpecialHandlings", kind: "collection", elementType: SpecialHandling },
            { name: "IsBUP", type: Boolean },
            { name: "HostPlus_OutboundQueues", kind: "collection", elementType: HostPlus_OutboundQueue }
        ]),

        ULDVolumeCode: $defineEntity(ULDVolumeCode, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Code", type: Number },
            { name: "Description", type: String },
            { name: "ULDs", kind: "collection", elementType: ULD }
        ]),

        UnloadingPort: $defineEntity(UnloadingPort, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "POU_ETD", type: Date },
            { name: "POU_ETA", type: Date },
            { name: "IsNilCargo", type: Boolean },
            { name: "FlightManifest", kind: "reference", type: FlightManifest },
            { name: "Port", kind: "reference", type: Port },
            { name: "ManifestAWBDetails", kind: "collection", elementType: ManifestAWBDetail },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail }
        ]),

        UOM: $defineEntity(UOM, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "ThreeCharCode", type: String },
            { name: "TwoCharCode", type: String },
            { name: "OneCharCode", type: String },
            { name: "UOMName", type: String },
            { name: "AWBs", kind: "collection", elementType: AWB },
            { name: "AWBs1", kind: "collection", elementType: AWB },
            { name: "UOMSystem", kind: "reference", type: UOMSystem },
            { name: "UOMType", kind: "reference", type: UOMType },
            { name: "HWBs", kind: "collection", elementType: HWB },
            { name: "ULDs", kind: "collection", elementType: ULD },
            { name: "ULDs1", kind: "collection", elementType: ULD },
            { name: "Dimensions", kind: "collection", elementType: Dimension },
            { name: "Dimensions1", kind: "collection", elementType: Dimension },
            { name: "ManifestDetails", kind: "collection", elementType: ManifestDetail },
            { name: "ManifestDetails1", kind: "collection", elementType: ManifestDetail },
            { name: "ManifestAWBDetails", kind: "collection", elementType: ManifestAWBDetail },
            { name: "ManifestAWBDetails1", kind: "collection", elementType: ManifestAWBDetail }
        ]),

        UOMSystem: $defineEntity(UOMSystem, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "System", type: String },
            { name: "UOMs", kind: "collection", elementType: UOM }
        ]),

        UOMType: $defineEntity(UOMType, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Type", type: String },
            { name: "UOMs", kind: "collection", elementType: UOM }
        ]),

        UserProfile: $defineEntity(UserProfile, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "UserGuid", type: String },
            { name: "UserId", type: String },
            { name: "Title", type: String },
            { name: "FirstName", type: String },
            { name: "MiddleName", type: String },
            { name: "LastName", type: String },
            { name: "JobTitle", type: String },
            { name: "Phone", type: String },
            { name: "Mobile", type: String },
            { name: "Fax", type: String },
            { name: "Email", type: String },
            { name: "Photo", type: String },
            { name: "UserStations", kind: "collection", elementType: UserStation },
            { name: "UserWarehouses", kind: "collection", elementType: UserWarehous }
        ]),

        UserStation: $defineEntity(UserStation, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "IsDefault", type: Boolean },
            { name: "Port", kind: "reference", type: Port },
            { name: "UserProfile", kind: "reference", type: UserProfile }
        ]),

        UserWarehous: $defineEntity(UserWarehous, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "IsDefault", type: Boolean },
            { name: "UserProfile", kind: "reference", type: UserProfile },
            { name: "Warehouse", kind: "reference", type: Warehouse }
        ]),

        WarehouseLocation: $defineEntity(WarehouseLocation, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Location", type: String },
            { name: "Barcode", type: String },
            { name: "FlightManifests", kind: "collection", elementType: FlightManifest },
            { name: "HWBs", kind: "collection", elementType: HWB },
            { name: "Dispositions", kind: "collection", elementType: Disposition },
            { name: "WarehouseLocationType", kind: "reference", type: WarehouseLocationType },
            { name: "Warehouse1", kind: "reference", type: Warehouse },
            { name: "WarehouseZone", kind: "reference", type: WarehouseZone },
            { name: "InventoryLocationItems", kind: "collection", elementType: InventoryLocationItem }
        ]),

        WarehouseLocationType: $defineEntity(WarehouseLocationType, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "LocationType", type: String },
            { name: "Code", type: String },
            { name: "BarcodeIdentifier", type: String },
            { name: "Description", type: String },
            { name: "WarehouseLocations", kind: "collection", elementType: WarehouseLocation }
        ]),

        Warehouse: $defineEntity(Warehouse, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "WarehouseName", type: String },
            { name: "Code", type: String },
            { name: "Address", type: String },
            { name: "City", type: String },
            { name: "PostalCode", type: String },
            { name: "Phone", type: String },
            { name: "FirmCode", type: String },
            { name: "Supervisor", type: String },
            { name: "SupervisorCode", type: String },
            { name: "Tasks", kind: "collection", elementType: Task },
            { name: "Country1", kind: "reference", type: Country },
            { name: "Port", kind: "reference", type: Port },
            { name: "State1", kind: "reference", type: State },
            { name: "WarehouseLocations", kind: "collection", elementType: WarehouseLocation },
            { name: "WarehouseZones", kind: "collection", elementType: WarehouseZone },
            { name: "UserWarehouses", kind: "collection", elementType: UserWarehous }
        ]),

        WarehouseZone: $defineEntity(WarehouseZone, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Zone", type: String },
            { name: "WarehouseLocations", kind: "collection", elementType: WarehouseLocation },
            { name: "Warehouse", kind: "reference", type: Warehouse }
        ]),

        Condition1: $defineEntity(Condition1, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "Condition2", type: String },
            { name: "IsDamage", type: Boolean },
            { name: "EntityConditions", kind: "collection", elementType: EntityCondition1 },
            { name: "ImagePath", type: String }
        ]),

        EntityCondition1: $defineEntity(EntityCondition1, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "EntityId", type: String },
            { name: "Count", type: Number },
            { name: "UserName", type: String },
            { name: "Timestamp", type: Date },
            { name: "TaskId", type: String },
            { name: "TaskTypeId", type: Number },
            { name: "Condition", kind: "reference", type: Condition1 },
            { name: "EntityType", kind: "reference", type: EntityType1 },
            { name: "EntitySnapshots", kind: "collection", elementType: EntitySnapshot1 }
        ]),

        EntitySnapshot1: $defineEntity(EntitySnapshot1, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "UserName", type: String },
            { name: "Timestamp", type: Date },
            { name: "EntityCondition", kind: "reference", type: EntityCondition1 },
            { name: "Snapshot", kind: "reference", type: Snapshot1 }
        ]),

        EntityType1: $defineEntity(EntityType1, [
            { name: "Id", type: Number, isReadOnly: true },
            { name: "EntityName", type: String },
            { name: "EntityConditions", kind: "collection", elementType: EntityCondition1 },
            { name: "Snapshots", kind: "collection", elementType: Snapshot1 }
        ]),

        Snapshot1: $defineEntity(Snapshot1, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "Count", type: Number },
            { name: "UserName", type: String },
            { name: "Timestamp", type: Date },
            { name: "EntityId", type: String },
            { name: "DisplayText", type: String },
            { name: "EntitySnapshots", kind: "collection", elementType: EntitySnapshot1 },
            { name: "EntityType", kind: "reference", type: EntityType1 },
            { name: "ImagePath", type: String }
        ]),

        CIMPMessageLog: $defineEntity(CIMPMessageLog, [
            { name: "Id", type: String, isReadOnly: true },
            { name: "Timestamp", type: Date },
            { name: "FileName", type: String },
            { name: "MessageType", type: String },
            { name: "MailboxName", type: String },
            { name: "RawMessage", type: String },
            { name: "ProcessingStatus", type: Boolean },
            { name: "ErrorMessage", type: String },
            { name: "DownloadedTime", type: Date },
            { name: "SentTime", type: Date }
        ]),

        HostPlus_AuditTrail: $defineEntity(HostPlus_AuditTrail, [
            { name: "RecId", type: String, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "TransactionId", type: String },
            { name: "UserId", type: String },
            { name: "DBUser", type: String },
            { name: "AppName", type: String },
            { name: "Operation", type: String },
            { name: "TableName", type: String },
            { name: "Before", type: String },
            { name: "After", type: String }
        ]),

        HostPlus_Email: $defineEntity(HostPlus_Email, [
            { name: "RecId", type: String, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "UserId", type: String },
            { name: "Status", type: Number },
            { name: "MailForm", type: String },
            { name: "MailTo", type: String },
            { name: "MailCc", type: String },
            { name: "MailBcc", type: String },
            { name: "Body", type: String },
            { name: "Subject", type: String },
            { name: "Gateway", type: String },
            { name: "MailDate", type: Date },
            { name: "Attachment", type: String },
            { name: "TransmitionError", type: String },
            { name: "IsAutoEmail", type: Number },
            { name: "Tries", type: Number },
            { name: "IsZip", type: Number }
        ]),

        HostPlus_Location: $defineEntity(HostPlus_Location, [
            { name: "RecId", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "Location", type: String },
            { name: "Description", type: String },
            { name: "Connection", type: String },
            { name: "TimeDifference", type: Number },
            { name: "WebConnectionName", type: String },
            { name: "EntityID", type: Number },
            { name: "DBName", type: String }
        ]),

        HostPlus_Log: $defineEntity(HostPlus_Log, [
            { name: "RecId", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "Description", type: String },
            { name: "StackTrace", type: String }
        ]),

        HostPlus_Parameter: $defineEntity(HostPlus_Parameter, [
            { name: "RecId", type: String, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "Name", type: String },
            { name: "DefaultValue", type: String },
            { name: "Description", type: String },
            { name: "AllowBlank", type: Boolean },
            { name: "DataType", type: String },
            { name: "TaskId", type: String },
            { name: "SessionTypeId", type: String },
            { name: "DataEnumeration", type: String }
        ]),

        HostPlus_SessionParameter: $defineEntity(HostPlus_SessionParameter, [
            { name: "RecId", type: String, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "UserId", type: String },
            { name: "SessionId", type: String },
            { name: "Name", type: String },
            { name: "Value", type: String },
            { name: "ParameterId", type: String }
        ]),

        HostPlus_Session: $defineEntity(HostPlus_Session, [
            { name: "RecId", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "UserId", type: String },
            { name: "Name", type: String },
            { name: "Description", type: String },
            { name: "SessionTypeId", type: Number },
            { name: "IsActive", type: Boolean },
            { name: "Server", type: String }
        ]),

        HostPlus_SessionTaskParameter: $defineEntity(HostPlus_SessionTaskParameter, [
            { name: "RecId", type: String, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "UserId", type: String },
            { name: "SessionTaskId", type: String },
            { name: "Name", type: String },
            { name: "Value", type: String },
            { name: "ParameterId", type: String }
        ]),

        HostPlus_SessionTask: $defineEntity(HostPlus_SessionTask, [
            { name: "RecID", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "UserId", type: String },
            { name: "SessionId", type: Number },
            { name: "TaskId", type: Number },
            { name: "LastDate", type: Date },
            { name: "NextDate", type: Date },
            { name: "Timer", type: String },
            { name: "TimerWeekDays", type: String },
            { name: "TimerType", type: String },
            { name: "IsActive", type: Boolean },
            { name: "Status", type: String },
            { name: "Trace", type: String },
            { name: "IsTraceable", type: Boolean },
            { name: "Progress", type: String },
            { name: "c_Error", type: String },
            { name: "LocationId", type: Number },
            { name: "Description", type: String },
            { name: "Retries", type: Number }
        ]),

        HostPlus_SessionType: $defineEntity(HostPlus_SessionType, [
            { name: "RecId", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "UserId", type: String },
            { name: "Name", type: String },
            { name: "Description", type: String },
            { name: "AuthRequired", type: Boolean },
            { name: "GroupName", type: String },
            { name: "Icon", type: String }
        ]),

        HostPlus_Task: $defineEntity(HostPlus_Task, [
            { name: "RecId", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "SessionTypeId", type: Number },
            { name: "Name", type: String },
            { name: "Description", type: String },
            { name: "Icon", type: String },
            { name: "RetriesOnFail", type: Number }
        ]),

        HostPlus_TimestampsCode: $defineEntity(HostPlus_TimestampsCode, [
            { name: "RecID", type: Number, isReadOnly: true },
            { name: "RecDate", type: Date },
            { name: "Code", type: String },
            { name: "Description", type: String }
        ]),

        WFSDBData: $defineDataService(WFSDBData, lightSwitchApplication.rootUri + "/WFSDBData.svc", [
            { name: "Agents", elementType: Agent },
            { name: "AWBs", elementType: AWB },
            { name: "AWBs_HWBs", elementType: AWBs_HWB },
            { name: "CarrierDrivers", elementType: CarrierDriver },
            { name: "CarrierRoutes", elementType: CarrierRoute },
            { name: "Carriers", elementType: Carrier },
            { name: "ChargeDeclarations", elementType: ChargeDeclaration },
            { name: "ChargeTypes", elementType: ChargeType },
            { name: "Conditions", elementType: Condition },
            { name: "ConsignmentCodes", elementType: ConsignmentCode },
            { name: "Contacts", elementType: Contact },
            { name: "Countries", elementType: Country },
            { name: "Currencies", elementType: Currency },
            { name: "Customers", elementType: Customer },
            { name: "CustomsInfoIdentifiers", elementType: CustomsInfoIdentifier },
            { name: "CustomsNotifications", elementType: CustomsNotification },
            { name: "DensityGroups", elementType: DensityGroup },
            { name: "DescriptionOfGoods", elementType: DescriptionOfGood },
            { name: "Dimensions", elementType: Dimension },
            { name: "DischargeCharges", elementType: DischargeCharge },
            { name: "DischargeCheckInLists", elementType: DischargeCheckInList },
            { name: "DischargeDetails", elementType: DischargeDetail },
            { name: "DischargePayments", elementType: DischargePayment },
            { name: "Discharges", elementType: Discharge },
            { name: "Dispositions", elementType: Disposition },
            { name: "Drivers", elementType: Driver },
            { name: "EntityTypes", elementType: EntityType },
            { name: "FlightManifests", elementType: FlightManifest },
            { name: "ForkliftDetails", elementType: ForkliftDetail },
            { name: "ForkliftViews", elementType: ForkliftView },
            { name: "FSNs", elementType: FSN },
            { name: "HostPlus_OutboundQueues", elementType: HostPlus_OutboundQueue },
            { name: "HWBHTSSet", elementType: HWBHTSSet },
            { name: "HWBs", elementType: HWB },
            { name: "ImportExportCodes", elementType: ImportExportCode },
            { name: "InventoryLocationItems", elementType: InventoryLocationItem },
            { name: "InventoryViews", elementType: InventoryView },
            { name: "LegSegments", elementType: LegSegment },
            { name: "LineIdentifiers", elementType: LineIdentifier },
            { name: "LoadingIndicators", elementType: LoadingIndicator },
            { name: "ManifestAWBDetails", elementType: ManifestAWBDetail },
            { name: "ManifestDetails", elementType: ManifestDetail },
            { name: "MOTs", elementType: MOT },
            { name: "MovementPriorityCodes", elementType: MovementPriorityCode },
            { name: "OCIs", elementType: OCI },
            { name: "ParticipantIdentifiers", elementType: ParticipantIdentifier },
            { name: "PaymentTypes", elementType: PaymentType },
            { name: "Ports", elementType: Port },
            { name: "PutAwayLists", elementType: PutAwayList },
            { name: "PutedAwayItems", elementType: PutedAwayItem },
            { name: "ReceiverAwbManifestLists", elementType: ReceiverAwbManifestList },
            { name: "ReceiverFlightViews", elementType: ReceiverFlightView },
            { name: "ReceiverUldManifestLists", elementType: ReceiverUldManifestList },
            { name: "ShipmentUnitTypes", elementType: ShipmentUnitType },
            { name: "SnapshotTasksViews", elementType: SnapshotTasksView },
            { name: "SpecialHandlingCodes", elementType: SpecialHandlingCode },
            { name: "SpecialHandlings", elementType: SpecialHandling },
            { name: "States", elementType: State },
            { name: "Statuses", elementType: Status },
            { name: "TableLists", elementType: TableList },
            { name: "TaskFlightReceivers", elementType: TaskFlightReceiver },
            { name: "Tasks", elementType: Task },
            { name: "TaskTypes", elementType: TaskType },
            { name: "TrackTraceViews", elementType: TrackTraceView },
            { name: "TransactionActions", elementType: TransactionAction },
            { name: "Transactions", elementType: Transaction },
            { name: "ULDs", elementType: ULD },
            { name: "ULDVolumeCodes", elementType: ULDVolumeCode },
            { name: "UnloadingPorts", elementType: UnloadingPort },
            { name: "UOMs", elementType: UOM },
            { name: "UOMSystems", elementType: UOMSystem },
            { name: "UOMTypes", elementType: UOMType },
            { name: "UserProfiles", elementType: UserProfile },
            { name: "UserStations", elementType: UserStation },
            { name: "UserWarehouses", elementType: UserWarehous },
            { name: "WarehouseLocations", elementType: WarehouseLocation },
            { name: "WarehouseLocationTypes", elementType: WarehouseLocationType },
            { name: "Warehouses", elementType: Warehouse },
            { name: "WarehouseZones", elementType: WarehouseZone }
        ], [
            {
                name: "Agents_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Agents },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Agents(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "AWBs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.AWBs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/AWBs(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "DischargeAWBs", value: function (DischargeId) {
                    return new $DataServiceQuery({ _entitySet: this.AWBs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargeAWBs()",
                        {
                            DischargeId: $toODataString(DischargeId, "Int64?")
                        });
                }
            },
            {
                name: "FlightAWBs", value: function (FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.AWBs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightAWBs()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "AWBs_HWBs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.AWBs_HWBs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/AWBs_HWBs(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "CarrierDrivers_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.CarrierDrivers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/CarrierDrivers(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "CarrierRoutes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.CarrierRoutes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/CarrierRoutes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Carriers_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Carriers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Carriers(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ChargeDeclarations_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ChargeDeclarations },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ChargeDeclarations(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ChargeTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ChargeTypes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ChargeTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Conditions_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Conditions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Conditions(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ConsignmentCodes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ConsignmentCodes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ConsignmentCodes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Contacts_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Contacts },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Contacts(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Countries_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Countries },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Countries(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Currencies_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Currencies },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Currencies(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Customers_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Customers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Customers(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "CustomsInfoIdentifiers_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.CustomsInfoIdentifiers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/CustomsInfoIdentifiers(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "CustomsNotifications_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.CustomsNotifications },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/CustomsNotifications(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "DensityGroups_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.DensityGroups },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DensityGroups(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "DescriptionOfGoods_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.DescriptionOfGoods },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DescriptionOfGoods(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Dimensions_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Dimensions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Dimensions(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "DischargeCharges_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.DischargeCharges },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargeCharges(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "AllDischargeCharges", value: function (DischargeId) {
                    return new $DataServiceQuery({ _entitySet: this.DischargeCharges },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/AllDischargeCharges()",
                        {
                            DischargeId: $toODataString(DischargeId, "Int64?")
                        });
                }
            },
            {
                name: "DischargeCheckInLists_SingleOrDefault", value: function (TaskId, DischargeId, StatusId) {
                    return new $DataServiceQuery({ _entitySet: this.DischargeCheckInLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargeCheckInLists(" + "TaskId=" + $toODataString(TaskId, "Int64?") + "," + "DischargeId=" + $toODataString(DischargeId, "Int64?") + "," + "StatusId=" + $toODataString(StatusId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ActiveCheckInList", value: function (IsComplete, SearchFilter, DateFilter) {
                    return new $DataServiceQuery({ _entitySet: this.DischargeCheckInLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveCheckInList()",
                        {
                            IsComplete: $toODataString(IsComplete, "Boolean?"),
                            SearchFilter: $toODataString(SearchFilter, "String?"),
                            DateFilter: $toODataString(DateFilter, "DateTime?")
                        });
                }
            },
            {
                name: "DischargeDetails_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.DischargeDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargeDetails(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "DischargeAWBDetail", value: function (AWBId, DischargeId) {
                    return new $DataServiceQuery({ _entitySet: this.DischargeDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargeAWBDetail()",
                        {
                            AWBId: $toODataString(AWBId, "Int64?"),
                            DischargeId: $toODataString(DischargeId, "Int64?")
                        });
                }
            },
            {
                name: "DischargeHWBDetail", value: function (HWBId, DischargeId) {
                    return new $DataServiceQuery({ _entitySet: this.DischargeDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargeHWBDetail()",
                        {
                            HWBId: $toODataString(HWBId, "Int64?"),
                            DischargeId: $toODataString(DischargeId, "Int64?")
                        });
                }
            },
            {
                name: "DischargePayments_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.DischargePayments },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargePayments(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "AllDischargePayments", value: function (DischargeId) {
                    return new $DataServiceQuery({ _entitySet: this.DischargePayments },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/AllDischargePayments()",
                        {
                            DischargeId: $toODataString(DischargeId, "Int64?")
                        });
                }
            },
            {
                name: "Discharges_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Discharges },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Discharges(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "ActiveCheckIns", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.Discharges },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveCheckIns()",
                        {
                        });
                }
            },
            {
                name: "ActiveDischarges", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.Discharges },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveDischarges()",
                        {
                        });
                }
            },
            {
                name: "Dispositions_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Dispositions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Dispositions(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "Drivers_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Drivers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Drivers(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "EntityTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.EntityTypes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/EntityTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "FlightManifests_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.FlightManifests },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightManifests(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "ForkliftDetails_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ForkliftDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ForkliftDetails(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "ForkliftViews_SingleOrDefault", value: function (Type, EntityId) {
                    return new $DataServiceQuery({ _entitySet: this.ForkliftViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ForkliftViews(" + "Type=" + $toODataString(Type, "String?") + "," + "EntityId=" + $toODataString(EntityId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "ActiveForkliftView", value: function (TaskId) {
                    return new $DataServiceQuery({ _entitySet: this.ForkliftViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveForkliftView()",
                        {
                            TaskId: $toODataString(TaskId, "Int64?")
                        });
                }
            },
            {
                name: "ActiveScannerView", value: function (TaskId) {
                    return new $DataServiceQuery({ _entitySet: this.ForkliftViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveScannerView()",
                        {
                            TaskId: $toODataString(TaskId, "Int64?")
                        });
                }
            },
            {
                name: "FSNs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.FSNs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FSNs(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_OutboundQueues_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_OutboundQueues },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/HostPlus_OutboundQueues(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "HWBHTSSet_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.HWBHTSSet },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/HWBHTSSet(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HWBs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.HWBs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/HWBs(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "DischargeHWBs", value: function (DischargeId) {
                    return new $DataServiceQuery({ _entitySet: this.HWBs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/DischargeHWBs()",
                        {
                            DischargeId: $toODataString(DischargeId, "Int64?")
                        });
                }
            },
            {
                name: "FlightHWBs", value: function (FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.HWBs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightHWBs()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "ImportExportCodes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ImportExportCodes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ImportExportCodes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "InventoryLocationItems_SingleOrDefault", value: function (Type, DispositionId) {
                    return new $DataServiceQuery({ _entitySet: this.InventoryLocationItems },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/InventoryLocationItems(" + "Type=" + $toODataString(Type, "String?") + "," + "DispositionId=" + $toODataString(DispositionId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "ActiveLocationItems", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.InventoryLocationItems },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveLocationItems()",
                        {
                        });
                }
            },
            {
                name: "SearchLocationItems", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.InventoryLocationItems },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/SearchLocationItems()",
                        {
                        });
                }
            },
            {
                name: "InventoryViews_SingleOrDefault", value: function (Type, WarehouseId, Id, EntityId, EntityId1) {
                    return new $DataServiceQuery({ _entitySet: this.InventoryViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/InventoryViews(" + "Type=" + $toODataString(Type, "String?") + "," + "WarehouseId=" + $toODataString(WarehouseId, "Int32?") + "," + "Id=" + $toODataString(Id, "Int64?") + "," + "EntityId=" + $toODataString(EntityId, "Int64?") + "," + "EntityId1=" + $toODataString(EntityId1, "Int64?") + ")"
                    );
                }
            },
            {
                name: "ActiveInventory", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.InventoryViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveInventory()",
                        {
                        });
                }
            },
            {
                name: "LegSegments_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.LegSegments },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/LegSegments(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "LineIdentifiers_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.LineIdentifiers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/LineIdentifiers(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "LoadingIndicators_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.LoadingIndicators },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/LoadingIndicators(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ManifestAWBDetails_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ManifestAWBDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ManifestAWBDetails(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "BreakdownAwbs", value: function (ShowComplete, ShowAll, UldId, FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.ManifestAWBDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/BreakdownAwbs()",
                        {
                            ShowComplete: $toODataString(ShowComplete, "Boolean?"),
                            ShowAll: $toODataString(ShowAll, "Boolean?"),
                            UldId: $toODataString(UldId, "Int64?"),
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "ManifestDetails_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ManifestDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ManifestDetails(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "BreakdownHwbs", value: function (FlightManifestId, UldId, AwbId, ShowComplete) {
                    return new $DataServiceQuery({ _entitySet: this.ManifestDetails },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/BreakdownHwbs()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?"),
                            UldId: $toODataString(UldId, "Int64?"),
                            AwbId: $toODataString(AwbId, "Int64?"),
                            ShowComplete: $toODataString(ShowComplete, "Boolean?")
                        });
                }
            },
            {
                name: "MOTs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.MOTs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/MOTs(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "MovementPriorityCodes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.MovementPriorityCodes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/MovementPriorityCodes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "OCIs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.OCIs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/OCIs(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "ParticipantIdentifiers_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ParticipantIdentifiers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ParticipantIdentifiers(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "PaymentTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.PaymentTypes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/PaymentTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Ports_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Ports },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Ports(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "PutAwayLists_SingleOrDefault", value: function (Type, EntityId) {
                    return new $DataServiceQuery({ _entitySet: this.PutAwayLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/PutAwayLists(" + "Type=" + $toODataString(Type, "String?") + "," + "EntityId=" + $toODataString(EntityId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "FlightPutAwayList", value: function (FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.PutAwayLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightPutAwayList()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "PutedAwayItems_SingleOrDefault", value: function (DispositionId, Type, EntityId) {
                    return new $DataServiceQuery({ _entitySet: this.PutedAwayItems },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/PutedAwayItems(" + "DispositionId=" + $toODataString(DispositionId, "Int64?") + "," + "Type=" + $toODataString(Type, "String?") + "," + "EntityId=" + $toODataString(EntityId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "FlightPutedAwayList", value: function (FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.PutedAwayItems },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightPutedAwayList()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "ReceiverAwbManifestLists_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ReceiverAwbManifestLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ReceiverAwbManifestLists(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "ReceiverAwbList", value: function (FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.ReceiverAwbManifestLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ReceiverAwbList()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "ReceiverFlightViews_SingleOrDefault", value: function (StatusId, UnloadingId, Origin) {
                    return new $DataServiceQuery({ _entitySet: this.ReceiverFlightViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ReceiverFlightViews(" + "StatusId=" + $toODataString(StatusId, "Int32?") + "," + "UnloadingId=" + $toODataString(UnloadingId, "Int32?") + "," + "Origin=" + $toODataString(Origin, "String?") + ")"
                    );
                }
            },
            {
                name: "ReceiverFlightList", value: function (IsComplete, SearchFilter, DateFilter) {
                    return new $DataServiceQuery({ _entitySet: this.ReceiverFlightViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ReceiverFlightList()",
                        {
                            IsComplete: $toODataString(IsComplete, "Boolean?"),
                            SearchFilter: $toODataString(SearchFilter, "String?"),
                            DateFilter: $toODataString(DateFilter, "DateTime?")
                        });
                }
            },
            {
                name: "ReceiverUldManifestLists_SingleOrDefault", value: function (Id, ULDId, StatusId) {
                    return new $DataServiceQuery({ _entitySet: this.ReceiverUldManifestLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ReceiverUldManifestLists(" + "Id=" + $toODataString(Id, "Int64?") + "," + "ULDId=" + $toODataString(ULDId, "Int64?") + "," + "StatusId=" + $toODataString(StatusId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "BreakdownUldList", value: function (FlightManifestId, ShowComplete) {
                    return new $DataServiceQuery({ _entitySet: this.ReceiverUldManifestLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/BreakdownUldList()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?"),
                            ShowComplete: $toODataString(ShowComplete, "Boolean?")
                        });
                }
            },
            {
                name: "ReceiverUldList", value: function (FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.ReceiverUldManifestLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ReceiverUldList()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "ShipmentUnitTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ShipmentUnitTypes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ShipmentUnitTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "SnapshotTasksViews_SingleOrDefault", value: function (Type, EntityId, Condition, PortId, TaskStatusId) {
                    return new $DataServiceQuery({ _entitySet: this.SnapshotTasksViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/SnapshotTasksViews(" + "Type=" + $toODataString(Type, "String?") + "," + "EntityId=" + $toODataString(EntityId, "Int64?") + "," + "Condition=" + $toODataString(Condition, "String?") + "," + "PortId=" + $toODataString(PortId, "Int32?") + "," + "TaskStatusId=" + $toODataString(TaskStatusId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "SnapshotTasks", value: function (EntityType, IsComplete) {
                    return new $DataServiceQuery({ _entitySet: this.SnapshotTasksViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/SnapshotTasks()",
                        {
                            EntityType: $toODataString(EntityType, "String?"),
                            IsComplete: $toODataString(IsComplete, "Boolean?")
                        });
                }
            },
            {
                name: "SpecialHandlingCodes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.SpecialHandlingCodes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/SpecialHandlingCodes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "SpecialHandlings_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.SpecialHandlings },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/SpecialHandlings(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "States_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.States },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/States(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Statuses_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Statuses },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Statuses(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "TableLists_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.TableLists },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/TableLists(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "TaskFlightReceivers_SingleOrDefault", value: function (FlightManifestId, TaskId) {
                    return new $DataServiceQuery({ _entitySet: this.TaskFlightReceivers },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/TaskFlightReceivers(" + "FlightManifestId=" + $toODataString(FlightManifestId, "Int64?") + "," + "TaskId=" + $toODataString(TaskId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "Tasks_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Tasks },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Tasks(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "AllInventoryTasks", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.Tasks },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/AllInventoryTasks()",
                        {
                        });
                }
            },
            {
                name: "InventoryTasks", value: function (UserName) {
                    return new $DataServiceQuery({ _entitySet: this.Tasks },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/InventoryTasks()",
                        {
                            UserName: $toODataString(UserName, "String?")
                        });
                }
            },
            {
                name: "TaskTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.TaskTypes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/TaskTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "TrackTraceViews_SingleOrDefault", value: function (Type, EntityId, GeneralOrder, CustomsCode, Carrier, FlightNumber) {
                    return new $DataServiceQuery({ _entitySet: this.TrackTraceViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/TrackTraceViews(" + "Type=" + $toODataString(Type, "String?") + "," + "EntityId=" + $toODataString(EntityId, "Int64?") + "," + "GeneralOrder=" + $toODataString(GeneralOrder, "String?") + "," + "CustomsCode=" + $toODataString(CustomsCode, "String?") + "," + "Carrier=" + $toODataString(Carrier, "String?") + "," + "FlightNumber=" + $toODataString(FlightNumber, "String?") + ")"
                    );
                }
            },
            {
                name: "ActiveTrackTraceView", value: function (Reference) {
                    return new $DataServiceQuery({ _entitySet: this.TrackTraceViews },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ActiveTrackTraceView()",
                        {
                            Reference: $toODataString(Reference, "String?")
                        });
                }
            },
            {
                name: "TransactionActions_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.TransactionActions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/TransactionActions(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Transactions_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Transactions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Transactions(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "AwbHistory", value: function (FlightManifestId, AwbId) {
                    return new $DataServiceQuery({ _entitySet: this.Transactions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/AwbHistory()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?"),
                            AwbId: $toODataString(AwbId, "Int64?")
                        });
                }
            },
            {
                name: "FlightHistory", value: function (FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.Transactions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightHistory()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "InventoryHistory", value: function (TaskId) {
                    return new $DataServiceQuery({ _entitySet: this.Transactions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/InventoryHistory()",
                        {
                            TaskId: $toODataString(TaskId, "Int64?")
                        });
                }
            },
            {
                name: "UldHistory", value: function (FlightManifestId, UldId) {
                    return new $DataServiceQuery({ _entitySet: this.Transactions },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UldHistory()",
                        {
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?"),
                            UldId: $toODataString(UldId, "Int64?")
                        });
                }
            },
            {
                name: "ULDs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ULDs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ULDs(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "BreakdownUlds", value: function (ShowComplete, FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.ULDs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/BreakdownUlds()",
                        {
                            ShowComplete: $toODataString(ShowComplete, "Boolean?"),
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "FlightULDs", value: function (UnloadingPortId, FlightManifestId) {
                    return new $DataServiceQuery({ _entitySet: this.ULDs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightULDs()",
                        {
                            UnloadingPortId: $toODataString(UnloadingPortId, "Int32?"),
                            FlightManifestId: $toODataString(FlightManifestId, "Int64?")
                        });
                }
            },
            {
                name: "ULDVolumeCodes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.ULDVolumeCodes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/ULDVolumeCodes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "UnloadingPorts_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.UnloadingPorts },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UnloadingPorts(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "FlightList", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.FlightManifests },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightList()",
                        {
                        });
                }
            },
            {
                name: "FlightManifestList", value: function (IsComplete) {
                    return new $DataServiceQuery({ _entitySet: this.UnloadingPorts },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/FlightManifestList()",
                        {
                            IsComplete: $toODataString(IsComplete, "Boolean?")
                        });
                }
            },
            {
                name: "UOMs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.UOMs },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UOMs(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "UOMSystems_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.UOMSystems },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UOMSystems(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "UOMTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.UOMTypes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UOMTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "UserProfiles_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.UserProfiles },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UserProfiles(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "UserList", value: function () {
                    return new $DataServiceQuery({ _entitySet: this.UserProfiles },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UserList()",
                        {
                        });
                }
            },
            {
                name: "UserStations_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.UserStations },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UserStations(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "UserWarehouses_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.UserWarehouses },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/UserWarehouses(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "WarehouseLocations_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.WarehouseLocations },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/WarehouseLocations(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "WarehouseLocationTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.WarehouseLocationTypes },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/WarehouseLocationTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Warehouses_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Warehouses },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/Warehouses(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "WarehouseZones_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.WarehouseZones },
                        lightSwitchApplication.rootUri + "/WFSDBData.svc" + "/WarehouseZones(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            }
        ]),

        SnapshotDBData: $defineDataService(SnapshotDBData, lightSwitchApplication.rootUri + "/SnapshotDBData.svc", [
            { name: "Conditions", elementType: Condition1 },
            { name: "EntityConditions", elementType: EntityCondition1 },
            { name: "EntitySnapshots", elementType: EntitySnapshot1 },
            { name: "EntityTypes", elementType: EntityType1 },
            { name: "Snapshots", elementType: Snapshot1 }
        ], [
            {
                name: "Conditions_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Conditions },
                        lightSwitchApplication.rootUri + "/SnapshotDBData.svc" + "/Conditions(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "EntityConditions_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.EntityConditions },
                        lightSwitchApplication.rootUri + "/SnapshotDBData.svc" + "/EntityConditions(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "EntitySnapshots_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.EntitySnapshots },
                        lightSwitchApplication.rootUri + "/SnapshotDBData.svc" + "/EntitySnapshots(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "EntityTypes_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.EntityTypes },
                        lightSwitchApplication.rootUri + "/SnapshotDBData.svc" + "/EntityTypes(" + "Id=" + $toODataString(Id, "Int32?") + ")"
                    );
                }
            },
            {
                name: "Snapshots_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.Snapshots },
                        lightSwitchApplication.rootUri + "/SnapshotDBData.svc" + "/Snapshots(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            }
        ]),

        MCHCoreDBData: $defineDataService(MCHCoreDBData, lightSwitchApplication.rootUri + "/MCHCoreDBData.svc", [
            { name: "CIMPMessageLogs", elementType: CIMPMessageLog },
            { name: "HostPlus_AuditTrails", elementType: HostPlus_AuditTrail },
            { name: "HostPlus_Emails", elementType: HostPlus_Email },
            { name: "HostPlus_Locations", elementType: HostPlus_Location },
            { name: "HostPlus_Logs", elementType: HostPlus_Log },
            { name: "HostPlus_Parameters", elementType: HostPlus_Parameter },
            { name: "HostPlus_SessionParameters", elementType: HostPlus_SessionParameter },
            { name: "HostPlus_Sessions", elementType: HostPlus_Session },
            { name: "HostPlus_SessionTaskParameters", elementType: HostPlus_SessionTaskParameter },
            { name: "HostPlus_SessionTasks", elementType: HostPlus_SessionTask },
            { name: "HostPlus_SessionTypes", elementType: HostPlus_SessionType },
            { name: "HostPlus_Tasks", elementType: HostPlus_Task },
            { name: "HostPlus_TimestampsCodes", elementType: HostPlus_TimestampsCode }
        ], [
            {
                name: "CIMPMessageLogs_SingleOrDefault", value: function (Id) {
                    return new $DataServiceQuery({ _entitySet: this.CIMPMessageLogs },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/CIMPMessageLogs(" + "Id=" + $toODataString(Id, "Int64?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_AuditTrails_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_AuditTrails },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_AuditTrails(" + "RecId=" + $toODataString(RecId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_Emails_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_Emails },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_Emails(" + "RecId=" + $toODataString(RecId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_Locations_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_Locations },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_Locations(" + "RecId=" + $toODataString(RecId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_Logs_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_Logs },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_Logs(" + "RecId=" + $toODataString(RecId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_Parameters_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_Parameters },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_Parameters(" + "RecId=" + $toODataString(RecId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_SessionParameters_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_SessionParameters },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_SessionParameters(" + "RecId=" + $toODataString(RecId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_Sessions_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_Sessions },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_Sessions(" + "RecId=" + $toODataString(RecId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_SessionTaskParameters_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_SessionTaskParameters },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_SessionTaskParameters(" + "RecId=" + $toODataString(RecId, "Int64?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_SessionTasks_SingleOrDefault", value: function (RecID) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_SessionTasks },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_SessionTasks(" + "RecID=" + $toODataString(RecID, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_SessionTypes_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_SessionTypes },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_SessionTypes(" + "RecId=" + $toODataString(RecId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_Tasks_SingleOrDefault", value: function (RecId) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_Tasks },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_Tasks(" + "RecId=" + $toODataString(RecId, "Int32?") + ")"
                    );
                }
            },
            {
                name: "HostPlus_TimestampsCodes_SingleOrDefault", value: function (RecID) {
                    return new $DataServiceQuery({ _entitySet: this.HostPlus_TimestampsCodes },
                        lightSwitchApplication.rootUri + "/MCHCoreDBData.svc" + "/HostPlus_TimestampsCodes(" + "RecID=" + $toODataString(RecID, "Int32?") + ")"
                    );
                }
            }
        ]),

        DataWorkspace: $defineDataWorkspace(DataWorkspace, [
            { name: "WFSDBData", type: WFSDBData },
            { name: "SnapshotDBData", type: SnapshotDBData },
            { name: "MCHCoreDBData", type: MCHCoreDBData }
        ])

    });

}(msls.application));
