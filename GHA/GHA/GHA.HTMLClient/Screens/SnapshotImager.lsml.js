﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var localMediaStream = null;
var $ConditionItems;

myapp.SnapshotImager.created = function (screen) {
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/GetConditionList.ashx',
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            })
        });

    }).then(function PromiseSuccess(PromiseResult) {
        $ConditionItems = PromiseResult;
    }));

};

myapp.SnapshotImager.VideoElement_render = function (element, contentItem) {    
    var container = $(element);
    container.css('padding', '0').css('height', '355px');
    container.attr('id', 'videoContainer');
    var videoElement = $('<video id="videoElement" autoplay style="height:350px; max-width:580px; width:100%"></video>');
    container.append(videoElement);
};

myapp.SnapshotImager.Slider_render = function (element, contentItem) {
    var container = $(element);
    container.css('padding', '6px 2px 2px').css('border', '1px solid gray').css('border-radius', '4px');
    var sliderWrapper = $('<div id="sliderWrapper" style="position:relative; margin-right:12px; float:right; overflow: hidden; display:inline-block; white-space: nowrap; height: 100%; width: calc(100% - 160px);"></div>');
    var slider = $('<div id="slider" style="min-width:100%; text-align:center;white-space: nowrap; height: 100%; position:absolute; left:0"></div>');
    sliderWrapper.append(slider);

    var arrowLeft = $('<div style="cursor:pointer;margin-top:6px; display:inline-block; float:left; height:68px; width:68px; background-image:url(Content/images/arrow-left.png)"></div>');
    arrowLeft.on('click', function () { slide(106);});

    var arrowRight = $('<div style="cursor:pointer;margin-top:6px; display:inline-block; float:right; height:68px; width:68px; background-image:url(Content/images/arrow-right.png)"></div>');
    arrowRight.on('click', function () { slide(-106); });

    container.append(arrowLeft);
    container.append(arrowRight);
    container.append(sliderWrapper);
};

function slide(amount) {
    var slider = $('#slider');
    var left = parseInt(slider.css('left'));
    if (left === 0 && amount > 0) return;
    if (amount < 0 && $('#sliderWrapper').width() - left - amount > slider.width()) return;
    slider.css('left', left + amount);
}

myapp.SnapshotImager.NewPhoto_Tap_execute = function (screen) {   
    if (localMediaStream) {
        return;
    }
    $('#slider span').css('border-color', 'blue');
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
    if (navigator.getUserMedia) {
        navigator.getUserMedia({
            video: {
                mandatory: {
                    maxWidth: 1024,
                    maxHeight: 768
                }
            }
        },
            function (stream) {
                var video = document.querySelector('#videoElement');
                video.src = window.URL.createObjectURL(stream);
                localMediaStream = stream;
                $('#videoContainer canvas').remove();
                $('#videoElement').show();

            }, function () { alert('Cannot get user media.'); });
    } else {
        alert('getUserMedia() is not supported in your browser');
    }
};

myapp.SnapshotImager.TakePicture_Tap_execute = function (screen) {
    if (localMediaStream) {
        $('#slider span').css('border-color', 'blue');
        var canvas = $('<canvas style=""></canvas>');
        var ctx = canvas[0].getContext('2d');
        var video = document.querySelector('#videoElement');

        canvas[0].width = video.videoWidth;
        canvas[0].height = video.videoHeight;
        ctx.drawImage(video, 0, 0, canvas[0].width, canvas[0].height);        

        canvas.css('width', canvas[0].width / 8);
        canvas.css('height', canvas[0].height / 8);

        var newImage = $('<span style="position:relative;cursor:pointer;border:2px solid blue; display:inline-block; border-radius:4px; padding: 6px 6px 0; margin-right:5px; margin-left:5px"></span>');
        var piecesHolder = $('<div class="piece-holder" style="font-size:11px">Pieces:0</div>');
        var conditionsListHolder = $('<span style="display:none" class="conditionsListHolder"></span>');

        var topLayer = $('<div class="topLayer" style="font-weight:bold;font-size:8px;text-align:right;overflow:hidden;position:absolute;right:6px;top:5px;height:50px; width:50px;"></div>');

        newImage.on('click', imageClicked);
        newImage.append(canvas);
        newImage.append(topLayer);
        newImage.append(piecesHolder);
        newImage.append(conditionsListHolder);
        $('#slider').append(newImage);
    }
};

myapp.SnapshotImager.DeleteImage_execute = function (screen) {
    var imageToBeDeleted = $('#slider span[data-selected="1"]');

    var next = imageToBeDeleted.next('span');
    var prev = imageToBeDeleted.prev('span');

    if (next.length) {
        showImage(next);
    } else {
        showImage(prev);
    }

    imageToBeDeleted.remove();
};

myapp.SnapshotImager.SelectCondition_postRender = function (element, contentItem) {
    var iScrn = contentItem.screen;
    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var selectedIds = $('#conditionlist .mycheckbox:checked').map(function () {
            return $(this).attr('value');
        }).get().join(",");

        var selectedTexts = $('#conditionlist .mycheckbox:checked').map(function () {
            return $(this).next().text();
        }).get().join("<br>");

        $('#slider span[data-selected="1"] .conditionsListHolder').text(selectedIds);
        $('#slider span[data-selected="1"] .piece-holder').text('Pieces:' + iScrn.EffectedQuantity);

        $('#slider span[data-selected="1"] .topLayer').append(selectedTexts);
        $('#videoContainer .canvasContainer .overlay').append(selectedTexts);
        
        iScrn.closePopup("ConditionListPopup");
    });    
};

function imageClicked() {
    showImage($(this));
}

function showImage(element) {
    if (!element || !element.length) {
        $('#videoContainer .canvasContainer').remove();
        return;
    }

    if (localMediaStream) {
        var video = document.querySelector('#videoElement');
        video.pause();
        localMediaStream.stop();
        localMediaStream = null;
        $(video).css('display', 'none');
    }

    $('#slider span').css('border-color', 'blue');
    $('#slider span').attr('data-selected', '0');
    element.css('border-color', 'red');
    element.attr('data-selected', '1');
    $('#videoContainer .canvasContainer').remove();

    var canvasContainer = $('<div class="canvasContainer" style="position:relative;display:inline-block;"></div>');
    var newCanvas = cloneCanvas(element.find('canvas')[0]);
    var overlay = $('<div class="overlay" style="font-weight:bold;position:absolute;height:95%;width:200px;top:0;right:10px;overflow:hidden;text-align:right;"></div>');
    overlay.html(element.find('.topLayer').html());
    canvasContainer.append(newCanvas);
    canvasContainer.append(overlay);
    $('#videoContainer').append(canvasContainer);
}

function cloneCanvas(oldCanvas) {    
    var newCanvas = document.createElement('canvas');
    var context = newCanvas.getContext('2d');    
    newCanvas.width = $('#videoElement').width();
    newCanvas.height = $('#videoElement').height();
    context.drawImage(oldCanvas, 0, 0, newCanvas.width, newCanvas.height);
    return newCanvas;
}

myapp.SnapshotImager.TakePicture_postRender = function (element, contentItem) {
    setButtonPadding(element);
};

myapp.SnapshotImager.NewPhoto_postRender = function (element, contentItem) {
    setButtonPadding(element);
};

myapp.SnapshotImager.DeleteImage_postRender = function (element, contentItem) {
    setButtonPadding(element);
};

function setButtonPadding(element) {
    $(element).find('a').attr('style', 'padding-top: 8px !important; padding-bottom: 8px !important');    
}

myapp.SnapshotImager.Conditions_postRender = function (element, contentItem) {
    setButtonPadding(element);
};

myapp.SnapshotImager.ConditionList_render = function (element, contentItem) {
    if ($ConditionItems != null || $ConditionItems != "") {
        $(element).append($ConditionItems);
    }
};

myapp.SnapshotImager.Conditions_execute = function (screen) {
    $(window).one("popupcreate", function (e) {
        $(e.target).popup({ positionTo: "window" });
    });

    var selectedIds = $('#slider span[data-selected="1"] .conditionsListHolder').text().split(',');

    $('#conditionlist .ui-checkbox .mycheckbox').each(function () {        
        if (jQuery.inArray($(this).val(), selectedIds) != -1) {
            this.checked = true;
            var label = $(this).next('.ui-checkbox-off');
            label.removeClass('ui-checkbox-off').addClass('ui-checkbox-on');
            label.find('.ui-icon-checkbox-off').removeClass('ui-icon-checkbox-off').addClass('ui-icon-checkbox-on');            
        } else {
            this.checked = false;
            var label = $(this).next('.ui-checkbox-on');
            label.removeClass('ui-checkbox-on').addClass('ui-checkbox-off');
            label.find('.ui-icon-checkbox-on').removeClass('ui-icon-checkbox-on').addClass('ui-icon-checkbox-off');
        }
    });
    screen.EffectedQuantity = $('#slider span[data-selected="1"] .piece-holder').text().split(':')[1];

    screen.showPopup("ConditionListPopup");
};

//var $fileBrowseButton;

//myapp.SnapshotImager.TakePicture_execute = function (screen) {
    
//    $fileBrowseButton.click();

//};

//myapp.SnapshotImager.SnapshotImage1_postRender = function (element, contentItem) {
//    $fileBrowseButton = $('<input name="file" type="file" id="SnapshotFile" accept="image/*" capture="camera" style="display:none"/>');

//    $fileBrowseButton.bind('change', function handleFileSelect(evt) {
//        var files = evt.target.files;

//        if (files.length == 1) {
//            var reader = new FileReader();

//            reader.onload = function (e) {

//                var tempImg = new Image();
//                tempImg.src = reader.result;

//                tempImg.onload = function () {

//                    var MAX_WIDTH = 400;
//                    var MAX_HEIGHT = 300;
//                    var tempW = tempImg.width;
//                    var tempH = tempImg.height;
//                    if (tempW > tempH) {
//                        if (tempW > MAX_WIDTH) {
//                            tempH *= MAX_WIDTH / tempW;
//                            tempW = MAX_WIDTH;
//                        }
//                    } else {
//                        if (tempH > MAX_HEIGHT) {
//                            tempW *= MAX_HEIGHT / tempH;
//                            tempH = MAX_HEIGHT;
//                        }
//                    }

//                    var canvas = document.createElement('canvas');
//                    canvas.width = tempW;
//                    canvas.height = tempH;

//                    var ctx = canvas.getContext("2d");
//                    ctx.drawImage(this, 0, 0, tempW, tempH);

//                    var dataURL = canvas.toDataURL("image/png");

//                    $(element).append($('<img src="' + dataURL + '" />'));
//                    contentItem.value = dataURL.substring(dataURL.indexOf(",") + 1);

//                    msls.showProgress(msls.promiseOperation(function (operation) {
//                        $.ajax({
//                            type: 'post',
//                            data: { 'filename': contentItem.value },
//                            url: '../Web/FileUpload.ashx',
//                            success: operation.code(function AjaxSuccess(AjaxResult) {
//                                alert("success");
//                                operation.complete(AjaxResult);
//                            }),
//                            error: function () {
//                                alert("fail");
//                                operation.complete(AjaxResult);
//                            }
//                        });
//                    }));

//                }
//            };
//            reader.readAsDataURL(files[0]);
//        }
//    });

//};
