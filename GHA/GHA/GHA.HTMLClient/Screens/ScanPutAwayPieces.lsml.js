﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $ScanLogger;
var $LastScan;
var $ScanLog;

myapp.ScanPutAwayPieces.created = function (screen) {

 

};

myapp.ScanPutAwayPieces.BarcodeListener_render = function (element, contentItem) {
    // Write code here.
    var iScrn = contentItem.screen;
    var bcText = contentItem.value;
    var cUser = iScrn.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cTask = iScrn.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var cFlt = iScrn.SelectedFlt;
    if (!cFlt) {
        cFlt = $FlightManifest;
    }
    var aTimeout = iScrn.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    var chars = [];
    var ctr = "0";
    var result = "";

    if (bcText == null) {
        bcText = "";
    }

    $(document).off('keypress').on('keypress', function (event) {

        var charCode = event.which || event.keyCode;

        if (charCode == 13) {
            //$ShipmentFilter.blur();
            //$rButton.focus();
        }

        if (ctr == "1" && charCode != 126) {
            chars.push(String.fromCharCode(charCode));
        }

        if (charCode == 126) {
            if (ctr == "0") {
                ctr = "1";
            }
            else {
                ctr = "2";
            }
        }

        if (ctr == "2") {
            bcText = chars.join("");

            msls.showProgress(msls.promiseOperation(function (operation) {

                $.ajax({
                    type: 'post',
                    //data: { 'bcScan': bcText, 'tId': cTask.Id, 'fId': cFlt.Id, 'cUser': cUser },
                    data: { 'bcScan': bcText, 'tId': 222, 'fId': 218, 'cUser': 'cmxadmin' },
                    url: '../Web/ScanPutAwayPieces.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        chars = [];
                        ctr = "0";
                        bcText = "";
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        console.log(AjaxResult);
                        AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                        chars = [];
                        ctr = "0";
                        bcText = "";
                        operation.complete(AjaxResult);
                    })
                });

            }).then(function PromiseSuccess(PromiseResult) {
                try {
                    var result = PromiseResult;

                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {

                            var rItems = result.split(',');

                            if (rItems[0] == "S") {

                                $LastScan = rItems[2];
                                $ScanLog += rItems[2] +"\r\n" + iScrn.LoggerText;

                                $('#scLogger').val($LastScan);

                                console.log('S-Location');
                                $('span#flCtr').text("0");
                                iScrn.ForkLiftCount = "0";
                                event = null;
                                result = null;

                            }
                            else if (rItems[0] == "A") {

                                $LastScan = rItems[2];
                                $ScanLog += rItems[2] + "\r\n" + iScrn.LoggerText;

                                $('#scLogger').val($LastScan);

                                console.log('A-AWB');
                                iScrn.EntityType = "AWB";
                                iScrn.EntityId = rItems[1];
                                iScrn.AddToForkliftText = rItems[2];
                                iScrn.SelectedQuantity = rItems[3];
                                iScrn.MaxPutAwayPieces = rItems[3];

                                event = null;
                                result = null;

                                //$(window).one("popupcreate", function (e) {

                                //    $(e.target).popup({

                                //        positionTo: "window"

                                //    });

                                //});

                                //iScrn.showPopup("AddToForkliftPopup").then(function () {
                                //    setTimeout(function () {

                                //        $PutAwaySelQuantityInput.focus();
                                //        $PutAwaySelQuantityInput.select();

                                //    }, 100);
                                //});

                            }
                            else if (rItems[0] == "H") {

                                $LastScan = rItems[2];
                                $ScanLog += rItems[2] + "\r\n" + iScrn.LoggerText;

                                $('#scLogger').val($LastScan);

                                console.log('H-HWB');
                                iScrn.EntityType = "HWB";
                                iScrn.EntityId = rItems[1];
                                iScrn.AddToForkliftText = rItems[2];
                                iScrn.SelectedQuantity = rItems[3];
                                iScrn.MaxPutAwayPieces = rItems[3];

                                event = null;
                                result = null;

                                //$(window).one("popupcreate", function (e) {

                                //    $(e.target).popup({

                                //        positionTo: "window"

                                //    });

                                //});

                                //iScrn.showPopup("AddToForkliftPopup").then(function () {
                                //    setTimeout(function () {

                                //        $PutAwaySelQuantityInput.focus();
                                //        $PutAwaySelQuantityInput.select();

                                //    }, 100);
                                //});
                            }
                            else if (rItems[0] == "D") {

                                $LastScan = "The shipment scan is valid but not available for put away on current flight.";
                                $ScanLog = "The shipment scan is valid but not available for put away on current flight.\r\n" + iScrn.LoggerText;
                                
                                $('#scLogger').val($LastScan);

                                console.log('D-Valid Scan');
                                //iScrn.UpdatePopupHeader = "Valid Scan";
                                //iScrn.UpdateText = "The shipment scan is valid but not available for put away on current flight.";

                                event = null;
                                result = null;

                            }
                            else if (rItems[0] == "V") {

                                $LastScan = "No shipment pieces on the forklift.  Add pieces to forklift and scan location again.";
                                $ScanLog = "No shipment pieces on the forklift.  Add pieces to forklift and scan location again.\r\n" + iScrn.LoggerText;

                                $('#scLogger').val($LastScan);

                                console.log('V-No Shipments');
                                //$('span#flCtr').text("0");                    
                                //iScrn.UpdatePopupHeader = "No Shipments";
                                //iScrn.UpdateText = "No shipment pieces on the forklift. \n\rAdd pieces to forklift and scan location again.";

                                event = null;
                                result = null;


                            }
                            else {

                                $LastScan = "Invalid Barcode Scan.";
                                $ScanLog = "Invalid Barcode Scan.\r\n" + iScrn.LoggerText;

                                $('#scLogger').val($LastScan);

                                console.log('Invalid Scan');
                                //iScrn.UpdatePopupHeader = "Invalid Barcode";
                                //iScrn.UpdateText = "Invalid Barcode Scan";                    

                                event = null;
                                result = null;


                            }
                        }
                    }
                }
                catch (e) {
                    console.log(e);
                }
            }

            ));
        }
    });
};

myapp.ScanPutAwayPieces.ScanLogger_render = function (element, contentItem) {
    
    $ScanLogger = $(element);

    var hCode = '<div><textarea id="scLogger" name="scLogger" style="width:550px; min-width:550px; max-width:550px; height:400px; max-height:400px; min-height:400px;" readonly="readonly"></textarea></div>';

    $(element).append(hCode);

};

myapp.ScanPutAwayPieces.SelectedFlt_WarehouseLocation_Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};

myapp.ScanPutAwayPieces.SelectedFlt_Status_Name_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};

myapp.ScanPutAwayPieces.SelectedFlt_PercentPutAway_render = function (element, contentItem) {

    var fscrn = contentItem.screen;
    var fItem = fscrn.SelectedFlt;
    if (!fItem) {
        fItem = $FlightManifest;
    }
    var sInd = fItem.PutAwayPieces;
    var eInd = fItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "hProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var fscrn = contentItem.screen;
        var fItem = fscrn.SelectedFlt;
        if (!fItem) {
            fItem = $FlightManifest;
        }
        var sInd = fItem.PutAwayPieces;
        var eInd = fItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "hProgressBar", element);

    });

};
myapp.ScanPutAwayPieces.ForkliftCount_render = function (element, contentItem) {
    
    if (contentItem.value == null) {
        contentItem.value = 0;
    }

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_forklift' onclick='showForkliftView()' ><img src='content/images/recoveryinprogress_icon_w.png' style='height: 41px;' /><span id='flCtr'>" + contentItem.value + "</span></div>");
    $(element).trigger("create");

    contentItem.addChangeListener("value", function () {

        $('span#flCtr').text(contentItem.value);

    });

};
myapp.ScanPutAwayPieces.RefreshButton_render = function (element, contentItem) {
    
    $(element).html("<div class='am_refresh' onclick='refreshView6()' ><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    $(element).trigger("create");

};

