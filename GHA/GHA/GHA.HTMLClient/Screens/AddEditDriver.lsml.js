﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.AddEditDriver.Photo_render = function (element, contentItem) {

    createImageUploader(element, contentItem, "max-width: 100px; max-height: 120px");

};

myapp.AddEditDriver.LicenseImage_render = function (element, contentItem) {

    createImageUploader(element, contentItem, "max-width: 240px; max-height: 120px");

};

myapp.AddEditDriver.SelectCarrier_execute = function (screen) {
    
    myapp.showSelectCarrier(screen.Driver, {
        afterClosed: function (prevScreen, navigationAction) {
            
            if (navigationAction === msls.NavigateBackAction.cancel) {
                
                if (prevScreen.SelectedCarrierName != null) {

                    var sDriver = screen.Driver;
                    sDriver.Carrier = prevScreen.SelectedCarrierName;
                }
            }

        }
    });

};