﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.AddDischargePaymentsOld.AddPayment_execute = function (screen) {
    
    var cAmount = screen.TotalAmountDue;
    var sPayment = screen.EnteredPayment;
    var pMethod = sPayment.PaymentType;
    screen.SelectedPayMethod = pMethod.Id + ",";
    var pAmount = sPayment.PaymentAmount;
    var ttlDue = +cAmount - +pAmount;
    cAmount = ttlDue;

    var nPayment = new myapp.DischargePayment();
    nPayment.Discharge = screen.SelectedDischarge;
    nPayment.PaymentAmount = +cAmount.toFixed(2);
    screen.EnteredPayment = nPayment;

};

myapp.AddDischargePaymentsOld.ApplyPayment_execute = function (screen) {

        msls.promiseOperation(function (operation) {
          
            var applyAmount = +screen.TotalAmountDue;

            var sCharges = screen.SelectedCharges;

            var ePayment = screen.EnteredPayment;

            var pType = ePayment.PaymentType.Id;

            var pTypes;
            if (screen.SelectedPayMethod != null && screen.SelectedPayMethod != "") {
                pTypes = screen.SelectedPayMethod + "," + pType;
            }
            else
            {
                pTypes = pType;
            }

            var sDischarge = screen.SelectedDischarge.Id;

                $.ajax({
                    type: 'post',
                    data: { 'cList': sCharges, 'dId': sDischarge, 'ttlAmt': applyAmount, 'pTypes': pTypes },
                    url: '../Web/ApplyPayment.ashx',
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    })

                });
            
        }).then(function PromiseSuccess(PromiseResult) {

            var success = PromiseResult;
            
            if (success == "1") {
                myapp.commitChanges();
            }

        });
};

myapp.AddDischargePaymentsOld.EnteredPayment_PaymentType_postRender = function (element, contentItem) {
    
    contentItem.addChangeListener("value", function () {
        var iScrn = contentItem.screen;
        var currValue = contentItem.value;

        switch (currValue.Id) {

            case 1:
                break;
            case 2:
                
                var iElement = iScrn.findContentItem("EnteredPayment_Reference");
                
                iElement.details.displayName = "Check# ";
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                break;

        };

    });

};
