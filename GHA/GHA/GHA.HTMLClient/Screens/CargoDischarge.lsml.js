﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $iScrn;

myapp.CargoDischarge.ShowNotReleased_postRender = function (element, contentItem) {
    
    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Released' || span && span.text() == 'Customs Hold') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Released' || span && span.text() == 'Customs Hold') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};
myapp.CargoDischarge.ShowReleased_postRender = function (element, contentItem) {
    
    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Released' || span && span.text() == 'Customs Hold') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Released' || span && span.text() == 'Customs Hold') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};
myapp.CargoDischarge.ShowHolds_postRender = function (element, contentItem) {
    
    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Released' || span && span.text() == 'Released') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Released' || span && span.text() == 'Released') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};

myapp.CargoDischarge.RefreshButton_render = function (element, contentItem) {

    $iScrn = contentItem.screen;
    var a = function (s) {
        return function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                //s.SelectedUld.details.refresh();
                //s.SelectedFlt.details.refresh();
                //s.BreakdownAwbs.refresh();
                //s.FlightHistory.refresh();
                operation.complete();

            }));
        }
    }($iScrn);

    var div = $("<div class='am_refresh' id='myTest'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    div.on('click', a);
    $(element).append(div);

    $(element).trigger("create");

};
myapp.CargoDischarge.created = function (screen) {
   
    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            })
        })

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            var uItems = result.split(',');
            if (uItems.length > 1) {

                screen.CurrentUser = uItems[0];
                var sId = +uItems[1];
                myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {
                    screen.CurrentUserStation = selectedStation.results[0];
                });

            }
            else {

                screen.CurrentUser = uItems[0];
            }

        }
    }));

};