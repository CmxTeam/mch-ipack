﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $CurrentUser;
var $CurrentUserStation;
var $CurrentUserWarehouse;
var $SelectFlightUnloadingPort;
var $iScrn;

myapp.RecoverFlight.created = function (screen) {

    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    screen.CompletedFlights = false;
    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                var uItems = result.split(',');
                if (uItems.length > 2) {

                    screen.CurrentUser = uItems[0];
                    $CurrentUser = uItems[0];
                    var sId = +uItems[1];
                    myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {
                        screen.CurrentUserStation = selectedStation.results[0];
                        $CurrentUserStation = selectedStation.results[0];
                    });
                    var wId = +uItems[2];
                    myapp.activeDataWorkspace.WFSDBData.Warehouses_SingleOrDefault(+wId).execute().then(function (selectedWarehouse) {
                        screen.CurrentUserWarehouse = selectedWarehouse.results[0];
                        $CurrentUserWarehouse = selectedWarehouse.results[0];
                    });

                }

                else {
                    screen.CurrentUser = uItems[0];
                    $CurrentUser = uItems[0];
                }
            }
        }
    }));

};

myapp.RecoverFlight.GoToHome_execute = function (screen) {

    myapp.navigateHome();

};

myapp.RecoverFlight.Reset_execute = function (screen) {
    screen.Filter = null;
    screen.SearchFilter = null;
    screen.DateFilter = null;
    screen.SearchDateFilter = null;
    screen.DateStart = null;
    screen.DateEnd = null;
    fillFlightList(screen, true, 1);
};

//execute when item tapped
myapp.RecoverFlight.ViewFlightManifest_execute = function (screen) {

    var nPrt = screen.ReceiverFlightList.selectedItem;
    $SelectFlightUnloadingPort = screen.ReceiverFlightList.selectedItem;

    if (!nPrt) {
        nPrt = $SelectFlightUnloadingPort;
    }

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    myapp.showViewFlightManifest(nPrt, cUser, cStation, cWarehouse, aTimeout, 1);

};

myapp.RecoverFlight.ETALabel_postRender = function (element, contentItem) {
    element.textContent = "ETA:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.RecoverFlight.ULDLabel_postRender = function (element, contentItem) {
    element.textContent = "ULD:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.RecoverFlight.AWBLabel_postRender = function (element, contentItem) {
    element.textContent = "AWB:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.RecoverFlight.PCSLabel_postRender = function (element, contentItem) {
    element.textContent = "PCS:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.RecoverFlight.POU_ETA_postRender = function (element, contentItem) {

    var eDate = contentItem.value;

    dateFormat(eDate, element);

};

myapp.RecoverFlight.Group_postRender = function (element, contentItem) {

    $(element).css("overflow", "hidden");

};

myapp.RecoverFlight.FlightManifestList_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};

myapp.RecoverFlight.Incomplete_postRender = function (element, contentItem) {
    $(element).find('a').first().makeBackgroundRed();    

    $(element).on(getSuitableEventName(), function (e) {
         $(this).find('a').first().makeBackgroundRed();
        
        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Complete') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });
};

myapp.RecoverFlight.Complete_postRender = function (element, contentItem) {
    $(element).on(getSuitableEventName(), function (e) {
        $(this).find('a').first().makeBackgroundRed();        

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Incomplete') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });
};

myapp.RecoverFlight.RefreshButton_render = function (element, contentItem) {

    $iScrn = contentItem.screen;
    var elem = $("<div class='am_refresh'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    elem.on('click', function () {
        fillFlightList(contentItem.screen, true, 1);
    });
    $(element).append(elem);
    $(element).trigger("create");

};

function refreshView6() {
    //$iScrn.ReceiverFlightList.refresh();
};

myapp.RecoverFlight.Incomplete_execute = function (screen) {
    screen.DateFilter = null;
    screen.Filter = null;
    myapp.RecoverFlight.isComplete = false;    
    fillFlightList(screen, true, 1);

};

myapp.RecoverFlight.Complete_execute = function (screen) {
    screen.Filter = null;
    screen.DateFilter = new Date();
    myapp.RecoverFlight.isComplete = true;
    fillFlightList(screen, true, 1);
};

myapp.RecoverFlight.DateFilter_postRender = function (element, contentItem) {
    $(element).css('padding-top', 0);

    contentItem.addChangeListener(null, function () {
        if (myapp.RecoverFlight.isComplete && !contentItem.value) {
            contentItem.value = new Date();
        }
    });
};

myapp.RecoverFlight.OrgFlightNumber_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "30px");
    $(element).css("line-height", "32px");

};

myapp.RecoverFlight.ArrivalTime_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};

myapp.RecoverFlight.TotalULDs_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};

myapp.RecoverFlight.TotalAWBs_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};

myapp.RecoverFlight.TotalPieces_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};

myapp.RecoverFlight.Filter_postRender = function (element, contentItem) {
    $(element).find('input').attr('id', 'receiverFilterTextbox');
};

myapp.RecoverFlight.openEtaPopup = function (screen) {
    screen.showPopup("UpdateFlightEtaPopup");
}

myapp.RecoverFlight.ReceiverFlightList_render = function (element, contentItem) {
    var ul = $('<ul id="flightList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.RecoverFlight.listLoadPageNumber = 2;
    myapp.RecoverFlight.isComplete = false;
    $(element).on('scroll', function () {
        if ($('ul#flightList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            fillFlightList(contentItem.screen, false, myapp.RecoverFlight.listLoadPageNumber);
            myapp.RecoverFlight.listLoadPageNumber = myapp.RecoverFlight.listLoadPageNumber + 1;
        }
    });

    //fillFlightList(contentItem.screen, true, 1, myapp.RecoverFlight.isComplete);
};


function fillFlightList(screen, clearList, pageNumber) {
    var tDate = screen.DateFilter;
    var flightFilterDate = tDate ? (tDate.getMonth() + 1) + '/' + tDate.getDate() + '/' + tDate.getFullYear() : '';
    $.ajax({
        type: 'GET',
        data: { 'userId': screen.CurrentUser, 'rowPerPage': 10, 'pageNumber': pageNumber, 'isComplete': myapp.RecoverFlight.isComplete, 'filter': $('input#receiverFilterTextbox').last().val(), 'dateFilter': flightFilterDate },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetCargoReceiverFlights',
        success: function (data) {
            $('span#flightNoItems').remove();
            var list = $('ul#flightList').last();
            if (clearList) {
                list.empty();
                myapp.RecoverFlight.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:74px"></li>');
                var that = this;
                li.on('click', function () {
                    if (!screen.ReceiverFlightList) {
                        screen.ReceiverFlightList = {};
                    }

                    screen.ReceiverFlightList.selectedItem = that;
                    myapp.RecoverFlight.ViewFlightManifest_execute(screen);
                });

                var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                var carrierImage = $('<img class="float-left" style="width: 30px;"></img>');
                carrierImage.attr('src', this.CarrierLogoPath);
                var statusImage = $('<img class="float-left" style="width:30px;margin-top:15px;"></img>');
                statusImage.attr('src', this.StatusImagePath);
                statusContainer.append(carrierImage);
                statusContainer.append(statusImage);


                var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                var refNumber = $('<div style="font-size:30px">' + this.OrgFlightNumber + '</div>');
                var refNumber1 = $('<div style="font-size:18px"></div>');
                var acctNumber = $('<span style="margin-left:20px">ACCT: ' + this.AccountCode + '</span>')
                var etaDate = $('<span>ETA: ' + this.ETA + '</span>');
                var infoHolder = $('<div style="font-size:18px"></div>');
                var uldCount = $('<span>ULD:' + this.TotalULDs + '</span>');
                var awbCount = $('<span style="margin-left:20px">AWB: ' + this.TotalAWBs + '</span>');
                var piecesCount = $('<span style="margin-left:20px">PCS: ' + this.TotalPieces + '</span>');

                refNumber1.append(etaDate);
                refNumber1.append(acctNumber);

                infoHolder.append(uldCount);
                infoHolder.append(awbCount);
                infoHolder.append(piecesCount);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(infoHolder);

                var imageContainer = $('<div class="custom-image-container"></div>');
                if (myapp.permissions["LightSwitchApplication:ModifyFlightETA"]) {
                    var calendarButton = $('<div class="custom-snapshot-button" style="padding:20px"></div>');
                    var calendarImage = $('<img class="float-left" style="height:22px" src="../htmlclient/content/images/calendar_icon.png"></img>');
                    calendarButton.append(calendarImage);
                    calendarButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.ReceiverFlightList) {
                            screen.ReceiverFlightList = {};
                        }

                        screen.ReceiverFlightList.selectedItem = that;
                        var sItem = that;

                        var etaSplit = sItem.ETA.split(' ');

                        var dSplit = etaSplit[0].split('-');
                        var tSplit = etaSplit[1].split(':');
                        var amPm = 0;
                        if (tSplit[0] > 11) {
                            amPm = 1;
                        }
                        var mSplit = 0;

                        switch (dSplit[1]) {
                            case "Jan":
                                mSplit = 0;
                                break;
                            case "Feb":
                                mSplit = 1;
                                break;
                            case "Mar":
                                mSplit = 2;
                                break;
                            case "Apr":
                                mSplit = 3;
                                break;
                            case "May":
                                mSplit = 4;
                                break;
                            case "Jun":
                                mSplit = 5;
                                break;
                            case "Jul":
                                mSplit = 6;
                                break;
                            case "Aug":
                                mSplit = 7;
                                break;
                            case "Sep":
                                mSplit = 8;
                                break;
                            case "Oct":
                                mSplit = 9;
                                break;
                            case "Nov":
                                mSplit = 10;
                                break;
                            case "Dec":
                                mSplit = 11;
                                break;

                        }


                        screen.SelectedEta = new Date(parseInt("20" + dSplit[2]), mSplit, dSplit[0], tSplit[0], tSplit[1], amPm);
                        screen.SelectedFlight = sItem.OrgFlightNumber;
                        myapp.RecoverFlight.openEtaPopup(screen);
                    });

                    imageContainer.append(calendarButton);
                }

                li.append(statusContainer);
                li.append(refContainer);
                li.append(imageContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="flightNoItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get data from GetCargoReceiverFlights.' + e);
        }
    })
    //});
};
myapp.RecoverFlight.UpdateFlightEtaPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'updateEtaPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.RecoverFlight.UpdateEtaHeader_postRender = function (element, contentItem) {

    element.textContent = "Update Flight ETA";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.RecoverFlight.Reset1_execute = function (screen) {

    screen.SelectedEta = null;

};
myapp.RecoverFlight.UpdateFlightEta_execute = function (screen) {
    var tDate = screen.SelectedEta;

    var date = (tDate.getMonth() + 1) + '/' + tDate.getDate() + '/' + tDate.getFullYear() + ' ' + tDate.getHours() + ':' + tDate.getMinutes();

    $.ajax({
        type: 'post',
        data: { FlightId: screen.ReceiverFlightList.selectedItem.FlightManifestId, PortId: screen.ReceiverFlightList.selectedItem.PortId, Eta: date, Username: screen.CurrentUser },
        url: '../api/cargoreceiver/UpdateFlightEta',
        success: function () {
            fillFlightList(screen, true, 1);
            screen.closePopup("UpdateFlightEtaPopup");
        }
    });
};
myapp.RecoverFlight.SelectedFlight_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.RecoverFlight.SelectedEta_postRender = function (element, contentItem) {
    //$(element).removeClass("msls-font-style-normal msls-text");
    //$(element).find("span.ui-btn-inner").removeClass("ui-btn-inner");
    //$(element).css("font-size", "24px");
    //$(element).css("line-height", "24px");
};

myapp.RecoverFlight.Search_execute = function (screen) {
    fillFlightList(screen, true, 1);
};