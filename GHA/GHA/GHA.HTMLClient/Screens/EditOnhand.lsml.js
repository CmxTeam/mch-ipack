﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.EditOnhand.ShipperName_postRender = function (element, contentItem) {
   
    $(element).find('input').attr("readonly", "readonly");

};
myapp.EditOnhand.ConsigneeName_postRender = function (element, contentItem) {
    
    $(element).find('input').attr("readonly", "readonly");

};
myapp.EditOnhand.Destination_postRender = function (element, contentItem) {
    
    $(element).find('input').attr("readonly", "readonly");

};
myapp.EditOnhand.ShipperPopup_postRender = function (element, contentItem) {
   
    var container = $(element);
    container.attr('id', 'shipperPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.EditOnhand.ConsigneePopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'consigneePopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.EditOnhand.DestinationPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'destinationPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.EditOnhand.ShipperHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Select Shipper";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.EditOnhand.ConsigneeHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Select Consignee";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.EditOnhand.DestinationHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Select Destination";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.EditOnhand.ShipperLookup_execute = function (screen) {
    
    $('#shipperPopupContent').parent().center();
    screen.showPopup("ShipperPopup");

};
myapp.EditOnhand.ConsigneeLookup_execute = function (screen) {
    
    $('#consigneePopupContent').parent().center();
    screen.showPopup("ConsigneePopup");

};
myapp.EditOnhand.DestinationLookup_execute = function (screen) {
    
    $('#destinationPopupContent').parent().center();
    screen.showPopup("DestinationPopup");

};
myapp.EditOnhand.IATACode_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.EditOnhand.DestinationSearch_execute = function (screen) {
    
    screen.SearchDestinationFilter = screen.DestinationFilter;

};
myapp.EditOnhand.DestinationReset_execute = function (screen) {
    
    screen.SearchDestinationFilter = null;
    screen.DestinationFilter = null;

};
myapp.EditOnhand.SetDestination_execute = function (screen) {
    
    screen.SelectedDestination = screen.Destinations.selectedItem;
    screen.Destination = screen.Destinations.selectedItem.IATACode;

    screen.closePopup("DestinationPopup");

};
myapp.EditOnhand.ShipperNext_execute = function (screen) {
    
    screen.closePopup("ShipperPopup").then(function () {

        $('#consigneePopupContent').parent().center();
        screen.showPopup("ConsigneePopup");

    });

};
myapp.EditOnhand.ShipperCancel_execute = function (screen) {
    
    screen.closePopup("ShipperPopup");

};
myapp.EditOnhand.ConsigneePrevious_execute = function (screen) {
    
    screen.closePopup("ConsigneePopup").then(function () {

        $('#shipperPopupContent').parent().center();
        screen.showPopup("ShipperPopup");

    });

};
myapp.EditOnhand.ConsigneeNext_execute = function (screen) {
    
    screen.closePopup("ConsigneePopup").then(function () {

        $('#destinationPopupContent').parent().center();
        screen.showPopup("DestinationPopup");

    });

};
myapp.EditOnhand.ConsigneeCancel_execute = function (screen) {
    
    screen.closePopup("ConsigneePopup");

};
myapp.EditOnhand.DestinationPrevious_execute = function (screen) {
    
    screen.closePopup("DestinationPopup").then(function () {

        $('#consigneePopupContent').parent().center();
        screen.showPopup("ConsigneePopup");

    });

};
myapp.EditOnhand.DestinationCancel_execute = function (screen) {
    
    screen.closePopup("DestinationPopup");

};
myapp.EditOnhand.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.EditOnhand.DimUOM_postRender = function (element, contentItem) {
    
    element.textContent = "IN";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");
    $(element).css("overflow", "hidden");

};
myapp.EditOnhand.WeightUOM_postRender = function (element, contentItem) {
    
    element.textContent = "LBS";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "50px");
    $(element).css("overflow", "hidden");

};
myapp.EditOnhand.AddPackagePopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'addPackagePopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.EditOnhand.AddPackageHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.EditOnhand.OpenAddPackagesPopup_execute = function (screen) {
   
    screen.AddPackageHeader = "Add Package";
    screen.findContentItem("PackagePieces").isEnabled = true;
    screen.findContentItem("AddPackage").isVisible = true;
    screen.findContentItem("UpdatePackage").isVisible = false;

    screen.PackagePieces = null;
    screen.PackageType = null;
    screen.PackageLength = null;
    screen.PackageWidth = null;
    screen.PackageHeight = null;
    screen.PackageWgt = null;
    screen.IsHazmat = false;

    $('#addPackagePopupContent').parent().center();
    screen.showPopup("AddPackagePopup");

};
myapp.EditOnhand.PackageReset_execute = function (screen) {

    if (screen.AddPackageHeader == "Edit Package") {
        screen.PackageType = null;
        screen.PackageLength = null;
        screen.PackageWidth = null;
        screen.PackageHeight = null;
        screen.PackageWgt = null;
        screen.IsHazmat = false;
    }
    else {
        screen.PackagePieces = null;
        screen.PackageType = null;
        screen.PackageLength = null;
        screen.PackageWidth = null;
        screen.PackageHeight = null;
        screen.PackageWgt = null;
        screen.IsHazmat = false;
    }

};
myapp.EditOnhand.PackageList_render = function (element, contentItem) {
    
    var ul = $('<ul id="packageList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.EditOnhand.listLoadPageNumber = 2;
    myapp.EditOnhand.isComplete = false;
    $(element).on('scroll', function () {
        if ($('ul#packageList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            fillPackageList(contentItem.screen, false, myapp.EditOnhand.listLoadPageNumber);
            myapp.EditOnhand.listLoadPageNumber = myapp.EditOnhand.listLoadPageNumber + 1;
        }
    });

    fillPackageList(contentItem.screen, true, 1);

};

function fillPackageList(screen, clearList, pageNumber) {
 
    var oHand = screen.SelectedOnhand;

    $.ajax({
        type: 'GET',
        data: { 'OnhandId': oHand.OnhandId, 'userId': screen.CurrentUser, 'rowPerPage': 10, 'pageNumber': pageNumber, 'filter': $('input#packageFilterTextbox').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetOnhandPackages',
        success: function (data) {
            $('span#packageNoItems').remove();
            var list = $('ul#packageList').last();
            if (clearList) {
                list.empty();
                myapp.EditOnhand.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:74px"></li>');
                var that = this;
                //li.on('click', function () {
                //    if (!screen.PackageList) {
                //        screen.PackageList = {};
                //    }

                //    screen.PackageList.selectedItem = that;
                //    myapp.EditOnhand.ViewPackages_execute(screen);
                //});

                
                var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                if (this.IsHazmat == true) {
                    var hazmatImage = $('<img class="float-left" style="width: 30px;" src="content/images/hazmat_icon.png"></img>');
                    statusContainer.append(hazmatImage);
                }
                    li.append(statusContainer);
                

                var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                var refNumber = $('<div style="font-size:30px">' + this.PackageNumber + ' ' + this.PackageType + '</div>');
                var refNumber1 = $('<div style="font-size:18px"></div>');
                var pDim = $('<span>' + this.Length + ' X ' + this.Width + ' X ' + this.Height + ' ' + this.DimUOM + '</span>')
                var pWeight = $('<span>' + this.Weight + ' ' + this.WeightUOM + '</span>');
                var infoHolder = $('<div style="font-size:18px"></div>');
                
                var imageContainer = $('<div class="custom-image-container"></div>');

                var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px;"></div>');
                var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
                snapshotButton.append(snapshotImage);
                snapshotButton.on('click', function (e) {
                    e.stopPropagation();
                    if (!screen.PackageList) {
                        screen.PackageList = {};
                    }
                    screen.PackageList.selectedItem = that;
                    myapp.EditOnhand.ShowOnhandSnapshot_execute(screen);
                });

                var deleteButton = $('<div class="custom-snapshot-button" style="padding:14px; margin-right:10px;"></div>');
                var deleteImage = $('<img class="float-left" style="height:22px" src="content/images/delete_icon_w.png"></img>');
                deleteButton.append(deleteImage);
                deleteButton.on('click', function (e) {
                    e.stopPropagation();
                    
                    if (!screen.PackageList) {
                        screen.PackageList = {};
                    }
                    screen.PackageList.selectedItem = that;
                    $('#deletePackagePopupContent').parent().center();
                    screen.showPopup("DeletePackagePopup");
                    
                });

                var editButton = $('<div class="custom-snapshot-button" style="padding:14px; margin-right:10px;"></div>');
                var editImage = $('<img class="float-left" style="height:22px" src="content/images/edit_icon_w.png"></img>');
                editButton.append(editImage);
                editButton.on('click', function (e) {
                    e.stopPropagation();
                    if (!screen.PackageList) {
                        screen.PackageList = {};
                    }
                    screen.PackageList.selectedItem = that;

                    screen.AddPackageHeader = "Edit Package";
                    
                    screen.findContentItem("PackagePieces").isEnabled = false;
                    screen.findContentItem("AddPackage").isVisible = false;
                    screen.findContentItem("UpdatePackage").isVisible = true;

                    myapp.activeDataWorkspace.WFSDBData.PackageTypes_SingleOrDefault(that.PackageTypeId).execute().then(function (selectedPackageType) {
                        var st = $('#selectedPackageType');
                        st.val(selectedPackageType.results[0].Name);
                        st.attr('data-id', selectedPackageType.results[0].Id);
                    });

                    screen.PackagePieces = +1;
                    //screen.PackageType = null;
                    screen.PackageLength = that.Length;
                    screen.PackageWidth = that.Width;
                    screen.PackageHeight = that.Height;
                    screen.PackageWgt = that.Weight;
                    screen.IsHazmat = that.IsHazmat;

                    $('#addPackagePopupContent').parent().center();
                    screen.showPopup("AddPackagePopup");
                    
                });



                
                
                imageContainer.append(editButton);
                imageContainer.append(deleteButton);
                imageContainer.append(snapshotButton);


                refNumber1.append(pDim);
                infoHolder.append(pWeight);               

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(infoHolder);

                li.append(imageContainer);
                li.append(refContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="packageNoItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get data from GetPackageList.' + e);
        }
    })
};

myapp.EditOnhand.AddPackage_execute = function (screen) {
    
    var oHand = screen.SelectedOnhand;
    var pType = $('#selectedPackageType').attr('data-id');
    var pLength = $('#lengthTextbox').val();
    var pWidth = $('#widthTextbox').val();
    var pHeight = $('#heightTextbox').val();
    var pPieces = $('#piecesTextbox').val();
    var pWeight = $('#weightTextbox').val();
    var pId = null;

    $.ajax({
        type: 'post',
        data: {
            'UserName': screen.CurrentUser,
            'OnhandId': oHand.OnhandId,
            'Count': pPieces,
            'Length':pLength,
            'Width': pWidth,
            'Height': pHeight,
            'DimUOMId': 1,
            'Weight': pWeight,
            'WeightUOMId': 3,
            'PackageTypeId': pType,
            'IsHazmat': screen.IsHazmat,
            'PackageId': pId
        },
        url: '../api/cargoreceiver/AddEditOnhandPackage',
        success: function () {
            checkOnhandForHazmat(screen);
            fillPackageList(screen, true, 1);
            myapp.EditOnhand.PackageReset_execute(screen);
        },
        error: function (e) { alert("Unable to add package.  " + e); }
    }); 

};
myapp.EditOnhand.PackageSearch_execute = function (screen) {
    
    fillPackageList(screen, true, 1);

};
myapp.EditOnhand.PackagesReset_execute = function (screen) {
    
    screen.SearchPackageFilter = null
    screen.PackageFilter = null;

    fillPackageList(screen, true, 1);

};
myapp.EditOnhand.PackageFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'packageFilterTextbox');

};
myapp.EditOnhand.DeletePackageText_postRender = function (element, contentItem) {
    
    element.textContent = "Do you want to delete this package?";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.EditOnhand.DeletePackageHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Delete Package";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.EditOnhand.DeletePackagePopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'deletePackagePopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.EditOnhand.ConfirmNo_execute = function (screen) {
    
    screen.closePopup("Delete Package Popup");

};
myapp.EditOnhand.created = function (screen) {

    var oHand = screen.SelectedOnhand;
    var sDest = null;

    $.ajax({
        type: 'GET',
        data: { 'onhandId': oHand.OnhandId },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetOnhandDetails',
        success: function (data) {
            screen.ShipperName = data.Shipper;
            screen.ConsigneeName = data.Consignee;
            screen.DescriptionOfGoods = data.GoodsDesc;
            screen.Destination = data.DestinationCode;
            var sDest = data.DestinationId;
    
            myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(sDest).execute().then(function (selectedDest) {
                screen.SelectedDestination = selectedDest.results[0];
            });

            myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(6).execute().then(function (selectedEntity) {
                screen.OnhandEntityType = selectedEntity.results[0];
            });
        
        },
        error: function (e) {
            alert("Unable to get Onhand Details." + e);
        }   
    });

    checkOnhandForHazmat(screen);

};


function checkOnhandForHazmat(screen)
{
    var oHand = screen.SelectedOnhand;

    $.ajax({
        type: 'GET',
        data: { 'onhandId': oHand.OnhandId },
        contentType: 'application/json',
        url: '../api/cargoreceiver/CheckOnhandForHazmat',
        success: function (data) {
            
            if(data == "HAZMAT")
            {
                screen.findContentItem("DGFChecklist").isVisible = true;
            }
            else
            {
                screen.findContentItem("DGFChecklist").isVisible = false;
            }

        },
        error: function (e) {
            alert("Unable to check onhand for hazmat." + e);
        }
    });
}

    myapp.EditOnhand.ShowOnhandSnapshot_execute = function (screen) {
    
        var tOnhand = screen.SelectedOnhand;
        var tPackage = screen.PackageList.selectedItem;
        var uDescULD = tOnhand.OnhandNumber + ' - ' + tPackage.PackageNumber;
        var cTask = tOnhand.TaskId;

        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        myapp.showSnapshot(screen.OnhandEntityType, cTask, screen.CurrentUser, screen.CurrentUserWarehouse, tOnhand.OnhandId, uDescULD, aTimeout);


    };
    myapp.EditOnhand.ConfirmYes_execute = function (screen) {

        var tOnhand = screen.SelectedOnhand;
        var tPackage = screen.PackageList.selectedItem;
        var cUser = screen.CurrentUser;

        $.ajax({
            type: 'post',
            data: {
                'onhandId': tOnhand.OnhandId,
                'packageId': tPackage.PackageId,
                'userName': cUser
            },
            url: '../api/cargoreceiver/DeleteOnhandPackage',
            success: function () {
                fillPackageList(screen, true, 1);
                screen.closePopup("DeletePackagePopup");
            },
            error: function (e) {
                alert("Unable to delete package.  " + e);
            }
        });

    };
    myapp.EditOnhand.UpdatePackage_execute = function (screen) {
    
        var oHand = screen.SelectedOnhand;
        var pType = $('#selectedPackageType').attr('data-id');
        var tPackage = screen.PackageList.selectedItem;
        var pLength = $('#lengthTextbox').val();
        var pWidth = $('#widthTextbox').val();
        var pHeight = $('#heightTextbox').val();
        var pPieces = $('#piecesTextbox').val();
        var pWeight = $('#weightTextbox').val();

        $.ajax({
            type: 'post',
            data: {
                'UserName': screen.CurrentUser,
                'OnhandId': oHand.OnhandId,
                'Count': pPieces,
                'Length': pLength,
                'Width': pWidth,
                'Height': pHeight,
                'DimUOMId': 1,
                'Weight': pWeight,
                'WeightUOMId': 3,
                'PackageTypeId': pType,
                'IsHazmat': screen.IsHazmat,
                'PackageId': tPackage.PackageId
            },
            url: '../api/cargoreceiver/AddEditOnhandPackage',
            success: function () {
                screen.closePopup("AddPackagePopup");
                checkOnhandForHazmat(screen);
                fillPackageList(screen, true, 1);
            },
            error: function (e) { alert("Unable to update package.  " + e); }
        });

    };
    myapp.EditOnhand.SaveAddtionalDetails_execute = function (screen) {
    
        var oHand = screen.SelectedOnhand;
        var sLoc = screen.SelectedDestination;
        var sShip = $('#shipperTextbox').val();
        var sCons = $('#consigneeTextbox').val();
        var sGoods = $('#descGoodsTextbox').val();
        var locId = null;
        if (sLoc)
        {
            locId = sLoc.Id;
        }


        $.ajax({
            type: 'post',
            data: {
                'OnhandId': oHand.OnhandId,
                'ShipperName': sShip,
                'ConsigneeName': sCons,
                'DestinationId': locId,
                'GoodsDescription': sGoods,
                'UserName': screen.CurrentUser
            },
            url: '../api/cargoreceiver/EditOnhandDetails',
            success: function () {
                alert("Save Complete");
            },
            error: function (e) { alert("Unable to update package.  " + e); }
        });

    };
    myapp.EditOnhand.DgrChecklist_render = function (element, contentItem) {

        var ul = $('<div id="dgrList"></div>')
        $(element).append(ul);
        myapp.EditOnhand.listLoadPageNumber = 2;
        $(element).on('scroll', function () {
            if ($('div#dgrList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
                fillDgrList(contentItem.screen, false, myapp.EditOnhand.listLoadPageNumber);
                setDGRItems(contentItem.screen, myapp.EditOnhand.listLoadPageNumber);
                getDGRStatus(contentItem.screen, 0);
                myapp.EditOnhand.listLoadPageNumber = myapp.EditOnhand.listLoadPageNumber + 1;
            }
        });

        fillDgrList(contentItem.screen, true, 1);
        setDGRItems(contentItem.screen, 1);
        getDGRStatus(contentItem.screen, 0);

    };

    function fillDgrList(screen, clearList, pageNumber) {

        var oHand = screen.SelectedOnhand;

        $.ajax({
            type: 'GET',
            data: { 'rowPerPage': 10, 'pageNumber': pageNumber },
            contentType: 'application/json',
            url: '../api/cargoreceiver/GetDGRCheckListItems',
            success: function (data) {
                //$('span#dgrNoItems').remove();
                var list = $('div#dgrList').last();
                if (clearList) {
                    list.empty();
                    myapp.EditOnhand.listLoadPageNumber = 2;
                }
                var activeHeader = null;
            

                $(data).each(function () {
                   
                    if(activeHeader != this.ListItemHeader)
                    {
                    
                        activeHeader = this.ListItemHeader;

                        var hd = $('<div style="font-size:20px !important; font-weight:bold;">' + this.ListItemHeader + '</div>'); 
                        var lItem = $('<div>' + this.ListItemNo + '. ' + this.ListItem + '</div>');
                        var rSpace = $('<div style="padding:10px;" />');
                        list.append(hd);
                        list.append(lItem);

                        if (this.SubLineNo != "S") {

                            var fSet = $('<fieldset data-role="controlgroup" data-type="horizontal" style="margin-left:13px;" />');

                            var vList = this.ValueList.split(",");
                            var choiceList = [];
                            for (i = 0; i < vList.length; i++) {

                                var rButton = $('<input type="radio" name="' + this.ItemId + '" id="' + this.ItemId + vList[i] + '" value="' + vList[i] + '"  />');
                                var rLabel = $('<label for="' + this.ItemId + vList[i] + '" style="font-size:24;">' + vList[i] + '</label>');

                                if (vList[i] == "NO")
                                {
                                    rButton.on('click', function(){
                                        
                                            screen.CheckListItemId = $(this).attr('id').slice(0, -2);

                                            screen.DGRItemComment = null;

                                            var oHand = screen.SelectedOnhand;
                                            var sTask = screen.SelectedTask;
                                            var eType = screen.OnhandEntityType;
                                            var sItemId = $(this).attr('id').slice(0, -2);;
                                            
                                            //alert(eType.Id + " | " + oHand.OnhandId + " | " + sItemId + " | " + sTaskId);

                                            $.ajax({
                                                type: 'GET',
                                                data: {
                                                    'EntityTypeId': eType.Id,
                                                    'EntityId': oHand.OnhandId,
                                                    'CheckListItemId': sItemId,
                                                    'TaskId': sTask.Id
                                                },
                                                url: '../api/cargoreceiver/GetDGRCheckListItemComment',
                                                success: function (data) {
                                                    screen.DGRItemComment = data;
                                                    if (data)
                                                    {
                                                        screen.findContentItem("AddDGRItemComment").isVisible = false;
                                                        screen.findContentItem("UpdateDGRItemComment").isVisible = true;
                                                    }
                                                    else
                                                    {
                                                        screen.findContentItem("AddDGRItemComment").isVisible = true;
                                                        screen.findContentItem("UpdateDGRItemComment").isVisible = false;
                                                    }

                                                    $('#commentDGRPopupContent').parent().center();
                                                    screen.showPopup("AddDGRItemCommentPopup");
                                                },
                                                error: function (e) { alert("Unable to get DGR checklist item comment.  " + e); }
                                            });

                                            

                                    });
                                }

                                fSet.append(rButton);
                                fSet.append(rLabel);

                            }

                            var commentButton = $("<div id='" + this.ItemId + "comment' class='comment-presenter' style='display: none;'><img src='content/images/comment_icon_w.png' style='height: 25px;' /></div>");
                            commentButton.on('click', function () {

                                screen.CheckListItemId = $(this).attr('id').slice(0, -7);

                                screen.DGRItemComment = null;

                                var oHand = screen.SelectedOnhand;
                                var sTask = screen.SelectedTask;
                                var eType = screen.OnhandEntityType;
                                var sItemId = $(this).attr('id').slice(0, -7);

                                //alert(eType.Id + " | " + oHand.OnhandId + " | " + sItemId + " | " + sTaskId);

                                $.ajax({
                                    type: 'GET',
                                    data: {
                                        'EntityTypeId': eType.Id,
                                        'EntityId': oHand.OnhandId,
                                        'CheckListItemId': sItemId,
                                        'TaskId': sTask.Id
                                    },
                                    url: '../api/cargoreceiver/GetDGRCheckListItemComment',
                                    success: function (data) {
                                        screen.DGRItemComment = data;

                                        if (data) {
                                            screen.findContentItem("AddDGRItemComment").isVisible = false;
                                            screen.findContentItem("UpdateDGRItemComment").isVisible = true;
                                        }
                                        else {
                                            screen.findContentItem("AddDGRItemComment").isVisible = true;
                                            screen.findContentItem("UpdateDGRItemComment").isVisible = false;
                                        }

                                        $('#commentDGRPopupContent').parent().center();
                                        screen.showPopup("AddDGRItemCommentPopup");
                                    },
                                    error: function (e) { alert("Unable to get DGR checklist item comment.  " + e); }
                                });

                                
                            });
                            
                            fSet.append(commentButton);
                            list.append(fSet);

                        }
                    
                        list.append(rSpace);
                    }
                    else
                    {
                        var lItem = $('<div>' + this.ListItemNo + '. ' + this.ListItem + '</div>');

                        var fSet = $('<fieldset data-role="controlgroup" data-type="horizontal" style="margin-left:13px;" />');

                        var rSpace = $('<div style="padding:10px;" />');

                        list.append(lItem);

                        if (this.SubLineNo != "S") {
                            var vList = this.ValueList.split(",");
                            var choiceList = [];
                            for (i = 0; i < vList.length; i++) {

                                var rButton = $('<input type="radio" name="' + this.ItemId + '" id="' + this.ItemId + vList[i] + '" value="' + vList[i] + '"  />');
                                var rLabel = $('<label for="' + this.ItemId + vList[i] + '" style="font-size:24;">' + vList[i] + '</label>');

                                if (vList[i] == "NO") {
                                    rButton.on('click', function () {
                                            
                                        screen.CheckListItemId = $(this).attr('id').slice(0, -2);

                                        screen.DGRItemComment = null;

                                        var oHand = screen.SelectedOnhand;
                                        var sTask = screen.SelectedTask;
                                        var eType = screen.OnhandEntityType;
                                        var sItemId = $(this).attr('id').slice(0, -2);;

                                        //alert(eType.Id + " | " + oHand.OnhandId + " | " + sItemId + " | " + sTask.Id);

                                        $.ajax({
                                            type: 'GET',
                                            data: {
                                                'EntityTypeId': eType.Id,
                                                'EntityId': oHand.OnhandId,
                                                'CheckListItemId': sItemId,
                                                'TaskId': sTask.Id
                                            },
                                            url: '../api/cargoreceiver/GetDGRCheckListItemComment',
                                            success: function (data) {
                                                screen.DGRItemComment = data;

                                                if (data) {
                                                    screen.findContentItem("AddDGRItemComment").isVisible = false;
                                                    screen.findContentItem("UpdateDGRItemComment").isVisible = true;
                                                }
                                                else {
                                                    screen.findContentItem("AddDGRItemComment").isVisible = true;
                                                    screen.findContentItem("UpdateDGRItemComment").isVisible = false;
                                                }

                                                $('#commentDGRPopupContent').parent().center();
                                                screen.showPopup("AddDGRItemCommentPopup");
                                            },
                                            error: function (e) { alert("Unable to get DGR checklist item comment.  " + e); }
                                        });

                                    });
                                }

                                fSet.append(rButton);
                                fSet.append(rLabel);

                            }
                            var commentButton = $("<div id='" + this.ItemId + "comment' class='comment-presenter' style='display: none;'><img src='content/images/comment_icon_w.png' style='height: 25px;' /></div>");
                            commentButton.on('click', function () {

                                screen.CheckListItemId = $(this).attr('id').slice(0, -7);

                                screen.DGRItemComment = null;

                                var oHand = screen.SelectedOnhand;
                                var sTask = screen.SelectedTask;
                                var eType = screen.OnhandEntityType;
                                var sItemId = $(this).attr('id').slice(0, -7);

                                //alert(eType.Id + " | " + oHand.OnhandId + " | " + sItemId + " | " + sTask.Id);

                                $.ajax({
                                    type: 'GET',
                                    data: {
                                        'EntityTypeId': eType.Id,
                                        'EntityId': oHand.OnhandId,
                                        'CheckListItemId': sItemId,
                                        'TaskId': sTask.Id
                                    },
                                    url: '../api/cargoreceiver/GetDGRCheckListItemComment',
                                    success: function (data) {
                                        screen.DGRItemComment = data;

                                        if (data) {
                                            screen.findContentItem("AddDGRItemComment").isVisible = false;
                                            screen.findContentItem("UpdateDGRItemComment").isVisible = true;
                                        }
                                        else {
                                            screen.findContentItem("AddDGRItemComment").isVisible = true;
                                            screen.findContentItem("UpdateDGRItemComment").isVisible = false;
                                        }

                                        $('#commentDGRPopupContent').parent().center();
                                        screen.showPopup("AddDGRItemCommentPopup");
                                    },
                                    error: function (e) { alert("Unable to get DGR checklist item comment.  " + e); }
                                });

                            });
                            
                            fSet.append(commentButton);
                            list.append(fSet);
                           
                        }

                        list.append(rSpace);
                    
                    }
                    $('#dgrList').trigger('create');
                    var that = this;
                
                });
            
            },
            error: function (e) {
                console.log('Unable get data from GetDgrList.' + e);
            }
        });
    };
        
    myapp.EditOnhand.SaveDGRChecklist_execute = function (screen) {
    
        var sList = "";

        $('div#dgrList').find('input:radio').each(function () {
            if ($(this).is(':checked')) {
                sList += $(this).attr('name') + ',' + $(this).val() + '|';
            }
        });
        sList = sList.slice(0, sList.length - 1);

        var oHand = screen.SelectedOnhand;
        var sTask = screen.SelectedTask;
        var cUser = screen.CurrentUser;
        var eType = screen.OnhandEntityType;

        $.ajax({
            type: 'post',
            data: {
                'EntityTypeId': eType.Id, 
                'EntityId': oHand.OnhandId,
                'Value': sList,
                'UserName': cUser,
                'TaskId': sTask.Id
            },
            url: '../api/cargoreceiver/SaveDGRCheckListResults',
            success: function () {
                getDGRStatus(screen, 1);
            
                //fillPackageList(screen, true, 1);
                //myapp.EditOnhand.PackageReset_execute(screen);
            },
            error: function (e) { alert("Unable to save DGR checklist items.  " + e); }
        });

    
    };
    myapp.EditOnhand.ResetDGRChecklist_execute = function (screen) {
    
        $('#confirmResetPopupContent').parent().center();
        screen.showPopup("ConfirmResetPopup");

    };
    myapp.EditOnhand.ReferenceList_render = function (element, contentItem) {
    
        var ul = $('<ul id="referenceList" class="custom-list"></ul>')
        $(element).append(ul);
        myapp.EditOnhand.listLoadPageNumber = 2;
        myapp.EditOnhand.isComplete = false;
        $(element).on('scroll', function () {
            if ($('ul#referenceList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
                fillReferenceList(contentItem.screen, false, myapp.EditOnhand.listLoadPageNumber);
                myapp.EditOnhand.listLoadPageNumber = myapp.EditOnhand.listLoadPageNumber + 1;
            }
        });

        fillReferenceList(contentItem.screen, true, 1);

    };

    function fillReferenceList(screen, clearList, pageNumber) {

        var oHand = screen.SelectedOnhand;
        var rId = null;

        $.ajax({
            type: 'GET',
            data: { 'ReferenceId': rId, 'EntityTypeId': 6, 'EntityId': oHand.OnhandId, 'RowsPerPage': 10, 'PageNumber': pageNumber, 'Filter': $('input#referenceFilterTextbox').last().val() },
            contentType: 'application/json',
            url: '../api/cargoreceiver/GetEntityReferences',
            success: function (data) {
                $('span#referenceNoItems').remove();
                var list = $('ul#referenceList').last();
                if (clearList) {
                    list.empty();
                    //myapp.EditOnhand.listLoadPageNumber = 2;
                }

                $(data).each(function () {
                    var li = $('<li class="custom-list-item" style="height:74px"></li>');
                    var that = this;
                    li.on('click', function () {
                        if (!screen.ReferenceList) {
                            screen.ReferenceList = {};
                        }

                        screen.ReferenceList.selectedItem = that;
                        myapp.EditOnhand.ViewReferences_execute(screen);
                    });

                    var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                    var refNumber = $('<div style="font-size:18px">' + this.ReferenceType+ '</div>');
                    var refNumber1 = $('<div style="font-size:30px">' + this.Value + '</div>');
                    var rUser = $('<span>Entered By: ' + this.UserName + '</span>');
                    var infoHolder = $('<div style="font-size:18px"></div>');

                    var imageContainer = $('<div class="custom-image-container"></div>');

                    var deleteButton = $('<div class="custom-snapshot-button" style="padding:14px; margin-right:10px;"></div>');
                    var deleteImage = $('<img class="float-left" style="height:22px" src="content/images/delete_icon_w.png"></img>');
                    deleteButton.append(deleteImage);
                    deleteButton.on('click', function (e) {
                        e.stopPropagation();

                        if (!screen.ReferenceList) {
                            screen.ReferenceList = {};
                        }
                        screen.ReferenceList.selectedItem = that;
                        $('#deleteReferencePopupContent').parent().center();
                        screen.showPopup("DeleteReferencePopup");

                    });

                    var editButton = $('<div class="custom-snapshot-button" style="padding:14px; margin-right:10px;"></div>');
                    var editImage = $('<img class="float-left" style="height:22px" src="content/images/edit_icon_w.png"></img>');
                    editButton.append(editImage);
                    editButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.ReferenceList) {
                            screen.ReferenceList = {};
                        }
                        screen.ReferenceValue = "";
                        screen.ReferenceList.selectedItem = that;

                        screen.AddReferenceHeader = "Edit Reference";

                        screen.findContentItem("AddNewReference").isVisible = false;
                        screen.findContentItem("UpdateReference").isVisible = true;

                        myapp.activeDataWorkspace.WFSDBData.ReferenceTypes_SingleOrDefault(that.ReferenceTypeId).execute().then(function (selectedReferenceType) {
                            screen.SelectedRefType = selectedReferenceType.results[0];
                        });

                    
                        var st = $('#selectedReferenceType');
                        st.val('');
                        st.attr('data-id', '');
                    
                        $('#addReferencePopupContent').parent().center();
                        screen.showPopup("AddReferencePopup").then(function () {
                            setTimeout(function () {
                                var st = $('#selectedReferenceType');
                                st.val(screen.SelectedRefType.ReferenceType1);
                                st.attr('data-id', screen.SelectedRefType.Id);

                                screen.ReferenceValue = that.Value;
                            }, 500);});                 

                    });

                    imageContainer.append(editButton);
                    imageContainer.append(deleteButton);
                    //imageContainer.append(snapshotButton);


                    //refNumber1.append(pDim);
                    infoHolder.append(rUser);

                    refContainer.append(refNumber);
                    refContainer.append(refNumber1);
                    refContainer.append(infoHolder);

                    li.append(imageContainer);
                    li.append(refContainer);
                    list.append(li);
                });

                if (!list.find('li').length) {
                    list.before('<span id="referenceNoItems">No Items</span>')
                }
            },

            error: function (e) {
                console.log('Unable get data from GetEntityReferences.' + e);
            }
        })
    };

    myapp.EditOnhand.ConfirmResetPopup_postRender = function (element, contentItem) {
    
        var container = $(element);
        container.attr('id', 'confirmResetPopupContent');
        container.parent().center();

        var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

        container.children().first().before(header);
        container.css("-moz-border-radius", "10px");
        container.css("-webkit-border-radius", "10px");
        container.css("border-radius", "10px");

    };
    myapp.EditOnhand.ConfirmResetHeader_postRender = function (element, contentItem) {
    
        element.textContent = "Confirm Reset";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "36px");
        $(element).css("line-height", "36px");
        $(element).css("font-weight", "bold");
        $(element).css("margin-top", "4px");

    };
    myapp.EditOnhand.ConfirmResetText_postRender = function (element, contentItem) {
    
        element.textContent = "Do you want to reset all fields?";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "24px");
        $(element).css("line-height", "24px");

    };
    myapp.EditOnhand.ConfirmResetYes_execute = function (screen) {
    
        screen.closePopup("ConfirmResetPopup");

        $('div#dgrList').find('input:radio').each(function () {
            if ($(this).is(':checked')) {
                $(this).prop('checked', false);
            }
        });

    };
    myapp.EditOnhand.ConfirmResetNo_execute = function (screen) {
    
        screen.closePopup("ConfirmResetPopup");

    };
    myapp.EditOnhand.ViewReferences_execute = function (screen) {
    


    };
    myapp.EditOnhand.DeleteReferencePopup_postRender = function (element, contentItem) {
    
        var container = $(element);
        container.attr('id', 'deleteReferencePopupContent');
        container.parent().center();

        var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

        container.children().first().before(header);
        container.css("-moz-border-radius", "10px");
        container.css("-webkit-border-radius", "10px");
        container.css("border-radius", "10px");

    };
    myapp.EditOnhand.DeleteReferenceHeader_postRender = function (element, contentItem) {
    
        element.textContent = "Delete Reference";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "36px");
        $(element).css("line-height", "36px");
        $(element).css("font-weight", "bold");
        $(element).css("margin-top", "4px");

    };
    myapp.EditOnhand.DeleteReferenceText_postRender = function (element, contentItem) {
    
        element.textContent = "Do you want to delete this reference?";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "24px");
        $(element).css("line-height", "24px");

    };
    myapp.EditOnhand.ConfirmRefNo_execute = function (screen) {
    
        screen.closePopup("DeleteReferencePopup");

    };
    myapp.EditOnhand.AddReferencePopup_postRender = function (element, contentItem) {
    
        var container = $(element);
        container.attr('id', 'addReferencePopupContent');
        container.parent().center();

        var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

        container.children().first().before(header);
        container.css("-moz-border-radius", "10px");
        container.css("-webkit-border-radius", "10px");
        container.css("border-radius", "10px");

    };
    myapp.EditOnhand.AddReferenceHeader_postRender = function (element, contentItem) {
    
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "36px");
        $(element).css("line-height", "36px");
        $(element).css("font-weight", "bold");
        $(element).css("margin-top", "4px");

    };
    myapp.EditOnhand.AddReferences_execute = function (screen) {
    
        screen.AddReferenceHeader = "Add Reference";

        screen.findContentItem("AddNewReference").isVisible = true;
        screen.findContentItem("UpdateReference").isVisible = false;

        screen.ReferenceValue = "";

        var st = $('#selectedReferenceType');
        st.val('');
        st.attr('data-id', '');

        $('#addReferencePopupContent').parent().center();
        screen.showPopup("AddReferencePopup");

    };
    myapp.EditOnhand.SelectedReferenceType_render = function (element, contentItem) {
    
        $(element).append('<input id="selectedReferenceType" type="text" readonly></input>');

    };
    myapp.EditOnhand.AddNewReference_execute = function (screen) {
    
        var oHand = screen.SelectedOnhand;
        var rId = null;
        var refVal = $('#refValTextbox').val();

        $.ajax({
            type: 'post',
            data: {
                'ReferenceId': rId,
                'EntityTypeId': 6,
                'EntityId': oHand.OnhandId,
                'ReferenceTypeId': $('#selectedReferenceType').attr('data-id'),
                'ReferenceValue': refVal,
                'UserName': screen.CurrentUser
            },
            url: '../api/cargoreceiver/AddEditEntityReferences',
            success: function () {
                fillReferenceList(screen, true, 1);
                myapp.EditOnhand.ResetReference_execute(screen);
            },
            error: function (e) { alert("Unable to add reference.  " + e); }
        });

    };
    myapp.EditOnhand.UpdateReference_execute = function (screen) {
    
        var oHand = screen.SelectedOnhand;
        var sRef = screen.ReferenceList.selectedItem;
        var refVal = $('#refValTextbox').val();

        $.ajax({
            type: 'post',
            data: {
                'ReferenceId': sRef.ReferenceId,
                'EntityTypeId': 6,
                'EntityId': oHand.OnhandId,
                'ReferenceTypeId': $('#selectedReferenceType').attr('data-id'),
                'ReferenceValue': refVal,
                'UserName': screen.CurrentUser
            },
            url: '../api/cargoreceiver/AddEditEntityReferences',
            success: function () {
                screen.closePopup("AddReferencePopup");
                fillReferenceList(screen, true, 1);
                myapp.EditOnhand.ResetReference_execute(screen);
            },
            error: function (e) { alert("Unable to update reference.  " + e); }
        });

    };
    myapp.EditOnhand.ResetReference_execute = function (screen) {
    
        var st = $('#selectedReferenceType');
        st.val('');
        st.attr('data-id', '');

        screen.ReferenceValue = null;

    };
    myapp.EditOnhand.OpenReferenceTypes_execute = function (screen) {
    
        screen.closePopup("AddReferencePopup").then(function () {
            screen.showPopup("ReferenceTypePopup");
        });

    };
    myapp.EditOnhand.ReferenceTypePopup_postRender = function (element, contentItem) {
    
        var container = $(element);
        container.attr('id', 'selectRefTypePopupContent');
        container.parent().center();
        var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

        container.children().first().before(header);
        container.css("-moz-border-radius", "10px");
        container.css("-webkit-border-radius", "10px");
        container.css("border-radius", "10px");

    };
    myapp.EditOnhand.ReferenceTypeHeader_postRender = function (element, contentItem) {
    
        element.textContent = "Reference Type";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "36px");
        $(element).css("line-height", "36px");
        $(element).css("font-weight", "bold");
        $(element).css("margin-top", "4px");

    };
    myapp.EditOnhand.ReferenceTypesContainer_render = function (element, contentItem) {

        var accountId = null;

        var ul = $('<ul class="reference-type-list"></ul>');
        $(element).append(ul);

        $.ajax({
            type: 'GET',
            data: { 'AccountId': accountId },
            contentType: 'application/json',
            url: '../api/cargoreceiver/GetReferenceTypes',
            success: function (data) {
                $(data).each(function () {
                    var li = $('<li data-id="' + this.Id + '">' + this.Name + '</li>');
                    li.on(getSuitableEventName(), function () {
                        var st = $('#selectedReferenceType');
                        st.val($(this).text());
                        st.attr('data-id', $(this).attr('data-id'));

                        contentItem.screen.closePopup("ReferenceTypePopup").then(function () {
                            contentItem.screen.showPopup("AddReferencePopup");
                        });
                    });
                    ul.append(li);
                });
            },
            error: function (e) {
                alert('Unable get reference types: ' + e.message);
            }
        });
    }

    myapp.EditOnhand.IsHazmat_postRender = function (element, contentItem) {
        $(element).css('width', '105px');
        setTimeout(function () {
            $(element).find('> div').css('width', '103px').css('height', '76px');
            $(element).find('> div a').css('height', '72px');
            $(element).find('> div > span').css('font-size', '38px');
            $(element).find('> div > span:last').css('padding-right', '15px');
            $(element).find('> div > span:first').css('padding-left', '14px');
        }, 200);

        $(element).find('input').attr('id', 'isHazmatSlider');
    };

    myapp.EditOnhand.PackageType_render = function (element, contentItem) {
        $(element).append('<input id="selectedPackageType" type="text" readonly></input>');
    };
    myapp.EditOnhand.SelectPackageType_execute = function (screen) {
        screen.closePopup("AddPackagePopup").then(function () {
            screen.showPopup("SelectPackageTypesPopup");
        });
    };
    myapp.EditOnhand.SelectPackageType_postRender = function (element, contentItem) {
        $(element).css('margin-top', '16px');
    };
    myapp.EditOnhand.PackageTypeContainer_render = function (element, contentItem) {
        var ul = $('<ul class="shipment-type-list"></ul>');
        $(element).append(ul);

        $.ajax({
            type: 'GET',
            contentType: 'application/json',
            url: '../api/cargoreceiver/GetPackageTypes',
            success: function (data) {
                $(data).each(function () {
                    var li = $('<li data-id="' + this.Id + '">' + this.Name + '</li>');
                    li.on(getSuitableEventName(), function () {
                        var st = $('#selectedPackageType');
                        st.val($(this).text());
                        st.attr('data-id', $(this).attr('data-id'));

                        contentItem.screen.closePopup("SelectPackageTypesPopup").then(function () {
                            contentItem.screen.showPopup("AddPackagePopup");
                        });
                    });
                    ul.append(li);
                });
            },
            error: function (e) {
                alert('Unable get package types: ' + e.message);
            }
        });
    };
    myapp.EditOnhand.PackageTypeHeader_postRender = function (element, contentItem) {
        element.textContent = "Package Type";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "36px");
        $(element).css("line-height", "36px");
        $(element).css("font-weight", "bold");
        $(element).css("margin-top", "4px");
    };

    myapp.EditOnhand.ConfirmRefYes_execute = function (screen) {
    
        var tTask = screen.SelectedTask;
        var tReference = screen.ReferenceList.selectedItem;
        var cUser = screen.CurrentUser;

        $.ajax({
            type: 'post',
            data: {
                'TaskId': tTask.Id,
                'ReferenceId': tReference.ReferenceId,
                'UserName': cUser
            },
            url: '../api/cargoreceiver/DeleteEntityReferences',
            success: function () {
                fillReferenceList(screen, true, 1);
                screen.closePopup("DeleteReferencePopup");
            },
            error: function (e) {
                alert("Unable to delete reference.  " + e);
            }
        });

    };
    myapp.EditOnhand.ReferenceFilter_postRender = function (element, contentItem) {
    
        $(element).find('input').attr('id', 'referenceFilterTextbox');

    };
    myapp.EditOnhand.ReferenceReset_execute = function (screen) {
    
        screen.SearchReferenceFilter = null
        screen.ReferenceFilter = null;

        fillReferenceList(screen, true, 1);

    };
    myapp.EditOnhand.ReferenceSearch_execute = function (screen) {
   
        fillReferenceList(screen, true, 1);

    };
    myapp.EditOnhand.SelectPackageTypesPopup_postRender = function (element, contentItem) {
    
        var container = $(element);
        container.attr('id', 'selectPackTypesPopupContent');
        container.parent().center();
        var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

        container.children().first().before(header);
        container.css("-moz-border-radius", "10px");
        container.css("-webkit-border-radius", "10px");
        container.css("border-radius", "10px");

    };
    myapp.EditOnhand.CheckinHistoryFilter_render = function (element, contentItem) {
        $(element).append('<input type="text" id="checkinHistoryFilter"></input>');
    };
    myapp.EditOnhand.EditOnhandHistoryContainer_render = function (element, contentItem) {
        var table = $('<table id="onhandHistory" class="msls-table ui-responsive table-stripe msls-hstretch ui-table ui-table-reflow">' +
                        '<thead>' +
                            '<tr>' +
                                '<td style="width:50px; font-weight:bold">Date</td>' +
                                '<td style="width:125px; font-weight:bold">Reference</td>' +
                                '<td style="width:125px; font-weight:bold">Description</td>' +
                                '<td style="width:100px; font-weight:bold">User</td>' +
                            '</tr>' +
                        '</thead>' +
                        '<tbody id="onhandHistoryBody"></tbody>' +
                       '</table>');
        $(element).append(table);
        myapp.EditOnhand.historyLoadPageNumber = 2;
        $(element).on('scroll', function () {
            if ($('table#onhandHistory').last().height() === $(this).scrollTop() + $(this).height() + 6) {
                createOnhandHistory(contentItem.screen.SelectedOnhand.OnhandId, myapp.EditOnhand.historyLoadPageNumber, false);
                myapp.EditOnhand.historyLoadPageNumber = myapp.EditOnhand.historyLoadPageNumber + 1;
            }
        });

        createOnhandHistory(contentItem.screen.SelectedOnhand.OnhandId, 1, true);
    };

    function createOnhandHistory(onhandId, pageNumber, clearList) {
        $.ajax({
            type: 'get',
            data: { 'onhandId': onhandId, 'filter': $('#checkinHistoryFilter').last().val(), 'rowsPerPage': 20, 'pageNumber': pageNumber },
            url: '../api/cargoreceiver/GetOnhandHistory',
            success: function (data) {
                var tbody = $('tbody#onhandHistoryBody').last();

                if (clearList) {
                    tbody.empty();
                    myapp.EditOnhand.historyLoadPageNumber = 2;
                }

                $(data).each(function () {
                    tbody.append('<tr class="msls-tr msls-style ui-shadow ui-tr msls-presenter msls-ctl-table-row-layout msls-vauto msls-hstretch msls-presenter-content msls-font-style-normal msls-hscroll msls-table-row">' +
                                    '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Date + '</td>' +
                                    '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Reference + '</td>' +
                                    '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Description + '</td>' +
                                    '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.UserName + '</td>' +
                                 '</tr>');
                });
            },
            error: function (e) {
                console.log('Error History: ' + e);
            }
        });
    }
    myapp.EditOnhand.SearchOnhandHistoryTap_execute = function (screen) {
        createOnhandHistory(screen.SelectedOnhand.OnhandId, 1, true);
    };
    myapp.EditOnhand.ResetOnhandHistoryTap_execute = function (screen) {
        $('input#checkinHistoryFilter').val('');
        createOnhandHistory(screen.SelectedOnhand.OnhandId, 1, true);
    };
    myapp.EditOnhand.CheckerSignature_render = function (element, contentItem) {
        var cName = contentItem.screen.CurrentUserName;
        //Create the control & attach to the DOM
        var sig = $("<div id='cksignature' class='signature-presenter'></div>");
        var sName = $('<div id="sName" style="position:relative; text-align:center; padding-bottom:10px;" ><span style="font-size:18px; line-height:22px;"><b>' + cName + '</b></span>');
    
        sig.appendTo($(element));
        setTimeout(function () {
            //Initialize and start capturing
            sig.jSignature();
            sig.append(sName);
            // Listen for changes made via the custom control and update the 
            // content item whenever it changes.  
            sig.bind("change", function (e) {
                var img = sig.jSignature("getData", "image");
                if (img != null) {
                    if (contentItem.value != img[1]) {
                        contentItem.value = img[1];
                    }
                }
            });
        }, 0);

    };
    myapp.EditOnhand.OpenCompleteDGR_execute = function (screen) {
    
        screen.DGRPlace = null;
        screen.DGRComments = null;

        if ($("#cksignature").length > 0) {
            $("#cksignature").jSignature("reset");
        }

        $('#completeDGRPopupContent').parent().center();
        screen.showPopup("CompleteDGRPopup");

    };
    myapp.EditOnhand.CompleteDGRPopup_postRender = function (element, contentItem) {
    
        var container = $(element);
        container.attr('id', 'completeDGRPopupContent');
        container.parent().center();

        var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

        container.children().first().before(header);
        container.css("-moz-border-radius", "10px");
        container.css("-webkit-border-radius", "10px");
        container.css("border-radius", "10px");

    };
    myapp.EditOnhand.CompleteDGRHeader_postRender = function (element, contentItem) {
    
        element.textContent = "Finalize DGR Checklist";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "36px");
        $(element).css("line-height", "36px");
        $(element).css("font-weight", "bold");
        $(element).css("margin-top", "4px");

    };
    myapp.EditOnhand.ResetSignature_execute = function (screen) {
    
        $("#cksignature").jSignature("reset");

    };
    myapp.EditOnhand.CancelCompleteDGR_execute = function (screen) {
    
        screen.closePopup("CompleteDGRPopup");

    };
    myapp.EditOnhand.DGFChecklist_postRender = function (element, contentItem) {

        var evtName = 'click';

        if ('ontouchstart' in document.documentElement) {
            evtName = 'touchstart';
        }

        $($("span").filter(function () { return $(this).text() === "DGR" })).parent().on(evtName, function (e) {
            setTimeout(function () {
                getDGRStatus(contentItem.screen, 0);
            }, 500);
        });

    };

    function getDGRStatus(screen, openFinalize)
    {
        var oHand = screen.SelectedOnhand;
        var sTask = screen.SelectedTask;
        var eType = screen.OnhandEntityType;
        var cUser = screen.CurrentUser;

        $.ajax({
            type: 'get',
            data: {
                'EntityTypeId': eType.Id,
                'EntityId': oHand.OnhandId,            
                'TaskId': sTask.Id,
                'UserName': cUser
            },
            url: '../api/cargoreceiver/CheckForDGRCheckListStatus',
            success: function (data) {

                if(data.IsChecklistFinalized)
                {
                    screen.findContentItem("SaveDGRChecklist").isVisible = false;
                    screen.findContentItem("OpenCompleteDGR").isVisible = false;
                    screen.findContentItem("ResetDGRChecklist").isVisible = false;
                    screen.findContentItem("ReopenDGRChecklist").isVisible = true;

                    $('div#dgrList').find('input:radio').each(function () {
                 
                        $(this).prop('disabled', true);
                      
                    });
                }
                else
                {
                    screen.findContentItem("SaveDGRChecklist").isVisible = true;
                    screen.findContentItem("ResetDGRChecklist").isVisible = true;
                    if (data.TotalQuestions == data.Answered) {
                        screen.findContentItem("OpenCompleteDGR").isVisible = true;
                        screen.findContentItem("ReopenDGRChecklist").isVisible = false;
                        if(openFinalize==1)
                        {
                            screen.DGRPlace = null;
                            screen.DGRComments = null;

                            if ($("#cksignature").length > 0) {
                                $("#cksignature").jSignature("reset");
                            }

                            $('#completeDGRPopupContent').parent().center();
                            screen.showPopup("CompleteDGRPopup");
                        }
                    }
                    else
                    {
                        if (openFinalize == 1) {
                            alert("Save Complete.");
                        }
                        screen.findContentItem("OpenCompleteDGR").isVisible = false;
                        screen.findContentItem("ReopenDGRChecklist").isVisible = false;
                    }
                }

            },
            error: function (e) {
                console.log('Unable to set DGR checklist items: ' + e);
            }
        });
    }

    function setDGRItems(screen, pageNumber) {

        var oHand = screen.SelectedOnhand;
        var sTask = screen.SelectedTask;
        var eType = screen.OnhandEntityType;

        $.ajax({
            type: 'get',
            data: { 
                'EntityTypeId': eType.Id,
                'EntityId': oHand.OnhandId,
                'RowsPerPage': 10,
                'PageNumber': pageNumber,
                'TaskId': sTask.Id
            },
            url: '../api/cargoreceiver/GetDGRCheckListResults',
            success: function (data) {

                $(data).each(function () {

                    $('input:radio[name="' + this.ItemId + '"][value="' + this.ResultValue + '"]').prop('checked', true);

                    if(this.ListItemComment)
                    {
                        $('#' + this.ItemId + "comment").show("fast");
                    }
                    else
                    {
                        $('#' + this.ItemId + "comment").hide("fast");
                    }
              
                });
            },
            error: function (e) {
                console.log('Unable to set DGR checklist items: ' + e);
            }
        });

    };

    function countDGRItems(screen) {
        var ttlCount = +0;
        var selCount = +0;
        $('div#dgrList').find('input:radio').each(function () {
            ttlCount++;
            if ($(this).is(':checked')) {
                selCount++;
            }
        });
        alert(selCount + ' of ' + ttlCount);
    };


    myapp.EditOnhand.CompleteDGRChecklist_execute = function (screen) {
    
        var oHand = screen.SelectedOnhand;
        var sTask = screen.SelectedTask;
        var cUser = screen.CurrentUser;
        var eType = screen.OnhandEntityType;
        var sPlace = $('#dgrPlaceTextbox').val();
        var sComments = $('#dgrCommentTextbox').val();

        var signImage = $("#cksignature").jSignature("getData");

        $.ajax({
            type: 'post',
            data: {
                'EntityTypeId': eType.Id,
                'EntityId': oHand.OnhandId,
                'Comments': sComments,
                'Place': sPlace,
                'UserName': cUser,
                'TaskId': sTask.Id,
                'SigImage': signImage
            },
            url: '../web/FinalizeDGRChecklist.ashx',
            success: function () {
                getDGRStatus(screen, 0);
                screen.closePopup("CompleteDGRPopup");

            },
            error: function (e) { alert("Unable to save DGR checklist items.  " + e); }
        });

    };
    myapp.EditOnhand.AddDGRItemCommentPopup_postRender = function (element, contentItem) {
    
        var container = $(element);
        container.attr('id', 'commentDGRPopupContent');
        container.parent().center();

        var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

        container.children().first().before(header);
        container.css("-moz-border-radius", "10px");
        container.css("-webkit-border-radius", "10px");
        container.css("border-radius", "10px");

    };
    myapp.EditOnhand.AddDGRCommentHeader_postRender = function (element, contentItem) {
    
        element.textContent = "DGR Item Comment";
        $("div.msls-text").removeClass("msls-font-style-normal msls-text");
        $(element).css("font-size", "36px");
        $(element).css("line-height", "36px");
        $(element).css("font-weight", "bold");
        $(element).css("margin-top", "4px");

    };

myapp.EditOnhand.CancelDGRItemComment_execute = function (screen) {
    
    screen.closePopup("AddDGRItemCommentPopup");

};
myapp.EditOnhand.AddDGRItemComment_execute = function (screen) {
    
    $('#dgrItemCommentTextbox').blur();

    if(!screen.DGRItemComment)
    {
        alert("Comment is required.");
    }
    else
    {
        var oHand = screen.SelectedOnhand;
        var sTask = screen.SelectedTask;
        var cUser = screen.CurrentUser;
        var eType = screen.OnhandEntityType;
        var sComment = $('#dgrItemCommentTextbox').val();
        var sItemId = screen.CheckListItemId;

        $.ajax({
            type: 'post',
            data: {
                'EntityTypeId': eType.Id,
                'EntityId': oHand.OnhandId,
                'CheckListItemId': sItemId,
                'Comment': sComment,
                'UserName': cUser,
                'TaskId': sTask.Id
            },
            url: '../api/cargoreceiver/SaveDGRCheckListItemComment',
            success: function () {
                $('#' + sItemId + "comment").show("fast");
                screen.closePopup("AddDGRItemCommentPopup");

            },
            error: function (e) { alert("Unable to save DGR checklist item comment.  " + e); }
        });
    }

};
myapp.EditOnhand.FinalizeOnhandPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'finalizeOnhandPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");
};
myapp.EditOnhand.FinalizeOnhandHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Finalize Onhand";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.EditOnhand.FinalizeOnhandText_postRender = function (element, contentItem) {
    
    element.textContent = "Do you want to finalize this onhand?";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.EditOnhand.FinalizeOnhand_execute = function (screen) {
    
    $('#finalizeOnhandPopupContent').parent().center();
    screen.showPopup("FinalizeOnhandPopup");

};
myapp.EditOnhand.FinalizeNo_execute = function (screen) {
    
    screen.closePopup("FinalizeOnhandPopup");

};
myapp.EditOnhand.FinalizeYes_execute = function (screen) {
    
    var oHand = screen.SelectedOnhand;
    var cUser = screen.CurrentUser;
    var sTask = screen.SelectedTask

    $.ajax({
        type: 'post',
        data: {
            'UserName': cUser,
            'OnhandId': oHand.OnhandId,
            'TaskId': sTask.Id
        },
        url: '../api/cargoreceiver/FinalizeOnhand',
        success: function () {
            myapp.showOnhandReceipt(screen.AjaxTimeout);
        },
        error: function (e) { alert("Unable to finalize onhand.  " + e); }
    });

};
myapp.EditOnhand.PackageWgt_postRender = function (element, contentItem) {
   
    $(element).find('input').attr('id', 'weightTextbox');

};
myapp.EditOnhand.ReferenceValue_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'refValTextbox');

};
myapp.EditOnhand.PackageLength_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'lengthTextbox');

};
myapp.EditOnhand.PackageWidth_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'widthTextbox');

};
myapp.EditOnhand.PackageHeight_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'heightTextbox');

};
myapp.EditOnhand.PackagePieces_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'piecesTextbox');

};
myapp.EditOnhand.DGRItemComment_postRender = function (element, contentItem) {
    
    $(element).find('textarea').attr('id', 'dgrItemCommentTextbox');

};
myapp.EditOnhand.ShipperName1_postRender = function (element, contentItem) {
    
    $(element).find('textarea').attr('id', 'shipperTextbox');

};
myapp.EditOnhand.ConsigneeName1_postRender = function (element, contentItem) {
    
    $(element).find('textarea').attr('id', 'consigneeTextbox');

};
myapp.EditOnhand.DescriptionOfGoods_postRender = function (element, contentItem) {
    
    $(element).find('textarea').attr('id', 'descGoodsTextbox');

};
myapp.EditOnhand.DGRComments_postRender = function (element, contentItem) {
    
    $(element).find('textarea').attr('id', 'dgrCommentTextbox');

};
myapp.EditOnhand.DGRPlace_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'dgrPlaceTextbox');

};
myapp.EditOnhand.UpdateDGRItemComment_execute = function (screen) {
    
    $('#dgrItemCommentTextbox').blur();

    if (!screen.DGRItemComment) {
        alert("Comment is required.");
    }
    else {
        var oHand = screen.SelectedOnhand;
        var sTask = screen.SelectedTask;
        var cUser = screen.CurrentUser;
        var eType = screen.OnhandEntityType;
        var sComment = $('#dgrItemCommentTextbox').val();
        var sItemId = screen.CheckListItemId;

        $.ajax({
            type: 'post',
            data: {
                'EntityTypeId': eType.Id,
                'EntityId': oHand.OnhandId,
                'CheckListItemId': sItemId,
                'Comment': sComment,
                'UserName': cUser,
                'TaskId': sTask.Id
            },
            url: '../api/cargoreceiver/SaveDGRCheckListItemComment',
            success: function () {
                $('#' + sItemId + "comment").show("fast");
                screen.closePopup("AddDGRItemCommentPopup");

            },
            error: function (e) { alert("Unable to save DGR checklist item comment.  " + e); }
        });
    }

};