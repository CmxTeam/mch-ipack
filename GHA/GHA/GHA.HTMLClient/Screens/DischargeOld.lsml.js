﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.DischargeOld.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.DischargeOld.Reset_execute = function (screen) {
    
    screen.Filter = null;

};
myapp.DischargeOld.AddDischarge_execute = function (screen) {
    
    //myapp.showDischargeCheckIn(null,{
    //    beforeShown: function (addEditScreen) {

    //        var nDischarge = new myapp.Discharge();

    //        addEditScreen.Discharge = nDischarge;

    //    }
    //});

    myapp.showDischargeCheckIn();

};
myapp.DischargeOld.created = function (screen) {
   
    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            })
        })

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            var uItems = result.split(',');
            if (uItems.length > 1) {

                screen.CurrentUser = uItems[0];
                var sId = +uItems[1];
                myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {

                    screen.CurrentUserStation = selectedStation.results[0];

                });

            }
            else {

                screen.CurrentUser = uItems[0];
            }
        }
    }));

};
myapp.DischargeOld.Group_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.DischargeOld.Discharges_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.DischargeOld.ActiveCheckIns_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.DischargeOld.Driver_FirstName1_postRender = function (element, contentItem) {
    // Write code here.

};