﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.DischargeCheckInOld.created = function (screen) {

  

    if (screen.Discharge != null) {
        var dTask = screen.Discharge;
        if (dTask.Driver != null) {
            var dDriver = dTask.Driver;
            screen.SelectedDriver = dDriver;
            screen.DriverName = dDriver.FirstName + " " + dDriver.LastName;
        }

        if (dTask.TotalAmountDue != null) {
            if (dTask.TotalAmountPaid != null) {
                screen.AmountDue = (+dTask.TotalAmountDue - +dTask.TotalAmountPaid);
            }
            else {
                screen.AmountDue = +dTask.TotalAmountDue;
            }
        }
        else {
            screen.AmountDue = 0;
        }
    }

    //msls.promiseOperation(GetLoggedUser).then(function PromiseSuccess(PromiseResult) {

        myapp.activeDataWorkspace.WFSDBData.Statuses_SingleOrDefault(12).execute().then(function (selectedStatus) {

            myapp.activeDataWorkspace.WFSDBData.Statuses_SingleOrDefault(13).execute().then(function (selectedStatus1) {

                myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(2).execute().then(function (selectedEntity) {

                    myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(3).execute().then(function (selectedEntity1) {

                        myapp.activeDataWorkspace.WFSDBData.TaskTypes_SingleOrDefault(3).execute().then(function (selectedTaskType) {

                            screen.CheckInInProgress = selectedStatus.results[0];
                            screen.CheckInComplete = selectedStatus1.results[0];
                            screen.DischargeTaskType = selectedTaskType.results[0];
                            screen.AwbEntityType = selectedEntity.results[0];
                            screen.HwbEntityType = selectedEntity1.results[0];
                            //screen.CurrentUser = PromiseResult;

                        });
                    });
                });
            });
        });
   // });

};

myapp.DischargeCheckInOld.CollectPayment_execute = function (screen) {
    
    myapp.showDischargePayments(screen.Discharge, {
        afterClosed: function () {

            if (screen.Discharge != null) {
                var dTask = screen.Discharge;
                if (dTask.TotalAmountDue != null) {
                    if (dTask.TotalAmountPaid != null) {
                        screen.AmountDue = (+dTask.TotalAmountDue - +dTask.TotalAmountPaid);
                    }
                    else {
                        screen.AmountDue = +dTask.TotalAmountDue;
                    }
                }
                else {
                    screen.AmountDue = 0;
                }
            }
            screen.Discharge.details.refresh();
            screen.DischargeDetails.refresh();

        }
    });

};

myapp.DischargeCheckInOld.SelectDriver_execute = function (screen) {
    
    myapp.showSelectDriver(screen.Discharge, {
        beforeShown:function(dScreen){
            
            if (screen.Discharge == null) {
                var nDischarge = new myapp.Discharge();
                nDischarge.TotalDeliveryOrders = 0;
                nDischarge.TotalAmountDue = 0;
                nDischarge.TotalAmountPaid = 0;
                nDischarge.Status = screen.CheckInInProgress;
                nDischarge.CheckInTimestamp = new Date();
                screen.Discharge = nDischarge;
                dScreen.SelectedDischarge = nDischarge;
            }

        },
        afterClosed: function () {

            if (screen.Discharge != null) {
                var dTask = screen.Discharge;
                if (dTask.Driver != null) {
                    var dDriver = dTask.Driver;
                    screen.SelectedDriver = dDriver;
                    screen.DriverName = dDriver.FirstName + " " + dDriver.LastName;
                }
                if (dTask.TotalAmountDue != null) {
                    if (dTask.TotalAmountPaid != null) {
                        screen.AmountDue = (+dTask.TotalAmountDue - +dTask.TotalAmountPaid);
                    }
                    else {
                        screen.AmountDue = +dTask.TotalAmountDue;
                    }
                }
                else {
                    screen.AmountDue = 0;
                }
            }

            screen.Discharge.details.refresh();
            screen.DischargeDetails.refresh();

        }
    });
};

myapp.DischargeCheckInOld.AddDeliveryOrder_execute = function (screen) {
   
    myapp.showDischargeShipments(screen.Discharge, {
        afterClosed: function () {

            screen.Discharge.details.refresh().then(function () {

                if (screen.Discharge != null) {
                    var dTask = screen.Discharge;
                    if (dTask.TotalAmountDue != null) {
                        if (dTask.TotalAmountPaid != null) {
                            screen.AmountDue = (+dTask.TotalAmountDue - +dTask.TotalAmountPaid);
                        }
                        else {
                            screen.AmountDue = +dTask.TotalAmountDue;
                        }
                    }
                    else {
                        screen.AmountDue = 0;
                    }
                }
            });
        }
    });
};

myapp.DischargeCheckInOld.CompleteCheckIn_execute = function (screen) {
    
    var cfrm = confirm("Do you want to complete check-in?");

    if (cfrm == true) {
        var tDischarge = screen.Discharge;

        tDischarge.Status = screen.CheckInComplete;
        tDischarge.StatusTimestamp = new Date();

        myapp.commitChanges();
    }

};
myapp.DischargeCheckInOld.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};