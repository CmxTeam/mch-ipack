﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.TaskManager.ActionName_postRender = function (element, contentItem) {

    //$(element).find("div.msls.text").css("line-height", "100px");
    //$(element).find("div.msls.text").css("font-size", "28px");
    //$(element).find("div.msls.text").css("vertical-align", "middle");
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("line-height", "28px");
    $(element).css("font-size", "28px");
    $(element).css("padding-top", "15");
      
};

myapp.TaskManager.SelectTaskType_execute = function (screen) {
    
    var tType = screen.TaskTypes.selectedItem;

    switch(tType.Id)
    {

        case 1:
            myapp.showCargoReceiverTasks();
            break;
        case 2:
            myapp.showCargoInventoryTasks();
            break;
        case 3:
            break;
        default:
            break;

    }

};

myapp.TaskManager.GoToHome_execute = function (screen) {
    myapp.navigateHome();
};
