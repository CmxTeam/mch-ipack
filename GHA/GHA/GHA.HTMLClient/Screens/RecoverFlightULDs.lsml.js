﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />
var $RecoverQuantityInput;
var $LocationFilterInput1;
var $SelectedUld1;
var $iScrn1;

myapp.RecoverFlightULDs.created = function (screen) {

    //screen.findContentItem("ShowNotRecovered");

    var nFlt = screen.SelectedFlight;

    if (nFlt.Status.Id > 2) {

        screen.findContentItem("OpenAddUldPopup").isVisible = false;
        screen.findContentItem("RecoverSelected").isVisible = false;
        screen.findContentItem("OpenFinalize").isVisible = false;
        screen.findContentItem("OpenReopenFlight").isVisible = true;
      
    }
    else {
        screen.findContentItem("OpenAddUldPopup").isVisible = true;
        screen.findContentItem("RecoverSelected").isVisible = true;
        screen.findContentItem("OpenFinalize").isVisible = true;
        screen.findContentItem("OpenReopenFlight").isVisible = false;

    }

    screen.StatusId = +1;
    screen.StatusId1 = +2;
    screen.StatusId2 = +100000;

    screen.details.displayName = "Recover FLT# " + nFlt.Carrier.CarrierCode + nFlt.FlightNumber;

    myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(4).execute().then(function (selectedEntity) {

        screen.ULDEntityType = selectedEntity.results[0];

    });

};

myapp.RecoverFlightULDs.Recover_execute = function (screen) {


    screen.ConfirmPopupHeader = "Recover ULD";
    screen.ConfirmText = "Do you want to recover ULD?";

    screen.closePopup("RecoverPopup").then(function () {

        $('#confirmPopupContent').parent().center();

        screen.showPopup("ConfirmPopup");

    });
};

myapp.RecoverFlightULDs.RecoverSelected_execute = function (screen) {

    $('#recoverMultiplePopupContent').parent().center();

    screen.showPopup("RecoverMultipleUlds");
};

myapp.RecoverFlightULDs.GoToHome_execute = function (screen) {

    myapp.navigateHome();

};

myapp.RecoverFlightULDs.PercentRecovered_render = function (element, contentItem) {

    var UList = contentItem.parent;
    var UItem = UList.value;
    var sInd = UItem.TotalReceivedCount;
    var eInd = UItem.TotalUnitCount;

    progress(contentItem.value, sInd, eInd, "uProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var UList = contentItem.parent;
        var UItem = UList.value;
        var sInd = UItem.TotalReceivedCount;
        var eInd = UItem.TotalUnitCount;
        
        progress(contentItem.value, sInd, eInd, "uProgressBar", element);

    });

};

myapp.RecoverFlightULDs.SelectedFlight_PercentRecovered_render = function (element, contentItem) {

    var fList = contentItem.parent;
    var fscrn = fList.value;
    var fItem = fscrn.SelectedFlight;
    var sInd = fItem.RecoveredULDs;
    var eInd = fItem.TotalULDs;

    progress(contentItem.value, sInd, eInd, "hProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var fList = contentItem.parent;
        var fscrn = fList.value;
        var fItem = fscrn.SelectedFlight;
        var sInd = fItem.RecoveredULDs;
        var eInd = fItem.TotalULDs;

        progress(contentItem.value, sInd, eInd, "hProgressBar", element);

    });
   
};

myapp.RecoverFlightULDs.Reset_execute = function (screen) {
    $('input#uldRecoverFilter').last().val('');
    fillRecoverFlightList(screen, true, 1);

    //screen.Filter = null;
    //screen.SearchFilter = null;

};

myapp.RecoverFlightULDs.Reset1_execute = function (screen) {

    screen.HistoryFilter = null;
    screen.SearchHistoryFilter = null;

};

myapp.RecoverFlightULDs.RecoverQuantity_postRender = function (element, contentItem) {

    $RecoverQuantityInput = $("input", $(element));
    var $iControl = $(element).find("input");
    $iControl.attr("type", "text");
    $iControl.attr("readonly", "readonly");
    $iControl.addClass("inlineTarget");

};

myapp.RecoverFlightULDs.SetStageLocation_execute = function (screen) {

    var nFlt = screen.SelectedFlight;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var nLoc = screen.WarehouseLocations.selectedItem;
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'FltId': nFlt.Id, 'LocId': nLoc.Id, 'cUser': cUser },
            url: '../Web/SetStagingLocation.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;
        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                screen.closePopup("SetStageLocationPopup").then(function () {
                    screen.SelectedFlight.details.refresh();
                    screen.FlightHistory.refresh();
                });
            }
        }
    }));
};

myapp.RecoverFlightULDs.Reset2_execute = function (screen) {
    
    screen.LocationFilter = null;
    screen.SearchLocationFilter = null;

};

myapp.RecoverFlightULDs.LocationFilter_postRender = function (element, contentItem) {
    
    $LocationFilterInput1 = $("input", $(element));
    $(element).find('input').attr('id', 'locationFilterTextbox1');

};

myapp.RecoverFlightULDs.SetStageLocationPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'stageLocationPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.RecoverFlightULDs.SetStagePopupHeader_postRender = function (element, contentItem) {

    element.textContent = "Set Stage Location";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.RecoverFlightULDs.RecoverPopupHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Loose Quantity";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.RecoverFlightULDs.RecoverBUP_execute = function (screen) {

    screen.ConfirmPopupHeader = "Recover ULD as BUP";
    screen.ConfirmText = "Do you want to recover ULD as BUP?";

    screen.closePopup("RecoverPopup").then(function () {

        $('#confirmPopupContent').parent().center();

        screen.showPopup("ConfirmPopup");

    });
};

//execute on item tap
myapp.RecoverFlightULDs.OpenRecoverPopup_execute = function (screen) {
    
    var nUld = screen.ULDs.selectedItem;
    var selectedFlt = screen.SelectedFlight;
    
    if (nUld.Code != "LOOSE") {
        screen.RecoverULDLabel = nUld.Code + " " + nUld.SerialNumber;

        if (nUld.TotalReceivedCount == 1)
        {
            screen.findContentItem("Recover").isVisible = false;
            screen.findContentItem("RecoverBUP").isVisible = false;
            screen.findContentItem("UnRecoverUld").isVisible = true;
        }
        else
        {
            screen.findContentItem("Recover").isVisible = true;
            screen.findContentItem("RecoverBUP").isVisible = true;
            screen.findContentItem("UnRecoverUld").isVisible = false;
        }

        $('#recoverPopupContent').parent().center();

        screen.showPopup("RecoverPopup");
    }
    else {

        var currReceived = 0;

        if (nUld.TotalReceivedCount != null) {
            currReceived = nUld.TotalReceivedCount;
            if ((+nUld.TotalUnitCount - +currReceived) < +0) {
                screen.RecoverQuantity = +0;
            }
            else {
                screen.RecoverQuantity = (+nUld.TotalUnitCount - +currReceived);
            }
        }

        $('#recoverLoosePopupContent').parent().center();

        screen.showPopup("RecoverLoosePopup").then(function () {
            setTimeout(function () {
                $RecoverQuantityInput.focus();
                $RecoverQuantityInput.select();
            }, 1);
        });
    }

};

myapp.RecoverFlightULDs.RecoverULDLabel_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "22px");
    $(element).css("line-height", "26px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.RecoverFlightULDs.StatusTimestamp_postRender = function (element, contentItem) {

    var eDate = contentItem.value;

    dateFormat(eDate, element);

};

myapp.RecoverFlightULDs.ShowNotRecovered_execute = function (screen) {
    myapp.RecoverFlightULDs.showComplete = false;

    screen.StatusId = +1;
    screen.StatusId1 = +2;
    screen.StatusId2 = +100000;
    if (screen.SelectedFlight.Status.Id == 3) {
        screen.findContentItem("RecoverSelected").isVisible = false;
    }
    else {
        screen.findContentItem("RecoverSelected").isVisible = true;
    }

    var sAllButton = screen.findContentItem("SelectAllULDs").isVisible = true;
    $('#selectAllButton').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    fillRecoverFlightList(screen, true, 1);
};

myapp.RecoverFlightULDs.ShowRecovered_execute = function (screen) {
    myapp.RecoverFlightULDs.showComplete = true;
    screen.StatusId = +0;
    screen.StatusId1 = +0;
    screen.StatusId2 = +2;
    screen.findContentItem("RecoverSelected").isVisible = false;
    screen.findContentItem("SelectAllULDs").isVisible = false;
    fillRecoverFlightList(screen, true, 1);
};

myapp.RecoverFlightULDs.ShowNotRecovered_postRender = function (element, contentItem) {

    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {

        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Recovered'){
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {

        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Recovered') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });
};

myapp.RecoverFlightULDs.ShowRecovered_postRender = function (element, contentItem) {

    $(element).on('touchstart', function (e) {

        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Recovered') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });     
    });

    $(element).click(function (e) {

        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Recovered') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });
};

myapp.RecoverFlightULDs.SelectAllULDs_postRender = function (element, contentItem) {

    $(element).find('a').first().attr('id', 'selectAllButton');

    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    } 

    $(element).on(evtName, function (e) {
        var a = $(this).find('a').first();       
        selectAllUlds(a);
    });
};

function selectAllUlds(sender) {
    var allItems = $(".recovery-checkbox");
    var selectedItems = $('.selected-recovery-checkbox');
    var unselectedItems = $('.unselected-recovery-checkbox');

    if (allItems.length == selectedItems.length) {
        selectedItems.removeClass('selected-recovery-checkbox').addClass('unselected-recovery-checkbox');
        sender.css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    } else {
        unselectedItems.addClass('selected-recovery-checkbox').removeClass('unselected-recovery-checkbox');
        sender.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    }
};

myapp.RecoverFlightULDs.UnRecoverUld_execute = function (screen) {

    var selectedUld = screen.ULDs.selectedItem;
    var iBup = selectedUld.IsBUP;

    if (iBup) {

        screen.ConfirmPopupHeader = "Un-Recover ULD as BUP";
        screen.ConfirmText = "Do you want to un-recover ULD as BUP?";

        screen.closePopup("RecoverPopup").then(function () {

            $('#confirmPopupContent').parent().center();

            screen.showPopup("ConfirmPopup");

        });
    }
    else {

        screen.ConfirmPopupHeader = "Un-Recover ULD";
        screen.ConfirmText = "Do you want to un-recover ULD?";

        screen.closePopup("RecoverPopup").then(function () {

            $('#confirmPopupContent').parent().center();

            screen.showPopup("ConfirmPopup");

        });
    }
};
myapp.RecoverFlightULDs.RefreshButton_render = function (element, contentItem) {
    $iScrn1 = contentItem.screen;
    var div = $("<div class='am_refresh'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    div.on('click', function () {
        refreshView1(contentItem.screen);
    });
    $(element).append(div);
    $(element).trigger("create");
};

function refreshView1(screen) {
    fillRecoverFlightList(screen, true, 1);
};

myapp.RecoverFlightULDs.ShipmentUnitType_Code_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.RecoverFlightULDs.SerialNumber_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.RecoverFlightULDs.Filter_postRender = function (element, contentItem) {

    //$(element).find('input').attr('id', 'recoverFilterTextbox1');

};
myapp.RecoverFlightULDs.Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.RecoverFlightULDs.WarehouseLocationType_LocationType_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.RecoverFlightULDs.ConfirmPopupHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.RecoverFlightULDs.ConfirmYes_execute = function (screen) {

    screen.closePopup("ConfirmPopup").then(function () {

        var nFlt = screen.SelectedFlight;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var nUld = screen.ULDs.selectedItem;
        var cUser = screen.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        switch (screen.ConfirmPopupHeader) {
            case "Recover ULD":

                msls.showProgress(msls.promiseOperation(function (operation) {
                    $.ajax({
                        type: 'post',
                        data: { 'FltId': nFlt.Id, 'UldId': nUld.Id, 'cUser': cUser },
                        url: '../Web/RecoverFlightULD.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });
                }).then(function PromiseSuccess(PromiseResult) {

                    var result = PromiseResult;
                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {
                            screen.SelectedFlight.details.refresh();
                            fillRecoverFlightList(screen, true, 1);
                            $('#selectAllButton').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                            //screen.FlightHistory.refresh();
                        }
                    }
                }));

                break;
            case "Recover ULD as BUP":
                msls.showProgress(msls.promiseOperation(function (operation) {
                    $.ajax({
                        type: 'post',
                        data: { 'FltId': nFlt.Id, 'UldId': nUld.Id, 'cUser': cUser },
                        url: '../Web/RecoverFlightBUP.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });
                }).then(function PromiseSuccess(PromiseResult) {

                    var result = PromiseResult;
                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {
                            msls.showProgress(msls.promiseOperation(function (operation) {
                                fillRecoverFlightList(screen, true, 1);
                                screen.SelectedFlight.details.refresh();
                                $('#selectAllButton').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                                //screen.FlightHistory.refresh();
                                operation.complete();
                            }));
                        }
                    }
                }));
                break;
            case "Un-Recover ULD":

                var nFlt = screen.SelectedFlight;
                if (!nFlt) {
                    nFlt = $FlightManifest;
                }
                var nUld = screen.ULDs.selectedItem;

                msls.showProgress(msls.promiseOperation(function (operation) {
                    $.ajax({
                        type: 'post',
                        data: { 'FltId': nFlt.Id, 'UldId': nUld.Id, 'cUser': cUser },
                        url: '../Web/UnRecoverFlightULD.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });
                }).then(function PromiseSuccess(PromiseResult) {

                    var result = PromiseResult;
                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {
                            screen.SelectedFlight.details.refresh();
                            fillRecoverFlightList(screen, true, 1);
                            //screen.FlightHistory.refresh();
                        }
                    }
                }));

                break;
            case "Un-Recover ULD as BUP":

                var nFlt = screen.SelectedFlight;
                if (!nFlt) {
                    nFlt = $FlightManifest;
                }
                var nUld = screen.ULDs.selectedItem;

                msls.showProgress(msls.promiseOperation(function (operation) {
                    $.ajax({
                        type: 'post',
                        data: { 'FltId': nFlt.Id, 'UldId': nUld.Id, 'cUser': cUser },
                        url: '../Web/UnRecoverFlightBUP.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });
                }).then(function PromiseSuccess(PromiseResult) {

                    var result = PromiseResult;

                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {
                            screen.SelectedFlight.details.refresh();
                            fillRecoverFlightList(screen, true, 1);
                            //screen.FlightHistory.refresh();
                        }
                    }
                }));
                break;
        }

    });
};

myapp.RecoverFlightULDs.ConfirmText_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.RecoverFlightULDs.ConfirmNo_execute = function (screen) {
    
    screen.closePopup("ConfirmPopup").then(function () {

        $('#recoverPopupContent').parent().center();

        screen.showPopup("RecoverPopup");

    });

};
myapp.RecoverFlightULDs.Carrier_CarrierCode_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "20px");
    $(element).css("line-height", "20px");
    $(element).css("text-align", "center");

};
myapp.RecoverFlightULDs.SelectedFlight_Status_Name_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};
myapp.RecoverFlightULDs.RecoverLoose_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var nFlt = iScrn.SelectedFlight;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var nUld = iScrn.ULDs.selectedItem;
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = iScrn.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var rQty = $(this).parent().parent().find('input').val();
        iScrn.closePopup("RecoverLoosePopup").then(function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                $.ajax({
                    type: 'post',
                    data: { 'FltId': nFlt.Id, 'UldId': nUld.Id, 'RecQty': rQty, 'cUser': cUser },
                    url: '../Web/RecoverFlightLoose.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        iScrn.ULDs.refresh();
                        iScrn.SelectedFlight.details.refresh();
                        iScrn.FlightHistory.refresh();
                    }
                }
            }));
        }, 1000);

    });

};
myapp.RecoverFlightULDs.UnRecoverLoose_postRender = function (element, contentItem) {
    
    var iScrn = contentItem.screen;

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var nFlt = iScrn.SelectedFlight;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var nUld = iScrn.ULDs.selectedItem;
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = iScrn.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var rQty = $(this).parent().parent().find('input').val();
        iScrn.closePopup("RecoverLoosePopup").then(function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                $.ajax({
                    type: 'post',
                    data: { 'FltId': nFlt.Id, 'UldId': nUld.Id, 'RecQty': rQty, 'cUser': cUser },
                    url: '../Web/UnRecoverFlightLoose.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        iScrn.ULDs.refresh();
                        iScrn.SelectedFlight.details.refresh();
                        iScrn.FlightHistory.refresh();
                    }
                }
            }));
        }, 1000);

    });

};
myapp.RecoverFlightULDs.RecoverLoosePopup_postRender = function (element, contentItem) {
   
    var container = $(element);
    container.attr('id', 'recoverLoosePopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.RecoverFlightULDs.RecoverPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'recoverPopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.RecoverFlightULDs.ConfirmPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'confirmPopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.RecoverFlightULDs.ULDs_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.RecoverFlightULDs.RecoverTab_postRender = function (element, contentItem) {

    $(element).css("overflow", "hidden");

};
myapp.RecoverFlightULDs.HistoryTab_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "History" })).parent().on(evtName, function (e) {

        contentItem.screen.FlightHistory.refresh();

    });

};
myapp.RecoverFlightULDs.FlightHistory_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.RecoverFlightULDs.WarehouseLocations_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};

myapp.RecoverFlightULDs.ULDsTemplate_postRender = function (element, contentItem) {

};

myapp.RecoverFlightULDs.RecoverUlds_execute = function (screen) {    
    recoverUlds(screen, '../Web/RecoverFlightULDs.ashx');
};
myapp.RecoverFlightULDs.RecoverUldsAsBUP_execute = function (screen) {    
    recoverUlds(screen, '../Web/RecoverFlightULDsBup.ashx');
};

function recoverUlds(screen, webMethodName) {
    var ulds = [];
    $("div.selected-recovery-checkbox").each(function () {
        ulds.push($(this).attr('uldId'));
    });

    var nFlt = screen.FlightManifest;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'FltId': nFlt.Id, 'UldIds': JSON.stringify(ulds), 'cUser': cUser },
            url: webMethodName,
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
                screen.closePopup("RecoverMultipleUlds");
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                screen.SelectedFlight.details.refresh();
                screen.ULDs.refresh();
                $('#selectAllButton').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                //screen.FlightHistory.refresh();
            }
        }
    }));
};
myapp.RecoverFlightULDs.FlightHistoryTemplate_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-text");

};
myapp.RecoverFlightULDs.PopupKeypad_render = function (element, contentItem) {
    
    $(element).attr("id", "inlineTargetKeypad");
    $(element).addClass("keypad-inline");

    $('div#inlineTargetKeypad').keypad({
        target: $('.inlineTarget:visible'),
        layout: ['123', '456', '789', $.keypad.SPACE + '0' + $.keypad.BACK]
    });

    var keypadTarget = null;
    $('input.inlineTarget').focus(function () {
        if (keypadTarget != this) {
            keypadTarget = this;
            $('div#inlineTargetKeypad').keypad('option', { target: this });
        }
    });

};
myapp.RecoverFlightULDs.RecoverMultipleUlds_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'recoverMultiplePopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.RecoverFlightULDs.RecoverMultipleUldsHeader_postRender = function (element, contentItem) {

    element.textContent = "Recover Multiple ULDs";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.RecoverFlightULDs.Search_execute = function (screen) {

    //if ($('#recoverFilterTextbox1').val()) {
    //    screen.SearchFilter = $('#recoverFilterTextbox1').val();
    //}
    //else {
    //    screen.SearchFilter = null;
    //}

};
myapp.RecoverFlightULDs.Search1_execute = function (screen) {

    if ($('#historyFilterTextbox1').val()) {
        screen.SearchHistoryFilter = $('#historyFilterTextbox1').val();
    }
    else {
        screen.SearchHistoryFilter = null;
    }

};
myapp.RecoverFlightULDs.Search2_execute = function (screen) {

    if ($('#locationFilterTextbox1').val()) {
        screen.SearchLocationFilter = $('#locationFilterTextbox1').val();
    }
    else {
        screen.SearchLocationFilter = null;
    }

};
myapp.RecoverFlightULDs.Search_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        fillRecoverFlightList(contentItem.screen, true, 1);
        //myapp.RecoverFlight.Search_execute(contentItem.screen);
    });

};
myapp.RecoverFlightULDs.HistoryFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'historyFilterTextbox1');

};
myapp.RecoverFlightULDs.Search1_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.RecoverFlightULDs.Search1_execute(contentItem.screen);
    });

};
myapp.RecoverFlightULDs.Search2_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.RecoverFlightULDs.Search2_execute(contentItem.screen);
    });

};
myapp.RecoverFlightULDs.ULDs_render = function (element, contentItem) {
    var ul = $('<ul id="recoverFlightList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.RecoverFlightULDs.listLoadPageNumber = 2;
    myapp.RecoverFlightULDs.showComplete = false;
    $(element).on('scroll', function () {
        if ($('ul#recoverFlightList').last().height() === $(this).scrollTop() + $(this).height() + 6) {            
            fillRecoverFlightList(contentItem.screen, false, myapp.RecoverFlightULDs.listLoadPageNumber);
            myapp.RecoverFlightULDs.listLoadPageNumber = myapp.RecoverFlightULDs.listLoadPageNumber + 1;
        }
    });

    fillRecoverFlightList(contentItem.screen, true, 1);
};

function fillRecoverFlightList(screen, clearList, pageNumber) {    
    $.ajax({
        type: 'GET',
        data: { 'flightId': screen.SelectedFlight.Id, 'isRecovered': myapp.RecoverFlightULDs.showComplete, 'rowsPerPage': 8, 'pageNumber': pageNumber, 'filter': $('input#uldRecoverFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetRecoverUldsForFlight',
        success: function (data) {
            $('span#noRItemsUlds').remove();
            var list = $('ul#recoverFlightList').last();
            if (clearList) {
                list.empty();
                myapp.RecoverFlightULDs.listLoadPageNumber = 2;
            }
            $(data).each(function (i) {
                if (i > 0) {
                    var li = $('<li class="custom-list-item" style="height:75px"></li>');
                    var that = this;
                    li.on('click', function () {
                        if (!screen.ULDs) {
                            screen.ULDs = {};
                        }

                        screen.ULDs.selectedItem = that;
                        myapp.RecoverFlightULDs.OpenRecoverPopup_execute(screen);                        
                    });

                    //var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                    //var carrierCode = $('<div style="font-size: 20px;font-weight: bold;margin-top: 35px;">' + this.CarrierCode + '</div>');
                    //statusContainer.append(carrierCode);

                    var refContainer = $('<div class="ref-container"></div>');
                    var refNumber = $('<div>' + this.Code + '<BR>' + this.SerialNumber + '</div>');

                    var progressBar = $('<div class="custom-progressbar"></div>');
                    var progressBarCountHolder = $('<div class="custom-progressbar-count-holder">' + (myapp.RecoverFlightULDs.showComplete ? 1 : 0) + ' of ' + 1 + '</div>');
                    var progressBarIndicator = $('<div class="custom-progressbar-indicator"></div>');
                    progressBarIndicator.css('width', (myapp.RecoverFlightULDs.showComplete ? 100 : 0) + '%');
                    progressBar.append(progressBarCountHolder);
                    progressBar.append(progressBarIndicator);

                    refContainer.append(refNumber);
                    refContainer.append(progressBar);

                    var imageContainer1 = $('<div class="custom-image-container1" style="margin-top:5px"></div>');

                    if (screen.StatusId == +1) {
                        if (that.SerialNumber === "LOOSE") return;

                        var div = $('<div class="recovery-checkbox unselected-recovery-checkbox" style="z-index: 100"></div>');
                        div.attr('uldId', that.Id);

                        var evtName = 'click';
                        if ('ontouchstart' in document.documentElement) {
                            evtName = 'touchstart';
                        }

                        div.on(evtName, function (e) {
                            e.stopPropagation();
                            $(this).toggleClass('unselected-recovery-checkbox').toggleClass('selected-recovery-checkbox');

                            var selectAllBtn = $('a:has(span.ui-btn-text:contains("Select All"))');

                            var allItems = $(".recovery-checkbox");
                            var selectedItems = $('.selected-recovery-checkbox');

                            if (allItems.length == selectedItems.length) {
                                selectAllBtn.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                            } else {
                                selectAllBtn.css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                            }                            
                            return false;
                        });
                        imageContainer1.append(div);
                    }

                    var imageContainer = $('<div class="custom-image-container"></div>');

                    var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px"></div>');
                    var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
                    snapshotButton.append(snapshotImage);
                    snapshotButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.ULDs) {
                            screen.ULDs = {};
                        }
                        screen.ULDs.selectedItem = that;
                        myapp.RecoverFlightULDs.ShowUldSnapshot_execute(screen);
                    });

                    imageContainer.append(snapshotButton);

                    //li.append(statusContainer);
                    li.append(imageContainer1);
                    li.append(imageContainer);
                    li.append(refContainer);          
                    list.append(li);
                }
            });            

            if (!list.find('li').length) {
                list.before('<span id="noRItemsUlds">No Items</span>')
            }
            
                $('#bruldProgressBar p').text(data[0].FlightReceivedPiecs + ' of ' + data[0].FlightTotalReceivedPieces);
                $('#bruldProgressBar div').css('width', data[0].FlightPercentReceived + '%');                        
        },

        error: function (e) {
            console.log('Unable get data.' + e);
        }
    })
}
myapp.RecoverFlightULDs.Filter_render = function (element, contentItem) {
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="uldRecoverFilter" style="margin-left:3px;"></input>');
    elem.append(filter);
};
myapp.RecoverFlightULDs.SelectedFlight_WarehouseLocation1_Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};
myapp.RecoverFlightULDs.GoToFlights_execute = function (screen) {
    
    var aTimeout = screen.AjaxTimeout;
    if(!aTimeout)
    {
        aTimeout = $AjaxTimeout;
    }

    myapp.showRecoverFlight(aTimeout);

};

myapp.RecoverFlightULDs.ShowUldSnapshot_execute = function (screen) {

    var tULD = screen.ULDs.selectedItem;
    $SelectedUld1 = screen.ULDs.selectedItem;
    var uDescULD = tULD.Code + " " + tULD.SerialNumber;
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    myapp.showSnapshot(screen.ULDEntityType, cTask, screen.CurrentUser, screen.CurrentUserWarehouse, tULD.Id, uDescULD, aTimeout);

};
myapp.RecoverFlightULDs.FinalizePopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'finalizePopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.RecoverFlightULDs.FinalizeHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.RecoverFlightULDs.FinalizeText_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.RecoverFlightULDs.FinalizeYes_execute = function (screen) {

    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    if (screen.FinalizeHeader == "Finalize Recovery") {
        
        $.ajax({
            type: 'post',
            data: { 'flightId': screen.SelectedFlight.Id, 'taskId': screen.SelectedTask.Id, 'userName': screen.CurrentUser },
            url: '../api/cargoreceiver/FinalizeRecoverFlight',
            success: function () {
                screen.SelectedFlight.details.refresh();
                myapp.showRecoverFlight(aTimeout);
            },

            error: function (e) {
                console.log('Unable finalize flight.' + e);
            }
        });
    }
    else {

        $.ajax({
            type: 'post',
            data: { 'flightId': screen.SelectedFlight.Id, 'taskId': screen.SelectedTask.Id, 'userName': screen.CurrentUser },
            url: '../api/cargoreceiver/ReopenRecoverFlight',
            success: function () {
                screen.SelectedFlight.details.refresh();
                screen.findContentItem("OpenAddUldPopup").isVisible = true;
                screen.findContentItem("RecoverSelected").isVisible = true;
                screen.findContentItem("OpenFinalize").isVisible = true;
                screen.findContentItem("OpenReopenFlight").isVisible = false;
                screen.closePopup("FinalizePopup");

            },

            error: function (e) {
                console.log('Unable to reopen flight.' + e);
            }
        });

    }

};
myapp.RecoverFlightULDs.FinalizeNo_execute = function (screen) {
    
    screen.closePopup("FinalizePopup");

};
myapp.RecoverFlightULDs.OpenFinalize_execute = function (screen) {
    
    screen.FinalizeHeader = "Finalize Recovery";
    screen.FinalizeText = "Do you want to finalize recovery?";

    $('#finalizePopupContent').parent().center();

    screen.showPopup("FinalizePopup");

};
myapp.RecoverFlightULDs.OpenReopenFlight_execute = function (screen) {
    
    screen.FinalizeHeader = "Reopen Flight";
    screen.FinalizeText = "Do you want to re-open flight?";

    $('#finalizePopupContent').parent().center();

    screen.showPopup("FinalizePopup");

};
myapp.RecoverFlightULDs.AddUldPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'addUldPopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.RecoverFlightULDs.AddUldHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Add ULD";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.RecoverFlightULDs.CancelAddUld_execute = function (screen) {
    
    screen.closePopup("AddUldPopup");

};
myapp.RecoverFlightULDs.OpenAddUldPopup_execute = function (screen) {
    
    $('#addUldPopupContent').parent().center();

    screen.showPopup("AddUldPopup");

};
myapp.RecoverFlightULDs.SelectedUnitType_render = function (element, contentItem) {
    $(element).append('<input id="selectedUnitType" type="text" readonly></input>');
};
myapp.RecoverFlightULDs.OpenUnitTypes_postRender = function (element, contentItem) {
    //$(element).css('margin-top', '18px');
};
myapp.RecoverFlightULDs.ShipmentTypesContainer_render = function (element, contentItem) {    
    var ul = $('<ul class="shipment-type-list"></ul>');
    $(element).append(ul);

    $.ajax({
        type: 'GET',        
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetShipmentUnitTypes',
        success: function (data) {
            $(data).each(function(){
                var li = $('<li data-id="' + this.Id + '">' + this.Code + '</li>');
                li.on(getSuitableEventName(), function () {
                    var st = $('#selectedUnitType');
                    st.val($(this).text());
                    st.attr('data-id', $(this).attr('data-id'));

                    contentItem.screen.closePopup("SelectShipmentTypePopup").then(function () {
                        contentItem.screen.showPopup("AddUldPopup");
                    });
                });
                ul.append(li);
            });            
        },
        error: function (e) {
            alert('Unable get shipment types: ' + e.message);
        }
    });
};
myapp.RecoverFlightULDs.OpenUnitTypes_execute = function (screen) {
    screen.closePopup("AddUldPopup").then(function () {
        screen.showPopup("SelectShipmentTypePopup");
    });
};
myapp.RecoverFlightULDs.ShipmentTypeHeader_postRender = function (element, contentItem) {
    element.textContent = "Shipment Type";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");
};

myapp.RecoverFlightULDs.AddUld_execute = function (screen) {
    $.ajax({
        type: 'POST',
        data: {
            flightManifestId: screen.SelectedFlight.Id,
            taskId: screen.SelectedTask.Id,
            unitTypeId: $('#selectedUnitType').attr('data-id'),
            serialNumber: screen.SelectedSerialNumber,
            totalPieces: screen.SelectedTotalPieces,
            weight: screen.SelectedWeight,
            userName: screen.CurrentUser
        },        
        url: '../api/cargoreceiver/AddUnManifestedUls',
        success: function () {
            fillRecoverFlightList(screen, true, 1);
            screen.SelectedSerialNumber = null;
            screen.SelectedTotalPieces = null;
            screen.SelectedWeight = null;
            $('#selectedUnitType').attr('data-id', '');
            $('#selectedUnitType').val('');
            screen.closePopup("AddUldPopup");
        },
        error: function (e) {
            alert('Cannot add ULD: ' + e.statusText);
        }
    });
};
myapp.RecoverFlightULDs.SelectShipmentTypePopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'selectShipTypePopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};