﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $CarrierInput;

myapp.TrackAndTrace.AWBDash_postRender = function (element, contentItem) {
    element.textContent = "-";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "60px");
    $(element).css("line-height", "39px");
    $(element).css("padding-top", "14px");
    $(element).css("overflow", "hidden");
    $(element).css("text-align", "center");
};
myapp.TrackAndTrace.CarrierCode_postRender = function (element, contentItem) {
    $(element).addClass('track-and-trace-text-box');
   
    $CarrierInput = $("input", $(element));
    var $iControl = $(element).find('input[id*="CarrierCode"]');

    $iControl.attr("maxlength", "3");

};
myapp.TrackAndTrace.AwbNumber_postRender = function (element, contentItem) {
    $(element).addClass('track-and-trace-text-box');    
    $(element).css("padding-top", "8px");
    var $iControl = $(element).find("input");
    $iControl.attr("maxlength", "8");

};

myapp.TrackAndTrace.Reset_execute = function (screen) {

    screen.SearchCarrier = "";
    screen.CarrierCode = "";
    screen.SearchAwb = "";
    screen.AwbNumber = "";
    screen.SearchHwb = "";
    screen.HwbNumber = "";
    screen.ShipmentCharges = "";
    screen.ShipmentClearance = "";
    screen.ShipmentHwbCount = "";
    screen.ShipmentPieces = "";
    screen.ShipmentReference = "";
    screen.ShipmentStatus = "";
    screen.ShipmentWeight = "";
    screen.ShipmentChargesPlus1 = "";
    screen.ShipmentChargesPlus2 = "";
    screen.findContentItem("ShipmentCharges").isVisible = false;
    screen.findContentItem("ShipmentClearance").isVisible = false;
    screen.findContentItem("ShipmentHwbCount").isVisible = false;
    screen.findContentItem("ShipmentPieces").isVisible = false;
    screen.findContentItem("ShipmentReference").isVisible = false;
    screen.findContentItem("ShipmentStatus").isVisible = false;
    screen.findContentItem("ShipmentWeight").isVisible = false;
    screen.findContentItem("ShipmentReceivedDate").isVisible = false;
    screen.findContentItem("ShipmentChargesPlus1").isVisible = false;
    screen.findContentItem("ShipmentChargesPlus2").isVisible = false;
    screen.findContentItem("RecordNotFound").isVisible = false;
    $CarrierInput.focus();

};

myapp.TrackAndTrace.Search_postRender = function (element, contentItem) {

    $(element).addClass('track-and-trace-button');
    $(element).find('a:first-child').attr('style', 'padding-top:8px!important;padding-bottom:9px!important');

    var iScrn = contentItem.screen;
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {        
        
        
        var p = $(this).parent().parent();

        var cCode = p.find('input[id*="CarrierCode"]').val();
        var AwbNum = p.find('input[id*="AwbNumber"]').val();
        var HwbNum = p.find('input[id*="HwbNumber"]').val();

        msls.showProgress(msls.promiseOperation(function (operation) {
        iScrn.ShipmentCharges = "";
        iScrn.ShipmentClearance = "";
        iScrn.ShipmentHwbCount = "";
        iScrn.ShipmentPieces = "";
        iScrn.ShipmentReference = "";
        iScrn.ShipmentStatus = "";
        iScrn.ShipmentWeight = "";
        iScrn.ShipmentReceivedDate = "";
        iScrn.ShipmentChargesPlus1 = "";
        iScrn.ShipmentChargesPlus2 = "";
        iScrn.findContentItem("ShipmentCharges").isVisible = false;
        iScrn.findContentItem("ShipmentClearance").isVisible = false;
        iScrn.findContentItem("ShipmentHwbCount").isVisible = false;
        iScrn.findContentItem("ShipmentPieces").isVisible = false;
        iScrn.findContentItem("ShipmentReference").isVisible = false;
        iScrn.findContentItem("ShipmentStatus").isVisible = false;
        iScrn.findContentItem("ShipmentWeight").isVisible = false;
        iScrn.findContentItem("ShipmentChargesPlus1").isVisible = false;
        iScrn.findContentItem("ShipmentChargesPlus2").isVisible = false;
        iScrn.findContentItem("ShipmentReceivedDate").isVisible = false;

        iScrn.findContentItem("RecordNotFound").isVisible = false;
     
            $.ajax({
                type: 'post',
                data: { 'Carrier': cCode, 'Awb': AwbNum, 'Hwb': HwbNum },
                url: '../Web/GetTrackingDetails.ashx',
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                })
            });
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;


            var items = result.split(',');
            if (items[0] == "0") {

                iScrn.findContentItem("ShipmentCharges").isVisible = false;
                iScrn.findContentItem("ShipmentClearance").isVisible = false;
                iScrn.findContentItem("ShipmentHwbCount").isVisible = false;
                iScrn.findContentItem("ShipmentPieces").isVisible = false;
                iScrn.findContentItem("ShipmentReference").isVisible = false;
                iScrn.findContentItem("ShipmentStatus").isVisible = false;
                iScrn.findContentItem("ShipmentWeight").isVisible = false;
                iScrn.findContentItem("ShipmentReceivedDate").isVisible = false;
                iScrn.findContentItem("ShipmentChargesPlus1").isVisible = false;
                iScrn.findContentItem("ShipmentChargesPlus2").isVisible = false;
                iScrn.RecordNotFound = "Shipment not found";
                iScrn.findContentItem("RecordNotFound").isVisible = true;

            }
            else {

                iScrn.ShipmentReference = items[0];
                iScrn.ShipmentHwbCount = items[1];
                iScrn.ShipmentPieces = items[2];
                iScrn.ShipmentWeight = items[3];
                iScrn.ShipmentStatus = items[4];
                iScrn.ShipmentReceivedDate = items[5];
                iScrn.ShipmentClearance = items[6];
                iScrn.ShipmentCharges = items[7];
                iScrn.ShipmentChargesPlus1 = items[8];
                iScrn.ShipmentChargesPlus2 = items[9];

                iScrn.findContentItem("ShipmentCharges").isVisible = true;
                iScrn.findContentItem("ShipmentClearance").isVisible = true;
                iScrn.findContentItem("ShipmentHwbCount").isVisible = true;
                iScrn.findContentItem("ShipmentPieces").isVisible = true;
                iScrn.findContentItem("ShipmentReference").isVisible = true;
                iScrn.findContentItem("ShipmentStatus").isVisible = true;
                iScrn.findContentItem("ShipmentWeight").isVisible = true;
                iScrn.findContentItem("ShipmentReceivedDate").isVisible = true;
                iScrn.findContentItem("ShipmentChargesPlus1").isVisible = true;
                iScrn.findContentItem("ShipmentChargesPlus2").isVisible = true;
                iScrn.findContentItem("RecordNotFound").isVisible = false;
                
            }

        }));

    });

};

myapp.TrackAndTrace.HwbNumber_postRender = function (element, contentItem) {
    $(element).addClass('track-and-trace-text-box');
};
myapp.TrackAndTrace.Reset_postRender = function (element, contentItem) {   
    $(element).addClass('track-and-trace-button');
    $(element).find('a:first-child').attr('style', 'padding-top:8px!important;padding-bottom:9px!important');
};
myapp.TrackAndTrace.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.TrackAndTrace.SearchShipment_postRender = function (element, contentItem) {
    
    var iScrn = contentItem.screen;
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var sReference = $(this).parent().parent().find('input').val();
        iScrn.SelectedReference = sReference;

    });

};
myapp.TrackAndTrace.ResetSearch_execute = function (screen) {
    
    screen.SearchReference = null;
    screen.SelectedReference = null;

};
myapp.TrackAndTrace.Group_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.TrackAndTrace.ActiveTrackTraceView_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.TrackAndTrace.Group12_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.TrackAndTrace.CurrentCharges_postRender = function (element, contentItem) {
    applyMoneyFormat(element);
};
myapp.TrackAndTrace.ChargesPlus1Day_postRender = function (element, contentItem) {
    applyMoneyFormat(element);
};

myapp.TrackAndTrace.ChargesPlus2Day_postRender = function (element, contentItem) {
    applyMoneyFormat(element);
};

function applyMoneyFormat(element) {
    var span = $(element).find('span');
    var result = formatMoney(Number(span.text().replace(',', '')), 2, '.', ',');
    span.text('$' + result);
};

function formatMoney(n, c, d, t) {   
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
myapp.TrackAndTrace.ISC_postRender = function (element, contentItem) {
    applyMoneyFormat(element);
};
myapp.TrackAndTrace.StorageChargesPerDay_postRender = function (element, contentItem) {   
    applyMoneyFormat(element);
};
myapp.TrackAndTrace.CurrentStorageCharges_postRender = function (element, contentItem) {
    applyMoneyFormat(element);
};
myapp.TrackAndTrace.OutstandingBalance_postRender = function (element, contentItem) {
    applyMoneyFormat(element);
};