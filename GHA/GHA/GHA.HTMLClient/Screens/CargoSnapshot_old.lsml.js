﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $file_browse_button;
var $CurrentQuantityInput;
var $preview;
var $carouselEl;
var $carouselItems;
var carousel;

myapp.CargoSnapshot_old.created = function (screen) {

    var sEntity = screen.SelectedEntityType;
    var sLabel = sEntity.EntityName + "# ";
    var sDesc = screen.EntityDescription;
    var disName = "Proof of Condition for " + sLabel + sDesc;
    screen.details.displayName = disName;

};

function createImageUploader1(element, contentItem) {

    var relativePathFromClientRootToThisFolder = "Scripts/";
    //     (This path will be used for the fallback ASPX uploader.) 


    var $element = $(element);


    // Try the local file-reading HTML-5 standard method first. 
    //      If it fails, fall back on a method that will require an extra round-trip  
    //      to the server 
    if (window.FileReader) {
        createHTML5Uploader1();
    } else {
        createFallbackASPXUploader1();
    }


    function createHTML5Uploader1() {
        $file_browse_button = $('<input name="file" type="file" style="display:none"/>');
        //$element.append($file_browse_button);

        //var $preview = $('<div></div>');
        //$element.append($preview);

        $file_browse_button.bind('change', function handleFileSelect(evt) {
            var files = evt.target.files;
            if (files.length == 1) {
                var reader = new FileReader();
                reader.onload = function (e) {

                    var tempImg = new Image();
                    tempImg.src = reader.result;
                    tempImg.onload = function () {

                        var MAX_WIDTH = 400;
                        var MAX_HEIGHT = 300;
                        var tempW = tempImg.width;
                        var tempH = tempImg.height;
                        if (tempW > tempH) {
                            if (tempW > MAX_WIDTH) {
                                tempH *= MAX_WIDTH / tempW;
                                tempW = MAX_WIDTH;
                            }
                        } else {
                            if (tempH > MAX_HEIGHT) {
                                tempW *= MAX_HEIGHT / tempH;
                                tempH = MAX_HEIGHT;
                            }
                        }

                        var canvas = document.createElement('canvas');
                        canvas.width = tempW;
                        canvas.height = tempH;

                        var ctx = canvas.getContext("2d");
                        ctx.drawImage(this, 0, 0, tempW, tempH);

                        var dataURL = canvas.toDataURL("image/png");

                        previewImageAndSetContentItem1(dataURL, contentItem);
                    }
                };

                reader.readAsDataURL(files[0]);

            } else {
                previewImageAndSetContentItem(null, contentItem);
            }
        });
    }
};

function createFallbackASPXUploader1() {
    // Create a file submission form 
    var $file_upload_form = $('<form method="post" ' +
            'action="' + relativePathFromClientRootToThisFolder + 'image-uploader-base64-encoder.aspx" ' +
            'enctype="multipart/form-data" target="uploadTargetIFrame" />');
    var $file_browse_button = $('<input name="file" type="file" style="display:none" />');
    $file_upload_form.append($file_browse_button);
    $element.append($file_upload_form);

    // Add an invisible IFrame that the contents of the file will be posted to: 
    var $uploadTargetIFrame = $('<iframe name="uploadTargetIFrame" ' +
            'style="width: 0px; height: 0px; border: 0px solid #fff;"></iframe>');
    $element.append($uploadTargetIFrame);

    // Finally, add a preview div that will show a "processing" text during a round-trip to the server,  
    //      and will show the contents of the image once it is loaded. 
    //var $preview = $('<div></div>');
    //$element.append($preview);


    // Having set up the content, wire it up: 

    // On browsing to a file, automatically submit to inner IFrame 
    $file_browse_button.change(function () {
        $file_upload_form.submit();
    });

    // On form submission, show a "processing" message: 
    $file_upload_form.submit(function () {
        $preview.append($('<div>Processing...</div>'));
    });

    // Once the result frame is loaded (e.g., result came back),  
    //      preview the image and set the content item appropriately.  
    $uploadTargetIFrame.load(function () {
        var serverResponse = null;
        try {
            //serverResponse = $uploadTargetIFrame.contents().find("body").html();
        } catch (e) {
            // request must have failed, keep server response empty.  
        }
        previewImageAndSetContentItem1(serverResponse, contentItem);
    });
};

function previewImageAndSetContentItem1(fullBinaryString, contentItem) {
    //$preview.empty();

    if ((fullBinaryString == null) || (fullBinaryString.length == 0)) {
        contentItem.value = null;
    } else {
        //$preview.append($('<img src="' + fullBinaryString + '" style="' + previewStyle + '" />'));
        // As far as storing the data in the database, beyond previewing it, 
        //     remove the preamble returned by FileReader or the server  
        //     (always of the same form: "data:jpeg;base64," with variations only on the  
        //     type of data -- jpeg, png, etc). 
        //     The first comma serves as the necessary marker where the binary data begins. 
        contentItem.value = fullBinaryString.substring(fullBinaryString.indexOf(",") + 1);
    }
};

myapp.CargoSnapshot_old.TakePicture_execute = function (screen) {
    
    $file_browse_button.click();

};

myapp.CargoSnapshot_old.CapturedImage_postRender = function (element, contentItem) {

    createImageUploader1(element, contentItem);

    contentItem.dataBind("value", function (result) {

        if (result != null) {

            var iScrn = contentItem.screen;
            var sDesc = iScrn.EntityDescription;
            var eEntity = iScrn.SelectedEntityType;
            var sLabel = eEntity.EntityName + "# ";
            var eId = iScrn.SelectedEntityId;
            var sQty = iScrn.CurrentQuantity;
            var sCond = iScrn.CurrentConditions;
            var cUser = iScrn.CurrentUser;
            var tStamp = new Date();
            var disText = sLabel + " " + sDesc + " | QTY: " + sQty + " | Condition: " + sCond + " | User: " + cUser + " | Date: " + tStamp;

            $(element).append(contentItem);

            var nImage = new myapp.Snapshot1();
            nImage.Image = result;
            nImage.EntityType = eEntity;
            nImage.EntityId = eId;
            nImage.Count = sQty;
            nImage.UserName = cUser;
            nImage.Timestamp = tStamp;
            nImage.DisplayText = disText;

            myapp.applyChanges().then(function () {

                screen.Snapshots.refresh();

            });

        }

    });

};

myapp.CargoSnapshot_old.CheckboxColumn_render = function (element, contentItem) {
    
    var checkbox = getCheckBox(contentItem);
    $(checkbox).appendTo($(element));

};

function getCheckBox(contentItem) {

    var checkbox = $("<input type='checkbox' />");
    checkbox.css("height", "20px");
    checkbox.css("width", "20px");
    checkbox.css("margin", "2px");

    checkbox.change(function () {
        var checked = checkbox[0].checked;
        if (!!contentItem.value.details["__isSelected"] !== checked) {
            contentItem.value.details["__isSelected"] = checked;
        }
    });

    contentItem.myCheckBox = checkbox[0];
    return checkbox;
};

myapp.CargoSnapshot_old.CurrentQuantity1_postRender = function (element, contentItem) {
    
    $CurrentQuantityInput = $("input", $(element));

};

myapp.CargoSnapshot_old.SelectConditionPopup_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};

myapp.CargoSnapshot_old.AddSnapshotPopup_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};

myapp.CargoSnapshot_old.AddSnapshot_execute = function (screen) {

    screen.CurrentQuantity = 1;

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("SelectConditionPopup").then(function () {
        setTimeout(function () {
            $CurrentQuantityInput.focus();
            $CurrentQuantityInput.select();
        }, 1);
    });
};

myapp.CargoSnapshot_old.SelectCondition_execute = function (screen) {

    var cList = "";
    var cItems = "";

    $("#conditionlist .mycheckbox:checked").each(function () {

        cList += $(this).attr("value") + ",";
        cItems += $.trim($(this).next().text()) + ", ";

    });

    cList = cList.slice(0, cList.length - 1);
    cItems = cItems.slice(0, cItems.length - 2);
    screen.CurrentSelectedConditions = cList;
    screen.CurrentConditions = cItems;

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.closePopup("SelectConditionPopup").then(function () {
        screen.showPopup("AddSnapshotPopup");
    });
    

};

myapp.CargoSnapshot_old.SelectConditionPopupHeader_postRender = function (element, contentItem) {

    element.textContent = "Select Condition";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.CargoSnapshot_old.AddSnapshotPopupHeader_postRender = function (element, contentItem) {

    element.textContent = "Take Picture";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.CargoSnapshot_old.SelectConditionItem_execute = function (screen) {
   
    var sCond = screen.Conditions.selectedItem;

    screen.getConditions().then(function (conditions) {
        conditions.data.forEach(function (condition) {
            if (condition.Id == sCond.Id) {
                
                screen.findContentItem("CheckboxColumn").myCheckBox.checked = true;

            }
        });
    });
   
};

myapp.CargoSnapshot_old.ConditionList_render = function (element, contentItem) {
   
    createCheckboxList(element, contentItem);

};

function createCheckboxList(element, contentItem) {
    var iScrn = contentItem.screen;
    var cList = "";
    cList += '<div id="conditionlist" data-role="fieldcontain"><fieldset data-role="controlgroup"><ul>';

    iScrn.getConditions().then(function (conditions) {
        conditions.data.forEach(function (condition) {

            var cName = "checkbox-" + condition.Id + "a";
            var cValue = condition.Id;
            var condName = condition.Condition2;
            var cImage = condition.Image;

            cList += '<li><input type="checkbox" name="' + cName + '" id="' + cName + '" value="' + cValue + '" class="mycheckbox" />' +
                      '<label for="' + cName + '" style="vertical-align: middle;" >' + '<img src="data:image/png;base64,' + cImage + '" height="30px" width="30px" style="max-width: 100%; max-height: 100%; vertical-align: middle;"><big><b>&nbsp&nbsp' + condName + '</b></big></label></li>';
        });
    }).then(function () {

        cList += '</ul></fieldset></div>';

        $(element).append(cList);
        $(element).trigger("create");
        //$("input[type='checkbox']").attr("checked", true).checkboxradio("refresh");
    });
};

myapp.CargoSnapshot_old.Snapshots2_render = function (element, contentItem) {

    createImageGallery(element, contentItem);
};

function createImageGallery(element, contentItem) {

    var iScrn = contentItem.screen;
    var picList = "";
    var firstPic = "";
    var isFirst = true;


    picList += '<div class="container rGallery"><div class="gallery">';

    iScrn.getSnapshots().then(function (pics) {
        pics.data.forEach(function (pic) {

            if (isFirst) {
                firstPic = pic.Image;
                isFirst = false;
                picList += '<br><div class="image-preview"><img id="preview" src="data:image/png;base64,' + firstPic + '" /></div>' + pic.DisplayText + '<br><ul id="carousel" class="elastislide-list">';
            }

            var pImage = pic.Image;
            var imgAlt = "image" + pic.Id;

            //picList += '<li data-preview="data:image/png;base64,' + pImage + '><a href="#"><img src="data:image/png;base64,' + pImage + '" alt="' + imgAlt + '" style="height: 100px; width: 160px; cursor:pointer; border-radius: 10px;" /></a></li>';
            picList += '<li data-preview="data:image/png;base64,' + pImage + '" style="height: 100px; width: 160px; cursor:pointer; border-radius: 10px;">' +
                         '<a href="#">' +
                            '<img src="data:image/png;base64,' + pImage + '" alt="' + imgAlt + '"/>' +
                         '</a>' +
                       '</li>';

        });
    }).then(function () {

        picList += '</ul></div></div>';

        $(element).append(picList);
        $(element).trigger("create");

        var current = 0;

        $preview = $('#preview');
        $carouselEl = $('#carousel');
        $carouselItems = $carouselEl.children();
        carousel = $carouselEl.elastislide({
            current: current,
            minItems: 4,
            onClick: function (el, pos, evt) {                
                changeImage(el, pos);
                evt.preventDefault();
            },
            onReady: function () {

                changeImage($carouselItems.eq(current), current);

            }
        });

    });
};

function changeImage(el, pos) {

    $preview.attr('src', el.data('preview'));
    $carouselItems.removeClass('current-img');
    el.addClass('current-img');
    carousel.setCurrent(pos);

};