﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.OnhandReceipt.Incomplete_postRender = function (element, contentItem) {
    
    $(element).find('a').first().makeBackgroundRed();

    $(element).on(getSuitableEventName(), function (e) {
        $(this).find('a').first().makeBackgroundRed();

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Complete') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });

};
myapp.OnhandReceipt.Complete_postRender = function (element, contentItem) {
    
    $(element).on(getSuitableEventName(), function (e) {
        $(this).find('a').first().makeBackgroundRed();

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Incomplete') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });

};
myapp.OnhandReceipt.RefreshButton_render = function (element, contentItem) {
    
    $iScrn = contentItem.screen;
    var elem = $("<div class='am_refresh'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    elem.on('click', function () {
        fillOnhandList(contentItem.screen, true, 1);
    });
    $(element).append(elem);
    $(element).trigger("create");

};
myapp.OnhandReceipt.Search_postRender = function (element, contentItem) {
   
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        //myapp.RecoverFlight.Search_execute(contentItem.screen);
    });

};
myapp.OnhandReceipt.Reset_execute = function (screen) {
    
    screen.Filter = null;
    screen.SearchFilter = null;
    screen.DateFilter = null;
    screen.SearchDateFilter = null;

};
myapp.OnhandReceipt.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.OnhandReceipt.ReceiveNewPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'receiveNewPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.OnhandReceipt.ReceiveNewHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Receive Shipment";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.OnhandReceipt.ReceiveNewOnhand_execute = function (screen) {
    
    //$('#receiveNewPopupContent').parent().center();

    //screen.showPopup("ReceiveNewPopup");

    myapp.showReceiveNewShipment(screen.CurrentUser, screen.CurrentUserStation, screen.CurrentUserWarehouse, screen.CurrentUserName,screen.AjaxTimeout);

};
myapp.OnhandReceipt.created = function (screen) {
    
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                var uItems = result.split(',');
                if (uItems.length > 2) {

                    screen.CurrentUser = uItems[0];
                    $CurrentUser = uItems[0];

                    var sId = +uItems[1];
                    myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {
                        screen.CurrentUserStation = selectedStation.results[0];
                        $CurrentUserStation = selectedStation.results[0];
                    });
                    var wId = +uItems[2];
                    myapp.activeDataWorkspace.WFSDBData.Warehouses_SingleOrDefault(+wId).execute().then(function (selectedWarehouse) {
                        screen.CurrentUserWarehouse = selectedWarehouse.results[0];
                        $CurrentUserWarehouse = selectedWarehouse.results[0];
                    });

                    screen.CurrentUserName = uItems[3];

                }

                else {
                    screen.CurrentUser = uItems[0];
                    $CurrentUser = uItems[0];
                }
            }
        }
    }));
    
};
myapp.OnhandReceipt.OnhandList_render = function (element, contentItem) {

    var ul = $('<ul id="onhandList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.OnhandReceipt.listLoadPageNumber = 2;
    myapp.OnhandReceipt.isComplete = false;
    $(element).on('scroll', function () {
        if ($('ul#onhandList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            fillOnhandList(contentItem.screen, false, myapp.OnhandReceipt.listLoadPageNumber);
            myapp.OnhandReceipt.listLoadPageNumber = myapp.OnhandReceipt.listLoadPageNumber + 1;
        }
    });

};

function fillOnhandList(screen, clearList, pageNumber) {
    var tDate = screen.DateFilter;
    var onhandFilterDate = tDate ? (tDate.getMonth() + 1) + '/' + tDate.getDate() + '/' + tDate.getFullYear() : '';
    $.ajax({
        type: 'GET',
        data: { 'userId': screen.CurrentUser, 'rowPerPage': 10, 'pageNumber': pageNumber, 'isComplete': myapp.OnhandReceipt.isComplete, 'filter': $('input#onhandFilterTextbox').last().val(), 'dateFilter': onhandFilterDate },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetOnhandList',
        success: function (data) {
            $('span#onhandNoItems').remove();
            var list = $('ul#onhandList').last();
            if (clearList) {
                list.empty();
                myapp.OnhandReceipt.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:74px"></li>');
                var that = this;
                li.on('click', function () {
                    if (!screen.OnhandList) {
                        screen.OnhandList = {};
                    }

                    screen.OnhandList.selectedItem = that;
                    myapp.OnhandReceipt.ViewOnhand_execute(screen);
                });

                var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                //var carrierImage = $('<img class="float-left" style="width: 30px;"></img>');
                //carrierImage.attr('src', this.CarrierLogoPath);
                var statusImage = $('<img class="float-left" style="width:30px;margin-top:15px;"></img>');
                statusImage.attr('src', this.StatusImagePath);
                //statusContainer.append(carrierImage);
                statusContainer.append(statusImage);


                var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                var refNumber = $('<div style="font-size:30px">' + this.OnhandNumber + '</div>');
                var refNumber1 = $('<div style="font-size:18px"></div>');
                var acctNumber = $('<span style="margin-left:20px">ACCT: ' + this.AccountCode + '</span>')
                var rcvDate = $('<span>DTE: ' + this.ReceivedDate + '</span>');
                var infoHolder = $('<div style="font-size:18px"></div>');
                var ttlCount = $('<span>PCS:' + this.TotalPieces + '</span>');
                var rcvCount = $('<span style="margin-left:20px">RCV: ' + this.ReceivedPieces + '</span>');
                var tPro = $('<span style="margin-left:20px">PRO: ' + this.TruckerPro + '</span>');

                refNumber1.append(rcvDate);
                refNumber1.append(acctNumber);

                infoHolder.append(ttlCount);
                infoHolder.append(rcvCount);
                infoHolder.append(tPro);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(infoHolder);

                li.append(statusContainer);
                li.append(refContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="onhandNoItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get data from GetOnhandList.' + e);
        }
    })
};
myapp.OnhandReceipt.Filter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'onhandFilterTextbox');

};
myapp.OnhandReceipt.DateFilter_postRender = function (element, contentItem) {
    
    $(element).css('padding-top', 0);

    contentItem.addChangeListener(null, function () {
        if (myapp.OnhandReceipt.isComplete && !contentItem.value) {
            contentItem.value = new Date();
        }
    });

};
myapp.OnhandReceipt.Search_execute = function (screen) {
    
    fillOnhandList(screen, true, 1);

};

myapp.OnhandReceipt.ViewOnhand_execute = function (screen) {

    var nOnhand = screen.OnhandList.selectedItem;

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var cName = screen.CurrentUserName;

    myapp.showViewOnhand(nOnhand, cUser, cStation, cWarehouse, cName, aTimeout);
};
myapp.OnhandReceipt.Complete_execute = function (screen) {
    
    screen.Filter = null;
    screen.DateFilter = new Date();
    myapp.OnhandReceipt.isComplete = true;
    fillOnhandList(screen, true, 1);

};
myapp.OnhandReceipt.Incomplete_execute = function (screen) {
    
    screen.DateFilter = null;
    screen.Filter = null;
    myapp.OnhandReceipt.isComplete = false;
    fillOnhandList(screen, true, 1);

};