﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $CheckInAWBQuantityInput;
var $iScrn3;
var $AWBTypeHeader;
var $SelectedAwb;
var $AwbEntityType;

myapp.BreakdownAWB.created = function (screen) {

    var nULD = screen.SelectedUld;
    if (!nULD) {
        $SelectedUld;
    }
    var nFLT = screen.SelectedFlt;
    if (!nFLT) {
        nFLT = $FlightManifest;
    }

    screen.ShowComplete = false;
    screen.ShowAll = false;
    screen.AWBHyphen = "-";

    screen.details.displayName = "Breakdown FLT# " + nFLT.Carrier.CarrierCode + nFLT.FlightNumber +
                                 " ULD# " + nULD.Code + " " + nULD.SerialNumber;

    myapp.activeDataWorkspace.SnapshotDBData.EntityTypes_SingleOrDefault(2).execute().then(function (selectedEntity) {
        screen.AwbEntityType = selectedEntity.results[0];
        $AwbEntityType = selectedEntity.results[0];
    });

};

//call this on item tap
myapp.BreakdownAWB.CheckInAWBPieces_execute = function (screen) {    
    
    var tMft = screen.BreakdownAwbs.selectedItem;
    //if (!tMft) {
    //    tMft = $SelectedAwb;
    //}
    //var tAwb = tMft.AWB;
    var tUld = screen.SelectedUld;
    if (!tUld) {
        tUld = $SelectedUld;
    }
    var tFlt = screen.SelectedFlt;
    if (!tFlt) {
        tFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    console.log(tMft.IsTransfer);

    if (!tMft.IsTransfer) {        
        msls.showProgress(msls.promiseOperation(function (operation) {
            $.ajax({
                type: 'post',
                data: { 'FltId': tFlt.Id, 'AwbId': tMft.AwbId },
                url: '../Web/CheckForAvailHWB.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    tMft.IsTransfer = result;
                    switch (+result) {
                        case 0:  //AWB Type Undetermined
                            $(window).one("popupcreate", function (e) {
                                $(e.target).popup({
                                    positionTo: "window"
                                });
                            });
                            screen.showPopup("MissingHWBsPopup");
                            break;
                        case 1:  //Consolidation with HWBs
                            myapp.showCheckInAWBHWB(tFlt, tUld, tMft, cUser, cStation, cWarehouse, cTask, aTimeout);
                            break;
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            var sMft = screen.BreakdownAwbs.selectedItem;
                            if (!sMft) {
                                sMft = $SelectedAwb;
                            }
                            
                            screen.CheckInQuantity = +sMft.LoadCount - +sMft.ReceivedCount;
                            screen.__CheckInQuantity = sMft.LoadCount - sMft.ReceivedCount;
                            $(window).one("popupcreate", function (e) {
                                $(e.target).popup({
                                    positionTo: "window"
                                });
                            });

                            screen.showPopup("CheckInAWBPopup").then(function () {
                                setTimeout(function () {
                                    $CheckInAWBQuantityInput.focus();
                                    $CheckInAWBQuantityInput.select();

                                }, 1);
                            });
                            break;                                                
                        default:
                            break;
                    }
                }
            }
        }));
    }
    else {
        console.log(tMft.IsTransfer);
        switch (tMft.IsTransfer) {            
            case 0:  //AWB Type Undetermined
                $(window).one("popupcreate", function (e) {
                    $(e.target).popup({
                        positionTo: "window"
                    });
                });
                screen.showPopup("MissingHWBsPopup");
                break;
            case 1:  //Consolidation with HWBs
                myapp.showCheckInAWBHWB(tFlt, tUld, tMft, cUser, cStation, cWarehouse, cTask, aTimeout);
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                var sMft = screen.BreakdownAwbs.selectedItem;
                if (!sMft) {
                    sMft = $SelectedAwb;
                }                

                screen.CheckInQuantity = +sMft.LoadCount - +sMft.ReceivedCount;
                screen.__CheckInQuantity = sMft.LoadCount - sMft.ReceivedCount;
                $(window).one("popupcreate", function (e) {
                    $(e.target).popup({
                        positionTo: "window"
                    });
                });

                screen.showPopup("CheckInAWBPopup").then(function () {
                    setTimeout(function () {
                        $CheckInAWBQuantityInput.focus();
                        $CheckInAWBQuantityInput.select();
                    }, 1);
                });

                break;            
            default:
                break;
        }
    }
};

myapp.BreakdownAWB.GoToHome_execute = function (screen) {
    myapp.navigateHome();
};

myapp.BreakdownAWB.ResetFilter_execute = function (screen) {
    $('input#awbFilter').last().val('');
    fillBreakdownAwbList(screen, true, screen.SelectedFlt.Id, screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
};

myapp.BreakdownAWB.ULD_PercentReceived_render = function (element, contentItem) {            
    setTimeout(function () {
        $(element).parent().removeClass('ui-disabled');
    }, 100);

    var uScrn = contentItem.screen;
    var uItem = uScrn.SelectedUld;
    var sInd = uItem.ReceivedPieces;
    var eInd = uItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "brawbProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var uScrn = contentItem.screen;
        var uItem = uScrn.SelectedUld;
        var sInd = uItem.ReceivedPieces;
        var eInd = uItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "brawbProgressBar", element);

    });    
};

myapp.BreakdownAWB.PercentReceived_render = function (element, contentItem) {

    var aList = contentItem.parent;
    var aItem = aList.value;
    var sInd = aItem.ReceivedCount;
    var eInd = aItem.LoadCount;

    progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var aList = contentItem.parent;
        var aItem = aList.value;
        var sInd = aItem.ReceivedCount;
        var eInd = aItem.LoadCount;

        progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    });

};

myapp.BreakdownAWB.Recover_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showRecoverFlight(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.BreakdownAWB.PutAway_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    myapp.showPutAwayPieces(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.BreakdownAWB.Reset_execute = function (screen) {
    screen.HistoryFilter = null;
    screen.SearchHistoryFilter = null;
};

myapp.BreakdownAWB.Breakdown_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showBreakdownULD(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.BreakdownAWB.StatusTimestamp1_postRender = function (element, contentItem) {

    var eDate = contentItem.value;

    dateFormat(eDate, element);

};

myapp.BreakdownAWB.SnapshotImage_postRender = function (element, contentItem) {

    $(element).html("<div class='am_camera'><img src='content/images/camera_icon_w.png' style='height:24px' /></div>");
    $(element).trigger("create");

};

//show on snapshot click
myapp.BreakdownAWB.ShowAwbSnapshot_execute = function (screen) {

    var tManifest = screen.BreakdownAwbs.selectedItem;
    if (!tManifest) {
        tManifest = $FlightManifest;
    }
    //var tAWB = tManifest.AWB;
    //var tCarrier = tAWB.Carrier;
    //var cCode = tCarrier.Carrier3Code;
    //var uDescAWB = cCode + "-" + tAWB.AWBSerialNumber;
    var uDescAWB = tManifest.AwbSerialNumber;
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showSnapshot(screen.AwbEntityType, cTask, screen.CurrentUser, screen.CurrentUserWarehouse, tManifest.AwbId, uDescAWB, aTimeout);

};

myapp.BreakdownAWB.ShowNotCheckedIn_postRender = function (element, contentItem) {

    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In' || span && span.text() == 'ALL AWBs') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });

        myapp.BreakdownAWB.showComplete = 1;
        fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In' || span && span.text() == 'ALL AWBs') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });

        myapp.BreakdownAWB.showComplete = 1;
        fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
    });

};

myapp.BreakdownAWB.ShowCheckedIn_postRender = function (element, contentItem) {

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked-In' || span && span.text() == 'ALL AWBs') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        myapp.BreakdownAWB.showComplete = 2;
        fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked-In' || span && span.text() == 'ALL AWBs') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        myapp.BreakdownAWB.showComplete = 2;
        fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
    });

};

myapp.BreakdownAWB.ShowAllAwbs_postRender = function (element, contentItem) {

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In' || span && span.text() == 'Not Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        myapp.BreakdownAWB.showComplete = 3;
        fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In' || span && span.text() == 'Not Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        myapp.BreakdownAWB.showComplete = 3;
        fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
    });

};

myapp.BreakdownAWB.AddUnManifestedAWB_execute = function (screen) {

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("SearchShipmentPopup").then(function () {
        setTimeout(function () {
            //$CheckInAWBQuantityInput.focus();
            //$CheckInAWBQuantityInput.select();
            screen.CarrierSearch = null;
            screen.AWBSearch = null;
            screen.HWBSearch = null;
        }, 1);
    });

};

//myapp.BreakdownAWB.CheckIn_execute = function (screen) {
//    console.log('CheckIn_execute ');
//    $("input[type=number]").blur();

//    if (+screen.CheckInQuantity != null && +screen.CheckInQuantity > 0) {

//        var nMAWB = screen.BreakdownAwbs.selectedItem;
//        if (!nMAWB) {
//            nMAWB = $SelectedAwb;
//        }
//        var nFlt = screen.SelectedFlt;
//        if (!nFlt) {
//            nFlt = $FlightManifest;
//        }
//        var nUld = screen.SelectedUld;
//        if (!nUld) {
//            nUld = $SelectedUld;
//        }
//        var cUser = screen.CurrentUser;
//        if (!cUser) {
//            cUser = $CurrentUser;
//        }
//        var aTimeout = screen.AjaxTimeout;
//        if (!aTimeout) {
//            aTimeout = $AjaxTimeout;
//        }
//        var cQty = screen.CheckInQuantity;

//        screen.closePopup("CheckInAWBPopup").then(function () {
//            msls.showProgress(msls.promiseOperation(function (operation) {
//                $.ajax({
//                    type: 'post',
//                    data: { 'FltId': nFlt.Id, 'MawbId': nMAWB.AwbManifestId, 'cQty': cQty, 'cUser': cUser },
//                    url: '../Web/CheckInAWBPieces.ashx',
//                    timeout: aTimeout,
//                    success: operation.code(function AjaxSuccess(AjaxResult) {
//                        operation.complete(AjaxResult);
//                    }),
//                    error: operation.code(function AjaxError(AjaxResult) {
//                        AjaxResult = "Error: Unable to process request. \n\n" +
//                                      "Try again later.";
//                        operation.complete(AjaxResult);
//                    })
//                });
//            }).then(function PromiseSuccess(PromiseResult) {

//                var result = PromiseResult;

//                if (result != null || result < 0) {
//                    if (result.substring(0, 5) == "Error") {
//                        alert(result);
//                    }
//                    else {
//                        screen.SelectedUld.details.refresh();
//                        screen.SelectedFlt.details.refresh();
//                        screen.BreakdownAwbs.refresh();
//                    }
//                }
//            }));
//        }, 2000);

//    }

//};

myapp.BreakdownAWB.CheckInQuantity_postRender = function (element, contentItem) {

    $CheckInAWBQuantityInput = $("input", $(element));
    var $iControl = $(element).find("input");
    $iControl.attr("type", "text");
    $iControl.attr("readonly", "readonly");
    $iControl.addClass("inlineTarget1");

};

myapp.BreakdownAWB.CheckInAWBPopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Piece Check-In Quantity";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.BreakdownAWB.Group12_postRender = function (element, contentItem) {
    var splitIndicator = 2; // use correct property, this one is dummy
    var hasFhlMessage = 1; // use correct property, this one is dummy
    addIndicatorIfNeeded(element, 'content/images/split-icon_sm.png', splitIndicator, 1);  //use correct image 
    addIndicatorIfNeeded(element, 'content/images/exception-icon_sm.png', hasFhlMessage, 2); // use correct image
};

function addIndicatorIfNeeded(element, imageUrl, indicator, expectedIndicatorValue) {
    if (indicator == expectedIndicatorValue) {
        var image = $('<img src="' + imageUrl + '" style="height: 16px; width: 16px; margin-left: 4px; margin-top: 3px;" />');
        $(element).children().last().before(image);
    }
};

myapp.BreakdownAWB.MissingHWBsPopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $AWBTypeHeader = element;
    element.textContent = "Set AWB Type";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};


myapp.BreakdownAWB.AsssignTransfer_execute = function (screen) {
    assignMissingAwb(screen, 2);
};

myapp.BreakdownAWB.AssignSimpleAwb_execute = function (screen) {
    assignMissingAwb(screen, 3);
};

myapp.BreakdownAWB.AssignMissingHwb_execute = function (screen) {
    assignMissingAwb(screen, 4);
};

myapp.BreakdownAWB.InterlineTransfer_execute = function (screen) {
    assignMissingAwb(screen, 5);    
};

function assignMissingAwb(screen, type) {    
    var sMft = screen.BreakdownAwbs.selectedItem;
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    screen.closePopup("MissingHWBsPopup").then(function () {
        msls.showProgress(msls.promiseOperation(function (operation) {
            $.ajax({
                type: 'post',
                data: { 'AwbId': sMft.AwbId, 'TypeId': type },
                url: '../Web/SetAWBType.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;
            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    fillBreakdownAwbList(screen, true, screen.SelectedFlt.Id, screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete, function () {
                        screen.BreakdownAwbs.selectedItem = sMft;
                        screen.CheckInQuantity = +sMft.LoadCount - +sMft.ReceivedCount;
                        screen.__CheckInQuantity = sMft.LoadCount - sMft.ReceivedCount;
                        $(window).one("popupcreate", function (e) {
                            $(e.target).popup({ positionTo: "window" });
                        });

                        screen.showPopup("CheckInAWBPopup").then(function () {
                            setTimeout(function () {
                                $CheckInAWBQuantityInput.focus();
                                $CheckInAWBQuantityInput.select();
                            }, 1);
                        });
                    });
                }
            }
        }));
    });
}

//myapp.BreakdownAWB.UnCheckIn_execute = function (screen) {
//    console.log('UnCheckIn_execute');
//    $("input[type=number]").blur();

//    if (+screen.CheckInQuantity != null && +screen.CheckInQuantity > 0) {

//        var nMAWB = screen.BreakdownAwbs.selectedItem;
//        if (!nMAWB) {
//            nMAWB = $SelectedAwb;
//        }
//        var nFlt = screen.SelectedFlt;
//        if (!nFlt) {
//            nFlt = $FlightManifest;
//        }
//        var cUser = screen.CurrentUser;
//        if (!cUser) {
//            cUser = $CurrentUser;
//        }
//        var aTimeout = screen.AjaxTimeout;
//        if (!aTimeout) {
//            aTimeout = $AjaxTimeout;
//        }
//        var cQty = screen.CheckInQuantity;

//        screen.closePopup("CheckInAWBPopup").then(function () {
//            msls.showProgress(msls.promiseOperation(function (operation) {
//                $.ajax({
//                    type: 'post',
//                    data: { 'FltId': nFlt.Id, 'MawbId': nMAWB.AwbManifestId, 'cQty': cQty, 'cUser': cUser },
//                    url: '../Web/UnCheckInAWBPieces.ashx',
//                    timeout: aTimeout,
//                    success: operation.code(function AjaxSuccess(AjaxResult) {
//                        operation.complete(AjaxResult);
//                    }),
//                    error: operation.code(function AjaxError(AjaxResult) {
//                        AjaxResult = "Error: Unable to process request. \n\n" +
//                                      "Try again later.";
//                        operation.complete(AjaxResult);

//                    })
//                });
//            }).then(function PromiseSuccess(PromiseResult) {

//                var result = PromiseResult;

//                if (result != null || result < 0) {
//                    if (result.substring(0, 5) == "Error") {
//                        alert(result);
//                    }
//                    else {
//                        screen.SelectedUld.details.refresh();
//                        screen.SelectedFlt.details.refresh();
//                        screen.BreakdownAwbs.refresh();
//                    }
//                }
//            }));
//        }, 2000);
//    }
//};

myapp.BreakdownAWB.RefreshButton_render = function (element, contentItem) {

    $iScrn3 = contentItem.screen;
    var a = function (s) {
        return function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                //s.SelectedUld.details.refresh();
                s.SelectedFlt.details.refresh();
                //s.BreakdownAwbs.refresh();
                s.FlightHistory.refresh();
                operation.complete();

            }));
        }
    }($iScrn3);

    var div = $("<div class='am_refresh' id='myTest'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    div.on('click', a);
    $(element).append(div);

    $(element).trigger("create");

};

function refreshView3() {

    msls.showProgress(msls.promiseOperation(function (operation) {

        $iScrn3.SelectedUld.details.refresh();
        $iScrn3.SelectedFlt.details.refresh();
        $iScrn3.BreakdownAwbs.refresh();
        $iScrn3.FlightHistory.refresh();
        operation.complete();

    }));
};
myapp.BreakdownAWB.AWB_Carrier_Carrier3Code_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.BreakdownAWB.AWBHyphen_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.BreakdownAWB.AWB_AWBSerialNumber1_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.BreakdownAWB.SelectedFlt_WarehouseLocation_Location_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "18px");

};
myapp.BreakdownAWB.SelectedFlt_Status_Name_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "18px");

};
myapp.BreakdownAWB.CheckIn_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var nMAWB = iScrn.BreakdownAwbs.selectedItem;
        if (!nMAWB) {
            nMAWB = $SelectedAwb;
        }
        var nFlt = iScrn.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var cQty = $(this).parent().parent().find('input').val();

        iScrn.closePopup("CheckInAWBPopup").then(function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                $.ajax({
                    type: 'post',
                    data: { 'FltId': nFlt.Id, 'MawbId': nMAWB.AwbManifestId, 'cQty': cQty, 'cUser': cUser },
                    url: '../Web/CheckInAWBPieces.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        fillBreakdownAwbList(iScrn, true, iScrn.SelectedFlt.Id, iScrn.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete, function (rp, tp, pr) {
                            $('#brawbProgressBar p').text(rp + ' of ' + tp);
                            $('#brawbProgressBar div').css('width', pr);
                            iScrn.SelectedUld.PercentReceived = pr;
                            iScrn.SelectedUld.ReceivedPieces = rp;
                            fillBreakdownUldList(iScrn, true, iScrn.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
                        });

                        iScrn.SelectedFlt.details.refresh();
                    }
                }
            }));
        }, 2000);
    });
};
myapp.BreakdownAWB.UnCheckIn_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var nMAWB = iScrn.BreakdownAwbs.selectedItem;
        if (!nMAWB) {
            nMAWB = $SelectedAwb;
        }
        var nFlt = iScrn.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = iScrn.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var cQty = $(this).parent().parent().find('input').val();

        iScrn.closePopup("CheckInAWBPopup").then(function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                $.ajax({
                    type: 'post',
                    data: { 'FltId': nFlt.Id, 'MawbId': nMAWB.AwbManifestId, 'cQty': cQty, 'cUser': cUser },
                    url: '../Web/UnCheckInAWBPieces.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        iScrn.SelectedFlt.details.refresh();
                        fillBreakdownAwbList(iScrn, true, iScrn.SelectedFlt.Id, iScrn.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete, function (rp, tp, pr) {
                            $('#brawbProgressBar p').text(rp + ' of ' + tp);
                            $('#brawbProgressBar div').css('width', pr);
                            iScrn.SelectedUld.PercentReceived = pr;
                            iScrn.SelectedUld.ReceivedPieces = rp;
                            fillBreakdownUldList(iScrn, true, iScrn.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
                        });
                    }
                }
            }));
        }, 2000);
    });
};
myapp.BreakdownAWB.CheckInAWBPopup_postRender = function (element, contentItem) {

    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.BreakdownAWB.MissingHWBsPopup_postRender = function (element, contentItem) {

    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.BreakdownAWB.BreakdownAwbs_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};
myapp.BreakdownAWB.ManifestAWBDetails_postRender = function (element, contentItem) {

    $(element).css("overflow", "hidden");

};
myapp.BreakdownAWB.HistoryTab_postRender = function (element, contentItem) {

    $(element).css("overflow", "hidden");

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "History" })).parent().on(evtName, function (e) {

        contentItem.screen.FlightHistory.refresh();

    });

};
myapp.BreakdownAWB.FlightHistory_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};
myapp.BreakdownAWB.ListImage_postRender = function (element, contentItem) {

    $(element).html("<div class='am_camera'><img src='content/images/list_icon_w.png' style='padding-left: 5px; height:24px' /></div>");
    $(element).trigger("create");

};

//execute on list icon click
myapp.BreakdownAWB.OpenAWBTypePopup_execute = function (screen) {

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("MissingHWBsPopup");

};

myapp.BreakdownAWB.AssignBreakdownAwb_execute = function (screen) {

    var tMft = screen.BreakdownAwbs.selectedItem;
    var tUld = screen.SelectedUld;
    if (!tUld) {
        tUld = $SelectedUld;
    }
    var tFlt = screen.SelectedFlt;
    if (!tFlt) {
        tFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    screen.BreakdownAwbs.selectedItem.IsTransfer = 1;
    screen.closePopup("MissingHWBsPopup").then(function () {
        msls.showProgress(msls.promiseOperation(function (operation) {
            $.ajax({
                type: 'post',
                data: { 'AwbId': tMft.AwbId, 'TypeId': 1 },
                url: '../Web/SetAWBType.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    myapp.showCheckInAWBHWB(tFlt, tUld, tMft, cUser, cStation, cTask, aTimeout);
                }
            }
        }));
    });
};
myapp.BreakdownAWB.SearchShipmentPopup_postRender = function (element, contentItem) {

    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.BreakdownAWB.UnManifestedPopup_postRender = function (element, contentItem) {

    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.BreakdownAWB.SearchShipmentHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Search Shipment";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.BreakdownAWB.UnManifestedHeader_postRender = function (element, contentItem) {
    var label = $(element).parent().find('label');
    label.text('Un-Manifested Shipment');
    label.css('font-size', '32px').css('font-weight', 'bold').css('color', 'black');
};

myapp.BreakdownAWB.SearchShipment_execute = function (screen) {

    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    $.ajax({
        type: 'post',
        data: { 'carrier': screen.CarrierSearch, 'awb': screen.AWBSearch, 'hwb': screen.HWBSearch, 'uldId': screen.SelectedUld.Id },
        url: '../Web/SearchShipment.ashx',
        timeout: aTimeout,
        success: function (result) {
            var jResult = JSON.parse(result);

            screen.closePopup("SearchShipmentPopup").then(function () {
                $(window).one("popupcreate", function (e) {
                    $(e.target).popup({
                        positionTo: "window"
                    });
                });

                if (!jResult.sId) {
                    screen.showPopup("UnManifestedPopup").then(function () {
                        screen.AddCarrier = screen.CarrierSearch;
                        screen.AddAWBSerialNumber = screen.AWBSearch;
                        screen.AddHWBSerialNumber = screen.HWBSearch;
                    });
                }
                else {
                    screen.foundAwbId = jResult.sId;
                    screen.showPopup("CheckInAWBPopup");
                }
            });
        },
        error: function (result) {
            var a = result;
        }
    });

};

myapp.BreakdownAWB.CheckInAwbQuantity_execute = function (screen) {

    var cTask = screen.SelectedTask;

    if (!cTask) {
        cTask = $ActiveTask;
    }

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    $.ajax({
        type: 'post',
        data: { 'awbId': screen.foundAwbId, 'quantity': screen.CheckInQuantity, 'userName': cUser, 'taskId': cTask.Id },
        url: '../Web/CheckinAwbQuantity.ashx',
        timeout: aTimeout,
        success: function () {
            screen.closePopup("CheckInAWBPopup");
        },
        error: function () {
            screen.closePopup("CheckInAWBPopup");
        }
    });
};

myapp.BreakdownAWB.AddShipment_execute = function (screen) {

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }

    var nUld = screen.SelectedUld;
    if (!nUld) {
        nUld = $SelectedUld;
    }

    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    var aOrigin = screen.AddAOrigin;
    if (!aOrigin) {
        aOrigin = null;
    }
    else {
        aOrigin = aOrigin.Id;
    }
    var aDest = screen.AddADestination;
    if (!aDest) {
        aDest = null;
    }
    else {
        aDest = aDest.Id;
    }
    var hOrigin = screen.AddHOrigin;
    if (!hOrigin) {
        hOrigin = null;
    }
    else {
        hOrigin = hOrigin.Id;
    }
    var hDest = screen.AddHDestination;
    if (!hDest) {
        hDest = null;
    }
    else {
        hDest = hDest.Id;
    }



    $.ajax({
        type: 'post',
        data: {
            'carrier': screen.AddCarrier,
            'awbNumber': screen.AddAWBSerialNumber,
            'awbOriginId': aOrigin,
            'awbDestinationId': aDest,
            'hwbNumber': screen.AddHWBSerialNumber,
            'hwbOriginId': hOrigin,
            'hwbDestinationId': hDest,
            'quantity': screen.AddPieces,
            'userName': cUser,
            'taskId': cTask.Id,
            'uldId': nUld.Id,
            'manifestId': nFlt.Id
        },
        url: '../Web/AddUnManifestedShipment.ashx',
        timeout: aTimeout,
        success: function () {
            screen.closePopup("UnManifestedPopup");
        },
        error: function () {
            screen.closePopup("UnManifestedPopup");
        }
    });
};

myapp.BreakdownAWB.SearchReset_execute = function (screen) {

    screen.CarrierSearch = null;
    screen.AWBSearch = null;
    screen.HWBSearch = null;

};

myapp.BreakdownAWB.FlightHistoryTemplate_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-text");

};
//myapp.BreakdownAWB.SelectedFltUld_ReceivedPieces_render = function (element, contentItem) {

//    var uScrn = contentItem.screen;
//    var uItem = uScrn.SelectedUld;
//    var sInd = uItem.ReceivedPieces;
//    var eInd = uItem.TotalPieces;

//    progress(contentItem.value, sInd, eInd, "hProgressBar", element);

//    contentItem.addChangeListener("value", function () {

//        var uScrn = contentItem.screen;
//        var uItem = uScrn.SelectedFltUld;
//        var sInd = uItem.ReceivedPieces;
//        var eInd = uItem.TotalPieces;

//        progress(contentItem.value, sInd, eInd, "hProgressBar", element);

//    });

//};
myapp.BreakdownAWB.PopupKeypad_render = function (element, contentItem) {

    $(element).attr("id", "inlineTargetKeypad1");
    $(element).addClass("keypad-inline");

    $('div#inlineTargetKeypad1').keypad({
        target: $('.inlineTarget1:visible'),
        layout: ['123', '456', '789', $.keypad.SPACE + '0' + $.keypad.BACK]
    });

    var keypadTarget = null;
    $('input.inlineTarget1').focus(function () {
        if (keypadTarget != this) {
            keypadTarget = this;
            $('div#inlineTargetKeypad1').keypad('option', { target: this });
        }
    });

};
myapp.BreakdownAWB.AddCancel_execute = function (screen) {

    screen.closePopup("UnManifestedPopup");

};

myapp.BreakdownAWB.BreakdownAwbs_render = function (element, contentItem) {
    var ul = $('<ul id="breakdownAwbsList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.BreakdownAWB.listLoadPageNumber = 2;
    myapp.BreakdownAWB.showComplete = 1;
    $(element).on('scroll', function () {
        if ($('ul#breakdownAwbsList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            //console.log(myapp.BreakdownAWB.listLoadPageNumber);
            fillBreakdownAwbList(contentItem.screen, false, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, myapp.BreakdownAWB.listLoadPageNumber, myapp.BreakdownAWB.showComplete);
            myapp.BreakdownAWB.listLoadPageNumber = myapp.BreakdownAWB.listLoadPageNumber + 1;
        }
    });

    fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
};

function fillBreakdownAwbList(screen, clearList, flightId, uldId, pageNumber, listType, callbackFunc) {
    var showComplete = false;
    var showAll = false;

    if (listType === 2) {
        showComplete = true;
    }

    if (listType === 3) {
        showAll = true;
    }

    $.ajax({
        type: 'GET',
        data: { 'flightId': flightId, 'uldId': uldId, 'rowPerPage': 8, 'pageNumber': pageNumber, 'showComplete': showComplete, 'showAll': showAll, 'filter': $('input#awbFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetBreakdownAwbs',
        success: function (data) {
            $('span#noItemsAwbs').remove();
            var list = $('ul#breakdownAwbsList').last();
            if (clearList) {
                list.empty();
                myapp.BreakdownAWB.listLoadPageNumber = 2;
            }
            $(data).each(function (i) {
                if (i > 0) {
                    var li = $('<li class="custom-list-item" style="height:60px"></li>');
                    var that = this;
                    li.on('click', function () {
                        if (!screen.BreakdownAwbs) {
                            screen.BreakdownAwbs = {};
                        }

                        screen.BreakdownAwbs.selectedItem = that;
                        myapp.BreakdownAWB.CheckInAWBPieces_execute(screen);
                    });

                    var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                    var stat = $('<div style="height:36px"></div');
                    var statusImage = $('<img class="float-left" style="width: 30px;"></img>');                    
                    statusImage.attr('src', this.StatusImagePath);
                    
                    stat.append(statusImage);
                    statusContainer.append(stat);

                    if (!this.HasHwb) {
                        var hasHwbImage = $('<img class="float-left" style="margin-top: 5px;"></img>');
                        hasHwbImage.attr('src', '../images/exception-icon_sm.png');
                        statusContainer.append(hasHwbImage);
                    }

                    var refContainer = $('<div class="ref-container"></div>');
                    var refNumber = $('<div>' + this.Carrier3Code + '-' + this.AwbSerialNumber + '</div>');
                    var progressBar = $('<div class="custom-progressbar"></div>');
                    var progressBarCountHolder = $('<div class="custom-progressbar-count-holder">' + this.ReceivedCount + ' of ' + this.LoadCount + '</div>');
                    var progressBarIndicator = $('<div class="custom-progressbar-indicator"></div>');
                    progressBarIndicator.css('width', this.PercentReceived + '%');
                    progressBar.append(progressBarCountHolder);
                    progressBar.append(progressBarIndicator);

                    refContainer.append(refNumber);
                    refContainer.append(progressBar);

                    var imageContainer = $('<div class="custom-image-container"></div>');

                    var forkliftButton = $('<div class="custom-snapshot-button" style="padding:14px; margin-right:5px"></div>');
                    var forkliftImage = $('<img class="float-left" style="height:22px" src="content/images/list_icon_w.png"></img>');

                    forkliftButton.append(forkliftImage);
                    forkliftButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.BreakdownAwbs) {
                            screen.BreakdownAwbs = {};
                        }
                        screen.BreakdownAwbs.selectedItem = that;
                        myapp.BreakdownAWB.OpenAWBTypePopup_execute(screen);
                    });


                    var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px"></div>');
                    var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
                    snapshotButton.append(snapshotImage);
                    snapshotButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.BreakdownAwbs) {
                            screen.BreakdownAwbs = {};
                        }
                        screen.BreakdownAwbs.selectedItem = that;
                        myapp.BreakdownAWB.ShowAwbSnapshot_execute(screen);
                    });

                    imageContainer.append(forkliftButton);
                    imageContainer.append(snapshotButton);

                    li.append(statusContainer);
                    li.append(refContainer);
                    li.append(imageContainer);
                    list.append(li);
                }
            });

            if (!list.find('li').length) {
                list.before('<span id="noItemsAwbs">No Items</span>')
            }

            $('#brawbProgressBar p').text(data[0].UldReceivedPieces + ' of ' + data[0].UldTotalPieces);
            $('#brawbProgressBar div').css('width', data[0].UldPercentReceived);

            if (callbackFunc) {
                callbackFunc(data[0].UldReceivedPieces, data[0].UldTotalPieces, data[0].UldPercentReceived);                
            }
        },

        error: function (e) {
            console.log('Unable get date from GetPutAwayList.' + e);
        }
    })
}
myapp.BreakdownAWB.Filter_render = function (element, contentItem) {
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="awbFilter" style="margin-left:3px;"></input>');
    elem.append(filter);
};
myapp.BreakdownAWB.HistoryFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'historyFilterTextbox3');

};
myapp.BreakdownAWB.SearchBreakdownAwb_postRender = function (element, contentItem) {
   
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).find('a').off(evtName).on(evtName, function () {
        fillBreakdownAwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
    });

};
myapp.BreakdownAWB.Search_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.BreakdownAWB.Search_execute(contentItem.screen);
    });

};
myapp.BreakdownAWB.Search_execute = function (screen) {
    
    if ($('#historyFilterTextbox3').val()) {
        screen.SearchHistoryFilter = $('#historyFilterTextbox3').val();
    }
    else {
        screen.SearchHistoryFilter = null;
    }

};