﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />
/// <reference path="../Scripts/c1ls-3.0.20132.20.js" />

if (typeof c1ls === "undefined") {
    window.location.href = "http://bit.ly/c1ls-nuget";
}

myapp.Config.ScreenContent_render = function (element, contentItem) {

    var ul = "<ul class='msls-tile-list ui-listview' data-role='listview' data-inset='false'>";
    var liClass = "ui-li ui-btn ui-btn-up-a";
    var divClass = "msls-presenter msls-list-child msls-ctl-group-custom-control msls-vauto msls-hstretch msls-compact-padding msls-presenter-content msls-hscroll";
    var border = "border-left-width:1px; border-right-width:1px; border-top-width:1px; border-bottom-width:1px;";
    var width = "width: 254px; height: 100px;";
    var radius = "-moz-border-radius: 10px; -webkit-border-radius: 10px; border-radius: 10px;";

    var items = [];
    var excluded = [
                    "Screen",
                    "AddEditCarrier",
                    "AddEditCondition",
                    "AddEditStatus",
                    "AddEditWarehouseLocation",
                    "BreakdownAWB",
                    "BreakdownULD",
                    "CargoDischarge",
                    "CargoInventory",
                    "CargoReceiver",
                    "CargoSnapshot_old",
                    "CheckInAWB",
                    "CheckInAWBHWB",
                    "CheckInHWB",
                    "Home",
                    "PutAwayAWB",
                    "PutAwayHWB",
                    "PutAwayPieces",
                    "RecoverFlight",
                    "RecoverFlightULDs",
                    "RecoverLoose",
                    "SelectConditions",
                    "SelectLocation",
                    "SetStageLocation",
                    "ViewFlightManifest",
                    "ViewSnapshot",
                    "DischargeCheckInOld",
                    "ViewDischargePayments",
                    "AddEditDriver",
                    "SelectDriver",
                    "DischargePayments",
                    "AddCarrierName",
                    "SelectCarrier",
                    "SelectDriver",
                    "DischargeShipments",
                    "AddDischargePayments",
                    "DeliveryOrderDetails",
                    "AddDischargeCharge",
                    "AddSnapshot",
                    "CargoReceiverTasks",
                    "CargoInventoryTasks",
                    "TaskManager",
                    "Snapshot",
                    "InventoryTask",
                    "TrackAndTrace",
                    "ScanPutAwayPieces",
                    "CheckInFlight",
                    "CheckInFlightPieces",
                    "ReceiveNewShipment",
                    "ViewOnhand",
                    "EditOnhand"
                    ];
    excluded.push(contentItem.screen.details.getModel().name);

    $.each(myapp, function (key, value) {
        if (key.slice(0, 4) === "show") {
            var name = key.substring(4);

            if (excluded.indexOf(name) < 0) {
                myapp[name].prototype.constructor([], undefined);
                var model = myapp[name].prototype.details.getModel();
                var display = model.displayName;
                var tap = " onclick=myapp." + key + "("+ contentItem.screen.AjaxTimeout +")";
                var li = "<li title='" + display + "' class='" + liClass + "' data-msls='true' style='" + width + border + radius + "'" + tap + ">";
                var div = "<div class='" + divClass + "' style='font-size:16px; text-align:center'><span style='line-height:70px;'>" + display + "</span></div>" + "</li>";
                items.push(li + div);
            }
        }
    });

    items.sort();
    ul = ul + items.join("\r\n") + "</ul>";
    $(ul).appendTo($(element));

};

myapp.Config.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};