﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.CargoInventoryTasks.GoToHome_execute = function (screen) {

    myapp.navigateHome();

};
myapp.CargoInventoryTasks.Reset_execute = function (screen) {
    
    screen.Filter = null;

};
myapp.CargoInventoryTasks.DownloadReport_execute = function (screen) {
    
    window.open("../DownloadReport.aspx", "_blank");

};
myapp.CargoInventoryTasks.TaskCreationDate_postRender = function (element, contentItem) {
    
    var eDate = contentItem.value;

    dateFormat(eDate, element);

};
myapp.CargoInventoryTasks.StartDate_postRender = function (element, contentItem) {
    
    var eDate = contentItem.value;

    dateFormat(eDate, element);

};
myapp.CargoInventoryTasks.EndDate_postRender = function (element, contentItem) {
    
    var eDate = contentItem.value;

    dateFormat(eDate, element);

};
myapp.CargoInventoryTasks.DueDate_postRender = function (element, contentItem) {
    
    var eDate = contentItem.value;

    dateFormat(eDate, element);

};
myapp.CargoInventoryTasks.GoToHome1_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.CargoInventoryTasks.Inventory_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.CargoInventoryTasks.ActiveInventory_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.CargoInventoryTasks.Tasks_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.CargoInventoryTasks.AllInventoryTasks_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.CargoInventoryTasks.PromptViewReport_execute = function (screen) {
    
    screen.PromptViewHeader = "View Report";
    screen.PromptViewText = "Do you want to view this inventory report?";

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("ConfirmViewPopup");

};
myapp.CargoInventoryTasks.ConfirmViewPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.CargoInventoryTasks.PromptViewHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.CargoInventoryTasks.PromptViewText_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.CargoInventoryTasks.ConfirmYes_execute = function (screen) {
    
    screen.closePopup("ConfirmViewPopup").then(function () {

        var taskId = screen.AllInventoryTasks.selectedItem

        window.open("../DownloadTaskReport.aspx?TaskId=" + taskId.Id, "_blank");

    });

};
myapp.CargoInventoryTasks.ConfirmNo_execute = function (screen) {
    
    screen.closePopup("ConfirmViewPopup");

};
myapp.CargoInventoryTasks.AssignTaskPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.CargoInventoryTasks.AssignTaskHeader_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Assign Task";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.CargoInventoryTasks.HorizontalLine_render = function (element, contentItem) {
    
    $(element).append('<HR>');

};
myapp.CargoInventoryTasks.FirstName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.CargoInventoryTasks.LastName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.CargoInventoryTasks.UserId_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "40px");

};
myapp.CargoInventoryTasks.Reset1_execute = function (screen) {
   
    screen.UserFilter = null;

};
myapp.CargoInventoryTasks.Assign_postRender = function (element, contentItem) {
    
    var iScrn = contentItem.screen;

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var sUser = iScrn.UserList.selectedItem;

        var iName = $(this).parent().parent().find('input').val();

        msls.showProgress(msls.promiseOperation(function (operation) {

            $.ajax({
                type: 'post',
                data: { 'iName': iName, 'sUser': sUser.UserId },
                url: '../Web/AssignInventoryTask.ashx',
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                })
            });

        }).then(function PromiseSuccess(PromiseResult) {
            result = PromiseResult;
            iScrn.closePopup("AssignTaskPopup").then(function () {
                iScrn.AllInventoryTasks.refresh();
                iScrn.ActiveInventory.refresh();
                iScrn.UserList.refresh();
            }, 1000);
        }));
    });

};
myapp.CargoInventoryTasks.Cancel_execute = function (screen) {
    
    screen.closePopup("AssignTaskPopup");

};
myapp.CargoInventoryTasks.AssignTask_execute = function (screen) {
    
    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("AssignTaskPopup");

};
myapp.CargoInventoryTasks.created = function (screen) {

    $.getJSON("../Perms/UserPermissions/", function (data) {

        //attach the permissions to the global 'myapp' object 
        //so it is accessible to the client anywhere.
        myapp.permissions = data;
    }).then(function () {

        if (myapp.permissions["LightSwitchApplication:AssignInventoryTasks"]) {
            screen.findContentItem("AssignTask").isVisible = true;
        }
        else {
            screen.findContentItem("AssignTask").isVisible = false;
        }

    });

};
myapp.CargoInventoryTasks.Reset2_execute = function (screen) {
    
    screen.TaskFilter = null;

};