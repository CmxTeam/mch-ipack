﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.AddDischargeChargeOld.ToggleView_render = function (element, contentItem) {

    lscontrols.radioButtons(element, contentItem, {

        isHorizontal: true,
        defaultSelection: "1"

    });

    contentItem.addChangeListener("value", function () {

        var tValue = contentItem.value;
        var tScrn = contentItem.screen;

        if (tValue == "1") {
            tScrn.findContentItem("SelectedAWB").isVisible = false;
            tScrn.SelectedAWB = null;
            tScrn.findContentItem("SelectedHWB").isVisible = true;
        }
        else {
            tScrn.findContentItem("SelectedAWB").isVisible = true;
            tScrn.SelectedHWB = null;
            tScrn.findContentItem("SelectedHWB").isVisible = false;
        }

    });

};

myapp.AddDischargeChargeOld.SaveCharge_execute = function (screen) {

    var dDischarge = screen.SelectedDischarge;
    var dAmt = dDischarge.TotalAmountDue;
    var dCharge = screen.SelectedCharge;
    var cAmt = dCharge.ChargeAmount;
    var tHWB = screen.SelectedHWB;
    var tAWB = screen.SelectedAWB;

    if (dAmt != null) {
        dAmt += +cAmt;
    }
    else {
        dAmt = +cAmt;
    }

    if (tHWB != null) {
        
        myapp.activeDataWorkspace.WFSDBData.DischargeHWBDetail(+tHWB.Id, dDischarge.Id).top(1).execute().then(function (rList) {
       
            var dDetail = rList.results[0];
            dCharge.DischargeDetail = dDetail;

            myapp.applyChanges().then(function () {
                var nCharge = new myapp.DischargeCharge();
                screen.SelectedCharge = nCharge;
                screen.SelectedAWB = null;
                screen.SelectedHWB = null;
            });
        });
    }
    else {
        myapp.activeDataWorkspace.WFSDBData.DischargeAWBDetail(tAWB.Id, dDischarge.Id).top(1).execute().then(function (rList) {

            var dDetail = rList.results[0];
            dCharge.DischargeDetail = dDetail;

            myapp.applyChanges().then(function () {
                var nCharge = new myapp.DischargeCharge();
                screen.SelectedCharge = nCharge;
                screen.SelectedAWB = null;
                screen.SelectedHWB = null;
            });
            
        });

    }

};

myapp.AddDischargeChargeOld.SaveAndCloseCharge_execute = function (screen) {
    
    var dDischarge = screen.SelectedDischarge;
    var dAmt = +dDischarge.TotalAmountDue;
    var dCharge = screen.SelectedCharge;
    var cAmt = +dCharge.ChargeAmount;
    var tHWB = screen.SelectedHWB;
    var tAWB = screen.SelectedAWB;

    if (tHWB != null) {

        myapp.activeDataWorkspace.WFSDBData.DischargeHWBDetail(tHWB.Id, dDischarge.Id).top(1).execute().then(function (rList) {

            if (dAmt != null) {
                dAmt += +cAmt;
            }
            else {
                dAmt = +cAmt;
            }

            var dDetail = rList.results[0];
            //dDetail.TotalAmountDue += +cAmt;
            dCharge.DischargeDetail = dDetail;
            myapp.commitChanges();

        });
    }
    else {
        myapp.activeDataWorkspace.WFSDBData.DischargeAWBDetail(+tAWB.Id, dDischarge.Id).top(1).execute().then(function (rList) {

            var dDetail = rList.results[0];
            dCharge.DischargeDetail = dDetail;
            myapp.commitChanges();

        });

    }

};
myapp.AddDischargeChargeOld.created = function (screen) {
   
    screen.ToggleView = "1";

};