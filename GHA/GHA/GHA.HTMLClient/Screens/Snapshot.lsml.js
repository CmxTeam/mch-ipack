﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var sGallery;
var sGalleryItem;
var $file_browse_button;
var $CurrentQuantityInput;
var $ConditionList;

myapp.Snapshot.created = function (screen) {
    $.ajax({
        type: 'post',
        data: {},
        url: '../Web/GetDefaultCondition.ashx',
        success: function (result) {
            var dftItem = result.split(',');
            screen.SelectedConditionIds = dftItem[0];
            screen.Condition = dftItem[1];
            //$ConditionList = PromiseResult;
            //screen.Condition = $($ConditionList).find('input').first().attr('name');
            //screen.SelectedConditionIds = $($ConditionList).find('input').first().attr('value');        

            var sEntity = screen.SelectedEntityType;
            var sLabel = sEntity.EntityName + "# ";
            var sDesc = screen.EntityDescription;
            var disName = "Proof of Condition for " + sLabel + sDesc;

            screen.details.displayName = disName;

            if (screen.Quantity == null) {
                screen.Quantity = +1;
            }
        }
    });
};

myapp.Snapshot.ImageGallery_render = function (element, contentItem) {

    sGallery = element;
    sGalleryItem = contentItem;

    $(sGallery).css("border", "solid 1px white");

    createSlideGallery(element, contentItem);

};

function createSlideGallery(element, contentItem) {

    var iScrn = contentItem.screen;
    var picList = "";
    var firstPic = "";
    var isFirst = true;

    var wrapperDiv = $('<div class="imgContainer"></div>');
    var ul = $('<ul id="Gallery"></ul>');
    wrapperDiv.append(ul);
    $(element).append(wrapperDiv);

    getImages(contentItem.screen.SelectedEntityType.Id, contentItem.screen.SelectedEntityId);
};

myapp.Snapshot.ConditionsList_render = function (element, contentItem) {

    //if ($ConditionList != null || $ConditionList != "") {
    //    $(element).append($ConditionList);
    //}

    var wrapperDiv = $('<ul id="conditionList" class="msls-tile-list ui-listview" style="height:200px;"></ul>');
    $(element).append(wrapperDiv);

    getConditions(element, contentItem);

};

function createConditionList(element, contentItem) {
    var iScrn = contentItem.screen;
    //var cList = "";

    var wrapperDiv = $('<div id="conditionlist" class="condContainer"></div>');
    var wrapperField = $('<fieldset data-role="controlgroup"></fieldset>');
    var wrapperUl = $('<ul></ul>');
    //cList += '<div id="conditionlist" class="condContainer"><fieldset data-role="controlgroup">' +
    //         '<ul>';
    wrapperField.append(wrapperUl);
    wrapperDiv.append(wrapperField);
    $(element).append(wrapperDiv);

    iScrn.getConditions().then(function (conditions) {
        conditions.data.forEach(function (condition) {

            var cName = "checkbox-" + condition.Id + "a";
            var cValue = condition.Id;
            var condName = condition.Condition2;
            var cImage = condition.Image;

            wrapperUl.append('<li tabindex="0" data-icon="false" style="width:383px;">' +
                      '<input type="checkbox" name="' + cName + '" id="' + cName + '" value="' + cValue + '" class="mycheckbox" />' +
                      '<label for="' + cName + '" style="vertical-align: middle;" >' + '<img src="data:image/png;base64,' + cImage + '" height="30px" width="30px" style="max-width: 100%; max-height: 100%; vertical-align: middle;"><big><b>&nbsp&nbsp' + condName + '</b></big></label></li>');
            //cList += '<li tabindex="0" data-icon="false" style="width:380px;">' +
            //          '<input type="checkbox" name="' + cName + '" id="' + cName + '" value="' + cValue + '" class="mycheckbox" />' +
            //          '<label for="' + cName + '" style="vertical-align: middle;" >' + '<img src="data:image/png;base64,' + cImage + '" height="30px" width="30px" style="max-width: 100%; max-height: 100%; vertical-align: middle;"><big><b>&nbsp&nbsp' + condName + '</b></big></label></li>';
        });
    }).then(function () {

        //cList += '</ul></fieldset></div>';

        //$(element).append(cList);
        $(element).trigger("create");
        //$("input[type='checkbox']").attr("checked", true).checkboxradio("refresh");
    });
};

myapp.Snapshot.SelectCondition_execute = function (screen) {

    if (screen.Quantity == null || screen.Quantity < 0 || screen.Quantity == "") {
        screen.Quantity = 1;
    }

    $('#conditionPopupContent').parent().center();

    screen.showPopup("ConditionListPopup").then(function () {
        setTimeout(function () {
            $CurrentQuantityInput.focus();
            $CurrentQuantityInput.select();
        }, 1);
    });

};

myapp.Snapshot.TakePicture_execute = function (screen) {
    $('#htmlUploader').click();
    //$file_browse_button.click();
};

function createImageUploader1(element, contentItem) {

};

function createFallbackASPXUploader1() {

};

function previewImageAndSetContentItem1(fullBinaryString, contentItem) {
    //$preview.empty();

    if ((fullBinaryString == null) || (fullBinaryString.length == 0)) {
        contentItem.value = null;
    } else {
        //$preview.append($('<img src="' + fullBinaryString + '" style="' + previewStyle + '" />'));
        // As far as storing the data in the database, beyond previewing it, 
        //     remove the preamble returned by FileReader or the server  
        //     (always of the same form: "data:jpeg;base64," with variations only on the  
        //     type of data -- jpeg, png, etc). 
        //     The first comma serves as the necessary marker where the binary data begins. 
        contentItem.value = fullBinaryString.substring(fullBinaryString.indexOf(",") + 1);
    }
};

myapp.Snapshot.ConditionListPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'conditionPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.Snapshot.CapturedImage_postRender = function (element, contentItem) {

};

myapp.Snapshot.SelectConditionLabel_postRender = function (element, contentItem) {

    element.textContent = "Select Condition and Enter Quantity";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.Snapshot.SelectConditions_execute = function (screen) {

    var cList = "";
    var cItems = "";

    $("#conditionlist .mycheckbox:checked").each(function () {

        cList += $(this).attr("value") + ",";
        cItems += $.trim($(this).next().text()) + ", ";

    });

    cList = cList.slice(0, cList.length - 1);
    cItems = cItems.slice(0, cItems.length - 2);
    screen.SelectedConditionIds = cList;
    screen.Condition = cItems;
    screen.Quantity1 = screen.Quantity;

    screen.closePopup("ConditionListPopup");

};

myapp.Snapshot.Quantity_postRender = function (element, contentItem) {

    $CurrentQuantityInput = $("input", $(element));
    var $iControl = $(element).find("input");
    $iControl.attr("type", "text");
    $iControl.attr("readonly", "readonly");
    $iControl.addClass("inlineTarget");

};
myapp.Snapshot.Group4_postRender = function (element, contentItem) {

    $(element).css("margin-top", "8px");

};
myapp.Snapshot.PopupKeypad_render = function (element, contentItem) {

    $(element).attr("id", "inlineTargetKeypad");
    $(element).addClass("keypad-inline");

    $('div#inlineTargetKeypad').keypad({
        target: $('.inlineTarget:visible'),
        layout: ['123', '456', '789', $.keypad.SPACE + '0' + $.keypad.BACK]
    });

    var keypadTarget = null;
    $('input.inlineTarget').focus(function () {
        if (keypadTarget != this) {
            keypadTarget = this;
            $('div#inlineTargetKeypad').keypad('option', { target: this });
        }
    });

};
myapp.Snapshot.SelectConditions_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var cList = "";
        var cItems = "";

        $("div.selected-condition-checkbox").each(function () {
            var selItem = $(this).attr('id');
            var tmpItem = selItem.split('_');
            if (tmpItem.length > 2) {
                var tmpName = "";
                for (var i = 1; i < tmpItem.length; i++) {
                    tmpName += tmpItem[i] + " ";
                }
                tmpName = tmpName.slice(0, tmpName.length - 1);
                tmpItem[1] = tmpName;
            }
            cList += tmpItem[0] + ",";
            cItems += $.trim(tmpItem[1]) + ", ";
        });

        //$("#conditionlist .mycheckbox:checked").each(function () {

        //    cList += $(this).attr("value") + ",";
        //    cItems += $.trim($(this).next().text()) + ", ";

        //});

        cList = cList.slice(0, cList.length - 1);
        cItems = cItems.slice(0, cItems.length - 2);

        //if (!cList) {
        //    cList = $($ConditionList).find('input').first().attr('value');
        //    cItems = $($ConditionList).find('input').first().attr('name');
        //}

        iScrn.SelectedConditionIds = cList;
        iScrn.Condition = cItems;

        iScrn.Quantity = $(this).parent().parent().find('input[type="text"]').val();
        iScrn.Quantity1 = iScrn.Quantity;
        iScrn.closePopup("ConditionListPopup");

    });

};


myapp.Snapshot.ScreenContent_render = function (element, contentItem) {
    var htmlUploader = $('<input id="htmlUploader" type="file" accept="image/*;capture=camera">');
    htmlUploader.on('change', function (e) {
        uploadImage(e, contentItem.screen, this.files[0]);
    });
    $(element).append(htmlUploader);
};

function uploadImage(e, screen, file) {
    var overlay = $('<div id="loadingImage" style="position:absolute; top:0; left:0; width:100%; height:100%; z-index:9999; background-color:gray; opacity:0.5; background-repeat:no-repeat; background-image:url(../images/ajax-loader.gif); background-position:center center;"><div>');
    $('body').append(overlay);

    var reader = new FileReader();

    var tStamp = new Date();
    var disText = screen.SelectedEntityType.EntityName + "# " + screen.EntityDescription + " | QTY: " + screen.Quantity +
        " | Condition: " + screen.Condition + " | User: " + screen.CurrentUser + " | Date: " + tStamp.toDateString();

    reader.onload = function (event) {
        resizeBase64Img(event.target.result, 400, 400).then(function (newImage) {
            $.post('../api/cargoreceiver/SaveSnapshotImage', {
                'Image': newImage,
                'Conditions': screen.Condition,
                'EntityTypeId': screen.SelectedEntityType.Id,
                'Count': screen.Quantity,
                'EntityId': screen.SelectedEntityId,
                'UserName': screen.CurrentUser,
                'TaskId': screen.SelectedTask ? screen.SelectedTask.Id : $ActiveTask.Id,
                'WarehouseId': screen.CurrentUserWarehouse.Id,
                'FlightId': screen.SelectedTask ? screen.SelectedTask.EntityId : $ActiveTask.EntityId,
                'DisplayText': disText
            }).always(function () {
                getImages(screen.SelectedEntityType.Id, screen.SelectedEntityId);
                $('#loadingImage').remove();
            });
        });
    };

    reader.readAsDataURL(file);
}

function resizeBase64Img(base64, size) {
    var deferred = $.Deferred();
    $("<img/>").attr("src", base64).load(function () {
        var c = this.width / this.height;
        var canvas = document.createElement("canvas");
        canvas.width = size * c;
        canvas.height = size;
        var context = canvas.getContext("2d");

        context.scale((size * c) / this.width, size / this.height);
        context.drawImage(this, 0, 0);
        deferred.resolve(canvas.toDataURL());
    });
    return deferred.promise();
}

function getImages(entityTypeId, entityId) {
    var ul = $('#Gallery');
    ul.empty();
    $.ajax({
        type: 'GET',
        data: { 'entityTypeId': entityTypeId, 'entityId': entityId },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetSnapshotItems',
        success: function (data) {
            $(data).each(function () {
                var pImage = this.ImagePath;
                var imgAlt = this.DisplayText;
                ul.append('<div class="msls-presenter msls-ctl-rows-layout msls-vauto msls-hauto msls-compact-padding msls-presenter-content msls-font-style-normal msls-fixed-width msls-label-host msls-rows-layout" style="height: 85px; width: 110px;">' +
                           '<div class="msls-clear msls-presenter msls-ctl-image msls-vauto msls-hauto msls-compact-padding msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-height msls-fixed-width" style="height: 75px; width: 100px;">' +
                           '<div class="msls-image-container">' +
                           '<div class="msls-image-border" style="height:75px; width:100px; border-radius:10px;">' +
                           '<li tabindex="0" data-icon="false" class="msls-li msls-style ui-shadow ui-li ui-btn ui-first-child ui-btn-up-a" data-theme="a"><a href="' + pImage + '">' +
                           '<img src="' + pImage + '" class="' + (ul.find('li').length ? '' : 'start') + '" alt="' + imgAlt +
                           '" style="height:75px; width:100px; margin-left:0px; margin-top:0px;" />' +
                           '</a></li>' +
                           '</div>' +
                           '</div>' +
                           '</div>' +
                           '</div>');
            }).promise().done(function () {
                $(function () {
                    try {
                        $("#Gallery a").photoSwipe({ enableMouseWheel: false, enableKeyboard: false, captionAndToolbarFlipPosition: true, captionAndToolbarAutoHideDelay: 0, preventSlideshow: true });
                    } catch (ex) {
                        console.log(ex);
                    }
                });
            });
        },
        error: function () {
            console.log('Error: GetSnapshotItems');
        }
    });
}

function getConditions(element, contentItem) {

    var ul = $('#conditionList');
    ul.empty();
    $.ajax({
        type: 'GET',
        data: {},
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetConditionItems',
        success: function (data) {

            $(data).each(function () {

                var cId = this.Id;
                var cName = this.ConditionName;
                var isDefault = this.IsDefault;
                var cItem = $('<li class="custom-list-item1" style="height:40px; width:220px;"><div id="' + cId + '_' + cName.replace(/ /g, "_") + '" class="condition-checkbox unselected-condition-checkbox" /><div class="condition-span" >' + cName + '</div></li>');

                cItem.on('click', function (e) {
                    e.stopPropagation();

                    $(this).find("#" + cId + '_' + cName.replace(/ /g, "_")).toggleClass('unselected-condition-checkbox').toggleClass('selected-condition-checkbox');

                });
                ul.append(cItem);
            });
        },
        error: function () {
            console.log('Error: GetSnapshotConditions');
        }
    });

};