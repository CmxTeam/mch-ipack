﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $ScanSelQuantityInput;
var $SearchSelectInput;
var $LocationSelectInput;

myapp.InventoryTask.ScanLocationLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Scan or Select Location...";
    $(element).css("font-size", "20px");
    $(element).css("line-height", "24px");
    $(element).css("color", "red");

};

myapp.InventoryTask.ScanShipmentLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Scan or Select Shipment...";
    $(element).css("font-size", "20px");
    $(element).css("line-height", "24px");
    $(element).css("color", "red");

};

myapp.InventoryTask.SelectedLocation_Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "40px");
    $(element).css("font-weight", "bold");

};

myapp.InventoryTask.ScreenContent_render = function (element, contentItem) {
    $(element).append('<HR>');

};

myapp.InventoryTask.created = function (screen) {
 
    if (screen.SelectedInventoryTask.TaskReference != null && screen.SelectedInventoryTask.TaskReference != '') {
        screen.details.displayName = "Inventory - " + screen.SelectedInventoryTask.TaskReference;
    }
    else
    {
        screen.details.displayName = "Inventory";
    }

    screen.PiecesLabel = "PCS:";

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    var cTask = screen.SelectedInventoryTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: { 'tId': cTask.Id, 'cUser': cUser},
            url: '../Web/GetScannerPieceCount.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {

                myapp.activeDataWorkspace.SnapshotDBData.EntityTypes_SingleOrDefault(4).execute().then(function (selectedEntity2) {

                    myapp.activeDataWorkspace.SnapshotDBData.EntityTypes_SingleOrDefault(3).execute().then(function (selectedEntity1) {

                        myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(2).execute().then(function (selectedEntity3) {

                            screen.ScannerCount = result;
                            screen.AwbEntityType = selectedEntity3.results[0];
                            screen.UldEntityType = selectedEntity2.results[0];
                            screen.HwbEntityType = selectedEntity1.results[0];

                        });
                    });
                });
            }
        }
    }));

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    if (screen.SelectedInventoryTask.StartDate == null) {
        msls.showProgress(msls.promiseOperation(function (operation) {

            $.ajax({
                type: 'post',
                data: { 'tId': screen.SelectedInventoryTask.Id, 'cUser': cUser },
                url: '../Web/SetInventoryStart.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
            }
        }));
    }

};

myapp.InventoryTask.BarcodeScan_render = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var bcText = contentItem.value;
    var aTimeout = iScrn.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var cTask = iScrn.SelectedInventoryTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var chars = [];
    var ctr = "0";
    var result = "";

    if (bcText == null) {
        bcText = "";
    }

    $(document).keypress(function (event) {

        var charCode = event.which || event.keyCode;

        if (ctr == "1" && charCode != 126) {
            chars.push(String.fromCharCode(charCode));
        }

        if (charCode == 126) {
            if (ctr == "0") {
                ctr = "1";
            }
            else {
                ctr = "2";
            }

        }

        if (ctr == "2") {
            bcText = chars.join("");

            msls.showProgress(msls.promiseOperation(function (operation) {
                if (iScrn.SelectedLocation != null) {
                    $.ajax({
                        type: 'post',
                        data: { 'bcScan': bcText, 'tId': cTask.Id, 'lId': iScrn.SelectedLocation.Id },
                        url: '../Web/InventoryScan.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });
                }
                else {
                    $.ajax({
                        type: 'post',
                        data: { 'bcScan': bcText, 'tId': cTask.Id, 'lId': 0 },
                        url: '../Web/InventoryScan.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });
                }
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {

                        var rItems = result.split(',');

                        switch (rItems[0]) {

                            case "L": //Location Scan

                                myapp.activeDataWorkspace.WFSDBData.WarehouseLocations_SingleOrDefault(rItems[1]).execute().then(function (selectedLoc) {

                                    var returnedLoc = selectedLoc.results[0];

                                    if (iScrn.SelectedLocation == null) {
                                        iScrn.SelectedLocation = returnedLoc;
                                        iScrn.findContentItem("ScanLocationLabel").isVisible = false;
                                        iScrn.findContentItem("ScanShipmentLabel").isVisible = true;
                                    }
                                    else if (returnedLoc != iScrn.SelectedLocation) {
                                        iScrn.SelectedLocation = returnedLoc;
                                        iScrn.TempLocation = returnedLoc;
                                        iScrn.CloseLocationHeader = "Change Location";
                                        iScrn.CloseLocationText = "The location will be changed to " + iScrn.TempLocation.Location + " for items currently in the scanner view.";
                                        iScrn.showPopup("CloseLocationPopup");
                                    }
                                    else {
                                        iScrn.CloseLocationHeader = "Confirm";
                                        if (iScrn.ScannerCount == 1) {
                                            iScrn.CloseLocationText = iScrn.ScannerCount + " piece will be scanned at " + iScrn.SelectedLocation.Location;
                                        }
                                        else {
                                            iScrn.CloseLocationText = iScrn.ScannerCount + " pieces will be scanned at " + iScrn.SelectedLocation.Location;
                                        }
                                        iScrn.showPopup("CloseLocationPopup");
                                    }

                                });

                                chars = [];
                                ctr = "0";
                                bcText = "";
                                event = null;
                                result = null;

                                break;

                            case "A": //AWB Scan Received at AWB Level

                                if (iScrn.SelectedLocation == null) {
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;
                                    iScrn.PromptPopupHeader = "No Selected Location";
                                    iScrn.PromptPopupText = "Scan or select a location before scanning a shipment barcode.";
                                    iScrn.showPopup("PromptPopup");
                                }
                                else {
                                    iScrn.EntityType = "AWB";
                                    iScrn.EntityId = rItems[1];
                                    iScrn.SelectedQuantity = 1;
                                    iScrn.EntityCount = 1;
                                    iScrn.AddToScannerText = "AWB# " + rItems[2];
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                    $(window).one("popupcreate", function (e) {

                                        $(e.target).popup({

                                            positionTo: "window"

                                        });

                                    });

                                    iScrn.showPopup("AddToScannerPopup").then(function () {
                                        setTimeout(function () {

                                            $ScanSelQuantityInput.focus();
                                            $ScanSelQuantityInput.select();

                                        }, 100);
                                    });
                                }

                                break;

                            case "H": // HWB Scan Received at HWB Level

                                if (iScrn.SelectedLocation == null) {
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;
                                    iScrn.PromptPopupHeader = "No Selected Location";
                                    iScrn.PromptPopupText = "Scan or select a location before scanning a shipment barcode.";
                                    iScrn.showPopup("PromptPopup");
                                }
                                else {
                                    iScrn.EntityType = "HWB";
                                    iScrn.EntityId = rItems[1];
                                    iScrn.SelectedQuantity = 1;
                                    iScrn.EntityCount = 1;
                                    iScrn.AddToScannerText = "HWB# " + rItems[2];
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                    $(window).one("popupcreate", function (e) {

                                        $(e.target).popup({

                                            positionTo: "window"

                                        });

                                    });

                                    iScrn.showPopup("AddToScannerPopup").then(function () {
                                        setTimeout(function () {

                                            $ScanSelQuantityInput.focus();
                                            $ScanSelQuantityInput.select();

                                        }, 100);
                                    });
                                }

                                break;

                            case "F": // Scan Details Not Found


                                chars = [];
                                ctr = "0";
                                bcText = "";
                                event = null;
                                result = null;
                                iScrn.PromptPopupHeader = "Invalid Scan";
                                iScrn.PromptPopupText = "Invalid Barcode Scan.";
                                iScrn.showPopup("PromptPopup");
                                //msls.showMessageBox("Invalid Barcode Scan");

                                break;

                            default: // Scan Details Not Found

                                chars = [];
                                ctr = "0";
                                bcText = "";
                                event = null;
                                result = null;
                                iScrn.PromptPopupHeader = "Invalid Scan";
                                iScrn.PromptPopupText = "Invalid Barcode Scan.";
                                iScrn.showPopup("PromptPopup");
                                //msls.showMessageBox("Invalid Barcode Scan");

                                break;

                        }
                    }
                }
            }));
        }
    });
};

myapp.InventoryTask.RefNumber_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.InventoryTask.RefNumber1_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.InventoryTask.Count_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.InventoryTask.SnapshotImage_postRender = function (element, contentItem) {
    
    $iScrn = contentItem.screen;

    $(element).html("<div class='am_camera_big'><img src='content/images/camera_icon_w.png' style='height:36px' /></div>");
    $(element).trigger("create");

};

myapp.InventoryTask.ShowSnapshot_execute = function (screen) {
    
    var sItem = screen.ActiveLocationItems.selectedItem;
    var sTask = screen.SelectedInventoryTask;

    switch (sItem.Type) {
        case "AWB":
            var uDesc = sItem.RefNumber;
            myapp.showSnapshot(screen.AwbEntityType, sTask, screen.CurrentUser, sItem.EntityId, uDesc);
            break;
        case "HWB":
            var uDesc = sItem.RefNumber1;
            myapp.showSnapshot(screen.HwbEntityType, sTask, screen.CurrentUser, sItem.EntityId, uDesc);
            break;
        case "ULD":
            var uDesc = sItem.RefNumber;
            myapp.showSnapshot(screen.UldEntityType, sTask, screen.CurrentUser, sItem.EntityId, uDesc);
            break;
        default:
            break;
    }

};

myapp.InventoryTask.CountLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "PCS: ";
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.InventoryTask.ActiveLocationItems_postRender = function (element, contentItem) {
    
    $(element).css('overflow', 'auto');

};

myapp.InventoryTask.Inventory_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};

myapp.InventoryTask.ScannerCount_render = function (element, contentItem) {
    
    if (contentItem.value == null) {
        contentItem.value = 0;
    }

    $iScrn = contentItem.screen;

    $(element).html("<div class='am_scanner' onclick='showScannerView()' ><img src='content/images/scanner_icon.png' style='height: 41px;' /><span id='scCtr'>" + contentItem.value + "</span></div>");
    $(element).trigger("create");

    contentItem.addChangeListener("value", function () {

        $('#scCtr').text(contentItem.value);

    });

};

function showScannerView() {

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    $iScrn.showPopup("ScannerViewPopup");

};

myapp.InventoryTask.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};

myapp.InventoryTask.ScannerViewPopupHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Scanner View";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.InventoryTask.PiecesLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.InventoryTask.Pieces_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.InventoryTask.AddToScannerHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Add To Scanner";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.InventoryTask.ShowAddToScannerPopup_execute = function (screen) {
    
    var sItem = screen.ActiveLocationItems.selectedItem;
    var nTask = screen.SelectedInventoryTask;

    switch (sItem.Type) {
        case "AWB":

            screen.SelectedQuantity = sItem.Count;
            screen.EntityCount = sItem.Count;
            screen.EntityType = sItem.Type;
            screen.EntityId = sItem.EntityId;
            screen.AddToScannerText = "AWB# " + sItem.RefNumber;
            $(window).one("popupcreate", function (e) {

                $(e.target).popup({

                    positionTo: "window"

                });

            });

            screen.showPopup("AddToScannerPopup").then(function () {
                setTimeout(function () {

                    $ScanSelQuantityInput.focus();
                    $ScanSelQuantityInput.select();

                }, 100);
            });

            break;
        case "HWB":
            
            screen.SelectedQuantity = +sItem.Count;
            screen.EntityCount = sItem.Count;
            screen.EntityType = sItem.Type;
            screen.EntityId = sItem.EntityId;
            screen.AddToScannerText = "HWB# " + sItem.RefNumber1;
            $(window).one("popupcreate", function (e) {

                $(e.target).popup({

                    positionTo: "window"

                });

            });

            screen.showPopup("AddToScannerPopup").then(function () {
                setTimeout(function () {

                    $ScanSelQuantityInput.focus();
                    $ScanSelQuantityInput.select();

                }, 100);
            });

            break;
        case "ULD":

            screen.ConfirmPopupHeader = "Add to Scanner";
            screen.ConfirmText = "Do you want to add ULD# " + sItem.RefNumber + " to scanner?";
            screen.EntityCount = sItem.Count;
            screen.EntityType = sItem.Type;
            screen.EntityId = sItem.EntityId;

            $(window).one("popupcreate", function (e) {

                $(e.target).popup({

                    positionTo: "window"

                });

            });

            screen.showPopup("ConfirmPopup");

            break;
        default:
            break;

    }

};

myapp.InventoryTask.SelectedQuantity_postRender = function (element, contentItem) {
    
    $ScanSelQuantityInput = $("input", $(element));
    var $iControl = $(element).find("input");
    $iControl.attr("type", "text");

};

myapp.InventoryTask.AddToScannerPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.InventoryTask.ScannerViewPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.css("overflow", "hidden");
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

    addFooterIntoScanPopup(element, contentItem, 'awbForkliftViewFooter', 'ScannerViewPopup');

};

function addFooterIntoScanPopup(element, contentItem, footerId, popupName) {
    // Write code here.
    //$(element).css("overflow", "auto");
    var container = $(element);
    container.find('> div:nth-child(2)').css('max-height', '400px').css('overflow-y', 'auto').css('overflow-x', 'hidden');
    container.find('div:nth-child(2)').last().css('overflow', 'visible');

    var header = $('<div id=h"' + footerId + '" class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');
    var footer = $('<div id="' + footerId + '" class="popup-footer msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; bottom:0"></div>');

    //var buttonWrapper = $('<div style="float:right"></div>');
    //var selectLocationButton = $('<div class="msls-last-column  msls-presenter msls-ctl-button msls-leaf msls-presenter-content msls-font-style-normal msls-tap" style="min-width: 90px;"><a data-theme="a" data-mini="true" data-role="button" class="id-element ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-a" tabindex="0" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span"><span class="ui-btn-inner" style="padding:0 7px"><span class="ui-btn-text">Select Location</span></span></a></div>');
    //var assignLocationButton = $('<div class="msls-last-column  msls-presenter msls-ctl-button msls-vauto msls-hauto msls-leaf msls-presenter-content msls-font-style-normal msls-tap" style="min-width: 90px;"><a data-theme="a" data-mini="true" data-role="button" class="id-element ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-a" tabindex="0" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span"><span class="ui-btn-inner" style="padding:0 7px"><span class="ui-btn-text">Assign Location</span></span></a></div>');

    //var sc = contentItem.screen;

    //selectLocationButton.click(function () {
    //    sc.closePopup(popupName).then(function () {
    //        $sourcePopup = popupName;
    //        $locationHolder = locationHolderId;
    //        sc.showPopup("SelectLocationPopup");
    //    });
    //});

    //assignLocationButton.click(function () {
    //    executeAssignLocation(contentItem);
    //});

    //buttonWrapper.append(selectLocationButton);
    //buttonWrapper.append(assignLocationButton);

    //var selectedPiecesContainer = $('<div id="' + selectedContainerId + '" style="display:none"></div>');

    //var wrapper = $('<div style="float: left; margin-top: 27px; font-size:16px"></div>');

    //var piecesWrapper = $('<div></div>');
    //var lable = $('<span>Total selected pieces: </span>');
    //var total = $('<span id="' + totalHolderId + '"></span>');
    //piecesWrapper.append(lable);
    //piecesWrapper.append(total);

    //var locationWrapper = $('<div></div>');
    //var sLocation = $('<span id="' + selectedLocationId + '"></span>');
    //locationWrapper.append(sLocation);
    //sLocation.on('DOMNodeInserted', function () {
    //    if (sLocation.text() && sLocation.text() != '') {
    //        executeAwbAssignLocation(contentItem);
    //    }
    //});

    //wrapper.append(piecesWrapper);
    //wrapper.append(locationWrapper);

    //footer.append(wrapper);
    //footer.append(selectedPiecesContainer);
    //footer.append(buttonWrapper);
    container.children().first().before(header);
    container.children().last().before(footer);
};

myapp.InventoryTask.RefNumber2_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.InventoryTask.RefNumber11_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.InventoryTask.ActiveScannerView_postRender = function (element, contentItem) {
    
    $(element).css('overflow', 'auto');

};

myapp.InventoryTask.ConfirmPopupHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.InventoryTask.ConfirmText_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.InventoryTask.ConfirmPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.InventoryTask.AddToScannerText_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.InventoryTask.PromptOk_execute = function (screen) {
    
    screen.closePopup("PromptPopup");
    screen.PromptPopupHeader = null;
    screen.PromptPopupText = null;

};
myapp.InventoryTask.PromptPopupHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.InventoryTask.PromptPopupText_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.InventoryTask.PromptPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.InventoryTask.SelectLocationPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.InventoryTask.SelectLocationPopupHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Select Location";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.InventoryTask.Reset1_execute = function (screen) {
   
    screen.LocationFilter = null;
    $LocationSelectInput.focus();
    $LocationSelectInput.select();

};
myapp.InventoryTask.SelectLocation_execute = function (screen) {
    
    screen.WarehouseLocations.selectedItem = null;

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("SelectLocationPopup").then(function () {
        setTimeout(function () {

            $LocationSelectInput.focus();
            $LocationSelectInput.select();

        }, 100);
    });

};
myapp.InventoryTask.Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.InventoryTask.WarehouseLocationType_LocationType_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.InventoryTask.WarehouseLocations_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.InventoryTask.SetLocation_execute = function (screen) {
    
    screen.closePopup("SelectLocationPopup").then(function () {

        if (screen.SelectedLocation == null) {
            screen.SelectedLocation = screen.WarehouseLocations.selectedItem;
            screen.findContentItem("ScanLocationLabel").isVisible = false;
            screen.findContentItem("ScanShipmentLabel").isVisible = true;
        }
        else if (screen.SelectedLocation != screen.WarehouseLocations.selectedItem) {

            screen.TempLocation = screen.WarehouseLocations.selectedItem;
            screen.CloseLocationHeader = "Change Location";
            screen.CloseLocationText = "The location will be changed to " + screen.TempLocation.Location + " for items currently in the scanner view.";
            screen.showPopup("CloseLocationPopup");
            //screen.SelectedLocation = screen.WarehouseLocations.selectedItem

        }
        else {

            screen.CloseLocationHeader = "Confirm";
            if (screen.ScannerCount == 1) {
                screen.CloseLocationText = screen.ScannerCount + " piece will be scanned at " + screen.SelectedLocation.Location;
            }
            else {
                screen.CloseLocationText = screen.ScannerCount + " pieces will be scanned at " + screen.SelectedLocation.Location;
            }
            screen.showPopup("CloseLocationPopup");

            //screen.SelectedLocation = null;
            //screen.findContentItem("ScanLocationLabel").isVisible = true;
            //screen.findContentItem("ScanShipmentLabel").isVisible = false;
        }


    });

};

myapp.InventoryTask.ConfirmYes_execute = function (screen) {
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    screen.closePopup("ConfirmPopup").then(function () {
        msls.showProgress(msls.promiseOperation(function (operation) {

            var eType = screen.EntityType;
            var eId = screen.EntityId;
            var tId = screen.SelectedInventoryTask;
            var sQty = 1;
            if (eType != "ULD") {
                sQty = screen.SelectedQuantity;
            }

            $.ajax({
                type: 'post',
                data: { 'eType': eType, 'eId': eId, 'tId': tId.Id, 'sQty': sQty, 'cUser': cUser},
                url: '../Web/AddScannerPieces.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });

        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    screen.ActiveScannerView.refresh();
                    screen.ScannerCount = +screen.ScannerCount + +1;
                }
            }

        }));
    });

};
myapp.InventoryTask.Reset_execute = function (screen) {
    
    //var sItem = screen.ActiveLocationItems.selectedItem;
    screen.SelectedQuantity = screen.EntityCount;

    $ScanSelQuantityInput.focus();
    $ScanSelQuantityInput.select();

};
myapp.InventoryTask.ConfirmNo_execute = function (screen) {
    
    screen.closePopup("ConfirmPopup");

};
myapp.InventoryTask.AddToScanner1_postRender = function (element, contentItem) {
    
    var iScrn = contentItem.screen;
    var cUser = iScrn.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = iScrn.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var eType = iScrn.EntityType;
        var eId = iScrn.EntityId;
        var sTask = iScrn.SelectedInventoryTask;

        var sQty = $(this).parent().parent().find('input').val();

        msls.showProgress(msls.promiseOperation(function (operation) {

            $.ajax({
                type: 'post',
                data: { 'eType': eType, 'eId': eId, 'tId': sTask.Id, 'sQty': sQty, 'cUser': cUser },
                url: '../Web/AddScannerPieces.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });

        }).then(function PromiseSuccess(PromiseResult) {
            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    iScrn.closePopup("AddToScannerPopup").then(function () {
                        iScrn.ActiveScannerView.refresh();
                        iScrn.ScannerCount = +iScrn.ScannerCount + +sQty;
                    }, 1000);
                }
            }
        }));
    });

};
myapp.InventoryTask.CloseLocationPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.InventoryTask.CloseLocationHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.InventoryTask.CloseLocationText_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");
    
};
myapp.InventoryTask.ConfirmYes1_execute = function (screen) {
    
    screen.closePopup("CloseLocationPopup").then(function () {
        if (screen.CloseLocationHeader == "Change Location") {
            screen.SelectedLocation = screen.TempLocation;
        }

        if(screen.CloseLocationHeader == "Confirm")
        {
            msls.showProgress(msls.promiseOperation(function (operation) {

                var lId = screen.SelectedLocation;
                var tId = screen.SelectedInventoryTask;
                var cUser = screen.CurrentUser;
                if (!cUser) {
                    cUser = $CurrentUser;
                }
                var aTimeout = screen.AjaxTimeout;
                if (!aTimeout) {
                    aTimeout = $AjaxTimeout;
                }
                $.ajax({
                    type: 'post',
                    data: {'tId': tId.Id, 'sLocId': lId.Id, 'cUser': cUser },
                    url: '../Web/CloseInventoryLocation.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });

            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        screen.SelectedLocation = null;
                        screen.ScannerCount = 0;
                        screen.findContentItem("ScanLocationLabel").isVisible = true;
                        screen.findContentItem("ScanShipmentLabel").isVisible = false;
                        screen.ActiveScannerView.refresh();
                        screen.ActiveLocationItems.refresh();
                        screen.InventoryHistory.refresh();
                    }
                }
            }));
            
        }
    });

};
myapp.InventoryTask.ConfirmNo1_execute = function (screen) {
    
    screen.closePopup("CloseLocationPopup");

};
myapp.InventoryTask.StatusTimestamp_postRender = function (element, contentItem) {
   
    var eDate = contentItem.value;

    dateFormat(eDate, element);

};
myapp.InventoryTask.GoToHome1_execute = function (screen) {

    myapp.navigateHome();

};
myapp.InventoryTask.Reset2_execute = function (screen) {
    
    screen.HistoryFilter = null;

};
myapp.InventoryTask.History_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.InventoryTask.FinalizeInventoryPopup_postRender = function (element, contentItem) {
   
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.InventoryTask.FinalizeInventoryHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.InventoryTask.FinalizeInventoryText_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");
    
};
myapp.InventoryTask.Finalize_execute = function (screen) {
    
    screen.FinalizeInventoryHeader = "Finalize Inventory";
    screen.FinalizeInventoryText = "Do you want to finalize this inventory?";

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("FinalizeInventoryPopup");

};
myapp.InventoryTask.ConfirmNo2_execute = function (screen) {
    
    screen.closePopup("FinalizeInventoryPopup");

};
myapp.InventoryTask.ConfirmYes2_execute = function (screen) {
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {

        var tId = screen.SelectedInventoryTask;

        $.ajax({
            type: 'post',
            data: { 'tId': tId.Id, 'cUser': cUser },
            url: '../Web/FinalizeInventory.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;
        
        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                myapp.showCargoInventory();
            }
        }

    }));

};
myapp.InventoryTask.ActiveScannerViewTemplate_postRender = function (element, contentItem) {
    
    enhanceScannerViewItems(element, contentItem, '#selectedAwbsContainer', 'awbForkliftDeteilId', '#awbTtl', removeFromScanner, assignAwbLocation);

};

function enhanceScannerViewItems(element, contentItem, selectedContainerId, itemId, totalHolderId, removeAction) {
    //var assignLocationImage = $('<img title="Assign Location" src="content/images/selectputawayicon.png" style="height: 65px;" />');
    var removeImage = $('<img title="Remove" src="content/images/remove-icon.png" style="height: 60px; margin-left: 32px; margin-top:27px" />');

    //$(element).children().last().before(assignLocationImage);
    $(element).children().last().before(removeImage);

    removeImage.one('click', function (e) {
        return actionItemClicked1(e, contentItem, 'Remove', '15px', false, selectedContainerId, itemId, totalHolderId, removeAction);
    });

    //assignLocationImage.one('click', function (e) {
    //    return actionItemClicked(e, contentItem, 'Select', '0px', true, selectedContainerId, itemId, totalHolderId, assignLocationAction);
    //});
};

function actionItemClicked1(evt, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action) {
    var sender = $(evt.currentTarget);
    var mainDiv = sender.parent();

    var wrappaerDiv = $('<div style="display: none; float: right; height: 72px; position: relative; width: 448px; margin-top:0px;"></div>');
    var commentWrapper = $('<div style="height: 50px; position: relative; width: 380px; margin-top: ' + marginTop + '; display: inline-block; font-size:28px;"></div>');

    var firstComment = $('<span style="display: inline-block; margin-right:4px;">' + message + '</span>');
    var secondComment = $('<span style="margin-left:4px">pieces?</span>');
    var textbox = $('<input type="number" style="height: 28px; width: 80px;font-size:28px"></input>');

    var removeButton = $('<span title="Remove Pieces" style="position: absolute; display: inline-block; background-size: cover; height: 60px; background-position: -727px -1px; margin-top: 0px; width: 60px; margin-left: -62px;" class="ui-icon ui-icon-check ui-icon-shadow">&nbsp;</span>');
    var cancelButton = $('<span title="Cancel" style="position: absolute; display: inline-block; background-size: cover; height: 60px; margin-top: 0px; background-position: -253px -1px; width: 60px; margin-left: 16px;" class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span>');

    textbox.val(cntItem.value.Pieces);

    commentWrapper.append(firstComment);
    commentWrapper.append(textbox);
    commentWrapper.append(secondComment);

    if (needCountHolder) {
        removeButton.css('margin-top', '-15px');
        cancelButton.css('margin-top', '-15px');
        var countWrapper = $('<div style="padding-bottom: 5px; padding-top: 0; display:none;"></div>');
        var countHolderLabel = $('<span style="">Selected Pieces:</span>');
        var countHolder = $('<span style="" class="countHolder"></span>');
        var countRemover = $('<span title="Remove" style="display: inline-block; background-size: cover; height: 37px; width: 37px; background-position: -150px -4px;" class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span>');

        var itm = $('#selectedAwbsContainer').find('div[' + itemId + ' = ' + cntItem.value.PaId + ' ]').first();
        if (itm && itm.length >= 1) {
            countHolder.text(itm.attr('count'));
            countWrapper.css('display', 'inline-block');
        }

        countWrapper.append(countHolderLabel);
        countWrapper.append(countHolder);
        countWrapper.append(countRemover);
        countRemover.on('click', function () {
            $(this).parent().children('.countHolder').text('');
            $(this).parent().css('display', 'none');

            var selectedPiecesContainer = $(selectedContainerId);
            selectedPiecesContainer.find('div[' + itemId + ' = ' + cntItem.value.PaId + ' ]').remove();

            var totalCount = 0;
            selectedPiecesContainer.children().each(function () {
                totalCount += parseInt($(this).attr('count'));
            });

            $(totalHolderId).text(totalCount);
        });

        commentWrapper.append(countWrapper);
    }

    cancelButton.one('click', function () {
        restoreDefeultView(wrappaerDiv, sender, evt, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action);
    });

    removeButton.on('click', function () {
        action(cntItem, textbox.val(), countWrapper);
    });

    wrappaerDiv.append(commentWrapper);
    wrappaerDiv.append(removeButton);
    wrappaerDiv.append(cancelButton);


    mainDiv.children().last().before(wrappaerDiv);

    var elems = mainDiv.find('> div:first-child > div:not(:first-child), img');

    elems.each(function (idx) {
        $(this).fadeOut('slow', function () {
            if (idx == elems.length - 1) {
                wrappaerDiv.fadeIn('slow', function () {
                    wrappaerDiv.css('display', 'inline - block');
                });
            }
        });
    });
};

function restoreDefeultView(w, l, ie, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action) {
    w.fadeOut('slow', function () {
        var dvs = l.parent().find('> div:first-child > div:not(:first-child), img');
        dvs.each(function (idx) {
            $(this).fadeIn('slow');
        });
        $(this).remove();
        l.one('click', function () {
            return actionItemClicked(ie, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action);
        });
    });
};

function removeFromScanner(cntItem, cnt, cntHolder) {
    var sc = cntItem.screen;
    var cUser = sc.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = sc.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'ForkliftDetailId': cntItem.value.Type + cntItem.value.EntityId, 'Count': cnt, 'cUser': cUser },
            url: '../Web/RemoveForkliftAwbs.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                sc.ActiveScannerView.refresh();
                sc.ScannerCount -= cnt;
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
        }
    }));
};
myapp.InventoryTask.SearchShipmentPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.InventoryTask.SearchShipmentHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.InventoryTask.Reset3_execute = function (screen) {
   
    screen.SearchCriteria = null;
    $SearchSelectInput.focus();
    $SearchSelectInput.select();

};
myapp.InventoryTask.CountLabel1_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "PCS: ";
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.InventoryTask.Shipment_execute = function (screen) {
    
    screen.SearchShipmentHeader = "Search";
    screen.SearchCriteria = null;

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("SearchShipmentPopup").then(function () {
        setTimeout(function () {

            $SearchSelectInput.focus();
            $SearchSelectInput.select();

        }, 100);
    });
};
myapp.InventoryTask.SearchLocationItems_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.InventoryTask.RefNumber3_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.InventoryTask.RefNumber12_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.InventoryTask.Count1_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.InventoryTask.LocationLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "LOC: ";
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.InventoryTask.WarehouseLocation_Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.InventoryTask.SearchCriteria_postRender = function (element, contentItem) {
    
    $SearchSelectInput = $("input", $(element));

};
myapp.InventoryTask.LocationFilter_postRender = function (element, contentItem) {
    
    $LocationSelectInput = $("input", $(element));

};
myapp.InventoryTask.SearchSelectedShipment_execute = function (screen) {
    
    var sItem = screen.SearchLocationItems.selectedItem;

    screen.closePopup("SearchShipmentPopup").then(function () {

        screen.EntityType = sItem.Type;
        screen.EntityId = sItem.EntityId;
        screen.EntityCount = sItem.Count;
        screen.SelectedQuantity = screen.EntityCount;

        switch(sItem.Type)
        {
            case "AWB":
                screen.AddToScannerText = "AWB# " + sItem.RefNumber;

                $(window).one("popupcreate", function (e) {

                    $(e.target).popup({

                        positionTo: "window"

                    });

                });

                screen.showPopup("AddToScannerPopup").then(function () {
                    setTimeout(function () {

                        $ScanSelQuantityInput.focus();
                        $ScanSelQuantityInput.select();

                    }, 100);
                });

                break;
            case "HWB":
                screen.AddToScannerText = "HWB# " + sItem.RefNumber1;

                $(window).one("popupcreate", function (e) {

                    $(e.target).popup({

                        positionTo: "window"

                    });

                });

                screen.showPopup("AddToScannerPopup").then(function () {
                    setTimeout(function () {

                        $ScanSelQuantityInput.focus();
                        $ScanSelQuantityInput.select();

                    }, 100);
                });

                break;
            case "ULD":
                screen.ConfirmPopupHeader = "Add to Scanner";
                screen.ConfirmText = "Do you want to add ULD# " + sItem.RefNumber + " to scanner?";
                screen.AddToScannerText = "ULD# " + sItem.RefNumber;

                $(window).one("popupcreate", function (e) {

                    $(e.target).popup({

                        positionTo: "window"

                    });

                });

                screen.showPopup("ConfirmPopup");

                break;
            default:
                break;
        }

        

    });

};
myapp.InventoryTask.InventoryHistoryTemplate_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-text");

};