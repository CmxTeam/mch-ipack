﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.DischargePaymentsOld.ViewPayments_execute = function (screen) {
    
    myapp.showViewDischargePayments(screen.SelectedDischarge);

};

myapp.DischargePaymentsOld.CheckBoxColumn_render = function (element, contentItem) {
    
    var checkbox = getCheckBox(contentItem);
    contentItem.value.details["__isSelected"] = true;
    $(checkbox).prop("checked", true);
    $(checkbox).appendTo($(element));

};

function getCheckBox(contentItem) {

    var checkbox = $("<input type='checkbox' />");   
    checkbox.css("height", "30");
    checkbox.css("width", "30");
    checkbox.css("margin", "10px");
    

    checkbox.change(function () {
        var checked = checkbox[0].checked;
        if (!!contentItem.value.details["__isSelected"] !== checked) {
            contentItem.value.details["__isSelected"] = checked;
        }
    });
    return checkbox;
};

myapp.DischargePaymentsOld.AddPayment_execute = function (screen) {
    
    var total = 0;
    var cList = "";

    screen.getAllDischargeCharges().then(function (charges) {
        charges.data.forEach(function (charge) {
            if (charge.details["__isSelected"]) {
                total += +charge.ChargeAmount;
                cList += charge.Id + ",";
                condition.details["__isSelected"] = !condition.details["__isSelected"];
            }
        });
    });

    cList = cList.slice(0, cList.length - 1);
    var sDischarge = screen.SelectedDischarge;
    myapp.showAddDischargePayments(total, cList, sDischarge, {
        beforeShown: function (addScreen) {

            var nPayment = new myapp.DischargePayment();
            nPayment.Discharge = sDischarge;
            nPayment.PaymentAmount = total.toFixed(2);
            addScreen.EnteredPayment = nPayment;
            addScreen.TotalAmountPaid = 0;
            addScreen.InitialAmount = total.toFixed(2);

        },
        afterClosed: function()
        {
            screen.AllDischargeCharges.refresh().then(function () { 
                screen.SelectedDischarge.details.refresh().then(function () {
                    msls.promiseOperation(function (operation) {

                        $.ajax({
                            type: 'post',
                            data: { 'dId': screen.SelectedDischarge.Id },
                            url: '../Web/GetDischargeTotals.ashx',
                            success: operation.code(function AjaxSuccess(AjaxResult) {
                                operation.complete(AjaxResult);
                            })
                        });

                    }).then(function PromiseSuccess(PromiseResult) {

                        var ttlList = PromiseResult;
                        var ttlItems = ttlList.split(',');

                        screen.TotalCharges = ttlItems[0];
                        screen.TotalAmountPaid = ttlItems[1];
                        screen.TotalAmountDue = ttlItems[2];

                    });
                });
            });
        }
    });
};

myapp.DischargePaymentsOld.AddCharge_execute = function (screen) {

    var tDischarge = screen.SelectedDischarge;

    myapp.showAddDischargeCharge(tDischarge, null, {
        beforeShown: function (addScreen) {

            var nCharge = new myapp.DischargeCharge();
            addScreen.SelectedCharge = nCharge;

        },
        afterClosed: function () {

            screen.AllDischargeCharges.refresh().then(function () {
                screen.SelectedDischarge.details.refresh().then(function () {
                    msls.promiseOperation(function (operation) {

                        $.ajax({
                            type: 'post',
                            data: { 'dId': screen.SelectedDischarge.Id },
                            url: '../Web/GetDischargeTotals.ashx',
                            success: operation.code(function AjaxSuccess(AjaxResult) {
                                operation.complete(AjaxResult);
                            })
                        });

                    }).then(function PromiseSuccess(PromiseResult) {

                        var ttlList = PromiseResult;
                        var ttlItems = ttlList.split(',');

                        screen.TotalCharges = ttlItems[0];
                        screen.TotalAmountPaid = ttlItems[1];
                        screen.TotalAmountDue = ttlItems[2];

                    });
                });
            });
        }
    });
};

myapp.DischargePaymentsOld.created = function (screen) {
    
    msls.promiseOperation(function(operation){
    
        $.ajax({
            type: 'post',
            data: { 'dId': screen.SelectedDischarge.Id },
            url: '../Web/GetDischargeTotals.ashx',
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            })
        });
    
    }).then(function PromiseSuccess(PromiseResult) {

        var ttlList = PromiseResult;
        var ttlItems = ttlList.split(',');

        screen.TotalCharges = ttlItems[0];
        screen.TotalAmountPaid = ttlItems[1];
        screen.TotalAmountDue = ttlItems[2];

    });

};


myapp.DischargePaymentsOld.GoToHome_execute = function (screen) {

    myapp.navigateHome();

};