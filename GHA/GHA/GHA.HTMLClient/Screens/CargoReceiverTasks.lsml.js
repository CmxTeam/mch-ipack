﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.CargoReceiverTasks.FlightManifest_Status_StatusImage_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.Carrier3Code_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.FlightGroup_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.RecoveredBy_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.StartDate_postRender = function (element, contentItem) {

    var eDate = contentItem.value;

    dateFormat(eDate, element);

    $(element).css("vertical-align", "middle");

};

myapp.CargoReceiverTasks.EndDate_postRender = function (element, contentItem) {

    var eDate = contentItem.value;

    dateFormat(eDate, element);

    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.TotalULDs_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.TotalAWBs_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.TotalPieces_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.Snapshots_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.PercentOverall_render = function (element, contentItem) {

    $(element).css("vertical-align", "middle");

    var UList = contentItem.parent;
    var UItem = UList.value;

    percentProgress(contentItem.value, "tProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var UList = contentItem.parent;
        var UItem = UList.value;

        percentProgress(contentItem.value, "tProgressBar", element);

    });

};

myapp.CargoReceiverTasks.ETA_postRender = function (element, contentItem) {   

    var eDate = contentItem.value;

    dateFormat(eDate, element);

    $(element).css("vertical-align", "middle");
    
};

myapp.CargoReceiverTasks.SnapshotImage_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
    $(element).html("<img src='content/images/snapshot_logo.png' />");
};

myapp.CargoReceiverTasks.IsOSD_postRender = function (element, contentItem) {
    $(element).css("vertical-align", "middle");
};

myapp.CargoReceiverTasks.ShowSnapshots_execute = function (screen) {   
    alert("Snapshot");
};

myapp.CargoReceiverTasks.GoToHome_execute = function (screen) {
    myapp.navigateHome();
};