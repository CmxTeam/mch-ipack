﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.ViewOnhand.GoToHome_execute = function (screen) {
   
    myapp.navigateHome();

};
myapp.ViewOnhand.EditOnhand_execute = function (screen) {
    
    var nOnhand = screen.SelectedOnhand;

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }

    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    var cName = screen.CurrentUserName;

    myapp.showEditOnhand(nOnhand, cTask, cUser, cStation, cWarehouse, cName, aTimeout);

};
myapp.ViewOnhand.created = function (screen) {
    
    var oHand = screen.SelectedOnhand;

    $.ajax({
        type: 'GET',
        data: { 'onhandId': oHand.OnhandId },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetOnhandDetails',
        success: function (data) {            
            myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(6).execute().then(function (selectedEntity) {
                screen.OnhandEntityType = selectedEntity.results[0];
            });
        },
        error: function (e) {
            alert("Unable to get Onhand Details." + e);
        }
    });

    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: { 'tTypeId': 7, 'eTypeId': 6, 'eId': oHand.OnhandId },
            url: '../Web/GetTask.ashx',
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                if (result != "0") {
                    myapp.activeDataWorkspace.WFSDBData.Tasks_SingleOrDefault(+result).execute().then(function (selectedTask) {
                        screen.SelectedTask = selectedTask.results[0];
                        $ActiveTask = selectedTask.results[0];
                    });
                }
            }
        }

    }));

};
myapp.ViewOnhand.ViewOnhandPkgFilterContainer_render = function (element, contentItem) {
    $(element).append('<input type="text" id="viewOnhandPkgFilter"></input>');
};
myapp.ViewOnhand.ViewOnhandPkgReset_execute = function (screen) {
    $('input#viewOnhandPkgFilter').val('');
    fillOnhandPkgList(screen, true, 1);
};
myapp.ViewOnhand.ViewOnhandPkgSearch_execute = function (screen) {
    fillOnhandPkgList(screen, true, 1);
};
myapp.ViewOnhand.ViewOnhandList_render = function (element, contentItem) {
    $(element).css('padding-left', '0');
    var ul = $('<ul id="onhandPkgList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.ViewOnhand.listLoadPageNumber = 2;
    myapp.ViewOnhand.isComplete = false;
    $(element).on('scroll', function () {
        if ($('ul#onhandPkgList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            fillOnhandPkgList(contentItem.screen, false, myapp.ViewOnhand.listLoadPageNumber);
            myapp.ViewOnhand.listLoadPageNumber = myapp.ViewOnhand.listLoadPageNumber + 1;
        }
    });

    fillOnhandPkgList(contentItem.screen, true, 1);
};

function fillOnhandPkgList(screen, clearList, pageNumber) {

    var oHand = screen.SelectedOnhand;

    $.ajax({
        type: 'GET',
        data: { 'OnhandId': oHand.OnhandId, 'userId': screen.CurrentUser, 'rowPerPage': 10, 'pageNumber': pageNumber, 'filter': $('input#viewOnhandPkgFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetOnhandPackages',
        success: function (data) {
            $('span#onhandPkgNoItems').remove();
            var list = $('ul#onhandPkgList').last();
            if (clearList) {
                list.empty();
                myapp.ViewOnhand.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:74px"></li>');
                var that = this;
                
                var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                if (this.IsHazmat == true) {
                    var hazmatImage = $('<img class="float-left" style="width: 30px;" src="content/images/hazmat_icon.png"></img>');
                    statusContainer.append(hazmatImage);
                }
                li.append(statusContainer);


                var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                var refNumber = $('<div style="font-size:30px">' + this.PackageNumber + ' ' + this.PackageType + '</div>');
                var refNumber1 = $('<div style="font-size:18px"></div>');
                var pDim = $('<span>' + this.Length + ' X ' + this.Width + ' X ' + this.Height + ' ' + this.DimUOM + '</span>')
                var pWeight = $('<span>' + this.Weight + ' ' + this.WeightUOM + '</span>');
                var infoHolder = $('<div style="font-size:18px"></div>');

                var imageContainer = $('<div class="custom-image-container"></div>');

                var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px;"></div>');
                var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
                snapshotButton.append(snapshotImage);
                snapshotButton.on('click', function (e) {
                    e.stopPropagation();
                    if (!screen.PackageList) {
                        screen.PackageList = {};
                    }
                    screen.PackageList.selectedItem = that;
                    goToSnapshot(screen);                    
                });
                
                imageContainer.append(snapshotButton);


                refNumber1.append(pDim);
                infoHolder.append(pWeight);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(infoHolder);

                li.append(imageContainer);
                li.append(refContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="onhandPkgNoItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get data from GetPackageList.' + e);
        }
    })
};

function goToSnapshot(screen) {

    var tOnhand = screen.SelectedOnhand;
    var tPackage = screen.PackageList.selectedItem;
    var uDescULD = tOnhand.OnhandNumber + ' - ' + tPackage.PackageNumber;
    var cTask = tOnhand.TaskId;

    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    myapp.showSnapshot(screen.OnhandEntityType, cTask, screen.CurrentUser, screen.CurrentUserWarehouse, tOnhand.OnhandId, uDescULD, aTimeout);


};
myapp.ViewOnhand.ScreenContent_render = function (element, contentItem) {
    $(element).append('<input type="text" id="viewOnhandHistorySearch"></input>');
};
myapp.ViewOnhand.ViewOnhnadHistorySearch_execute = function (screen) {
    createViewOnhandHistory(screen.SelectedOnhand.OnhandId, 1, true);
};
myapp.ViewOnhand.VirwOnhandHistoryReset_execute = function (screen) {
    $('input#viewOnhandHistorySearch').val('');
    createViewOnhandHistory(screen.SelectedOnhand.OnhandId, 1, true);
};
myapp.ViewOnhand.ViewOnhandHistoryContainer_render = function (element, contentItem) {
    $(element).css('padding-left', '0');
    var table = $('<table id="viewOnhandHistory" class="msls-table ui-responsive table-stripe msls-hstretch ui-table ui-table-reflow">' +
                    '<thead>' +
                        '<tr>' +
                            '<td style="width:50px; font-weight:bold">Date</td>' +
                            '<td style="width:125px; font-weight:bold">Reference</td>' +
                            '<td style="width:125px; font-weight:bold">Description</td>' +
                            '<td style="width:100px; font-weight:bold">User</td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody id="viewOnhandHistoryBody"></tbody>' +
                   '</table>');
    $(element).append(table);
    myapp.ViewOnhand.historyLoadPageNumber = 2;
    $(element).on('scroll', function () {
        if ($('table#viewOnhandHistory').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            createViewOnhandHistory(contentItem.screen.SelectedOnhand.OnhandId, myapp.ViewOnhand.historyLoadPageNumber, false);
            myapp.ViewOnhand.historyLoadPageNumber = myapp.ViewOnhand.historyLoadPageNumber + 1;
        }
    });

    createViewOnhandHistory(contentItem.screen.SelectedOnhand.OnhandId, 1, true);
};

function createViewOnhandHistory(onhandId, pageNumber, clearList) {
    $.ajax({
        type: 'get',
        data: { 'onhandId': onhandId, 'filter': $('#viewOnhandHistorySearch').last().val(), 'rowsPerPage': 20, 'pageNumber': pageNumber },
        url: '../api/cargoreceiver/GetOnhandHistory',
        success: function (data) {
            var tbody = $('tbody#viewOnhandHistoryBody').last();

            if (clearList) {
                tbody.empty();
                myapp.ViewOnhand.historyLoadPageNumber = 2;
            }

            $(data).each(function () {
                tbody.append('<tr class="msls-tr msls-style ui-shadow ui-tr msls-presenter msls-ctl-table-row-layout msls-vauto msls-hstretch msls-presenter-content msls-font-style-normal msls-hscroll msls-table-row">' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Date + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Reference + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Description + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.UserName + '</td>' +
                             '</tr>');
            });
        },
        error: function (e) {
            console.log('Error History: ' + e);
        }
    });
}
myapp.ViewOnhand.ViewOnhadRefFilter_render = function (element, contentItem) {
    $(element).append('<input type="text" id="viewOnhandRefFilter"></input>');

};
myapp.ViewOnhand.ResetViewRefOnhand_execute = function (screen) {
    $('input#viewOnhandRefFilter').val('');
    fillViewOnhandReferenceList(screen, true, 1);
};
myapp.ViewOnhand.SearchViewRefOnhand_execute = function (screen) {
    fillViewOnhandReferenceList(screen, true, 1);
};
myapp.ViewOnhand.ViewOnhandRefListContainer_render = function (element, contentItem) {
    $(element).css('padding-left', '0');
    var ul = $('<ul id="viewOnhandReferenceList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.ViewOnhand.listLoadPageNumber = 2;    
    $(element).on('scroll', function () {
        if ($('ul#viewOnhandReferenceList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            fillViewOnhandReferenceList(contentItem.screen, false, myapp.ViewOnhand.listLoadPageNumber);
            myapp.ViewOnhand.listLoadPageNumber = myapp.ViewOnhand.listLoadPageNumber + 1;
        }
    });

    fillViewOnhandReferenceList(contentItem.screen, true, 1);
};

function fillViewOnhandReferenceList(screen, clearList, pageNumber) {

    var oHand = screen.SelectedOnhand;
    var rId = null;

    $.ajax({
        type: 'GET',
        data: { 'ReferenceId': rId, 'EntityTypeId': 6, 'EntityId': oHand.OnhandId, 'RowsPerPage': 10, 'PageNumber': pageNumber, 'Filter': $('input#viewOnhandRefFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetEntityReferences',
        success: function (data) {
            $('span#viewOnhandReferenceNoItems').remove();
            var list = $('ul#viewOnhandReferenceList').last();
            if (clearList) {
                list.empty();
                myapp.ViewOnhand.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:74px"></li>');
                var that = this;
                
                var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                var refNumber = $('<div style="font-size:18px">' + this.ReferenceType + '</div>');
                var refNumber1 = $('<div style="font-size:30px">' + this.Value + '</div>');
                var rUser = $('<span>Entered By: ' + this.UserName + '</span>');
                var infoHolder = $('<div style="font-size:18px"></div>');

                infoHolder.append(rUser);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(infoHolder);
                
                li.append(refContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="referenceNoItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get data from GetEntityReferences.' + e);
        }
    })
};