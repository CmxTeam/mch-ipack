﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $iScrn2;
var $SelectedUld;
var $UldEntityType;

myapp.BreakdownULD.created = function (screen) {
    var nFlt = screen.SelectedFlt;
    if (nFlt)
    {
        nFlt = $FlightManifest;
    }

    screen.ShowComplete = false;

    screen.details.displayName = "Breakdown FLT# " + nFlt.Carrier.CarrierCode + nFlt.FlightNumber;

    myapp.activeDataWorkspace.SnapshotDBData.EntityTypes_SingleOrDefault(4).execute().then(function (selectedEntity2) {

        screen.UldEntityType = selectedEntity2.results[0];
        $UldEntityType = selectedEntity2.results[0];

    });

};

myapp.BreakdownULD.PercentReceived_render = function (element, contentItem) {
    
    var UList = contentItem.parent;
    if (!UList || !UList.value) return;
    var UItem = UList.value;
    var sInd = UItem.ReceivedPieces;
    var eInd = UItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var UList = contentItem.parent;
        var UItem = UList.value;
        var sInd = UItem.ReceivedPieces;
        var eInd = UItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    });

};

myapp.BreakdownULD.BreakdownAWB_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }

    var nUld = screen.BreakdownUlds.selectedItem;
    $SelectedUld = screen.BreakdownUlds.selectedItem;

    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showBreakdownAWB(nUld, nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.BreakdownULD.SelectedFlt_PercentReceived_render = function (element, contentItem) {

    var fList = contentItem.parent;
    var fscrn = fList.value;
    var fItem = fscrn.SelectedFlt;
    var sInd = fItem.ReceivedPieces;
    var eInd = fItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "bruldProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var fList = contentItem.parent;
        var fscrn = fList.value;
        var fItem = fscrn.SelectedFlt;
        var sInd = fItem.RecoveredULDs;
        var sInd = fItem.ReceivedPieces;
        var eInd = fItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "bruldProgressBar", element);

    });

};

myapp.BreakdownULD.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};

myapp.BreakdownULD.Recover_execute = function (screen) {
    
    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showRecoverFlight(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.BreakdownULD.PutAway_execute = function (screen) {
    
    var nFlt = screen.FlightManifest;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showPutAwayPieces(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.BreakdownULD.Reset_execute = function (screen) {
    $('input#uldFilter').last().val('');
    fillBreakdownUldList(screen, true, screen.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
};

myapp.BreakdownULD.Reset1_execute = function (screen) {
    
    screen.HistoryFilter = null;
    screen.SearchHistoryFilter = null;

};

myapp.BreakdownULD.StatusTimestamp_postRender = function (element, contentItem) {
   
    var eDate = contentItem.value;

    dateFormat(eDate, element);

};

myapp.BreakdownULD.SnapshotImage_postRender = function (element, contentItem) {
    
    $(element).html("<div class='am_camera'><img src='content/images/camera_icon_w.png' style='height:24px' /></div>");
    $(element).trigger("create");

};

myapp.BreakdownULD.ShowUldSnapshot_execute = function (screen) {
    
    var tULD = screen.BreakdownUlds.selectedItem;
    $SelectedUld = screen.BreakdownUlds.selectedItem;    
    var uDescULD = tULD.Code + " " + tULD.SerialNumber;
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    myapp.showSnapshot(screen.UldEntityType, cTask, screen.CurrentUser, screen.CurrentUserWarehouse, tULD.Id, uDescULD, aTimeout);

};

myapp.BreakdownULD.ShowNotCheckedIn_execute = function (screen) {
    
    screen.ShowComplete = false;

};

myapp.BreakdownULD.ShowCheckedIn_execute = function (screen) {
    
    screen.ShowComplete = true;

};

myapp.BreakdownULD.ShowNotCheckedIn_postRender = function (element, contentItem) {
   
    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        myapp.BreakdownULD.showComplete = false;
        fillBreakdownUldList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        myapp.BreakdownULD.showComplete = false;
        fillBreakdownUldList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
    });

};

myapp.BreakdownULD.ShowCheckedIn_postRender = function (element, contentItem) {

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        
        myapp.BreakdownULD.showComplete = true;
        fillBreakdownUldList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
        myapp.BreakdownULD.showComplete = true;
        fillBreakdownUldList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
    });

};

myapp.BreakdownULD.RefreshButton_render = function (element, contentItem) {    
    $iScrn2 = contentItem.screen;
    var a = function (s) {
        return function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                s.SelectedFlt.details.refresh();
                s.BreakdownUlds.refresh();
                s.FlightHistory.refresh();
                operation.complete();

            }));
        }        
    }($iScrn2);

    var div = $("<div class='am_refresh' id='myTest'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    div.on('click', a);
    $(element).append(div);

    $(element).trigger("create");

};

myapp.BreakdownULD.PercentReceived1_render = function (element, contentItem) {
    
    var UList = contentItem.parent;

    if (!UList || !UList.value) return;

    var UItem = UList.value;
    var sInd = UItem.ReceivedPieces;
    var eInd = UItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "aProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var UList = contentItem.parent;
        var UItem = UList.value;
        var sInd = UItem.ReceivedPieces;
        var eInd = UItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "aProgressBar", element);

    });

};

myapp.BreakdownULD.ShipmentUnitType_Code1_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.BreakdownULD.SerialNumber1_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.BreakdownULD.SelectedFlt_WarehouseLocation_Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "18px");

};
myapp.BreakdownULD.SelectedFlt_Status_Name_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "18px");

};
myapp.BreakdownULD.Carrier_CarrierCode_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "2px");
    $(element).css("font-size", "20px");
    $(element).css("line-height", "20px");
    $(element).css("text-align", "center");

};
myapp.BreakdownULD.CheckInTab_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
myapp.BreakdownULD.BreakdownUlds_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.BreakdownULD.HistoryTab_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "History" })).parent().on(evtName, function (e) {

        contentItem.screen.FlightHistory.refresh();

    });

};
myapp.BreakdownULD.FlightHistory_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.BreakdownULD.FlightHistoryTemplate_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-text");

};
myapp.BreakdownULD.CarrierCode_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "2px");
    $(element).css("font-size", "20px");
    $(element).css("line-height", "20px");
    $(element).css("text-align", "center");

};
myapp.BreakdownULD.UldNumber_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.BreakdownULD.BreakdownUlds_render = function (element, contentItem) {
    var ul = $('<ul id="breakdownUldsList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.BreakdownULD.listLoadPageNumber = 2;
    myapp.BreakdownULD.showComplete = false;
    $(element).on('scroll', function () {
        if ($('ul#breakdownUldsList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            console.log(myapp.BreakdownULD.listLoadPageNumber);
            fillBreakdownUldList(contentItem.screen, false, contentItem.screen.SelectedFlt.Id, myapp.BreakdownULD.listLoadPageNumber, myapp.BreakdownULD.showComplete);
            myapp.BreakdownULD.listLoadPageNumber = myapp.BreakdownULD.listLoadPageNumber + 1;
        }
    });

    fillBreakdownUldList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
};

function fillBreakdownUldList(screen, clearList, flightId, pageNumber, showComplete, callbackFunc) {
    console.log('here');
    $.ajax({
        type: 'GET',
        data: { 'flightId': flightId, 'rowPerPage': 8, 'pageNumber': pageNumber, 'showComplete': showComplete, 'filter': $('input#uldFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetBreakdownUlds',
        success: function (data) {
            $('span#noItemsUlds').remove();
            var list = $('ul#breakdownUldsList').last();
            if (clearList) {
                list.empty();
                myapp.BreakdownULD.listLoadPageNumber = 2;
            }            
            $(data).each(function () {                
                var li = $('<li class="custom-list-item" style="height:60px"></li>');
                var that = this;
                li.on('click', function () {
                    if (!screen.BreakdownUlds) {
                        screen.BreakdownUlds = {};
                    }

                    screen.BreakdownUlds.selectedItem = that;
                    myapp.BreakdownULD.BreakdownAWB_execute(screen);                    
                });
                
                var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');

                var statusImage = $('<img class="float-left" style="width: 30px;"></img>');
                statusImage.attr('src', this.StatusImagePath);

                var carrierCode = $('<div style="font-size: 20px;font-weight: bold;margin-top: 35px;">' + this.CarrierCode + '</div>');

                statusContainer.append(statusImage);
                statusContainer.append(carrierCode);

                var refContainer = $('<div class="ref-container"></div>');
                var refNumber = $('<div>' + this.Code + this.SerialNumber + '</div>');
                
                var progressBar = $('<div class="custom-progressbar"></div>');
                var progressBarCountHolder = $('<div class="custom-progressbar-count-holder">' + this.ReceivedPieces + ' of ' + this.TotalPieces + '</div>');
                var progressBarIndicator = $('<div class="custom-progressbar-indicator"></div>');
                progressBarIndicator.css('width', this.PercentReceived + '%');
                progressBar.append(progressBarCountHolder);
                progressBar.append(progressBarIndicator);

                refContainer.append(refNumber);                
                refContainer.append(progressBar);

                var imageContainer = $('<div class="custom-image-container"></div>');                

                var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px"></div>');
                var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
                snapshotButton.append(snapshotImage);
                snapshotButton.on('click', function (e) {
                    e.stopPropagation();
                    if (!screen.BreakdownUlds) {
                        screen.BreakdownUlds = {};
                    }                                    
                    screen.BreakdownUlds.selectedItem = that;
                    myapp.BreakdownULD.ShowUldSnapshot_execute(screen);
                });
                
                imageContainer.append(snapshotButton);
                
                li.append(statusContainer);
                li.append(refContainer);
                li.append(imageContainer);
                list.append(li);
            });
            
            console.log($('ul#breakdownUldsList'));

            if (!list.find('li').length) {
                list.before('<span id="noItemsUlds">No Items</span>')
            }

            if (data[0]) {
                $('#bruldProgressBar p').text(data[0].FlightReceivedPiecs + ' of ' + data[0].FlightTotalReceivedPieces);
                $('#bruldProgressBar div').css('width', data[0].FlightPercentReceived + '%');
            }
            if (callbackFunc) {
                if (data[0]) {                    
                    callbackFunc(data[0].FlightReceivedPiecs, data[0].FlightTotalReceivedPieces, data[0].FlightPercentReceived);
                }
            }
        },

        error: function (e) {
            console.log('Unable get date from GetPutAwayList.' + e);
        }
    })
}
myapp.BreakdownULD.Filter_render = function (element, contentItem) {
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="uldFilter" style="margin-left:3px;"></input>');
    elem.append(filter);
};

myapp.BreakdownULD.Search_execute = function (screen) {        
    if ($('#historyFilterTextbox2').val()) {
        screen.SearchHistoryFilter = $('#historyFilterTextbox2').val();
    }
    else {
        screen.SearchHistoryFilter = null;
    }

};
myapp.BreakdownULD.SearchBreakdownUld_postRender = function (element, contentItem) {

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).find('a').off(evtName).on(evtName, function () {
        fillBreakdownUldList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
    });

};
myapp.BreakdownULD.Search_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }    
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.BreakdownULD.Search_execute(contentItem.screen);
    });

};
myapp.BreakdownULD.HistoryFilter_postRender = function (element, contentItem) {
    $(element).find('input').attr('id', 'historyFilterTextbox2');

};