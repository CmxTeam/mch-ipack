﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />
/// <reference path="../Scripts/c1ls-3.0.20132.20.js" />

var $AjaxTimeout = 120000;

if (typeof c1ls === "undefined") {
    window.location.href = "http://bit.ly/c1ls-nuget";
}

myapp.Home.ScreenContent_render = function (element, contentItem) {

    screen.AjaxTimeout = 120000;

    $.getJSON("../Perms/UserPermissions/", function (data) {

        //attach the permissions to the global 'myapp' object 
        //so it is accessible to the client anywhere.
        myapp.permissions = data;
    }).then(function () {

        var ul = "<ul class='msls-tile-list ui-listview' data-role='listview' data-inset='false'>";
        var liClass = "ui-li ui-btn ui-btn-up-a";
        var divClass = "msls-presenter msls-list-child msls-ctl-group-custom-control msls-vauto msls-hstretch msls-compact-padding msls-presenter-content msls-hscroll";
        var border = "border-left-width:1px; border-right-width:1px; border-top-width:1px; border-bottom-width:1px;";
        var width = "width: 254px; height: 100px;";
        var radius = "-moz-border-radius: 10px; -webkit-border-radius: 10px; border-radius: 10px;";

        var items = [];
        var excluded = [
                        "Screen",
                        "AddEditCarrier",
                        "AddEditCondition",
                        "AddEditStatus",
                        "AddEditWarehouseLocation",
                        "BreakdownAWB",
                        "BreakdownULD",
                        "BrowseCarriers",
                        "BrowseConditions",
                        "BrowseStatuses",
                        "BrowseWarehouseLocations",
                        "CheckInAWB",
                        "CheckInAWBHWB",
                        "CheckInHWB",
                        "PutAwayAWB",
                        "PutAwayHWB",
                        "PutAwayPieces",
                        "RecoverFlightULDs",
                        "RecoverLoose",
                        "SelectConditions",
                        "SelectLocation",
                        "SetStageLocation",
                        "ViewFlightManifest",
                        "ViewSnapshot",
                        "Config",
                        "DischargeCheckInOld",
                        "ViewDischargePaymentsOld",
                        "AddEditDriver",
                        "SelectDriver",
                        "DischargePaymentsOld",
                        "AddCarrierName",
                        "SelectCarrier",
                        "SelectDriver",
                        "DischargeShipmentsOld",
                        "AddDischargePaymentsOld",
                        "DeliveryOrderDetailsOld",
                        "AddDischargeChargeOld",
                        "AddSnapshot",
                        "CargoReceiverTasks",
                        "CargoInventoryTasks",
                        "BarcodeParser",
                        "InventoryTask",
                        "Snapshot",
                        "CargoSnapshot_old",
                        "SnapshotImager",
                        "ScanPutAwayPieces",
                        "DischargeOld",
                        "CheckInFlightPieces",
                        "ReceiveNewShipment",
                        "ViewOnhand",
                        "EditOnhand"
        ];

        //------------------------------------------------------------------------------
        //           Menu Item Permissions
        //------------------------------------------------------------------------------


        //Recover Flight Access
        if (!myapp.permissions["LightSwitchApplication:ModifyRecoverFlight"] && 
            !myapp.permissions["LightSwitchApplication:ReadOnlyRecoverFlight"]) {
            excluded.push("RecoverFlight");
        }

        //Check In Flight Access
        if (!myapp.permissions["LightSwitchApplication:ModifyCheckInFlight"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyCheckInFlight"]) {
            excluded.push("CheckInFlight");
        }

        //Cargo Discharge Access
        if (!myapp.permissions["LightSwitchApplication:ModifyCargoDischarge"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyCargoDischarge"]) {
            excluded.push("CargoDischarge");
            
        }

        //Discharge Check In Access
        if (!myapp.permissions["LightSwitchApplication:ModifyDischargeCheckIn"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyDischargeCheckIn"]) {
            excluded.push("DischargeCheckIn");
        }

        //Cargo Inventory Access
        if (!myapp.permissions["LightSwitchApplication:ModifyCargoInventory"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyCargoInventory"]) {
            excluded.push("CargoInventory");
        }

        //Cargo Task Manager Access
        if (!myapp.permissions["LightSwitchApplication:ModifyCargoTaskManager"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyCargoTaskManager"]) {
            excluded.push("TaskManager");
        }

        //Cargo Snapshot Access
        if (!myapp.permissions["LightSwitchApplication:ModifyCargoSnapshot"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyCargoSnapshot"]) {
            excluded.push("CargoSnapshot");
        }

        //Onhand Receipt Access
        if (!myapp.permissions["LightSwitchApplication:ModifyOnhandReceipt"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyOnhandReceipt"]) {
            excluded.push("OnhandReceipt");
        }

        //System Configuration Access
        if (myapp.permissions["LightSwitchApplication:ModifySystemConfig"] ||
            myapp.permissions["LightSwitchApplication:ReadOnlySystemConfig"]) {
            
            contentItem.screen.findContentItem("Config").isVisible = true;
        }
        else
        {
            contentItem.screen.findContentItem("Config").isVisible = false;
        }

        //Customer Track and Trace Access
        if (!myapp.permissions["LightSwitchApplication:ModifyCustomerTrackTrace"] &&
            !myapp.permissions["LightSwitchApplication:ReadOnlyCustomerTrackTrace"]) {

            excluded.push("TrackAndTrace");
        }

        

        //------------------------------------------------------------------------------

        excluded.push(contentItem.screen.details.getModel().name);

        $.each(myapp, function (key, value) {
            if (key.slice(0, 4) === "show") {
                var name = key.substring(4);

                if (excluded.indexOf(name) < 0) {
                    myapp[name].prototype.constructor([], undefined);
                    var model = myapp[name].prototype.details.getModel();
                    var display = model.displayName;
                    var tap = " onclick=myapp." + key + "(" + screen.AjaxTimeout + ")";
                    var li = "<li title='" + display + "' class='" + liClass + "' data-msls='true' style='" + width + border + radius + "'" + tap + ">";
                    var div = "<div class='" + divClass + "' style='font-size:28px; text-align:center;' ><span style='line-height:96px;'>" + display + "</span></div>" + "</li>";
                    items.push(li + div);
                }
            }
        });

        items.sort();
        ul = ul + items.join("\r\n") + "</ul>";
        $(ul).appendTo($(element));
    });

};
myapp.Home.Config_execute = function (screen) {
    
    myapp.showConfig();

};