﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.CheckInFlight.created = function (screen) {
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    screen.CompletedFlights = false;
    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                var uItems = result.split(',');
                if (uItems.length > 2) {

                    screen.CurrentUser = uItems[0];
                    $CurrentUser = uItems[0];
                    var sId = +uItems[1];
                    myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {
                        screen.CurrentUserStation = selectedStation.results[0];
                        $CurrentUserStation = selectedStation.results[0];
                    });
                    var wId = +uItems[2];
                    myapp.activeDataWorkspace.WFSDBData.Warehouses_SingleOrDefault(+wId).execute().then(function (selectedWarehouse) {
                        screen.CurrentUserWarehouse = selectedWarehouse.results[0];
                        $CurrentUserWarehouse = selectedWarehouse.results[0];
                    });

                }

                else {
                    screen.CurrentUser = uItems[0];
                    $CurrentUser = uItems[0];
                }
            }
        }
    }));
};
myapp.CheckInFlight.GoToHome_execute = function (screen) {
    myapp.navigateHome();
};

myapp.CheckInFlight.CheckedIn_postRender = function (element, contentItem) {    
    $(element).on(getSuitableEventName(), function (e) {
        $(this).find('a').first().makeBackgroundRed();

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked In') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });
};
myapp.CheckInFlight.NotCheckedIn_postRender = function (element, contentItem) {
    $(element).find('a').first().makeBackgroundRed();
    $(element).on(getSuitableEventName(), function (e) {
        $(this).find('a').first().makeBackgroundRed();

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked In') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });
};
myapp.CheckInFlight.RefreshButton_render = function (element, contentItem) {
    var elem = $("<div class='am_refresh'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    $(element).append(elem);
    $(element).trigger("create");
};

myapp.CheckInFlight.FlightsList_render = function (element, contentItem) {
    var ul = $('<ul id="flightList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.CheckInFlight.listLoadPageNumber = 2;
    myapp.CheckInFlight.isComplete = false;
    $(element).on('scroll', function () {
        if ($('ul#flightList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            getCheckinFlights(contentItem.screen, false, myapp.CheckInFlight.listLoadPageNumber);
            myapp.CheckInFlight.listLoadPageNumber = myapp.CheckInFlight.listLoadPageNumber + 1;
        }
    });
};

function showDetailsScreen(screen) {

    var cUser = screen.CurrentUser;
    var cStation = screen.CurrentUserStation;
    var cWarehouse = screen.CurrentUserWarehouse;
    var aTimeout = screen.AjaxTimeout;

    myapp.showViewFlightManifest(screen.CheckinFlights.selectedItem, cUser, cStation, cWarehouse, aTimeout, 6);
}

function getCheckinFlights(screen, clearList, pageNumber) {
    var tDate = screen.DateFilter;
    var flightFilterDate = tDate ? ((tDate.getMonth() + 1) + '/' + tDate.getDate() + '/' + tDate.getFullYear()) : '';

    $.ajax({
        type: 'GET',
        data: { 'userId': screen.CurrentUser, 'rowPerPage': 10, 'pageNumber': pageNumber, 'isComplete': myapp.CheckInFlight.isComplete, 'filter': $('input#checkinFilter').last().val(), 'dateFilter': flightFilterDate },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetCheckinFlights',
        success: function (data) {
            $('span#flightNoItems').remove();
            var list = $('ul#flightList').last();
            if (clearList) {
                list.empty();
                myapp.CheckInFlight.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:74px"></li>');
                var that = this;
                li.on('click', function () {
                    if (!screen.CheckinFlights) {
                        screen.CheckinFlights = {};
                    }

                    screen.CheckinFlights.selectedItem = that;
                    showDetailsScreen(screen);
                });

                var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                var carrierImage = $('<img class="float-left" style="width: 30px;"></img>');
                carrierImage.attr('src', this.CarrierLogoPath);
                var statusImage = $('<img class="float-left" style="width:30px;margin-top:15px;"></img>');
                statusImage.attr('src', this.StatusImagePath);
                statusContainer.append(carrierImage);
                statusContainer.append(statusImage);


                var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                var refNumber = $('<div style="font-size:30px">' + this.OrgFlightNumber + '</div>');
                var refNumber1 = $('<div style="font-size:18px">ETA: ' + this.ETA + '<span style="margin-left:25px">ACCT: ' + this.AccountCode + ' </span></div>');
                var infoHolder = $('<div style="font-size:18px"></div>');
                var uldCount = $('<span>ULD:' + this.TotalULDs + '</span>');
                var awbCount = $('<span style="margin-left:20px">AWB: ' + this.TotalAWBs + '</span>');
                var piecesCount = $('<span style="margin-left:20px">PCS: ' + this.TotalPieces + '</span>');
                infoHolder.append(uldCount);
                infoHolder.append(awbCount);
                infoHolder.append(piecesCount);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(infoHolder);

                li.append(statusContainer);
                li.append(refContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="flightNoItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get date from GetPutAwayList.' + e);
        }
    })
}

myapp.CheckInFlight.Filter_render = function (element, contentItem) {
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="checkinFilter" style="margin-left:3px;"></input>');
    elem.append(filter);
};

myapp.CheckInFlight.Search_execute = function (screen) {    
    getCheckinFlights(screen, true, 1);
};

myapp.CheckInFlight.CheckedIn_execute = function (screen) {
    myapp.CheckInFlight.isComplete = true;
    screen.DateFilter = new Date();
    getCheckinFlights(screen, true, 1);
};
myapp.CheckInFlight.NotCheckedIn_execute = function (screen) {
    myapp.CheckInFlight.isComplete = false;
    screen.DateFilter = null;
    getCheckinFlights(screen, true, 1);
};

myapp.CheckInFlight.Reset_execute = function (screen) {
    $('input#checkinFilter').last().val('');
    if (myapp.CheckInFlight.isComplete) {
        screen.DateFilter = new Date();
    } else {
        screen.DateFilter = null;
    }
    getCheckinFlights(screen, true, 1);
};

myapp.CheckInFlight.DateFilter_postRender = function (element, contentItem) {
    contentItem.addChangeListener(null, function () {        
        if (myapp.CheckInFlight.isComplete && !contentItem.value) {            
            contentItem.value = new Date();
        }
    });
};