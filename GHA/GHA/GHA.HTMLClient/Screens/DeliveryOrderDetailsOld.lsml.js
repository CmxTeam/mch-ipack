﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.DeliveryOrderDetailsOld.created = function (screen) {

    var tHWB = screen.SelectedHWB;
    var tAWB = screen.SelectedAWB;

    if(tHWB != null)
    {
        screen.AvailableSlac = tHWB.AvailablePieces;
        screen.SelectedDischargeDetail.TotalPiecesRequested = tHWB.AvailablePieces;
    }
    
    if(tAWB != null)
    {


    }

    myapp.activeDataWorkspace.WFSDBData.ChargeTypes_SingleOrDefault(1).execute().then(function (selectedChargeType) {

        myapp.activeDataWorkspace.WFSDBData.ChargeTypes_SingleOrDefault(2).execute().then(function (selectedChargeType1) {

            screen.ImportFeeChargeType = selectedChargeType.results[0];
            screen.StorageChargeType = selectedChargeType1.results[0];


        });

    });

};

myapp.DeliveryOrderDetailsOld.SelectedDischargeDetail_DeliveryOrderFile_render = function (element, contentItem) {

    createImageUploader(element, contentItem, "max-width: 250px; max-height: 250px");

};

myapp.DeliveryOrderDetailsOld.beforeApplyChanges = function (screen) {
    
    //var tDischarge = screen.SelectedDischarge;
    //var tDischargeDetail = screen.SelectedDischargeDetail;
    //var cNotification = screen.CustomsNotifications.data[0];
    ////tDischargeDetail.AWB = cNotification.AWB;
    //tDischargeDetail.TotalSLACRequested = tDischargeDetail.TotalPiecesRequested;
    //tDischargeDetail.TotalAmountPaid = 0;
    //tDischargeDetail.TotalAmountDue = 190;
    //tDischargeDetail.TotalPiecesReleased = 0;
    //tDischargeDetail.TotalSLACReleased = 0;
    //tDischargeDetail.DisplayText = "HWB# " + tDischargeDetail.HWB.HWBSerialNumber +
    //                               //" AWB# " + tDischargeDetail.AWB.Carrier.Carrier3Code + "-" + tDischargeDetail.AWB.AwbSerialNumber +
    //                               " SLAC: " + tDischargeDetail.TotalPiecesRequested;

    //var charge1 = new myapp.DischargeCharge();

    //charge1.DischargeDetail = tDischargeDetail;
    //charge1.ChargeType = screen.ImportFeeChargeType;
    //charge1.ChargeAmount = 40;
    //charge1.ChargePaid = 0;

    //var charge2 = new myapp.DischargeCharge();

    //charge2.DischargeDetail = tDischargeDetail;
    //charge2.ChargeType = screen.StorageChargeType;
    //charge2.ChargeAmount = 150;
    //charge2.ChargePaid = 0;

    //tDischarge.TotalDeliveryOrders += +1;

    //var currAmount = tDischarge.TotalAmountDue.toString();
    //var ttlAmount = (+currAmount + +190);
    //var ttlAmount1 = +parseFloat(ttlAmount);
    //var ttlAmount2 = +ttlAmount1.toFixed(2);
    //tDischarge.TotalAmountDue = +ttlAmount2;

};

myapp.DeliveryOrderDetailsOld.SaveDeliveryOrder_execute = function (screen) {

    var tDischarge = screen.SelectedDischarge;
    var tDischargeDetail = screen.SelectedDischargeDetail;

    tDischargeDetail.TotalSLACRequested = tDischargeDetail.TotalPiecesRequested;
    tDischargeDetail.TotalAmountPaid = 0;
    tDischargeDetail.TotalPiecesReleased = 0;
    tDischargeDetail.TotalSLACReleased = 0;

    myapp.applyChanges().then(function () {

        msls.promiseOperation(function (operation) {
            var tHWB = null;
            var tAWB = null;

            if (screen.SelectedHWB != null) {
                tHWB = screen.SelectedHWB.Id;
            }

            if (screen.SelectedAWB != null) {
                tAWB = screen.SelectedAWB.Id;
            }

            var tDis = tDischargeDetail.Id;

            $.ajax({
                type: 'post',
                data: { 'aId': tAWB, 'hId': tHWB, 'dId': tDis },
                url: '../Web/GenerateCharges.ashx',
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                })

            });


        }).then(function PromiseSuccess(PromiseResult) {

            var success = PromiseResult;
           
            if (success == "1") {
                myapp.cancelChanges();
            }

        });
    });
};