﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.ReceiveNewShipment.created = function (screen) {

    //screen.findContentItem("AccountCode2").isEnabled = false;
    //screen.findContentItem("AccountLookup").isEnabled = false;
    screen.findContentItem("CarrierName1").isEnabled = false;
    screen.findContentItem("CarrierLookup").isEnabled = false;
    screen.findContentItem("DriverName").isEnabled = false;
    screen.findContentItem("DriverLookup").isEnabled = false;
    screen.findContentItem("IdTypeName1").isEnabled = false;
    screen.findContentItem("IdTypeLookup1").isEnabled = false;
    screen.findContentItem("IdTypeName2").isEnabled = false;
    screen.findContentItem("IdTypeLookup2").isEnabled = false;
    screen.findContentItem("TotalPieces").isEnabled = false;
    screen.findContentItem("ReceivedPieces").isEnabled = false;
    screen.findContentItem("Weight").isEnabled = false;
    screen.findContentItem("MatchingPhoto1").isEnabled = false;
    screen.findContentItem("MatchingPhoto2").isEnabled = false;
    screen.findContentItem("WeightUOM").isEnabled = false;

    myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(+6).execute().then(function (selectedEntity) {

        screen.OnhandEntityType = selectedEntity.results[0];

    });

    screen.MatchingPhoto1 = true;
    screen.MatchingPhoto2 = true;
    screen.IsHazmat = false;

};
myapp.ReceiveNewShipment.BarcodeListener_render = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var bcText = contentItem.value;

    var chars = [];
    var ctr = "0";

    if (bcText == null) {
        bcText = "";
    }

    $(document).off('keypress').on('keypress', function (event) {

        var charCode = event.which || event.keyCode;

        if (charCode == 13) {
            //$ShipmentFilter.blur();
            //$rButton.focus();
        }

        if (ctr == "1" && charCode != 126) {
            chars.push(String.fromCharCode(charCode));
        }

        if (charCode == 126) {
            if (ctr == "0") {
                ctr = "1";
            }
            else {
                ctr = "2";
            }
        }

        if (ctr == "2") {
            bcText = chars.join("");
            iScrn.TruckerPro = bcText;

            chars = [];
            ctr = "0";
            bcText = "";
            event = null;

            $('#accountPopupContent').parent().center();
            iScrn.showPopup("AccountPopup");
        }
    });

};
myapp.ReceiveNewShipment.DriverSignaturePopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'signaturePopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.DriverSignatureHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Complete Onhand";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.DriverSignature_render = function (element, contentItem) {
    
    var dName = contentItem.screen.DriverName;
    //Create the control & attach to the DOM
    var sig = $("<div id='signature' class='signature-presenter'></div>");
    var sName = $('<div id="sName" style="position:relative; text-align:center; padding-bottom:10px;" ><span style="font-size:18px; line-height:22px;"><b>' + dName + '</b></span>');

    sig.appendTo($(element));
    
    setTimeout(function () {
        //Initialize and start capturing
        sig.jSignature();
        sig.append(sName);
        // Listen for changes made via the custom control and update the 
        // content item whenever it changes.  
        sig.bind("change", function (e) {
            var img = sig.jSignature("getData", "image");
            if (img != null) {
                if (contentItem.value != img[1]) {
                    contentItem.value = img[1];
                }
            }
        });
    }, 0);

};
myapp.ReceiveNewShipment.CompleteReceiveNew_execute = function (screen) {

    var missingHeader = "The following fields are required:";
    var missingList = "";


    //Create List of missing required fields
    if (!screen.SelectedAccount) { missingList += "Account \r\n"; }
    if (!screen.SelectedCarrier) { missingList += "Carrier \r\n"; }
    if (!screen.SelectedDriver) { missingList += "Driver \r\n"; }
    if (!screen.SelectedIdType1) { missingList += "ID Type 1 \r\n"; }

    if (!screen.TruckerPro) { missingList += "Trucker Pro \r\n"; }
    if (screen.MatchingPhoto1 == false) {
        if (!screen.SelectedIdType2) { missingList += "ID Type 2 \r\n"; }
    }
    if (!screen.TotalPieces) { missingList += "Total Pieces \r\n"; }
    if (!screen.Weight) { missingList += "Weight \r\n"; }
    if (!screen.ReceivedPieces) { missingList += "Received Pieces \r\n"; }

    if (missingList) {
        alert(missingHeader + '\r\n\n' + missingList);
    }
    else {
        //Both Mactching Photo 1 and Matching Photo 2 Cannot be false to create Onhand
        if (screen.MatchingPhoto1 == false && screen.MatchingPhoto2 == false) {
            alert("Unable to complete onhand.  Must have 1 ID type with matching photo.");
        }
        else {
            //Do not create an Onhand if Onhand Number exists
            if (!screen.OnhandNumber) {

                var aId = screen.SelectedAccount;
                var cId = screen.SelectedCarrier;
                var dId = screen.SelectedDriver;
                var tId1 = screen.SelectedIdType1;
                var tId2_1 = screen.SelectedIdType2;
                var tId2 = null;
                var mP2 = null;

                if (tId2_1 != null) {
                    tId2 = tId2_1.Id;
                }

                if (tId2 != null) {
                    mP2 = screen.MatchingPhoto2;
                }
                var wId = screen.CurrentUserWarehouse;

                var sTruckerPro = $('#truckerProTextbox').val();
                var sTotalPieces = $('#totalPiecesTextbox').val();
                var sTotalWeight = $('#ttlWeightTextbox').val();
                var sReceivedPieces = $('#receivedPiecesTextbox').val();

                msls.showProgress(msls.promiseOperation(function (operation) {
                    $.ajax({
                        type: 'post',
                        data: {
                            'accountId': aId.Id,
                            'truckerPro': sTruckerPro,
                            'carrierId': cId.Id,
                            'driverId': dId.Id,
                            'idType1': tId1.Id,
                            'matchingPhoto1': screen.MatchingPhoto1,
                            'idType2': tId2,
                            'matchingPhoto2': mP2,
                            'totalPieces': sTotalPieces,
                            'totalWeight': sTotalWeight,
                            'uomId': 3,
                            'receivedPieces': sReceivedPieces,
                            'userName': screen.CurrentUser,
                            'warehouseId': wId.Id
                        },
                        url: '../web/SaveNewOnhand.ashx',
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);

                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    }).then(function PromiseSuccess(PromiseResult) {

                        var result = PromiseResult;

                        var rItems = result.split(',');

                        if (rItems[0] == "S") {

                            screen.OnhandId = +rItems[1];
                            screen.OnhandNumber = rItems[2];
                            screen.SelectedTaskId = +rItems[3];
                            screen.findContentItem("AccountLookup").isVisible = false;
                            screen.findContentItem("ResetAll").isVisible = false;

                            $('#signaturePopupContent').parent().center();
                            screen.showPopup("DriverSignaturePopup");

                        }
                        else {
                            alert(rItems[1]);
                        }

                    });
                }));

            }
            else {
                $('#signaturePopupContent').parent().center();
                screen.showPopup("DriverSignaturePopup");
            }
        }
    }
};
myapp.ReceiveNewShipment.Reset_execute = function (screen) {
    
    $("#signature").jSignature("reset");

};

myapp.ReceiveNewShipment.AccountPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'accountPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.ReceiveNewShipment.AccountHeader_postRender = function (element, contentItem) {

    element.textContent = "Select Account";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.ReceiveNewShipment.AccountCancel_execute = function (screen) {

    screen.closePopup("AccountPopup");

};
myapp.ReceiveNewShipment.AccountLookup_execute = function (screen) {

    $('#truckerProTextbox').blur();
    $('#accountPopupContent').parent().center();
    screen.showPopup("AccountPopup");

};

myapp.ReceiveNewShipment.Accounts_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.ReceiveNewShipment.AccountCode_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ReceiveNewShipment.AccountName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ReceiveNewShipment.SetAccount_execute = function (screen) {
   
    screen.findContentItem("CarrierName1").isEnabled = true;
    screen.findContentItem("CarrierLookup").isEnabled = true;

    screen.SelectedAccount = screen.Accounts.selectedItem;
    screen.AccountCode = screen.Accounts.selectedItem.AccountCode;
    //screen.closePopup("AccountPopup");

};
myapp.ReceiveNewShipment.AccountCode2_postRender = function (element, contentItem) {
    
    $(element).find('input').attr("readonly", "readonly");

};
myapp.ReceiveNewShipment.CarrierName1_postRender = function (element, contentItem) {
   
    $(element).find('input').attr("readonly", "readonly");

};
myapp.ReceiveNewShipment.DriverName_postRender = function (element, contentItem) {
    
    $(element).find('input').attr("readonly", "readonly");

};
myapp.ReceiveNewShipment.CarrierPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'carrierPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.CarrierHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Select Carrier";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.CarrierLookup_execute = function (screen) {
    
    if (screen.OnhandNumber)
    {
        screen.findContentItem("CarrierPrevious").isVisible = false;
    }
    $('#carrierPopupContent').parent().center();
    screen.showPopup("CarrierPopup");

};
myapp.ReceiveNewShipment.CarrierName_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ReceiveNewShipment.SetCarrier_execute = function (screen) {
    
    screen.findContentItem("DriverName").isEnabled = true;
    screen.findContentItem("DriverLookup").isEnabled = true;

    if (screen.SelectedCarrier != screen.Carriers.selectedItem) {

        screen.SelectedDriver = null;
        screen.DriverName = null;

        screen.SelectedCarrier = screen.Carriers.selectedItem;
        screen.CarrierName = screen.Carriers.selectedItem.CarrierName;
        //screen.closePopup("CarrierPopup");
    }

};
myapp.ReceiveNewShipment.AccountReset_execute = function (screen) {
   
    screen.SearchAccountFilter = null;
    screen.AccountFilter = null;

};
myapp.ReceiveNewShipment.CarrierReset_execute = function (screen) {
    
    screen.SearchCarrierFilter = null;
    screen.CarrierFilter = null;

};
myapp.ReceiveNewShipment.DriverPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'driverPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.DriverHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Select Driver";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.SetDriver_execute = function (screen) {
    
    screen.findContentItem("IdTypeName1").isEnabled = true;
    screen.findContentItem("IdTypeLookup1").isEnabled = true;

    screen.SelectedDriver = screen.CarrierDrivers.selectedItem;
    screen.DriverName = screen.CarrierDrivers.selectedItem.FirstName + ' ' + screen.CarrierDrivers.selectedItem.LastName;
    //screen.closePopup("DriverPopup");

};
myapp.ReceiveNewShipment.FirstName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ReceiveNewShipment.LastName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ReceiveNewShipment.DriverLookup_execute = function (screen) {
    
    $('#driverPopupContent').parent().center();
    screen.showPopup("DriverPopup");

};
myapp.ReceiveNewShipment.DriverReset_execute = function (screen) {
    
    screen.SearchDriverFilter = null;
    screen.DriverFilter = null;

};
myapp.ReceiveNewShipment.DriverSearch_execute = function (screen) {
    
    screen.SearchDriverFilter = screen.DriverFilter;

};
myapp.ReceiveNewShipment.DriverCancel_execute = function (screen) {
    
    screen.closePopup("DriverPopup");

};
myapp.ReceiveNewShipment.CarrierSearch_execute = function (screen) {
    
    screen.SearchCarrierFilter = screen.CarrierFilter;

};
myapp.ReceiveNewShipment.AccountSearch_execute = function (screen) {
   
    screen.SearchAccountFilter = screen.AccountFilter;

};
myapp.ReceiveNewShipment.CarrierCancel_execute = function (screen) {
    
    screen.closePopup("CarrierPopup");

};
myapp.ReceiveNewShipment.IdType1Popup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'idType1PopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.IdType2Popup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'idType2PopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.IdType1Header_postRender = function (element, contentItem) {
    
    element.textContent = "Select ID Type";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.IdType2Header_postRender = function (element, contentItem) {
    
    element.textContent = "Select ID Type";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.IdType1Search_execute = function (screen) {
    
    screen.SearchIdType1Filter = screen.IdType1Filter;

};
myapp.ReceiveNewShipment.IdType1Reset_execute = function (screen) {

    screen.SearchIdType1Filter = null;
    screen.IdType1Filter = null;

};
myapp.ReceiveNewShipment.IdType2Search_execute = function (screen) {
    
    screen.SearchIdType2Filter = screen.IdType2Filter;

};
myapp.ReceiveNewShipment.IdType2Reset_execute = function (screen) {

    screen.SearchIdType1Filter = null;
    screen.IdType1Filter = null;

};
myapp.ReceiveNewShipment.Name_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ReceiveNewShipment.Name1_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ReceiveNewShipment.SetIdType1_execute = function (screen) {
   
    screen.findContentItem("MatchingPhoto1").isEnabled = true;
    screen.findContentItem("TotalPieces").isEnabled = true;
    screen.findContentItem("ReceivedPieces").isEnabled = true;
    screen.findContentItem("Weight").isEnabled = true;
    screen.findContentItem("WeightUOM").isEnabled = true;

    screen.SelectedIdType1 = screen.CredentialTypes1.selectedItem;
    screen.IdTypeName1 = screen.CredentialTypes1.selectedItem.Name;
    screen.closePopup("IdType1Popup");

    $('#detailGroup').animate({ scrollTop: 60000 }, 'fast');

};
myapp.ReceiveNewShipment.SetIdType2_execute = function (screen) {
    
    screen.findContentItem("MatchingPhoto2").isEnabled = true;

    screen.SelectedIdType2 = screen.CredentialTypes2.selectedItem;
    screen.IdTypeName2 = screen.CredentialTypes2.selectedItem.Name;
    screen.closePopup("IdType2Popup");

};
myapp.ReceiveNewShipment.IdType1Cancel_execute = function (screen) {
    
    screen.closePopup("IdType1Popup");

};
myapp.ReceiveNewShipment.IdType2Cancel_execute = function (screen) {
    
    screen.closePopup("IdType2Popup");

};
myapp.ReceiveNewShipment.IdTypeLookup1_execute = function (screen) {
    
    $('#idType1PopupContent').parent().center();
    screen.showPopup("IdType1Popup");

};
myapp.ReceiveNewShipment.IdTypeLookup2_execute = function (screen) {
   
    $('#idType2PopupContent').parent().center();
    screen.showPopup("IdType2Popup");

};

myapp.ReceiveNewShipment.ValidateTruckerPro_execute = function (screen) {
    
    screen.findContentItem("AccountCode2").isEnabled = true;
    screen.findContentItem("AccountLookup").isEnabled = true;

};
myapp.ReceiveNewShipment.MatchingPhoto1_postRender = function (element, contentItem) {

    $(element).css('width', '105px');
    setTimeout(function () {
        $(element).find('> div').css('width', '103px').css('height', '76px');
        $(element).find('> div a').css('height', '72px');
        $(element).find('> div > span').css('font-size', '38px');
        $(element).find('> div > span:last').css('padding-right', '15px');
        $(element).find('> div > span:first').css('padding-left', '14px');
    }, 200);


    $(contentItem).change(function () {

            var mVal = contentItem.value;

            if (mVal == false) {

                contentItem.screen.findContentItem("IdTypeName2").isEnabled = true;
                contentItem.screen.findContentItem("IdTypeLookup2").isEnabled = true;
            }
            else {

                contentItem.screen.IdTypeName2 = null;
                contentItem.screen.SelectedIdType2 = null;

                contentItem.screen.findContentItem("IdTypeName2").isEnabled = false;
                contentItem.screen.findContentItem("IdTypeLookup2").isEnabled = false;
                contentItem.screen.findContentItem("MatchingPhoto2").isEnabled = false;
            }

    });

};
myapp.ReceiveNewShipment.WeightUOM_postRender = function (element, contentItem) {
    
    element.textContent = "LBS";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");
    $(element).css("overflow", "hidden");

};
myapp.ReceiveNewShipment.SnapshotButton_render = function (element, contentItem) {
    
    var imageContainer = $('<div class="custom-image-container"></div>');

    var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px"></div>');
    var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
    snapshotButton.append(snapshotImage);
    snapshotButton.on('click', function (e) {
        e.stopPropagation();
        
        myapp.ReceiveNewShipment.ShowOnhandSnapshot_execute(contentItem.screen);
    });

    imageContainer.append(snapshotButton);

    $(element).append(imageContainer);

};
myapp.ReceiveNewShipment.OnhandNumber_postRender = function (element, contentItem) {
    
    //element.textContent = "2014040900001";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");
    $(element).css("overflow", "hidden");

};
myapp.ReceiveNewShipment.DriverSignatureLabel_postRender = function (element, contentItem) {
    
    element.textContent = "Driver Signature";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");
    $(element).css("font-weight", "bold");

};
myapp.ReceiveNewShipment.ShowOnhandSnapshot_execute = function (screen) {

    myapp.activeDataWorkspace.WFSDBData.Tasks_SingleOrDefault(+screen.SelectedTaskId).execute().then(function (selectedTaskItem) {

        screen.SelectedTask = selectedTaskItem.results[0];
        $ActiveTask = selectedTaskItem.results[0];

        var cTask = screen.SelectedTask;
        var tOnhand = screen.OnhandId;
        var uDescOnhand = screen.OnhandNumber;
        var aTimeout = screen.AjaxTimeout;

        myapp.showSnapshot(screen.OnhandEntityType, cTask, screen.CurrentUser, screen.CurrentUserWarehouse, tOnhand, uDescOnhand, aTimeout);

    });

};

myapp.ReceiveNewShipment.ResetAll_execute = function (screen) {
    
    $('#confirmResetPopupContent').parent().center();
    screen.showPopup("ConfirmResetPopup");

};
myapp.ReceiveNewShipment.IdTypeName2_postRender = function (element, contentItem) {
    
    $(element).find('input').attr("readonly", "readonly");

};
myapp.ReceiveNewShipment.IdTypeName1_postRender = function (element, contentItem) {
    
    $(element).find('input').attr("readonly", "readonly");

};
myapp.ReceiveNewShipment.AccountNext_execute = function (screen) {
    
    if (screen.SelectedAccount) {
        screen.closePopup("AccountPopup").then(function () {

            $('#carrierPopupContent').parent().center();
            screen.showPopup("CarrierPopup");

        });
    }

};
myapp.ReceiveNewShipment.CarrierPrevious_execute = function (screen) {
    
    screen.closePopup("CarrierPopup").then(function () {

            $('#accountPopupContent').parent().center();
            screen.showPopup("AccountPopup");

        });

};
myapp.ReceiveNewShipment.CarrierNext_execute = function (screen) {

    if (screen.SelectedCarrier) {
        screen.closePopup("CarrierPopup").then(function () {

            $('#driverPopupContent').parent().center();
            screen.showPopup("DriverPopup");

        });
    }

};
myapp.ReceiveNewShipment.DriverPrevious_execute = function (screen) {
    
    screen.closePopup("DriverPopup").then(function () {

        if (screen.OnhandNumber) {
            screen.findContentItem("CarrierPrevious").isVisible = false;
        }

        $('#carrierPopupContent').parent().center();
        screen.showPopup("CarrierPopup");

    });

};
myapp.ReceiveNewShipment.DriverNext_execute = function (screen) {

    if (screen.SelectedDriver) {
        screen.closePopup("DriverPopup").then(function () {

            $('#idType1PopupContent').parent().center();
            screen.showPopup("IdType1Popup");

        });
    }
};
myapp.ReceiveNewShipment.IdType1Previous_execute = function (screen) {
    
    screen.closePopup("IdType1Popup").then(function () {

        $('#driverPopupContent').parent().center();
        screen.showPopup("DriverPopup");

    });

};
myapp.ReceiveNewShipment.Accept_execute = function (screen) {
    
    var aId = screen.SelectedAccount;
    var cId = screen.SelectedCarrier;
    var dId = screen.SelectedDriver;
    var tId1 = screen.SelectedIdType1;
    var tId2_1 = screen.SelectedIdType2;
    var tId2 = null;
    var mP2 = null;

    if (tId2_1 != null) {
        tId2 = tId2_1.Id;
    }

    if (tId2 != null) {
        mP2 = screen.MatchingPhoto2;
    }
    var wId = screen.CurrentUserWarehouse;

    //var img = screen.DriverSignature;

    //var canvas = document.createElement("canvas");
    //canvas.width = img.width;
    //canvas.height = img.height;
    //var ctx = canvas.getContext("2d");
    //ctx.drawImage(img, 0, 0);
    //var dataURL = canvas.toDataURL("image/jpg");
   
    //var signImage = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

    var signImage = $("#signature").jSignature("getData");

    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: {
                'onhandId': screen.OnhandId,
                'accountId': aId.Id,
                'truckerPro': screen.TruckerPro,
                'carrierId': cId.Id,
                'driverId': dId.Id,
                'idType1': tId1.Id,
                'matchingPhoto1': screen.MatchingPhoto1,
                'idType2': tId2,
                'matchingPhoto2': mP2,
                'totalPieces': screen.TotalPieces,
                'totalWeight': screen.Weight,
                'uomId': 3,
                'receivedPieces': screen.ReceivedPieces,
                'userName': screen.CurrentUser,
                'warehouseId': wId.Id,
                'sigimage': signImage
            },
            url: '../web/CompleteSaveNewOnhand.ashx',
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);

            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            var rItems = result.split(',');

            if (rItems[0] == "S") {

                screen.closePopup("DriverSignaturePopup").then(function () {

                    setTimeout(function () {

                        screen.showPopup("StageOrContinuePopup");

                    }, 1);
                    

                });

                //myapp.showOnhandReceipt(screen.AjaxTimeout);

            }
            else {
                alert(rItems[1]);
            }

        });
    }));

};
myapp.ReceiveNewShipment.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.ReceiveNewShipment.ConfirmResetHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Confirm Reset";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.ConfirmResetText_postRender = function (element, contentItem) {
    
    element.textContent = "Do you want to reset all fields?";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.ReceiveNewShipment.ConfirmYes_execute = function (screen) {
    
    screen.closePopup("ConfirmResetPopup");

    if (!screen.OnhandNumber) {
        screen.TruckerPro = null;
        screen.AccountCode = null;
        screen.SelectedAccount = null;
        screen.CarrierName = null;
        screen.SelectedCarrier = null;
        screen.DriverName = null;
        screen.SelectedDriver = null;
        screen.IdTypeName1 = null;
        screen.SelectedIdType1 = null;
        screen.MatchingPhoto1 = true;
        screen.IdTypeName2 = null;
        screen.SelectedIdType2 = null;
        screen.MatchingPhoto2 = true;
        screen.TotalPieces = null;
        screen.ReceivedPieces = null;
        screen.Weight = null;

        screen.findContentItem("CarrierName1").isEnabled = false;
        screen.findContentItem("CarrierLookup").isEnabled = false;
        screen.findContentItem("DriverName").isEnabled = false;
        screen.findContentItem("DriverLookup").isEnabled = false;
        screen.findContentItem("IdTypeName1").isEnabled = false;
        screen.findContentItem("IdTypeLookup1").isEnabled = false;
        screen.findContentItem("IdTypeName2").isEnabled = false;
        screen.findContentItem("IdTypeLookup2").isEnabled = false;
        screen.findContentItem("TotalPieces").isEnabled = false;
        screen.findContentItem("ReceivedPieces").isEnabled = false;
        screen.findContentItem("Weight").isEnabled = false;
        screen.findContentItem("MatchingPhoto1").isEnabled = false;
        screen.findContentItem("MatchingPhoto2").isEnabled = false;
        screen.findContentItem("WeightUOM").isEnabled = false;
    }

};
myapp.ReceiveNewShipment.ConfirmNo_execute = function (screen) {
    
    screen.closePopup("ConfirmResetPopup");

};
myapp.ReceiveNewShipment.ConfirmResetPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'confirmResetPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.Group_postRender = function (element, contentItem) {
    
    $(element).attr('id', 'detailGroup');

};
myapp.ReceiveNewShipment.MatchingPhoto2_postRender = function (element, contentItem) {
    
    $(element).css('width', '105px');
    setTimeout(function () {
        $(element).find('> div').css('width', '103px').css('height', '76px');
        $(element).find('> div a').css('height', '72px');
        $(element).find('> div > span').css('font-size', '38px');
        $(element).find('> div > span:last').css('padding-right', '15px');
        $(element).find('> div > span:first').css('padding-left', '14px');
    }, 200);

};
myapp.ReceiveNewShipment.StageOrContinuePopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'stageOrContinuePopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.StageOrContinueHeader_postRender = function (element, contentItem) {
    
    element.textContent = "Stage Or Continue";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.StageOrContinueText_postRender = function (element, contentItem) {
    
    element.textContent = "Do you want to stage or continue with current onhand?";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.ReceiveNewShipment.SetStageLocationPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'stageOnhandLocationPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.ReceiveNewShipment.SetStageLocationHeader_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Select Onhand Location";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.ReceiveNewShipment.Location_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.ReceiveNewShipment.WarehouseLocationType_LocationType_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.ReceiveNewShipment.LocationSearch_execute = function (screen) {
    
    if ($('#locFilterTextbox').val()) {
        screen.SearchLocationFilter = $('#locFilterTextbox').val();
    }
    else {
        screen.SearchLocationFilter = null;
    }

};
myapp.ReceiveNewShipment.LocationSearch_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.ReceiveNewShipment.LocationSearch_execute(contentItem.screen);
    });

};
myapp.ReceiveNewShipment.LocationFilter_postRender = function (element, contentItem) {
    
    //$LocationFilterInput = $("input", $(element));
    $(element).find('input').attr('id', 'locFilterTextbox');

};
myapp.ReceiveNewShipment.LocationReset_execute = function (screen) {
    
    screen.LocationFilter = null;
    screen.SearchLocationFilter = null;

};
myapp.ReceiveNewShipment.StageOnhand_execute = function (screen) {
    
    screen.closePopup("StageOrContinuePopup").then(function () {

        setTimeout(function () {

            screen.showPopup("SetStageLocationPopup");

        },1);

    });

};
myapp.ReceiveNewShipment.ContinueOnhand_execute = function (screen) {
    
    var oHandId = screen.OnhandId;
    msls.showProgress(msls.promiseOperation(function (operation) {
        myapp.activeDataWorkspace.WFSDBData.OnhandView(oHandId).execute().then(function (selectedOnhand) {

            var oHand = selectedOnhand.results[0];

            var cUser = screen.CurrentUser;
            var cStation = screen.CurrentUserStation;
            var cWarehouse = screen.CurrentUserWarehouse;
            var cUserName = screen.CurrentUserName;
            var aTimeout = screen.AjaxTimeout;

            operation.complete();
            myapp.showViewOnhand(oHand, cUser, cStation, cWarehouse, cUserName, aTimeout);

        });
    }));

};
myapp.ReceiveNewShipment.SetLocation_execute = function (screen) {
    
    var oHandId = screen.OnhandId;
    var cUser = screen.CurrentUser;
    var sLoc = screen.WarehouseLocations.selectedItem;
    var sTask = screen.SelectedTaskId;

    $.ajax({
        type: 'post',
        data: {
                'UserName': cUser,
                'OnhandId': oHandId,
                'LocationId': sLoc.Id,
                'TaskId': sTask
        },
        url: '../api/cargoreceiver/SetOnhandStagingLocation',
        success: function (s) {
            myapp.showOnhandReceipt(screen.AjaxTimeout);
        },
        error: function (e) { alert("Unable to set onhand location.  " + e); }
    });

};
myapp.ReceiveNewShipment.TruckerPro_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'truckerProTextbox');
};
myapp.ReceiveNewShipment.TotalPieces_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'totalPiecesTextbox');

};
myapp.ReceiveNewShipment.ReceivedPieces_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'receivedPiecesTextbox');

};
myapp.ReceiveNewShipment.Weight_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'ttlWeightTextbox');

};