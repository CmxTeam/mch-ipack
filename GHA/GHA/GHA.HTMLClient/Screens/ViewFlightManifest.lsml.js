﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $LocationFilterInput;
var $ActiveTask;
var $FlightManifest;

myapp.ViewFlightManifest.created = function (screen) {

    var sItem = screen.SelectedFlightUnloadPort;
    if (!sItem)
    {
        sItem = $SelectFlightUnloadingPort;
    }

    if (screen.SelectedTaskTypeId == 1) {

        screen.findContentItem("CheckIn").isVisible = false;
        screen.findContentItem("CheckIn1").isVisible = false;
        screen.findContentItem("CheckIn2").isVisible = false;
        screen.findContentItem("CheckIn3").isVisible = false;

        screen.findContentItem("OpenStagedAt").isVisible = false;
        screen.findContentItem("OpenStagedAt1").isVisible = false;
        screen.findContentItem("OpenStagedAt2").isVisible = false;
        screen.findContentItem("OpenStagedAt3").isVisible = false;

        screen.findContentItem("ShowRecoverFlight").isVisible = true;
        screen.findContentItem("Recover").isVisible = true;
        screen.findContentItem("Recover1").isVisible = true;
        screen.findContentItem("Recover2").isVisible = true;

        if (sItem.StatusId == 1 || sItem.StatusId == 3) {
            screen.findContentItem("OpenRecoveredAt").isVisible = false;
            screen.findContentItem("RecoveredAt").isVisible = false;
            screen.findContentItem("RecoveredAt1").isVisible = false;
            screen.findContentItem("RecoveredAt2").isVisible = false;
        }
        else {
            screen.findContentItem("OpenRecoveredAt").isVisible = true;
            screen.findContentItem("RecoveredAt").isVisible = true;
            screen.findContentItem("RecoveredAt1").isVisible = true;
            screen.findContentItem("RecoveredAt2").isVisible = true;
        }
    }
    else
    {
        
        screen.findContentItem("OpenRecoveredAt").isVisible = false;
        screen.findContentItem("RecoveredAt").isVisible = false;
        screen.findContentItem("RecoveredAt1").isVisible = false;
        screen.findContentItem("RecoveredAt2").isVisible = false;

        screen.findContentItem("ShowRecoverFlight").isVisible = false;
        screen.findContentItem("Recover").isVisible = false;
        screen.findContentItem("Recover1").isVisible = false;
        screen.findContentItem("Recover2").isVisible = false;

        screen.findContentItem("CheckIn").isVisible = true;
        screen.findContentItem("CheckIn1").isVisible = true;
        screen.findContentItem("CheckIn2").isVisible = true;
        screen.findContentItem("CheckIn3").isVisible = true;

        if (sItem.StatusId == 4) {
            screen.findContentItem("OpenStagedAt").isVisible = false;
            screen.findContentItem("OpenStagedAt1").isVisible = false;
            screen.findContentItem("OpenStagedAt2").isVisible = false;
            screen.findContentItem("OpenStagedAt3").isVisible = false;
        }
        else {
            screen.findContentItem("OpenStagedAt").isVisible = true;
            screen.findContentItem("OpenStagedAt1").isVisible = true;
            screen.findContentItem("OpenStagedAt2").isVisible = true;
            screen.findContentItem("OpenStagedAt3").isVisible = true;
        }
    }

    screen.OpenedByRecoveredAt = false;

    myapp.activeDataWorkspace.WFSDBData.FlightManifests_SingleOrDefault(+sItem.FlightManifestId).execute().then(function (selectedFlight) {
        screen.FlightManifest = selectedFlight.results[0];
        $FlightManifest = selectedFlight.results[0];
    }).then(function () {

        var sFlt = screen.FlightManifest;
        if (!sFlt)
        {
            sFlt = $FlightManifest;
        }
        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var fStatus = sFlt.Status;
        screen.details.displayName = "FLT# " + sItem.FlightNumber + " Details";

        msls.showProgress(msls.promiseOperation(function (operation) {

            $.ajax({
                type: 'post',
                data: { 'tTypeId': screen.SelectedTaskTypeId, 'eTypeId': 1, 'eId': sFlt.Id },
                url: '../Web/GetTask.ashx',
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });

        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    if (result != "0") {
                        myapp.activeDataWorkspace.WFSDBData.Tasks_SingleOrDefault(+result).execute().then(function (selectedTask) {
                            screen.SelectedTask = selectedTask.results[0];
                            $ActiveTask = selectedTask.results[0];
                        });
                    }
                }
            }

        }));
    });
};

myapp.ViewFlightManifest.Recover_execute = function (screen) {

    var sFlt = screen.FlightManifest;
    if (!sFlt) {
        sFlt = $FlightManifest;
    }
    var fStatus = sFlt.Status;
    screen.LocationFilter = null;
    screen.WarehouseLocations.selectedItem = null;
    screen.SetStageLocationPopupHeader = "Select Recover Location";
    screen.LocationTypeId = 3;

    if (sFlt.WarehouseLocation1 == null && fStatus.Id == +1) {

        
        $('#stageLocationPopupContent').parent().center();

        screen.showPopup("SetStageLocationPopup").then(function () {
            setTimeout(function () {
                $LocationFilterInput.focus();
                $LocationFilterInput.select();
            }, 1);
        });

    }
    else {
        var cUser = screen.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var cStation = screen.CurrentUserStation;
        if (!cStation) {
            cStation = $CurrentUserStation;
        }
        var cWarehouse = screen.CurrentUserWarehouse;
        if (!cWarehouse) {
            cWarehouse = $CurrentUserWarehouse;
        }
        var cTask = screen.SelectedTask;
        if (!cTask) {
            cTask = $ActiveTask;
        }
        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        myapp.showRecoverFlightULDs(sFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

    }

};

myapp.ViewFlightManifest.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};

myapp.ViewFlightManifest.PercentReceived_render = function (element, contentItem) {
    
    var uScrn = contentItem.parent;
    var uItem = uScrn.value;
    var sInd = uItem.ReceivedCount;
    var eInd = uItem.LoadCount;

    progress(contentItem.value, sInd, eInd, "progressBar", element);

    contentItem.addChangeListener("value", function () {

        var uScrn = contentItem.parent;
        var uItem = uScrn.value;
        var sInd = uItem.ReceivedCount;
        var eInd = uItem.LoadCount;

        progress(contentItem.value, sInd, eInd, "progressBar", element);

    });

};

myapp.ViewFlightManifest.Reset_execute = function (screen) {
    
    screen.Filter = null;

};

myapp.ViewFlightManifest.Reset1_execute = function (screen) {
    
    screen.HistoryFilter = null;
    screen.SearchHistoryFilter = null;

};

myapp.ViewFlightManifest.Reset2_execute = function (screen) {
    
    screen.LocationFilter = null;
    screen.SearchLocationFilter = null;

};

myapp.ViewFlightManifest.SetStageLocationPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'stageLocationPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.ViewFlightManifest.LocationFilter_postRender = function (element, contentItem) {

    $LocationFilterInput = $("input", $(element));
    $(element).find('input').attr('id', 'locationFilterTextbox');

};

myapp.ViewFlightManifest.SetStageLocation_execute = function (screen) {

    var nFlt = screen.FlightManifest;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var nLoc = screen.WarehouseLocations.selectedItem;
    screen.closePopup("SetStageLocationPopup").then(function () {
        msls.showProgress(msls.promiseOperation(function (operation) {
            $.ajax({
                type: 'post',
                data: { 'FltId': nFlt.Id, 'LocId': nLoc.Id, 'cUser': cUser, 'tType': screen.SelectedTaskTypeId },
                url: '../Web/SetStagingLocation.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    $('#recoveredLocation div span').text(nLoc.Location);
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });
        }).then(function PromiseSuccess(PromiseResult) {
            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {

                    msls.showProgress(msls.promiseOperation(function (operation) {

                        screen.FlightManifest.details.refresh().then(function () {

                            if (screen.OpenedByRecoveredAt == false) {

                                var cUser = screen.CurrentUser;
                                if (!cUser) {
                                    cUser = $CurrentUser;
                                }
                                var cStation = screen.CurrentUserStation;
                                if (!cStation) {
                                    cStation = $CurrentUserStation;
                                }
                                var cTask = screen.SelectedTask;
                                if (!cTask) {
                                    cTask = $ActiveTask;
                                }
                                var aTimeout = screen.AjaxTimeout;
                                if (!aTimeout) {
                                    aTimeout = $AjaxTimeout;
                                }

                                if (screen.SelectedTaskTypeId == 1) {
                                    screen.findContentItem("OpenRecoveredAt").isVisible = true;
                                    operation.complete();
                                    myapp.showRecoverFlightULDs(nFlt, cUser, cStation, cTask, aTimeout);
                                }
                                else
                                {
                                    screen.findContentItem("OpenStagedAt").isVisible = true;
                                    operation.complete();
                                    myapp.showCheckInFlightPieces();
                                }
                            }
                            else {
                                screen.OpenedByRecoveredAt = false;
                                operation.complete();

                            }

                        });
                    }));
                }
            }
        }));
    });
};

myapp.ViewFlightManifest.SetStageLocationPopupHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    //element.textContent = "Select Recover Location";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.ViewFlightManifest.Reset3_execute = function (screen) {
    
    screen.UldFilter = null;
    screen.SearchUldFilter = null;

};

myapp.ViewFlightManifest.ULDImage_render = function (element, contentItem) {
    $(element).html("<img src='content/images/uldicon.png' style='height:18px; width:18px' />");
    $(element).trigger("create");
};

myapp.ViewFlightManifest.StatusTimestamp1_postRender = function (element, contentItem) {
    
    var eDate = contentItem.value;

    dateFormat(eDate, element);

};

myapp.ViewFlightManifest.SelectedUnloadingPort_FlightManifest_Carrier_CarrierName_postRender = function (element, contentItem) {
   
    $(element).css("vertical-align", "middle");
    $(element).css("padding-top", "3px");

};
myapp.ViewFlightManifest.Reset4_execute = function (screen) {
   
    screen.AwbFilter = null;
    screen.SearchAwbFilter = null;

};

myapp.ViewFlightManifest.Location_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.ViewFlightManifest.WarehouseLocationType_LocationType_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.ViewFlightManifest.Group15_postRender = function (element, contentItem) {
   
    $(element).css("text-align", "center");

};
myapp.ViewFlightManifest.ULDListTab_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "ULDs" })).parent().on(evtName, function (e) {

        contentItem.screen.ReceiverUldList.refresh();

    });

};
myapp.ViewFlightManifest.FlightULDs_postRender = function (element, contentItem) {
   
    $(element).css("overflow", "auto");

};
myapp.ViewFlightManifest.AWBListTab_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "AWBs" })).parent().on(evtName, function (e) {

        contentItem.screen.ReceiverAwbList.refresh();

    });

};
myapp.ViewFlightManifest.FlightAWBs_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};
myapp.ViewFlightManifest.HistoryTab_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "History" })).parent().on(evtName, function (e) {

        contentItem.screen.FlightHistory.refresh();

    });

};
myapp.ViewFlightManifest.FlightHistory_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "auto");

};

myapp.ViewFlightManifest.WarehouseLocations_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_CarrierName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "30px");
    $(element).css("line-height", "32px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_FlightNumber_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_ArrivalTime_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_Origin_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_TotalAWBs_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_TotalULDs_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_TotalPieces_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_Location_postRender = function (element, contentItem) {
    $(element).attr('id', 'recoveredLocation');
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_RecoveredBy_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_RecoveredOn_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");   

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_RecoveredULDs_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_ReceivedPieces_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.SelectedFlightUnloadPort_StatusName_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.ViewFlightManifest.UldNumber_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "28px");

};
myapp.ViewFlightManifest.AwbNumber_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "40px");

};
myapp.ViewFlightManifest.FlightHistoryTemplate_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-text");

};
myapp.ViewFlightManifest.Search_execute = function (screen) {

    if ($('#uldFilterTextbox').val()) {
        screen.SearchUldFilter = $('#uldFilterTextbox').val();
    }
    else {
        screen.SearchUldFilter = null;
    }

};
myapp.ViewFlightManifest.Search1_execute = function (screen) {

    if ($('#awbFilterTextbox').val()) {
        screen.SearchAwbFilter = $('#awbFilterTextbox').val();
    }
    else {
        screen.SearchAwbFilter = null;
    }

};
myapp.ViewFlightManifest.Search2_execute = function (screen) {
    
    if ($('#historyFilterTextbox').val()) {
        screen.SearchHistoryFilter = $('#historyFilterTextbox').val();
    }
    else {
        screen.SearchHistoryFilter = null;
    }

};
myapp.ViewFlightManifest.Search3_execute = function (screen) {

    if ($('#locationFilterTextbox').val()) {
        screen.SearchLocationFilter = $('#locationFilterTextbox').val();
    }
    else {
        screen.SearchLocationFilter = null;
    }

};
myapp.ViewFlightManifest.UldFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'uldFilterTextbox');

};
myapp.ViewFlightManifest.Search_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.ViewFlightManifest.Search_execute(contentItem.screen);
    });

};
myapp.ViewFlightManifest.AwbFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'awbFilterTextbox');

};
myapp.ViewFlightManifest.Search1_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.ViewFlightManifest.Search1_execute(contentItem.screen);
    });

};
myapp.ViewFlightManifest.HistoryFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'historyFilterTextbox');

};
myapp.ViewFlightManifest.Search2_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.ViewFlightManifest.Search2_execute(contentItem.screen);
    });

};
myapp.ViewFlightManifest.Search3_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.ViewFlightManifest.Search3_execute(contentItem.screen);
    });

};
myapp.ViewFlightManifest.Details_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "Details" })).parent().on(evtName, function (e) {
        alert("Details");
        contentItem.screen.ReceiverFlightView.details.refresh();

    });

};
myapp.ViewFlightManifest.RecoveredAt_execute = function (screen) {
    
    screen.OpenedByRecoveredAt = true;
    screen.SetStageLocationPopupHeader = "Select Recover Location";
    screen.LocationTypeId = 3;

    $('#stageLocationPopupContent').parent().center();

    screen.showPopup("SetStageLocationPopup").then(function () {
        setTimeout(function () {
            $LocationFilterInput.focus();
            $LocationFilterInput.select();
        }, 1);
    });

};
myapp.ViewFlightManifest.CheckIn_execute = function (screen) {
    
    var sFlt = screen.FlightManifest;
    if (!sFlt) {
        sFlt = $FlightManifest;
    }
    var fStatus = sFlt.Status;
    screen.LocationFilter = null;
    screen.WarehouseLocations.selectedItem = null;
    screen.LocationTypeId = null;
    screen.SetStageLocationPopupHeader = "Select Staging Location";

    if (sFlt.WarehouseLocation == null && fStatus.Id == +4) {


        $('#stageLocationPopupContent').parent().center();

        screen.showPopup("SetStageLocationPopup").then(function () {
            setTimeout(function () {
                $LocationFilterInput.focus();
                $LocationFilterInput.select();
            }, 1);
        });

    }
    else {
        var cUser = screen.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }

        var cFlt = screen.FlightManifest;

        var cStation = screen.CurrentUserStation;
        if (!cStation) {
            cStation = $CurrentUserStation;
        }
        var cWarehouse = screen.CurrentUserWarehouse;
        if (!cWarehouse) {
            cWarehouse = $CurrentUserWarehouse;
        }
        var cTask = screen.SelectedTask;
        if (!cTask) {
            cTask = $ActiveTask;
        }
        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        myapp.showCheckInFlightPieces(cTask, cFlt, cUser, cStation, cWarehouse, aTimeout);

    }

};
myapp.ViewFlightManifest.OpenStagedAt_execute = function (screen) {
    
    screen.OpenedByRecoveredAt = true;
    screen.SetStageLocationPopupHeader = "Select Staging Location";
    screen.LocationTypeId = null;
    $('#stageLocationPopupContent').parent().center();

    screen.showPopup("SetStageLocationPopup").then(function () {
        setTimeout(function () {
            $LocationFilterInput.focus();
            $LocationFilterInput.select();
        }, 1);
    });

};