﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />
var $CheckInHWBQuantityInput;
var $iScrn4;
var $HwbEntityType;
var $SelectedHwb;

myapp.CheckInAWBHWB.created = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var nUld = screen.SelectedUld;
    if (!nUld) {
        nUld = $SelectedUld;
    }

    screen.HwbHyphen = "-";

    screen.details.displayName = "Check In FLT# " + nFlt.Carrier.CarrierCode + nFlt.FlightNumber +
                                 " ULD# " + nUld.Code + " " + nUld.SerialNumber +
                                 " AWB# " + screen.SelectedMawb.AwbSerialNumber;

    myapp.activeDataWorkspace.SnapshotDBData.EntityTypes_SingleOrDefault(3).execute().then(function (selectedEntity) {

        screen.HwbEntityType = selectedEntity.results[0];
        $HwbEntityType = selectedEntity.results[0];
    });


};

myapp.CheckInAWBHWB.Reset_execute = function (screen) {
    $('input#hwbFilter').last().val('');
    fillBreakdownHwbList(screen, true, screen.SelectedFlt.Id, screen.SelectedUld.Id, screen.SelectedMawb.AwbManifestId, 1);
};

myapp.CheckInAWBHWB.CheckInHWB_execute = function (screen) {

    $SelectedHwb = screen.BreakdownHwbs.selectedItem;
    //var sMft = screen.BreakdownHwbs.selectedItem;
    //var sHWB = sMft.HWB;

    var sMft = screen.SelectedMawb;
    if (!sMft) {
        sMft = $SelectedAwb;
    }

    screen.CheckInQuantity = +sMft.LoadCount - +sMft.ReceivedCount;
    //console.log(screen.CheckInQuantity);
    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("CheckInHWBPopup").then(function () {
        setTimeout(function () {
            $CheckInHWBQuantityInput.focus();
            $CheckInHWBQuantityInput.select();

        }, 100);
    });
};

myapp.CheckInAWBHWB.GoToHome_execute = function (screen) {

    myapp.navigateHome();

};

myapp.CheckInAWBHWB.HWB_PercentReceived_render = function (element, contentItem) {    
    var UList = contentItem.parent;
    var UItem = UList.value;
    var aHWB = UItem.HWB;
    var sInd = UItem.ReceivedCount;
    var eInd = UItem.LoadCount;

    progress(contentItem.value, sInd, eInd, "progressBar", element);

    contentItem.addChangeListener("value", function () {

        var UList = contentItem.parent;
        var UItem = UList.value;
        var aHWB = UItem.HWB;
        var sInd = UItem.ReceivedCount;
        var eInd = UItem.LoadCount;

        progress(contentItem.value, sInd, eInd, "progressBar", element);

    });

};

myapp.CheckInAWBHWB.SelectedManifestAWBDetail_PercentReceived_render = function (element, contentItem) {    
    setTimeout(function () {
        $(element).parent().removeClass('ui-disabled');
    }, 100);
    var fScrn = contentItem.screen;
    var fItem = fScrn.SelectedMawb;
    var sInd = fItem.ReceivedCount;
    var eInd = fItem.LoadCount;

    progress(contentItem.value, sInd, eInd, "brhwbProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var fScrn = contentItem.screen;
        var fItem = fScrn.SelectedMawb;
        var sInd = fItem.ReceivedCount;
        var eInd = fItem.LoadCount;

        progress(contentItem.value, sInd, eInd, "brhwbProgressBar", element);

    });

};

myapp.CheckInAWBHWB.PercentReceived_render = function (element, contentItem) {

    var UList = contentItem.parent;
    var UItem = UList.value;
    var sInd = UItem.ReceivedCount;
    var eInd = UItem.LoadCount;

    progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var UList = contentItem.parent;
        var UItem = UList.value;
        var sInd = UItem.ReceivedCount;
        var eInd = UItem.LoadCount;

        progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    });

};

myapp.CheckInAWBHWB.PutAwayPieces_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showPutAwayPieces(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.CheckInAWBHWB.Reset1_execute = function (screen) {

    screen.HistoryFilter = null;
    screen.SearchHistoryFilter = null;

};

myapp.CheckInAWBHWB.CheckInQuantity_postRender = function (element, contentItem) {

    $CheckInHWBQuantityInput = $("input", $(element));
    var $iControl = $(element).find("input");
    $iControl.attr("type", "text");
    $iControl.attr("readonly", "readonly");
    $iControl.addClass("inlineTarget2");

};

myapp.CheckInAWBHWB.Breakdown_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showBreakdownULD(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.CheckInAWBHWB.Recover_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showRecoverFlight(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.CheckInAWBHWB.CheckInHWBPopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Piece Check-In Quantity";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.CheckInAWBHWB.StatusTimestamp1_postRender = function (element, contentItem) {

    var eDate = contentItem.value;

    dateFormat(eDate, element);

};

myapp.CheckInAWBHWB.SnapshotImage_postRender = function (element, contentItem) {

    $(element).html("<div class='am_camera'><img src='content/images/camera_icon_w.png' style='height:22px' /></div>");
    $(element).trigger("create");

};

myapp.CheckInAWBHWB.ShowHwbSnapshot_execute = function (screen) {

    var tMft = screen.BreakdownHwbs.selectedItem;
    var uDescHWB = tMft.HwbSerialNumber;
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var hType = screen.HwbEntityType;
    if (!hType) {
        hType = $HwbEntityType;
    }
    myapp.showSnapshot(hType, cTask, cUser, cWarehouse, tMft.HwbId, uDescHWB, aTimeout);

};

myapp.CheckInAWBHWB.ShowNotCheckedIn_execute = function (screen) {

    screen.ShowComplete = false;

};

myapp.CheckInAWBHWB.ShowCheckedIn_execute = function (screen) {

    screen.ShowComplete = true;

};

myapp.CheckInAWBHWB.ShowNotCheckedIn_postRender = function (element, contentItem) {

    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};

myapp.CheckInAWBHWB.ShowCheckedIn_postRender = function (element, contentItem) {

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked-In') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};

myapp.CheckInAWBHWB.RefreshButton_render = function (element, contentItem) {

    $iScrn4 = contentItem.screen;

    $(element).html("<div class='am_refresh' onclick='refreshView4()' ><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    $(element).trigger("create");

};

function refreshView4() {
    fillBreakdownHwbList($iScrn4, true, $iScrn4.SelectedFlt.Id, $iScrn4.SelectedUld.Id, $iScrn4.SelectedMawb.AwbManifestId, 1);
};
myapp.CheckInAWBHWB.PercentReceived1_render = function (element, contentItem) {

    var UList = contentItem.parent;
    var UItem = UList.value;
    var sInd = UItem.ReceivedCount;
    var eInd = UItem.LoadCount;

    progress(contentItem.value, sInd, eInd, "cProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var UList = contentItem.parent;
        var UItem = UList.value;
        var sInd = UItem.ReceivedCount;
        var eInd = UItem.LoadCount;

        progress(contentItem.value, sInd, eInd, "cProgressBar", element);

    });

};
myapp.CheckInAWBHWB.HWB_HWBSerialNumber1_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "14px");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.CheckInAWBHWB.HWB_HwbDestination_IATACode1_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "14px");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.CheckInAWBHWB.HwbHyphen_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "14px");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};
myapp.CheckInAWBHWB.SelectedFLT_WarehouseLocation_Location_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "18px");

};
myapp.CheckInAWBHWB.SelectedFLT_Status_Name_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "18px");

};
myapp.CheckInAWBHWB.HWB_Status_StatusImage1_postRender = function (element, contentItem) {

    $(element).css("padding-top", "14px");

};
myapp.CheckInAWBHWB.CheckIn_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var nMHWB = iScrn.BreakdownHwbs.selectedItem;
        if (!nMHWB) {
            nMHWB = $SelectedHwb;
        }
        var nMAWB = iScrn.SelectedMawb;
        if (!nMAWB) {
            nMAWB = $SelectedAwb;
        }
        var nFlt = iScrn.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = iScrn.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var cQty = $(this).parent().parent().find('input').val();
        iScrn.closePopup("CheckInHWBPopup").then(function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                $.ajax({
                    type: 'post',
                    data: { 'FltId': nFlt.Id, 'MawbId': nMAWB.AwbManifestId, 'MhwbId': nMHWB.ManifestDetailId, 'cQty': cQty, 'cUser': cUser },
                    url: '../Web/CheckInHWBPieces.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        fillBreakdownHwbList(iScrn, true, iScrn.SelectedFlt.Id, iScrn.SelectedUld.Id, iScrn.SelectedMawb.AwbManifestId, 1, function (rc, lc, p) {
                            $('#brhwbProgressBar p').text(rc + ' of ' + lc);
                            $('#brhwbProgressBar div').css('width', p + '%');

                            iScrn.SelectedMawb.LoadCount = lc;
                            iScrn.SelectedMawb.ReceivedCount = rc;
                            fillBreakdownAwbList(iScrn, true, iScrn.SelectedFlt.Id, iScrn.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
                            fillBreakdownUldList(iScrn, true, iScrn.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
                        });
                        iScrn.SelectedFlt.details.refresh();
                    }
                }
            }));
        }, 1000);
    });
};
myapp.CheckInAWBHWB.UnCheckIn_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var nMHWB = iScrn.BreakdownHwbs.selectedItem;
        if (!nMHWB) {
            nMHWB = $SelectedHwb;
        }
        var nMAWB = iScrn.SelectedMawb;
        if (!nMAWB) {
            nMAWB = $SelectedAwb;
        }
        var nFlt = iScrn.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = iScrn.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var cQty = $(this).parent().parent().find('input').val();

        iScrn.closePopup("CheckInHWBPopup").then(function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                $.ajax({
                    type: 'post',
                    data: { 'FltId': nFlt.Id, 'MawbId': nMAWB.AwbManifestId, 'MhwbId': nMHWB.ManifestDetailId, 'cQty': cQty, 'cUser': cUser },
                    url: '../Web/UnCheckInHWBPieces.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;
                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        fillBreakdownHwbList(iScrn, true, iScrn.SelectedFlt.Id, iScrn.SelectedUld.Id, iScrn.SelectedMawb.AwbManifestId, 1, function (rc, lc, p) {
                            $('#brhwbProgressBar p').text(rc + ' of ' + lc);
                            $('#brhwbProgressBar div').css('width', p + '%');
                            iScrn.SelectedMawb.LoadCount = lc;
                            iScrn.SelectedMawb.ReceivedCount = rc;
                            fillBreakdownAwbList(iScrn, true, iScrn.SelectedFlt.Id, iScrn.SelectedUld.Id, 1, myapp.BreakdownAWB.showComplete);
                            fillBreakdownUldList(iScrn, true, iScrn.SelectedFlt.Id, 1, myapp.BreakdownULD.showComplete);
                        });

                        iScrn.SelectedFlt.details.refresh();
                    }
                }
            }));
        }, 1000);
    });
};
myapp.CheckInAWBHWB.CheckInHWBPopup_postRender = function (element, contentItem) {

    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.CheckInAWBHWB.HWBListTab_postRender = function (element, contentItem) {

    $(element).css("overflow", "hidden");

};
myapp.CheckInAWBHWB.BreakdownHwbs_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};
myapp.CheckInAWBHWB.HistoryTab_postRender = function (element, contentItem) {

    $(element).css("overflow", "hidden");

    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "History" })).parent().on(evtName, function (e) {

        contentItem.screen.FlightHistory.refresh();

    });

};
myapp.CheckInAWBHWB.FlightHistory_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};
myapp.CheckInAWBHWB.FlightHistoryTemplate_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-text");

};
myapp.CheckInAWBHWB.PopupKeypad_render = function (element, contentItem) {

    $(element).attr("id", "inlineTargetKeypad2");
    $(element).addClass("keypad-inline");

    $('div#inlineTargetKeypad2').keypad({
        target: $('.inlineTarget2:visible'),
        layout: ['123', '456', '789', $.keypad.SPACE + '0' + $.keypad.BACK]
    });

    var keypadTarget = null;
    $('input.inlineTarget2').focus(function () {
        if (keypadTarget != this) {
            keypadTarget = this;
            $('div#inlineTargetKeypad2').keypad('option', { target: this });
        }
    });

};
myapp.CheckInAWBHWB.BreakdownHwbs_render = function (element, contentItem) {
    var ul = $('<ul id="breakdownHwbsList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.CheckInAWBHWB.listLoadPageNumber = 2;
    $(element).on('scroll', function () {
        if ($('ul#breakdownHwbsList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            //console.log(myapp.CheckInAWBHWB.listLoadPageNumber);
            fillBreakdownHwbList(contentItem.screen, false, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, contentItem.screen.SelectedMawb.AwbManifestId, myapp.CheckInAWBHWB.listLoadPageNumber);
            myapp.CheckInAWBHWB.listLoadPageNumber = myapp.CheckInAWBHWB.listLoadPageNumber + 1;
        }
    });

    fillBreakdownHwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, contentItem.screen.SelectedMawb.AwbManifestId, 1);
};

function fillBreakdownHwbList(screen, clearList, flightId, uldId, awbId, pageNumber, callbackFunc) {
    $.ajax({
        type: 'GET',
        data: {
            'flightId': flightId, 'uldId': uldId, 'awbId': awbId, 'rowPerPage': 8, 'pageNumber': pageNumber, 'filter': $('input#hwbFilter').last().val()
        },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetBreakdownHwbs',
        success: function (data) {
            $('span#noItemsHwbs').remove();
            var list = $('ul#breakdownHwbsList').last();
            if (clearList) {
                list.empty();
                myapp.CheckInAWBHWB.listLoadPageNumber = 2;
            }
            $(data).each(function (i) {
                if (i > 0) {
                    var li = $('<li class="custom-list-item" style="height:60px"></li>');
                    var that = this;
                    li.on('click', function () {
                        if (!screen.BreakdownHwbs) {
                            screen.BreakdownHwbs = {};
                        }
                        screen.BreakdownHwbs.selectedItem = that;
                        myapp.CheckInAWBHWB.CheckInHWB_execute(screen);
                    });

                    var statusContainer = $('<div style="float: left;height: 60px;width: 40px;"></div>');
                    console.log(this.IsSplitFlight);
                    if (this.IsSplitFlight) {
                        var splitFlightImage = $('<img class="float-left" style="width: 30px;"></img>');
                        splitFlightImage.attr('src', '../htmlclient/content/images/indicatorimages/awbsplit_icon.png');
                        statusContainer.append(splitFlightImage);
                    }

                    if (this.IsSplitUld) {
                        var splitUldImage = $('<img class="float-left" style="width: 30px;"></img>');
                        if (this.IsSplitFlight) {
                            splitUldImage.css('margin-top', '8px');
                        }

                        splitUldImage.attr('src', '../htmlclient/content/images/indicatorimages/uldsplit_icon.png');
                        statusContainer.append(splitUldImage);
                    }

                    var refContainer = $('<div class="ref-container" style="line-height:28px"></div>');
                    var refNumber = $('<div>' + this.HwbSerialNumber + '-' + this.DestinationIata + '</div>');
                    //var cntContainer = $('<div class="cnt-container" style="margin-top:12px"></div>');
                    var cntNumber = $('<span class="cnt-container">Checked-In: ' + this.HwbReceivedCount + '</span>');
                    var tCounr = $('<span class="cnt-container" style="margin-left:20px">Total Pcs: ' + this.HwbTotalPcs + '</span>');
                    refContainer.append(refNumber);
                    refContainer.append(cntNumber);
                    refContainer.append(tCounr);

                    var imageContainer = $('<div class="custom-image-container"></div>');

                    var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px"></div>');
                    var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
                    snapshotButton.append(snapshotImage);
                    snapshotButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.BreakdownHwbs) {
                            screen.BreakdownHwbs = {};
                        }
                        screen.BreakdownHwbs.selectedItem = that;
                        myapp.CheckInAWBHWB.ShowHwbSnapshot_execute(screen);
                    });

                    imageContainer.append(snapshotButton);

                    li.append(statusContainer);
                    li.append(refContainer);
                    li.append(imageContainer);
                    list.append(li);
                }
            });

            if (!list.find('li').length) {
                list.before('<span id="noItemsHwbs">No Items</span>')
            }

            if (callbackFunc) {
                callbackFunc(data[0].AwbReceivedCount, data[0].AwbLoadCount, data[0].AwbPercentReceived);
            }
        },

        error: function (e) {
            console.log('Unable get date from GetPutAwayList.' + e);
        }
    })
}
myapp.CheckInAWBHWB.Filter_render = function (element, contentItem) {
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="hwbFilter" style="margin-left:3px;"></input>');
    elem.append(filter);
};
myapp.CheckInAWBHWB.Search1_execute = function (screen) {

    if ($('#historyFilterTextbox4').val()) {

        screen.SearchHistoryFilter = $('#historyFilterTextbox4').val();
    }
    else {
        screen.SearchHistoryFilter = null;
    }

};
myapp.CheckInAWBHWB.Search_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        fillBreakdownHwbList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, contentItem.screen.SelectedUld.Id, contentItem.screen.SelectedMawb.AwbManifestId, 1);
    });

};
myapp.CheckInAWBHWB.HistoryFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'historyFilterTextbox4');

};
myapp.CheckInAWBHWB.Search1_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.CheckInAWBHWB.Search1_execute(contentItem.screen);
    });

};