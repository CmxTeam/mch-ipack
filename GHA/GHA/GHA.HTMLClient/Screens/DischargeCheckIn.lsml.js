﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.DischargeCheckIn.ShowNotCheckedIn_postRender = function (element, contentItem) {

    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Complete') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Complete') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};
myapp.DischargeCheckIn.ShowCheckedIn_postRender = function (element, contentItem) {
    
    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Incomplete') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Incomplete') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};
myapp.DischargeCheckIn.RefreshButton_render = function (element, contentItem) {
    
    $iScrn = contentItem.screen;
    var a = function (s) {
        return function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                contentItem.screen.ActiveCheckInList.refresh();
                operation.complete();

            }));
        }
    }($iScrn);

    var div = $("<div class='am_refresh' id='myTest'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    div.on('click', a);
    $(element).append(div);

    $(element).trigger("create");

};
myapp.DischargeCheckIn.created = function (screen) {
    screen.IsComplete = false;

    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            })
        })

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            var uItems = result.split(',');
            if (uItems.length > 1) {

                screen.CurrentUser = uItems[0];
                var sId = +uItems[1];
                myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {
                    screen.CurrentUserStation = selectedStation.results[0];
                });

            }
            else {

                screen.CurrentUser = uItems[0];
            }

        }
    }));

};
myapp.DischargeCheckIn.DriverName_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "30px");
    $(element).css("line-height", "32px");

};
myapp.DischargeCheckIn.CarrierName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};
myapp.DischargeCheckIn.CheckInTimestamp_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};
myapp.DischargeCheckIn.TotalPieces_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};
myapp.DischargeCheckIn.TotalDeliveryOrders_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};
myapp.DischargeCheckIn.BalanceDue_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");

};
myapp.DischargeCheckIn.TotalPiecesLabel_postRender = function (element, contentItem) {
    
    element.textContent = "PCS:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.DischargeCheckIn.TotalDOLabel_postRender = function (element, contentItem) {
    
    element.textContent = "DO:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.DischargeCheckIn.TotalBalanceLabel_postRender = function (element, contentItem) {
   
    element.textContent = "DUE:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");

};
myapp.DischargeCheckIn.Reset_execute = function (screen) {
    
    screen.Filter = null;
    screen.SearchFilter = null;
    screen.DateFilter = null;
    screen.SearchDateFilter = null;
    screen.DateStart = null;
    screen.DateEnd = null;
    
};
myapp.DischargeCheckIn.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.DischargeCheckIn.DateFilter_postRender = function (element, contentItem) {
    
    $(element).css('padding-top', 0);

    //var iScrn = contentItem.screen;

    //contentItem.addChangeListener("value", function () {

    //    //var sItem = contentItem.value;

    //    if (contentItem.value == null) {
    //        iScrn.DateStart = null;
    //        iScrn.DateEnd = null;
    //    }
    //    else {
    //        var sItem = contentItem.value;
    //        var tMon = contentItem.value.getUTCMonth();
    //        var tDay = contentItem.value.getUTCDate();
    //        var tYear = contentItem.value.getUTCFullYear();
    //        iScrn.DateStart = new Date(tYear, tMon, tDay, 0, 0, 0);
    //        iScrn.DateEnd = new Date(tYear, tMon, tDay, 23, 59, 59);
    //    }

    //});

};
myapp.DischargeCheckIn.Search_execute = function (screen) {
    
    screen.SearchDateFilter = screen.DateFilter;
    var tDate = screen.DateFilter;
    if (tDate) {
        var tMon = tDate.getUTCMonth();
        var tDay = tDate.getUTCDate();
        var tYear = tDate.getUTCFullYear();
        screen.DateStart = new Date(tYear, tMon, tDay, 0, 0, 0);
        screen.DateEnd = new Date(tYear, tMon, tDay, 23, 59, 59);

    }
    else {
        screen.DateStart = null;
        screen.DateEnd = null;
    }

    if (screen.Filter)
    {
        screen.SearchFilter = screen.Filter;
    }
    else
    {
        screen.SearchFilter = null;
    }
    

};
myapp.DischargeCheckIn.ShowCheckedIn_execute = function (screen) {
    
    screen.IsComplete = true;

};
myapp.DischargeCheckIn.ShowNotCheckedIn_execute = function (screen) {
    
    screen.IsComplete = false;

};
myapp.DischargeCheckIn.DriverImage_postRender = function (element, contentItem) {
    
    $(element).css({ 'border-top-left-radius': 15 });
    $(element).css({ 'border-bottom-left-radius': 15 });
    $(element).css({ 'border-top-right-radius': 15 });
    $(element).css({ 'border-bottom-right-radius': 15 });

};
myapp.DischargeCheckIn.NewDischargePopup_postRender = function (element, contentItem) {

    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.DischargeCheckIn.SelectCarrierPopup_postRender = function (element, contentItem) {
   
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.DischargeCheckIn.SelectDriverPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.DischargeCheckIn.AddEditCarrierPopup_postRender = function (element, contentItem) {
   
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.DischargeCheckIn.AddEditDriverPopup_postRender = function (element, contentItem) {
    
    var container = $(element);

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.DischargeCheckIn.NewDischargeLabel_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");
    
};
myapp.DischargeCheckIn.SelectCarrierLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.DischargeCheckIn.SelectDriverLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.DischargeCheckIn.AddEditCarrierLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.DischargeCheckIn.AddEditDriverLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.DischargeCheckIn.NewDischarge_execute = function (screen) {
    
    screen.NewDischargeLabel = "Add Discharge";

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("NewDischargePopup");

};
myapp.DischargeCheckIn.Group26_postRender = function (element, contentItem) {
    
    $(element).css('border', '1px solid #ABABAB');
    $(element).css('border-radius', '15px');

};
myapp.DischargeCheckIn.Group27_postRender = function (element, contentItem) {
    
    $(element).css('border', '1px solid #ABABAB');
    $(element).css('border-radius', '15px');

};
myapp.DischargeCheckIn.SearchDriver_execute = function (screen) {
    
    screen.SelectDriverLabel= "Select Driver";

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.closePopup("NewDischargePopup").then(function () {

        screen.showPopup("SelectDriverPopup");

    });

};
myapp.DischargeCheckIn.SearchCarrier_execute = function (screen) {
    
    screen.SelectCarrierLabel = "Select Carrier";

    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.closePopup("NewDischargePopup").then(function () {

        screen.showPopup("SelectCarrierPopup");

    });

};
myapp.DischargeCheckIn.FilterCarrierReset_execute = function (screen) {
    
    screen.SearchCarrierFilter = null;

};
myapp.DischargeCheckIn.FilterCarrierCancel_execute = function (screen) {
    
    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.closePopup("SelectCarrierPopup").then(function () {

        screen.showPopup("NewDischargePopup");

    });

};