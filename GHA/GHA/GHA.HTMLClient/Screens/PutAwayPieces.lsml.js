﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $PutAwaySelQuantityInput;
var $PutAwayQuantityInput;
var $PutAwayQuantityInput1;
var $LocationFilterInput2;
var $LocationFilterInput3;
var $ShipmentFilter;
var $UnAssignCountInput;
var unAssignHeader;
var bcInit;
var $forkliftCount;
var $iScrn5;
var $rButton;
var $sourcePopup;
var $locationHolder;
var $NotPutAwayButton;
var $PutAwayButton;
var $SelectedPutAway;
var $SelectedPutedAway;

///////////////////////////////////////
//             Methods
//////////////////////////////////////

myapp.PutAwayPieces.created = function (screen) {
    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    //screen.findContentItem("FlightPutAwayList").isVisible = true;
    screen.findContentItem("FlightPutedAwayList").isVisible = false;

    screen.PiecesLabel = "PCS:";

    screen.details.displayName = "Put Away FLT# " + nFlt.Carrier.CarrierCode + nFlt.FlightNumber;

    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: { 'tId': cTask.Id, 'cUser': cUser },
            url: '../Web/GetForkliftPieceCount.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {

                myapp.activeDataWorkspace.SnapshotDBData.EntityTypes_SingleOrDefault(4).execute().then(function (selectedEntity3) {

                    myapp.activeDataWorkspace.SnapshotDBData.EntityTypes_SingleOrDefault(3).execute().then(function (selectedEntity2) {

                        myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(2).execute().then(function (selectedEntity4) {

                            screen.AwbEntityType = selectedEntity4.results[0];
                            screen.UldEntityType = selectedEntity3.results[0];
                            screen.HwbEntityType = selectedEntity2.results[0];
                            $('span#flCtr').text(result);
                            screen.ForkLiftCount = result;

                            if (nFlt.TotalPieces == nFlt.PutAwayPieces && nFlt.Status != screen.CompleteStatus) {

                                screen.FinalizePopupHeader = "Finalize Flight";
                                screen.FinalizeText = "Do you want to finalize flight?";

                                $('#finalizePopupContent').parent().center();

                                screen.showPopup("FinalizePopup");

                            }

                        });
                    });
                });
            }
        }
    }));
};

myapp.PutAwayPieces.Recover_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showRecoverFlight(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.PutAwayPieces.PutULDAway_execute = function (screen) {

    var sLoc = screen.WarehouseLocations1.selectedItem;

    var resp4 = msls.showMessageBox("Do you want to put away this ULD at " + sLoc.Location + "?", { title: 'Put Away ULD', buttons: msls.MessageBoxButtons.yesNo });
    resp4.then(function (val) {
        if (val == msls.MessageBoxResult.yes) {
            screen.closePopup("PutAwayULDPopup").then(function () {
                msls.showProgress(msls.promiseOperation(function (operation) {

                    var nFlt = screen.SelectedFlt;
                    if (!nFlt) {
                        nFlt = $FlightManifest;
                    }
                    var nUld = screen.ULDs.selectedItem;
                    if (!nUld) {
                        nUld = $SelectedUld;
                    }
                    var cUser = screen.CurrentUser;
                    if (!cUser) {
                        cUser = $CurrentUser;
                    }
                    var aTimeout = screen.AjaxTimeout;
                    if (!aTimeout) {
                        aTimeout = $AjaxTimeout;
                    }

                    $.ajax({
                        type: 'post',
                        data: { 'FltId': nFlt.Id, 'UldId': nUld.Id, 'uLoc': sLoc.Id, 'cUser': cUser },
                        url: '../Web/PutAwayULD.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });

                }).then(function PromiseSuccess(PromiseResult) {

                    var result = PromiseResult;

                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {

                            screen.SelectedFlt.details.refresh();

                        }
                    }

                }));
            });
        }
    });
};

myapp.PutAwayPieces.Breakdown_execute = function (screen) {

    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cStation = screen.CurrentUserStation;
    if (!cStation) {
        cStation = $CurrentUserStation;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    myapp.showBreakdownULD(nFlt, cUser, cStation, cWarehouse, cTask, aTimeout);

};

myapp.PutAwayPieces.FinalizeFlight_execute = function (screen) {

    screen.FinalizePopupHeader = "Finalize Flight";
    screen.FinalizeText = "Do you want to finalize flight?";

    $('#finalizePopupContent').parent().center();

    screen.showPopup("FinalizePopup");

};

myapp.PutAwayPieces.GoToHome_execute = function (screen) {

    myapp.navigateHome();

};

//myapp.PutAwayPieces.PutAway_execute = function (screen) {

//    var nFlt = screen.SelectedFlt;
//    if (!nFlt) {
//        nFlt = $FlightManifest;
//    }
//    var aTimeout = screen.AjaxTimeout;
//    if (!aTimeout) {
//        aTimeout = $AjaxTimeout;
//    }
//    var nHWB = screen.FlightHWBs.selectedItem;
//    screen.HwbId = nHWB.Id;
//    screen.EnteredLocation = null;
//    screen.SelectedLocation = null;

//    msls.showProgress(msls.promiseOperation(function (operation) {

//        $.ajax({
//            type: 'post',
//            data: { 'FltId': nFlt.Id, 'HwbId': nHWB.Id },
//            url: '../Web/GetPutAwayPieces.ashx',
//            timeout: aTimeout,
//            success: operation.code(function AjaxSuccess(AjaxResult) {
//                operation.complete(AjaxResult);
//            }),
//            error: operation.code(function AjaxError(AjaxResult) {
//                AjaxResult = "Error: Unable to process request. \n\n" +
//                              "Try again later.";
//                operation.complete(AjaxResult);

//            })
//        });

//    }).then(function PromiseSuccess(PromiseResult) {

//        var result = PromiseResult;

//        if (result != null || result < 0) {
//            if (result.substring(0, 5) == "Error") {
//                alert(result);
//            }
//            else {

//                if (result != "0") {

//                    var rItems = result.split(',');

//                    if (rItems.length == 4) {

//                        screen.EnteredQuantity = rItems[0];
//                        screen.InitRemPutAwayPieces = rItems[0];
//                        screen.TotalPieces = rItems[2];
//                        screen.PercentPutAway = rItems[3];

//                        if (screen.TotalPutAwayPieces == rItems[1]) {

//                            screen.TotalPutAwayPieces = -1;
//                            screen.TotalPutAwayPieces = rItems[1];

//                        }
//                        else {
//                            screen.TotalPutAwayPieces = rItems[1];
//                        }
//                    }
//                    else {
//                        screen.EnteredQuantity = 0;
//                        screen.InitRemPutAwayPieces = 0;
//                        screen.TotalPieces = 0;
//                        screen.PercentPutAway = 0;
//                        screen.TotalPutAwayPieces = 0;
//                    }

//                    $('#stageLocationPopupContent').parent().center();

//                    screen.showPopup("PutAwayPiecesPopup").then(function () {
//                        setTimeout(function () {
//                            if (screen.TotalPutAwayPieces == rItems[1]) {

//                                screen.TotalPutAwayPieces = -1;
//                                screen.TotalPutAwayPieces = rItems[1];

//                            }
//                            $PutAwayQuantityInput.focus();
//                            $PutAwayQuantityInput.select();

//                        }, 1);
//                    });
//                }
//            }
//        }
//    }));
//};

myapp.PutAwayPieces.Reset_execute = function (screen) {
    $('input#refFilter').last().val('');
    fillPutAwayList(screen, true, screen.SelectedFlt.Id, 1);
    fillPutedAwayList(screen, true, screen.SelectedFlt.Id, 1);
    //screen.Filter = null;

};

myapp.PutAwayPieces.Reset1_execute = function (screen) {
    $('input#historyFilterTextbox5').last().val('');
    createHistory(screen.SelectedFlt.Id, 1, true);
    //screen.HistoryFilter = null;
    //screen.SearchHistoryFilter = null;
};

myapp.PutAwayPieces.SelectPutAwayLocation_execute = function (screen) {

    screen.LocationFilter = null;
    screen.WarehouseLocations.selectedItem = null;

    screen.closePopup("PutAwayPiecesPopup").then(function () {

        $('#selectLocationPopupContent').parent().center();

        screen.showPopup("SelectLocationPopup").then(function () {
            setTimeout(function () {
                $LocationFilterInput2.focus();
                $LocationFilterInput2.select();
            }, 1);
        });
    });



};

myapp.PutAwayPieces.SelectLocation_execute = function (screen) {

    var sLoc = screen.WarehouseLocations.selectedItem;
    screen.SelectedLocation = sLoc;
    screen.EnteredLocation = sLoc.Location;

    screen.closePopup("SelectLocationPopup").then(function () {

        $(window).one("popupcreate", function (e) {

            $(e.target).popup({

                positionTo: "window"

            });

        });

        screen.showPopup($sourcePopup).then(function () {
            $($locationHolder).text(screen.EnteredLocation);
        });
    });

};

myapp.PutAwayPieces.Reset3_execute = function (screen) {

    screen.LocationFilter = null;

};

myapp.PutAwayPieces.PutPiecesAway_execute = function (screen) {

    $("input[type=text]").blur();

    msls.showProgress(msls.promiseOperation(function (operation) {

        var nHwb = screen.FlightHWBs.selectedItem;
        var pQty = screen.EnteredQuantity;
        var nFlt = screen.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cTask = screen.SelectedTask;
        if (!cTask) {
            cTask = $ActiveTask;
        }
        var cUser = screen.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        $.ajax({
            type: 'post',
            data: { 'eTypeId': 3, 'eId': nHwb.Id, 'pQty': pQty, 'tId': cTask.Id, 'cUser': cUser },
            url: '../Web/AddForkliftPieces.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {

                var curCnt = +screen.ForkLiftCount;
                var addCnt = +screen.EnteredQuantity;
                var newCnt = +curCnt + +addCnt;
                screen.ForkLiftCount = newCnt;

                if (result != "0") {

                    var rItems = result.split(',');

                    if (rItems.length == 4) {

                        screen.EnteredQuantity = parseInt(rItems[0]);
                        screen.InitRemPutAwayPieces = parseInt(rItems[0]);
                        screen.TotalPieces = parseInt(rItems[2]);
                        screen.PercentPutAway = parseFloat(rItems[3]);

                        if (screen.TotalPutAwayPieces == parseInt(rItems[1])) {

                            screen.TotalPutAwayPieces = -1;
                            screen.TotalPutAwayPieces = parseInt(rItems[1]);

                        }
                        else {
                            screen.TotalPutAwayPieces = parseInt(rItems[1]);
                        }

                        $PutAwayQuantityInput.focus();
                        $PutAwayQuantityInput.select();
                    }
                    else {
                        screen.EnteredQuantity = 0;
                        screen.InitRemPutAwayPieces = 0;
                        screen.TotalPieces = 0;
                        screen.PercentPutAway = 0;
                        screen.TotalPutAwayPieces = 0;

                        $PutAwayQuantityInput.focus();
                        $PutAwayQuantityInput.select();
                    }

                    screen.closePopup("PutAwayPiecesPopup").then(function () {

                        screen.SelectedFlt.details.refresh();

                    }, 1000);
                }
            }
        }
    }));
};

myapp.PutAwayPieces.PutAwayPieces1_execute = function (screen) {

    $("input[type=text]").blur();

    msls.showProgress(msls.promiseOperation(function (operation) {

        var nAwb = screen.FlightAWBs.selectedItem;
        var pQty = screen.EnteredQuantity1;
        var nFlt = screen.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cUser = screen.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var cTask = screen.SelectedTask;
        if (!cTask) {
            cTask = $ActiveTask;
        }
        var aTimeout = screen.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        $.ajax({
            type: 'post',
            data: { 'eTypeId': 2, 'eId': nAwb.Id, 'pQty': pQty, 'tId': cTask.Id, 'cUser': cUser },
            url: '../Web/AddForkliftPieces.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                var curCnt = +screen.ForkLiftCount;
                var addCnt = +screen.EnteredQuantity1;
                var newCnt = +curCnt + +addCnt;
                screen.ForkLiftCount = newCnt;

                if (result != "0") {

                    var rItems = result.split(',');

                    if (rItems.length == 4) {

                        screen.EnteredQuantity1 = parseInt(rItems[0]);
                        screen.InitRemPutAwayPieces1 = parseInt(rItems[0]);
                        screen.TotalPieces1 = parseInt(rItems[2]);
                        screen.PercentPutAway1 = parseFloat(rItems[3]);

                        if (screen.TotalPutAwayPieces1 == parseInt(rItems[1])) {

                            screen.TotalPutAwayPieces1 = -1;
                            screen.TotalPutAwayPieces1 = parseInt(rItems[1]);

                        }
                        else {
                            screen.TotalPutAwayPieces1 = parseInt(rItems[1]);
                        }

                        $PutAwayQuantityInput1.focus();
                        $PutAwayQuantityInput1.select();
                    }
                    else {
                        screen.EnteredQuantity1 = 0;
                        screen.InitRemPutAwayPieces1 = 0;
                        screen.TotalPieces1 = 0;
                        screen.PercentPutAway1 = 0;
                        screen.TotalPutAwayPieces1 = 0;

                        $PutAwayQuantityInput1.focus();
                        $PutAwayQuantityInput1.select();
                    }

                    screen.closePopup("PutAwayAWBPopup").then(function () {

                        screen.SelectedFlt.details.refresh();
 
                    }, 1000);
                }
            }
        }
    }));
};

//myapp.PutAwayPieces.PutAway1_execute = function (screen) {

//    var nFlt = screen.SelectedFlt;
//    if (!nFlt) {
//        nFlt = $FlightManifest;
//    }
//    var aTimeout = screen.AjaxTimeout;
//    if (!aTimeout) {
//        aTimeout = $AjaxTimeout;
//    }
//    var nAWB = screen.FlightAWBs.selectedItem;
//    screen.AwbId = nAWB.Id;

//    msls.showProgress(msls.promiseOperation(function (operation) {

//        $.ajax({
//            type: 'post',
//            data: { 'FltId': nFlt.Id, 'AwbId': nAWB.Id },
//            url: '../Web/GetPutAwayAwbPieces.ashx',
//            timeout: aTimeout,
//            success: operation.code(function AjaxSuccess(AjaxResult) {
//                operation.complete(AjaxResult);
//            }),
//            error: operation.code(function AjaxError(AjaxResult) {
//                AjaxResult = "Error: Unable to process request. \n\n" +
//                              "Try again later.";
//                operation.complete(AjaxResult);

//            })
//        });

//    }).then(function PromiseSuccess(PromiseResult) {

//        var result = PromiseResult;

//        if (result != null || result < 0) {
//            if (result.substring(0, 5) == "Error") {
//                alert(result);
//            }
//            else {

//                if (result != "0") {

//                    var rItems = result.split(',');

//                    if (rItems.length == 4) {

//                        screen.EnteredQuantity1 = rItems[0];
//                        screen.InitRemPutAwayPieces1 = rItems[0];
//                        screen.TotalPieces1 = rItems[2];
//                        screen.PercentPutAway1 = rItems[3];

//                        if (screen.TotalPutAwayPieces1 == rItems[1]) {

//                            screen.TotalPutAwayPieces1 = -1;
//                            screen.TotalPutAwayPieces1 = rItems[1];

//                        }
//                        else {
//                            screen.TotalPutAwayPieces1 = rItems[1];
//                        }
//                    }
//                    else {
//                        screen.EnteredQuantity1 = 0;
//                        screen.InitRemPutAwayPieces1 = 0;
//                        screen.TotalPieces1 = 0;
//                        screen.PercentPutAway1 = 0;
//                        screen.TotalPutAwayPieces1 = 0;
//                    }

//                    $('#stageLocationPopupContent').parent().center();

//                    screen.showPopup("PutAwayAWBPopup").then(function () {
//                        setTimeout(function () {
//                            if (screen.TotalPutAwayPieces1 == rItems[1]) {

//                                screen.TotalPutAwayPieces1 = -1;
//                                screen.TotalPutAwayPieces1 = rItems[1];

//                            }
//                            $PutAwayQuantityInput1.focus();
//                            $PutAwayQuantityInput1.select();

//                        }, 1);
//                    });
//                }
//            }
//        }
//    }));
//};

myapp.PutAwayPieces.ShowAddToForkliftPopup_execute = function (screen) {

    var sItem = screen.FlightPutAwayList.selectedItem;
    $SelectedPutAway = screen.FlightPutAwayList.selectedItem;
    screen.EntityType = sItem.Type;
    screen.EntityId = sItem.EntityId;
    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }
    switch (sItem.Type) {
        case "AWB":

            screen.SelectedQuantity = +sItem.TotalPieces - (+sItem.PutAwayPieces + +sItem.ForkliftPieces);
            screen.MaxPutAwayPieces = +sItem.TotalPieces - (+sItem.PutAwayPieces + +sItem.ForkliftPieces);
            screen.AddToForkliftText = "AWB# " + sItem.RefNumber;

            $('#addToForkliftPopupContent').parent().center();

            screen.showPopup("AddToForkliftPopup").then(function () {
                setTimeout(function () {

                    $PutAwaySelQuantityInput.focus();
                    $PutAwaySelQuantityInput.select();

                }, 100);
            });

            break;
        case "HWB":

            screen.SelectedQuantity = +sItem.TotalPieces - (+sItem.PutAwayPieces + +sItem.ForkliftPieces);
            screen.MaxPutAwayPieces = +sItem.TotalPieces - (+sItem.PutAwayPieces + +sItem.ForkliftPieces);
            screen.AddToForkliftText = "HWB# " + sItem.RefNumber1;
            
            $('#addToForkliftPopupContent').parent().center();

            screen.showPopup("AddToForkliftPopup").then(function () {
                setTimeout(function () {

                    $PutAwaySelQuantityInput.focus();
                    $PutAwaySelQuantityInput.select();

                }, 100);
            });

            break;
        case "ULD":

            screen.ConfirmPopupHeader = "Add to Forklift";
            screen.ConfirmText = "Do you want to add ULD to forklift?";

            $('#confirmPopupContent').parent().center();

            screen.showPopup("ConfirmPopup");

            break;
        default:
            break;

    }

};

myapp.PutAwayPieces.ShowSnapshot_execute = function (screen) {

    var sItem = screen.FlightPutAwayList.selectedItem;
    if (!sItem) {
        sItem = $SelectedPutAway;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cWarehouse = screen.CurrentUserWarehouse;
    if (!cWarehouse) {
        cWarehouse = $CurrentUserWarehouse;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    switch (sItem.Type) {
        case "AWB":
            var uDesc = sItem.RefNumber;
            var hType = screen.AwbEntityType;
            if (!hType) {
                hType = $AwbEntityType;
            }
            myapp.showSnapshot(hType, cTask, cUser, cWarehouse, sItem.EntityId, uDesc, aTimeout);
            break;
        case "HWB":
            var uDesc = sItem.RefNumber1;
            var hType = screen.HwbEntityType;
            if (!hType) {
                hType = $HwbEntityType;
            }
            myapp.showSnapshot(hType, cTask, cUser, cWarehouse, sItem.EntityId, uDesc, aTimeout);
            break;
        case "ULD":
            var uDesc = sItem.RefNumber;
            var hType = screen.UldEntityType;
            if (!hType) {
                hType = $UldEntityType;
            }
            myapp.showSnapshot(hType, cTask, cUser, cWarehouse, sItem.EntityId, uDesc, aTimeout);
            break;
        default:
            break;
    }

};

myapp.PutAwayPieces.FinalizeYes_execute = function (screen) {

    screen.closePopup("FinalizePopup").then(function () {

        msls.showProgress(msls.promiseOperation(function (operation) {

            var nFlt = screen.SelectedFlt;
            if (!nFlt) {
                nFlt = $FlightManifest;
            }
            var cTask = screen.SelectedTask;
            if (!cTask) {
                cTask = $ActiveTask;
            }
            var cUser = screen.CurrentUser;
            if (!cUser) {
                cUser = $CurrentUser;
            }
            var aTimeout = screen.AjaxTimeout;
            if (!aTimeout) {
                aTimeout = $AjaxTimeout;
            }
            $.ajax({
                type: 'post',
                data: { 'fId': nFlt.Id, 'tId': cTask.Id, 'cUser': cUser },
                url: '../Web/FinalizeFlight.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    myapp.showCargoReceiver(aTimeout);
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });

        }).then(function PromiseSuccess(PromiseResult) {
            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }

            }
        }));
    }, 100);
};

myapp.PutAwayPieces.FinalizeNo_execute = function (screen) {

    screen.closePopup("FinalizePopup");

};

myapp.PutAwayPieces.ConfirmYes_execute = function (screen) {

    screen.closePopup("ConfirmPopup").then(function () {
        msls.showProgress(msls.promiseOperation(function (operation) {
            var sItem = screen.FlightPutAwayList.selectedItem;
            if (!sItem) {
                sItem = $SelectedPutAway;
            }
            var nFlt = screen.SelectedFlt;
            if (!nFlt) {
                nFlt = $FlightManifest;
            }
            var pQty = +1;
            var cTask = screen.SelectedTask;
            if (!cTask) {
                cTask = $ActiveTask;
            }
            var tType = +4;
            var cUser = screen.CurrentUser;
            if (!cUser) {
                cUser = $CurrentUser;
            }
            var aTimeout = screen.AjaxTimeout;
            if (!aTimeout) {
                aTimeout = $AjaxTimeout;
            }

            $.ajax({
                type: 'post',
                data: { 'eTypeId': tType, 'eId': sItem.EntityId, 'pQty': pQty, 'tId': cTask.Id, 'cUser': cUser },
                url: '../Web/AddForkliftPieces.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });

        }).then(function PromiseSuccess(PromiseResult) {
            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    $('span#flCtr').text(screen.ForkLiftCount + 1);
                    screen.ForkLiftCount = +screen.ForkLiftCount + +1;

                    //screen.FlightPutAwayList.refresh();

                    fillPutAwayList(screen, true, screen.SelectedFlt.Id, 1);
                    fillPutedAwayList(screen, true, screen.SelectedFlt.Id, 1);
                }
            }
        }));
    });

};

myapp.PutAwayPieces.ConfirmNo_execute = function (screen) {

    screen.closePopup("ConfirmPopup");

};

myapp.PutAwayPieces.Reset2_execute = function (screen) {

    var sItem = screen.FlightPutAwayList.selectedItem;
    if (!sItem) {
        sItem = $SelectedPutAway;
    }
    var nFlt = screen.SelectedFlt;
    if (!nFlt) {
        nFlt = $FlightManifest;
    }

    $PutAwaySelQuantityInput.val(+sItem.TotalPieces - (+sItem.PutAwayPieces + +sItem.ForkliftPieces));
    //screen.SelectedQuantity = +sItem.TotalPieces - (+sItem.PutAwayPieces + +sItem.ForkliftPieces);

    $PutAwaySelQuantityInput.focus();
    $PutAwaySelQuantityInput.select();

};

myapp.PutAwayPieces.ShowSnapshot1_execute = function (screen) {

    var sItem = screen.FlightPutedAwayList.selectedItem;
    if (!sItem) {
        sItem = $SelectedPutAway;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    switch (sItem.Type) {
        case "AWB":
            var uDesc = sItem.Reference;
            var hType = screen.AwbEntityType;
            if (!hType) {
                hType = $AwbEntityType;
            }
            myapp.showSnapshot(hType, cTask, cUser, sItem.EntityId, uDesc, aTimeout);
            break;
        case "HWB":
            var uDesc = sItem.Reference1;
            var hType = screen.HwbEntityType;
            if (!hType) {
                hType = $HwbEntityType;
            }
            myapp.showSnapshot(hType, cTask, cUser, sItem.EntityId, uDesc, aTimeout);
            break;
        case "ULD":
            var uDesc = sItem.Reference;
            var hType = screen.UldEntityType;
            if (!hType) {
                hType = $UldEntityType;
            }
            myapp.showSnapshot(shType, cTask, cUser, sItem.EntityId, uDesc, aTimeout);
            break;
        default:
            break;
    }

};

myapp.PutAwayPieces.ShowUnAssignLocationPopup_execute = function (screen) {

    var selItem = screen.FlightPutedAwayList.selectedItem;
    $SelectedPutedAway = screen.FlightPutedAwayList.selectedItem;

    switch (selItem.Type) {
        case "AWB":

            screen.findContentItem("PopupKeypad1").isVisible = true;
            screen.findContentItem("UnAssignCount").isVisible = true;
            screen.findContentItem("UnAssignText").isVisible = true;
            screen.findContentItem("UnAssignULDText").isVisible = false;
            screen.UnAssignCount = selItem.Count;
            screen.MaxPutAwayPieces = selItem.Count;

            screen.UnAssignText = "AWB# " + selItem.Reference + "\r" + "LOC: " + selItem.Location + "\r" + "PCS: " + selItem.Count;

            $('#unassignLocationPopupContent').parent().center();

            screen.showPopup("UnAssignLocationPopup").then(function () {
                setTimeout(function () {

                    unAssignHeader.textContent = "Un-Assign Quantity";
                    $UnAssignCountInput.focus();
                    $UnAssignCountInput.select();

                }, 100);
            });

            break;
        case "HWB":

            screen.findContentItem("PopupKeypad1").isVisible = true;
            screen.findContentItem("UnAssignCount").isVisible = true;
            screen.findContentItem("UnAssignText").isVisible = true;
            screen.findContentItem("UnAssignULDText").isVisible = false;
            screen.UnAssignCount = selItem.Count;
            screen.MaxPutAwayPieces = selItem.Count;

            screen.UnAssignText = "AWB# " + selItem.Reference + "\r" + "HWB# " + selItem.Reference1 + "\r" + "LOC: " + selItem.Location + "\r" + "PCS: " + selItem.Count;

            $('#unassignLocationPopupContent').parent().center();

            screen.showPopup("UnAssignLocationPopup").then(function () {
                setTimeout(function () {

                    unAssignHeader.textContent = "Un-Assign Quantity";
                    $UnAssignCountInput.focus();
                    $UnAssignCountInput.select();

                }, 100);
            });

            break;
        case "ULD":

            screen.findContentItem("PopupKeypad1").isVisible = false;
            screen.findContentItem("UnAssignCount").isVisible = false;
            screen.findContentItem("UnAssignText").isVisible = false;
            screen.findContentItem("UnAssignULDText").isVisible = true;
            screen.UnAssignULDText = "Do you want to un-assign location " + selItem.Location + " for ULD# " + selItem.Reference + "?";

            $('#unassignLocationPopupContent').parent().center();

            screen.showPopup("UnAssignLocationPopup").then(function () {
                setTimeout(function () {

                    unAssignHeader.textContent = "Un-Assign ULD Location";

                }, 100);
            });

            break;
        default:
            break;
    }

};

myapp.PutAwayPieces.Reset4_execute = function (screen) {

    //var tShp = screen.FlightPutedAwayList.selectedItem;
    $UnAssignCountInput.val(screen.MaxPutAwayPieces);
    $UnAssignCountInput.focus();
    $UnAssignCountInput.select();

};


///////////////////////////////////////
//        Rendor Functions
//////////////////////////////////////

myapp.PutAwayPieces.BarcodeListener_render = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var bcText = contentItem.value;
    var cUser = iScrn.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var cTask = iScrn.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var cFlt = iScrn.SelectedFlt;
    if (!cFlt) {
        cFlt = $FlightManifest;
    }
    var aTimeout = iScrn.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    var chars = [];
    var ctr = "0";
    var result = "";

    if (bcText == null) {
        bcText = "";
    }

    $(document).off('keypress').on('keypress', function (event) {

        var charCode = event.which || event.keyCode;

        if (charCode == 13) {
            $ShipmentFilter.blur();
            $rButton.focus();
        }

        if (ctr == "1" && charCode != 126) {
            chars.push(String.fromCharCode(charCode));
        }

        if (charCode == 126) {
            if (ctr == "0") {
                ctr = "1";
            }
            else {
                ctr = "2";
            }
        }

        if (ctr == "2") {
            bcText = chars.join("");

            iScrn.findContentItem("FlightPutAwayList").isVisible = false;
            iScrn.findContentItem("FlightPutedAwayList").isVisible = false;
            contentItem.screen.findContentItem("FilterGroup").isVisible = false;

            $PutAwayButton.css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            $NotPutAwayButton.css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            console.log('here');
            msls.showProgress(msls.promiseOperation(function (operation) {

                $.ajax({
                    type: 'post',
                    data: { 'bcScan': bcText, 'tId': cTask.Id, 'fId': cFlt.Id, 'cUser': cUser },
                    url: '../Web/ScanPutAwayPieces.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        chars = [];
                        ctr = "0";
                        bcText = "";
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        console.log(AjaxResult);
                        AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                        chars = [];
                        ctr = "0";
                        bcText = "";
                        operation.complete(AjaxResult);
                    })
                });

            }).then(function PromiseSuccess(PromiseResult) {
                try {
                    var result = PromiseResult;

                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {

                            var rItems = result.split(',');

                            if (rItems[0] == "S") {
                                console.log('S-Location');
                                $('span#flCtr').text("0");
                                iScrn.UpdatePopupHeader = "Assigned Location";
                                iScrn.UpdateText = rItems[1];

                                $('#updatePopupContent').parent().center();

                                iScrn.showPopup("UpdatePopup").then(function () {
                                    fillPutAwayList(iScrn, true, iScrn.SelectedFlt.Id, 1, function (pap, ttl, percent) {
                                        $('#papProgressBar p').text(pap + ' of ' + ttl);
                                        $('#papProgressBar div').css('width', percent);
                                    });


                                    iScrn.ForkLiftCount = "0";

                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                });


                            }
                            else if (rItems[0] == "A") {
                                console.log('A-AWB');
                                iScrn.EntityType = "AWB";
                                iScrn.EntityId = rItems[1];
                                iScrn.AddToForkliftText = rItems[2];
                                iScrn.SelectedQuantity = rItems[3];
                                iScrn.MaxPutAwayPieces = rItems[3];
                                chars = [];
                                ctr = "0";
                                bcText = "";
                                event = null;
                                result = null;

                                $('#addToForkliftPopupContent').parent().center();

                                iScrn.showPopup("AddToForkliftPopup").then(function () {
                                    setTimeout(function () {

                                        $PutAwaySelQuantityInput.focus();
                                        $PutAwaySelQuantityInput.select();

                                    }, 100);
                                });

                            }
                            else if (rItems[0] == "H") {
                                console.log('H-HWB');
                                iScrn.EntityType = "HWB";
                                iScrn.EntityId = rItems[1];
                                iScrn.AddToForkliftText = rItems[2];
                                iScrn.SelectedQuantity = rItems[3];
                                iScrn.MaxPutAwayPieces = rItems[3];
                                chars = [];
                                ctr = "0";
                                bcText = "";
                                event = null;
                                result = null;

                                $('#addToForkliftPopupContent').parent().center();

                                iScrn.showPopup("AddToForkliftPopup").then(function () {
                                    setTimeout(function () {

                                        $PutAwaySelQuantityInput.focus();
                                        $PutAwaySelQuantityInput.select();

                                    }, 100);
                                });
                            }
                            else if (rItems[0] == "L") {
                                console.log('L-Invalid Location');
                                iScrn.UpdatePopupHeader = "Invalid Location";
                                iScrn.UpdateText = "Unable to find location.";

                                $('#updatePopupContent').parent().center();

                                iScrn.showPopup("UpdatePopup").then(function () {


                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                });
                            }
                            else if (rItems[0] == "M") {
                                console.log('M-Multiple HWBs');
                                iScrn.UpdatePopupHeader = "Multiple HWBs";
                                iScrn.UpdateText = "The AWB scanned has multiple HWBs to be put away.  Scan HWB barcode or select HWB from the put away list.";

                                $('#updatePopupContent').parent().center();

                                iScrn.showPopup("UpdatePopup").then(function () {


                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                });
                            }
                            else if (rItems[0] == "D") {
                                console.log('D-Valid Scan');
                                iScrn.UpdatePopupHeader = "Valid Scan";
                                iScrn.UpdateText = "The shipment scan is valid but not available for put away on current flight.";

                                $('#updatePopupContent').parent().center();

                                iScrn.showPopup("UpdatePopup").then(function () {


                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                });
                            }
                            else if (rItems[0] == "V") {
                                console.log('V-No Shipments');
                                $('span#flCtr').text("0");
                                iScrn.UpdatePopupHeader = "No Shipments";
                                iScrn.UpdateText = "No shipment pieces on the forklift. \n\rAdd pieces to forklift and scan location again.";

                                $('#updatePopupContent').parent().center();

                                iScrn.showPopup("UpdatePopup").then(function () {

                                    //msls.showMessageBox("No shipment pieces on the forklift. \n\r Add pieces to forklift and scan location again.");                        
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                });
                            }
                            else {
                                console.log('Invalid Scan');
                                iScrn.UpdatePopupHeader = "Invalid Barcode";
                                iScrn.UpdateText = "Invalid Barcode Scan.";

                                $('#updatePopupContent').parent().center();

                                iScrn.showPopup("UpdatePopup").then(function () {

                                    //msls.showMessageBox("Invalid Barcode Scan");                        
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                });
                            }
                        }
                    }
                }
                catch (e) {
                    console.log(e);
                }
            }

            ));
        }
    });
};

myapp.PutAwayPieces.SelectedFlt_PercentPutAway_render = function (element, contentItem) {

    var fscrn = contentItem.screen;
    var fItem = fscrn.SelectedFlt;
    if (!fItem) {
        fItem = $FlightManifest;
    }
    var sInd = fItem.PutAwayPieces;
    var eInd = fItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "papProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var fscrn = contentItem.screen;
        var fItem = fscrn.SelectedFlt;
        if (!fItem) {
            fItem = $FlightManifest;
        }
        var sInd = fItem.PutAwayPieces;
        var eInd = fItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "papProgressBar", element);

    });

};

myapp.PutAwayPieces.PercentPutAway1_render = function (element, contentItem) {

    var uScrn = contentItem.parent;
    var uHWB = uScrn.value;
    var sInd = uHWB.PutAwayPieces;
    var eInd = uHWB.Pieces;

    progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var uScrn = contentItem.parent;
        var uHWB = uScrn.value;
        var sInd = uHWB.PutAwayPieces;
        var eInd = uHWB.Pieces;

        progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    });

};

myapp.PutAwayPieces.TotalPutAwayPieces_render = function (element, contentItem) {

    progress(0, 0, 0, "pProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var iScrn = contentItem.screen;
        var pPutAway = iScrn.PercentPutAway;
        var sInd = iScrn.TotalPutAwayPieces;
        var eInd = iScrn.TotalPieces;

        progress(pPutAway, sInd, eInd, "pProgressBar", element);

    });
};

myapp.PutAwayPieces.PercentPutAway_render = function (element, contentItem) {

    var uScrn = contentItem.parent;
    var uULD = uScrn.value;
    var sInd = uULD.PutAwayPieces;
    var eInd = 1;

    progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var uScrn = contentItem.parent;
        var uULD = uScrn.value;
        var sInd = uULD.PutAwayPieces;
        var eInd = 1;

        progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    });

};

myapp.PutAwayPieces.SelectedFlt_PercentPutAway1_render = function (element, contentItem) {

    var fscrn = contentItem.screen;
    var fItem = fscrn.SelectedFlt;
    var sInd = fItem.PutAwayPieces;
    var eInd = fItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "papProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var fscrn = contentItem.screen;
        var fItem = fscrn.SelectedFlt;
        var sInd = fItem.PutAwayPieces;
        var eInd = fItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "papProgressBar", element);

    });

};

myapp.PutAwayPieces.ForkLiftCount_render = function (element, contentItem) {

    if (contentItem.value == null) {
        contentItem.value = 0;
    }

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_forklift' onclick='showForkliftView()' ><img src='content/images/recoveryinprogress_icon_w.png' style='height: 41px;' /><span id='flCtr'>" + contentItem.value + "</span></div>");
    $(element).trigger("create");

    contentItem.addChangeListener("value", function () {

        $('span#flCtr').text(contentItem.value);

    });

};

myapp.PutAwayPieces.ForkLiftCount1_render = function (element, contentItem) {

    if (contentItem.value == null) {
        contentItem.value = 0;
    }

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_forklift' onclick='showUldForkliftView()' ><img src='content/images/recoveryinprogress_icon_w.png' style='height:34px' /><span id='flCtr1'>" + contentItem.value + "</span></div>");
    $(element).trigger("create");

    contentItem.addChangeListener("value", function () {

        $('#flCtr1').text(contentItem.value);

    });

};

myapp.PutAwayPieces.SelectedFlt_PercentPutAway2_render = function (element, contentItem) {

    var fscrn = contentItem.screen;
    var fItem = fscrn.SelectedFlt;
    var sInd = fItem.PutAwayPieces;
    var eInd = fItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "papProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var fscrn = contentItem.screen;
        var fItem = fscrn.SelectedFlt;
        var sInd = fItem.PutAwayPieces;
        var eInd = fItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "papProgressBar", element);

    });

};

myapp.PutAwayPieces.ForkLiftCount2_render = function (element, contentItem) {

    if (contentItem.value == null) {
        contentItem.value = 0;
    }

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_forklift' onclick='showAwbForkliftView()' ><img src='content/images/recoveryinprogress_icon_w.png' style='height:34px' /><span id='flCtr'>" + contentItem.value + "</span></div>");
    $(element).trigger("create");

    contentItem.addChangeListener("value", function () {

        $('span#flCtr').text(contentItem.value);

    });

};

myapp.PutAwayPieces.PercentPutAway2_render = function (element, contentItem) {

    var uScrn = contentItem.parent;
    var uAWB = uScrn.value;
    var sInd = uAWB.PutAwayPieces;
    var eInd = uAWB.TotalPieces;

    progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var uScrn = contentItem.parent;
        var uAWB = uScrn.value;
        var sInd = uAWB.PutAwayPieces;
        var eInd = uAWB.TotalPieces;

        progress(contentItem.value, sInd, eInd, "bProgressBar", element);

    });

};

myapp.PutAwayPieces.TotalPutAwayPieces1_render = function (element, contentItem) {

    progress(0, 0, 0, "pProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var iScrn = contentItem.screen;
        var pPutAway = iScrn.PercentPutAway1;
        var sInd = iScrn.TotalPutAwayPieces1;
        var eInd = iScrn.TotalPieces1;

        progress(pPutAway, sInd, eInd, "pProgressBar", element);

    });

};

myapp.PutAwayPieces.PercentPutAway3_render = function (element, contentItem) {

    var uScrn = contentItem.parent;

    if (!uScrn || !uScrn.value) return;

    var uItem = uScrn.value;
    var sInd = uItem.PutAwayPieces;
    var eInd = uItem.TotalPieces;

    progress(contentItem.value, sInd, eInd, "dProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var uScrn = contentItem.parent;
        var uItem = uScrn.value;
        var sInd = uItem.PutAwayPieces;
        var eInd = uItem.TotalPieces;

        progress(contentItem.value, sInd, eInd, "dProgressBar", element);

    });

};

myapp.PutAwayPieces.RefreshButton_render = function (element, contentItem) {

    $(element).html("<div class='am_refresh' onclick='refreshView5()' ><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    $(element).trigger("create");

};

myapp.PutAwayPieces.ForkliftPieces_render = function (element, contentItem) {

    if (contentItem.value == null) {
        contentItem.value = 0;
    }

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_forklift_big'><img src='content/images/recoveryinprogress_icon.png' style='height: 41px;' /><span id='flCtrp'>" + contentItem.value + "</span></div>");
    $(element).trigger("create");

    contentItem.addChangeListener("value", function () {

        $('#flCtrp').text(contentItem.value);

    });

};


///////////////////////////////////////
//       Post Rendor Functions
//////////////////////////////////////

myapp.PutAwayPieces.SelectedFlt_WarehouseLocation_Location_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};

myapp.PutAwayPieces.SelectedFlt_Status_Name_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};

myapp.PutAwayPieces.ShowNotPutAway_postRender = function (element, contentItem) {

    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $NotPutAwayButton = $(element).find('a').first();
    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(a).off(evtName).on(evtName, function (e) {
        try {
            e.stopPropagation();
            console.log('start not put away click');
            //var a = $(this).find('a').first();
            $(this).css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

            $(this).parent().siblings('div.msls-column').each(function () {
                var span = $(this).find('a span.ui-btn-text');
                if (span && span.text() == 'Put Away') {
                    $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                }
            });

            if (contentItem.screen.findContentItem("FlightPutAwayList").isVisible != true) {
                fillPutAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
                //contentItem.screen.FlightPutAwayList.refresh([]).then(function () {
                //    console.log('FlightPutAwayList refresh end.');
                //});
                contentItem.screen.findContentItem("FilterGroup").isVisible = true;
                contentItem.screen.findContentItem("FlightPutAwayList").isVisible = true;
                contentItem.screen.findContentItem("FlightPutedAwayList").isVisible = false;

            }
            console.log('End not put away click');
        }
        catch (e) {
            console.log('error in ShowNotPutAway_postRender');
            console.log(e);
        }
    });
};

myapp.PutAwayPieces.EnteredQuantity_postRender = function (element, contentItem) {

    $PutAwayQuantityInput = $("input", $(element));

};

myapp.PutAwayPieces.LocationFilter_postRender = function (element, contentItem) {

    $LocationFilterInput2 = $("input", $(element));
    $(element).find('input').attr('id', 'locationFilterTextbox5');

};

myapp.PutAwayPieces.PutAwayPopupHeader_postRender = function (element, contentItem) {

    element.textContent = "Put Away HWB Pieces";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.PutAwayPieces.SelectLocationPopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Select Location";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.PutAwayPieces.EnteredLocation_postRender = function (element, contentItem) {

    $(element).attr("readonly");

};

myapp.PutAwayPieces.PutAwayULDPopupHeader_postRender = function (element, contentItem) {

    element.textContent = "Select Location";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.PutAwayPieces.LocationFilter1_postRender = function (element, contentItem) {

    $LocationFilterInput3 = $("input", $(element));

};

myapp.PutAwayPieces.SelectLocationPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'selectLocationPopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:550px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.PutAwayPieces.PutAwayULDPopup_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};

myapp.PutAwayPieces.PutAwayPiecesPopup_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};

myapp.PutAwayPieces.ForkliftViewHeader_postRender = function (element, contentItem) {

    element.textContent = "Forklift View";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.PutAwayPieces.ForkliftDetailsTemplate_postRender = function (element, contentItem) {
    enhanceForkliftViewItens(element, contentItem, '#selectedPiecesContainer', 'forkliftDeteilId', '#ttl', removePiecesFromForklift, assignLocation);
};

myapp.PutAwayPieces.HWB_HWBSerialNumber_postRender = function (element, contentItem) {
    $(element.parentNode).css('width', '150px');
};

myapp.PutAwayPieces.ForkliftViewPopop_postRender = function (element, contentItem) {
    addFooterIntoPopup(element, contentItem, 'forkliftViewFooter', 'ForkliftViewPopop', '#sLocation', 'selectedPiecesContainer', 'ttl', 'sLocation', executeHwbAssignLocation);
};

myapp.PutAwayPieces.SnapshotImage_postRender = function (element, contentItem) {

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_camera_big'><img src='content/images/camera_icon_w.png' style='height:36px' /></div>");
    $(element).trigger("create");

};

myapp.PutAwayPieces.StatusTimestamp_postRender = function (element, contentItem) {

    var eDate = contentItem.value;

    dateFormat(eDate, element);

};

myapp.PutAwayPieces.ShowPutAway_postRender = function (element, contentItem) {

    $PutAwayButton = $(element).find('a').first();

    $(element).on('touchstart', function (e) {
        try {
            var a = $(this).find('a').first();

            a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

            $(this).siblings().each(function () {
                var span = $(this).find('a span.ui-btn-text');
                if (span && span.text() == 'Not Put Away') {

                    $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                }
            });

            if (contentItem.screen.findContentItem("FlightPutedAwayList").isVisible != true) {


                fillPutedAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);

                contentItem.screen.findContentItem("FilterGroup").isVisible = true;
                contentItem.screen.findContentItem("FlightPutAwayList").isVisible = false;
                contentItem.screen.findContentItem("FlightPutedAwayList").isVisible = true;
            }
        }
        catch (e) {
            console.log('Error in ShowPutAway_postRender');
            console.log(e);
        }
    });

    $(element).click(function (e) {
        try {
            var a = $(this).find('a').first();
            a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

            $(this).siblings().each(function () {
                var span = $(this).find('a span.ui-btn-text');
                if (span && span.text() == 'Not Put Away') {
                    $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                }
            });

            if (contentItem.screen.findContentItem("FlightPutedAwayList").isVisible != true) {

                fillPutedAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
                contentItem.screen.findContentItem("FilterGroup").isVisible = true;
                contentItem.screen.findContentItem("FlightPutAwayList").isVisible = false;
                contentItem.screen.findContentItem("FlightPutedAwayList").isVisible = true;
            }
        }
        catch (e) {
            console.log('Error in ShowPutAway_postRender');
            console.log(e);
        }

    });

};

myapp.PutAwayPieces.ShowNotPutAway1_postRender = function (element, contentItem) {

    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};

myapp.PutAwayPieces.ShowPutAway1_postRender = function (element, contentItem) {

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};

myapp.PutAwayPieces.Dispositions_postRender = function (element, contentItem) {

    $(element).css("overflow", "auto");

};

myapp.PutAwayPieces.UldForkliftViewHeader_postRender = function (element, contentItem) {

    element.textContent = "Forklift View";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.PutAwayPieces.UldForkliftDetailsTemplate_postRender = function (element, contentItem) {
    var checkbox = $('<input type="checkbox" class="mycheckbox" style="float:right;height:45px;width:50" />')
    checkbox.click(function () {
        selectedUldsContainer = $('#selectedUldsContainer');
        selectedUldsContainer.find('div[uldId = ' + contentItem.value.Id + ' ]').remove();

        if (this.checked) {
            var item = $('<div></div>');
            item.attr('uldId', contentItem.value.Id);
            selectedUldsContainer.append(item);
        }
    });
    $(element).children().last().before(checkbox);
};

myapp.PutAwayPieces.UldForkliftViewPopup_postRender = function (element, contentItem) {
    $(element).css("overflow", "auto");
    var container = $(element);
    container.find('div:nth-child(2)').css('max-height', '400px');

    var footer = $('<div id="uldForkliftViewFooter" class="msls-clear msls-first-row  msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:38px; position:absolute; bottom:0"></div>');

    var buttonWrapper = $('<div style="float:right"></div>');
    var removeButton = $('<div class="msls-last-column  msls-presenter msls-ctl-button msls-vauto msls-hauto msls-leaf msls-presenter-content msls-font-style-normal msls-tap" style="min-width: 90px;"><a data-theme="a" data-mini="true" data-role="button" class="id-element ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-a" tabindex="0" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span"><span class="ui-btn-inner" style="padding:0 7px"><span class="ui-btn-text">Remove</span></span></a></div>');
    var selectLocationButton = $('<div class="msls-last-column  msls-presenter msls-ctl-button msls-vauto msls-hauto msls-leaf msls-presenter-content msls-font-style-normal msls-tap" style="min-width: 90px;"><a data-theme="a" data-mini="true" data-role="button" class="id-element ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-a" tabindex="0" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span"><span class="ui-btn-inner" style="padding:0 7px"><span class="ui-btn-text">Select Location</span></span></a></div>');

    selectLocationButton.click(function () {
        contentItem.screen.closePopup("UldForkliftViewPopup").then(function () {
            $sourcePopup = "UldForkliftViewPopup";
            $locationHolder = '#uldLocation';
            contentItem.screen.WarehouseLocations.selectedItem = null;
            contentItem.screen.showPopup("SelectLocationPopup");
        });
    });

    removeButton.click(function () {
        var sc = contentItem.screen;
        var aTimeout = sc.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        var arr = new Array();
        $('#selectedUldsContainer div').each(function () {
            arr.push($(this).attr('uldId'));
        });

        msls.showProgress(msls.promiseOperation(function (operation) {
            $.ajax({
                type: 'post',
                data: { 'items': JSON.stringify(arr) },
                url: '../Web/RemoveForkliftUlds.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    sc.ActiveForkliftView.refresh();
                    $('#selectedUldsContainer').empty();
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
            }

        }));
    });

    var selectedUldsContainer = $('<div id="selectedUldsContainer" style="display:none"></div>');

    var uldLocation = $('<span id="uldLocation" style="display:none"></span>');
    uldLocation.on('DOMNodeInserted', function () {
        if (uldLocation.text() && uldLocation.text() != '') {
            executeAssignLocationUld(contentItem);
        }
    });

    buttonWrapper.append(removeButton);
    buttonWrapper.append(selectLocationButton);

    footer.append(uldLocation);
    footer.append(selectedUldsContainer);
    footer.append(buttonWrapper);
    container.children().last().before(footer);
};

myapp.PutAwayPieces.ShowNotPutAway2_postRender = function (element, contentItem) {

    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};

myapp.PutAwayPieces.ShowPutAway2_postRender = function (element, contentItem) {

    $(element).on('touchstart', function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

    $(element).click(function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Put Away') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });

};

myapp.PutAwayPieces.SnapshotImage2_postRender = function (element, contentItem) {

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_camera'><img src='content/images/camera_icon_w.png' style='height:24px' /></div>");
    $(element).trigger("create");

};

myapp.PutAwayPieces.AwbForkliftViewHeader_postRender = function (element, contentItem) {

    element.textContent = "Forklift View";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.PutAwayPieces.EnteredQuantity1_postRender = function (element, contentItem) {
    $PutAwayQuantityInput1 = $("input", $(element));
};

myapp.PutAwayPieces.AWBPutAwayPopupHeader_postRender = function (element, contentItem) {

    element.textContent = "Put Away AWB Pieces";
    $(element).css("font-size", "23px");
    $(element).css("font-weight", "bold");

};

myapp.PutAwayPieces.AwbForkliftDetailsTemplate_postRender = function (element, contentItem) {
    enhanceForkliftViewItens(element, contentItem, '#selectedAwbsContainer', 'awbForkliftDeteilId', '#awbTtl', removeAwbsFromForklift, assignAwbLocation);
};

myapp.PutAwayPieces.AwbForkliftViewPopup_postRender = function (element, contentItem) {
    addFooterIntoPopup(element, contentItem, 'awbForkliftViewFooter', 'AwbForkliftViewPopup', '#awbSLocation', 'selectedAwbsContainer', 'awbTtl', 'awbSLocation', executeAwbAssignLocation);
};

myapp.PutAwayPieces.AWB_AWBSerialNumber_postRender = function (element, contentItem) {
    $(element.parentNode).css('width', '150px');
};

myapp.PutAwayPieces.AddToForkliftHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Add To Forklift";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");
};

myapp.PutAwayPieces.ForkliftViewPopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Forklift View";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.PutAwayPieces.ActiveForkliftViewTemplate_postRender = function (element, contentItem) {

    enhanceForkliftViewItens(element, contentItem, '#selectedAwbsContainer', 'awbForkliftDeteilId', '#awbTtl', removeAwbsFromForklift, assignAwbLocation);
    var d = contentItem.value || contentItem.data;
    assignLocationDefault(contentItem, d.Pieces);
};

myapp.PutAwayPieces.ForkliftViewPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'forkliftPopupContent');
    container.parent().center();

    container.css("overflow", "hidden");
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

    addFooterIntoPopup(element, contentItem, 'awbForkliftViewFooter', 'ForkliftViewPopup', '#awbSLocation', 'selectedAwbsContainer', 'awbTtl', 'awbSLocation', executeAwbAssignLocation);

};

myapp.PutAwayPieces.SelectedQuantity_postRender = function (element, contentItem) {

    $PutAwaySelQuantityInput = $("input", $(element));
    var $iControl = $(element).find("input");
    $iControl.attr("type", "text");
    $iControl.attr('readonly', 'readonly');
    $iControl.addClass("inlineTarget3");

};

myapp.PutAwayPieces.RefNumber_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.RefNumber11_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.PutAwayList_postRender = function (element, contentItem) {

    $(element).css("overflow", "hidden");

};

myapp.PutAwayPieces.Location_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.WarehouseLocationType_LocationType_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.ConfirmPopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.PutAwayPieces.ConfirmText_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.PutAwayPieces.RefNumber1_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.RefNumber12_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.PiecesLabel_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.Pieces2_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
};

myapp.PutAwayPieces.FinalizeText_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.PutAwayPieces.FinalizePopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.PutAwayPieces.AddToForklift_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var eType = iScrn.EntityType;
        var eId = iScrn.EntityId;
        var nFlt = iScrn.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cTask = iScrn.SelectedTask;
        if (!cTask) {
            cTask = $ActiveTask;
        }
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var tType;
        if (!cTask) {
            cTask = $ActiveTask;
        }
        var aTimeout = iScrn.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        switch (eType) {
            case "AWB":
                tType = 2;
                break;
            case "HWB":
                tType = 3;
                break;
            case "ULD":
                tType = 4;
                break;
            default:
                tType = 0;
                break;
        }

        var pQty = $(this).parent().parent().find('input').val();

        if (+pQty > +iScrn.MaxPutAwayPieces) {
            alert("Add to forklift quantity is greater than available quantity. \r\nInput quantity less than or equal to remaining quantity.");
        }
        else if (+pQty < 1) {
            alert("Add to forklift quantity should be greater than 0. \r\nInput quantity greater than 0.")
        }
        else {
            contentItem.screen.closePopup("AddToForkliftPopup").then(function () {
                msls.showProgress(msls.promiseOperation(function (operation) {
                    $.ajax({
                        type: 'post',
                        data: { 'eTypeId': tType, 'eId': eId, 'pQty': pQty, 'tId': cTask.Id, 'cUser': cUser },
                        url: '../Web/AddForkliftPieces.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            $('span#flCtr').text(+$('#flCtr').text() + +pQty);
                            iScrn.ForkLiftCount = $('#flCtr').text();
                            if (iScrn.findContentItem("FlightPutAwayList").isVisible == true) {
                                fillPutAwayList(iScrn, true, iScrn.SelectedFlt.Id, 1);
                            }
                            operation.complete(AjaxResult);

                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });

                }).then(function PromiseSuccess(PromiseResult) {

                    var result = PromiseResult;
                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }

                    }

                }));
            }, 1000);
        }
    });
};

myapp.PutAwayPieces.AddToForkliftPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'addToForkliftPopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.PutAwayPieces.ConfirmPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'confirmPopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.PutAwayPieces.FinalizePopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'finalisePopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.PutAwayPieces.FlightPutAwayList_postRender = function (element, contentItem) {
    $(element).css('overflow', 'auto');
};

myapp.PutAwayPieces.HistoryTab_postRender = function (element, contentItem) {
    $(element).css("overflow", "hidden");
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $($("span").filter(function () { return $(this).text() === "History" })).parent().on(evtName, function (e) {
        createHistory(contentItem.screen.SelectedFlt.Id, 1, true);
    });

};

myapp.PutAwayPieces.FlightHistory_postRender = function (element, contentItem) {
    $(element).css("overflow", "auto");
};

myapp.PutAwayPieces.WarehouseLocations_postRender = function (element, contentItem) {
    $(element).css("overflow", "auto");
};

myapp.PutAwayPieces.Reference1_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.Reference11_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");

};

myapp.PutAwayPieces.Count_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.PutAwayPieces.Location1_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.PutAwayPieces.FlightPutedAwayList_postRender = function (element, contentItem) {

    $(element).css('overflow', 'auto');

};

myapp.PutAwayPieces.SnapshotImage1_postRender = function (element, contentItem) {

    $iScrn5 = contentItem.screen;

    $(element).html("<div class='am_camera_big'><img src='content/images/camera_icon_w.png' style='height:36px' /></div>");
    $(element).trigger("create");

};

myapp.PutAwayPieces.LocationLabel_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "LOC: ";
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.PutAwayPieces.CountLabel_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "PCS: ";
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.PutAwayPieces.UnAssignLocationPopupHeader_postRender = function (element, contentItem) {

    unAssignHeader = element;
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Un-Assign Quantity";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.PutAwayPieces.UnAssignCount_postRender = function (element, contentItem) {

    $UnAssignCountInput = $("input", $(element));
    var $iControl = $(element).find("input");
    $iControl.attr("type", "text");
    $iControl.attr("readonly", "readonly");
    $iControl.addClass("inlineTarget4");

};

myapp.PutAwayPieces.UnAssignText_postRender = function (element, contentItem) {

    $('textarea').attr('readonly', 'readonly');
    $('textarea').addClass('un-assign-text');

};

myapp.PutAwayPieces.UnAssignULDText_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};

myapp.PutAwayPieces.UnAssignLocationPopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'unassignLocationPopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};

myapp.PutAwayPieces.UnAssignLocation_postRender = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var aTimeout = iScrn.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var sItem = iScrn.FlightPutedAwayList.selectedItem;
        if (!sItem) {
            sItem = $SelectedPutedAway;
        }
        var cUser = iScrn.CurrentUser;
        if (!cUser) {
            cUser = $CurrentUser;
        }
        var nFlt = contentItem.screen.SelectedFlt;
        if (!nFlt) {
            nFlt = $FlightManifest;
        }
        var cTask = contentItem.screen.SelectedTask;
        if (!cTask) {
            cTask = $ActiveTask;
        }
        var aTimeout = iScrn.AjaxTimeout;
        if (!aTimeout) {
            aTimeout = $AjaxTimeout;
        }
        switch (sItem.Type) {
            case "AWB":
                var uQty = $(this).parent().parent().find('input').val();

                if (+uQty > +contentItem.screen.MaxPutAwayPieces) {
                    alert("Un-assign quantity is greater than available quantity. \r\nInput quantity less than or equal to remaining quantity.");
                }
                else if (+uQty < 1) {
                    alert("Un-assign quantity should be greater than 0. \r\nInput quantity greater than 0.")
                }
                else {
                    msls.showProgress(msls.promiseOperation(function (operation) {

                        $.ajax({
                            type: 'post',
                            data: { 'eType': sItem.Type, 'eId': sItem.DispositionId, 'uQty': uQty, 'tId': cTask.Id, 'fId': nFlt.Id, 'cUser': cUser },
                            url: '../Web/UnAssignLocationToAwbs.ashx',
                            timeout: aTimeout,
                            success: operation.code(function AjaxSuccess(AjaxResult) {
                                operation.complete(AjaxResult);
                            }),
                            error: operation.code(function AjaxError(AjaxResult) {
                                AjaxResult = "Error: Unable to process request. \n\n" +
                                              "Try again later.";
                                operation.complete(AjaxResult);

                            })
                        });

                    }).then(function PromiseSuccess(PromiseResult) {
                        var result = PromiseResult;

                        if (result != null || result < 0) {
                            if (result.substring(0, 5) == "Error") {
                                alert(result);
                            }
                            else {
                                contentItem.screen.closePopup("UnAssignLocationPopup").then(function () {
                                    contentItem.screen.SelectedFlt.details.refresh();
                                    fillPutedAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
                                }, 1000);
                            }
                        }
                    }));
                }
                break;
            case "HWB":
                var uQty = $(this).parent().parent().find('input').val();

                if (+uQty > +contentItem.screen.MaxPutAwayPieces) {
                    alert("Un-assign quantity is greater than available quantity. \r\nInput quantity less than or equal to remaining quantity.");
                }
                else if (+uQty < 1) {
                    alert("Un-assign quantity should be greater than 0.  \r\nInput quantity greater than 0.")
                }
                else {

                    msls.showProgress(msls.promiseOperation(function (operation) {

                        $.ajax({
                            type: 'post',
                            data: { 'eType': sItem.Type, 'eId': sItem.DispositionId, 'uQty': uQty, 'tId': cTask.Id, 'fId': nFlt.Id, 'cUser': cUser },
                            url: '../Web/UnAssignLocationToAwbs.ashx',
                            timeout: aTimeout,
                            success: operation.code(function AjaxSuccess(AjaxResult) {
                                operation.complete(AjaxResult);
                            }),
                            error: operation.code(function AjaxError(AjaxResult) {
                                AjaxResult = "Error: Unable to process request. \n\n" +
                                              "Try again later.";
                                operation.complete(AjaxResult);

                            })
                        });

                    }).then(function PromiseSuccess(PromiseResult) {
                        var result = PromiseResult;
                        if (result != null || result < 0) {
                            if (result.substring(0, 5) == "Error") {
                                alert(result);
                            }
                            else {
                                contentItem.screen.closePopup("UnAssignLocationPopup").then(function () {
                                    
                                    contentItem.screen.SelectedFlt.details.refresh();
                                    fillPutedAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
                                }, 1000);
                            }
                        }
                    }));
                }
                break;
            case "ULD":

                msls.showProgress(msls.promiseOperation(function (operation) {

                    $.ajax({
                        type: 'post',
                        data: { 'eType': sItem.Type, 'eId': sItem.DispositionId, 'uQty': 1, 'tId': cTask.Id, 'fId': nFlt.Id, 'cUser': cUser },
                        url: '../Web/UnAssignLocationToAwbs.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });

                }).then(function PromiseSuccess(PromiseResult) {
                    var result = PromiseResult;
                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {
                            contentItem.screen.closePopup("UnAssignLocationPopup").then(function () {
                                contentItem.screen.SelectedFlt.details.refresh();
                                fillPutedAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);                              
                            }, 1000);
                        }
                    }
                }));
                break;
            default:
                tType = 0;
                break;
        }


    });

};

myapp.PutAwayPieces.Reset_postRender = function (element, contentItem) {
    $rButton = $(element);
};

myapp.PutAwayPieces.Filter_postRender = function (element, contentItem) {
    $ShipmentFilter = $("input", $(element));
    //$ShipmentFilter.keypad();
};

myapp.PutAwayPieces.AddToForkliftText_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};


///////////////////////////////////////
//             Functions
//////////////////////////////////////

function showForkliftView() {

    $iScrn5.ActiveForkliftView.refresh().then(function () {

        $('#forkliftPopupContent').parent().center();

        $iScrn5.showPopup("ForkliftViewPopup");

    });


};

//function showUldForkliftView() {

//    $(window).one("popupcreate", function (e) {

//        $(e.target).popup({

//            positionTo: "window"

//        });

//    });

//    $iScrn5.showPopup("UldForkliftViewPopup");

//};

//function showAwbForkliftView() {

//    $(window).one("popupcreate", function (e) {

//        $(e.target).popup({

//            positionTo: "window"

//        });

//    });

//    $iScrn5.showPopup("AwbForkliftViewPopup");

//};

function enhanceForkliftViewItens(element, contentItem, selectedContainerId, itemId, totalHolderId, removeAction, assignLocationAction) {
    var assignLocationImage = $('<img title="Assign Location" src="content/images/selectputawayicon.png" style="height: 65px;" />');
    var removeImage = $('<img title="Remove" src="content/images/remove-icon.png" style="height: 60px; margin-left: 32px; margin-top:27px" />');

    $(element).children().last().before(assignLocationImage);
    $(element).children().last().before(removeImage);

    removeImage.one('click', function (e) {
        return actionItemClicked(e, contentItem, 'Remove', '15px', false, selectedContainerId, itemId, totalHolderId, removeAction);
    });

    assignLocationImage.one('click', function (e) {
        return actionItemClicked(e, contentItem, 'Select', '0px', true, selectedContainerId, itemId, totalHolderId, assignLocationAction);
    });
};

function actionItemClicked(evt, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action) {
    var sender = $(evt.currentTarget);
    var mainDiv = sender.parent();

    var wrappaerDiv = $('<div style="display: none; float: right; height: 72px; position: relative; width: 448px; margin-top:0px;"></div>');
    var commentWrapper = $('<div style="height: 50px; position: relative; width: 380px; margin-top: ' + marginTop + '; display: inline-block; font-size:28px;"></div>');

    var firstComment = $('<span style="display: inline-block; margin-right:4px;">' + message + '</span>');
    var secondComment = $('<span style="margin-left:4px">pieces?</span>');
    var textbox = $('<input type="number" style="height: 28px; width: 80px;font-size:28px"></input>');

    var removeButton = $('<span title="Remove Pieces" style="position: absolute; display: inline-block; background-size: cover; height: 60px; background-position: -727px -1px; margin-top: 0px; width: 60px; margin-left: -62px;" class="ui-icon ui-icon-check ui-icon-shadow">&nbsp;</span>');
    var cancelButton = $('<span title="Cancel" style="position: absolute; display: inline-block; background-size: cover; height: 60px; margin-top: 0px; background-position: -253px -1px; width: 60px; margin-left: 16px;" class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span>');

    textbox.val(cntItem.value.Pieces);

    commentWrapper.append(firstComment);
    commentWrapper.append(textbox);
    commentWrapper.append(secondComment);

    if (needCountHolder) {
        removeButton.css('margin-top', '-15px');
        cancelButton.css('margin-top', '-15px');
        var countWrapper = $('<div style="padding-bottom: 5px; padding-top: 0; display:none;"></div>');
        var countHolderLabel = $('<span style="">Selected Pieces:</span>');
        var countHolder = $('<span style="" class="countHolder"></span>');
        var countRemover = $('<span title="Remove" style="display: inline-block; background-size: cover; height: 37px; width: 37px; background-position: -150px -4px;" class="ui-icon ui-icon-delete ui-icon-shadow">&nbsp;</span>');

        var itm = $('#selectedAwbsContainer').find('div[' + itemId + ' = ' + cntItem.value.PaId + ' ]').first();
        if (itm && itm.length >= 1) {
            countHolder.text(itm.attr('count'));
            countWrapper.css('display', 'inline-block');
        }

        countWrapper.append(countHolderLabel);
        countWrapper.append(countHolder);
        countWrapper.append(countRemover);
        countRemover.on('click', function () {
            $(this).parent().children('.countHolder').text('');
            $(this).parent().css('display', 'none');

            var selectedPiecesContainer = $(selectedContainerId);
            selectedPiecesContainer.find('div[' + itemId + ' = ' + cntItem.value.PaId + ' ]').remove();

            var totalCount = 0;
            selectedPiecesContainer.children().each(function () {
                totalCount += parseInt($(this).attr('count'));
            });

            $(totalHolderId).text(totalCount);
        });

        commentWrapper.append(countWrapper);
    }

    cancelButton.one('click', function () {
        restoreDefeultView(wrappaerDiv, sender, evt, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action);
    });

    removeButton.on('click', function () {
        action(cntItem, textbox.val(), countWrapper);
    });

    wrappaerDiv.append(commentWrapper);
    wrappaerDiv.append(removeButton);
    wrappaerDiv.append(cancelButton);


    mainDiv.children().last().before(wrappaerDiv);

    var elems = mainDiv.find('> div:first-child > div:not(:first-child), img');

    elems.each(function (idx) {
        $(this).fadeOut('slow', function () {
            if (idx == elems.length - 1) {
                wrappaerDiv.fadeIn('slow', function () {
                    wrappaerDiv.css('display', 'inline - block');
                });
            }
        });
    });
};

function restoreDefeultView(w, l, ie, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action) {
    w.fadeOut('slow', function () {
        var dvs = l.parent().find('> div:first-child > div:not(:first-child), img');
        dvs.each(function (idx) {
            $(this).fadeIn('slow');
        });
        $(this).remove();
        l.one('click', function () {
            return actionItemClicked(ie, cntItem, message, marginTop, needCountHolder, selectedContainerId, itemId, totalHolderId, action);
        });
    });
};

function assignLocation(cntItem, cnt, cntHolder) {

    cntHolder.children('.countHolder').text(cnt);
    cntHolder.css('display', '');

    var selectedPiecesContainer = $('#selectedPiecesContainer');

    selectedPiecesContainer.find('div[forkliftDeteilId = ' + cntItem.value.Id + ' ]').remove();

    var item = $('<div></div>');
    item.attr('forkliftDeteilId', cntItem.value.Id);
    item.attr('count', cnt);
    selectedPiecesContainer.append(item);

    var totalCount = 0;
    selectedPiecesContainer.children().each(function () {
        totalCount += parseInt($(this).attr('count'));
    });

    $('#ttl').text(totalCount);

};

function removePiecesFromForklift(cntItem, cnt, cntHolder) {
    var sc = cntItem.screen;
    var aTimeout = sc.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'ForkliftDetailId': cntItem.value.Id, 'Count': cnt },
            url: '../Web/RemoveForkliftPieces.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                sc.ActiveForkliftView.refresh();
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
        }

    }));
};

function executeHwbAssignLocation(cntItem) {
    var sc = cntItem.screen;

    var cUser = sc.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    var cTask = sc.SelectedTask;

    if (!cTask) {
        cTask = $ActiveTask;
    }

    var aTimeout = sc.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }

    var arr = new Array();
    $('#selectedPiecesContainer div').each(function () {
        arr.push({ forkliftDetailId: $(this).attr('forkliftdeteilid'), count: $(this).attr('count') });
    });

    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'locationId': sc.SelectedLocation.Id, 'taskId': cTask.Id, 'userName': cUser, 'items': JSON.stringify(arr) },
            url: '../Web/AssignLocationToPieces.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                sc.SelectedFlt.details.refresh();
                sc.ActiveForkliftView.refresh();
                $('#ttl').text('');
                $('#selectedPiecesContainer').empty();
                $('#sLocation').text('');
                operation.complete();
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                sc.ForkLiftCount = PromiseResult;
            }
        }
    }));
};

function addFooterIntoPopup(element, contentItem, footerId, popupName, locationHolderId, selectedContainerId, totalHolderId, selectedLocationId, executeAssignLocation) {
    var container = $(element);
    container.find('> div:nth-child(2)').css('max-height', '350px').css('overflow-y', 'auto').css('overflow-x', 'hidden');
    container.find('div:nth-child(2)').last().css('overflow', 'visible');

    var header = $('<div id=h"' + footerId + '" class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');
    var footer = $('<div id="' + footerId + '" class="popup-footer msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:80px; position:absolute; bottom:0"></div>');

    var buttonWrapper = $('<div style="float:right"></div>');
    var selectLocationButton = $('<div class="msls-last-column  msls-presenter msls-ctl-button msls-leaf msls-presenter-content msls-font-style-normal msls-tap" style="min-width: 90px;"><a data-theme="a" data-mini="true" data-role="button" class="id-element ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-up-a" tabindex="0" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span"><span class="ui-btn-inner" style="padding:0 7px"><span class="ui-btn-text">Select Location</span></span></a></div>');
    var sc = contentItem.screen;

    selectLocationButton.click(function () {
        sc.closePopup(popupName).then(function () {
            $sourcePopup = popupName;
            $locationHolder = locationHolderId;
            sc.WarehouseLocations.selectedItem = null;
            sc.showPopup("SelectLocationPopup");
        });
    });

    buttonWrapper.append(selectLocationButton);

    var selectedPiecesContainer = $('<div id="' + selectedContainerId + '" style="display:none"></div>');

    var wrapper = $('<div style="float: left; margin-top: 27px; font-size:16px"></div>');

    var piecesWrapper = $('<div></div>');
    var lable = $('<span>Total selected pieces: </span>');
    var total = $('<span id="' + totalHolderId + '"></span>');
    piecesWrapper.append(lable);
    piecesWrapper.append(total);

    var locationWrapper = $('<div></div>');
    var sLocation = $('<span id="' + selectedLocationId + '"></span>');
    locationWrapper.append(sLocation);
    sLocation.on('DOMNodeInserted', function () {
        if (sLocation.text() && sLocation.text() != '') {
            executeAwbAssignLocation(contentItem);
        }
    });

    wrapper.append(piecesWrapper);
    wrapper.append(locationWrapper);

    footer.append(wrapper);
    footer.append(selectedPiecesContainer);
    footer.append(buttonWrapper);
    container.children().first().before(header);
    container.children().last().before(footer);
};

function executeAssignLocationUld(cntItem) {
    var sc = cntItem.screen;

    var cUser = sc.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    var cTask = sc.SelectedTask;

    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = sc.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var arr = new Array();
    $('#selectedUldsContainer div').each(function () {
        arr.push($(this).attr('uldId'));
    });

    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'locationId': sc.SelectedLocation.Id, 'taskId': cTask.Id, 'userName': cUser, 'items': JSON.stringify(arr) },
            url: '../Web/AssignLocationToUlds.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                sc.SelectedFlt.details.refresh();
                sc.ActiveForkliftView.refresh();
                $('#selectedUldsContainer').empty();
                $('#uldLocation').text('');
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
        }

    }));
};

function removeAwbsFromForklift(cntItem, cnt, cntHolder) {
    var sc = cntItem.screen;
    var cUser = sc.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = sc.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'ForkliftDetailId': cntItem.value.Type + cntItem.value.EntityId, 'Count': cnt, 'cUser': cUser },
            url: '../Web/RemoveForkliftAwbs.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {

                var selectedPiecesContainer = $('#selectedAwbsContainer');
                selectedPiecesContainer.find('div[awbForkliftDeteilId = ' + cntItem.value.Type + cntItem.value.EntityId + ' ]').remove();

                sc.ActiveForkliftView.refresh();
                fillPutAwayList(sc, true, sc.SelectedFlt.Id, 1);
                fillPutedAwayList(sc, true, sc.SelectedFlt.Id, 1);                
                var totalCount = 0;
                selectedPiecesContainer.children().each(function () {
                    totalCount += parseInt($(this).attr('count'));
                });

                $('span#awbTtl').text(totalCount);

                $('span#flCtr').text(+$('#flCtr').text() - +cnt);
                sc.ForkLiftCount = sc.ForkLiftCount - cnt;
                if ($('#flCtr').text() == "0") {
                    sc.closePopup("ForkliftViewPopup");
                }

                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
        }

    }));
};

function assignLocationDefault(cntItem, cnt) {
    var selectedPiecesContainer = $('#selectedAwbsContainer');
    var data = cntItem.value || cntItem.data;
    selectedPiecesContainer.find('div[awbForkliftDeteilId = ' + data.Type + data.EntityId + ' ]').remove();

    var item = $('<div></div>');
    item.attr('awbForkliftDeteilId', data.Type + data.EntityId);
    item.attr('count', cnt);
    selectedPiecesContainer.append(item);

    var totalCount = 0;
    selectedPiecesContainer.children().each(function () {
        totalCount += parseInt($(this).attr('count'));
    });

    $('span#awbTtl').text(totalCount);
};

function assignAwbLocation(cntItem, cnt, cntHolder) {
    cntHolder.children('.countHolder').text(cnt);
    cntHolder.css('display', '');

    var selectedPiecesContainer = $('#selectedAwbsContainer');

    selectedPiecesContainer.find('div[awbForkliftDeteilId = ' + cntItem.value.Type + cntItem.value.EntityId + ' ]').remove();

    var item = $('<div></div>');
    item.attr('awbForkliftDeteilId', cntItem.value.Type + cntItem.value.EntityId);
    item.attr('count', cnt);
    selectedPiecesContainer.append(item);

    var totalCount = 0;
    selectedPiecesContainer.children().each(function () {
        totalCount += parseInt($(this).attr('count'));
    });

    $('span#awbTtl').text(totalCount);

};

function executeAwbAssignLocation(cntItem) {
    var sc = cntItem.screen;

    var cUser = sc.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    var cTask = sc.SelectedTask;

    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = sc.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var arr = new Array();
    $('#selectedAwbsContainer div').each(function () {
        arr.push({ forkliftDetailId: $(this).attr('awbForkliftDeteilId'), count: $(this).attr('count') });
    });
    var total = $('span#awbTtl').last().text();
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: { 'locationId': sc.SelectedLocation.Id, 'taskId': cTask.Id, 'userName': cUser, 'items': JSON.stringify(arr) },
            url: '../Web/AssignLocationToAwbs.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {

                sc.SelectedFlt.details.refresh();
                sc.ActiveForkliftView.refresh();
                $('span#awbTtl').text('');
                $('#selectedAwbsContainer').empty();
                $('#awbSLocation').text('');
                $('span#flCtr').text(+$('#flCtr').text() - +total);
                if ($('#flCtr').text() == "0") {
                    sc.closePopup("ForkliftViewPopup");
                }
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });
    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                sc.ForkLiftCount = result;
                fillPutAwayList(sc, true, sc.SelectedFlt.Id, 1, function (pap, ttl, percent) {
                    $('#papProgressBar p').text(pap + ' of ' + ttl);
                    $('#papProgressBar div').css('width', percent);
                });
            }
        }
    }));
};

function refreshView5() {

    var cUser = $iScrn5.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    var cTask = $iScrn5.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }

    var aTimeout = $iScrn5.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: { 'tId': cTask.Id, 'cUser': cUser },
            url: '../Web/GetForkliftPieceCount.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                $iScrn5.ForkLiftCount = result;
                $iScrn5.SelectedFlt.details.refresh();

                if ($iScrn5.findContentItem("FlightPutAwayList").isVisible == true) {
                    fillPutAwayList($iScrn5, true, $iScrn5.SelectedFlt.Id, 1);

                }

                if ($iScrn5.findContentItem("FlightPutedAwayList").isVisible == true) {

                    fillPutedAwayList($iScrn5, true, $iScrn5.SelectedFlt.Id, 1);
                }

            }
        }
    }));
};
myapp.PutAwayPieces.PopupKeypad_render = function (element, contentItem) {

    $(element).attr("id", "inlineTargetKeypad3");
    $(element).addClass("keypad-inline");
    $('div#inlineTargetKeypad3').keypad({
        target: $('.inlineTarget3:visible'),
        layout: ['123', '456', '789', $.keypad.SPACE + '0' + $.keypad.BACK]
    });

    var keypadTarget = null;
    $('input.inlineTarget3').focus(function () {
        if (keypadTarget != this) {
            keypadTarget = this;
            $('div#inlineTargetKeypad3').keypad('option', { target: this });
        }
    });

};
myapp.PutAwayPieces.UpdatePopupHeader_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.PutAwayPieces.UpdateText_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.PutAwayPieces.UpdatePopup_postRender = function (element, contentItem) {

    var container = $(element);
    container.attr('id', 'updatePopupContent');
    container.parent().center();

    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

    container.css("z-index", "999999999999");
};
myapp.PutAwayPieces.FlightHistoryTemplate_postRender = function (element, contentItem) {

    $("div.msls-text").removeClass("msls-text");

};
myapp.PutAwayPieces.PopupKeypad1_render = function (element, contentItem) {

    $(element).attr("id", "inlineTargetKeypad4");
    $(element).addClass("keypad-inline");

    $('div#inlineTargetKeypad4').keypad({
        target: $('.inlineTarget4:visible'),
        layout: ['123', '456', '789', $.keypad.SPACE + '0' + $.keypad.BACK]
    });

    var keypadTarget = null;
    $('input.inlineTarget4').focus(function () {
        if (keypadTarget != this) {
            keypadTarget = this;
            $('div#inlineTargetKeypad4').keypad('option', { target: this });
        }
    });

};
myapp.PutAwayPieces.UpdateOk_postRender = function (element, contentItem) {
    $(element).on('touchstart', function (e) {
        contentItem.screen.closePopup("UpdatePopup");
    });

    $(element).on('click', function () {
        contentItem.screen.closePopup("UpdatePopup");
    });
};

myapp.PutAwayPieces.FlightPutAwayList_render = function (element, contentItem) {
    var ul = $('<ul id="putAwayUnorderedList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.PutAwayPieces.listLoadPageNumber = 2;
    $(element).on('scroll', function () {        
        if ($('ul#putAwayUnorderedList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            console.log(myapp.PutAwayPieces.listLoadPageNumber);
            fillPutAwayList(contentItem.screen, false, contentItem.screen.SelectedFlt.Id, myapp.PutAwayPieces.listLoadPageNumber);
            myapp.PutAwayPieces.listLoadPageNumber = myapp.PutAwayPieces.listLoadPageNumber + 1;
        }
    });

    fillPutAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
};

myapp.PutAwayPieces.FlightPutedAwayList_render = function (element, contentItem) {
    var ul = $('<ul id="putedAwayUnorderedList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.PutAwayPieces.putedListLoadPageNumber = 2;
    $(element).on('scroll', function () {
        if ($('ul#putedAwayUnorderedList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            console.log(myapp.PutAwayPieces.putedListLoadPageNumber);
            fillPutedAwayList(contentItem.screen, false, contentItem.screen.SelectedFlt.Id, myapp.PutAwayPieces.putedListLoadPageNumber);
            myapp.PutAwayPieces.putedListLoadPageNumber = myapp.PutAwayPieces.putedListLoadPageNumber + 1;
        }
    });

    fillPutedAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
};

function fillPutedAwayList(screen, clearList, flightId, pageNumber, callbackFunc) {
    $.ajax({
        type: 'GET',
        data: { 'flightId': flightId, 'rowPerPage': 6, 'pageNumber': pageNumber, 'refNumber': $('input#refFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetPutedAwayList',
        success: function (data) {
            $('span#noItems').remove();
            var list = $('ul#putedAwayUnorderedList').last();
            if (clearList) {
                list.empty();
                myapp.PutAwayPieces.putedListLoadPageNumber = 2;
            }
            $(data).each(function () {
                var li = $('<li class="custom-list-item"></li>');
                var that = this;
                li.on('click', function () {
                    if (!screen.FlightPutedAwayList) {
                        screen.FlightPutedAwayList = {};
                    }
                    screen.FlightPutedAwayList.selectedItem = that;
                    myapp.PutAwayPieces.ShowUnAssignLocationPopup_execute(screen);                    
                });


                var statusImage = $('<img class="float-left" style="width: 30px;"></img>');
                statusImage.attr('src', this.StatusImagePath);

                var refContainer = $('<div class="ref-container"></div>');
                var refNumber = $('<div>' + this.Reference + '</div>');
                var refNumber1 = $('<div style="min-height:36px;">' + this.Reference1 + '</div>');                

                var locationContainer = $('<div style="font-size:22px"></div>');
                var location = $('<span style="margin-right:15px">LOC:' + this.Location + '</span>');
                var pieces = $('<span>PCS:' + this.Count + '</span>');
                locationContainer.append(location);
                locationContainer.append(pieces);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(locationContainer);

                var imageContainer = $('<div class="custom-image-container"></div>');                                              

                var snapshotButton = $('<div class="custom-snapshot-button"></div>');
                var snapshotImage = $('<img class="float-left" src="content/images/camera_icon_w.png"></img>');
                snapshotButton.append(snapshotImage);
                snapshotButton.on('click', function (e) {
                    e.stopPropagation();
                    if (!screen.FlightPutedAwayList) {
                        screen.FlightPutedAwayList = {};
                    }
                    screen.FlightPutedAwayList.selectedItem = that;                    
                    myapp.PutAwayPieces.ShowSnapshot1_execute(screen);
                });
                
                imageContainer.append(snapshotButton);

                li.append(statusImage);
                li.append(refContainer);
                li.append(imageContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="noItems">No Items</span>')
            }

            if (callbackFunc) {                
                    callbackFunc();                
            }
        },

        error: function (e) {
            console.log('Unable get date from GetPutAwayList.' + e);
        }
    })
}

function fillPutAwayList(screen, clearList, flightId, pageNumber, callbackFunc) {
    $.ajax({
        type: 'GET',
        data: { 'flightId': flightId, 'rowPerPage': 6, 'pageNumber': pageNumber, 'refNumber': $('input#refFilter').last().val() },
        contentType: 'application/json',        
        url: '../api/cargoreceiver/GetPutAwayList',
        success: function (data) {
            $('span#noItems').remove();
            var list = $('ul#putAwayUnorderedList').last();
            if (clearList) {
                list.empty();
                myapp.PutAwayPieces.listLoadPageNumber = 2;
            }
            
            $(data).each(function () {
                var li = $('<li class="custom-list-item"></li>');
                var that = this;
                li.on('click', function () {
                    if (!screen.FlightPutAwayList){
                        screen.FlightPutAwayList = {};
                    }
                    
                    screen.FlightPutAwayList.selectedItem = that;
                    myapp.PutAwayPieces.ShowAddToForkliftPopup_execute(screen);
                });


                var statusImage = $('<img class="float-left" style="width: 30px;"></img>');
                statusImage.attr('src', this.StatusImagePath);

                var refContainer = $('<div class="ref-container"></div>');
                var refNumber = $('<div>' + this.RefNumber + '</div>');
                var refNumber1 = $('<div style="min-height:36px;">' + this.RefNumber1 + '</div>');
                var progressBar = $('<div class="custom-progressbar"></div>');
                var progressBarCountHolder = $('<div class="custom-progressbar-count-holder">' + this.PutAwayPieces + ' of ' + this.TotalPieces + '</div>');
                var progressBarIndicator = $('<div class="custom-progressbar-indicator"></div>');
                progressBarIndicator.css('width', this.PercentPutAway + '%');
                progressBar.append(progressBarCountHolder);
                progressBar.append(progressBarIndicator);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(progressBar);

                var imageContainer = $('<div class="custom-image-container"></div>');

                var forkliftButton = $('<div class="custom-forklift-button"></div>');
                var forkliftImage = $('<img class="float-left" src="content/images/recoveryinprogress_icon.png"></img>');

                var forkliftCounter = $('<div class="custom-forklift-counter">' + this.ForkliftPieces + '</div>');
                forkliftButton.append(forkliftImage);
                forkliftButton.append(forkliftCounter);

                var snapshotButton = $('<div class="custom-snapshot-button"></div>');
                var snapshotImage = $('<img class="float-left" src="content/images/camera_icon_w.png"></img>');
                snapshotButton.append(snapshotImage);
                snapshotButton.on('click', function (e) {
                    e.stopPropagation();
                    if (!screen.FlightPutAwayList) {
                        screen.FlightPutAwayList = {};
                    }
                    screen.FlightPutAwayList.selectedItem = that;
                    myapp.PutAwayPieces.ShowSnapshot_execute(screen);
                });

                imageContainer.append(forkliftButton);
                imageContainer.append(snapshotButton);

                li.append(statusImage);
                li.append(refContainer);
                li.append(imageContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="noItems">No Items</span>')
            }

            if (data[0]) {
                $('#papProgressBar p').text(data[0].FlightPutAwayPieces + ' of ' + data[0].FlightTotalPieces);
                $('#papProgressBar div').css('width', data[0].FlightPercentPutAway);
            }

            if (callbackFunc) {
                if (data[0]) {
                    callbackFunc(data[0].FlightPutAwayPieces, data[0].FlightTotalPieces, data[0].FlightPercentPutAway);
                }
            }
        },

        error: function (e) {
            console.log('Unable get date from GetPutAwayList.' + e);
        }
    })
}
myapp.PutAwayPieces.RefFilter_render = function (element, contentItem) {
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="refFilter" style="margin-left:3px;"></input>');    
    elem.append(filter);
};
myapp.PutAwayPieces.Search1_execute = function (screen) {
    createHistory(screen.SelectedFlt.Id, 1, true);
    //if ($('#historyFilterTextbox5').val()) {
    //    screen.SearchHistoryFilter = $('#historyFilterTextbox5').val();
    //}
    //else {
    //    screen.SearchHistoryFilter = null;
    //}
};
myapp.PutAwayPieces.Search_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        fillPutAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
        fillPutedAwayList(contentItem.screen, true, contentItem.screen.SelectedFlt.Id, 1);
    });
    
};
myapp.PutAwayPieces.HistoryFilter_postRender = function (element, contentItem) {
    
    $(element).find('input').attr('id', 'historyFilterTextbox5');

};
myapp.PutAwayPieces.Search1_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.PutAwayPieces.Search1_execute(contentItem.screen);
    });

};
myapp.PutAwayPieces.Search2_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        myapp.PutAwayPieces.Search2_execute(contentItem.screen);
    });

};
myapp.PutAwayPieces.Search2_execute = function (screen) {
    
    if ($('#locationFilterTextbox5').val()) {
        screen.SearchLocationFilter = $('#locationFilterTextbox5').val();
    }
    else {
        screen.SearchLocationFilter = null;
    }

};
myapp.PutAwayPieces.FlightHistory_render = function (element, contentItem) {    
    var table = $('<table id="putAwayHistory" class="msls-table ui-responsive table-stripe msls-hstretch ui-table ui-table-reflow">' +
                    '<thead>' +
                        '<tr>' +
                            '<td style="width:50px; font-weight:bold">Date</td>' +
                            '<td style="width:125px; font-weight:bold">Reference</td>' +
                            '<td style="width:125px; font-weight:bold">Description</td>' +
                            '<td style="width:100px; font-weight:bold">User</td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody id="putAwayHistoryBody"></tbody>' +
                   '</table>');
    $(element).append(table);
    myapp.PutAwayPieces.historyLoadPageNumber = 2;    
    $(element).on('scroll', function () {
        if ($('table#putAwayHistory').last().height() === $(this).scrollTop() + $(this).height() + 6) {            
            createHistory(contentItem.screen.SelectedFlt.Id, myapp.PutAwayPieces.historyLoadPageNumber, false);
            myapp.PutAwayPieces.historyLoadPageNumber = myapp.PutAwayPieces.historyLoadPageNumber + 1;
        }
    });

    createHistory(contentItem.screen.SelectedFlt.Id, 1, true);
};

function createHistory(flightId, pageNumber, clearList) {
    $.ajax({
        type: 'get',
        data: { 'flightManifestId': flightId, 'filter': $('#historyFilterTextbox5').length ? $('#historyFilterTextbox5').last().val() : '', 'rowsPerPage': 20, 'pageNumber': pageNumber },
        url: '../api/cargoreceiver/GetFlightHistory',
        success: function (data) {                        
            var tbody = $('tbody#putAwayHistoryBody').last();

            if (clearList) {
                tbody.empty();
                myapp.PutAwayPieces.historyLoadPageNumber = 2;
            }

            $(data).each(function () {
                tbody.append('<tr class="msls-tr msls-style ui-shadow ui-tr msls-presenter msls-ctl-table-row-layout msls-vauto msls-hstretch msls-presenter-content msls-font-style-normal msls-hscroll msls-table-row">' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Date + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Reference + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Description + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.UserName + '</td>' +
                             '</tr>');
            });            
        },
        error: function (e) {
            console.log('Error History: ' + e);
        }
    });
}
myapp.PutAwayPieces.HistoryFilter_render = function (element, contentItem) {
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="historyFilterTextbox5" style="margin-left:3px;"></input>');
    elem.append(filter);
};