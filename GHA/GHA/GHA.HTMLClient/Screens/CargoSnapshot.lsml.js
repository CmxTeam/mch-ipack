﻿var $iScrn;
var $soundFX;

myapp.CargoSnapshot.created = function (screen) {
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    screen.CompletedFlights = false;
    msls.showProgress(msls.promiseOperation(function (operation) {
        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);
            })
        });
    }).then(function PromiseSuccess(PromiseResult) {
        var result = PromiseResult;
        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                var uItems = result.split(',');
                if (uItems.length > 2) {
                    screen.CurrentUser = uItems[0];
                    var sId = +uItems[1];
                    myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {
                        screen.CurrentUserStation = selectedStation.results[0];
                    });
                    var wId = +uItems[2];
                    myapp.activeDataWorkspace.WFSDBData.Warehouses_SingleOrDefault(+wId).execute().then(function (selectedWarehouse) {
                        screen.CurrentUserWarehouse = selectedWarehouse.results[0];
                    });
                }
                else {
                    screen.CurrentUser = uItems[0];
                }
            }
        }
    }));
};

myapp.CargoSnapshot.SoundElement_render = function (element, contentItem) {
    var htmlcde = '<audio id="soundFX">' +
              '<source src="Content/Sounds/click.ogg"></source>' +
              'Update your browser to enjoy HTML5 audio!' +
              '</audio>';
    $(element).append(htmlcde);
    $soundFX = $('#soundFX');
};

myapp.CargoSnapshot.GoToHome_execute = function (screen) {
    $soundFX[0].play();
    myapp.navigateHome();
};

myapp.CargoSnapshot.RefreshButton_render = function (element, contentItem) {
    $iScrn = contentItem.screen;
    $(element).html("<div class='am_refresh' style='padding-left: 16px;' onclick='refreshView7()' ><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    $(element).trigger("create");
};

function refreshView7() {
    $iScrn.SnapshotTasks.refresh();
};

myapp.CargoSnapshot.Reset_execute = function (screen) {
    screen.Filter = null;
    screen.EntityTypeSelected = null;
    resetEntityTypes();
    screen.SnapshotTasks.refresh();
};

function resetEntityTypes(){
    $('.entityTypes').each(function () {
        var span = $(this).find('a span.ui-btn-text');
        $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    });
}

myapp.CargoSnapshot.Incomplete_postRender = function (element, contentItem) {
    var a = $(element).find('a').first();
    a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Complete') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });
};

myapp.CargoSnapshot.Complete_postRender = function (element, contentItem) {
    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Incomplete') {
                $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
            }
        });
    });
};

myapp.CargoSnapshot.Incomplete_execute = function (screen) {
    screen.Filter = null;
    screen.CompletedFlights = false;
};

myapp.CargoSnapshot.Complete_execute = function (screen) {
    screen.Filter = null;
    screen.CompletedFlights = true;
};

myapp.CargoSnapshot.HWB_execute = function (screen) {
    screen.EntityTypeSelected = "HWB";
    screen.SnapshotTasks.refresh();
};

myapp.CargoSnapshot.AWB_execute = function (screen) {
    screen.EntityTypeSelected = "AWB";
    screen.SnapshotTasks.refresh();
};

myapp.CargoSnapshot.ULD_execute = function (screen) {
    screen.EntityTypeSelected = "ULD";
    screen.SnapshotTasks.refresh();
};

myapp.CargoSnapshot.HWB_postRender = function (element, contentItem) {
    $(element).addClass('entityTypes');

    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
        });
    });
};

myapp.CargoSnapshot.AWB_postRender = function (element, contentItem) {
    $(element).addClass('entityTypes');

    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
        });
    });
};

myapp.CargoSnapshot.ULD_postRender = function (element, contentItem) {
    $(element).addClass('entityTypes');

    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var a = $(this).find('a').first();
        a.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            $(this).find('a').first().css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
        });
    });
};

myapp.CargoSnapshot.PiecesLabel_postRender = function (element, contentItem) {
    element.textContent = "PCS:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.CargoSnapshot.WeightLabel_postRender = function (element, contentItem) {
    element.textContent = "WGT:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.CargoSnapshot.ConditionLabel_postRender = function (element, contentItem) {
    element.textContent = "COND:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.CargoSnapshot.LocationLabel_postRender = function (element, contentItem) {
    element.textContent = "LOC:";
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
};

myapp.CargoSnapshot.Pieces_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");
};

myapp.CargoSnapshot.Weight_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");
};

myapp.CargoSnapshot.Condition_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");
};

myapp.CargoSnapshot.WarehouseLocation_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "20px");
    $(element).css("margin-top", "2px");
};

myapp.CargoSnapshot.RefNumber_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
};

myapp.CargoSnapshot.RefNumber1_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
};

myapp.CargoSnapshot.SnapshotImage_postRender = function (element, contentItem) {
    $(element).html("<div class='am_camera_big'><img src='content/images/camera_icon_w.png' style='height:36px' /><span id='snCnt'>" + contentItem.data.SnapshotCount + "</span></div>");
    $(element).trigger("create");
};

myapp.CargoSnapshot.ConfirmPopup_postRender = function (element, contentItem) {
    var container = $(element);
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');
    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");
};

myapp.CargoSnapshot.ConfirmPopupHeader_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");
};

myapp.CargoSnapshot.ConfirmText_postRender = function (element, contentItem) {
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");
};

myapp.CargoSnapshot.SnapshotImage_Tap_execute = function (screen) {
    var taskStatus = screen.SnapshotTasks.selectedItem.TaskStatus;

    if (taskStatus == "Snapshot Complete") {

        screen.ConfirmPopupHeader = "Snapshot Is Complete";
        screen.ConfirmText = "Would you like to capture additional images?";

        $(window).one("popupcreate", function (e) {
            $(e.target).popup({
                positionTo: "window"
            });
        });
        screen.showPopup("ConfirmPopup");
    }
    else if (taskStatus == "Snapshot In Progress") {
        // TODO ANI: Open the snapshot gallery
    }
};

myapp.CargoSnapshot.ConfirmYes_execute = function (screen) {
    switch (screen.ConfirmPopupHeader) {
        case "Snapshot Is Complete":
            // TODO ANI: reopen the task and go to Gallery display screen
            break;
    }
};

myapp.CargoSnapshot.ConfirmNo_execute = function (screen) {
    screen.closePopup("ConfirmPopup");
};

myapp.CargoSnapshot.Group_postRender = function (element, contentItem) {
    
    $(element).css("overflow", "hidden");

};
