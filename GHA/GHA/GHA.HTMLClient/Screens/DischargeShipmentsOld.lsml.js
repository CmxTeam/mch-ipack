﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.DischargeShipmentsOld.Reset_execute = function (screen) {
    
    screen.Filter = null;

};

myapp.DischargeShipmentsOld.Reset1_execute = function (screen) {

    screen.Filter1 = null;

};

myapp.DischargeShipmentsOld.CreateAWBDO_execute = function (screen) {
    
    var sAWB = screen.AWBs.selectedItem;
    var sDischarge = screen.SelectedDischarge;

    myapp.showDeliveryOrderDetails(null, sAWB, sDischarge, {
        beforeShown: function (dScreen) {

            var nDischargeDetail = new myapp.DischargeDetail();
            nDischargeDetail.Discharge = sDischarge;
            nDischargeDetail.HWB = sAWB;
            dScreen.SelectedDischargeDetail = nDischargeDetail;

        },
        afterClosed: function () {

            screen.Filter = null;
            screen.Filter1 = null;

        }


    });

};

myapp.DischargeShipmentsOld.CreateHWBDO_execute = function (screen) {
    
    var sHWB = screen.HWBs.selectedItem;
    var sDischarge = screen.SelectedDischarge;

    myapp.showDeliveryOrderDetails(sHWB, null, sDischarge, {
        beforeShown: function (dScreen) {

            var nDischargeDetail = new myapp.DischargeDetail();
            nDischargeDetail.Discharge = sDischarge;
            nDischargeDetail.HWB = sHWB;
            dScreen.SelectedDischargeDetail = nDischargeDetail;

        },
        afterClosed: function () {

            screen.Filter = null;
            screen.Filter1 = null;

        }
    });

};

myapp.DischargeShipmentsOld.created = function (screen) {
    // Write code here.

};