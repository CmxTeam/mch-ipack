﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $bcParser;

myapp.BarcodeParser.BarcodeScanText_render = function (element, contentItem) {

    var iScrn = contentItem.screen;
    var bcText = contentItem.value;

    var aTimeout = iScrn.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var cTask = iScrn.SelectedInventoryTask;
    var chars = [];
    var ctr = "0";
    var result = "";

    if (bcText == null) {
        bcText = "";
    }

    $(document).off('keypress').on('keypress', function (event) {

        var charCode = event.which || event.keyCode;

        if (ctr == "1" && charCode != 126) {
            chars.push(String.fromCharCode(charCode));
        }

        if (charCode == 126) {
            if (ctr == "0") {
                ctr = "1";
            }
            else {
                ctr = "2";
            }

        }

        if (ctr == "2") {
            bcText = chars.join("");

            if (bcText == null || bcText == "") {
                alert("Error: Missing Barcode Details");
            }
            else {

                $('#bcListener').val("Retrieving Details...");

                msls.showProgress(msls.promiseOperation(function (operation) {

                    $.ajax({
                        type: 'post',
                        data: { 'bcScan': bcText },
                        url: '../Web/ScanBarcodeParser.ashx',
                        timeout: aTimeout,
                        success: operation.code(function AjaxSuccess(AjaxResult) {
                            operation.complete(AjaxResult);
                        }),
                        error: operation.code(function AjaxError(AjaxResult) {
                            AjaxResult = "Error: Unable to process request. \n\n" +
                                          "Try again later.";
                            operation.complete(AjaxResult);

                        })
                    });

                }).then(function PromiseSuccess(PromiseResult) {

                    var result = PromiseResult;

                    if (result != null || result < 0) {
                        if (result.substring(0, 5) == "Error") {
                            alert(result);
                        }
                        else {

                            var rItems = result.split(',');

                            switch (rItems[0]) {

                                case "L": //Location Scan

                                    myapp.activeDataWorkspace.WFSDBData.WarehouseLocations_SingleOrDefault(rItems[1]).execute().then(function (selectedLoc) {

                                        var returnedLoc = selectedLoc.results[0];

                                        $('#bcListener').val("Location Scan \r\nBarcode: " + bcText + "\r\nId: " + rItems[1] + "\r\n" + rItems[2]);

                                        chars = [];
                                        ctr = "0";
                                        bcText = "";
                                        event = null;
                                        result = null;

                                    });

                                    break;

                                case "A": //AWB Scan Received at AWB Level
                                    $('#bcListener').val("AWB Scan \r\nBarcode: " + bcText + "\r\nId: " + rItems[1] + "\r\n" + rItems[2]);
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;


                                    break;

                                case "H": // HWB Scan Received at HWB Level
                                    $('#bcListener').val("HWB Scan \r\nBarcode: " + bcText + "\r\nId: " + rItems[1] + "\r\n" + rItems[2]);
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                    break;

                                case "F": // Scan Details Not Found

                                    $('#bcListener').val("Invalid Scan \r\nBarcode: " + bcText);
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                    break;

                                default: // Scan Details Not Found
                                    $('#bcListener').val("Invalid Scan \r\nBarcode: " + bcText);
                                    chars = [];
                                    ctr = "0";
                                    bcText = "";
                                    event = null;
                                    result = null;

                                    break;

                            }
                        }
                    }
                }));

            }

        }

    });
};
myapp.BarcodeParser.Listener_render = function (element, contentItem) {
    
    $bcParser = $(element);

    var hCode = '<div><textarea id="bcListener" name="bcListener" style="width:250px; height:250px;" readonly="readonly"></textarea></div>';

    $(element).append(hCode);

};