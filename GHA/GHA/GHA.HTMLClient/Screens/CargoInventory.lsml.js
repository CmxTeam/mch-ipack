﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

var $InventoryNameInput;

myapp.CargoInventory.created = function (screen) {
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    msls.showProgress(msls.promiseOperation(function (operation) {

        $.ajax({
            type: 'post',
            data: {},
            url: '../Web/LoggedUser.ashx',
            timeout: aTimeout,
            success: operation.code(function AjaxSuccess(AjaxResult) {
                operation.complete(AjaxResult);
            }),
            error: operation.code(function AjaxError(AjaxResult) {
                AjaxResult = "Error: Unable to process request. \n\n" +
                              "Try again later.";
                operation.complete(AjaxResult);

            })
        });

    }).then(function PromiseSuccess(PromiseResult) {

        var result = PromiseResult;

        if (result != null || result < 0) {
            if (result.substring(0, 5) == "Error") {
                alert(result);
            }
            else {
                var uItems = result.split(',');
                if (uItems.length > 1) {

                    screen.CurrentUser = uItems[0];
                    var sId = +uItems[1];
                    myapp.activeDataWorkspace.WFSDBData.Ports_SingleOrDefault(+sId).execute().then(function (selectedStation) {

                        screen.CurrentUserStation = selectedStation.results[0];

                    });

                }
                else {

                    screen.CurrentUser = uItems[0];
                }
            }
        }
    }));
};

myapp.CargoInventory.InventoryNameLabel_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    element.textContent = "Inventory Name";
    $(element).css("font-size", "36px");
    $(element).css("line-height", "40px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};

myapp.CargoInventory.ShowInventoryNamePopup_execute = function (screen) {
    
    $(window).one("popupcreate", function (e) {

        $(e.target).popup({

            positionTo: "window"

        });

    });

    screen.showPopup("InventoryRefPopup").then(function () {
        setTimeout(function () {
            $InventoryNameInput.focus();
            $InventoryNameInput.select();
        }, 1);
    });

};

myapp.CargoInventory.InventoryName_postRender = function (element, contentItem) {
    
    $InventoryNameInput = $("input", $(element));

};

myapp.CargoInventory.SkipName_execute = function (screen) {

    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    var cUser = screen.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }

    screen.closePopup("InventoryRefPopup").then(function () {
        msls.showProgress(msls.promiseOperation(function (operation) {
            $.ajax({
                type: 'post',
                data: { 'InvName': '', 'cUser': cUser},
                url: '../Web/AddNewInventoryTask.ashx',
                timeout: aTimeout,
                success: operation.code(function AjaxSuccess(AjaxResult) {
                    operation.complete(AjaxResult);
                }),
                error: operation.code(function AjaxError(AjaxResult) {
                    AjaxResult = "Error: Unable to process request. \n\n" +
                                  "Try again later.";
                    operation.complete(AjaxResult);

                })
            });
        }).then(function PromiseSuccess(PromiseResult) {

            var result = PromiseResult;

            if (result != null || result < 0) {
                if (result.substring(0, 5) == "Error") {
                    alert(result);
                }
                else {
                    if (result == "0") {
                        msls.showMessageBox("Unable to create new inventory task");
                    }
                    else {
                        screen.InventoryTasks.refresh();
                    }
                }
            }
        }));
    });
};

myapp.CargoInventory.CancelInventoryTask_execute = function (screen) {
    
    screen.closePopup("InventoryRefPopup");

};

myapp.CargoInventory.CreateWithName_postRender = function (element, contentItem) {
    
    var iScrn = contentItem.screen;
    var evtName = 'click';
    var cUser = iScrn.CurrentUser;
    if (!cUser) {
        cUser = $CurrentUser;
    }
    var aTimeout = iScrn.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {

        var iName = $(this).parent().parent().find('input').val();
        iScrn.closePopup("InventoryRefPopup").then(function () {
            msls.showProgress(msls.promiseOperation(function (operation) {
                $.ajax({
                    type: 'post',
                    data: { "InvName": iName, 'cUser': cUser },
                    url: '../Web/AddNewInventoryTask.ashx',
                    timeout: aTimeout,
                    success: operation.code(function AjaxSuccess(AjaxResult) {
                        operation.complete(AjaxResult);
                    }),
                    error: operation.code(function AjaxError(AjaxResult) {
                        AjaxResult = "Error: Unable to process request. \n\n" +
                                      "Try again later.";
                        operation.complete(AjaxResult);

                    })
                });
            }).then(function PromiseSuccess(PromiseResult) {

                var result = PromiseResult;

                if (result != null || result < 0) {
                    if (result.substring(0, 5) == "Error") {
                        alert(result);
                    }
                    else {
                        if (result == "0") {
                            msls.showMessageBox("Unable to create new inventory task");
                        }
                        else {
                            iScrn.InventoryTasks.refresh();
                        }
                    }
                }
            }));
        }, 1000);

    });

};

myapp.CargoInventory.TaskReference_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "40px");

};

myapp.CargoInventory.TaskCreationDate_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "26px");
    $(element).css("line-height", "30px");

};

myapp.CargoInventory.GoToInventoryTask_execute = function (screen) {
    
    var iTask = screen.InventoryTasks.selectedItem;
    var cUser = screen.CurrentUser;
    var cStation = screen.CurrentUserStation;
    var aTimeout = screen.AjaxTimeout;

    myapp.showInventoryTask(iTask, cUser, cStation, aTimeout);

};
myapp.CargoInventory.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};