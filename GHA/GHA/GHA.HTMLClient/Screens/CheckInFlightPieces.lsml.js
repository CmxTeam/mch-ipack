﻿/// <reference path="../GeneratedArtifacts/viewModel.js" />

myapp.CheckInFlightPieces.created = function (screen) {
    
    myapp.activeDataWorkspace.WFSDBData.EntityTypes_SingleOrDefault(5).execute().then(function (selectedEntity) {

        screen.PackageEntityType = selectedEntity.results[0];

    });

    screen.ReceivedPieces = 0;
    screen.TotalPieces = 0;
    screen.PiecesPercentReceived = 0;

    fillFlightSummary(screen);

};

function fillFlightSummary(screen) {

    var sTask = screen.SelectedTask;

    $.ajax({
        type: 'GET',
        data: { 'TaskId': sTask.Id },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetFlightCheckInSummary',
        success: function (data) {

            //alert(data.StageLocation + " | " + data.StatusName + " | " + data.ReceivedPieces + " | " + data.TotalPieces + " | " + data.PercentReceived);
            screen.StagedAtName = data.StageLocation;
            screen.StatusName = data.StatusName;
            screen.ReceivedPieces = data.ReceivedPieces;
            screen.TotalPieces = data.TotalPieces;
            screen.PiecesPercentReceived = data.PercentReceived;

        },
        error: function (e) {

            console.log('Unable get flight summary details.' + e);
        }
    });

};

myapp.CheckInFlightPieces.Filter_render = function (element, contentItem) {
    
    var elem = $(element);
    elem.css('padding-left', '10px');
    var filter = $('<input type="text" id="pieceCheckInFilter" style="margin-left:3px;"></input>');
    elem.append(filter);

};
myapp.CheckInFlightPieces.ShowNotCheckedIn_postRender = function (element, contentItem) {
    
    $(element).find('a').first().makeBackgroundRed();
    $(element).on(getSuitableEventName(), function (e) {
        $(this).find('a').first().makeBackgroundRed();

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Checked In') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });

};
myapp.CheckInFlightPieces.ShowCheckedIn_postRender = function (element, contentItem) {
    
    $(element).on(getSuitableEventName(), function (e) {
        $(this).find('a').first().makeBackgroundRed();

        $(this).siblings().each(function () {
            var span = $(this).find('a span.ui-btn-text');
            if (span && span.text() == 'Not Checked In') {
                $(this).find('a').first().makeBackgroundBlue();
            }
        });
    });

};

myapp.CheckInFlightPieces.RefreshButton_render = function (element, contentItem) {
    
    var elem = $("<div class='am_refresh'><img src='content/images/refresh-icon_w.png' style='height: 35px;' /></div>");
    $(element).append(elem);
    $(element).trigger("create");

};
myapp.CheckInFlightPieces.GoToHome_execute = function (screen) {
    
    myapp.navigateHome();

};
myapp.CheckInFlightPieces.Flights_execute = function (screen) {
   
    myapp.showCheckInFlight();

};
myapp.CheckInFlightPieces.AddPackages_execute = function (screen) {
    // Write code here.

};
myapp.CheckInFlightPieces.CheckInList_render = function (element, contentItem) {

    var ul = $('<ul id="checkinList" class="custom-list"></ul>');
    $(element).append(ul);
    myapp.CheckInFlightPieces.listLoadPageNumber = 1;
    myapp.CheckInFlightPieces.isComplete = false;

    $(element).on('scroll', function () {
        if ($('ul#checkinList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            getCheckInList(contentItem.screen, false, myapp.CheckInFlightPieces.listLoadPageNumber);
            myapp.CheckInFlightPieces.listLoadPageNumber = myapp.CheckInFlightPieces.listLoadPageNumber + 1;
        }
    });

    getCheckInList(contentItem.screen, false, myapp.CheckInFlightPieces.listLoadPageNumber);

};

function getCheckInList(screen, clearList, pageNumber) {

    sTask = screen.SelectedTask;
    sFlt = screen.SelectedFlight;
    //alert(screen.CurrentUser + " | " + sFlt.Id + " | " + sTask.Id + " | 10 | " + pageNumber + " | " + myapp.CheckInFlightPieces.isComplete + " | " + $('input#pieceCheckInFilter').last().val());
    $.ajax({
        type: 'GET',
        data: { 'UserName': screen.CurrentUser, 'FlightManifestId': sFlt.Id, 'TaskId': sTask.Id, 'RowsPerPage': 10, 'PageNumber': pageNumber, 'IsComplete': myapp.CheckInFlightPieces.isComplete, 'Filter': $('input#pieceCheckInFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetCheckInList',
        success: function (data) {
            
            $('span#checkinListNoItems').remove();
            var list = $('ul#checkinList').last();
            if (clearList) {
                list.empty();
                myapp.CheckInFlightPieces.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:74px"></li>');
                var that = this;
                li.on('click', function () {
                    if (!screen.CheckInList) {
                        screen.CheckInList = {};
                    }

                    if (myapp.CheckInFlightPieces.isComplete == false) {
                        screen.CheckInList.selectedItem = that;
                        screen.CheckInPiecesHeader = "Check In Piece";
                        screen.CheckInPiecesText = "Do you want to check in this piece?";
                        screen.findContentItem("SelectedPackage").isVisible = true;
                        screen.SelectedPackage = screen.CheckInList.selectedItem.RefNumber;
                        $('#checkInPiecesPopupContent').parent().center();

                        screen.showPopup("CheckInPiecesPopup");
                    }
                    else
                    {
                        screen.CheckInList.selectedItem = that;
                        screen.CheckInPiecesHeader = "Uncheck In Piece";
                        screen.CheckInPiecesText = "Do you want to uncheck in this piece?";
                        screen.findContentItem("SelectedPackage").isVisible = true;
                        screen.SelectedPackage = screen.CheckInList.selectedItem.RefNumber;
                        $('#checkInPiecesPopupContent').parent().center();

                        screen.showPopup("CheckInPiecesPopup");
                    }

                });

                var imageContainer1 = $('<div class="custom-image-container1" style="margin-top:5px"></div>');

                    var div = $('<div class="recovery-checkbox unselected-recovery-checkbox" style="z-index: 100"></div>');
                    div.attr('pieceId', that.PackageId);
                    div.attr('pieceNum', that.RefNumber);

                    var evtName = 'click';
                    if ('ontouchstart' in document.documentElement) {
                        evtName = 'touchstart';
                    }

                    div.on(evtName, function (e) {
                        e.stopPropagation();
                        $(this).toggleClass('unselected-recovery-checkbox').toggleClass('selected-recovery-checkbox');

                        var selectAllBtn = $('a:has(span.ui-btn-text:contains("Select All"))');

                        var allItems = $(".recovery-checkbox");
                        var selectedItems = $('.selected-recovery-checkbox');

                        if (allItems.length == selectedItems.length) {
                            selectAllBtn.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                        } else {
                            selectAllBtn.css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                        }
                        return false;
                    });
                    imageContainer1.append(div);
                
                    var imageContainer = $('<div class="custom-image-container"></div>');

                    var snapshotButton = $('<div class="custom-snapshot-button" style="padding:14px"></div>');
                    var snapshotImage = $('<img class="float-left" style="height:22px" src="content/images/camera_icon_w.png"></img>');
                    snapshotButton.append(snapshotImage);
                    snapshotButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.CheckInList) {
                            screen.CheckInList = {};
                        }
                        screen.CheckInList.selectedItem = that;
                        myapp.CheckInFlightPieces.ShowSnapshot_execute(screen);
                    });

                    var overShortButton = $('<div class="custom-snapshot-button" style="padding:14px 20px 14px 20px; margin-right:10px;"></div>');
                    var overShortImage = $('<img class="float-left" style="height:22px" src="content/images/plusminus_icon_w.png"></img>');
                    overShortButton.append(overShortImage);
                    overShortButton.on('click', function (e) {
                        e.stopPropagation();
                        if (!screen.CheckInList) {
                            screen.CheckInList = {};
                        }
                        screen.CheckInList.selectedItem = that;
                        //myapp.CheckInFlightPieces.ShowSnapshot_execute(screen);
                    });

                    imageContainer.append(overShortButton);
                    imageContainer.append(snapshotButton);

                var refContainer = $('<div class="ref-container" style="line-height:27px"></div>');
                var refNumber = $('<div style="font-size:30px">' + this.RefNumber + '</div>');
                var refNumber1 = $('<div style="font-size:18px">' + this.HWBSerialNumber + '</div>');
                var infoHolder = $('<div style="font-size:18px"></div>');
                var uldNumber = $('<span>' + this.ULDNumber + '</span>');

                infoHolder.append(uldNumber);

                refContainer.append(refNumber);
                refContainer.append(refNumber1);
                refContainer.append(infoHolder);

                li.append(imageContainer);
                li.append(imageContainer1);
                li.append(refContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="checkinListNoItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get date from GetCheckInList.' + e);
        }
    })
};
myapp.CheckInFlightPieces.ShowSnapshot_execute = function (screen) {
    
    var tPiece = screen.CheckInList.selectedItem;

    var uDescPiece= tPiece.RefNumber;
    var cTask = screen.SelectedTask;
    if (!cTask) {
        cTask = $ActiveTask;
    }
    var aTimeout = screen.AjaxTimeout;
    if (!aTimeout) {
        aTimeout = $AjaxTimeout;
    }
    myapp.showSnapshot(screen.PackageEntityType, cTask, screen.CurrentUser, screen.CurrentUserWarehouse, tPiece.PackageId, uDescPiece, aTimeout);

};
myapp.CheckInFlightPieces.ShowNotCheckedIn_execute = function (screen) {

    $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    screen.findContentItem("RecoverMultiple").isVisible = true;
    screen.findContentItem("UnRecoverMultiple").isVisible = false;
    myapp.CheckInFlightPieces.isComplete = false;
    getCheckInList(screen, true, 1);

};
myapp.CheckInFlightPieces.ShowCheckedIn_execute = function (screen) {

    $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    screen.findContentItem("RecoverMultiple").isVisible = false;
    screen.findContentItem("UnRecoverMultiple").isVisible = true;
    myapp.CheckInFlightPieces.isComplete = true;
    getCheckInList(screen, true, 1);

};
myapp.CheckInFlightPieces.Search_postRender = function (element, contentItem) {
    
    var evtName = 'click';

    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }
    $(element).find('a').off(evtName).on(evtName, function () {
        getCheckInList(contentItem.screen, true, 1);

    });

};
myapp.CheckInFlightPieces.Reset_execute = function (screen) {
    
    $('input#pieceCheckInFilter').last().val('');
    getCheckInList(screen, true, 1);

};
myapp.CheckInFlightPieces.SelectAllPieces_postRender = function (element, contentItem) {
    
    $(element).find('a').first().attr('id', 'selectAllPieces');

    var evtName = 'click';
    if ('ontouchstart' in document.documentElement) {
        evtName = 'touchstart';
    }

    $(element).on(evtName, function (e) {
        var a = $(this).find('a').first();
        selectAllPieces(a);
    });

};

function selectAllPieces(sender) {
    var allItems = $(".recovery-checkbox");
    var selectedItems = $('.selected-recovery-checkbox');
    var unselectedItems = $('.unselected-recovery-checkbox');

    if (allItems.length == selectedItems.length) {
        selectedItems.removeClass('selected-recovery-checkbox').addClass('unselected-recovery-checkbox');
        sender.css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    } else {
        unselectedItems.addClass('selected-recovery-checkbox').removeClass('unselected-recovery-checkbox');
        sender.css('background', 'linear-gradient(to bottom, #C00000 0%, #960000 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
    }
};

myapp.CheckInFlightPieces.PiecesPercentReceived_render = function (element, contentItem) {
    
    var iScrn = contentItem.screen;
    var sInd = iScrn.ReceivedPieces;
    var eInd = iScrn.TotalPieces;

    progress(contentItem.value, sInd, eInd, "hProgressBar", element);

    contentItem.addChangeListener("value", function () {

        var iScrn = contentItem.screen;
        var sInd = iScrn.ReceivedPieces;
        var eInd = iScrn.TotalPieces;

        progress(contentItem.value, sInd, eInd, "hProgressBar", element);

    });

};
myapp.CheckInFlightPieces.StagedAtName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};
myapp.CheckInFlightPieces.StatusName_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");

    $(element).css("padding-top", "4px");
    $(element).css("font-size", "18px");
    $(element).css("line-height", "22px");

};
myapp.CheckInFlightPieces.RecoverMultiple_execute = function (screen) {

    var sCount = +0;
    var sNum = "";
    $('div.selected-recovery-checkbox').each(function () {
        sCount += +1;
        sNum = $(this).attr('pieceNum');
    });

    if (sCount == +1) {

        screen.CheckInPiecesHeader = "Check In Piece";
        screen.CheckInPiecesText = "Do you want to check in this piece?";
        screen.findContentItem("SelectedPackage").isVisible = true;
        screen.SelectedPackage = sNum;
        $('#checkInPiecesPopupContent').parent().center();

        screen.showPopup("CheckInPiecesPopup");

    }
    else if (sCount > +1) {

        screen.CheckInPiecesHeader = "Multiple Check In";
        screen.CheckInPiecesText = "Do you want to check in the selected pieces?";
        screen.findContentItem("SelectedPackage").isVisible = false;

        $('#checkInPiecesPopupContent').parent().center();

        screen.showPopup("CheckInPiecesPopup");

    }
    else {

        alert("At least 1 piece needs to be selected to check in.");

    }

};
myapp.CheckInFlightPieces.CheckInPiecesPopup_postRender = function (element, contentItem) {
    
    var container = $(element);
    container.attr('id', 'checkInPiecesPopupContent');
    container.parent().center();
    var header = $('<div class="popup-header msls-clear msls-first-row  msls-presenter msls-ctl-text msls-leaf msls-redraw msls-presenter-content msls-font-style-normal" style="max-width:480px; min-width: 30px; font-size: 12px; font-weight: bold; width:100%; height:36px; position:absolute; top:0"></div>');

    container.children().first().before(header);
    container.css("-moz-border-radius", "10px");
    container.css("-webkit-border-radius", "10px");
    container.css("border-radius", "10px");

};
myapp.CheckInFlightPieces.CheckInPiecesHeader_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "36px");
    $(element).css("line-height", "36px");
    $(element).css("font-weight", "bold");
    $(element).css("margin-top", "4px");

};
myapp.CheckInFlightPieces.CheckInPiecesText_postRender = function (element, contentItem) {
   
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.CheckInFlightPieces.ConfirmNo_execute = function (screen) {
    
    screen.closePopup("CheckInPiecesPopup");

};
myapp.CheckInFlightPieces.CheckinFlightHistoryContainer_render = function (element, contentItem) {
    var table = $('<table id="checkinHistory" class="msls-table ui-responsive table-stripe msls-hstretch ui-table ui-table-reflow">' +
                    '<thead>' +
                        '<tr>' +
                            '<td style="width:50px; font-weight:bold">Date</td>' +
                            '<td style="width:125px; font-weight:bold">Reference</td>' +
                            '<td style="width:125px; font-weight:bold">Description</td>' +
                            '<td style="width:100px; font-weight:bold">User</td>' +
                        '</tr>' +
                    '</thead>' +
                    '<tbody id="checkinHistoryBody"></tbody>' +
                   '</table>');
    $(element).append(table);
    myapp.CheckInFlightPieces.historyLoadPageNumber = 2;
    $(element).on('scroll', function () {
        if ($('table#checkinHistory').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            createCheckinFlightHistory(contentItem.screen.SelectedFlight.Id, myapp.CheckInFlightPieces.historyLoadPageNumber, false);
            myapp.CheckInFlightPieces.historyLoadPageNumber = myapp.CheckInFlightPieces.historyLoadPageNumber + 1;
        }
    });

    createCheckinFlightHistory(contentItem.screen.SelectedFlight.Id, 1, true);
};

function createCheckinFlightHistory(flightId, pageNumber, clearList) {
    $.ajax({
        type: 'get',
        data: { 'flightManifestId': flightId, 'filter': $('#checkinFlightFilter').last().val(), 'rowsPerPage': 20, 'pageNumber': pageNumber },
        url: '../api/cargoreceiver/GetFlightHistory',
        success: function (data) {
            var tbody = $('tbody#checkinHistoryBody').last();

            if (clearList) {
                tbody.empty();
                myapp.CheckInFlightPieces.historyLoadPageNumber = 2;
            }

            $(data).each(function () {
                tbody.append('<tr class="msls-tr msls-style ui-shadow ui-tr msls-presenter msls-ctl-table-row-layout msls-vauto msls-hstretch msls-presenter-content msls-font-style-normal msls-hscroll msls-table-row">' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Date + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Reference + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.Description + '</td>' +
                                '<td class="msls-column msls-presenter msls-ctl-text msls-vauto msls-hauto msls-leaf msls-redraw msls-presenter-content msls-font-style-normal msls-fixed-width">' + this.UserName + '</td>' +
                             '</tr>');
            });
        },
        error: function (e) {
            console.log('Error History: ' + e);
        }
    });
}
myapp.CheckInFlightPieces.CheckinFlightFilter_render = function (element, contentItem) {
    $(element).append('<input type="text" id="checkinFlightFilter"></input>');
};
myapp.CheckInFlightPieces.CheckinFlightFilterButton_postRender = function (element, contentItem) {
    $(element).css('margin-top', '0');
};
myapp.CheckInFlightPieces.ResetCheckinLightButton_postRender = function (element, contentItem) {
    $(element).css('margin-top', '0');
};
myapp.CheckInFlightPieces.CheckinFlightFilterButton_execute = function (screen) {
    createCheckinFlightHistory(screen.SelectedFlight.Id, 1, true);
};
myapp.CheckInFlightPieces.ResetCheckinLightButton_execute = function (screen) {
    $('#checkinFlightFilter').val('');
    createCheckinFlightHistory(screen.SelectedFlight.Id, 1, true);
};
myapp.CheckInFlightPieces.SelectedPackage_postRender = function (element, contentItem) {
    
    $("div.msls-text").removeClass("msls-font-style-normal msls-text");
    $(element).css("font-size", "24px");
    $(element).css("line-height", "24px");

};
myapp.CheckInFlightPieces.ConfirmYes_execute = function (screen) {

    var sTask = screen.SelectedTask;
    var cUser = screen.CurrentUser;

    var sCount = +0;
    var pcs = "";
    $("div.selected-recovery-checkbox").each(function () {
        pcs += $(this).attr('pieceId') + ',';
        sCount += +1;
    });
    
    if (myapp.CheckInFlightPieces.isComplete == false) {
        if (sCount > +0) {
            pcs = pcs.slice(0, pcs.length - 1);
            if (screen.findContentItem("SelectedPackage").isVisible == false) {

                //alert(sTask.Id + " | " + cUser + " | " + pcs);
                $.ajax({
                    type: 'post',
                    data: {
                        'TaskId': sTask.Id,
                        'UserName': cUser,
                        'PackageIds': pcs
                    },
                    url: '../api/cargoreceiver/CheckInFlightPieces',
                    success: function (s) {
                        screen.closePopup("CheckInPiecesPopup");
                        $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                        fillFlightSummary(screen);
                        getCheckInList(screen, true, 1);
                    },
                    error: function (e) {
                        alert("Unable to check in selected pieces.  " + e);
                    }
                });

            }
            else {
                //alert(sTask.Id + " | " + cUser + " | " + pcs);
                if (sCount == +1) {
                    $.ajax({
                        type: 'post',
                        data: {
                            'TaskId': sTask.Id,
                            'UserName': cUser,
                            'PackageIds': pcs
                        },
                        url: '../api/cargoreceiver/CheckInFlightPieces',
                        success: function (s) {
                            screen.closePopup("CheckInPiecesPopup");
                            $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                            fillFlightSummary(screen);
                            getCheckInList(screen, true, 1);
                        },
                        error: function (e) {
                            alert("Unable to check in selected piece.  " + e);
                        }
                    });
                }
                else {
                    var pItem = screen.CheckInList.selectedItem;
                    //alert(sTask.Id + " | " + cUser + " | " + pItem.PackageId);
                    $.ajax({
                        type: 'post',
                        data: {
                            'TaskId': sTask.Id,
                            'UserName': cUser,
                            'PackageIds': pItem.PackageId
                        },
                        url: '../api/cargoreceiver/CheckInFlightPieces',
                        success: function (s) {
                            screen.closePopup("CheckInPiecesPopup");

                            $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                            fillFlightSummary(screen);
                            getCheckInList(screen, true, 1);

                        },
                        error: function (e) {
                            alert("Unable to check in selected piece.  " + e);
                        }
                    });
                }

            }
        }
        else {
            var pItem = screen.CheckInList.selectedItem;
            //alert(sTask.Id + " | " + cUser + " | " + pItem.PackageId);
            $.ajax({
                type: 'post',
                data: {
                    'TaskId': sTask.Id,
                    'UserName': cUser,
                    'PackageIds': pItem.PackageId
                },
                url: '../api/cargoreceiver/CheckInFlightPieces',
                success: function (s) {
                    screen.closePopup("CheckInPiecesPopup");

                    $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                    fillFlightSummary(screen);
                    getCheckInList(screen, true, 1);

                },
                error: function (e) {
                    alert("Unable to check in selected piece.  " + e);
                }
            });
        }
    }
    else
    {
        if (sCount > +0) {
            pcs = pcs.slice(0, pcs.length - 1);
            if (screen.findContentItem("SelectedPackage").isVisible == false) {

                //alert(sTask.Id + " | " + cUser + " | " + pcs);
                $.ajax({
                    type: 'post',
                    data: {
                        'TaskId': sTask.Id,
                        'UserName': cUser,
                        'PackageIds': pcs
                    },
                    url: '../api/cargoreceiver/UnCheckInFlightPieces',
                    success: function (s) {
                        screen.closePopup("CheckInPiecesPopup");
                        $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                        fillFlightSummary(screen);
                        getCheckInList(screen, true, 1);
                    },
                    error: function (e) {
                        alert("Unable to uncheck in selected pieces.  " + e);
                    }
                });

            }
            else {
                //alert(sTask.Id + " | " + cUser + " | " + pcs);
                if (sCount == +1) {
                    $.ajax({
                        type: 'post',
                        data: {
                            'TaskId': sTask.Id,
                            'UserName': cUser,
                            'PackageIds': pcs
                        },
                        url: '../api/cargoreceiver/UnCheckInFlightPieces',
                        success: function (s) {
                            screen.closePopup("CheckInPiecesPopup");
                            $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                            fillFlightSummary(screen);
                            getCheckInList(screen, true, 1);
                        },
                        error: function (e) {
                            alert("Unable to uncheck in selected piece.  " + e);
                        }
                    });
                }
                else {
                    var pItem = screen.CheckInList.selectedItem;
                    //alert(sTask.Id + " | " + cUser + " | " + pItem.PackageId);
                    $.ajax({
                        type: 'post',
                        data: {
                            'TaskId': sTask.Id,
                            'UserName': cUser,
                            'PackageIds': pItem.PackageId
                        },
                        url: '../api/cargoreceiver/UnCheckInFlightPieces',
                        success: function (s) {
                            screen.closePopup("CheckInPiecesPopup");

                            $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                            fillFlightSummary(screen);
                            getCheckInList(screen, true, 1);

                        },
                        error: function (e) {
                            alert("Unable to uncheck in selected piece.  " + e);
                        }
                    });
                }

            }
        }
        else {
            var pItem = screen.CheckInList.selectedItem;
            //alert(sTask.Id + " | " + cUser + " | " + pItem.PackageId);
            $.ajax({
                type: 'post',
                data: {
                    'TaskId': sTask.Id,
                    'UserName': cUser,
                    'PackageIds': pItem.PackageId
                },
                url: '../api/cargoreceiver/UnCheckInFlightPieces',
                success: function (s) {
                    screen.closePopup("CheckInPiecesPopup");

                    $('#selectAllPieces').css('background', 'linear-gradient(to bottom, #3744BA 1%, #273584 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)');
                    fillFlightSummary(screen);
                    getCheckInList(screen, true, 1);

                },
                error: function (e) {
                    alert("Unable to uncheck in selected piece.  " + e);
                }
            });
        }
    }

};
myapp.CheckInFlightPieces.UnRecoverMultiple_execute = function (screen) {

    var sCount = +0;
    var sNum = "";
    $('div.selected-recovery-checkbox').each(function () {
        sCount += +1;
        sNum = $(this).attr('pieceNum');
    });

    if (sCount == +1) {

        screen.CheckInPiecesHeader = "Uncheck In Piece";
        screen.CheckInPiecesText = "Do you want to uncheck in this piece?";
        screen.findContentItem("SelectedPackage").isVisible = true;
        screen.SelectedPackage = sNum;
        $('#checkInPiecesPopupContent').parent().center();

        screen.showPopup("CheckInPiecesPopup");

    }
    else if (sCount > +1) {

        screen.CheckInPiecesHeader = "Multiple Uncheck In";
        screen.CheckInPiecesText = "Do you want to uncheck in the selected pieces?";
        screen.findContentItem("SelectedPackage").isVisible = false;

        $('#checkInPiecesPopupContent').parent().center();

        screen.showPopup("CheckInPiecesPopup");

    }
    else {

        alert("At least 1 piece needs to be selected to uncheck in.");

    }

};
myapp.CheckInFlightPieces.CheckinAwbFilter_render = function (element, contentItem) {
    $(element).append('<input type="text" id="checkinAwbFilter"></input>');
};
myapp.CheckInFlightPieces.CheckinAwbsList_render = function (element, contentItem) {
    var ul = $('<ul id="checkinAwbsList" class="custom-list"></ul>')
    $(element).append(ul);
    myapp.CheckInFlightPieces.listLoadPageNumber = 2;
    $(element).on('scroll', function () {
        if ($('ul#checkinAwbsList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            fillcheckinAwbsList(contentItem.screen.SelectedFlight.Id, false, myapp.CheckInFlightPieces.listLoadPageNumber);
            myapp.CheckInFlightPieces.listLoadPageNumber = myapp.CheckInFlightPieces.listLoadPageNumber + 1;
        }
    });

    fillcheckinAwbsList(contentItem.screen.SelectedFlight.Id, true, 1);
};

function fillcheckinAwbsList(flightId, clearList, pageNumber) {
    $.ajax({
        type: 'GET',
        data: { 'flightManifestId': flightId, 'rowsPerPage': 6, 'pageNumber': pageNumber, 'filter': $('input#checkinAwbFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetFlightAwbs',
        success: function (data) {
            $('span#noItems').remove();
            var list = $('ul#checkinAwbsList').last();
            if (clearList) {
                list.empty();
                myapp.CheckInFlightPieces.listLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:30px"></li>');
               
                var refContainer = $('<div class="ref-container"></div>');
                var refNumber = $('<div style="font-size:36px; line-height:32px">' + this.AwbNumber + '</div>');
                
                refContainer.append(refNumber);
                li.append(refContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="noItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get data.' + e);
        }
    })
}
myapp.CheckInFlightPieces.SearchCheckinAwbButton_execute = function (screen) {
    fillcheckinAwbsList(screen.SelectedFlight.Id, true, 1);
};
myapp.CheckInFlightPieces.ResetCheckinAwbButton_execute = function (screen) {
    $('input#checkinAwbFilter').val('');
    fillcheckinAwbsList(screen.SelectedFlight.Id, true, 1);
};
myapp.CheckInFlightPieces.PutAway_execute = function (screen) {    
    myapp.showPutAwayPieces(screen.SelectedFlight, screen.CurrentUser, screen.CurrentUserStation, screen.CurrentUserWarehouse, screen.SelectedTask, screen.AjaxTimeout);
};

myapp.CheckInFlightPieces.CheckinHwbsList_render = function (element, contentItem) {
    var ul = $('<ul id="checkinHwbsList" class="custom-list"></ul>');
    $(element).css('padding', '0');
    $(element).append(ul);
    myapp.CheckInFlightPieces.hwblistLoadPageNumber = 2;
    $(element).on('scroll', function () {
        if ($('ul#checkinHwbsList').last().height() === $(this).scrollTop() + $(this).height() + 6) {
            fillcheckinHwbsList(contentItem.screen.SelectedFlight.Id, false, myapp.CheckInFlightPieces.hwblistLoadPageNumber);
            myapp.CheckInFlightPieces.hwblistLoadPageNumber = myapp.CheckInFlightPieces.hwblistLoadPageNumber + 1;
        }
    });

    fillcheckinHwbsList(contentItem.screen.SelectedFlight.Id, true, 1);
};

function fillcheckinHwbsList(flightId, clearList, pageNumber) {
    $.ajax({
        type: 'GET',
        data: { 'flightManifestId': flightId, 'rowsPerPage': 6, 'pageNumber': pageNumber, 'filter': $('input#checkinHwbFilter').last().val() },
        contentType: 'application/json',
        url: '../api/cargoreceiver/GetFlightHwbs',
        success: function (data) {
            $('span#noHwbItems').remove();
            var list = $('ul#checkinHwbsList').last();
            if (clearList) {
                list.empty();
                myapp.CheckInFlightPieces.hwblistLoadPageNumber = 2;
            }

            $(data).each(function () {
                var li = $('<li class="custom-list-item" style="height:55px"></li>');

                var refContainer = $('<div class="ref-container" style="width:100%;"></div>');
                var refNumber = $('<span style="font-size:30px; line-height:30px">' + this.Hwb + '</span>');
                var pieces = $('<span style="font-size:30px; line-height:30px; float:right;padding-right:30px;">PCS:' + this.TotalPieces + '</span>');

                var descContainer = $('<div class="ref-container" style="width:100%;"></div>');
                var svc = $('<span style="font-size:17px;">SVC:' + this.ServiceType + '</span>');
                var desc = $('<span style="font-size:17px; float:right;padding-right:30px;">DEST:' + this.Destination + '</span>');


                refContainer.append(refNumber);
                refContainer.append(pieces);
                descContainer.append(svc);
                descContainer.append(desc);


                li.append(refContainer);
                li.append(descContainer);
                list.append(li);
            });

            if (!list.find('li').length) {
                list.before('<span id="noHwbItems">No Items</span>')
            }
        },

        error: function (e) {
            console.log('Unable get data.' + e);
        }
    })
}

myapp.CheckInFlightPieces.CheckinHwbFilter_render = function (element, contentItem) {
    $(element).append('<input type="text" id="checkinHwbFilter"></input>');
};
myapp.CheckInFlightPieces.ResetCheckinHwbButton_execute = function (screen) {
    $('input#checkinHwbFilter').val('');
    fillcheckinHwbsList(screen.SelectedFlight.Id, true, 1);
};
myapp.CheckInFlightPieces.SearchCheckinHwbButton_execute = function (screen) {
    fillcheckinHwbsList(screen.SelectedFlight.Id, true, 1);
};