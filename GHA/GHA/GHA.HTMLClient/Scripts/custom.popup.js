﻿/// <reference path="jquery.mobile-1.3.2.js" />

(function ($) {

    $.fn.confirmation = function (header, question, additionalText, buttons) {

        var template = '<div class="custom-popup">' +
                            '<div class="custom-popup-header">' +
                                '<h1></h1>' +
                            '</div>' +
                            '<div class="custom-popup-body">' +
                                '<h3></h3>' +
                                '<p></p>' +                                
                            '</div>' +
                        '</div>';
        
        var operation = '';
        var promise = msls.promiseOperation(function (o) {
            operation = o;
        });
        var p = $(template);

        p.find('div.custom-popup-header h1').html(header);
        p.find('div.custom-popup-body h3').html(question);
        p.find('div.custom-popup-body p').html(additionalText);

        var customBody = p.find('div.custom-popup-body');

        $.each(buttons, function (index, value) {
            var button = $('<a>' +
                            '<span class="custom-popup-button-inner">' +
                                '<span>' + index + '</span>' +
                            '</span>' +
                           '</a>');

            if (value && value.color) {               
                    button.find('span.custom-popup-button-inner span').css('color', value.color);                
            }

            button.click(function () {
                p.popup('close');
                operation.complete(index);                
            });;

            customBody.append(button);
        });

        p.bind({
            popupafterclose: function (event, ui) {
                $(this).popup('destroy');
            },
            popupbeforeposition: function () {
                $('.ui-popup-screen').off();
            }
        });

        p.popup();
        p.popup('open');
        return promise;
    };

}(jQuery));