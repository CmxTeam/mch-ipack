﻿/// <reference path="datajs-1.1.1.js" />

(function () {

    function radioButtons(element, displayname, options) {
        var rbSetName = 'rb' + displayname,
            $rb,
            $rbInputs,
            $div;

        options = options || {};
        options.isHorizontal = options.isHorizontal || false;
        options.choiceList = options.choiceList;
        options.defaultSelection = options.defaultSelection || 0;

        function radioButtonChoice(item) {

            if (options.defaultSelection == item.value) {
                var id = rbSetName + item.value,
                html = '<input type="radio" name="' + rbSetName + '" id="' + id + '" value="' + item.value + '" checked="true" />' +
                    '<label for="' + id + '">' + item.stringValue + '</label>';
            }
            else {
                var id = rbSetName + item.value,
                html = '<input type="radio" name="' + rbSetName + '" id="' + id + '" value="' + item.value + '" />' +
                    '<label for="' + id + '">' + item.stringValue + '</label>';

            }
            return $(html);
        }

        function label(text) {
            var $l = $("<label class='msls-label-text'>").text(text);

            // to keep this example simple, I'm hardcoding the label alignment to 'Top'
            // if you need a different alignment, you may want to look at the _addAttachedLabel
            // function in the msls js file
            var $d = $("<div class='msls-attached-label msls-label-align-top msls-clear msls-vauto'>");
            return $d.append($l);
        }

        $rb = $("<fieldset id='" + rbSetName + "' data-role='controlgroup' data-type='" +
            (options.isHorizontal ? "horizontal" : "vertical") + "'>");

        options.choiceList.forEach(function (item) {
            $rb.append(radioButtonChoice(item));
        });



      
            // msls doesn't auto add a label for custom group controls
            label(displayName).appendTo(element);

            // msls isn't putting the leaf class on custom group controls, 
            // though it does use it on custom value controls and its own group controls
            // we have to add it in manually otherwise the alignment will be off
            $(element).addClass("msls-leaf");
        

        // the classes of the div and parent are important so that things align properly
        // I'm hardcoding for top-labels to keep things simple for now
        $div = $("<div class='msls-clear msls-vauto'>").append($rb);
        $div.appendTo(element);


        // bind so that changing radio buttons changes the contentItem value
        //$rbInputs = $rb.find("input");
        //$rbInputs.change(function () {
        //    var newValue = $(this).val();

        //    if (contentItem.kind === "Details") {
        //        // ids should be unique, so return the first match
        //        // (note: inefficient... better would be something like underscore/lodash's find)
        //        contentItem.value = options.choiceList.filter(function (choice) {
        //            return choice.value.toString() === newValue;
        //        })[0].entity;
        //    } else {
        //        contentItem.value = newValue;
        //    }
        //});

        // bind so that changing the contentItem value changes the radio button
        //contentItem.dataBind("value", function (data) {
        //    if (!!data) {
        //        if (contentItem.kind === "Details") {
        //            data = contentItem.value.Id;
        //        }
        //        $rbInputs.each(function () {
        //            this.checked = this.value === data.toString();
        //            // make sure control has been initialized first... if so, refresh
        //            if ($(this).parent().hasClass("ui-radio")) {
        //                $(this).checkboxradio("refresh");
        //            }
        //        });
        //    }
        //});
    }

    window.lscontrols = {
        radioButtons: radioButtons
    };

}());
