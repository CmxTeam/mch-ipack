﻿/// <reference path="datajs-1.1.1.js" />

function progress(percent, startInd, endInd, tagName, element) {

    /*------- DOM Initialization ---------*/
    this.PercentContainer = $('<div id="' + tagName + '"></div>');
    this.PercentBar = $("<div></div>");
    this.NumberContainer = $("<p class='am_numbers'>" + startInd + "&nbspof&nbsp" + endInd + "</p>");

    /*------- Elements injection into DOM --------*/
    $(this.PercentBar).appendTo(this.PercentContainer);
    $(this.NumberContainer).appendTo(this.PercentContainer);
    $(element).html(this.PercentContainer);

    /*------- Percent Calculation -------*/
    this.ContainerWidth = $('#' + tagName).width();
    if (tagName == "pProgressBar") {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 126) / this.ContainerWidth;
        }
    }
    else if (tagName == "tProgressBar") {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 56) / this.ContainerWidth;
        }
    }
    else if (tagName == "uProgressBar") {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 332) / this.ContainerWidth;
        }
    }
    else if (tagName == "aProgressBar") {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 332) / this.ContainerWidth;
        }
    }
    else if (tagName == "bProgressBar") {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 552) / this.ContainerWidth;
        }
    }
    else if (tagName == "cProgressBar") {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 841) / this.ContainerWidth;
        }
    }
    else if (tagName == "dProgressBar") {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 600) / this.ContainerWidth;
        }
    }
    else {
        if (percent > 100) {
            this.ProgressBarWidth = 100;
        }
        else {
            this.ProgressBarWidth = (percent * 100) / this.ContainerWidth;
        }
    }

    /*------- Percent bar animation ------*/
    $(this.PercentBar).animate({ width: this.ProgressBarWidth }, 500);
};

function percentProgress(percent, tagName, element) {

    /*------- DOM Initialization ---------*/
    this.PercentContainer = $('<div id="' + tagName + '"></div>');
    this.PercentBar = $("<div></div>");
    this.NumberContainer = $("<p class='am_numbers'>" + percent +  "%</p>");

    /*------- Elements injection into DOM --------*/
    $(this.PercentBar).appendTo(this.PercentContainer);
    $(this.NumberContainer).appendTo(this.PercentContainer);
    $(element).html(this.PercentContainer);

    /*------- Percent Calculation -------*/
    this.ContainerWidth = $('#' + tagName).width();
    if (tagName == "pProgressBar") {
        this.ProgressBarWidth = (percent * 126) / this.ContainerWidth;
    }
    else if (tagName == "tProgressBar")
    {
        this.ProgressBarWidth = (percent * 56) / this.ContainerWidth;
    }
    else {
        this.ProgressBarWidth = (percent * 100) / this.ContainerWidth;
    }

    /*------- Percent bar animation ------*/
    $(this.PercentBar).animate({ width: this.ProgressBarWidth }, 500);
};

function dateFormat(fDate, element) {

    var fMonth = "";
    var fDay = "";
    var fHour = "";
    var fMinute = "";
    var fDateString = "";

    if (fDate.getDate() < 10) {
        fDay = "0" + fDate.getDate().toString();
    }
    else {
        fDay = fDate.getDate().toString();
    }

    if (fDate.getHours() < 10) {
        fHour = "0" + fDate.getHours().toString();
    }
    else {
        fHour = fDate.getHours().toString();
    }

    if (fDate.getMinutes() < 10) {
        fMinute = "0" + fDate.getMinutes().toString();
    }
    else {
        fMinute = fDate.getMinutes().toString();
    }

    switch (fDate.getMonth() + +1) {
        case 1:
            fMonth = "JAN";
            break;
        case 2:
            fMonth = "FEB";
            break;
        case 3:
            fMonth = "MAR";
            break;
        case 4:
            fMonth = "APR";
            break;
        case 5:
            fMonth = "MAY";
            break;
        case 6:
            fMonth = "JUN";
            break;
        case 7:
            fMonth = "JUL";
            break;
        case 8:
            fMonth = "AUG";
            break;
        case 9:
            fMonth = "SEP";
            break;
        case 10:
            fMonth = "OCT";
            break;
        case 11:
            fMonth = "NOV";
            break;
        case 12:
            fMonth = "DEC";
            break;
        default:
            break;

    }

    fDateString = fMonth + "-" + fDay + " " + fHour + ":" + fMinute;

    element.textContent = fDateString;

};

//function contentScroll() {
//    var height = $(window).height();
//    $(".msls-tab-content .msls-last-row ").css({ "height": height - 210, "overflow-y": "auto" });
//    console.log($(".ui-content").length);
//}

//$(document).on("DOMSubtreeModified", function () { contentScroll() });
//$(window).resize(function () {
//    contentScroll();
//});
