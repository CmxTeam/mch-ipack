﻿Imports System.Text

Public Class LogObj
    Public Property Operation() As enmOperation
    Public Property IsSuccess() As Boolean
    Public Property Retry() As Integer
    Public Property Host() As String
    Public Property Message() As String
    Public Property File() As String
    Public Overloads Function ToString()
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(Environment.NewLine)
        If (Retry > 1) Then sb.Append("RETRY ")
        If (Operation = enmOperation.FtpGet) Then sb.Append("GET ")
        If (Operation = enmOperation.FtpPut) Then sb.Append("PUT ")
        If (Operation = enmOperation.FtpDelete) Then sb.Append("DELETE ")

        sb.Append(Host)
        If (IsSuccess) Then sb.Append(" Transaction Response Successful") Else sb.Append(" Transaction Response Failed")
        sb.Append(Environment.NewLine)
        sb.AppendLine("Txn Date: " + DateTime.Now)
        sb.AppendLine("Txn Ref: " + File)
        If (IsSuccess) Then sb.AppendLine("Txn Conf: Success") Else sb.AppendLine("Txn Conf: " + Message)
        Return sb.ToString()
    End Function


End Class

Public Enum enmOperation
    FtpPut
    FtpGet
    FtpDelete
End Enum