Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports System.Threading

Public Class FtpObj
    Implements IDisposable

    Private _host As String
    Private _portNumber As Integer
    Private _userID As String
    Private _password As String
    Private _applicationFolder As String
    Private _result As String
    Private ReadOnly _numberOfRetries As Integer = Integer.Parse(ConfigurationManager.AppSettings("NumberOfRetries"))

    Public Property Result() As String
        Get
            Return _result
        End Get
        Set(ByVal Value As String)
            _result = Value
        End Set
    End Property

    Public Property ApplicationFolder() As String
        Get
            Return _applicationFolder
        End Get
        Set(ByVal Value As String)
            _applicationFolder = Value
        End Set
    End Property

    Public Property Host() As String
        Get
            Return _host
        End Get
        Set(ByVal Value As String)
            _host = Value
        End Set
    End Property

    Public Property PortNumber() As Integer
        Get
            Return _portNumber
        End Get
        Set(ByVal Value As Integer)
            _portNumber = Value
        End Set
    End Property

    Public Property UserID() As String
        Get
            Return _userID
        End Get
        Set(ByVal Value As String)
            _userID = Value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal Value As String)
            _password = Value
        End Set
    End Property

    Public Sub New(ByVal host As String, ByVal portNumber As Integer, ByVal userID As String, ByVal password As String)
        _host = host
        _portNumber = IIf(portNumber < 1, 21, portNumber)
        _userID = IIf(userID = "", "Anonymous", userID)
        _password = IIf(password = "", "Anonymous@mail.com", password)
        _applicationFolder = My.Application.Info.DirectoryPath
    End Sub

    Public Sub DoGetFilesByExtension(ByVal remoteFolders As String, ByVal remoteFoldersBackUp As String, ByVal folderPath As String, ByVal fileExtension As String, ByVal isDeleteFile As Boolean)
        Dim folders() As String = Split(remoteFolders, ",")
        Dim backups() As String = Split(remoteFoldersBackUp, ",")

        For i As Integer = 0 To folders.Length - 1
            Dim files As New List(Of String)
            files = GetFileListByExtension(folders(i).Trim, fileExtension)
            For Each file As String In files

                Dim backFolder As String = ""
                If folders.Length = backups.Length Then
                    backFolder = backups(i).Trim
                End If

                TryGet(folders(i).Trim, file.Trim, "", folderPath, isDeleteFile)

                If backFolder <> "" Then
                    TryPut(backFolder, file.Trim, "", folderPath, "", False)
                End If

            Next
        Next

    End Sub


    Public Sub DoGetFilesByExtensionAll(ByVal rootFolder As String, ByVal folderPath As String, ByVal fileExtension As String, ByVal isDeleteFile As Boolean)
        Dim devices As New List(Of String)
        devices = GetDevices(rootFolder)
        For Each device As String In devices
            Dim files As New List(Of String)
            files = GetFileListByExtension(rootFolder & "/" & device, fileExtension)
            For Each file As String In files
                TryGet(file.Trim, file.Trim, "", folderPath, isDeleteFile)
            Next
        Next
    End Sub

    Public Sub TryGet(ByVal remoteFolder As String, ByVal fileName As String, ByVal fileAlias As String, ByVal folderPath As String, ByVal isDeleteFile As Boolean)
        Dim logs As String = String.Empty
        Dim errors As String = String.Empty
        For i As Integer = 1 To _numberOfRetries
            Dim log As LogObj = DoGet(remoteFolder, fileName, fileAlias, folderPath, isDeleteFile)
            log.Retry = i
            logs += log.ToString()
            If (log.IsSuccess) Then Exit For
            errors = log.ToString()
        Next

        Trace.Write(logs)

        If Not String.IsNullOrEmpty(errors) Then
            Throw New Exception(errors)
        End If
    End Sub

    Public Function DoGet(ByVal remoteFolder As String, ByVal fileName As String, ByVal fileAlias As String, ByVal folderPath As String, ByVal isDeleteFile As Boolean) As LogObj
        Dim log As LogObj = New LogObj()
        log.IsSuccess = True
        log.Host = _host
        log.File = fileName
        log.Operation = enmOperation.FtpGet
        Try

            If Directory.Exists(folderPath) = False Then
                Directory.CreateDirectory(folderPath)
            End If

            Dim filePath As String = String.Empty
            Dim script As New StringBuilder

            script.Append("o" & vbCrLf)
            script.Append(_host & " " & _portNumber & vbCrLf)
            script.Append(_userID & vbCrLf)
            script.Append(_password & vbCrLf)

            If remoteFolder <> String.Empty Then
                script.Append("cd " & Chr("34") & remoteFolder & Chr("34") & vbCrLf)
            End If
            script.Append("lcd " & Chr("34") & folderPath & Chr("34") & vbCrLf)

            If fileAlias = String.Empty Then
                fileAlias = fileName
            ElseIf fileAlias = "*" Then
                fileAlias = "IN_" & Guid.NewGuid.ToString & "_" & fileName
            End If

            script.Append("get " & fileName & " " & fileAlias & vbCrLf)

            script.Append("bye")

            filePath = CreateScriptFile(script.ToString)

            'Shell("ftp -s:" & Chr("34") & filePath & Chr("34"), AppWinStyle.Hide, True)
            '_result = ExecuteShell("ftp -s:" & Chr("34") & filePath & Chr("34"))
            _result = ExecuteShellAsync("ftp -s:" & Chr("34") & filePath & Chr("34"))
            DeleteScriptFile(filePath)

            CheckTransaction(_result)

            If isDeleteFile Then
                DeleteRemoteFile(remoteFolder, fileName, folderPath)
            End If



            'If File.Exists(Path.Combine(folderPath, fileAlias)) Then
            '    Dim fileData As FileStream
            '    fileData = File.Open(Path.Combine(folderPath, fileAlias), FileMode.Open)
            '    If fileData.Length = 0 Then
            '        fileData.Close()
            '        File.Delete(Path.Combine(folderPath, fileAlias))
            '    Else
            '        fileData.Close()
            '    End If
            'End If

        Catch ex As Exception
            log.IsSuccess = False
            log.Message = ex.Message
            'Throw New Exception("GET FTP Failed. " & ex.Message)
        End Try
        Return log
    End Function

    Private Sub DeleteRemoteFile(ByVal remoteFolder As String, ByVal fileName As String, ByVal folderPath As String)
        Try
            Dim filePath As String = String.Empty
            Dim script As New StringBuilder

            script.Append("o" & vbCrLf)
            script.Append(_host & " " & _portNumber & vbCrLf)
            script.Append(_userID & vbCrLf)
            script.Append(_password & vbCrLf)


            If remoteFolder <> String.Empty Then
                script.Append("cd " & Chr("34") & remoteFolder & Chr("34") & vbCrLf)
            End If

            script.Append("lcd " & Chr("34") & folderPath & Chr("34") & vbCrLf)
            script.Append("delete " & fileName & vbCrLf)
            script.Append("bye")

            filePath = CreateScriptFile(script.ToString)
            _result = ExecuteShell("ftp -s:" & Chr("34") & filePath & Chr("34"))
            DeleteScriptFile(filePath)
            CheckTransaction(_result)
        Catch ex As Exception
            Throw New Exception("Deleting remote file failed. " & ex.Message)
        End Try
    End Sub

    Private Sub CheckTransaction(ByVal streamValue As String)
        Dim matchObj As Match
        matchObj = Regex.Match(streamValue, "(cannot log in.)|(Invalid Command.)|(failed.)|(Time Out.)", RegexOptions.IgnoreCase)
        If matchObj.Success = True Then
            Throw New Exception(streamValue)
        End If
    End Sub

    Public Function GetFileListByExtension(ByVal remoteFolder As String, ByVal fileExtension As String) As List(Of String)
        Try
            Dim filePath As String = String.Empty

            Dim script As New StringBuilder

            script.Append("o" & vbCrLf)
            script.Append(_host & " " & _portNumber & vbCrLf)
            script.Append(_userID & vbCrLf)
            script.Append(_password & vbCrLf)


            If remoteFolder <> "" Then
                script.Append("cd " & Chr("34") & remoteFolder & Chr("34") & vbCrLf)
            End If

            script.Append("ls" & vbCrLf)

            script.Append("bye")

            filePath = CreateScriptFile(script.ToString)

            'Shell("ftp -s:" & Chr("34") & filePath & Chr("34"), AppWinStyle.Hide, True)
            _result = ExecuteShellAsync("ftp -s:" & Chr("34") & filePath & Chr("34"))
            DeleteScriptFile(filePath)
            CheckTransaction(_result)

            Dim col As New List(Of String)
            'Dim tempArr() As String = Split(_result, vbCrLf & "ls" & vbCrLf)
            Dim resArr() As String
            'If tempArr.Length = 2 Then
            'resArr = Split(tempArr(1).ToString, vbCrLf)
            'Else
            resArr = Split(_result, vbCrLf)
            'End If

            For Each res As String In resArr
                Dim MatchObj As Match
                MatchObj = Regex.Match(res, ".*?\." & fileExtension, RegexOptions.IgnoreCase)
                If MatchObj.Success = True Then
                    If MatchObj.Value.Trim.Length <= 60 Then
                        col.Add(MatchObj.Value.Trim)
                    End If
                End If
            Next

            If col.Count > 0 Then
                Thread.Sleep(10000)
            End If

            Return col
        Catch ex As Exception
            Throw New Exception("Failed. " & ex.Message)
        End Try
    End Function


    Public Function GetDevices(ByVal rootFolder As String) As List(Of String)
        Try
            Dim filePath As String = String.Empty

            Dim script As New StringBuilder

            script.Append("o" & vbCrLf)
            script.Append(_host & " " & _portNumber & vbCrLf)
            script.Append(_userID & vbCrLf)
            script.Append(_password & vbCrLf)


            If rootFolder <> "" Then
                script.Append("cd " & Chr("34") & rootFolder & Chr("34") & vbCrLf)
            End If

            script.Append("ls" & vbCrLf)

            script.Append("bye")

            filePath = CreateScriptFile(script.ToString)

            _result = ExecuteShell("ftp -s:" & Chr("34") & filePath & Chr("34"))
            DeleteScriptFile(filePath)
            CheckTransaction(_result)

            Dim col As New List(Of String)
            Dim resArr() As String
            resArr = Split(_result, vbCrLf)
            For Each res As String In resArr
                ' If IsNumeric(res) Then
                If IsNumeric(res) And (res.Trim.Length = 5 Or res.Trim.Length = 6) Then
                    col.Add(res)
                End If
                '  End If
            Next

            Return col
        Catch ex As Exception
            Throw New Exception("Failed. " & ex.Message)
        End Try
    End Function

    Private Sub TryPut(ByVal remoteFolder As String, ByVal fileName As String, ByVal fileAlias As String, ByVal folderPath As String, ByVal achivePath As String, ByVal delete As Boolean)
        Dim logs As String = String.Empty
        Dim errors As String = String.Empty
        For i As Integer = 1 To _numberOfRetries
            Dim log As LogObj = DoPut(remoteFolder, fileName, fileAlias, folderPath, achivePath, delete)
            log.Retry = i
            logs += log.ToString()

            If (log.IsSuccess) Then Exit For
            errors = log.ToString()
        Next

        Trace.Write(logs)

        Dim fullPath As String = ConfigurationManager.AppSettings("PathToLogFile")
        LogResult(logs, fullPath)

        If (Not String.IsNullOrEmpty(errors)) Then
            Throw New Exception(errors)
        End If
    End Sub

    Private Sub LogResult(ByVal streamValue As String, ByVal logFilePath As String)
        If logFilePath <> String.Empty Then
            Dim fs As FileStream
            If Not File.Exists(logFilePath) Then
                fs = File.Create(logFilePath)
            Else
                fs = File.OpenWrite(logFilePath)
            End If

            'Dim lines = streamValue.StandardOutput.Split(Environment.NewLine)

            'Dim result As String = String.Empty

            'For Each line As String In lines
            '    If line.Trim.StartsWith("200") Or line.Trim.StartsWith("226") Then
            '        result = result + line + Environment.NewLine
            '    End If
            'Next

            Dim logString As String = "PUT FTP " + Format(Now, "MMddyyHHmmss") + Environment.NewLine + streamValue + Environment.NewLine + "----------------------------------------------------------" + Environment.NewLine
            Dim bytes As Byte() = New UTF8Encoding(True).GetBytes(logString)
            fs.Position = fs.Length
            fs.Write(bytes, 0, bytes.Length)
            fs.Flush()
            fs.Close()
        End If
    End Sub

    Public Function DoPut(ByVal remoteFolder As String, ByVal fileName As String, ByVal fileAlias As String, ByVal folderPath As String, ByVal achivePath As String, ByVal delete As Boolean) As LogObj
        Dim log As LogObj = New LogObj()
        log.IsSuccess = True
        log.Host = _host
        log.File = fileName
        log.Operation = enmOperation.FtpPut

        Try
            Dim filePath As String = String.Empty

            Dim script As New StringBuilder

            script.Append("o" & vbCrLf)
            script.Append(_host & " " & _portNumber & vbCrLf)
            script.Append(_userID & vbCrLf)
            script.Append(_password & vbCrLf)

            If remoteFolder <> "" Then
                script.Append("cd " & Chr("34") & remoteFolder & Chr("34") & vbCrLf)
            End If

            'script.Append("lcd " & Chr("34") & folderPath & Chr("34") & vbCrLf)

            If fileAlias = String.Empty Then
                fileAlias = fileName
            ElseIf fileAlias = "*" Then
                fileAlias = "OUT" & "_" & Guid.NewGuid.ToString & "_" & fileName
            End If

            script.Append("put " & Chr(34) & Path.Combine(folderPath, fileName) & Chr(34) & " " & Chr(34) & fileAlias & Chr(34) & vbCrLf)

            script.Append("bye")

            filePath = CreateScriptFile(script.ToString)

            'Shell("ftp -s:" & Chr("34") & filePath & Chr("34"), AppWinStyle.Hide, True)
            '_result = ExecuteShell("ftp -v -s:" & Chr("34") & filePath & Chr("34"))

            _result = ExecuteShellAsync("ftp -v -s:" & Chr("34") & filePath & Chr("34"))
            DeleteScriptFile(filePath)

            CheckTransaction(Result)

            If achivePath <> String.Empty Then
                If Not Directory.Exists(achivePath) Then
                    Directory.CreateDirectory(achivePath)
                End If

                If File.Exists(Path.Combine(folderPath, fileName)) Then
                    File.Copy(Path.Combine(folderPath, fileName), Path.Combine(achivePath, "OUT_" & Guid.NewGuid.ToString & "_" & fileName))
                End If
            End If

            If delete Then
                File.Delete(Path.Combine(folderPath, fileName))
            End If

        Catch ex As Exception
            log.IsSuccess = False
            log.Message = ex.Message

            'Throw New Exception("PUT FTP Failed. " & ex.Message)
        End Try
        Return log
    End Function

    Public Sub DoPutFolder(ByVal remoteFolder As String, ByVal folderPath As String, ByVal archivePath As String, ByVal delete As Boolean)
        Dim files As String() = Directory.GetFiles(folderPath)

        For Each fl As String In files
            Dim tmp As String = Path.GetFileName(fl)
            TryPut(remoteFolder, tmp, "", folderPath, archivePath, delete)
        Next

    End Sub

    Private Function CreateScriptFile(ByVal script As String) As String
        Try
            Dim filePath As String = Path.Combine(_applicationFolder, "TEMP" & "_" & Guid.NewGuid.ToString & "_" & ".TXT")
            Using file As New StreamWriter(filePath, True)
                file.WriteLine(script)
                file.Flush()
            End Using
            Return filePath
        Catch ex As Exception
            Throw New Exception("Could not create file. " & ex.Message)
        End Try
    End Function

    Private Sub DeleteScriptFile(ByVal filePath As String)
        Try
            Dim retries As Integer = 0
            While Not TryDeleteFile(filePath) AndAlso retries < 5
                retries = retries + 1
                Thread.Sleep(2000)
            End While            
        Catch ex As Exception
            Throw New Exception("Could not delete file. " & ex.Message)
        End Try
    End Sub

    Private Function TryDeleteFile(ByVal filePath As String) As Boolean
        Try
            If (File.Exists(filePath)) Then
                File.Delete(filePath)
            End If
            Return True
        Catch ex As IOException
            Return False
        End Try
    End Function

    Private Sub LogResult(ByVal streamValue As Pair, ByVal logFilePath As String, ByVal fileName As String, ByVal ftpHostName As String)
        If logFilePath <> String.Empty Then
            Dim fs As FileStream
            If Not File.Exists(logFilePath) Then
                fs = File.Create(logFilePath)
            Else
                fs = File.OpenWrite(logFilePath)
            End If

            Dim lines = streamValue.StandardOutput.Split(Environment.NewLine)

            Dim result As String = String.Empty

            For Each line As String In lines
                If line.Trim.StartsWith("200") Or line.Trim.StartsWith("226") Then
                    result = result + line + Environment.NewLine
                End If
            Next

            Dim logString As String = "PUT FTP " + ftpHostName + " " + fileName + " " + Format(Now, "MMddyyHHmmss") + Environment.NewLine + result + Environment.NewLine + streamValue.ErrorOutput + "----------------------------------------------------------" + Environment.NewLine
            Dim bytes As Byte() = New UTF8Encoding(True).GetBytes(logString)
            fs.Position = fs.Length
            fs.Write(bytes, 0, bytes.Length)
            fs.Flush()
            fs.Close()
        End If
    End Sub

    Private Function ExecuteShellAsync(ByVal command As String) As String
        Dim lockObj As New Object

        Dim myProcess As Process = Nothing
        Dim sIn As StreamWriter = Nothing

        Try
            myProcess = New Process
            Dim output As StringBuilder = New StringBuilder()

            myProcess.StartInfo.FileName = "cmd.exe"
            myProcess.StartInfo.UseShellExecute = False
            myProcess.StartInfo.CreateNoWindow = True
            myProcess.StartInfo.RedirectStandardInput = True
            myProcess.StartInfo.RedirectStandardOutput = True

            Dim outputWaitHandle As AutoResetEvent = New AutoResetEvent(False)

            AddHandler myProcess.OutputDataReceived, Sub(sender As Object, e As DataReceivedEventArgs)
                                                         If e.Data = Nothing Then
                                                             outputWaitHandle.Set()
                                                         Else
                                                             SyncLock lockObj
                                                                 output.AppendLine(e.Data)
                                                             End SyncLock
                                                         End If
                                                     End Sub

            myProcess.Start()
            sIn = myProcess.StandardInput
            sIn.AutoFlush = True
            sIn.Write(command & System.Environment.NewLine)
            sIn.Write("exit" & System.Environment.NewLine)
            myProcess.BeginOutputReadLine()
            Dim result As String = String.Empty
            If myProcess.WaitForExit(130000) AndAlso outputWaitHandle.WaitOne(130000) Then
                SyncLock lockObj
                    result = output.ToString
                End SyncLock
            Else
                result = "Time Out."
            End If

            If Not myProcess.HasExited Then
                myProcess.Kill()
            End If

            sIn.Close()
            sIn.Dispose()
            myProcess.Close()
            myProcess.Dispose()

            Return result
        Catch ex As Exception
            Result = "Command failed. " & ex.Message
            Return Result
        End Try
    End Function

    'Private Function ExecuteShellNew(ByVal command As String) As Pair
    '    Dim myProcess As Process = Nothing
    '    Dim sIn As StreamWriter = Nothing
    '    Try

    '        myProcess = New Process

    '        Dim output As StringBuilder = New StringBuilder()
    '        Dim err As StringBuilder = New StringBuilder()

    '        myProcess.StartInfo.FileName = "cmd.exe"
    '        myProcess.StartInfo.UseShellExecute = False
    '        myProcess.StartInfo.CreateNoWindow = True
    '        myProcess.StartInfo.RedirectStandardInput = True
    '        myProcess.StartInfo.RedirectStandardOutput = True
    '        myProcess.StartInfo.RedirectStandardError = True

    '        Dim outputWaitHandle As AutoResetEvent = New AutoResetEvent(False)
    '        Dim errorWaitHandle As AutoResetEvent = New AutoResetEvent(False)

    '        AddHandler myProcess.OutputDataReceived, Sub(sender As Object, e As DataReceivedEventArgs)
    '                                                     If e.Data = Nothing Then
    '                                                         outputWaitHandle.Set()
    '                                                     Else
    '                                                         output.AppendLine(e.Data)
    '                                                     End If
    '                                                 End Sub

    '        AddHandler myProcess.ErrorDataReceived, Sub(sender As Object, e As DataReceivedEventArgs)
    '                                                    If e.Data = Nothing Then
    '                                                        errorWaitHandle.Set()
    '                                                    Else
    '                                                        err.AppendLine(e.Data)
    '                                                    End If
    '                                                End Sub
    '        myProcess.Start()
    '        sIn = myProcess.StandardInput
    '        sIn.AutoFlush = True
    '        sIn.Write(command & System.Environment.NewLine)
    '        sIn.Write("exit" & System.Environment.NewLine)
    '        myProcess.BeginOutputReadLine()
    '        myProcess.BeginErrorReadLine()
    '        Dim pair = New Pair()
    '        If myProcess.WaitForExit(15000) AndAlso outputWaitHandle.WaitOne(15000) AndAlso errorWaitHandle.WaitOne(15000) Then
    '            pair.StandardOutput = output.ToString
    '            pair.ErrorOutput = err.ToString
    '        Else
    '            pair.StandardOutput = ""
    '            pair.ErrorOutput = "Time Out"
    '        End If

    '        sIn.Close()
    '        sIn.Dispose()
    '        myProcess.Close()
    '        myProcess.Dispose()
    '        Return pair
    '    Catch ex As Exception
    '        Dim pair = New Pair()
    '        pair.StandardOutput = ""
    '        pair.ErrorOutput = "Command failed. " & ex.Message
    '        Return pair
    '    Finally
    '        If sIn IsNot Nothing Then
    '            sIn.Close()
    '            sIn.Dispose()
    '        End If

    '        If sIn IsNot Nothing Then
    '            myProcess.Close()
    '            myProcess.Dispose()
    '        End If
    '    End Try
    'End Function


    Private Function ExecuteShell(ByVal command As String) As String
        Try
            Dim myProcess As Process = New Process
            Dim result As String
            myProcess.StartInfo.FileName = "cmd.exe"
            myProcess.StartInfo.UseShellExecute = False
            myProcess.StartInfo.CreateNoWindow = True
            myProcess.StartInfo.RedirectStandardInput = True
            myProcess.StartInfo.RedirectStandardOutput = True
            myProcess.StartInfo.RedirectStandardError = True
            myProcess.Start()
            Dim sIn As StreamWriter = myProcess.StandardInput
            sIn.AutoFlush = True
            Dim sOut As StreamReader = myProcess.StandardOutput
            sIn.Write(command & System.Environment.NewLine)
            sIn.Write("exit" & System.Environment.NewLine)
            result = sOut.ReadToEnd()
            If Not myProcess.HasExited Then
                myProcess.Kill()
            End If
            sIn.Close()
            sOut.Close()
            myProcess.Close()
            Return result
        Catch ex As Exception
            Return "Command failed. " & ex.Message
        End Try
    End Function



    Public Sub Dispose() Implements IDisposable.Dispose
        MyBase.Finalize()
    End Sub

End Class
Public Class Pair
    Public StandardOutput As String
    Public ErrorOutput As String
End Class
