Imports System.Text.RegularExpressions
Imports System.IO

Public Class Form1


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ftp As New FtpObj.FtpObj(txtURL.Text, txtPort.Text, txtID.Text, txtPass.Text)
        Try

            ftp.DoGetFilesByExtension(txtRemote.Text, txtBack.Text, txtlocal.Text, "txt", chkDelete.Checked)
            MsgBox("Completed!")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        txtTrace.Text = ftp.Result
    End Sub




    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim ftp As New FtpObj.FtpObj(txtURL.Text, txtPort.Text, txtID.Text, txtPass.Text)
        Try
            Dim l As New List(Of String)
            l = ftp.GetDevices(txtRemote.Text)
            MsgBox("Completed!")

            For Each s As String In l
                txtTrace.Text += s & vbCrLf
            Next

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        txtTrace.Text += vbCrLf + "---------------" + ftp.Result
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim ftp As New FtpObj.FtpObj(txtURL.Text, txtPort.Text, txtID.Text, txtPass.Text)
        Try

            ftp.DoGetFilesByExtensionAll(txtRemote.Text, txtlocal.Text, "xml", chkDelete.Checked)
            MsgBox("Completed!")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        txtTrace.Text = ftp.Result
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        Dim ftp As New FtpObj.FtpObj(txtURL.Text, txtPort.Text, txtID.Text, txtPass.Text)

        ftp.DoPutFolder(txtRemote.Text, txtlocal.Text, "D:\Dropbox\Work\CMX OFFICE COMP\COM FLUOR\FTP\ARCHIVE", chkDelete.Checked)
        ' ''Dim files As String() = Directory.GetFiles(txtlocal.Text)
        ' ''For Each fl As String In files
        ' ''    Dim tmp As String = Path.GetFileName(fl)
        ' ''    ftp.DoPut(txtRemote.Text, tmp, "", txtlocal.Text, "", chkDelete.Checked)
        ' ''Next
    End Sub

End Class
