Public Class Settings

    Public Shared Function Log(ByVal MessageVal As String) As Boolean
        Try
            Using com As New SqlClient.SqlCommand("INSERT INTO HostPlus_Log  (Description) VALUES (@Message)", New SqlClient.SqlConnection(Connection))
                com.Parameters.Add("@Message", SqlDbType.NVarChar).Value = MessageVal
                com.Connection.Open() : com.ExecuteNonQuery() : com.Connection.Close()
            End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared ReadOnly Property Server() As String
        Get
            Try
                Using ds As New DataSet
                    ds.ReadXml(IO.Path.Combine(My.Application.Info.DirectoryPath, "Settings.xml"))
                    Return ds.Tables(0).Rows(0).Item("Server").ToString
                End Using
            Catch ex As Exception
                Return "Error: " & ex.Message
            End Try
        End Get
    End Property

    Public Shared ReadOnly Property Connection() As String
        Get
            Try
                Using ds As New DataSet
                    ds.ReadXml(IO.Path.Combine(My.Application.Info.DirectoryPath, "Settings.xml"))
                    Return ds.Tables(0).Rows(0).Item("Connection").ToString
                End Using
            Catch ex As Exception
                Return "Error: " & ex.Message
            End Try
        End Get
    End Property

End Class