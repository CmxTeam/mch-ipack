﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace USPSBLL.Void.EntityClasses
{
    public class RefundRequest
    {
        public string AccountID;
        public string PassPhrase;
        public string Test;
        [XmlArrayItem("PICNumber")]
        public List<string> RefundList;
    }

    public class RefundResponse
    {
        public string AccountID;
        public string ErrorMsg;
        public string FormNumber;
        public string Test;

        public List<PICNumber> RefundList;
    }

    public class PICNumber
    {
        public string picNumber;
        public string IsApproved;
        public string ErrorMsg;
    }
}
