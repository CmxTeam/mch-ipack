﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using ShipmentEntities;
using ShippingInterface;
using USPSBLL.USPSTrackVoidService;

namespace USPSBLL.Void
{
    public class USPSVoidService : IVoidService
    {
        public VoidResult VoidShipment(Credentials creds, string shipmentIdentificationNumber, out List<VoidResult> packResults,
            string[] trackingNumbers = null)
        {
            //Send refund request for each package.
            if (trackingNumbers == null)
            {
                throw new ApplicationException("Missing Carrier shipment data.");
            }
            packResults = null;

            bool success = true;
            string status = "";
            foreach (var trackNo in trackingNumbers)
            {
                if (packResults == null)
                    packResults = new List<VoidResult>();
                VoidResult res = ProcessVoid(creds, trackNo);
                packResults.Add(res);

                if (!res.IsVoided)
                {
                    success = false;
                    status = res.StatusDescription;
                }
            }

            if (string.IsNullOrEmpty(status))
            {
                status = (packResults != null && packResults.Count > 0) ? packResults[0].StatusDescription : "";
            }
            return new VoidResult() { IsVoided = success, StatusDescription = status };
        }

        public static VoidResult ProcessVoid(Credentials creds, string trackingNumber)
        {
            var svc = new ELSServicesService { Url = ConfigurationManager.AppSettings["USPSAdditionalServiceURL"] };

            var req = new EntityClasses.RefundRequest()
                      {
                          AccountID = creds.Username,
                          PassPhrase = creds.Password,
                          Test = ConfigurationManager.AppSettings["IsTestEnvironment"],
                          RefundList = new List<string>() { trackingNumber }
                      };

            var serializer = new XmlSerializer(typeof(EntityClasses.RefundRequest));
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var textWriter = new StringWriter();

            serializer.Serialize(textWriter, req, ns);

            var response = (XmlNode[])svc.RefundRequest(textWriter.ToString());


            var xmlString = (from res in response
                             where res.NodeType == XmlNodeType.Element
                             select res.OuterXml).Aggregate((x, y) => x + "\n" + y);

            serializer = new XmlSerializer(typeof(EntityClasses.RefundResponse));
            StringReader reader = new StringReader(xmlString);
            var resp = (EntityClasses.RefundResponse)serializer.Deserialize(reader);

            if (!string.IsNullOrEmpty(resp.ErrorMsg))
            {
                return new VoidResult { EntityCarrierNo = trackingNumber, IsVoided = false, StatusDescription = resp.ErrorMsg };
            }

            if (resp.RefundList[0].IsApproved.ToUpper() == "NO")
                return new VoidResult { EntityCarrierNo = trackingNumber, IsVoided = false, StatusDescription = resp.RefundList[0].ErrorMsg };

            return new VoidResult { EntityCarrierNo = trackingNumber, IsVoided = true, StatusDescription = resp.RefundList[0].ErrorMsg };
        }
    }
}
