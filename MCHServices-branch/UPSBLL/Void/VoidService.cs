﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Services.Protocols;
using ShipmentEntities;
using UPSBLL.UPSShippingService;
using UPSBLL.UPSVoidService;
using UPSBLL.Utilities;
using ShippingInterface;
using RequestType = UPSBLL.UPSVoidService.RequestType;
using TransactionReferenceType = UPSBLL.UPSVoidService.TransactionReferenceType;
using UPSSecurity = UPSBLL.UPSVoidService.UPSSecurity;
using UPSSecurityServiceAccessToken = UPSBLL.UPSVoidService.UPSSecurityServiceAccessToken;
using UPSSecurityUsernameToken = UPSBLL.UPSVoidService.UPSSecurityUsernameToken;

namespace UPSBLL.Void
{
    /// <summary>
    /// Provides interface with UPS Void service.
    /// </summary>
    public class UPSVoidService : IVoidService
    {
        /// <summary>
        /// Process shipment void in UPS.
        /// </summary>
        ///  <param name="creds">UPS Account credentials.</param>
        /// <param name="shipmentIdentificationNumber">Shipment's identification number.</param>
        /// <param name="packResult">Output parameter. Used for returning results on package level.</param>
        /// <param name="trackingNumbers">Tracking numbers.</param>
        public static VoidResult ProcessVoid(Credentials creds, string shipmentIdentificationNumber, out List<VoidResult> packResult, string[] trackingNumbers = null)
        {
            var voidService = new VoidService();
            var voidRequest = new VoidShipmentRequest();
            voidService.Url = ConfigurationManager.AppSettings["VoidServiceURL"];
            var request = new RequestType();


            //Getting Security Token.
            var upss = new UPSSecurity();
            var upssSvcAccessToken = new UPSSecurityServiceAccessToken { AccessLicenseNumber = creds.AccessKey };
            upss.ServiceAccessToken = upssSvcAccessToken;
            var upssUsrNameToken = new UPSSecurityUsernameToken
                                   {
                                       Username =
                                           creds.Username,
                                       Password =
                                           creds.Password
                                   };
            upss.UsernameToken = upssUsrNameToken;

            voidService.UPSSecurityValue = upss;

            //Prepare reuest.
            String[] requestOption = { "1" };   //Value not used. Set as "1" based on UPS sample code.
            request.RequestOption = requestOption;

            // Create TransactionRef
            var tranRef = new TransactionReferenceType { CustomerContext = UPSConstants.CMXCustomerContext };

            request.TransactionReference = tranRef;

            voidRequest.Request = request;
            var voidShipment = new VoidShipmentRequestVoidShipment
                               {
                                   ShipmentIdentificationNumber =
                                       shipmentIdentificationNumber
                               };

            if (trackingNumbers != null)
            {
                voidShipment.TrackingNumber = trackingNumbers;
            }

            voidRequest.VoidShipment = voidShipment;
            SSLValidator.OverrideValidation();

            VoidShipmentResponse voidResponse;
            //Send Request
            try
            {
                voidResponse = voidService.ProcessVoid(voidRequest);
            }
            catch (SoapException soapEx)
            {
                String code = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerText;
                String description = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[1].InnerText;
                String exSeverity = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                ExceptionSeverity severity = ShippingException.GetSeverity(exSeverity);

                throw new ShippingException(severity, code, description, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                throw new ShippingException(ex);
            }

            packResult = new List<VoidResult>();

            if (voidResponse.Response.ResponseStatus.Code == "1")
            {
                if (voidResponse.PackageLevelResult != null)
                {
                    packResult.AddRange(voidResponse.PackageLevelResult.Select(vp => new VoidResult()
                    {
                        EntityCarrierNo = vp.TrackingNumber,
                        IsVoided = (vp.Status.Code == "1"),
                        StatusDescription = vp.Status.Description
                    }));
                }

                return new VoidResult()
                {
                    IsVoided = true,
                    EntityCarrierNo = shipmentIdentificationNumber,
                    StatusDescription = voidResponse.Response.ResponseStatus.Description
                };
            }
            else
            {
                throw new ShippingException(ExceptionSeverity.Hard, voidResponse.Response.ResponseStatus.Code,
                    voidResponse.Response.ResponseStatus.Description, MethodBase.GetCurrentMethod().Name);
            }

        }

        public VoidResult VoidShipment(Credentials creds, string shipmentIdentificationNumber, out List<VoidResult> packResults, string[] trackingNumbers = null)
        {
            return ProcessVoid(creds, shipmentIdentificationNumber, out packResults, trackingNumbers);
        }

    }
}
