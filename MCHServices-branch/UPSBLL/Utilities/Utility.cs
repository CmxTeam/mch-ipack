﻿namespace UPSBLL.Utilities
{
    //Statuses- UPS services are SUPER consistent!
    public static class ValidateAddressStatus
    {
        public const string Success = "0";
        public const string Failure = "1";
    }

    public static class Status
    {
        public const string Success = "1";
        public const string Failure = "0";
    }

    public static class UPSConstants
    {
        public const string QuantumViewRequestAction = "QvEvents";
        public const string CMXCustomerContext = "CMXContext";
    }
  
    public enum ShipmentType
    {
        Package = 1,
        Freight = 2
    }

    public static class ShipmentAddressValidate
    {
        public const string Validate = "validate";
        public const string NonValidate = "nonvalidate";
    }

    public enum LabelFormat
    {
        PDF,
        GIF
    }

    public enum ShippingType
    {
        GroundFreightShipping = 1,
        AirFreightShipping = 2
    }

    public static class Utility
    {
        public static string SafeSubstring(this string str, int start, int length = -1)
        {
            if (string.IsNullOrEmpty(str) || start > str.Length - 1 || start < 0 || length < -1)
                return "";
            else if (length == -1 || length + start > str.Length)
                return str.Substring(start);
            else
                return str.Substring(start, length);
        }
    }
}