﻿using System;

namespace UPSBLL.ExceptionHandling
{
    public class UPSException : Exception 
    {
        public string UPSSource { get; set; }
        public ErrorSeverity Severity { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorFieldName { get; set; }
    }
    public enum ErrorSeverity
    {
        Hard,
        Transient
    }
}
