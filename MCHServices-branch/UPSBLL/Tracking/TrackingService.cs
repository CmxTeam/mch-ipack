﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Services.Protocols;
using ShipmentEntities;
using UPSBLL.UPSTrackingService;
using ShippingInterface;

namespace UPSBLL.Tracking
{
    /// <summary>
    /// Provides interface to UPS Tracking Service.
    /// </summary>
    public class UPSTrackingService : ITrackingService
    {
        public List<TrackActivity> GetTracking(string inquiryNumber, TrackingType trackType, TrackingRequestType requestType, DateTime beginDate, DateTime endDate, Credentials credentials, string referenceNumber = "", string candidateBookmark = "", string shipperNumber = "",
                                        TrackAddress shipTo = null, TrackAddress shipFrom = null)
        {
            //Getting Security Token.
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = credentials.AccessKey;
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = credentials.Username;
            upssUsrNameToken.Password = credentials.Password;
            upss.UsernameToken = upssUsrNameToken;

            TrackService track = new TrackService();
            track.Url = ConfigurationManager.AppSettings["TrackingServiceURL"];
            track.UPSSecurityValue = upss;


            var tr = new TrackRequest();
            var request = new RequestType();
            String[] requestOption = { GetRequestTypeString(requestType) };
            request.RequestOption = requestOption;

            // Create TransactionRef
            var tranRef = new TransactionReferenceType { CustomerContext = Utilities.UPSConstants.CMXCustomerContext };
            request.TransactionReference = tranRef;

            tr.Request = request;
            tr.InquiryNumber = inquiryNumber;
            tr.TrackingOption = GetTrackingOption(trackType);
            tr.CandidateBookmark = candidateBookmark;
            tr.ShipperNumber = shipperNumber;
            if (!string.IsNullOrEmpty(referenceNumber))
            {
                tr.ReferenceNumber = new UPSBLL.UPSTrackingService.ReferenceNumberType() { Value = referenceNumber };
                tr.ShipmentType = new RefShipmentType() { Code = "01" };
            }

            if (beginDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                tr.PickupDateRange = new PickupDateRangeType()
                                         {
                                             BeginDate = beginDate.ToString("YYYYMMDD"),
                                             EndDate = endDate.ToString("YYYYMMDD")
                                         };
            }

            if (shipTo != null)
            {
                tr.ShipTo = new ShipToRequestType()
                                {
                                    Address = new AddressRequestType() { CountryCode = shipTo.CountryCode, PostalCode = shipTo.ZipCode }
                                };
            }

            if (shipFrom != null)
            {
                tr.ShipFrom = new ShipFromRequestType()
                {
                    Address = new AddressRequestType() { CountryCode = shipFrom.CountryCode, PostalCode = shipFrom.ZipCode }
                };
            }

            SSLValidator.OverrideValidation();
            TrackResponse trackResponse;
            try
            {
                try
                {
                    trackResponse = track.ProcessTrack(tr);
                }
                catch (SoapException soapEx)
                {
                    String code = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerText;
                    String description = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[1].InnerText;
                    String exSeverity = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                    ExceptionSeverity severity = ShippingException.GetSeverity(exSeverity);

                    throw new ShippingException(severity, code, description, MethodBase.GetCurrentMethod().Name);
                }
            }
            catch (ShippingException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ShippingException(ex);
            }

            List<TrackActivity> result;

            if (trackResponse.Response.ResponseStatus.Code == "1")
            {
                result = (from pack in trackResponse.Shipment[0].Package
                          from act in pack.Activity
                          where !string.IsNullOrEmpty(act.Status.Type)
                          select new TrackActivity()
                                {
                                    TrackingNumber = pack.TrackingNumber,
                                    ActivityLocation = ((act.ActivityLocation.Address.City ?? "") + ", " + (act.ActivityLocation.Address.StateProvinceCode ?? "") + ", " + (act.ActivityLocation.Address.City ?? "")).Trim(new[] { ' ', ',' }),
                                    ActivityTime = TrackActivity.GetActivityTime(act.Date, act.Time),
                                    ActivityCode = act.Status.Type,
                                    ActivityDescription = act.Status.Description,
                                    DeliveryPOD = trackResponse.Shipment[0].SignedForByName
                                }).ToList();
            }
            else
            {
                throw new ShippingException(ExceptionSeverity.Hard, trackResponse.Response.ResponseStatus.Code,
                    trackResponse.Response.ResponseStatus.Description, MethodBase.GetCurrentMethod().Name);
            }


            return result;
        }

        private static string GetRequestTypeString(TrackingRequestType requestType)
        {
            return ((int)requestType).ToString(CultureInfo.InvariantCulture);
        }

        private string GetTrackingOption(TrackingType trackType)
        {
            return ((int)trackType).ToString("00");
        }
    }

}
