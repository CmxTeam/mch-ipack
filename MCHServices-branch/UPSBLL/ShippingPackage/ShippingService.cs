﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Web.Services.Protocols;
using ShipmentEntities;
using UPSBLL.UPSShippingService;
using ShippingInterface;

namespace UPSBLL.ShippingPackage
{
    /// <summary>
    /// Provides interface to interact with UPS API Ship Web Service.
    /// </summary>
    public class UPSShippingService : IShippingService
    {
        //Utilizes Shipment method provided by UPS Ship API
        public static CreatedShipment SendShipment(bool validateAddress, ShipmentType shipmentEntity, LabelSpecificationType label, ReceiptSpecificationType receipt, Credentials creds)
        {
            var srv = new ShipService { Url = ConfigurationManager.AppSettings["ShipServiceURL"] };
            var shipmentRequest = new ShipmentRequest();
            var upss = new UPSSecurity();
            var upssSvcAccessToken = new UPSSecurityServiceAccessToken
            {
                AccessLicenseNumber = creds.AccessKey
            };
            upss.ServiceAccessToken = upssSvcAccessToken;
            var upssUsrNameToken = new UPSSecurityUsernameToken
            {
                Username = creds.Username,
                Password = creds.Password
            };
            upss.UsernameToken = upssUsrNameToken;
            srv.UPSSecurityValue = upss;

            var request = new RequestType();
            String[] requestOption = { validateAddress ? Utilities.ShipmentAddressValidate.Validate : Utilities.ShipmentAddressValidate.NonValidate };
            request.RequestOption = requestOption;

            // Create TransactionRef
            var tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;
            shipmentRequest.Request = request;


            shipmentRequest.Shipment = shipmentEntity;
            shipmentRequest.LabelSpecification = label;
            shipmentRequest.ReceiptSpecification = receipt;

            ShipmentResponse shipmentResponse = null;
            try
            {
                shipmentResponse = srv.ProcessShipment(shipmentRequest);
            }
            catch (SoapException soapEx)
            {
                String code = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0].InnerText;
                String description = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[1].InnerText;
                String exSeverity = soapEx.Detail.ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                ExceptionSeverity severity = ShippingException.GetSeverity(exSeverity);

                throw new ShippingException(severity, code, description, MethodBase.GetCurrentMethod().Name);
            }
            catch (Exception ex)
            {
                throw new ShippingException(ex);
            }

            if (shipmentResponse.Response.ResponseStatus.Code == Utilities.Status.Success)
            {
                var shipment = new CreatedShipment
                                   {
                                       TrackingNumbers = new List<string>(),
                                       PackageShippingLabels = new List<string>(),
                                       ShipmentIdentificationNumber =
                                          shipmentResponse.ShipmentResults.ShipmentIdentificationNumber,
                                       LabelURL = shipmentResponse.ShipmentResults.LabelURL
                                   };
                foreach (var track in shipmentResponse.ShipmentResults.PackageResults)
                {
                    shipment.TrackingNumbers.Add(track.TrackingNumber);
                    shipment.PackageShippingLabels.Add(track.ShippingLabel.GraphicImage);
                }
                return shipment;
            }
            else
            {
                throw new ShippingException(ExceptionSeverity.Hard, shipmentResponse.Response.ResponseStatus.Code,
                    shipmentResponse.Response.ResponseStatus.Description, MethodBase.GetCurrentMethod().Name);
            }
        }

        //Utilizes ShipConfirm method provided by UPS Ship API
        public static ShipConfirmResponse SendShipConfirm(bool validateAddress, ShipmentType shipmentEntity, LabelSpecificationType label, ReceiptSpecificationType receipt)
        {
            var srv = new ShipService();
            ShipConfirmRequest shipmentConfirmRequest = new ShipConfirmRequest();

            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;
            srv.UPSSecurityValue = upss;

            RequestType request = new RequestType();
            String[] requestOption = { validateAddress ? Utilities.ShipmentAddressValidate.Validate : Utilities.ShipmentAddressValidate.NonValidate };
            request.RequestOption = requestOption;

            // Create TransactionRef
            TransactionReferenceType tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;
            shipmentConfirmRequest.Request = request;


            shipmentConfirmRequest.Shipment = shipmentEntity;
            shipmentConfirmRequest.LabelSpecification = label;
            shipmentConfirmRequest.ReceiptSpecification = receipt;


            ShipConfirmResponse shipConfirmResp = null;

            try
            {
                shipConfirmResp = srv.ProcessShipConfirm(shipmentConfirmRequest);
            }
            catch (SoapException ex)
            {
                string ss = ex.Detail.InnerText;
                throw new Exception(ss);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return shipConfirmResp;
        }

        //Utilizes ShipAccept method provided by UPS Ship API
        public static ShipAcceptResponse SendShipAccept(bool validateAddress, string shipmentDigest)
        {
            var srv = new ShipService();
            ShipAcceptRequest shipmentAcceptRequest = new ShipAcceptRequest();

            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = ConfigurationManager.AppSettings["UPSAccessKey"];
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = ConfigurationManager.AppSettings["UPSUserName"];
            upssUsrNameToken.Password = ConfigurationManager.AppSettings["UPSPassword"];
            upss.UsernameToken = upssUsrNameToken;
            srv.UPSSecurityValue = upss;

            RequestType request = new RequestType();
            String[] requestOption = { validateAddress ? Utilities.ShipmentAddressValidate.Validate : Utilities.ShipmentAddressValidate.NonValidate };
            request.RequestOption = requestOption;

            // Create TransactionRef
            TransactionReferenceType tranRef = new TransactionReferenceType();
            tranRef.CustomerContext = Utilities.UPSConstants.CMXCustomerContext;
            request.TransactionReference = tranRef;
            shipmentAcceptRequest.Request = request;
            shipmentAcceptRequest.ShipmentDigest = shipmentDigest;

            ShipAcceptResponse shipAcceptResp = null;

            try
            {
                shipAcceptResp = srv.ProcessShipAccept(shipmentAcceptRequest);
            }
            catch (SoapException ex)
            {
                string ss = ex.Detail.InnerText;
                throw new Exception(ss);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return shipAcceptResp;
        }

        public CreatedShipment CreateShipment(Partner shipper, Partner shipFrom, Partner shipTo, string accountNumber, string serviceCode, List<ShipPackage> packages, Credentials creds)
        {
            var shipment = new ShipmentType();

            var lbl =
                new LabelSpecificationType() { LabelImageFormat = new LabelImageFormatType() { Code = "ZPL", Description = "ZPL" }, LabelStockSize = new LabelStockSizeType() { Height = "6", Width = "4" } };

            var shipperType = new UPSBLL.UPSShippingService.ShipperType();
            shipperType.Name = shipper.Name;
            shipperType.AttentionName = shipper.AttentionName;
            shipperType.ShipperNumber = shipper.ShipperNumber;
            shipperType.Phone = new ShipPhoneType()
                                    {
                                        Number = shipper.PhoneNumber,
                                        Extension = shipper.PhoneExtension
                                    };
            shipperType.Address = new ShipAddressType();
            shipperType.Address.AddressLine = new string[] { shipper.PartnerAddress.AddressLine };
            shipperType.Address.City = shipper.PartnerAddress.City;
            shipperType.Address.StateProvinceCode = shipper.PartnerAddress.StateProvinceCode;
            shipperType.Address.PostalCode = shipper.PartnerAddress.PostalCode;
            shipperType.Address.CountryCode = shipper.PartnerAddress.CountryCode;

            shipment.Shipper = shipperType;

            var shipFr = new ShipFromType();
            shipFr.Name = shipFrom.Name;
            shipFr.AttentionName = shipFrom.AttentionName;
            shipFr.Phone = new ShipPhoneType()
            {
                Number = shipFrom.PhoneNumber,
                Extension = shipFrom.PhoneExtension
            };
            shipFr.Address = new ShipAddressType();
            shipFr.Address.AddressLine = new string[] { shipFrom.PartnerAddress.AddressLine };
            shipFr.Address.City = shipFrom.PartnerAddress.City;
            shipFr.Address.StateProvinceCode = shipFrom.PartnerAddress.StateProvinceCode;
            shipFr.Address.PostalCode = shipFrom.PartnerAddress.PostalCode;
            shipFr.Address.CountryCode = shipFrom.PartnerAddress.CountryCode;

            shipment.ShipFrom = shipFr;

            var shipT = new UPSBLL.UPSShippingService.ShipToType
            {
                Name = shipTo.Name,
                AttentionName = shipTo.AttentionName,
                Phone = new ShipPhoneType()
                            {
                                Number = shipTo.PhoneNumber,
                                Extension = shipTo.PhoneExtension
                            },
                Address =
                    new UPSBLL.UPSShippingService.ShipToAddressType
                        {
                            AddressLine =
                                new string[] { shipTo.PartnerAddress.AddressLine },
                            City = shipTo.PartnerAddress.City,
                            StateProvinceCode =
                                shipTo.PartnerAddress.StateProvinceCode,
                            PostalCode = shipTo.PartnerAddress.PostalCode,
                            CountryCode = shipTo.PartnerAddress.CountryCode
                        }
            };

            shipment.ShipTo = shipT;

            var paym = new PaymentInfoType();
            var charge = new ShipmentChargeType()
            {
                Type = "01",
                BillShipper = new BillShipperType()
                                    {
                                        AccountNumber = accountNumber
                                    }
            };
            paym.ShipmentCharge = new ShipmentChargeType[] { charge };
            shipment.PaymentInformation = paym;

            var serv = new ServiceType() { Code = serviceCode };
            shipment.Service = serv;

            var packList = new List<UPSBLL.UPSShippingService.PackageType>();

            foreach (var pack in packages)
            {
                var pck = new UPSBLL.UPSShippingService.PackageType()
                {
                    Description = pack.Description,
                    Packaging = new PackagingType()
                                    {
                                        Code = pack.PackagingCode
                                    },
                    Dimensions = new UPSBLL.UPSShippingService.DimensionsType()
                                    {
                                        UnitOfMeasurement = new UPSBLL.UPSShippingService.ShipUnitOfMeasurementType()
                                                                {
                                                                    Code = pack.PackageDims.DimensionUOM.Code
                                                                },
                                        Width = pack.PackageDims.Width.ToString(),
                                        Height = pack.PackageDims.Height.ToString(),
                                        Length = pack.PackageDims.Length.ToString()
                                    },
                    PackageWeight = new UPSBLL.UPSShippingService.PackageWeightType()
                                        {
                                            UnitOfMeasurement = new UPSBLL.UPSShippingService.ShipUnitOfMeasurementType()
                                            {
                                                Code = pack.WeightUOM.Code
                                            },
                                            Weight = pack.Weight.ToString()
                                        },
                    ReferenceNumber = new[]
                                        {
                                            new UPSBLL.UPSShippingService.ReferenceNumberType ()
                                                {
                                                    Value = pack.RefNumber.Value 
                                                }
                                        }
                };
                packList.Add(pck);
            }

            shipment.Package = packList.ToArray();

            return SendShipment(false, shipment, lbl, null, creds);
        }
    }
}
