﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=4.0.30319.17929.
// 

namespace UPSBLL.QuantumView
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Request", Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0", IsNullable = false)]
    public partial class RequestType
    {

        private string[] requestOptionField;

        private TransactionReferenceType transactionReferenceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RequestOption")]
        public string[] RequestOption
        {
            get
            {
                return this.requestOptionField;
            }
            set
            {
                this.requestOptionField = value;
            }
        }

        /// <remarks/>
        public TransactionReferenceType TransactionReference
        {
            get
            {
                return this.transactionReferenceField;
            }
            set
            {
                this.transactionReferenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    public partial class TransactionReferenceType
    {

        private string customerContextField;

        private string transactionIdentifierField;

        /// <remarks/>
        public string CustomerContext
        {
            get
            {
                return this.customerContextField;
            }
            set
            {
                this.customerContextField = value;
            }
        }

        /// <remarks/>
        public string TransactionIdentifier
        {
            get
            {
                return this.transactionIdentifierField;
            }
            set
            {
                this.transactionIdentifierField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    public partial class CodeDescriptionType
    {

        private string codeField;

        private string descriptionField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Response", Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0", IsNullable = false)]
    public partial class ResponseType
    {

        private CodeDescriptionType responseStatusField;

        private CodeDescriptionType[] alertField;

        private TransactionReferenceType transactionReferenceField;

        /// <remarks/>
        public CodeDescriptionType ResponseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Alert")]
        public CodeDescriptionType[] Alert
        {
            get
            {
                return this.alertField;
            }
            set
            {
                this.alertField = value;
            }
        }

        /// <remarks/>
        public TransactionReferenceType TransactionReference
        {
            get
            {
                return this.transactionReferenceField;
            }
            set
            {
                this.transactionReferenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("ClientInformation", Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0", IsNullable = false)]
    public partial class ClientInformationType
    {

        private ClientInformationTypeProperty[] propertyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Property")]
        public ClientInformationTypeProperty[] Property
        {
            get
            {
                return this.propertyField;
            }
            set
            {
                this.propertyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    public partial class ClientInformationTypeProperty
    {

        private string keyField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

}