﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ShipmentEntities
{
    [DataContract]
    public class TrackAddress
    {
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
    }


    public enum TrackingType
    {
        Shipment = 1,
        Package = 2
    }

    public enum TrackingRequestType
    {
        Last = 0,
        All = 1
    }

    public class TrackActivity
    {
        public long PackageId { get; set; }
        public string TrackingNumber { get; set; }
        public string ActivityLocation { get; set; }
        public DateTime ActivityTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityCode { get; set; }

        public string DeliveryPOD { get; set; }

        public static DateTime GetActivityTime(string date, string time)
        {
            const string formatString = "yyyyMMddHHmmss";
            DateTime dt = DateTime.ParseExact(date + time, formatString, null);
            return dt;
        }
    }

    public class QVActivity
    {
        public string TrackingNumber { get; set; }

        public QVType MessageType { get; set; }
        public string ActivityLocation { get; set; }
        public DateTime ActivityTime { get; set; }
        public string ActivityDescription { get; set; }
        public string ActivityCode { get; set; }

        public static DateTime GetActivityTime(string date, string time)
        {
            string formatString = "yyyyMMddHHmmss";
            DateTime dt = DateTime.ParseExact(date + time, formatString, null);
            return dt;
        }
    }

    public enum QVType
    {
        Origin = 1,
        Delivery = 2,
        Exception = 3,
        Generic = 4,
        Manifest = 5
    }

}
