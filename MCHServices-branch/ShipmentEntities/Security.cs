﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShipmentEntities
{
    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string AccessKey { get; set; }
    }
}
