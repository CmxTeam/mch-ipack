﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShipmentEntities;

namespace ShippingInterface
{
    public interface IVoidService
    {
        VoidResult VoidShipment(Credentials creds, string shipmentIdentificationNumber, out List<VoidResult> packResults, string[] trackingNumber = null);
    }
}
