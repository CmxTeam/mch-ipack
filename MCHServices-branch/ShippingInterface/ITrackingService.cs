﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShipmentEntities;

namespace ShippingInterface
{
    public interface ITrackingService
    {
        List<TrackActivity> GetTracking(string inquiryNumber, TrackingType trackType, TrackingRequestType requestType, DateTime beginDate, DateTime endDate, Credentials credentials,
                                string referenceNumber = "", 
                                string candidateBookmark = "", string shipperNumber = "",
                                TrackAddress shipTo = null, TrackAddress shipFrom = null);
    }
}
