﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TestUPSServices.ShippingService;

using ShippingFault = TestUPSServices.ShippingService.ShippingFault;

namespace TestUPSServices
{
    class Program
    {
        static void Main(string[] args)
        {
            int carrierServiceTypeId = 1;
            string userName = "ADMIN";
            long taskId = 23;

            int carrierId = 5;
            int onhandId = 10106;


            int accountCarrierId = 2;
            int shipperCustomerId = 1;
            int shipFromCustomerId = 1;
            int shipToCustomerId = 7;

            var packageIds = new List<long>() { 20277, 20278, 20279 };
            DateTime begin = DateTime.Now;
            var client = new ShippingWSClient();
            try
            {
                
                //client.CreateShipmentForOnhand(5, 20212, 8, "TestUser", 0); 
                //client.TestVoidShipment("1Z2220060290530202", new string[] { "1Z2220060293874210", "1Z2220060292634221" }, 5, 40390);
                //1Z2220060290602143
                //client.VoidShipment(5, 40391, "TestUser");
                //client.GetLastTracking(5, 154, TrackingType.Package, "ADMIN", 23);
                client.CreateShipmentForOnhand(5, 40419, 8, "TestUser", 20090); 
                //client.GetLastTracking(carrierId, 110, TrackingType.Package, userName, taskId);

            }
            catch (FaultException<ShippingFault> faultException)
            {
                ShippingFault fault = faultException.Detail;

                string exCode = fault.ExceptionCode; //"CMX" if internal error. UPS error number, if UPS error.
                string exDescription = fault.ExceptionDescription;
                string exSource = fault.ExceptionSource; // Method name where exception occured if available, else exception StackTrace.
                ExceptionSeverity exSeverity = fault.Severity; // ExceptionSeverity.Hard - for CMX errors. Appropriate severity, if exception comes from UPS.

                string exData = "";

                //For unknown error from CMX- inner exception string will contain ex.ToString() value- 
                //includes exception description and stack trace.
                if (exCode == "CMX")
                {
                    exData = fault.InnerException;
                }
            }
            catch (Exception ex)
            {
                string exCode = ex.ToString();
            }
            var end = DateTime.Now - begin;
            Console.WriteLine(end);
            Console.ReadKey();
        }

        //client.CreateShipmentForOnhand(5, 20212, 8, "Test User", 17); // Should work.
        //client.CreateShipmentForOnhand(5, 20245, 8, "Test User", 20090); //Should fail.

        //client.TestGetTracking(accountCarrierId, "1Z648616E192760718", TrackingType.Shipment , TrackingRequestType.Last, userName, taskId);

        //client.GetLastTracking(accountCarrierId, 154, TrackingType.Package,userName, taskId);

        //client.CreateShipmentForOnhand(carrierId, onhandId, carrierServiceTypeId, userName, taskId);
    }
}
