﻿using System;
using System.Diagnostics;

namespace ShippingWS
{
    public class Logger
    {
        public static string LogError(string message, string stackTrace, DateTime date)
        {
            try
            {
                string errorNo = DateTime.Now.ToString("yyyyMMddHHmmss");

                Trace.WriteLine(Environment.NewLine + "Error number " + errorNo + " occured at: "
                    + DateTime.Now.ToString() + Environment.NewLine + "Error message: " + message +
                    Environment.NewLine + "Stack trace: " + stackTrace
                    + Environment.NewLine + "---------------------------" + Environment.NewLine);
                Trace.Flush();
                return errorNo;
            }
            catch
            {
            }
            return "";
        }
    }

}