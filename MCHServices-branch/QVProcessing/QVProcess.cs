﻿using System;
using System.Collections.Generic;
using System.Linq;
using ShipmentEntities;
using ShippingInterface;
using UPSBLL;

namespace QVProcessing
{
    public class QVProcess : IDisposable
    {
        public QVProcess(string connectionString, string qvServiceUrl, int accountCarrierId)
        {
            Connection = connectionString;
            QVUrl = qvServiceUrl;
            AccountCarrierId = accountCarrierId;
        }
        #region Fields

        public void ProcessQVEvents()
        {
            try
            {
                using (var db = new MCHDBDataContext(Connection))
                {
                    IQuantumViewService srv;

                    AccountCarrier acarr = (from acccar in db.AccountCarriers
                                            where acccar.Id.Equals(AccountCarrierId)
                                            select acccar).FirstOrDefault();

                    if (acarr == null)
                    {
                        throw new ApplicationException("Incorrect carrier Id.");
                    }

                    if (acarr.Carrier.Carrier3Code == "UPS")
                        srv = new UPSQuantumViewService();
                    else
                    {
                        throw new ApplicationException("Carrier not supported.");
                    }

                    List<QVActivity> result = srv.GetQVData(QVUrl, GetUPSCredentials(acarr.Id));

                    EntityType entType =
                                    (from types in db.EntityTypes where types.EntityName.Equals("Package") select types).FirstOrDefault();

                    foreach (var activity in result)
                    {
                        var tmpEvent = new Event();
                        var onhTr = new OnhandTransaction();
                        EventCode code = null;

                        Manifest mn = (from man in db.Manifests
                                       where man.Number.Equals(activity.TrackingNumber)
                                       select man).FirstOrDefault();


                        if (mn == null)
                            continue;

                        if (mn.Packages.Count == 0)
                            continue;
                        tmpEvent.EntityId = mn.Packages[0].Id;
                        tmpEvent.EntityType = entType;
                        tmpEvent.EventLocation = activity.ActivityLocation;
                        tmpEvent.RecDate = activity.ActivityTime;
                        tmpEvent.UserName = "UPS";
                        tmpEvent.IsTransmitted = 0;
                        tmpEvent.IsEmailed = 0;
                        tmpEvent.AccountId = acarr.AccountId;


                        onhTr.Onhand = mn.Packages[0].Onhand;
                        onhTr.Reference = activity.TrackingNumber;
                        onhTr.Package = mn.Packages[0];
                        onhTr.StatusTimestamp = activity.ActivityTime;
                        onhTr.TaskTypeId = 7;
                        onhTr.UserName = "UPS";

                        switch (activity.MessageType)
                        {
                            case QVType.Origin:
                                {
                                    code = (from codes in db.EventCodes
                                            where codes.EventName.Equals("Origin Scan")
                                            select codes).FirstOrDefault();
                                    if (code == null)
                                        continue;
                                    tmpEvent.EventReference = code.EventName;
                                    onhTr.Description = (code.EventName + ", " + activity.ActivityLocation).Trim(new[] { ' ', ',' });

                                    break;
                                }
                            case QVType.Delivery:
                                {
                                    code = (from codes in db.EventCodes
                                            where codes.EventName.Equals("Delivered")
                                            select codes).FirstOrDefault();
                                    if (code == null)
                                        continue;

                                    tmpEvent.EventReference = code.EventName;
                                    tmpEvent.EventPOD = activity.ActivityDescription;

                                    onhTr.Description = (code.EventName + ", " + activity.ActivityLocation + ", " + activity.ActivityDescription).Trim(new[] { ' ', ',' });

                                    mn.DeliveredOn = activity.ActivityTime;
                                    mn.DeliveredTo = activity.ActivityDescription;

                                    break;
                                }
                            case QVType.Exception:
                                {
                                    code = (from codes in db.EventCodes
                                            where codes.EventName.Equals("Exception")
                                            select codes).FirstOrDefault();
                                    if (code == null)
                                        continue;

                                    tmpEvent.EventReference = activity.ActivityDescription;

                                    onhTr.Description = activity.ActivityDescription;

                                    break;
                                }
                            case QVType.Generic:
                                {
                                    string codeName = GetCodeName(activity.ActivityCode);
                                    if (string.IsNullOrEmpty(codeName))
                                        continue;

                                    code = (from codes in db.EventCodes
                                            where codes.EventName.Equals(codeName)
                                            select codes).FirstOrDefault();
                                    if (code == null)
                                        continue;

                                    tmpEvent.EventReference = code.EventName;

                                    onhTr.Description = (code.EventName + ", " + activity.ActivityLocation).Trim(new[] { ' ', ',' });

                                    break;
                                }
                            default:
                                continue;

                        }

                        tmpEvent.EventCode = code;
                        onhTr.Statuse = code.Statuse;
                        if (code.StatusId_.HasValue)
                            mn.Packages[0].StatusId = code.StatusId_.Value;

                        db.OnhandTransactions.InsertOnSubmit(onhTr);
                        db.Events.InsertOnSubmit(tmpEvent);
                    }
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("QV processing failed: " + ex.ToString());
            }
        }

        private string GetCodeName(string code)
        {
            switch (code)
            {
                case "VM":
                    return "Shipment Information Voided";
                case "TC":
                    return "Transferred to Local Post Office for Delivery";
                case "PS":
                    return "In Transit by Local Post Office";
                default:
                    return "";
            }
        }


        public Credentials GetUPSCredentials(int accountCarrierId)
        {
            using (var db = new MCHDBDataContext(Connection))
            {
                var accCar = (from accCarrs in db.AccountCarriers
                              where accCarrs.Id.Equals(accountCarrierId)
                              select new Credentials()
                              {
                                  Username = accCarrs.AccountLogin,
                                  Password = accCarrs.AccountPassword,
                                  AccessKey = accCarrs.AccessKey
                              }

                          ).FirstOrDefault();
                return accCar;
            }
        }

        public string Connection { get; set; }

        public string QVUrl { get; set; }

        public int AccountCarrierId { get; set; }
        #endregion
        #region IDisposable Implementation
        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.

                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                _disposed = true;

            }
        }

        ~QVProcess()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion
    }
}
