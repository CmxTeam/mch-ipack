﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[DataContract]
public class Prealert
{

    public PrealertAV7 AV7 { get; set; }

    [XmlArray("Bags")]
    [XmlArrayItem("Bag")]
    public List<Bag> Bags { get; set; }

    [XmlArray("Shipments")]
    [XmlArrayItem("Shipment")]
    public List<Shipment> Shipments { get; set; }

    [XmlArray("InvoiceFiles", IsNullable = true)]
    [XmlArrayItem("InvoiceFileName")]
    public List<InvoiceFileName> InvoiceFiles { get; set; }
}

[DataContract]
public class Bag
{
    public string Number { get; set; }
    public double TotalWeight { get; set; }
    public string UOM { get; set; }
    public int TotalPackages { get; set; }

    [XmlArray("TrackingNumbers")]
    [XmlArrayItem("TrackingNumber")]
    public List<TrackingNumber> TrackingNumbers { get; set; }

}

[DataContract]
public class Shipment
{

    public string Id { get; set; }

    public int TotalPackages { get; set; }

    public string ServiceType { get; set; }

    public string GoodsDescription { get; set; }

    public PrealertReceiverDetails ReceiverDetails { get; set; }

    [XmlArray("TrackingNumbers")]
    [XmlArrayItem("TrackingNumber")]
    public List<TrackingNumber> TrackingNumbers { get; set; }
}

[DataContract]
public class PrealertAV7
{

    public string AV7ID { get; set; }

    public string MAWB { get; set; }

    public string FlightNo { get; set; }

    public string Origin { get; set; }

    [XmlIgnore]
    public System.DateTime ETD { get; set; }

    [XmlElement("ETD")]
    public string ETDString
    {
        get
        {
            return ETD.ToString("MM/dd/yyyy HH:mm");
        }
        set { this.ETD = DateTime.ParseExact(value, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture); }
    }

    public string Destination { get; set; }


    [XmlIgnore]
    public System.DateTime ETA { get; set; }

    [XmlElement("ETA")]
    public string ETAString
    {
        get
        {
            return ETA.ToString("MM/dd/yyyy HH:mm");
        }
        set { this.ETA = DateTime.ParseExact(value, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture); }
    }

    public int TotalBags { get; set; }

    public int TotalShipments { get; set; }

    public int TotalPackages { get; set; }

    public double TotalWeight { get; set; }

    public string UOM { get; set; }
}

[DataContract]
public class PrealertReceiverDetails
{

    public string CompanyName { get; set; }

    public string AttentionOf { get; set; }

    public string Address1 { get; set; }

    public string Address2 { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string ZipCode { get; set; }

    public string Country { get; set; }

    public string Phone { get; set; }

    public string Fax { get; set; }

    public string Email { get; set; }

    public int IsResidental { get; set; }
}

[DataContract]
public class TrackingNumber
{

    public string Number { get; set; }

    public double Weight { get; set; }

    public string UOMWeight { get; set; }

    public string BAGNumber { get; set; }

    public string PackageType { get; set; }

    public double Length { get; set; }

    public double Width { get; set; }

    public double Height { get; set; }

    public string UOMDim { get; set; }
}

[DataContract]
public class InvoiceFileName
{
    [XmlText()]
    public string Text { get; set; }
}
