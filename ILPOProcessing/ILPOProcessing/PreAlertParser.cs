﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace PreAlertProcessing
{
    /// <summary>
    /// Processes prealert xml files to CMX database.
    /// </summary>
    public class PreAlertParser : IDisposable
    {
        #region Public Methods

        /// <summary>
        /// Parses xml files from 'FolderPath' folder.
        /// </summary>
        public void ParseFolder()
        {
            string errorStr = "";
            if (!Directory.Exists(FolderPath))
            {
                throw new ApplicationException("Incorrect folder path: " + FolderPath);
            }

            string[] tmpFiles = Directory.GetFiles(FolderPath, "*.xml");

            foreach (string fileName in tmpFiles)
            {
                bool errorCopied = false;
                try
                {
                    //Process File.
                    Prealert preAlert = Parse(fileName);
                    string prealertNo;
                    if (SavePrealert(preAlert, out prealertNo))
                    {
                        UtilityMethods.SendAV7Email(prealertNo, fileName, GlobalConnectionString,LabelFolderRoot);
                        string archiveFileName = Path.Combine(ArchiveFolderPath,
                                                              Path.GetFileNameWithoutExtension(fileName) + "_" +
                                                              DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture) +
                                                              Path.GetExtension(fileName));
                        File.Move(fileName, archiveFileName);
                    }
                    else
                    {
                        if (!Directory.Exists(Path.Combine(FolderPath, "ERRORS")))
                        {
                            Directory.CreateDirectory(Path.Combine(FolderPath, "ERRORS"));
                        }
                        string errorFileName = Path.Combine(FolderPath, "ERRORS",
                                      Path.GetFileNameWithoutExtension(fileName) + "_" +
                                      DateTime.Now.Ticks.ToString() +
                                      Path.GetExtension(fileName));
                        File.Move(fileName, errorFileName);
                        errorCopied = true;
                    }
                }
                catch (Exception ex)
                {
                    if (!Directory.Exists(Path.Combine(FolderPath, "ERRORS")))
                    {
                        Directory.CreateDirectory(Path.Combine(FolderPath, "ERRORS"));
                    }
                    if (!errorCopied)
                    {
                        string errorFileName = Path.Combine(FolderPath, "ERRORS",
                                      Path.GetFileNameWithoutExtension(fileName) + "_" +
                                      DateTime.Now.Ticks.ToString() +
                                      Path.GetExtension(fileName));
                        File.Move(fileName, errorFileName);
                    }
                    errorStr += ex.ToString() + Environment.NewLine;
                }
            }

            if (!string.IsNullOrEmpty(errorStr))
            {
                throw new ApplicationException(errorStr.Trim());
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="connecttion">Db connection string.</param>
        /// <param name="globalConnection">Db connection string for global DB.</param>
        /// <param name="folder">Folder path to xml files.</param>
        /// <param name="archiveFolder">Folder path for archiving the files after processing.</param>
        /// <param name="accountCode">Account code</param>
        /// <param name="productType">Product Type - 'EMS' / 'EMSP'</param>
        public PreAlertParser(string connecttion, string globalConnection, string folder, string archiveFolder, string labelFolderRoot, string productType = "EMSP", string accountCode = "ILPO")
        {
            ConnectionString = connecttion;
            GlobalConnectionString = globalConnection;
            FolderPath = folder;
            ArchiveFolderPath = archiveFolder;
            AccountCode = accountCode;
            ProductType = productType;
            LabelFolderRoot = labelFolderRoot;
        }

        #endregion

        #region Fields

        /// <summary>
        /// DB connection string.
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// Connection string for global DB.
        /// </summary>
        public string GlobalConnectionString { get; set; }
        /// <summary>
        /// Path to files to process.
        /// </summary>
        public string FolderPath { get; set; }
        /// <summary>
        /// Path to archive the files after processing.
        /// </summary>
        public string ArchiveFolderPath { get; set; }
        /// <summary>
        /// Root path to labels folders.
        /// </summary>
        public string LabelFolderRoot { get; set; }


        /// <summary>
        /// Account code.
        /// </summary>
        public string AccountCode { get; set; }
        /// <summary>
        /// Product Type - is EMS or EMS Platinum (EMSP) pre-alert.
        /// </summary>
        public string ProductType { get; set; }

        #endregion

        #region Private Methods

        private Prealert Parse(string fileName)
        {
            FileStream fs = null;

            try
            {
                fs = new FileStream(fileName, FileMode.Open);
                var alert = new Prealert();
                var serializer = new XmlSerializer(alert.GetType());
                alert = (Prealert)serializer.Deserialize(fs);
                return alert;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to read xml file: " + fileName + ". Error: " + ex.ToString());
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        private bool SavePrealert(Prealert preAlert, out string preAlertNo)
        {
            try
            {
                string AV7Str = "";
                var internalError = new InternalHardError() { GlobalConnection = GlobalConnectionString };
                var externalError = new ExternalHardError() { GlobalConnection = GlobalConnectionString };
                if (string.IsNullOrEmpty(preAlert.AV7.AV7ID))
                {
                    externalError.AddErrorMessage("Missing AV7 number.");
                }
                else
                {
                    externalError.AV7 = AV7Str;
                    internalError.AV7 = AV7Str;
                    AV7Str = preAlert.AV7.AV7ID;
                }

                using (var db = new DBDataContext(ConnectionString))
                {
                    Account acc =
                        (from accs in db.Accounts where accs.AccountCode.ToUpper().Equals(AccountCode) select accs)
                            .FirstOrDefault();

                    if (acc == null)
                    {
                        internalError.AddErrorMessage("Missing account: " + AccountCode);
                    }

                    bool flManifestExists = true;

                    var flManifest =
                        (from fm in db.FlightManifests
                         where fm.AircraftRegistration.ToUpper().Equals(AV7Str) && fm.Account.Equals(acc) && fm.ProductType.Equals(ProductType)
                         select fm).FirstOrDefault();

                    if (flManifest == null)
                    {
                        //status 23 - customs pending
                        flManifest = new FlightManifest()
                                         {
                                             AircraftRegistration = AV7Str,
                                             ProductType = ProductType,
                                             Account = acc,
                                             StatusId = 23,
                                             StatusTimestamp = DateTime.UtcNow,
                                             RecDate = DateTime.UtcNow,
                                             ReceivedPieces = 0,
                                             RecoveredULDs = 0,
                                             PutAwayPieces = 0,
                                             PercentRecovered = 0,
                                             PercentReceived = 0,
                                             PercentPutAway = 0,
                                             PercentOverall = 0
                                         };
                        flManifestExists = false;
                    }
                    else if ((flManifest.StatusId != null) && flManifest.StatusId != 23)
                    {
                        internalError.AddErrorMessage("AV7 already exists with status != 23 (Customs Pending).");
                    }

                    flManifest.ETD = preAlert.AV7.ETD;
                    flManifest.ETA = preAlert.AV7.ETA;


                    string carrierCode = "";
                    string mawbNo = "";


                    if (string.IsNullOrEmpty(preAlert.AV7.FlightNo))
                    {
                        externalError.AddErrorMessage("Missing flight number.");
                    }
                    else
                    {
                        carrierCode = preAlert.AV7.FlightNo.SafeSubstring(0, 2);
                        flManifest.FlightNumber = preAlert.AV7.FlightNo.Replace(carrierCode, "");
                    }

                    if (string.IsNullOrEmpty(preAlert.AV7.MAWB))
                    {
                        //First phase- we don't receive Mawb No from ILPO.
                        mawbNo = preAlert.AV7.AV7ID;
                    }
                    else
                    {
                        //Get carrier code and mawb number from MAWB field. Format: <car>-<mawbNo>.
                        int delimiter = preAlert.AV7.MAWB.IndexOf("-", StringComparison.Ordinal);

                        if (delimiter < 0)
                        {
                            externalError.AddErrorMessage("Incorrect MAWB format: " + preAlert.AV7.MAWB);
                        }
                        else
                        {
                            //carrierCode = preAlert.AV7.MAWB.Substring(0, delimiter);
                            mawbNo = preAlert.AV7.MAWB.Substring(delimiter + 1, preAlert.AV7.MAWB.Length - delimiter - 1);
                        }
                    }

                    Carrier carr =
                        (from car in db.Carriers where car.CarrierCode.ToUpper().Equals(carrierCode.ToUpper()) select car).FirstOrDefault()
                        ??
                        new Carrier()
                        {
                            CarrierCode = carrierCode,
                            MOTId = 1
                        };
                    flManifest.Carrier = carr;

                    //Get origin and destination ports.
                    Port origin = (from p in db.Ports where p.IATACode.Equals(preAlert.AV7.Origin) select p).FirstOrDefault();
                    if (origin == null)
                    {
                        Country c =
                            (from countries in db.Countries where countries.Country1 == "Israel" select countries)
                                .FirstOrDefault();
                        if (c == null)
                        {
                            internalError.AddErrorMessage("Missing origin counrty.");
                        }

                        origin = new Port()
                        {
                            IATACode = "TLV",
                            CountryId = c == null ? 0 : c.Id,
                            Port1 = "Tel Aviv"
                        };

                    }

                    Port destination = (from p in db.Ports where p.IATACode.Equals(preAlert.AV7.Destination) select p).FirstOrDefault();
                    if (destination == null)
                    {
                        Country c =
                            (from countries in db.Countries where countries.TwoCharacterCode == "US" select countries)
                                .FirstOrDefault();
                        if (c == null)
                        {
                            internalError.AddErrorMessage("Missing destination counrty.");
                        }
                        destination = new Port()
                        {
                            IATACode = "JFK",
                            CountryId = c == null ? 0 : c.Id,
                            Port1 = "New York"
                        };
                    }

                    flManifest.Port = origin;
                    flManifest.Port1 = destination;
                    flManifest.TotalAWBs = 1;
                    flManifest.TotalULDs = preAlert.AV7.TotalBags;
                    flManifest.TotalPieces = preAlert.AV7.TotalPackages;

                    if (!flManifestExists)
                        db.FlightManifests.InsertOnSubmit(flManifest);

                    bool uportExists = true;
                    UnloadingPort unloadPort =
                        (from uports in db.UnloadingPorts where uports.FlightManifest.Equals(flManifest) && uports.Port.Equals(destination) select uports)
                            .FirstOrDefault();
                    if (unloadPort == null)
                    {
                        unloadPort = new UnloadingPort()
                        {
                            Port = destination,
                            FlightManifest = flManifest,
                        };
                        uportExists = false;
                    }

                    unloadPort.POU_ETA = preAlert.AV7.ETA;
                    unloadPort.POU_ETD = preAlert.AV7.ETD;
                    unloadPort.IsNilCargo = (!preAlert.Shipments.Any());

                    if (!uportExists)
                        db.UnloadingPorts.InsertOnSubmit(unloadPort);

                    bool mawbExists = true;

                    var mawb =
                        (from mw in db.AWBs
                         where mw.AWBSerialNumber.ToUpper().Equals(mawbNo) &&
                         mw.ManifestAWBDetails.Any(x => x.FlightManifest.AircraftRegistration.Equals(flManifest.AircraftRegistration)) &&
                         mw.Account.Equals(acc)
                         select mw).FirstOrDefault();
                    if (mawb == null)
                    {
                        mawb = new AWB()
                                   {
                                       AWBSerialNumber = mawbNo,
                                       CarrierId = carr.Id,
                                       ReceivedPieces = 0,
                                       PutAwayPieces = 0,
                                       PercentReceived = 0,
                                       PercentPutAway = 0,
                                       AvailablePieces = 0,
                                       ReleasedPieces = 0,
                                       RecDate = DateTime.UtcNow,
                                       StatusTimestamp = DateTime.UtcNow,
                                       Account = acc
                                   };
                        mawbExists = false;
                    }

                    mawb.Carrier = carr;
                    mawb.Port = origin;
                    mawb.Port1 = destination;
                    mawb.ULDCount = preAlert.AV7.TotalBags;
                    mawb.HBCount = preAlert.AV7.TotalShipments;
                    mawb.TotalWeight = preAlert.AV7.TotalWeight;
                    mawb.TotalPieces = preAlert.AV7.TotalPackages;

                    Customer shipper = (from cust in db.Customers where cust.Name.Equals(Constants.ShipperNameIPC) select cust).FirstOrDefault();
                    if (shipper == null)
                    {
                        internalError.AddErrorMessage("Missing shipper account for: " + Constants.ShipperNameIPC);
                    }
                    Customer consignee = (from cust in db.Customers where cust.Name.Equals(Constants.ConsigneeNameINSLog) select cust).FirstOrDefault();
                    if (consignee == null)
                    {
                        internalError.AddErrorMessage("Missing consignee account for: " + Constants.ConsigneeNameINSLog);
                    }
                    UOM weightUOM = (from uoms in db.UOMs where uoms.TwoCharCode.Equals(preAlert.AV7.UOM) select uoms).FirstOrDefault();
                    if (weightUOM == null)
                    {
                        if (string.IsNullOrEmpty(preAlert.AV7.UOM))
                        {
                            externalError.AddErrorMessage("Missing Weight UOM for AV7.");
                        }
                        else
                        {
                            internalError.AddErrorMessage("Missing UOM in database: " + preAlert.AV7.UOM);
                        }
                    }

                    mawb.TotalWeight = preAlert.AV7.TotalWeight;
                    mawb.UOM = weightUOM;
                    mawb.ShipperId = shipper == null ? 0 : shipper.Id;
                    mawb.ConsigneeId = consignee == null ? 0 : consignee.Id;
                    mawb.StatusId = 1;
                    mawb.StatusTimestamp = DateTime.UtcNow;
                    mawb.LoadType = "LOOSE";
                    mawb.DescriptionOfGoods = "AV7 " + (AV7Str ?? "");

                    if (!mawbExists)
                        db.AWBs.InsertOnSubmit(mawb);


                    ConsignmentCode consCode = (from code in db.ConsignmentCodes where code.Code.ToUpper().Equals("T") select code).FirstOrDefault();
                    if (consCode == null)
                        internalError.AddErrorMessage("Missing consigment code 'T'.");


                    foreach (Bag b in preAlert.Bags)
                    {
                        if (string.IsNullOrEmpty(b.Number))
                        {
                            externalError.AddErrorMessage("Missing Bag number.");
                        }

                        bool uldExists = true;
                        var uld = (from ulds in db.ULDs
                                   where
                                       ulds.SerialNumber.ToUpper().Equals(b.Number) &&
                                       ulds.FlightManifest.AircraftRegistration.ToUpper()
                                           .Equals(flManifest.AircraftRegistration)
                                   select ulds).FirstOrDefault();
                        if (uld == null)
                        {
                            uld = new ULD
                                  {
                                      RecDate = DateTime.UtcNow,
                                      SerialNumber = b.Number,
                                      FlightManifest = flManifest,
                                      TotalReceivedCount = 0,
                                      ReceivedPieces = 0,
                                      PutAwayPieces = 0,
                                      PercentRecovered = 0,
                                      PercentReceived = 0,
                                      PercentPutAway = 0
                                  };
                            uldExists = false;

                        }

                        uld.SerialNumber = b.Number;
                        uld.TotalWeight = b.TotalWeight;
                        uld.TotalPieces = b.TotalPackages;
                        uld.Carrier = carr;

                        UOM uldUOM = (from uoms in db.UOMs where uoms.TwoCharCode.Equals(b.UOM) select uoms).FirstOrDefault();
                        if (uldUOM == null)
                        {
                            if (string.IsNullOrEmpty(b.UOM))
                            {
                                externalError.AddErrorMessage("Missing Weight UOM for AV7.");
                            }
                            else
                            {
                                internalError.AddErrorMessage("Missing UOM in database: " + b.UOM);
                            }
                        }

                        uld.UOM = uldUOM;

                        ShipmentUnitType type = (from types in db.ShipmentUnitTypes where types.Code.ToUpper().Equals(Constants.DefaultULDTypeName) select types).FirstOrDefault();
                        if (type == null)
                            internalError.AddErrorMessage("Missing BAG ULD type in Db.");

                        uld.ShipmentUnitType = type;
                        uld.TotalUnitCount = 1;
                        uld.StatusID = 1;
                        uld.FlightManifest = flManifest;

                        if (!uldExists)
                            db.ULDs.InsertOnSubmit(uld);

                        bool manifestAWBExists = true;
                        var manifestAwb =
                            (from ma in db.ManifestAWBDetails
                             where ma.AWB.Equals(mawb) && ma.FlightManifest.Equals(flManifest)
                             select ma).FirstOrDefault();

                        if (manifestAwb == null)
                        {
                            manifestAwb = new ManifestAWBDetail
                                              {
                                                  FlightManifest = flManifest,
                                                  AWB = mawb,
                                                  ReceivedCount = 0,
                                                  PercentReceived = 0,
                                                  RecDate = DateTime.UtcNow,
                                                  PutAwayPieces = 0,
                                                  PercentPutAway = 0
                                              };
                            manifestAWBExists = false;
                        }

                        manifestAwb.ULD = uld;
                        manifestAwb.LoadCount = uld.TotalPieces;
                        manifestAwb.UnloadingPort = unloadPort;
                        manifestAwb.ConsignmentCodeId = consCode.Id;
                        manifestAwb.Weight = uld.TotalWeight;
                        manifestAwb.UOM = uldUOM;

                        if (!manifestAWBExists)
                            db.ManifestAWBDetails.InsertOnSubmit(manifestAwb);


                        foreach (TrackingNumber trNo in b.TrackingNumbers)
                        {
                            if (string.IsNullOrEmpty(trNo.Number))
                            {
                                externalError.AddErrorMessage("Missing Tracking number.");
                            }

                            if (string.IsNullOrEmpty(trNo.BAGNumber))
                                trNo.BAGNumber = b.Number;

                            bool packExists = true;
                            Package pack =
                                (from packs in db.Packages where packs.TrackingNumber.Equals(trNo.Number) && packs.Account.Equals(acc) select packs)
                                    .FirstOrDefault();

                            if (pack == null)
                            {
                                pack = new Package
                                       {
                                           TrackingNumber = trNo.Number,
                                           RecDate = DateTime.UtcNow,
                                           StatusId = 1,
                                           Account = acc,
                                           ReceivedPieces = 0,
                                           PutAwayPieces = 0,
                                           PercentReceived = 0,
                                           PercentPutAway = 0
                                       };
                                packExists = false;
                            }

                            pack.SaidToContain = 1;
                            pack.PieceNumber = 1;
                            pack.Length = trNo.Length;
                            pack.Width = trNo.Width;
                            pack.Height = trNo.Height;

                            pack.UOM2 = weightUOM;
                            pack.Weight = trNo.Weight;

                            pack.Port = destination;
                            pack.Port1 = origin;

                            pack.UOM2 = weightUOM;

                            //Weight calculation. Change UOM to AV7 UOM. Calculate Sum for HAWB.
                            if (trNo.UOMWeight.ToUpper() == preAlert.AV7.UOM.ToUpper())
                            {
                                pack.Weight = trNo.Weight;
                            }
                            else
                            {
                                if (preAlert.AV7.UOM.ToUpper() == "KG")
                                {
                                    pack.Weight = trNo.Weight * Constants.LBToKGCoeff;
                                }
                                else
                                {
                                    pack.Weight = trNo.Weight * Constants.KGToLBCoeff;
                                }
                            }

                            Carrier accCarrier =
                                (from accCarr in db.AccountCarriers
                                 where accCarr.Account.Equals(acc)
                                 select accCarr.Carrier).FirstOrDefault();

                            PackageType packType = (from pt in db.PackageTypes
                                                    where
                                                        pt.Code.Equals(trNo.PackageType) &&
                                                        pt.Carrier.Equals(accCarrier)
                                                    select pt).FirstOrDefault();
                            pack.PackageType = packType;

                            if (!packExists)
                                db.Packages.InsertOnSubmit(pack);


                            bool hwbExists = true;
                            var hawb =
                                (from hwbs in db.HWBs
                                 where hwbs.HWBSerialNumber.ToUpper().Equals(trNo.Number.ToUpper()) && hwbs.Account.Equals(acc)
                                 select hwbs).FirstOrDefault();

                            if (hawb == null)
                            {
                                hawb = new HWB()
                                {
                                    HWBSerialNumber = trNo.Number.ToUpper(),
                                    ReceivedPieces = 0,
                                    PutAwayPieces = 0,
                                    PercentReceived = 0,
                                    PercentPutAway = 0,
                                    AvailablePieces = 0,
                                    ReleasedPieces = 0,
                                    RecDate = DateTime.UtcNow,
                                    StatusId = 1,
                                    StatusTimestamp = DateTime.UtcNow,
                                    Account = acc
                                };
                                hwbExists = false;
                            }

                            hawb.Pieces = 1;
                            hawb.SLAC = 1;
                            hawb.Customer = consignee;
                            hawb.Port = origin;
                            hawb.Port1 = destination;
                            hawb.Weight = pack.Weight;
                            hawb.UOM = weightUOM;

                            if (!hwbExists)
                                db.HWBs.InsertOnSubmit(hawb);

                            bool hwbPackExists = true;

                            HWBs_Package hwbPack =
                                (from hwbPacks in db.HWBs_Packages
                                 where hwbPacks.Package.Equals(pack) && hwbPacks.HWB.Equals(hawb)
                                 select hwbPacks).FirstOrDefault();

                            if (hwbPack == null)
                            {
                                hwbPack = new HWBs_Package
                                          {
                                              HWB = hawb,
                                              Package = pack,
                                              RecDate = DateTime.UtcNow
                                          };
                                hwbPackExists = false;
                            }

                            if (!hwbPackExists)
                                db.HWBs_Packages.InsertOnSubmit(hwbPack);


                            bool manPackExists = true;
                            ManifestPackageDetail manPackDet = (from manPacks in db.ManifestPackageDetails
                                                                where
                                                                    manPacks.AWB.Equals(mawb) &&
                                                                    manPacks.HWB.Equals(hawb) &&
                                                                    manPacks.Package.Equals(pack)
                                                                select manPacks).FirstOrDefault();

                            if (manPackDet == null)
                            {
                                manPackDet = new ManifestPackageDetail()
                                {
                                    RecDate = DateTime.UtcNow,
                                    AWB = mawb,
                                    HWB = hawb,
                                    FlightManifest = flManifest,
                                    Package = pack,
                                    ULD = uld,
                                    ReceivedPieces = 0,
                                    PutAwayPieces = 0,
                                    PercentReceived = 0,
                                    PercentPutAway = 0
                                };
                                manPackExists = false;
                            }

                            manPackDet.LoadCount = pack.SaidToContain;
                            manPackDet.UnloadingPort = unloadPort;
                            manPackDet.Weight = pack.Weight;
                            manPackDet.WeightUOMId = pack.WeightUOMId;

                            if (!manPackExists)
                                db.ManifestPackageDetails.InsertOnSubmit(manPackDet);
                        }
                    }

                    foreach (Shipment shp in preAlert.Shipments)
                    {
                        if (string.IsNullOrEmpty(shp.Id))
                        {
                            externalError.AddErrorMessage("Missing Shipment id.");
                        }

                        bool hwbExists = true;
                        var hawb =
                            (from hwbs in db.HWBs
                             where hwbs.HWBSerialNumber.ToUpper().Equals(shp.Id.ToUpper()) && hwbs.Account.Equals(acc)
                             select hwbs).FirstOrDefault();

                        if (hawb == null)
                        {
                            hawb = new HWB()
                                       {
                                           ReceivedPieces = 0,
                                           PutAwayPieces = 0,
                                           PercentReceived = 0,
                                           PercentPutAway = 0,
                                           AvailablePieces = 0,
                                           ReleasedPieces = 0,
                                           RecDate = DateTime.UtcNow,
                                           StatusId = 1,
                                           StatusTimestamp = DateTime.UtcNow,
                                           Account = acc
                                       };
                            hwbExists = false;
                        }

                        hawb.HWBSerialNumber = shp.Id;
                        hawb.Pieces = shp.TotalPackages;
                        hawb.SLAC = shp.TotalPackages;
                        hawb.DescriptionOfGoods = shp.GoodsDescription.SafeSubstring(0, 15);
                        hawb.ServiceType = shp.ServiceType;

                        hawb.Customer = consignee;


                        hawb.Port = origin;

                        //Check all receiver fields + receiver contact.
                        //If no 100% match, assume new receiver.

                        State receiverState = (from state in db.States where state.TwoCharacterCode.Equals(shp.ReceiverDetails.State) select state).FirstOrDefault();
                        if (receiverState == null)
                            internalError.AddErrorMessage("Missing receiver state: " + shp.ReceiverDetails.State ?? " ");

                        Country receiverCountry = (from country in db.Countries where country.TwoCharacterCode.Equals(shp.ReceiverDetails.Country) || country.Country1.Equals(shp.ReceiverDetails.Country) select country).FirstOrDefault();
                        if (receiverCountry == null)
                            internalError.AddErrorMessage("Missing receiver country: " + shp.ReceiverDetails.Country ?? " ");

                        Shipment tmpShp = shp;

                        bool receiverExists = false;

                        Customer receiver =
                            (from cust in db.Customers
                             where cust.Name == tmpShp.ReceiverDetails.CompanyName &&
                                    cust.AddressLine1 == tmpShp.ReceiverDetails.Address1 &&
                                    cust.AddressLine2 == tmpShp.ReceiverDetails.Address2 &&
                                    cust.City == tmpShp.ReceiverDetails.City &&
                                    cust.StateId == receiverState.Id && cust.CountryId == receiverCountry.Id &&
                                    cust.PostalCode == tmpShp.ReceiverDetails.ZipCode
                             select cust)
                                .FirstOrDefault();

                        Contact cont;
                        string contactFullName = shp.ReceiverDetails.AttentionOf.Trim();
                        string contactPhone = shp.ReceiverDetails.Phone.ToUpper().Replace(" ", "").Replace("-", "");
                        string contactEmail = shp.ReceiverDetails.Email.Trim();

                        if (receiver != null)
                        {
                            cont =
                            (from contacts in db.Contacts
                             where contacts.Customer.Equals(receiver) &&
                                 contacts.FullName.ToUpper().Equals(contactFullName.ToUpper()) &&
                                 contacts.Phone.ToUpper().Replace(" ", "").Replace("-", "").Equals(contactPhone) &&
                                 contacts.Email.ToUpper().Equals(contactEmail.ToUpper())
                             select contacts
                             ).FirstOrDefault();

                            if (cont != null)
                            {
                                receiverExists = true;
                            }
                        }

                        if (receiverExists == false)
                        {
                            //Add new customer.
                            receiver = new Customer
                            {
                                Name = shp.ReceiverDetails.CompanyName,
                                AddressLine1 = shp.ReceiverDetails.Address1,
                                AddressLine2 = shp.ReceiverDetails.Address2,
                                City = shp.ReceiverDetails.City,
                                State = receiverState,
                                PostalCode = shp.ReceiverDetails.ZipCode,
                                Country = receiverCountry,
                                IsResidential = shp.ReceiverDetails.IsResidental != 0
                            };
                            db.Customers.InsertOnSubmit(receiver);

                            //Add new contact.
                            string[] nameArr = contactFullName.Split(' ');
                            cont = new Contact
                            {
                                Customer = receiver,
                                Phone = contactPhone,
                                FullName = contactFullName,
                                FirstName = nameArr.Any() ? nameArr[0] : "",
                                LastName = nameArr.Count() > 1 ? nameArr[1] : "",
                                Email = contactEmail,
                                Fax = shp.ReceiverDetails.Fax.Replace(" ", "").Replace("-", "")
                            };
                            db.Contacts.InsertOnSubmit(cont);
                        }

                        //Customer - shipper, Customer1 - receiver.
                        hawb.Customer1 = receiver;
                        Port destinationPort = (from ports in db.Ports where ports.StateId.Equals(receiverState.Id) select ports).FirstOrDefault();

                        hawb.Port1 = destinationPort ?? destination;


                        double packWeightSum = 0;

                        foreach (TrackingNumber trNo in shp.TrackingNumbers)
                        {
                            if (string.IsNullOrEmpty(trNo.Number))
                            {
                                externalError.AddErrorMessage("Missing Tracking number.");
                            }
                            bool packExists = true;
                            Package pack =
                                (from packs in db.Packages where packs.TrackingNumber.Equals(trNo.Number) && packs.Account.Equals(acc) select packs)
                                    .FirstOrDefault();

                            if (pack == null)
                            {
                                pack = new Package
                                       {
                                           TrackingNumber = trNo.Number,
                                           RecDate = DateTime.UtcNow,
                                           StatusId = 1,
                                           Account = acc,
                                           ReceivedPieces = 0,
                                           PutAwayPieces = 0,
                                           PercentReceived = 0,
                                           PercentPutAway = 0
                                       };
                                packExists = false;
                            }

                            pack.SaidToContain = 1;
                            pack.PieceNumber = 1;
                            pack.Length = trNo.Length;
                            pack.Width = trNo.Width;
                            pack.Height = trNo.Height;

                            string packUOM = trNo.UOMDim;
                            if (packUOM == "M")
                            {
                                packUOM = "CM";
                                pack.Length *= 100;
                                pack.Width *= 100;
                                pack.Height *= 100;
                            }

                            UOM packDIMUOM = (from uoms in db.UOMs where uoms.TwoCharCode.Equals(packUOM) select uoms).FirstOrDefault();

                            if (packDIMUOM == null)
                            {
                                internalError.AddErrorMessage("Missing UOM in database: " + packUOM);
                            }

                            pack.UOM = packDIMUOM;

                            pack.UOM2 = weightUOM;

                            pack.Port = destination;
                            pack.Port1 = origin;

                            //Weight calculation. Change UOM to AV7 UOM. Calculate Sum for HAWB.
                            if (trNo.UOMWeight.ToUpper() == preAlert.AV7.UOM.ToUpper())
                            {
                                pack.Weight = trNo.Weight;
                            }
                            else
                            {
                                if (preAlert.AV7.UOM.ToUpper() == "KG")
                                {
                                    pack.Weight = trNo.Weight * Constants.LBToKGCoeff;
                                }
                                else
                                {
                                    pack.Weight = trNo.Weight * Constants.KGToLBCoeff;
                                }
                            }

                            packWeightSum += pack.Weight;



                            Carrier accCarrier =
                                (from accCarr in db.AccountCarriers
                                 where accCarr.Account.Equals(acc)
                                 select accCarr.Carrier).FirstOrDefault();

                            PackageType packType = (from pt in db.PackageTypes
                                                    where
                                                        pt.Code.Equals(trNo.PackageType) &&
                                                        pt.Carrier.Equals(accCarrier)
                                                    select pt).FirstOrDefault();
                            pack.PackageType = packType;

                            if (!packExists)
                                db.Packages.InsertOnSubmit(pack);

                            bool hwbPackExists = true;

                            HWBs_Package hwbPack =
                                (from hwbPacks in db.HWBs_Packages
                                 where hwbPacks.Package.Equals(pack) && hwbPacks.HWB.Equals(hawb)
                                 select hwbPacks).FirstOrDefault();

                            if (hwbPack == null)
                            {
                                hwbPack = new HWBs_Package
                                          {
                                              HWB = hawb,
                                              Package = pack,
                                              RecDate = DateTime.UtcNow
                                          };
                                hwbPackExists = false;
                            }

                            if (!hwbPackExists)
                                db.HWBs_Packages.InsertOnSubmit(hwbPack);


                            bool manPackExists = true;
                            ManifestPackageDetail manPackDet = (from manPacks in db.ManifestPackageDetails
                                                                where
                                                                    manPacks.AWB.Equals(mawb) &&
                                                                    manPacks.HWB.Equals(hawb) &&
                                                                    manPacks.Package.Equals(pack)
                                                                select manPacks).FirstOrDefault();

                            if (manPackDet == null)
                            {
                                manPackDet = new ManifestPackageDetail()
                                {
                                    RecDate = DateTime.UtcNow,
                                    AWB = mawb,
                                    HWB = hawb,
                                    FlightManifest = flManifest,
                                    Package = pack,
                                    ULD =
                                    (from bags in flManifest.ULDs where bags.SerialNumber.Equals(trNo.BAGNumber) select bags)
                                        .FirstOrDefault(),
                                    ReceivedPieces = 0,
                                    PutAwayPieces = 0,
                                    PercentReceived = 0,
                                    PercentPutAway = 0
                                };
                                manPackExists = false;
                            }




                            manPackDet.LoadCount = pack.SaidToContain;
                            manPackDet.UnloadingPort = unloadPort;
                            manPackDet.Weight = pack.Weight;
                            manPackDet.WeightUOMId = pack.WeightUOMId;

                            if (!manPackExists)
                                db.ManifestPackageDetails.InsertOnSubmit(manPackDet);
                        }

                        hawb.Weight = packWeightSum;
                        hawb.UOM = weightUOM;

                        if (!hwbExists)
                            db.HWBs.InsertOnSubmit(hawb);

                        AWBs_HWB mbHb = (from awhbs in db.AWBs_HWBs
                                         where
                                             awhbs.AWB.Equals(mawb) &&
                                             awhbs.HWB.Equals(hawb)
                                         select awhbs).FirstOrDefault();
                        if (mbHb == null)
                        {
                            mbHb = new AWBs_HWB()
                            {
                                AWB = mawb,
                                HWB = hawb,
                                RecDate = DateTime.UtcNow
                            };
                            db.AWBs_HWBs.InsertOnSubmit(mbHb);
                        }

                        var hawbUldCounts = (from trNo in shp.TrackingNumbers
                                             group trNo by trNo.BAGNumber
                                                 into gr
                                                 select new { bagId = gr.Key, packs = new { count = gr.Count(), weight = gr.Sum(s => s.Weight) } }).ToDictionary(s => s.bagId, s => s.packs);

                        foreach (var uldNum in hawbUldCounts.Keys)
                        {
                            ULD tmpUld =
                                (from bags in flManifest.ULDs where bags.SerialNumber.Equals(uldNum) select bags)
                                    .FirstOrDefault();
                            if (tmpUld == null)
                            {
                                internalError.AddErrorMessage("Incorrect BAGNumber: " + uldNum + " for shipment " + hawb.HWBSerialNumber);
                                continue;
                            }

                            bool detailsExists = true;
                            ManifestDetail manDet = (from dets in db.ManifestDetails
                                                     where
                                                         dets.AWB.Equals(mawb) && dets.HWB.Equals(hawb) &&
                                                         dets.ULD.Equals(tmpUld) &&
                                                         dets.FlightManifest.Equals(flManifest)
                                                     select dets).FirstOrDefault();
                            if (manDet == null)
                            {
                                manDet = new ManifestDetail()
                                {
                                    AWB = mawb,
                                    HWB = hawb,
                                    ULD = tmpUld,
                                    FlightManifest = flManifest,
                                    ReceivedCount = 0,
                                    PercentReceived = 0,
                                    RecDate = DateTime.UtcNow,
                                    PutAwayPieces = 0,
                                    PercentPutAway = 0
                                };

                                detailsExists = false;
                            }

                            manDet.LoadCount = hawbUldCounts[uldNum].count;
                            manDet.UnloadingPort = unloadPort;
                            manDet.Weight = hawbUldCounts[uldNum].weight;
                            manDet.UOM = tmpUld.UOM; //Weight UOM
                            manDet.ConsignmentCode = consCode;

                            if (!detailsExists)
                                db.ManifestDetails.InsertOnSubmit(manDet);
                        }
                    }

                    TaskType taskType =
                        (from types in db.TaskTypes where types.ActionName.Equals(Constants.InitialTaskName) select types)
                            .FirstOrDefault();
                    if (taskType == null)
                        internalError.AddErrorMessage("Missing initial task record.");

                    EntityType entType =
                        (from entities in db.EntityTypes
                         where entities.EntityName.Equals(Constants.FlightManifestEntityName)
                         select entities).FirstOrDefault();
                    if (entType == null)
                        internalError.AddErrorMessage("Missing Flight Manifest entity type record.");

                    Warehouse wrh = (from wrhs in db.Warehouses where wrhs.Port.IATACode.Equals(destination.IATACode) select wrhs).FirstOrDefault();
                    if (wrh == null)
                    {
                        internalError.AddErrorMessage("Missing warehouse info for destination: " + destination.IATACode);
                    }


                    Task tsk = (from tasks in db.Tasks
                                where tasks.EntityId == flManifest.Id &&
                                tasks.TaskTypeId == taskType.Id &&
                                tasks.EntityTypeId == entType.Id
                                select tasks).FirstOrDefault();
                    if (tsk == null)
                    {
                        tsk = new Task
                              {
                                  TaskType = taskType,
                                  TaskCreationDate = DateTime.UtcNow,
                                  EntityType = entType,
                                  TaskCreator = Constants.TaskCreatorSystem,
                                  TaskDate = flManifest.ETA,
                                  Warehouse = wrh,
                                  StatusId = 1,
                                  StatusTimestamp = DateTime.UtcNow
                              };
                        db.Tasks.InsertOnSubmit(tsk);
                    }

                    if (internalError.ReportException() | externalError.ReportException())
                    {
                        preAlertNo = "";
                        return false; //Errors were submitted. Don't save to database.
                    }

                    EventCode evCode = (from codes in db.EventCodes
                                        where codes.EventName.ToUpper().Equals("PENDING CUSTOMS")
                                        select codes).FirstOrDefault();

                    var tmpEvent = new Event();
                    if (evCode != null)
                    {
                        tmpEvent = new Event()
                                        {
                                            EventCode = evCode,
                                            EntityType = entType,
                                            RecDate = DateTime.Now,
                                            Account = acc,
                                            IsEmailed = 0,
                                            IsTransmitted = 0,
                                            EventReference = evCode.EventName,
                                            UserName = "HostPlus"
                                        };
                        db.Events.InsertOnSubmit(tmpEvent);
                    }

                    db.SubmitChanges();
                    tmpEvent.EntityId = flManifest.Id;
                    tsk.EntityId = flManifest.Id;
                    flManifest.IsComplete = true;
                    db.SubmitChanges();
                    preAlertNo = AV7Str;
                    return true;
                }
            }
            catch (SqlException ex)
            {
                throw new ApplicationException("Database error: " + ex.ToString());
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Application error: " + ex.ToString());
            }
        }

        #endregion

        #region IDisposable Implementation
        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.

                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                _disposed = true;

            }
        }

        ~PreAlertParser()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion
    }
}
