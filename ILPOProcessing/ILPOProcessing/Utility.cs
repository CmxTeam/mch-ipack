﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace PreAlertProcessing
{
    internal static class Constants
    {
        public const string FlightManifestInitialStatus = "Recovery Pending";
        public const string ShipperNameIPC = "Israel Postal Company";
        public const string ConsigneeNameINSLog = "INS LOGISTICS";
        public const string PoundUOMCode = "LB";
        public const string DefaultULDTypeName = "BAG";
        public const string InitialTaskName = "Recover Flight";
        public const string FlightManifestEntityName = "FlightManifest";
        public const string TaskCreatorSystem = "System";
        public const string AV7InternalErrorMailTask = "AV7 Internal Error";
        public const string AV7ExternalErrorMailTask = "AV7 External Error";
        public const string AV7SendMailTask = "AV7 send email";
        public const double KGToLBCoeff = 2.20462;
        public const double LBToKGCoeff = 0.453592;
    }

    internal static class UtilityMethods
    {
        public static string SafeSubstring(this string str, int start, int length = -1)
        {
            if (string.IsNullOrEmpty(str) || start > str.Length - 1 || start < 0 || length < -1)
                return "";
            else if (length == -1 || length + start > str.Length)
                return str.Substring(start);
            else
                return str.Substring(start, length);
        }

        public static bool SendAV7ErrorEmail(string message, string template, string av7, string connectionString)
        {
            var db = new MCHCoreDBDataContext(connectionString);

            HostPlus_AutoEmail mailTemplate =
                (from autoMail in db.HostPlus_AutoEmails where autoMail.TaskName.ToUpper().Equals(template.ToUpper()) select autoMail)
                    .FirstOrDefault();

            if (mailTemplate == null)
                throw new ApplicationException("Missing error mail template " + template + ". Error message is: " + message);

            var mail = new HostPlus_Email()
                                      {
                                          MailFrom = mailTemplate.MailForm,
                                          MailTo = mailTemplate.MailTo,
                                          MailCc = mailTemplate.MailCc,
                                          MailBcc = mailTemplate.MailBcc,
                                          Subject = mailTemplate.Subject.Replace("@@AV7@@", av7),
                                          Body = mailTemplate.BodyHeader + Environment.NewLine + mailTemplate.BodyDetail.Replace("@@AV7@@", av7).Replace("@@ERRORS@@", message)
                                                    + Environment.NewLine + mailTemplate.BodyFooter,
                                          RecDate = DateTime.Now,
                                          Status = 0,
                                          IsAutoEmail = 0,
                                          IsZip = 0,
                                          Gateway = "",
                                          Tries = 0
                                      };
            db.HostPlus_Emails.InsertOnSubmit(mail);
            db.SubmitChanges();
            return true;
        }

        public static bool SendAV7Email(string av7Number, string fileName, string connectionString, string labelFolder)
        {
            var db = new MCHCoreDBDataContext(connectionString);

            HostPlus_AutoEmail mailTemplate =
                (from autoMail in db.HostPlus_AutoEmails where autoMail.TaskName.ToUpper().Equals(Constants.AV7SendMailTask) select autoMail)
                    .FirstOrDefault();

            if (mailTemplate == null)
                throw new ApplicationException("Missing AV7 mail template " + Constants.AV7SendMailTask + ".");

            var mail = new HostPlus_Email()
            {
                MailFrom = mailTemplate.MailForm,
                MailTo = mailTemplate.MailTo,
                MailCc = mailTemplate.MailCc,
                MailBcc = mailTemplate.MailBcc,
                Subject = mailTemplate.Subject.Replace("@@AV7@@", av7Number),
                Body = mailTemplate.BodyHeader + Environment.NewLine + mailTemplate.BodyDetail
                          + Environment.NewLine + mailTemplate.BodyFooter,
                RecDate = DateTime.Now,
                Status = 0,
                IsAutoEmail = 0,
                IsZip = 0,
                Gateway = "",
                Tries = 0
            };
            db.HostPlus_Emails.InsertOnSubmit(mail);

            //TODO: Get attachments.
            DirectoryInfo dir = Directory.GetParent(fileName);
            if (dir != null)
            {
                dir = dir.Parent;
                if (dir == null)
                    return false;

                string[] file = Path.GetFileNameWithoutExtension(fileName).Split('-');

                if (file.Count() != 3)
                    return false;

                string date = file[0];
                string av7 = file[2];

                string av7Dir = Path.Combine(dir.FullName, "AV7");
                List<string> sendFiles = new List<string>();
                if (!string.IsNullOrEmpty(date) && date.Length == 8)
                {
                    //Get Labels if any from folder LABELS/YYYY-MM-DD
                    //File name has date as YYYYMMDD, folder name is YYYY-MM-DD
                    var folderName = date.Substring(0, 4) + "-" + date.Substring(4, 2) + "-" + date.Substring(6, 2);
                    var labelDir = Path.Combine(labelFolder, folderName);

                    if (Directory.Exists(labelDir))
                    {
                        sendFiles = (from files in Directory.GetFiles(labelDir)
                                     where Path.GetFileNameWithoutExtension(files).StartsWith(date)
                                           && Path.GetFileNameWithoutExtension(files).EndsWith(av7)
                                     select files).ToList();

                    }
                }

                sendFiles.AddRange((from files in Directory.GetFiles(av7Dir)
                                    where Path.GetFileNameWithoutExtension(files).StartsWith(date)
                                          && Path.GetFileNameWithoutExtension(files).EndsWith(av7)
                                    select files).ToList());

                foreach (var av7File in sendFiles)
                {
                    var attachment = new HostPlus_EmailAttachment()
                                    {
                                        HostPlus_Email = mail,
                                        FileName = av7File
                                    };
                    db.HostPlus_EmailAttachments.InsertOnSubmit(attachment);
                }

            }

            db.SubmitChanges();
            return true;
        }
    }
}
