﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PreAlertProcessing
{
    public abstract class HardError
    {
        public bool ReportException()
        {
            if (_errors == null || _errors.Count == 0)
                return false;
            
            string message = _errors.Aggregate((i, j) => i + Environment.NewLine + j).Trim();
            UtilityMethods.SendAV7ErrorEmail(message, _template, AV7, GlobalConnection);

            return true;
        }

        protected HardError()
        {
            _errors = new List<string>();
        }

        protected HardError(string message)
        {
            _errors = new List<string>() { message };
        }

        public void AddErrorMessage(string errorMessage)
        {
            if (_errors == null)
                _errors = new List<string>();
            if(!_errors .Contains(errorMessage))
                _errors.Add(errorMessage);
        }

        public string GlobalConnection { protected get; set; }
        public string AV7 { protected get; set; }
        protected List<string> _errors;
        protected string _template;
    }

    public class InternalHardError : HardError
    {
        public InternalHardError()
            : base()
        {
            _template = Constants.AV7InternalErrorMailTask;
        }
        public InternalHardError(string message)
            : base(message)
        {
            _template = Constants.AV7InternalErrorMailTask;
        }
    }
    public class ExternalHardError : HardError
    {
        public ExternalHardError()
            : base()
        {
            _template = Constants.AV7ExternalErrorMailTask;
        }
        public ExternalHardError(string message)
            : base(message)
        {
            _template = Constants.AV7ExternalErrorMailTask;
        }
    }
}
