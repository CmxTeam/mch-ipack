﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using EventService.DBLayer;
using EventService.TevelEventService;

namespace EventService
{
    public class EventsProcessing : IDisposable
    {
        #region Constructor
        public EventsProcessing(string connection, string tevelServiceURL, string user, string systemcode, string password)
        {
            ConnectionString = connection;
            TevelServiceURL = tevelServiceURL;
            UserName = user;
            SystemCode = systemcode;
            Password = password;
        }
        #endregion

        #region Public Methods
        public void SendEvents()
        {
            try
            {
                var db = new MCHDbDataContext(ConnectionString);
                Event[] dbEvents = GetUnprocessedEvents(db);
                var events = dbEvents.Select(ConvertToTevelEvent).ToList();

                var factory = new ChannelFactory<WcfTevelEventServiceChannel>(new WebHttpBinding(WebHttpSecurityMode.Transport), TevelServiceURL);

                factory.Endpoint.Behaviors.Add(new WebHttpBehavior()
                {
                    DefaultOutgoingRequestFormat = WebMessageFormat.Json,
                    DefaultOutgoingResponseFormat = WebMessageFormat.Json
                });

                WcfTevelEventServiceChannel proxy = factory.CreateChannel();
                //proxy.OperationTimeout = new TimeSpan(0, 5, 0);
                ResultOfArrayOfboolean result;

                using (var scope = new OperationContextScope(proxy))
                {
                    AddHeaderParams(WebOperationContext.Current);

                    result = proxy.InsertEvents(events.ToArray());

                    proxy.Close();
                }

                for (int i = 0; i < result.Item.Length; i++)
                {
                    dbEvents[i].IsTransmitted = result.Item[i] ? (byte)1 : (byte)2;
                    dbEvents[i].ResponseStatusCode = result.Status[i].StatusCode;

                    //Gets ';' separated list of all messages.
                    string msgs = result.Status[i].Messages.Aggregate("", (current, message) => current + (message.Description + ";"));
                    dbEvents[i].ResponseStatusMessages = msgs == null ? "" : msgs.Trim(new[] { ' ', ';' });
                }

                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.ToString());
            }

        }
        #endregion

        private TevelEventDTO ConvertToTevelEvent(Event dbEvent)
        {
            var eventDTO = new TevelEventDTO()
                                         {
                                             ElementTypeID = ConvertToTevelTypeId(dbEvent.EntityType),
                                             ElementID = GetEntityName(dbEvent.EntityId, dbEvent.EntityType),
                                             EventSource = ConvertToTevelEventSource(dbEvent.EventCode.EventCodeOwner.Name),
                                             EventDescription = dbEvent.EventCode.Description,
                                             EventCode = dbEvent.EventCode.EventCode1,
                                             SubEventCode = dbEvent.EventCode.EventSubCode,
                                             EventDate = dbEvent.RecDate,
                                             OperationName = dbEvent.UserName,
                                             ReceiverName = dbEvent.EventPOD ?? ""
                                         };
            return eventDTO;
        }

        private string GetEntityName(long entityId, EntityType entityType)
        {
            var db = new MCHDbDataContext(ConnectionString);
            string retval = "";
            if (entityType.EntityName == "FlightManifest")
            {
                var entity = (from flMan in db.FlightManifests
                              where flMan.Id.Equals(entityId)
                              select flMan).FirstOrDefault();
                retval = entity == null ? "" : entity.AircraftRegistration;
            }
            else if (entityType.EntityName == "ULD")
            {
                var entity = (from ulds in db.ULDs
                              where ulds.Id.Equals(entityId)
                              select ulds).FirstOrDefault();
                retval = entity == null ? "" : entity.SerialNumber;
            }
            else if (entityType.EntityName == "Package")
            {
                var entity = (from packs in db.Packages
                              join mans in db.Manifests on packs.ManifestId equals mans.Id into packMans
                              from pm in packMans.DefaultIfEmpty()
                              where packs.Id.Equals(entityId)
                              select new { packs.TrackingNumber, Number = pm == null ? "" : ", " + pm.Number }).FirstOrDefault();
                retval = entity == null ? "" : entity.TrackingNumber + entity.Number;
            }
            return retval;
        }

        //Using function to avoid using Enum.Parse or similar.
        private string ConvertToTevelEventSource(string cmxName)
        {
            switch (cmxName)
            {
                case "UPS":
                    return "1";
                case "MCH":
                    return "2";
                default:
                    return "";
            }
        }

        private int ConvertToTevelTypeId(EntityType type)
        {
            if (type.EntityName.Equals("FlightManifest", StringComparison.InvariantCultureIgnoreCase))
                return 1;
            if (type.EntityName.Equals("ULD", StringComparison.InvariantCultureIgnoreCase))
                return 2;
            if (type.EntityName.Equals("Package", StringComparison.InvariantCultureIgnoreCase))
                return 3;
            return 0;
        }

        private Event[] GetUnprocessedEvents(MCHDbDataContext dbContext)
        {
            return (from events in dbContext.Events
                    where events.IsTransmitted.Equals(0)
                           && events.EventCodeId != null &&
                          (events.EntityType.EntityName.Equals("FlightManifest") ||
                           events.EntityType.EntityName.Equals("ULD") || events.EntityType.EntityName.Equals("Package"))
                    select events).Take(1).ToArray();
        }

        private void AddHeaderParams(WebOperationContext context)
        {
            context.OutgoingRequest.Headers.Add("UserName", UserName);
            context.OutgoingRequest.Headers.Add("SystemCode", SystemCode);
            context.OutgoingRequest.Headers.Add("Password", Password);
        }

        #region Fields
        /// <summary>
        /// DB connection string.
        /// </summary>
        public string ConnectionString { get; private set; }
        /// <summary>
        /// URL for Tevel Events service.
        /// </summary>
        public string TevelServiceURL { get; private set; }
        /// <summary>
        /// UserName for Tevel Events service.
        /// </summary>
        public string UserName { get; private set; }
        /// <summary>
        /// System Code for Tevel Events service.
        /// </summary>
        public string SystemCode { get; private set; }
        /// <summary>
        /// Password for Tevel Events service.
        /// </summary>
        public string Password { get; private set; }
        #endregion

        #region IDisposable Implementation
        private bool _disposed;
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.

                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                _disposed = true;

            }
        }

        ~EventsProcessing()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion
    }
}
